/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function($) {
	$.fn.tree = function(settings) {
		var params = {
			url : "",
			data : null,
			lUrl : "",
			sUrl : "",
			target : "lookup"
		};
		$.extend(params, settings);
		var tree = $(this);
		buildTree();
		function buildTree() {
			$.ajax({
				type : "post",
				dataType : "json",
				url : params.url,
				data : params.data,
				success : function(data) {
					var parent = $("<ul class=\"tree treeFolder\"></ul>");
					if (params.target == "lookup") {// 查找带回
						each_lookup(data.column, parent);
					}
					if (params.target == "content") {// 内容
						each_content(data.column, parent, params.lUrl);
					}
					if (params.target == "column") {// 栏目
						each_column(data.column, parent, params.lUrl, params.sUrl);
					}
					tree.append(parent);
				},
				complete : function() {
					$("ul.tree", tree).jTree();
					$("a[target=dialog]", tree).each(function() {
						$(this).click(function(event) {
							var $this = $(this);
							var title = $this.attr("title") || $this.text();
							var rel = $this.attr("rel") || "_blank";
							var options = {};
							var w = $this.attr("width");
							var h = $this.attr("height");
							if (w)
								options.width = w;
							if (h)
								options.height = h;
							options.max = eval($this.attr("max") || "false");
							options.mask = eval($this.attr("mask") || "false");
							options.maxable = eval($this.attr("maxable") || "true");
							options.minable = eval($this.attr("minable") || "true");
							options.fresh = eval($this.attr("fresh") || "true");
							options.resizable = eval($this.attr("resizable") || "true");
							options.drawable = eval($this.attr("drawable") || "true");
							options.close = eval($this.attr("close") || "");
							options.param = $this.attr("param") || "";
							var url = unescape($this.attr("href")).replaceTmById($(event.target).parents(".unitBox:first"));
							DWZ.debug(url);
							if (!url.isFinishedTm()) {
								alertMsg.error($this.attr("warn") || DWZ.msg("alertSelectMsg"));
								return false;
							}
							$.pdialog.open(url, rel, title, options);
							return false;
						});
					});
					$("a[target=ajax]", tree).each(function() {
						$(this).click(function(event) {
							var $this = $(this);
							var rel = $this.attr("rel");
							if (rel) {
								var $rel = $("#" + rel);
								$rel.loadUrl($this.attr("href"), {}, function() {
									$rel.find("[layoutH]").layoutH();
								});
							}
							event.preventDefault();
						});
					});
				},
				error : function(error) {
					alertMsg.error(error.responseText);
				}
			});
		}
		function each_lookup(json, parent) {
			for ( var data in json) {
				if (json[data].column.length > 0) {
					var li = $("<li><a href=\"javascript:\" onclick=\"$.bringBack({id:'" + json[data].id + "', name:'" + json[data].name + "'})\">" + json[data].name + "</a></li>");
					$(li).append("<ul></ul>").appendTo(parent);
					each_lookup(json[data].column, $(li).children().eq(1));
				} else {
					$("<li><a href=\"javascript:\" onclick=\"$.bringBack({id:'" + json[data].id + "', name:'" + json[data].name + "'})\">" + json[data].name + "</a></li>")
							.appendTo(parent);
				}
			}
		}
		function each_content(json, parent, lUrl) {
			for ( var data in json) {
				if (json[data].column.length > 0) {
					var li = $("<li><a href=\"" + lUrl + "?id=" + json[data].id + "\" target=\"ajax\" rel=\"jbsxBox2\">" + json[data].name + "</a></li>");
					$(li).append("<ul></ul>").appendTo(parent);
					each_content(json[data].column, $(li).children().eq(1), lUrl);
				} else {
					$("<li><a href=\"" + lUrl + "?id=" + json[data].id + "\" target=\"ajax\" rel=\"jbsxBox2\">" + json[data].name + "</a></li>").appendTo(parent);
				}
			}
		}
		function each_column(json, parent, lUrl, sUrl) {
			for ( var data in json) {
				if (json[data].column.length > 0) {
					var li = $("<li><a href=\"" + lUrl + "?id=" + json[data].id + "\" target=\"ajax\" rel=\"jbsxBox2\">" + json[data].name + "</a></li>");
					$(li).append("<ul></ul>").appendTo(parent);
					each_column(json[data].column, $(li).children().eq(1), lUrl, sUrl);
				} else {
					$("<li><a href=\"" + sUrl + "?id=" + json[data].id + "\" target=\"dialog\" max=\"true\" rel=\"edit\">" + json[data].name + "</a></li>").appendTo(parent);
				}
			}
		}
		return tree;
	};
})(jQuery);