/**
 * @requires jquery.validate.js
 * @author ZhangHuihua@msn.com
 */
(function($){
	if ($.validator) {
		
		$.validator.addMethod("bwlistcode", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9.]+$/i.test(value);
		}, "Letters, numbers or . only please");

		$.validator.addMethod("phoneformat", function(value, element) {
			return this.optional(element) || /^[0-9-*]*$/i.test(value);
		}, "numbers or underscores only please");

		$.validator.addMethod("username", function(value, element) {
			return this.optional(element) || /^[0-9a-z_A-Z\u4e00-\u9fa5]+$/i.test(value);
		}, "Letters, numbers, chinese or underscores only please");
		
		$.validator.addMethod("numword", function(value, element) {
			return this.optional(element) || /^([0-9a-zA-Z])*$/i.test(value);
		}, "仅支持字母、数字");

		$.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^\w+$/i.test(value);
		}, "Letters, numbers or underscores only please");
		
		$.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z]+$/i.test(value);
		}, "Letters only please"); 
		
		$.validator.addMethod("phone", function(value, element) {
			return this.optional(element) || /^[0-9 \(\)]{7,30}$/.test(value);
		}, "Please specify a valid phone number");
		
		$.validator.addMethod("postcode", function(value, element) {
			return this.optional(element) || /^[0-9 A-Za-z]{5,20}$/.test(value);
		}, "Please specify a valid postcode");
		
		$.validator.addMethod("date", function(value, element) {
			value = value.replace(/\s+/g, "");
			if (String.prototype.parseDate){
				var $input = $(element);
				var pattern = $input.attr('format') || 'yyyy-MM-dd';
	
				return !$input.val() || $input.val().parseDate(pattern);
			} else {
				return this.optional(element) || value.match(/^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/);
			}
		}, "Please enter a valid date.");
		
		$.validator.addMethod("username", function(value, element) {
			return this.optional(element) || /^[0-9a-z_A-Z\u4e00-\u9fa5]+$/.test(value);
		}, "Please enter the letters, numbers, chinese Or underline");
		
		$.validator.addMethod("mobile", function(value, element) {
			return this.optional(element) || /^13\d{9}$/g.test(value) || /^15[0-35-9]\d{8}$/g.test(value)
				|| /^18\d{9}$/g.test(value);
		}, "Please specify a valid mobile number");
		
		$.validator.addClassRules({
			bwlistcode:{bwlistcode:true},
			date: {date: true},
			alphanumeric: { alphanumeric: true },
			lettersonly: { lettersonly: true },
			phone: { phone: true },
			mobile: { mobile: true },
			postcode: {postcode: true},
			username: {username: true}
		});
		
		$.validator.setDefaults({errorElement:"span"});
		$.validator.autoCreateRanges = true;
		
		$.validator.addMethod("txnamount",function(value, element) {
			return this.optional(element) || /^([+]{1})?([0-9]+(\.[0-9]{1,2})?)?$/i.test(value);
		},"请输入最大精度为小数点后2位的数值");
		
		$.validator.addMethod("split_resp", function(value, element) {
			if(value==""){
				return true;
			}
			var arrAmount = value.split(",");
			for(var i=0;i<arrAmount.length;i++){
				if(!/^[0-9a-zA-Z]+$/i.test(arrAmount[i])){
					return false;
				}
			}
			return true;
		}, "仅支持字母、数字、逗号分隔符");
		
		$.validator.addMethod("desc", function(value, element) {
			return this.optional(element) || /^[^\<\>]*$/i.test(value);
		}, "描述不能包含 '<'或'>' 符号");
		$.validator.addMethod("split",function(value, element) {
			if(value==""){
				return true;
			}
			var arrAmount = value.split(";");
			for(var i=0;i<arrAmount.length;i++){
				if(!/^[0-9]{1,15}$/i.test(arrAmount[i])){
					return false;
				}
			}
			return true;
		},"类型(15位数字)或分隔符(分号)有误");
		$.validator.addMethod("split_comma",function(value, element) {
			if(value==""){
				return true;
			}
			var arrAmount = value.split(",");
			for(var i=0;i<arrAmount.length;i++){
				if(!/^[0-9]{1,15}$/i.test(arrAmount[i])){
					return false;
				}
			}
			return true;
		},"仅支持数字、逗号分隔符");		
		$.validator.addMethod("splitAmount",function(value, element) {
			if(value==""){
				return true;
			}
			var arrAmount = value.split(",");
			for(var i=0;i<arrAmount.length;i++){
				if(!/^(([1]?([0-9]{1,11}(\.[0-9]{1,2})?)))$/i.test(arrAmount[i])){
					return false;
				}
			}
				return true;
		},"金额类型(2位小数)或分隔符有误");
		$.validator.addMethod("splitEmail",function(value, element) {
			if(value==""){
				return true;
			}
			var arrAmount = value.split(";");
			for(var i=0;i<arrAmount.length;i++){
				if(!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(arrAmount[i])){
					return false;
				}
			}
			return true;
		},"邮件格式或分隔符(分号)有误");
	}

})(jQuery);