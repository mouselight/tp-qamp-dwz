<#list rootList as item>
	<#if item.isMenu?? && item.isMenu>
		<li>
			<a tname="idModules" tvalue="${item.idModule!}" >${item.name!}</a>
			<#if item.child?? && item.child?size gt 0 >
				<ul>
		            <#assign rootList = item.child>
		            <#include "/ftl/module/module_check.ftl">
	            </ul>
	        </#if>
        </li>
	<#else>	
		<li>
			<a tname="idModules" tvalue="${item.idModule!}" class="required" 
				<#if modulesMap?? ><#if modulesMap[item.idModule?string]?? >checked="true"</#if></#if> >${item.name!}</a>
		</li>
	</#if>
</#list> 