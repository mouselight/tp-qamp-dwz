<#list childList as item>
	<#if item.isMenu?? && item.isMenu && userModelsMap[item.idModule?string]?? >
		<li><a href="${base}${item.uri!}" target="navTab" rel="${item.description!}">${item.name!}</a>
			<#if item.child?? && menuModelsMap[item.idModule?string]?? && item.child?size gt 0 >
				<ul>
			        <#assign childList = item.child>
			        <#include "/ftl/module/module_navigate.ftl">
		        </ul>
		    </#if>
	    </li>
	</#if>
</#list>