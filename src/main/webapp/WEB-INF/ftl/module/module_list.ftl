<#list childList as item>
	<#if item.isMenu?? && item.isMenu>
		<li>
			<!--文件夹连接跳转至列表-->
			<#if item.child?? && item.child?size gt 0>
				<a href="${base}/framwork/module/list_module_child.htm?parentId=${item.idModule}" target="ajax" rel="jbsxBox">${item.name!}</a>
			<#else>
				<a target="dialog" width="570" height="400" mask="true" rel="edit_module" 
					href="${base}/framwork/module/edit_module.htm?idModule=${item.idModule!}" title="修改权限模型">
					<span>${item.name!}</span>
				</a>
			</#if>
			<#if item.child?? && item.child?size gt 0 >
				<ul>
	                <#assign childList = item.child>
	                <#include "/ftl/module/module_list.ftl">
                </ul>
            </#if>
        </li>
    <#else>	
		<li>
			<a target="dialog" target="dialog" width="570" height="400" mask="true" rel="edit_module" 
				href="${base}/framwork/module/edit_module.htm?idModule=${item.idModule!}" title="修改权限模型">
				<span>${item.name!}</span>
			</a>
		</li>
	</#if>	
</#list>