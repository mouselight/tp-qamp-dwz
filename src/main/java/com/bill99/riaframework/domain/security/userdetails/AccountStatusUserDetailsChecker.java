package com.bill99.riaframework.domain.security.userdetails;

import com.bill99.riaframework.domain.security.AccountExpiredException;
import com.bill99.riaframework.domain.security.AccountStatusException;
import com.bill99.riaframework.domain.security.CredentialsExpiredException;
import com.bill99.riaframework.domain.security.DisabledException;
import com.bill99.riaframework.domain.security.LockedException;

/**
 * @version $Id: AccountStatusUserDetailsChecker.java 3558 2009-04-15 07:39:21Z ltaylor $
 */
public class AccountStatusUserDetailsChecker implements UserDetailsChecker {
	public void check(UserDetails user) throws AccountStatusException {
		if (!user.isAccountNonLocked()) {
			throw new LockedException();
		}

		if (!user.isEnabled()) {
			throw new DisabledException("User is disabled", user);
		}

		if (!user.isAccountNonExpired()) {
			throw new AccountExpiredException("User account has expired", user);
		}

		if (!user.isCredentialsNonExpired()) {
			throw new CredentialsExpiredException("User credentials have expired", user);
		}
	}
}