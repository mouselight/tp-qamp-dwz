/*
   File: 
   Description:
   Copyright 2004-2012 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-10-11		    leofung	   		leofung
 */

package com.bill99.riaframework.domain.security;

/**
 * @author leofung
 * 
 */
@SuppressWarnings("serial")
public class UserNoPermsException extends AuthenticationException {
	public UserNoPermsException() {
	}

	public UserNoPermsException(String msg) {
		super(msg);
	}
}
