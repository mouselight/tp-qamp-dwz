package com.bill99.riaframework.domain.security.rememberme;

@SuppressWarnings("serial")
public class InvalidCookieException extends RememberMeAuthenticationException {
	public InvalidCookieException() {
	}

	public InvalidCookieException(String msg) {
		super(msg);
	}
}
