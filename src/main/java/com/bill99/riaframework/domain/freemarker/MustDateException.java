package com.bill99.riaframework.domain.freemarker;

import freemarker.template.TemplateModelException;

/**
 * date参数异常
 * 
 */
@SuppressWarnings("serial")
public class MustDateException extends TemplateModelException {
	public MustDateException(String paramName) {
		super("The \"" + paramName + "\" parameter must be a date.");
	}
}
