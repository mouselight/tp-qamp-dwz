package com.bill99.riaframework.domain.mail.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.riaframework.domain.mail.EmailSender;

public class EmailSenderImpl implements EmailSender {

	private static final Bill99Logger logger = Bill99Logger.getLogger(EmailSenderImpl.class);

	private JavaMailSender mailSender;
	private String defaultFromAddress;
	private String subject;
	private String content;
	private List<String> recieveAddressList;
	private List<String> ccAddressList;

	private String displaySender;
	private String displayName;

	/* (non-Javadoc)
	 * @see com.bill99.riaframework.domain.mail.impl.EmailSender#sendEmail(java.util.List, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.util.List)
	 */
	public void sendEmail(List<String> recvAddress, String subject, String content, String fromAddress, List bytesList, List nameList) throws UnsupportedEncodingException,
			MessagingException {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		if (null == recvAddress || recvAddress.size() == 0) {
			return;
		}
		this.processAddress(helper, recvAddress, null, null);
		sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
		helper.setFrom(new InternetAddress(!StringUtils.hasLength(displaySender) ? fromAddress : displaySender, "=?gb2312?B?" + enc.encode(displayName.getBytes("gb2312")) + "?="));
		helper.setSubject(subject);
		helper.setText(content, true);
		if (null != bytesList && null != nameList) {
			for (int j = 0; j < nameList.size(); j++) {
				String attachmentName = (String) nameList.get(j);
				InputStreamSource inputStreamSource = new ByteArrayResource((byte[]) bytesList.get(j));
				//添加附件
				helper.addAttachment(MimeUtility.encodeWord(attachmentName), inputStreamSource);
			}
		}
		mailSender.send(mimeMessage);
	}

	/* (non-Javadoc)
	 * @see com.bill99.riaframework.domain.mail.impl.EmailSender#sendEmail(java.util.List, java.util.List, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.util.List)
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, String subject, String content, String fromAddress, List bytesList, List nameList)
			throws UnsupportedEncodingException, MessagingException {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		if (null == recvAddress || recvAddress.size() == 0) {
			return;
		}
		this.processAddress(helper, recvAddress, ccAddress, null);
		sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
		helper.setFrom(new InternetAddress(!StringUtils.hasLength(displaySender) ? fromAddress : displaySender, "=?gb2312?B?" + enc.encode(displayName.getBytes("gb2312")) + "?="));
		helper.setSubject(subject);
		helper.setText(content, true);
		if (null != bytesList && null != nameList) {
			for (int j = 0; j < nameList.size(); j++) {
				String attachmentName = (String) nameList.get(j);
				InputStreamSource inputStreamSource = new ByteArrayResource((byte[]) bytesList.get(j));
				//添加附件
				helper.addAttachment(MimeUtility.encodeWord(attachmentName), inputStreamSource);
			}
		}
		mailSender.send(mimeMessage);
	}

	/* (non-Javadoc)
	 * @see com.bill99.riaframework.domain.mail.impl.EmailSender#sendEmail(java.util.List, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void sendEmail(List<String> recvAddress, String subject, String content, String fromAddress) throws Exception {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		if (null == recvAddress || recvAddress.size() == 0) {
			return;
		}
		this.processAddress(helper, recvAddress, null, null);
		sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
		helper.setFrom(new InternetAddress(!StringUtils.hasLength(displaySender) ? fromAddress : displaySender, "=?gb2312?B?" + enc.encode(displayName.getBytes("gb2312")) + "?="));
		helper.setSubject(subject);
		helper.setText(content, true);
		mailSender.send(mimeMessage);
	}

	/* (non-Javadoc)
	 * @see com.bill99.riaframework.domain.mail.impl.EmailSender#sendEmail(java.util.List, java.util.List, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, String subject, String content, String fromAddress) throws Exception {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		if (null == recvAddress || recvAddress.size() == 0) {
			return;
		}
		this.processAddress(helper, recvAddress, ccAddress, null);
		sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
		helper.setFrom(new InternetAddress(!StringUtils.hasLength(displaySender) ? fromAddress : displaySender, "=?gb2312?B?" + enc.encode(displayName.getBytes("gb2312")) + "?="));
		helper.setSubject(subject);
		helper.setText(content, true);
		try {
			if (content.contains("cid:logo")) {
				helper.addInline("logo", new ClassPathResource("/res/img/logo_99bill.jpg"));
			}
		} catch (Exception e) {
			logger.error("-- get logo_99bill error --", e);
		}
		mailSender.send(mimeMessage);
	}

	/* (non-Javadoc)
	 * @see com.bill99.riaframework.domain.mail.impl.EmailSender#sendEmail(java.util.List, java.util.List, java.util.List, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.util.List)
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, List<String> bccAddress, String subject, String content, String fromAddress, List bytesList,
			List nameList) throws UnsupportedEncodingException, MessagingException {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		if (null == recvAddress || recvAddress.size() == 0) {
			return;
		}
		this.processAddress(helper, recvAddress, ccAddress, bccAddress);
		sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
		helper.setFrom(new InternetAddress(!StringUtils.hasLength(displaySender) ? fromAddress : displaySender, "=?gb2312?B?" + enc.encode(displayName.getBytes("gb2312")) + "?="));
		helper.setSubject(subject);
		helper.setText(content, true);
		if (bytesList != null && bytesList.size() > 0 && nameList != null && nameList.size() > 0) {
			for (int j = 0; j < nameList.size(); j++) {
				String attachmentName = (String) nameList.get(j);
				InputStreamSource inputStreamSource = new ByteArrayResource((byte[]) bytesList.get(j));
				//添加附件
				helper.addAttachment(MimeUtility.encodeWord(attachmentName), inputStreamSource);
			}
		}
		mailSender.send(mimeMessage);
	}

	private void processAddress(MimeMessageHelper helper, List<String> recvAddress, List<String> ccAddress, List<String> bccAddress) throws MessagingException {
		try {
			for (String address : recvAddress) {
				if (StringUtils.hasLength(address)) {
					helper.addTo(address);
				}
			}
			if (ccAddress != null && ccAddress.size() > 0) {
				for (String cc : ccAddress) {
					if (StringUtils.hasLength(cc)) {
						helper.addCc(cc);
					}
				}
			}
			if (bccAddress != null && bccAddress.size() > 0) {
				for (String bcc : bccAddress) {
					if (StringUtils.hasLength(bcc)) {
						helper.addBcc(bcc);
					}
				}
			}
		} catch (MessagingException e) {
			logger.error("-- processAddress error --", e);
			throw e;
		}
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public JavaMailSender getMailSender() {
		return mailSender;
	}

	public String getDefaultFromAddress() {
		return defaultFromAddress;
	}

	public void setDefaultFromAddress(String defaultFromAddress) {
		this.defaultFromAddress = defaultFromAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<String> getRecieveAddressList() {
		return recieveAddressList;
	}

	public void setRecieveAddressList(List<String> recieveAddressList) {
		this.recieveAddressList = recieveAddressList;
	}

	public List<String> getCcAddressList() {
		return ccAddressList;
	}

	public void setCcAddressList(List<String> ccAddressList) {
		this.ccAddressList = ccAddressList;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplaySender(String displaySender) {
		this.displaySender = displaySender;
	}

	public String getDisplaySender() {
		return displaySender;
	}

}
