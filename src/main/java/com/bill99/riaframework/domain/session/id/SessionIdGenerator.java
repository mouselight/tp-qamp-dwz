package com.bill99.riaframework.domain.session.id;

/**
 * session id 生成接口
 * 
 */
public interface SessionIdGenerator {
	public String get();
}
