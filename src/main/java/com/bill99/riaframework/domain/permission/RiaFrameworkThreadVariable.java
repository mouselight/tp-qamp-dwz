package com.bill99.riaframework.domain.permission;

import com.bill99.riaframework.orm.entity.User;

/**
 * 线程变量
 */
public class RiaFrameworkThreadVariable {
	/**
	 * 当前用户线程变量
	 */
	private static ThreadLocal<User> mmsUserVariable = new ThreadLocal<User>();

	/**
	 * 获得当前用户
	 * 
	 * @return
	 */
	public static User getUser() {
		return mmsUserVariable.get();
	}

	/**
	 * 设置当前用户
	 * 
	 * @param user
	 */
	public static void setUser(User user) {
		mmsUserVariable.set(user);
	}

	/**
	 * 移除当前用户
	 */
	public static void removeUser() {
		mmsUserVariable.remove();
	}

}
