package com.bill99.riaframework.common.command;

import java.io.Serializable;

public class PageCmd implements Serializable {

	private static final long serialVersionUID = -7631602377764105475L;

	private Integer pageNum;
	private Integer numPerPage;

	public Integer getPageNum() {
		if (pageNum == null) {
			pageNum = 1;
		}
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getNumPerPage() {
		if (null == numPerPage) {
			numPerPage = 20;
		}
		return numPerPage;
	}

	public void setNumPerPage(Integer numPerPage) {
		this.numPerPage = numPerPage;
	}

}
