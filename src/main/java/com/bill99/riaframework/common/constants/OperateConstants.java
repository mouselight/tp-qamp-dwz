/**
 * OperateConstants.java Created by William.Wang in 2012-9-26.
 * Copyright 99Bill Corporation. All rights reserved.
 */
package com.bill99.riaframework.common.constants;

/**
 * @Description 框架操作常量类
 * @CreateDate 2012-9-26
 * @Author William
 * @Version 1.0 
 */

public class OperateConstants {

	/**
	 * statusCode
	 */
	//成功代码
	public static final String STATUS_CODE_SUCCESS = "200";

	//失败代码
	public static final String STATUS_CODE_FAILED = "300";

	//会话超时
	public static final String STATUS_CODE_SESSION_TIMEOUT = "301";

	/**
	 * callbackType
	 */
	//关闭当前tab
	public static final String CALLBACK_TYPE_CLOSECURRENT = "closeCurrent";

	//跳转到指定URL
	public static final String CALLBACK_TYPE_FORWARD = "forward";

	/**
	 * message
	 */
	public static final String MSG_NO_USER = "用户不存在!";
	
	public static final String MSG_NO_OBJECT = "对象不存在!";
	
	public static final String MSG_NO_SELECTED = "没有选择任何对象!";
	
	
	public static final String MSG_OPT_SUCCESS = "操作成功!";
	
	public static final String MSG_OPT_FAILED = "操作失败!";

	
	public static final String MSG_ADD_SUCCESS = "新增成功!";
	
	public static final String MSG_ADD_FAILED = "新增失败!";

	
	public static final String MSG_EDIT_SUCCESS = "修改成功!";
	
	public static final String MSG_EDIT_FAILED = "修改失败!";
	

	public static final String MSG_DEL_SUCCESS = "删除成功!";
	
	public static final String MSG_DEL_FAILED = "删除失败!";

}
