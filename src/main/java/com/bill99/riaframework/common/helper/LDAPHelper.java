package com.bill99.riaframework.common.helper;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

public class LDAPHelper {

	private static Bill99Logger logger = Bill99Logger.getLogger(LDAPHelper.class);
	private final static String LDAP_URL = "LDAP://shpdc.99bill.com:389/DC=99bill,DC=com";

	/**
	 * 验证域用户
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public static boolean validateLdap(String userName, String password) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		InitialContext iCnt = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", LDAP_URL);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userName + "@99bill.com");
			envi.put("java.naming.security.credentials", password);
			iCnt = new InitialContext(envi);
			return iCnt == null ? false : true;
		} catch (Exception e) {
			logger.info(userName + "@域密码错误:" + e.getMessage());
			return false;
		} finally {
			try {
				if (iCnt != null) {
					iCnt.close();
				}
			} catch (NamingException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * 获取域用户名称
	 * 
	 * @param userCode
	 * @param password
	 * @return userName
	 */
	public static String getDomainUserName(String userCode, String password) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		LdapContext ctx = null;
		String userName = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", LDAP_URL);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userCode + "@99bill.com");
			envi.put("java.naming.security.credentials", password);

			if (ctx == null) {
				return userName;
			}
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String searchFilter = "mail=" + userCode;
			String returnedAtts[] = { "" }; // 定制返回属性   
			searchCtls.setReturningAttributes(returnedAtts); // 设置返回属性集
			NamingEnumeration answer = ctx.search("", searchFilter, searchCtls);
			if (answer == null) {
				return userName;
			}
			if (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String[] srNameInfo = sr.getName().split(",")[0].split("\\|")[0].split("=");
				userName = srNameInfo[srNameInfo.length - 1];
				logger.info("login userName:" + userName);
			}
		} catch (Exception e) {
			logger.info(userName + "@获取域用户名称错误:" + e.getMessage());
			return null;
		} finally {
			try {
				if (ctx != null) {
					ctx.close();
				}
			} catch (NamingException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return userName;
	}

	/**
	 * 验证域用户是否存在,目前得不到用户密码不起作用
	 * 需要一个已有的帐号和密码来验证
	 * @param userName
	 * @return
	 */
	public static boolean validateLdap(String userName) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		InitialContext iCnt = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", LDAP_URL);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userName + "@99bill.com");
			iCnt = new InitialContext(envi);
			return iCnt == null ? false : true;
		} catch (Exception e) {
			logger.info(userName + "@域用户不存在:" + e.getMessage());
			return false;
		} finally {
			try {
				if (iCnt != null) {
					iCnt.close();
				}
			} catch (NamingException e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	//	public static void main(String[] args) {
	//		TestLDAPService t = new TestLDAPService();
	//		String userName = "leo.feng@99bill.com";
	//		String password = "admin";
	//		System.out.println(t.getDomainUserName(userName, password));
	//		System.out.println(validateLdap(userName, password));
	//		System.out.println(validateLdap(userName));
	//
	//		userName = "leo.feng@local";
	//		System.out.println(userName.replace("@local", ""));
	//
	//		System.out.println(userName.contains("@local"));
	//
	//		System.out.println(userName);
	//	}

}
