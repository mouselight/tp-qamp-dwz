package com.bill99.riaframework.common.utils;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

/**
 * 日期格式转换类
 * 
 * @author leo.feng
 */
public class DateUtil {

	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

	private static SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

	public static Date getTimeNow() {
		return Calendar.getInstance().getTime();
	}

	/**
	 * 将日期转换为纪念日
	 * 
	 * @param date
	 * @return
	 */
	public static String getAnniversary(Date date) {
		if (date == null) {
			return "";
		}
		String[] dateStr = DATE_FORMAT3.format(date).split("-");
		String anniver = dateStr[1] + "月" + dateStr[2] + "日";
		return anniver;
	}

	/**
	 * 根据日期返回距今的年数
	 * 
	 * @param date
	 * @return
	 */
	public static int getSkipYear(Date date) {
		if (date == null) {
			return 0;
		}
		String[] dateStr = DATE_FORMAT3.format(date).split("-");
		int year = Integer.valueOf(dateStr[0]);
		return Math.abs(getYear() - year);
	}

	/**
	 * 将日期型转换成字符型.
	 * 
	 * @param date
	 *            日期型
	 * @return 字符型
	 */
	public static String getStringFromDate(Date date) {
		if (date == null) {
			return "";
		}
		//yyyy-MM-dd HH:mm:ss
		return dateFormat.format(date);
	}
	
	/**
	 * 将日期型转换成字符型.
	 * 
	 * @param date
	 *            日期型
	 * @return 字符型
	 */
	public static String parseFromDate(Date date) {
		if (date == null) {
			return "";
		}
		//yyyy-MM-dd
		return DATE_FORMAT3.format(date);
	}

	/**
	 * 将字符型转换成日期型.
	 * 
	 * @param strDate
	 *            字符型
	 * @return 日期型
	 */
	public static Date getDateFromString(String strDate) {

		try {
			if (strDate.equals("")) {
				return null;
			}
			return dateFormat.parse(strDate);
		} catch (ParseException e) {
			return null;
		}
	}

	public static final String DATE_PATTERN1 = "yyyy/MM/dd";

	public static final String DATE_PATTERN2 = "yyyyMMdd";

	public static final String DATE_PATTERN3 = "yyyy-MM-dd";

	public static final String DATE_PATTERN4 = "MM-dd";

	public final static SimpleDateFormat DATE_FORMAT1 = new SimpleDateFormat(DATE_PATTERN1);
	static {
		DATE_FORMAT1.setLenient(false);
	}

	public final static SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat(DATE_PATTERN2);
	static {
		DATE_FORMAT2.setLenient(false);
	}

	public final static SimpleDateFormat DATE_FORMAT3 = new SimpleDateFormat(DATE_PATTERN3);
	static {
		DATE_FORMAT3.setLenient(false);
	}

	private static SimpleDateFormat DATE_FORMAT4 = new SimpleDateFormat(DATE_PATTERN4);
	static {
		DATE_FORMAT4.setLenient(false);
	}

	public static boolean isDate(String str) {
		try {
			DATE_FORMAT1.parse(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isDate1(String str) {
		try {
			DATE_FORMAT2.parse(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Date toDate(String str) {
		if (str == null || str.equals("")) {
			return null;
		}
		try {
			if (isDate(str))
				return DATE_FORMAT1.parse(str);
			else if (isDate1(str))
				return DATE_FORMAT2.parse(str);
			else
				return DATE_FORMAT3.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}

	/*
	 * 获取当前时间
	 */
	public static String getTime() {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sj_str = dateFormat1.format(new Date());

		return sj_str;
	}

	/*
	 * 获取当前日期
	 */
	public static String getDate() {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		String sj_str = dateFormat1.format(new Date());

		return sj_str;
	}

	/*
	 * 获取当前日期
	 */
	public static String getDate(String formatstr) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat(formatstr);
		String sj_str = dateFormat1.format(new Date());

		return sj_str;
	}

	/*
	 * 格式化时间
	 */
	public static String formatTime(String timeStr) {
		if (null == timeStr) {
			return null;
		}

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = parseTime(timeStr);
		String sj_str = dateFormat1.format(date);

		return sj_str;
	}

	/*
	 * 格式化日期
	 */
	public static String formatDate(String dateStr) {
		if (null == dateStr) {
			return null;
		}
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = parseDate(dateStr);
		String sj_str = dateFormat1.format(date);

		return sj_str;
	}

	/*
	 * 格式化日期
	 */
	public static String formatDate(String pattern, String dateStr) {
		if (null == dateStr) {
			return null;
		}
		SimpleDateFormat dateFormat1 = new SimpleDateFormat(pattern);
		Date date = parseDate(dateStr);
		String sj_str = dateFormat1.format(date);

		return sj_str;
	}

	/*
	 * 解析日期字符串
	 */
	public static Date parseDate(String dateStr) {
		if (null == dateStr) {
			return null;
		}
		Date date = null;

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

		try {
			date = dateFormat1.parse(dateStr);
		} catch (ParseException e) {
		}

		return date;
	}

	/**
	 * 将字符型转换成日期型.
	 * 
	 * @param strDate
	 *            字符型
	 * @return 日期型
	 */
	private final static String[] datePatternStrs = new String[] { "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", "yyyy-MM-dd", "yyyy/MM/dd", "yyyy/MM/dd HH/mm/ss", "yyyyMMdd" };

	public static Date getDateFromStrings(String strDate) {
		if (!StringUtils.hasLength(strDate)) {
			return null;
		}
		Date date = null;
		for (String datePattern : datePatternStrs) {
			try {
				date = new SimpleDateFormat(datePattern).parse(strDate);
			} catch (ParseException e) {
				date = null;
			}
			if (date != null) {
				return date;
			}
		}
		return null;
	}

	/*
	 * 解析时间字符串
	 */
	public static Date parseTime(String timeStr) {
		if (null == timeStr) {
			return null;
		}
		Date date = null;
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date = dateFormat1.parse(timeStr);
		} catch (ParseException e) {
		}
		return date;
	}

	/*
	 * 时间偏移运算
	 */
	public static String getTime(int skipDay) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());

		cal.add(Calendar.DAY_OF_MONTH, skipDay);
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return dateFormat1.format(cal.getTime());
	}

	/*
	 * 某一时间的偏移运算
	 */
	public static String getTime(String timeStr, int skipDay) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(parseTime(timeStr));

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return dateFormat.format(cal.getTime());
	}

	/*
	 * 日期偏移运算(增、减几日）
	 */
	public static String getDate(int skipDay) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return dateFormat.format(cal.getTime());
	}

	/*
	 * 日期偏移运算(增、减几日）
	 */
	public static String getDate(String dateStr, int skipDay) {
		if (null == dateStr) {
			return null;
		}

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(parseDate(dateStr));

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return dateFormat.format(cal.getTime());
	}

	/*
	 * 时间偏移运算(增、减几日、几小时、几分）
	 */
	public static String getTime(int skipDay, int skipHour, int skipMinute) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		cal.add(GregorianCalendar.HOUR_OF_DAY, skipHour);
		cal.add(GregorianCalendar.MINUTE, skipMinute);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return dateFormat.format(cal.getTime());
	}

	/*
	 * 某一时间的偏移运算(增、减几日、几小时、几分）
	 */
	public static String getTime(String timeStr, int skipDay, int skipHour, int skipMinute) {
		if (null == timeStr) {
			return null;
		}

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(parseTime(timeStr));

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		cal.add(GregorianCalendar.HOUR_OF_DAY, skipHour);
		cal.add(GregorianCalendar.MINUTE, skipMinute);
		cal.get(GregorianCalendar.DAY_OF_WEEK_IN_MONTH);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return dateFormat.format(cal.getTime());
	}

	/*
	 * 某一时间的偏移运算(增、减几日、几小时、几分）
	 */
	@SuppressWarnings("unused")
	public static Date getDateTime(Date time, int skipDay, int skipHour, int skipMinute) {
		if (null == time) {
			return null;
		}

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(time);

		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		cal.add(GregorianCalendar.HOUR_OF_DAY, skipHour);
		cal.add(GregorianCalendar.MINUTE, skipMinute);
		cal.get(GregorianCalendar.DAY_OF_WEEK_IN_MONTH);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return cal.getTime();
	}

	/*
	 * 计算日期相差几天
	 */
	public static long subtraction(Date minuend, Date subtrahend) {

		long daterange = minuend.getTime() - subtrahend.getTime();
		long time = 1000 * 3600 * 24;

		return (daterange % time == 0) ? (daterange / time) : (daterange / time + 1);
	}

	/*
	 * 时间偏移（相差几天） 返回字符串表示的 月份-日期
	 */
	public static String getSkip(int skipDay) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		return DATE_FORMAT4.format(cal.getTime());
	}

	public static long getM(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal.get(GregorianCalendar.DAY_OF_WEEK);
	}

	public static String getLastDate(String temp) { // 变量temp是看几天前的天数
		if (temp == null || temp.equals("")) {
			temp = "1";
		}
		int i = Integer.parseInt(temp);
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
		Calendar grc = Calendar.getInstance();
		grc.add(GregorianCalendar.DATE, -i);
		return dateFormat.format(grc.getTime());
	}

	// 获取上一年的日期（用来设置查询日期条件）
	public static String getLastYearDate() { // 上一年

		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		String year = String.valueOf(y - 1);
		return year;
	}

	// 获取上个月的日期（用来设置查询日期条件）
	public static String getLastMonthDate() { // 上一月
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH) + 1;
		String month = null;
		String year = String.valueOf(y);
		if (m > 1) {
			if (m > 10) {
				month = String.valueOf(m - 1);
			} else {
				month = "0" + String.valueOf(m - 1);
			}
		} else {
			year = String.valueOf(y - 1);
			month = String.valueOf(12);
		}

		return year + "-" + month;
	}

	// 获取前一天的日期（用来设置查询日期条件）
	public static String getLastDayDate() { // 前一天
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH) + 1;
		int d = c.get(Calendar.DAY_OF_MONTH);
		int days = 0;
		if (m > 1) {
			days = getMonthsDays(m - 1, y);
		} else {
			days = 31;
		}
		String day = null;
		String month = null;
		String year = String.valueOf(y);
		if (d > 1) { // 大于1号
			day = String.valueOf(d - 1);
			if (m > 9) {
				month = String.valueOf(m);
			} else {
				month = "0" + String.valueOf(m);
			}
		} else if ((d < 2) && (m < 2)) { // 一月一号
			day = String.valueOf(31);
			month = String.valueOf(12);
			year = String.valueOf(y - 1);
		} else if ((d < 2) && (m > 2)) {
			day = String.valueOf(days);
			if (m > 10) {
				month = String.valueOf(m - 1);
			} else {
				month = "0" + String.valueOf(m - 1);
			}
		}

		return year + "-" + month + "-" + day;
	}

	// 判断是否闰年
	public static boolean isLeapYear(int year) {
		if ((((year % 4) == 0) && ((year % 100) != 0)) || ((year % 4) == 0) && ((year % 400) == 0)) {
			return true;
		} else {
			return false;
		}
	}

	// 获取每个月的天数
	public static int getMonthsDays(int month, int year) {
		if ((isLeapYear(year) == true) && (month == 2)) {
			return 29;
		} else if ((isLeapYear(year) == false) && (month == 2)) {
			return 28;
		}

		if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12)) {
			return 31;
		}
		return 30;
	}

	@SuppressWarnings("unused")
	public static String getWeekDay() {
		DateFormatSymbols symboles = new DateFormatSymbols(Locale.getDefault());
		symboles.setShortWeekdays(new String[] { "", "7", "1", "2", "3", "4", "5", "6" });
		SimpleDateFormat date = new SimpleDateFormat("E", symboles);
		Date day = new Date();
		return date.format(new Date());
	}

	// 获取年
	public static int getYear() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.YEAR);
	}

	// 获取月
	public static int getMonth() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MONTH);
	}

	// 获取日
	public static int getDay() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_MONTH);
	}

	public static String getLastMonthDay(int lastmonths) {
		int month = getMonth() + 1;
		if (month - lastmonths > 0) {
			return String.valueOf(getYear()) + "-" + String.valueOf(month - lastmonths) + "-1";
		} else {
			return String.valueOf(getYear() - 1) + "-" + String.valueOf(12 + month - lastmonths) + "-1";
		}
	}

	/*
	 * 日期偏移运算(增、减几日）
	 */
	public static Date getskipDate(int skipDay) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(GregorianCalendar.DAY_OF_MONTH, skipDay);
		return cal.getTime();
	}

	public static boolean checkDate(String date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			dateFormat.parse(date);
		} catch (Exception e) {
			return false;
		}
		String eL = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-9]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$";
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(date);
		return m.matches();
	}

	/*
	 * 获取当前日期
	 */
	public static String getNowDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(getTimeNow());
	}
}
