package com.bill99.riaframework.common.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;

/**
 * 提供一些系统中使用到的共用方法
 * 
 * 比如获得会员信息,获得后台站点信息
 */
public class RiaFrameworkUtils {

	public final static String PROCESS_URL = "processUrl";
	public final static String RETURN_URL = "returnUrl";
	public final static String MESSAGE = "message";

	public final static String LOGIN_INPUT = "/framwork/login.html";
	public final static String LOGIN_SUCCESS = "/framwork/index.html";
	/**
	 * 认证信息session key
	 */
	public final static String AUTH_KEY = "auth_key";

	/**
	 * 用户KEY
	 */
	public static final String USER_KEY = "_user_key";

	// 超级用户标志
	public static final String IS_ADMIN = "_isAdmin";

	/**
	 * 获得用户
	 * 
	 * @param request
	 * @return
	 */
	public static User getUser(HttpServletRequest request) {
		return (User) request.getAttribute(USER_KEY);
	}

	/**
	 * 获得用户ID
	 * 
	 * @param request
	 * @return
	 */
	public static Long getUserId(HttpServletRequest request) {
		User user = getUser(request);
		if (user != null) {
			return user.getId();
		}
		return null;
	}

	/**
	 * 获得用户ID
	 * 
	 * @param request
	 * @return
	 */
	public static String getUserName(HttpServletRequest request) {
		User user = getUser(request);
		if (user != null) {
			return user.getUsername();
		}
		return null;
	}

	/**
	 * 设置用户
	 * 
	 * @param request
	 * @param user
	 */
	public static void setUser(HttpServletRequest request, User user) {
		request.setAttribute(USER_KEY, user);
	}

	/**
	 * 设置用户到session
	 * 
	 * @param httpSession
	 * @param user
	 */
	public static void setUseToSession(HttpSession httpSession, User user) {
		httpSession.setAttribute(USER_KEY, user);
	}

	/**
	 * 从session获取用户
	 * 
	 * @param httpSession
	 * @param user
	 */
	public static User getUseFromSession(HttpSession httpSession) {
		return (User) httpSession.getAttribute(USER_KEY);
	}

	/**
	 * 从session获取用例执行状态
	 * 
	 * @param httpSession
	 * @param user
	 */
	public static Integer getTcExeTypeFromSession(HttpSession httpSession) {
		String userTcKey = getUseFromSession(httpSession).getId().toString() + TreeUtils.TC_FILTER_EXETYPE_KEY_SUFFIX;
		return (Integer) httpSession.getAttribute(userTcKey);
	}

	/**
	 * 从session获取用例执行状态
	 * 
	 * @param httpSession
	 * @param user
	 */
	public static void setTcExeTypeFromSession(HttpSession httpSession, Integer exeStatus) {
		String userTcKey = getUseFromSession(httpSession).getId().toString() + TreeUtils.TC_FILTER_EXETYPE_KEY_SUFFIX;
		httpSession.setAttribute(userTcKey, exeStatus);
	}

}
