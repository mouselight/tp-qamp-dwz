package com.bill99.riaframework.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

/**
 * 字符串处理类.
 * 
 * @author Administrator
 * 
 */
public class StringUtil {

	public static boolean isNull(String str) {
		if (null != str) {
			str = spaceTrim(str);
			return str.trim().length() == 0;
		}
		return true;
	}

	/**
	 * 去除字符串中的空格
	 * 
	 * @param str
	 * @return
	 */
	public static String spaceTrim(String str) {
		if (null == str) {
			return null;
		}
		String[] spaceUnicode = { "\u0020", "\u3000", "\240" };
		for (int i = 0; i < spaceUnicode.length; i++) {
			str = str.replaceAll(spaceUnicode[i], "");
		}
		return str;
	}

	/**
	 * null字符串转换为空串
	 * 
	 * @param str
	 * @return
	 */
	public static String null2Space(String str) {
		if (null == str || "null".equals(str.toLowerCase())) {
			return "";
		}
		return spaceTrim(str);
	}

	/**
	 * 逗号分隔字符串转换为数组
	 * @param inWord
	 * @return
	 */
	public static Long[] string2LongArray(String inWord) {
		Long[] longA = {};
		if (!StringUtils.hasLength(inWord)) {
			return longA;
		}
		String[] strA = inWord.split(",");
		longA = new Long[strA.length];
		for (int i = 0; i < strA.length; i++) {
			longA[i] = Long.valueOf(strA[i]);
		}
		return longA;
	}

	public static Integer[] string2IntegerArray(String inWord) {
		Integer[] longA = {};
		if (!StringUtils.hasLength(inWord)) {
			return longA;
		}
		String[] strA = inWord.split(",");
		longA = new Integer[strA.length];
		for (int i = 0; i < strA.length; i++) {
			longA[i] = Integer.valueOf(strA[i]);
		}
		return longA;
	}

	/**
	 * 删除String数组中的空值
	 * @param sourcelist
	 * @return
	 */
	public static List<String> clearStrList(List<String> sourcelist) {
		List<String> targetList = new ArrayList<String>();
		if (null == sourcelist || sourcelist.size() <= 0) {
			return targetList;
		}
		for (String source : sourcelist) {
			if (!StringUtil.isNull(source)) {
				targetList.add(source);
			}
		}
		return targetList;
	}

	/**
	 * 删除Long数组中的空值
	 * @param sourcelist
	 * @return
	 */
	public static List<Long> clearLongList(List<Long> sourcelist) {
		List<Long> targetList = new ArrayList<Long>();
		if (null == sourcelist || sourcelist.size() <= 0) {
			return targetList;
		}
		for (Long source : sourcelist) {
			if (null != source) {
				targetList.add(source);
			}
		}
		return targetList;
	}

	/**
	 * 金额范围转为数组 ，若传入null则返回null
	 * 传入的金额字符串用逗号分隔，如"0,100" 。前者表示起始金额，后者表示结束金额(没有结束金额时第二位为Long.MAX_VALUE)
	 * 最终返回定长2位的数组， 第一位为起始金额 第二位为结束金额
	 * @param amountScope 金额范围 
	 * @return 数组
	 */
	public static List<Long> getAmountScope(String amountScope) {
		if (StringUtil.isNull(amountScope)) {
			return null;
		}
		List<Long> scopeList = new ArrayList<Long>();
		String scope[] = amountScope.split(",");
		for (String s : scope) {
			scopeList.add(Long.valueOf(s));
		}
		if (scopeList.size() == 1) {
			scopeList.add(Long.MAX_VALUE);
		}
		return scopeList;
	}

	/**
	 * List转换为Long型数组
	 * @param list
	 * @return
	 */
	public static Long[] listToArray(List list) {
		if (null == list || list.size() <= 0) {
			return null;
		}

		Long[] params = new Long[list.size()];
		for (int i = 0; i < list.size(); i++) {
			params[i] = Long.valueOf(list.get(i).toString());
		}
		return params;
	}

	/**
	 * 字符串转换为Long数组
	 * @param str
	 * @return
	 */
	public static List<Long> strToLongList(String str, String splitSymbol) {
		if ((null == str) || (str.equals(""))) {
			return null;
		}
		List result = new ArrayList();
		String[] strArr = str.split(splitSymbol);
		for (int i = 0; i < strArr.length; ++i) {
			result.add(Long.valueOf(strArr[i]));
		}
		return result;
	}

	/**
	 * 字符串转换为Long数组
	 * @param str
	 * @return
	 */
	public static List<Long> strToLongList(String str) {
		if ((null == str) || (str.equals(""))) {
			return null;
		}
		List result = new ArrayList();
		String[] strArr = str.split(",");
		for (int i = 0; i < strArr.length; ++i) {
			result.add(Long.valueOf(strArr[i]));
		}
		return result;
	}

	/**
	 * 字符串转换为Long数组
	 * @param str
	 * @return
	 */
	public static List<Long> strToListObj(String str) {
		if ((null == str) || (str.equals(""))) {
			return new ArrayList<Long>();
		}
		List result = new ArrayList();
		String[] strArr = str.split(",");
		for (int i = 0; i < strArr.length; ++i) {
			result.add(Long.valueOf(strArr[i]));
		}
		return result;
	}

	/**
	 * 使用;拼接content
	 * @param oldstr
	 * @param newstr
	 * @return
	 */
	public static String appendContent(String oldstr, String newstr) {
		if (!isNull(oldstr) && !isNull(newstr)) {
			return oldstr + ";" + newstr;
		}
		return isNull(oldstr) ? newstr : oldstr;
	}

	/**
	 * 合并list
	 * @param <T>
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static <T> List<T> appendList(List<T> list1, List<T> list2) {
		if (null != list1 && list1.size() > 0 && null != list2 && list2.size() > 0) {
			list1.addAll(list2);
			return list1;
		}
		return (null != list1 && list1.size() > 0) ? list1 : ((null != list2 && list2.size() > 0) ? list2 : new ArrayList<T>());
	}

}
