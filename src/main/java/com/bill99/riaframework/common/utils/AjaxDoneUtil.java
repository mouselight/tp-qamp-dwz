package com.bill99.riaframework.common.utils;

import java.io.Serializable;

import net.sf.json.JSONObject;

public class AjaxDoneUtil implements Serializable {

	private static final long serialVersionUID = -7011148707831508731L;

	private String forwardUrl;
	private String callbackType;
	private String rel;
	private String navTabId;
	private String message;
	private String statusCode;

	public String getJsonAjaxDone(AjaxDoneUtil ajaxDone) {
		return JSONObject.fromObject(ajaxDone).toString();
	}

	public AjaxDoneUtil() {

	}

	public AjaxDoneUtil(String statusCode, String message, String navTabId, String rel, String callbackType, String forwardUrl) {
		this.forwardUrl = forwardUrl;
		this.callbackType = callbackType;
		this.rel = rel;
		this.navTabId = navTabId;
		this.message = message;
		this.statusCode = statusCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNavTabId() {
		return navTabId;
	}

	public void setNavTabId(String navTabId) {
		this.navTabId = navTabId;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getCallbackType() {
		return callbackType;
	}

	public void setCallbackType(String callbackType) {
		this.callbackType = callbackType;
	}

	public String getForwardUrl() {
		return forwardUrl;
	}

	public void setForwardUrl(String forwardUrl) {
		this.forwardUrl = forwardUrl;
	}

}
