/**
 * StringConvertor.java Created by William.Wang in 2012-2-16.
 * Copyright 2004-2010 99Bill Corporation. All rights reserved.
 */
package com.bill99.riaframework.common.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description  *****
 * @CreateDate  2012-2-16
 * @Author  William.Wang
 * @Version  1.0
 */
public class StringConvertor {
	
	/**
	 * 有格式字符串转List
	 * @param str： 数值1,数值2,数值3,....
	 * @return List&lt;Long&gt;
	 */
	public static List<Long> stringTranferList(String str) {
		List<Long> longList = new ArrayList<Long>();
		if (null != str) {
			String[] idArr = str.split(",");
			for (int i = 0; i < idArr.length; i++) {
				longList.add(Long.valueOf(idArr[i]));
			}
		}
		return longList;
	}
}
