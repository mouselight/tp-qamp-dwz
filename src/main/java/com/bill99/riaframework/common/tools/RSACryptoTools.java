/*package com.bill99.riaframework.common.tools;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import s3.util.RSACrypto;

public class RSACryptoTools {

	private static RSACrypto rsa;

	static {
		KeyPairGenerator keyPairGen;
		try {
			keyPairGen = KeyPairGenerator.getInstance("RSA");
			keyPairGen.initialize(1024, new SecureRandom());
			KeyPair keyPair = keyPairGen.generateKeyPair();
			rsa = new RSACrypto();
			rsa.setPublicKey(keyPair.getPublic().getEncoded());
			rsa.setPrivateKey(keyPair.getPrivate().getEncoded());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String encrypt(String context) {
		String encryptData = null;
		try {
			// 加密
			byte[] encryptBytes = rsa.encrypt(context.getBytes());
			encryptData = new String(encryptBytes);
		} catch (Exception e) {
			//			e.printStackTrace();
		}
		return encryptData;
	}

	public static String decrypt(String context) {
		String decryptData = null;
		try {
			// 解密
			byte[] decryptBytes = rsa.decrypt(context.getBytes());
			decryptData = new String(decryptBytes);
		} catch (Exception e) {
			//			e.printStackTrace();
		}
		return decryptData;
	}
}
*/