/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-28		    leofung	   		leofung
 */

package com.bill99.riaframework.common.dto;

import java.io.Serializable;

/**
 * @author leofung
 * 
 */
public class ModuleResourceQueryDto implements Serializable {

	private static final long serialVersionUID = 5768515304812093888L;

	private Long idModule;
	private Long idModuleResource;
	private Long parentId;
	private String name;
	private String uri;
	private String moduleType;
	private Integer optType;
	private Boolean status;
	private Integer type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public Integer getOptType() {
		return optType;
	}

	public void setOptType(Integer optType) {
		this.optType = optType;
	}

	public Long getIdModuleResource() {
		return idModuleResource;
	}

	public void setIdModuleResource(Long idModuleResource) {
		this.idModuleResource = idModuleResource;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

}
