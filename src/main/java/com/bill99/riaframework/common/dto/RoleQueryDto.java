/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-9-4		    leofung	   		leofung
*/

package com.bill99.riaframework.common.dto;

import java.io.Serializable;

/**
 * @author leofung
 *
 */
public class RoleQueryDto implements Serializable {

	private static final long serialVersionUID = 8727591734078201367L;

	private String name;
	private Boolean status;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
}
