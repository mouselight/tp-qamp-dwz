package com.bill99.riaframework.common.dto;

import java.io.Serializable;
import java.util.Date;

public class UserExtDto implements Serializable {

	private static final long serialVersionUID = -1532578526200322008L;

	public static final Integer MALE = 0;
	public static final Integer FEMALE = 1;

	private Long idUser;
	private String realname;
	private Boolean gender;
	private Date birthday;
	private Date weddingday;
	private String intro;
	private String comefrom;
	private String qq;
	private String msn;
	private String phone;
	private String moble;
	private String createUser;
	private Date createDate;
	private String updateUser;
	private Date updateDate;

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getWeddingday() {
		return weddingday;
	}

	public void setWeddingday(Date weddingday) {
		this.weddingday = weddingday;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMoble() {
		return moble;
	}

	public void setMoble(String moble) {
		this.moble = moble;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getIntro() {
		return intro;
	}

	public void setComefrom(String comefrom) {
		this.comefrom = comefrom;
	}

	public String getComefrom() {
		return comefrom;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Long getIdUser() {
		return idUser;
	}

}
