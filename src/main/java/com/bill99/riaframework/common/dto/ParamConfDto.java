/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-9		    leo	   		leo
*/

package com.bill99.riaframework.common.dto;

import java.io.Serializable;

/**
 * @author leo
 *
 */
public class ParamConfDto implements Serializable {

	private static final long serialVersionUID = 3896699744735181531L;

	private String classCode;
	private String name;

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
