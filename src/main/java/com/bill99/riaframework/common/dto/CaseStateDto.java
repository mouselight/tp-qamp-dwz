package com.bill99.riaframework.common.dto;

import java.io.Serializable;
import java.util.Date;

public class CaseStateDto implements Serializable {

	private static final long serialVersionUID = 3866628593210561632L;
	//TC执行统计
	private Integer allCnt;
	private Integer doneCnt;
	private Integer undoneCnt;
	private Integer succCnt;
	private Integer failCnt;

	private String taTaskId;
	private Long planStepId;

	private Date updateDate;

	public Integer getAllCnt() {
		return allCnt;
	}

	public void setAllCnt(Integer allCnt) {
		this.allCnt = allCnt;
	}

	public Integer getDoneCnt() {
		return doneCnt;
	}

	public void setDoneCnt(Integer doneCnt) {
		this.doneCnt = doneCnt;
	}

	public Integer getUndoneCnt() {
		return undoneCnt;
	}

	public void setUndoneCnt(Integer undoneCnt) {
		this.undoneCnt = undoneCnt;
	}

	public Integer getSuccCnt() {
		return succCnt;
	}

	public void setSuccCnt(Integer succCnt) {
		this.succCnt = succCnt;
	}

	public Integer getFailCnt() {
		return failCnt;
	}

	public void setFailCnt(Integer failCnt) {
		this.failCnt = failCnt;
	}

	public String getTaTaskId() {
		return taTaskId;
	}

	public void setTaTaskId(String taTaskId) {
		this.taTaskId = taTaskId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
