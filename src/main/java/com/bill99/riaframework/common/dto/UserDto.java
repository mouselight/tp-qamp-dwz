package com.bill99.riaframework.common.dto;

import java.io.Serializable;
import java.util.Date;

import com.bill99.riaframework.orm.entity.UserExt;

public class UserDto implements Serializable {

	private static final long serialVersionUID = -7859905656087043502L;

	public static final Integer ENABLE = 0;
	public static final Integer DISABLE = 1;

	private Long id;
	private Long[] roleIds;
	private String username;
	private String password;
	private String email;
	private String registerIp;
	private Date lastLoginTime;
	private String lastLoginIp;
	private Integer loginCount;
	private Boolean isDisabled;
	private Date registerTime;
	private String resetKey;
	private String resetPwd;
	private String entryMode;

	private UserExtDto userExtDto;
	private UserExt userExt;

	private Long[] projectIds;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public Boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public String getResetPwd() {
		return resetPwd;
	}

	public void setResetPwd(String resetPwd) {
		this.resetPwd = resetPwd;
	}

	public UserExtDto getUserExtDto() {
		return userExtDto;
	}

	public void setUserExtDto(UserExtDto userExtDto) {
		this.userExtDto = userExtDto;
	}

	public UserExt getUserExt() {
		return userExt;
	}

	public void setUserExt(UserExt userExt) {
		this.userExt = userExt;
	}

	public static Integer getEnable() {
		return ENABLE;
	}

	public static Integer getDisable() {
		return DISABLE;
	}

	/**
	 * @param entryMode
	 *            the entryMode to set
	 */
	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}

	/**
	 * @return the entryMode
	 */
	public String getEntryMode() {
		return entryMode;
	}

	public Long[] getProjectIds() {
		return projectIds;
	}

	public void setProjectIds(Long[] projectIds) {
		this.projectIds = projectIds;
	}

}
