package com.bill99.riaframework.common.dto;

import java.io.Serializable;

public class CycleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8664956835006478357L;
	private String id;
	private String cycleName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

}
