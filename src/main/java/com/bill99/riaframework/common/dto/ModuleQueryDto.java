/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-28		    leofung	   		leofung
*/

package com.bill99.riaframework.common.dto;

import java.io.Serializable;

/**
 * @author leofung
 *
 */
public class ModuleQueryDto implements Serializable {

	private static final long serialVersionUID = -2074455345048145159L;

	private Long idModule;
	private String name;
	private String moduleType;
	private Boolean status;

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
