/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
*/

package com.bill99.riaframework.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author leo
 *
 */
public class ParamConfDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idConfDetail;
	private String classCode;
	private String code;
	private String name;
	private Integer ordNum;
	private Boolean status;
	private String updateUser;
	private Date updateDate;
	private String createUser;
	private Date createDate;

	public Long getIdConfDetail() {
		return idConfDetail;
	}

	public void setIdConfDetail(Long idConfDetail) {
		this.idConfDetail = idConfDetail;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getOrdNum() {
		return ordNum;
	}

	public void setOrdNum(Integer ordNum) {
		this.ordNum = ordNum;
	}

}
