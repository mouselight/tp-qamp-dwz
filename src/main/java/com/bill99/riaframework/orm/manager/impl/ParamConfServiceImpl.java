/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.riaframework.orm.manager.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;

import com.bill99.riaframework.common.dto.ParamConfDto;
import com.bill99.riaframework.orm.dao.ParamConfDao;
import com.bill99.riaframework.orm.entity.ParamConf;
import com.bill99.riaframework.orm.manager.ParamConfService;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 * 
 */
public class ParamConfServiceImpl implements ParamConfService {

	private ParamConfDao paramConfDao;

	public void setParamConfDao(ParamConfDao paramConfDao) {
		this.paramConfDao = paramConfDao;
	}

	public Pagination getConfList(int pageNo, int pageSize) {
		return paramConfDao.getConfList(pageNo, pageSize);
	}

	public ParamConfDto getConfById(String classCode) {
		ParamConfDto paramConfDto = new ParamConfDto();
		ParamConf paramConf = paramConfDao.get(classCode);
		if (paramConf != null) {
			BeanUtils.copyProperties(paramConf, paramConfDto);
		}
		return paramConfDto;
	}

	public void update(ParamConfDto paramConfDto) {
		ParamConf paramConf = paramConfDao.get(paramConfDto.getClassCode());
		Assert.notNull(paramConf);
		BeanUtils.copyProperties(paramConfDto, paramConf, new String[] { "classCode" });
		paramConfDao.update(paramConf);
	}

}
