/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
*/

package com.bill99.riaframework.orm.manager.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.dto.ParamConfDetailDto;
import com.bill99.riaframework.orm.dao.ParamConfDetalDao;
import com.bill99.riaframework.orm.entity.ParamConfDetail;
import com.bill99.riaframework.orm.manager.ParamConfDetailService;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 *
 */
public class ParamConfDetailServiceImpl implements ParamConfDetailService {

	private ParamConfDetalDao paramConfDetalDao;

	public void setParamConfDetalDao(ParamConfDetalDao paramConfDetalDao) {
		this.paramConfDetalDao = paramConfDetalDao;
	}

	public Pagination getConfDetailList(String classCode, int pageNo, int pageSize) {
		return paramConfDetalDao.getConfDetailList(classCode, pageNo, pageSize);
	}

	public List<ParamConfDetail> getConfDetailList(String classCode) {
		return paramConfDetalDao.getConfDetailList(classCode);
	}

	public ParamConfDetailDto getConfDetailById(Long id) {
		ParamConfDetailDto paramConfDetailDto = new ParamConfDetailDto();
		ParamConfDetail paramConfDetail = paramConfDetalDao.get(id);
		if (paramConfDetail != null) {
			BeanUtils.copyProperties(paramConfDetail, paramConfDetailDto);
		}
		return paramConfDetailDto;
	}

	public void update(ParamConfDetailDto paramConfDetailDto) {
		ParamConfDetail paramConfDetail = paramConfDetalDao.get(paramConfDetailDto.getIdConfDetail());
		Assert.notNull(paramConfDetail);
		paramConfDetail.setName(paramConfDetailDto.getName());
		paramConfDetail.setCode(paramConfDetailDto.getCode());

		if (paramConfDetailDto.getStatus() != null) {
			paramConfDetail.setStatus(paramConfDetailDto.getStatus());
		}
		if (StringUtils.hasLength(paramConfDetailDto.getUpdateUser())) {
			paramConfDetail.setUpdateUser(paramConfDetailDto.getUpdateUser());
		}
		if (paramConfDetailDto.getUpdateDate() != null) {
			paramConfDetail.setUpdateDate(paramConfDetailDto.getUpdateDate());
		}

		paramConfDetalDao.update(paramConfDetail);
	}

	public ParamConfDetail save(ParamConfDetailDto paramConfDetailDto) {
		ParamConfDetail paramConfDetail = new ParamConfDetail();
		BeanUtils.copyProperties(paramConfDetailDto, paramConfDetail);
		return paramConfDetalDao.save(paramConfDetail);
	}

	public void deleteLogic(ParamConfDetailDto paramConfDetailDto) {
		ParamConfDetail paramConfDetail = paramConfDetalDao.get(paramConfDetailDto.getIdConfDetail());
		Assert.notNull(paramConfDetail);
		paramConfDetail.setStatus(false);
		paramConfDetail.setUpdateDate(paramConfDetailDto.getUpdateDate());
		paramConfDetail.setUpdateUser(paramConfDetailDto.getUpdateUser());
		paramConfDetalDao.update(paramConfDetail);
	}

	public boolean vldCode(int type, String classCode, String code) {
		ParamConfDetail paramConfDetail = null;

		if (type == 1) {
			paramConfDetail = paramConfDetalDao.getByCode(classCode, code);
		}
		if (type == 2) {
			paramConfDetail = paramConfDetalDao.getByName(classCode, code);
		}
		return paramConfDetail == null ? true : false;

	}

	public ParamConfDetail getByClassCodeCode(String classCode, String code) {
		return paramConfDetalDao.getByClassCodeCode(classCode, code);
	}

}
