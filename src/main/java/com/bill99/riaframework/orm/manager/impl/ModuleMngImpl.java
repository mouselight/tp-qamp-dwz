/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.manager.impl;

import java.util.List;

import com.bill99.riaframework.orm.dao.ModuleDao;
import com.bill99.riaframework.orm.entity.Module;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.riaframework.orm.manager.UserService;

/**
 * @author leofung
 * 
 */
public class ModuleMngImpl implements ModuleMng {

	private ModuleDao moduleDao;
	private UserService userService;

	public void setModuleDao(ModuleDao moduleDao) {
		this.moduleDao = moduleDao;
	}

	public Module save(Module module) {
		return moduleDao.save(module);
	}

	public void update(Module module) {
		moduleDao.update(module);
	}

	public Module getModule(Long idModule) {
		return moduleDao.get(idModule);
	}

	public List<Module> getChild(Long parentId) {
		return moduleDao.getChild(parentId);
	}

	public List<Module> getRoots() {
		return moduleDao.getRoots();
	}

	public List<Module> getVldRoots() {
		return moduleDao.getVldRoots();
	}

	public List<Module> getModulesByUser(Long idUser) {
		User user = userService.findById(idUser);
		if ((user.getIsAdmin() != null && user.getIsAdmin())) {
			return moduleDao.getVldModules();
		}
		return moduleDao.getModulesByUser(idUser);
	}

	public List<Module> getBannerModules(Long userId) {
		User user = userService.findById(userId);
		if ((user.getIsAdmin() != null && user.getIsAdmin())) {
			return moduleDao.getVldRoots();
		}
		return moduleDao.getBannerModules(userId);
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<Module> getVldModules() {
		return moduleDao.getVldModules();
	}

}
