/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.manager.impl;

import com.bill99.riaframework.orm.dao.OperateTraceDao;
import com.bill99.riaframework.orm.entity.OperateTrace;
import com.bill99.riaframework.orm.manager.OperateTraceMng;

/**
 * @author leofung
 *
 */
public class OperateTraceMngImpl implements OperateTraceMng {

	private OperateTraceDao operateTraceDao;

	public void setOperateTraceDao(OperateTraceDao operateTraceDao) {
		this.operateTraceDao = operateTraceDao;
	}

	public OperateTrace save(OperateTrace operateTrace) {
		return operateTraceDao.save(operateTrace);
	}

}
