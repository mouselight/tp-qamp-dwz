package com.bill99.riaframework.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.RoleQueryDto;
import com.bill99.riaframework.orm.entity.Role;
import com.jeecms.common.page.Pagination;

public interface RoleService {

	public List<Role> getList();

	/**
	 * 查询出用户对应的所有角色
	 * 
	 * @return Map key=角色id value=角色名称
	 */
	public Map<String, String> getRoleMap();

	public Pagination getRoleList(RoleQueryDto roleQueryDto, Integer pageNo, Integer pageSize);

	public Role findById(Long id);

	public List<Role> findByIds(Long[] ids);

	public Role save(Role bean);

	public void update(Role bean);

	public Role deleteById(Long id);

	public Role[] deleteByIds(Long[] ids);

	//获取有效的Role
	public List<Role> getValidRole();

	//根据用户ID获取已有的Role
	public Map<String, Long> getCheckedRole(Long idUser);

	//验证用户是否具有角色权限
	public boolean getCheckedRole(Long userId, String roleName);

}
