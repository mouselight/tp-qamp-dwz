package com.bill99.riaframework.orm.manager.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.domain.security.BadCredentialsException;
import com.bill99.riaframework.domain.security.UsernameNotFoundException;
import com.bill99.riaframework.domain.session.SessionProvider;
import com.bill99.riaframework.orm.dao.AuthenticationDao;
import com.bill99.riaframework.orm.entity.Authentication;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.AuthenticationService;
import com.bill99.riaframework.orm.manager.UserService;
import com.bill99.rmca.common.util.Bill99Logger;

public class AuthenticationServiceImpl implements AuthenticationService {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public Authentication porocessLogin(String username, String password, String ip, HttpServletRequest request, HttpServletResponse response, SessionProvider session)
			throws UsernameNotFoundException, BadCredentialsException {
		User user = userService.login(username, password, ip);
		Authentication authentication = new Authentication();
		authentication.setIdUser(user.getId());
		authentication.setUsername(user.getUsername());
		authentication.setEmail(user.getEmail());
		authentication.setLoginIp(ip);
		save(authentication);
		session.setAttribute(request, response, RiaFrameworkUtils.AUTH_KEY, authentication.getId());
		return authentication;
	}

	public Authentication retrieve(String authId) {
		long current = System.currentTimeMillis();
		// 是否刷新数据库
		if (refreshTime < current) {
			refreshTime = getNextRefreshTime(current, interval);
			int count = authenticationDao.deleteExpire(new Date(current - timeout));
			logger.info("refresh Authentication, delete count: {}" + count);
		}
		Authentication auth = findById(authId);
		if (auth != null && auth.getUpdateTime().getTime() + timeout > current) {
			auth.setUpdateTime(new Timestamp(current));
			return auth;
		}
		return null;
	}

	public Long retrieveUserIdFromSession(SessionProvider session, HttpServletRequest request) {
		String authId = (String) session.getAttribute(request, RiaFrameworkUtils.AUTH_KEY);
		if (!StringUtils.hasLength(authId)) {
			return null;
		}
		Authentication auth = retrieve(authId);
		if (auth == null) {
			return null;
		}
		return auth.getIdUser();
	}

	public void storeAuthIdToSession(SessionProvider sessionProvider, HttpServletRequest request, HttpServletResponse response, String authId) {
		sessionProvider.setAttribute(request, response, RiaFrameworkUtils.AUTH_KEY, authId);
	}

	public Authentication findById(String id) {
		return authenticationDao.get(id);
	}

	public Authentication save(Authentication bean) {
		bean.setId(UUID.randomUUID().toString().replace("-", ""));
		bean.init();
		authenticationDao.save(bean);
		return bean;
	}

	public Authentication deleteById(String id) {
		return authenticationDao.deleteById(id);
	}

	// 过期时间
	private int timeout = 8 * 60 * 60 * 1000; // 8小时

	// 间隔时间
	private int interval = 4 * 60 * 60 * 1000; // 4小时

	// 刷新时间。
	private long refreshTime = getNextRefreshTime(System.currentTimeMillis(), this.interval);

	private UserService userService;
	private AuthenticationDao authenticationDao;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setAuthenticationDao(AuthenticationDao authenticationDao) {
		this.authenticationDao = authenticationDao;
	}

	/**
	 * 设置认证过期时间。默认30分钟。
	 * 
	 * @param timeout
	 *            单位分钟
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout * 60 * 1000;
	}

	/**
	 * 设置刷新数据库时间。默认4小时。
	 * 
	 * @param interval
	 *            单位分钟
	 */
	public void setInterval(int interval) {
		this.interval = interval * 60 * 1000;
		this.refreshTime = getNextRefreshTime(System.currentTimeMillis(), this.interval);
	}

	/**
	 * 获得下一个刷新时间。
	 * 
	 * @param current
	 * @param interval1
	 * @return 随机间隔时间
	 */
	private long getNextRefreshTime(long current, int interval1) {
		return current + interval1;
		// 为了防止多个应用同时刷新，间隔时间=interval+RandomUtils.nextInt(interval/4);
		// return current + interval + RandomUtils.nextInt(interval / 4);
	}
}
