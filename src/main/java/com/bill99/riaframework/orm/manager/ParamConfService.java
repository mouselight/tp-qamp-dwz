/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.riaframework.orm.manager;

import com.bill99.riaframework.common.dto.ParamConfDto;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 * 
 */
public interface ParamConfService {

	/**
	 * 参数配置，大类ID
	 * */

	public static final String SYS_PART_OPT = "sys_part_opt"; // 资源类型
	public static final String JIRA_PROJECT_ROLE = "jira_project_role";
	public static final String JIRA_ISSUE_ROLE = "jira_issue_role";
	public static final String PROJECT_CHANGE_TYPE = "project_change_type";

	public Pagination getConfList(int pageNo, int pageSize);

	public ParamConfDto getConfById(String classCode);

	public void update(ParamConfDto paramConfDto);

}
