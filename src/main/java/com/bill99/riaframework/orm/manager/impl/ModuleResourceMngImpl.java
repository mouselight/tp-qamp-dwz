/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.manager.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.orm.dao.ModuleResourceDao;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.bill99.riaframework.orm.manager.ModuleResourceMng;
import com.jeecms.common.page.Pagination;

/**
 * @author leofung
 *
 */
public class ModuleResourceMngImpl implements ModuleResourceMng {

	private ModuleResourceDao moduleResourceDao;

	public void setModuleResourceDao(ModuleResourceDao moduleResourceDao) {
		this.moduleResourceDao = moduleResourceDao;
	}

	public Pagination findAllModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage) {
		return moduleResourceDao.findAllModuleResources(moduleResourceQueryDto, pageNum, numPerPage);
	}

	public Pagination findUnboundModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage) {
		return moduleResourceDao.findUnboundResources(moduleResourceQueryDto, pageNum, numPerPage);
	}
	
	public ModuleResource save(ModuleResource moduleResource) {
		return moduleResourceDao.save(moduleResource);
	}

	public void update(ModuleResource moduleResource) {
		moduleResourceDao.update(moduleResource);
	}

	public ModuleResource getModuleResource(Long idModuleResource) {
		return moduleResourceDao.get(idModuleResource);
	}

	public List<ModuleResource> getValidModuleResource() {
		return moduleResourceDao.getValidModuleResource();
	}

	public Map<String, Long> getCheckedModuleResource(Long idModule) {
		Map<String, Long> checkedModuleResourceMap = new HashMap<String, Long>();
		List<ModuleResource> checkedModuleResourceList = moduleResourceDao.getCheckedModuleResource(idModule);
		if (checkedModuleResourceList != null) {
			for (ModuleResource moduleResource : checkedModuleResourceList) {
				checkedModuleResourceMap.put(moduleResource.getIdModuleResource().toString(), moduleResource.getIdModuleResource());
			}
		}
		return checkedModuleResourceMap;
	}

	public ModuleResource findByUri(String uri) {
		return moduleResourceDao.findByUri(uri);
	}
}
