/*
 * File: Description: Copyright 2004-2012 99Bill Corporation. All rights reserved. 
 * Date Author Changes 2012-9-18 
 * leofung
 */

package com.bill99.riaframework.orm.manager.impl;

import com.bill99.riaframework.orm.dao.RoleLogDao;
import com.bill99.riaframework.orm.entity.RoleLog;
import com.bill99.riaframework.orm.manager.RoleLogMng;

/**
 * @author leofung
 * 
 */
public class RoleLogMngImpl implements RoleLogMng {

	private RoleLogDao roleLogDao;

	/**
	 * @param roleLogDao
	 *            the roleLogDao to set
	 */
	public void setRoleLogDao(RoleLogDao roleLogDao) {
		this.roleLogDao = roleLogDao;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bill99.riaframework.orm.manager.RoleLogMng#save(com.bill99.riaframework.orm.entity.RoleLog)
	 */
	public RoleLog save(RoleLog roleLog) {
		return roleLogDao.save(roleLog);
	}

}
