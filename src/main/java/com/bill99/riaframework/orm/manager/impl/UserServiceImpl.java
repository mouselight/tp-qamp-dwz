package com.bill99.riaframework.orm.manager.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.common.dto.UserExtDto;
import com.bill99.riaframework.common.helper.LDAPHelper;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.domain.security.BadCredentialsException;
import com.bill99.riaframework.domain.security.UsernameNotFoundException;
import com.bill99.riaframework.domain.security.encoder.PwdEncoder;
import com.bill99.riaframework.orm.dao.UserDao;
import com.bill99.riaframework.orm.entity.Role;
import com.bill99.riaframework.orm.entity.RoleLog;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.entity.UserExt;
import com.bill99.riaframework.orm.manager.RoleLogMng;
import com.bill99.riaframework.orm.manager.RoleService;
import com.bill99.riaframework.orm.manager.UserService;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;
import com.jeecms.common.page.Pagination;

public class UserServiceImpl implements UserService {

	private PwdEncoder pwdEncoder;
	private UserDao userDao;
	private RoleService roleService;
	private RoleLogMng roleLogMng;
	private TestProjectsMng testProjectsMng;

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}

	public void editOptertorPwd(String newPwd, Long userId) {
		User user = findById(userId);
		user.setPassword(newPwd);
		userDao.update(user);
	}

	public User login(String username, String password, String ip) throws UsernameNotFoundException, BadCredentialsException {

		if (!username.toLowerCase().contains("@local") && !username.toLowerCase().contains("guest")) {
			if (!LDAPHelper.validateLdap(username)) {
				throw new UsernameNotFoundException("域用户不存在!!");
			} else if (!LDAPHelper.validateLdap(username, password)) {
				throw new BadCredentialsException("域密码错误");
			}
		}
		String userName = username.replace("@local", "");
		User user = getByUsername(userName);
		if (user == null) {
			throw new UsernameNotFoundException("用户无此系统权限!!");
		}
		if (username.contains("@local") && !pwdEncoder.isPasswordValid(user.getPassword(), password)) {
			throw new BadCredentialsException("密码错误");
		}
		updateLoginInfo(user.getId(), ip);
		return user;
	}

	public void updateLoginInfo(Long userId, String ip) {
		User user = findById(userId);
		user.setLoginCount(user.getLoginCount() == null ? 0 : user.getLoginCount() + 1);
		user.setLastLoginIp(ip);
		user.setLastLoginTime(DateUtil.getTimeNow());
		userDao.update(user);
	}

	public boolean usernameExist(String username) {
		return getByUsername(username) != null;
	}

	public boolean emailExist(String email) {
		return userDao.countByEmail(email) > 0;
	}

	public User getByUsername(String username) {
		return userDao.getByUsername(username);
	}

	public List<User> getByEmail(String email) {
		return userDao.getByEmail(email);
	}

	public Pagination getPage(UserDto userDto, int pageNo, int pageSize) {
		Pagination page = userDao.getPage(userDto, pageNo, pageSize);
		return page;
	}

	public User findById(Long id) {
		return userDao.get(id);
	}

	/**
	 * @see UnifiedUserMng#update(Long, String, String)
	 */
	public User update(Long id, String password, String email) {
		User user = findById(id);
		if (!StringUtils.isBlank(email)) {
			user.setEmail(email);
		} else {
			user.setEmail(null);
		}
		if (!StringUtils.isBlank(password)) {
			user.setPassword(pwdEncoder.encodePassword(password));
		}
		userDao.update(user);
		return user;
	}

	public boolean isPasswordValid(Long id, String password) {
		User user = findById(id);
		return pwdEncoder.isPasswordValid(user.getPassword(), password);
	}

	public User deleteById(Long id) {
		User bean = userDao.get(id);
		if (bean != null) {
			bean.setIsDisabled(Boolean.TRUE);
			userDao.update(bean);
		}
		return bean;
	}

	public User[] deleteByIds(Long[] ids) {
		User[] beans = new User[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	public void update(UserDto userDto) {
		// PO
		User user = userDao.get(userDto.getId());
		user.setIsAdmin(false);
		Set<Role> roleSet = new HashSet<Role>();
		// 如果选择了角色给roleSet赋值
		if (null != userDto.getRoleIds() && userDto.getRoleIds().length > 0) {
			List<Role> roleList = roleService.findByIds(userDto.getRoleIds());
			if (null != roleList && roleList.size() > 0) {
				// 关系表中维护用户与角色关系
				for (Role role : roleList) {
					if (role.getIsSuper() != null && role.getIsSuper()) {
						user.setIsAdmin(true);
					}
					roleSet.add(role);
					recordLog(user, user.getUserExt(), role);
				}
			}
		}
		// 若没有选择角色则roleSet=null
		user.setRoles(roleSet);

		if (userDto.getProjectIds() != null && userDto.getProjectIds().length > 0) {
			Set<TestProjects> testprojects = new HashSet<TestProjects>();
			for (Long projectId : userDto.getProjectIds()) {
				testprojects.add(testProjectsMng.getById(projectId));
			}
			user.setTestprojects(testprojects);
		}
		// 用户信息
		user.setEmail(userDto.getEmail());
		user.setIsDisabled(userDto.getIsDisabled());
		user.setEntryMode(userDto.getEntryMode());
		if (org.springframework.util.StringUtils.hasLength(userDto.getPassword())) {
			user.setPassword(userDto.getPassword());
		}

		// 扩展信息
		UserExt userExt = user.getUserExt();
		UserExtDto userExtDto = userDto.getUserExtDto();
		// 主键id、创建时间、创建人固定不变
		String[] ignorePor = { "idUser", "createUser", "createDate" };
		BeanUtils.copyProperties(userExtDto, userExt, ignorePor);
		// one-to-one
		user.setUserExt(userExt);
		userExt.setUser(user);
		// 更新
		userDao.update(user);
	}

	public void update4Person(UserDto userDto) {
		// PO
		User user = userDao.get(userDto.getId());

		// 用户信息
		user.setEmail(userDto.getEmail());

		// 扩展信息
		UserExt userExt = user.getUserExt();
		UserExtDto userExtDto = userDto.getUserExtDto();
		// 主键id、创建时间、创建人固定不变
		String[] ignorePor = { "idUser", "createUser", "createDate" };
		BeanUtils.copyProperties(userExtDto, userExt, ignorePor);

		// one-to-one
		user.setUserExt(userExt);
		userExt.setUser(user);

		// 更新
		userDao.update(user);
	}

	public void setPwdEncoder(PwdEncoder pwdEncoder) {
		this.pwdEncoder = pwdEncoder;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public User registerUser(User user, UserExt ext, Long[] ids, Long[] projectIds) {
		user.setIsAdmin(false);
		// 如果选择了角色
		if (null != ids && ids.length > 0) {
			Set<Role> roleSet = new HashSet<Role>();
			List<Role> roleList = roleService.findByIds(ids);
			if (null != roleList && roleList.size() > 0) {
				// 关系表中维护用户与角色关系
				for (Role role : roleList) {
					if (role.getIsSuper() != null && role.getIsSuper()) {
						user.setIsAdmin(true);
					}
					recordLog(user, ext, role);
					roleSet.add(role);
				}
				user.setRoles(roleSet);
			}
		}
		if (projectIds != null && projectIds.length > 0) {
			Set<TestProjects> testprojects = new HashSet<TestProjects>();
			for (Long projectId : projectIds) {
				testprojects.add(testProjectsMng.getById(projectId));
			}
			user.setTestprojects(testprojects);
		}
		ext.setUser(user);
		user.setUserExt(ext);
		userDao.save(user);
		return user;
	}

	public boolean usernameNotExist(String username) {
		return userDao.countByUsername(username) <= 0;
	}

	public boolean emailNotExist(String email) {
		return userDao.countByEmail(email) <= 0;
	}

	public void setRoleLogMng(RoleLogMng roleLogMng) {
		this.roleLogMng = roleLogMng;
	}

	private void recordLog(User user, UserExt ext, Role role) {
		RoleLog roleLog = new RoleLog();
		roleLog.setCreateUser(ext.getCreateUser());
		roleLog.setSourceRoleId(role.getId());
		roleLog.setIsWarning(role.getIsWarning());
		roleLog.setTargetUser(user.getUsername());
		roleLogMng.save(roleLog);
	}

	public void update(User user) {
		userDao.update(user);
	}
}
