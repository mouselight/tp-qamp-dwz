package com.bill99.riaframework.orm.manager;

import java.util.List;

import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.domain.security.BadCredentialsException;
import com.bill99.riaframework.domain.security.UsernameNotFoundException;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.entity.UserExt;
import com.jeecms.common.page.Pagination;

public interface UserService {
	/**
	 * 忘记密码
	 * 
	 * @param userId
	 *            用户ID
	 * @param email
	 *            发送者邮件信息
	 * @param tpl
	 *            邮件模板。内容模板可用变量${uid}、${username}、${resetKey}、${resetPwd}。
	 * @return
	 */

	public User login(String username, String password, String ip) throws UsernameNotFoundException,
			BadCredentialsException;

	public boolean usernameExist(String username);

	public boolean emailExist(String email);

	public User getByUsername(String username);

	public List<User> getByEmail(String email);

	public Pagination getPage(UserDto userDto, int pageNo, int pageSize);

	public User findById(Long id);

	/**
	 * 修改邮箱和密码
	 * 
	 * @param id
	 *            用户ID
	 * @param password
	 *            未加密密码。如果为null或空串则不修改。
	 * @param email
	 *            电子邮箱。如果为空串则设置为null。
	 * @return
	 */
	public User update(Long id, String password, String email);

	/**
	 * 密码是否正确
	 * 
	 * @param id
	 *            用户ID
	 * @param password
	 *            未加密密码
	 * @return
	 */
	public boolean isPasswordValid(Long id, String password);

	public User deleteById(Long id);

	public User[] deleteByIds(Long[] ids);

	public void updateLoginInfo(Long userId, String ip);

	/**
	 * 注册新会员
	 * 
	 * @param user
	 *            用户信息，如用户名、密码
	 * @param ext
	 *            用户扩展信息，如真实姓名、地址、电话
	 * @param ids
	 *            注册时确定的会员角色id，如超级管理员，可多选，没有选择置为null。
	 * @param projectIds 
	 * @return
	 */
	public User registerUser(User user, UserExt ext, Long[] ids, Long[] projectIds);

	public boolean usernameNotExist(String username);

	public void update(UserDto userDto);

	/**
	 * 手动修改密码
	 * 
	 * @param origPwd
	 *            新密码，已经加密.
	 * @param userId
	 *            需要修改密码的用户的id
	 */
	public void editOptertorPwd(String newPwd, Long userId);

	public void update4Person(UserDto userDto);

	public void update(User user);

}
