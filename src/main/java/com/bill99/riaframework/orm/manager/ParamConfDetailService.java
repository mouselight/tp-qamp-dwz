/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
*/

package com.bill99.riaframework.orm.manager;

import java.util.List;

import com.bill99.riaframework.common.dto.ParamConfDetailDto;
import com.bill99.riaframework.orm.entity.ParamConfDetail;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 *
 */
public interface ParamConfDetailService {

	public Pagination getConfDetailList(String classCode, int pageNo, int pageSize);

	public List<ParamConfDetail> getConfDetailList(String classCode);

	public ParamConfDetailDto getConfDetailById(Long id);

	public void update(ParamConfDetailDto paramConfDetailDto);

	public void deleteLogic(ParamConfDetailDto paramConfDetailDto);

	public ParamConfDetail save(ParamConfDetailDto paramConfDetailDto);

	public boolean vldCode(int type, String classCode, String code);

	public ParamConfDetail getByClassCodeCode(String classCode, String code);

}
