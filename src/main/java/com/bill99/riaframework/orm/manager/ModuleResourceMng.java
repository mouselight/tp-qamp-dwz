/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.jeecms.common.page.Pagination;

/**
 * @author leofung
 *
 */
public interface ModuleResourceMng {

	public Pagination findAllModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage);

	public Pagination findUnboundModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage);
	
	public List<ModuleResource> getValidModuleResource();

	public Map<String, Long> getCheckedModuleResource(Long idModule);

	public ModuleResource save(ModuleResource moduleResource);

	public void update(ModuleResource moduleResource);

	public ModuleResource getModuleResource(Long idModuleResource);

	public ModuleResource findByUri(String uri);

}
