package com.bill99.riaframework.orm.manager.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.RoleQueryDto;
import com.bill99.riaframework.orm.dao.RoleDao;
import com.bill99.riaframework.orm.entity.Role;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.RoleService;
import com.bill99.riaframework.orm.manager.UserService;
import com.jeecms.common.page.Pagination;

public class RoleServiceImpl implements RoleService {
	private RoleDao roleDao;
	private UserService userService;

	public List<Role> getList() {
		return roleDao.getList();
	}

	public Map<String, String> getRoleMap() {
		Map<String, String> roleMap = new HashMap<String, String>();
		List<Role> roleList = roleDao.getList();
		if (null != roleList && roleList.size() > 0) {
			for (Role role : roleList) {
				roleMap.put(role.getId().toString(), role.getName());
			}
		}
		return roleMap;
	}

	public Role findById(Long id) {
		return roleDao.get(id);
	}

	public Role save(Role bean) {
		return roleDao.save(bean);
	}

	public void update(Role bean) {
		// 同步user的超级管理员标志
		for (User user : bean.getUsers()) {
			if (user.getIsAdmin() != bean.getIsSuper()) {
				user.setIsAdmin(bean.getIsSuper());
				userService.update(user);
			}
		}
		roleDao.update(bean);
	}

	public Role deleteById(Long id) {
		Role bean = roleDao.get(id);
		if (bean != null) {
			bean.setStatus(Boolean.FALSE);
			bean.setUpdateDate(new Date());
			roleDao.update(bean);
		}
		return bean;
	}

	public Role[] deleteByIds(Long[] ids) {
		Role[] beans = new Role[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	public Pagination getRoleList(RoleQueryDto roleQueryDto, Integer pageNo, Integer pageSize) {
		return roleDao.getRoleList(roleQueryDto, pageNo, pageSize);
	}

	public List<Role> findByIds(Long[] ids) {
		return roleDao.findByIds(ids);
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public List<Role> getValidRole() {
		return roleDao.getValidRole();
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Map<String, Long> getCheckedRole(Long idUser) {

		Map<String, Long> checkedRoleMap = new HashMap<String, Long>();
		List<Role> checkedRoleList = roleDao.getCheckedRole(idUser);
		if (checkedRoleList != null) {
			for (Role Role : checkedRoleList) {
				checkedRoleMap.put(Role.getId().toString(), Role.getId());
			}
		}
		return checkedRoleMap;

	}

	public boolean getCheckedRole(Long userId, String roleName) {
		boolean hasRolePermisson = false;
		List<Role> checkedRoleList = roleDao.getCheckedRole(userId, roleName);
		if (null != checkedRoleList && checkedRoleList.size() > 0) {
			hasRolePermisson = true;
		}
		return hasRolePermisson;
	}

}
