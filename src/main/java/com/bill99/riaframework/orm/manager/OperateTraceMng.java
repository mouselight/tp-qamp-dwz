/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.manager;

import com.bill99.riaframework.orm.entity.OperateTrace;

/**
 * @author leofung
 *
 */
public interface OperateTraceMng {

	public OperateTrace save(OperateTrace operateTrace);

}
