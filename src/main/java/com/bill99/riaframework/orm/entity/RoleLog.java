/*
 * File: Description: Copyright 2004-2012 99Bill Corporation. All rights reserved. Date Author Changes 2012-9-18 leofung leofung
 */

package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author leofung
 * 
 */
public class RoleLog implements Serializable {

	private static final long serialVersionUID = -9215509730377181488L;

	private Long idRoleLog;
	private Long sourceRoleId;
	private String targetUser;
	private Boolean isWarning;
	private Date createDate;
	private String createUser;

	/**
	 * @return the idRoleLog
	 */
	public Long getIdRoleLog() {
		return idRoleLog;
	}

	/**
	 * @param idRoleLog
	 *            the idRoleLog to set
	 */
	public void setIdRoleLog(Long idRoleLog) {
		this.idRoleLog = idRoleLog;
	}

	/**
	 * @return the sourceRoleId
	 */
	public Long getSourceRoleId() {
		return sourceRoleId;
	}

	/**
	 * @param sourceRoleId
	 *            the sourceRoleId to set
	 */
	public void setSourceRoleId(Long sourceRoleId) {
		this.sourceRoleId = sourceRoleId;
	}

	/**
	 * @return the targetUser
	 */
	public String getTargetUser() {
		return targetUser;
	}

	/**
	 * @param targetUser
	 *            the targetUser to set
	 */
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	/**
	 * @return the isWarning
	 */
	public Boolean getIsWarning() {
		return isWarning;
	}

	/**
	 * @param isWarning
	 *            the isWarning to set
	 */
	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser
	 *            the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

}
