/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author leofung
 * 
 */
public class OperateTrace implements Serializable {

	private static final long serialVersionUID = -2024854381581346360L;

	private Long idOperateTrace;
	private String method;
	private String uri;
	private String sessionId;
	private String clientIp;
	private String userName;
	private Date createDate;

	public Long getIdOperateTrace() {
		return idOperateTrace;
	}

	public void setIdOperateTrace(Long idOperateTrace) {
		this.idOperateTrace = idOperateTrace;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
