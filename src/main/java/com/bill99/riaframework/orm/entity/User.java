package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public class User implements Serializable {

	private static final long serialVersionUID = -310580710938604364L;

	private Long id;
	private String username;
	private String realname;
	private String password;
	private String email;
	private String registerIp;
	private Date lastLoginTime;
	private String lastLoginIp;
	private Integer loginCount;
	private Boolean isDisabled;
	private Boolean isAdmin;
	private Date registerTime;
	private String resetKey;
	private String resetPwd;
	private String entryMode;

	private UserExt userExt;

	private Set<Role> roles;

	private Set<TestProjects> testprojects;
	private TestProjects currentTestProject;

	public Set<String> getPerms() {
		if (roles == null) {
			return null;
		}
		Set<String> allPerms = new HashSet<String>();
		for (Role role : roles) {
			if (role.getStatus() != null && role.getStatus()) {
				Set<Module> modules = role.getModules();
				if (modules != null) {
					for (Module module : modules) {
						if (module.getStatus()) {
							Set<ModuleResource> moduleResources = module.getModuleResources();
							if (moduleResources != null) {
								for (ModuleResource moduleResource : moduleResources) {
									if (moduleResource.getStatus()) {
										allPerms.add(moduleResource.getUri());
									}
								}
							}
						}
					}
				}
			}
		}
		if (getIsAdmin() != null && getIsAdmin()) {
			allPerms.add(RiaFrameworkUtils.IS_ADMIN);
		}
		return allPerms;
	}

	public List<Long> getRoleIds() {
		List<Long> roleIds = new ArrayList<Long>();
		Set<Role> roles1 = getRoles();
		if (roles1 == null) {
			return roleIds;
		}
		Iterator<Role> roleIter = getRoles().iterator();
		while (roleIter.hasNext()) {
			Role role = roleIter.next();
			roleIds.add(role.getId());
		}
		return roleIds;
	}

	public boolean isSuper() {
		if (getRoles() == null) {
			return false;
		}
		for (Role role : getRoles()) {
			if (role.getIsSuper()) {
				return true;
			}
		}
		return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public Boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public String getResetPwd() {
		return resetPwd;
	}

	public void setResetPwd(String resetPwd) {
		this.resetPwd = resetPwd;
	}

	public UserExt getUserExt() {
		if (null == userExt) {
			userExt = new UserExt();
		}
		return userExt;
	}

	public void setUserExt(UserExt userExt) {
		this.userExt = userExt;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}

	public String getEntryMode() {
		return entryMode;
	}

	public Set<TestProjects> getTestprojects() {
		return testprojects;
	}

	public void setTestprojects(Set<TestProjects> testprojects) {
		this.testprojects = testprojects;
	}

	public TestProjects getCurrentTestProject() {
		//		if (currentTestProject != null) {
		//			currentTestProject.setTreeName(currentTestProject.getPrefix() + TreeUtils.TC_QUANTITY_MAP.get(currentTestProject.getId()));
		//		}
		return currentTestProject;
	}

	public void setCurrentTestProject(TestProjects currentTestProject) {
		this.currentTestProject = currentTestProject;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getRealname() {
		return realname;
	}

}
