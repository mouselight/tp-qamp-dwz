package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Role implements Serializable {
	private static final long serialVersionUID = -7597399959810149499L;

	private Long id;
	private String name;
	private Integer priority;
	private Boolean isSuper;
	private Boolean isWarning;

	private Boolean status;
	private Date updateDate;
	private String updateUser;
	private Date createDate;
	private String createUser;

	private Set<User> users;
	private Set<Module> modules;
	private Map<String, Long> modulesMap;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Boolean getIsSuper() {
		return isSuper;
	}

	public void setIsSuper(Boolean isSuper) {
		this.isSuper = isSuper;
	}

	public Boolean getIsWarning() {
		return isWarning;
	}

	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @param modulesMap
	 *            the modulesMap to set
	 */
	public void setModulesMap(Map<String, Long> modulesMap) {
		this.modulesMap = modulesMap;
	}

	/**
	 * @return the modulesMap
	 */
	public Map<String, Long> getModulesMap() {
		Map<String, Long> modulesMap1 = new HashMap<String, Long>();
		if (modules != null) {
			for (Module module : modules) {
				modulesMap1.put(module.getIdModule().toString(), module.getIdModule());
			}
		}
		return modulesMap1;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @return the modules
	 */
	public Set<Module> getModules() {
		return modules;
	}

	/**
	 * @param modules
	 *            the modules to set
	 */
	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}

}
