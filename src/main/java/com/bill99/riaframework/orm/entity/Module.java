/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.bill99.testmp.framework.utils.PriorityInterface;
import com.jeecms.common.util.SelectTree;

/**
 * @author leofung
 * 
 */
public class Module implements Serializable, SelectTree, PriorityInterface {

	private static final long serialVersionUID = 5436773892927838597L;

	private Long idModule;
	private String name;
	private String moduleType;
	private String description;
	private Boolean status;
	private Date updateDate;
	private String updateUser;
	private Date createDate;
	private String createUser;
	private Integer priority;
	private String uri;
	private Boolean isMenu;
	private Long parentId;

	// many to one
	private Module parent;
	// collections
	private Set<Module> child;
	private Set<ModuleResource> moduleResources;

	// 下拉列表树
	private String selectTree;

	private Map<String, Long> moduleResourcesMap;

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Set<ModuleResource> getModuleResources() {
		return moduleResources;
	}

	public void setModuleResources(Set<ModuleResource> moduleResources) {
		this.moduleResources = moduleResources;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param isMenu
	 *            the isMenu to set
	 */
	public void setIsMenu(Boolean isMenu) {
		this.isMenu = isMenu;
	}

	/**
	 * @return the isMenu
	 */
	public Boolean getIsMenu() {
		return isMenu;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(Module parent) {
		this.parent = parent;
	}

	/**
	 * @return the parent
	 */
	public Module getParent() {
		return parent;
	}

	/**
	 * @param child
	 *            the child to set
	 */
	public void setChild(Set<Module> child) {
		this.child = child;
	}

	/**
	 * @return the child
	 */
	public Set<Module> getChild() {
		return child;
	}

	/************************/

	public String getSelectTree() {
		return selectTree;
	}

	public String getTreeName() {
		return getName();
	}

	public Module getTreeParent() {
		return getParent();
	}

	public void setSelectTree(String selectTree) {
		this.selectTree = selectTree;
	}

	public Set<? extends SelectTree> getTreeChild() {
		return getChild();
	}

	public void setTreeChild(Set treeChild) {
		// do nothing
	}

	public Set<? extends SelectTree> getTreeChildRaw() {
		return null;
	}

	public Long getId() {
		return idModule;
	}

	/**
	 * @param moduleResourcesMap
	 *            the moduleResourcesMap to set
	 */
	public void setModuleResourcesMap(Map<String, Long> moduleResourcesMap) {
		this.moduleResourcesMap = moduleResourcesMap;
	}

	/**
	 * @return the moduleResourcesMap
	 */
	public Map<String, Long> getModuleResourcesMap() {
		Map<String, Long> modulesMap1 = new HashMap<String, Long>();
		if (moduleResources != null) {
			for (ModuleResource moduleResource : moduleResources) {
				modulesMap1.put(moduleResource.getIdModuleResource().toString(), moduleResource.getIdModuleResource());
			}
		}
		return modulesMap1;
	}

}
