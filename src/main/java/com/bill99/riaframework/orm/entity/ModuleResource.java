/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author leofung
 * 
 */
public class ModuleResource implements Serializable {

	private static final long serialVersionUID = -1047560633075881394L;

	private Long idModuleResource;
	private Long idModule;
	private String name;
	private String moduleType;
	private Integer optType;
	private String description;
	private String ref;
	private String uri;
	private Boolean status;
	private Date updateDate;
	private String updateUser;
	private Date createDate;
	private String createUser;

	private Set<Module> modules;

	private Map<String, Long> modulesMap;

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getIdModuleResource() {
		return idModuleResource;
	}

	public void setIdModuleResource(Long idModuleResource) {
		this.idModuleResource = idModuleResource;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Integer getOptType() {
		return optType;
	}

	public void setOptType(Integer optType) {
		this.optType = optType;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	/**
	 * @param modules
	 *            the modules to set
	 */
	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}

	/**
	 * @return the modules
	 */
	public Set<Module> getModules() {
		return modules;
	}

	/**
	 * @param modulesMap
	 *            the modulesMap to set
	 */
	public void setModulesMap(Map<String, Long> modulesMap) {
		this.modulesMap = modulesMap;
	}

	/**
	 * @return the modulesMap
	 */
	public Map<String, Long> getModulesMap() {
		Map<String, Long> modulesMap1 = new HashMap<String, Long>();
		if (modules != null) {
			for (Module module : modules) {
				modulesMap1.put(module.getIdModule().toString(), module.getIdModule());
			}
		}
		return modulesMap1;
	}

}
