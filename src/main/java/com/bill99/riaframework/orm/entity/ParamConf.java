/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
*/

package com.bill99.riaframework.orm.entity;

import java.io.Serializable;

/**
 * @author leo
 *
 */
public class ParamConf implements Serializable {

	private static final long serialVersionUID = 8570267704174278284L;

	private String classCode;
	private String name;

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
