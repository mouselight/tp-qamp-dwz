/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.dao.impl;

import java.util.List;

import com.bill99.riaframework.orm.dao.ModuleDao;
import com.bill99.riaframework.orm.entity.Module;
import com.jeecms.common.hibernate3.BaseDaoImpl;

/**
 * @author leofung
 * 
 */
public class ModuleDaoImpl extends BaseDaoImpl<Module> implements ModuleDao {

	private final String getChildHql = "from Module where parent.idModule = :parentId order by priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getChild(Long parentId) {
		return getSession().createQuery(getChildHql).setLong("parentId", parentId).list();
	}

	private final String getRootsHql = "from Module where parent.idModule is null order by priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getRoots() {
		return getSession().createQuery(getRootsHql).list();
	}

	private final String getVldRootsHql = "from Module where parent.idModule is null and status=true order by priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getVldRoots() {
		return getSession().createQuery(getVldRootsHql).list();
	}

	private final String getVldModulesHql = "from Module where status=true order by priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getVldModules() {
		return getSession().createQuery(getVldModulesHql).list();
	}

	private final String getModulesByUserHql = "from Module module where module.idModule in"
			+ " (select f1.id from User user join user.roles role join role.modules f1 where user.id = :idUser )" + " order by module.priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getModulesByUser(Long idUser) {
		return getSession().createQuery(getModulesByUserHql).setLong("idUser", idUser).list();
	}

	private final String getBannerModulesHql = "from Module module where module.idModule in"
			+ " (select f1.id from User user join user.roles role join role.modules f1 where user.id = :idUser )" + " and module.parent.idModule is null "
			+ " order by module.priority asc";

	@SuppressWarnings("unchecked")
	public List<Module> getBannerModules(Long userId) {
		return getSession().createQuery(getBannerModulesHql).setLong("idUser", userId).list();
	}

}
