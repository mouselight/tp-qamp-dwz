package com.bill99.riaframework.orm.dao;

import java.util.List;

import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.orm.entity.User;
import com.jeecms.common.hibernate3.BaseDao;
import com.jeecms.common.page.Pagination;

public interface UserDao extends BaseDao<User> {

	public User getByUsername(String username);

	public List<User> getByEmail(String email);

	public int countByEmail(String email);

	public Pagination getPage(UserDto userDto, int pageNo, int pageSize);

	public int countByUsername(String realname);
}
