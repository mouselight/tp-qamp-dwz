/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.riaframework.orm.dao.impl;

import com.bill99.riaframework.orm.dao.ParamConfDao;
import com.bill99.riaframework.orm.entity.ParamConf;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.JeeCoreDaoImpl;

/**
 * @author leo
 * 
 */
public class ParamConfDaoImpl extends JeeCoreDaoImpl<ParamConf> implements ParamConfDao {

	private final String getConfListHql = "from ParamConf where 1=1";

	public Pagination getConfList(int pageNo, int pageSize) {
		Finder finder = new Finder(getConfListHql);
		return find(finder, pageNo, pageSize);
	}

}
