package com.bill99.riaframework.orm.dao;

import java.util.List;

import com.bill99.riaframework.common.dto.RoleQueryDto;
import com.bill99.riaframework.orm.entity.Role;
import com.jeecms.common.hibernate3.BaseDao;
import com.jeecms.common.page.Pagination;

public interface RoleDao extends BaseDao<Role> {

	public List<Role> getList();

	public Pagination getRoleList(RoleQueryDto roleQueryDto, Integer pageNo, Integer pageSize);

	public List<Role> findByIds(Long[] ids);

	public List<Role> getValidRole();

	public List<Role> getCheckedRole(Long idUser);

	public List<Role> getCheckedRole(Long idUser, String roleName);

}
