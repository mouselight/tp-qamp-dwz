/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.riaframework.orm.dao;

import com.bill99.riaframework.orm.entity.ParamConf;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.JeeCoreDao;

/**
 * @author leo
 * 
 */
public interface ParamConfDao extends JeeCoreDao<ParamConf> {

	public Pagination getConfList(int pageNo, int pageSize);

}
