/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.dao.impl;

import com.bill99.riaframework.orm.dao.OperateTraceDao;
import com.bill99.riaframework.orm.entity.OperateTrace;
import com.jeecms.common.hibernate3.BaseDaoImpl;

/**
 * @author leofung
 *
 */
public class OperateTraceDaoImpl extends BaseDaoImpl<OperateTrace> implements OperateTraceDao {

}
