/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.dao;

import java.util.List;

import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.jeecms.common.hibernate3.BaseDao;
import com.jeecms.common.page.Pagination;

/**
 * @author leofung
 *
 */
public interface ModuleResourceDao extends BaseDao<ModuleResource> {

	public Pagination findAllModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage);
	
	public Pagination findUnboundResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage);

	public List<ModuleResource> getValidModuleResource();

	public List<ModuleResource> getCheckedModuleResource(Long idModule);

	public ModuleResource findByUri(String uri);
}
