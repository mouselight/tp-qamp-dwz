/*
 * File: Description: Copyright 2004-2012 99Bill Corporation. All rights reserved. Date Author Changes 2012-9-18 leofung leofung
 */

package com.bill99.riaframework.orm.dao;

import com.bill99.riaframework.orm.entity.RoleLog;
import com.jeecms.common.hibernate3.BaseDao;

/**
 * @author leofung
 * 
 */
public interface RoleLogDao extends BaseDao<RoleLog> {

}
