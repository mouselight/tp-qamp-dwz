package com.bill99.riaframework.orm.dao.impl;

import java.util.Date;

import com.bill99.riaframework.orm.dao.AuthenticationDao;
import com.bill99.riaframework.orm.entity.Authentication;
import com.jeecms.common.hibernate3.BaseDaoImpl;

public class AuthenticationDaoImpl extends BaseDaoImpl<Authentication> implements AuthenticationDao {
	public int deleteExpire(Date d) {
		String hql = "delete Authentication bean where bean.updateTime <= :d";
		return getSession().createQuery(hql).setTimestamp("d", d).executeUpdate();
	}

	public Authentication getByUserId(String userId) {
		String hql = "from Authentication bean where bean.uid=?";
		return (Authentication) findUnique(hql, userId);
	}

}
