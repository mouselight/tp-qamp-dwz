/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.riaframework.orm.dao.impl;

import java.util.List;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.orm.dao.ModuleResourceDao;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.jeecms.common.hibernate3.BaseDaoImpl;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;

/**
 * @author leofung
 * 
 */
public class ModuleResourceDaoImpl extends BaseDaoImpl<ModuleResource> implements ModuleResourceDao {

	private final String queryModuleResourceHql = "select mr from ModuleResource mr";

	public Pagination findAllModuleResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage) {
		Finder finder = Finder.create(queryModuleResourceHql);

		if (moduleResourceQueryDto.getParentId() != null) {
			finder.append(" left join mr.modules m where m.idModule=:parentId");
			finder.setParam("parentId", moduleResourceQueryDto.getParentId());
		} else {
			finder.append(" where 1=1 ");
		}
		if (StringUtils.hasLength(moduleResourceQueryDto.getName())) {
			finder.append(" and mr.name like :name");
			finder.setParam("name", "%" + moduleResourceQueryDto.getName() + "%");
		}
		if (StringUtils.hasLength(moduleResourceQueryDto.getUri())) {
			finder.append(" and mr.uri like :uri");
			finder.setParam("uri", "%" + moduleResourceQueryDto.getUri() + "%");
		}
		if (moduleResourceQueryDto.getOptType() != null) {
			finder.append(" and mr.optType = :optType");
			finder.setParam("optType", moduleResourceQueryDto.getOptType());
		}
		if (moduleResourceQueryDto.getStatus() != null) {
			finder.append(" and mr.status = :status");
			finder.setParam("status", moduleResourceQueryDto.getStatus());
		}

		finder.append(" order by mr.createDate desc, mr.updateDate desc ");
		return find(finder, pageNum, numPerPage);
	}
	
	public Pagination findUnboundResources(ModuleResourceQueryDto moduleResourceQueryDto, Integer pageNum, Integer numPerPage) {
		
		String queryUnboundResourcesHql = " select mr from ModuleResource mr where mr.idModuleResource not in ( " +
				" select mrs.idModuleResource from Module m left join m.moduleResources mrs where m.idModule = :idModule " +
				" and mrs.idModuleResource is not null ) ";
		
		Finder finder = Finder.create(queryUnboundResourcesHql);
		
		if (moduleResourceQueryDto.getIdModule() != null) {
			finder.setParam("idModule", moduleResourceQueryDto.getIdModule());
		} else {
			return null;
		}
		
		if (StringUtils.hasLength(moduleResourceQueryDto.getName())) {
			finder.append(" and mr.name like :name");
			finder.setParam("name", "%" + moduleResourceQueryDto.getName() + "%");
		}
		if (StringUtils.hasLength(moduleResourceQueryDto.getUri())) {
			finder.append(" and mr.uri like :uri");
			finder.setParam("uri", "%" + moduleResourceQueryDto.getUri() + "%");
		}
		if (moduleResourceQueryDto.getStatus() != null) {
			finder.append(" and mr.status = :status");
			finder.setParam("status", moduleResourceQueryDto.getStatus());
		}

		finder.append(" order by mr.createDate desc, mr.updateDate desc ");
		return find(finder, pageNum, numPerPage);
	}
	

	private final String getValidModuleResourceHql = "select mr from ModuleResource mr where mr.status = true";

	@SuppressWarnings("unchecked")
	public List<ModuleResource> getValidModuleResource() {
		return getSession().createQuery(getValidModuleResourceHql).list();
	}

	private final String getCheckedModuleResourceHql = "select mr from ModuleResource mr left outer  join mr.modules m where m.idModule = :idModule and mr.status = true";

	@SuppressWarnings("unchecked")
	public List<ModuleResource> getCheckedModuleResource(Long idModule) {
		return getSession().createQuery(getCheckedModuleResourceHql).setLong("idModule", idModule).list();
	}

	private final String findByUriHql = "select mr from ModuleResource mr where mr.uri = :uri";

	public ModuleResource findByUri(String uri) {
		return (ModuleResource) getSession().createQuery(findByUriHql).setString("uri", uri).setMaxResults(1).uniqueResult();
	}
}
