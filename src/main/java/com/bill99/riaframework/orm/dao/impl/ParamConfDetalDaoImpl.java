/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
*/

package com.bill99.riaframework.orm.dao.impl;

import java.util.List;

import com.bill99.riaframework.orm.dao.ParamConfDetalDao;
import com.bill99.riaframework.orm.entity.ParamConfDetail;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.JeeCoreDaoImpl;

/**
 * @author leo
 *
 */
public class ParamConfDetalDaoImpl extends JeeCoreDaoImpl<ParamConfDetail> implements ParamConfDetalDao {

	private final String getConfDetailList = "from ParamConfDetail where 1=1 and classCode=:classCode and status=true  order by ordNum asc";

	public Pagination getConfDetailList(String classCode, int pageNo, int pageSize) {
		Finder finder = new Finder(getConfDetailList);
		finder.setParam("classCode", classCode);
		return find(finder, pageNo, pageSize);
	}

	@SuppressWarnings("unchecked")
	public List<ParamConfDetail> getConfDetailList(String classCode) {
		Finder finder = new Finder(getConfDetailList);
		finder.setParam("classCode", classCode);
		return find(finder);
	}

	private final String getByCodeHql = "from ParamConfDetail t where 1=1 and t.status=true and t.code=:code and classCode=:classCode";

	public ParamConfDetail getByCode(String classCode, String code) {
		return (ParamConfDetail) getSession().createQuery(getByCodeHql).setString("code", code).setString("classCode", classCode).setMaxResults(1).uniqueResult();
	}

	private final String getByNameHql = "from ParamConfDetail t where 1=1 and t.status=true and t.name=:code and classCode=:classCode";

	public ParamConfDetail getByName(String classCode, String code) {
		return (ParamConfDetail) getSession().createQuery(getByNameHql).setString("code", code).setString("classCode", classCode).setMaxResults(1).uniqueResult();
	}

	private final String getByClassCodeCodeHql = "from ParamConfDetail t where 1=1 and t.status=true and t.code=:code and classCode=:classCode";

	public ParamConfDetail getByClassCodeCode(String classCode, String code) {
		return (ParamConfDetail) getSession().createQuery(getByClassCodeCodeHql).setString("code", code).setString("classCode", classCode).setMaxResults(1).uniqueResult();
	}
}
