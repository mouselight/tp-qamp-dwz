package com.bill99.riaframework.orm.dao;

/*
 File: 
 Description:
 Copyright 2004-2010 99Bill Corporation. All rights reserved.
 Date            Author          Changes
 2012-8-27		    leofung	   		leofung
 */

import java.util.List;

import com.bill99.riaframework.orm.entity.Module;
import com.jeecms.common.hibernate3.BaseDao;

/**
 * @author leofung
 * 
 */
public interface ModuleDao extends BaseDao<Module> {

	// 获得所有根节点
	public List<Module> getRoots();

	// 获得有效根节点
	public List<Module> getVldRoots();

	public List<Module> getVldModules();

	// 获得子节点
	public List<Module> getChild(Long parentId);

	public List<Module> getModulesByUser(Long idUser);

	public List<Module> getBannerModules(Long userId);

}
