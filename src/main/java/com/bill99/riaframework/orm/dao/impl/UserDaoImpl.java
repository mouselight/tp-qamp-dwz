package com.bill99.riaframework.orm.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.orm.dao.UserDao;
import com.bill99.riaframework.orm.entity.User;
import com.jeecms.common.hibernate3.BaseDaoImpl;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	public User getByUsername(String username) {
		String hql = "from User where username=:username";
		Query query = getSession().createQuery(hql);
		query.setString("username", username);
		return (User) query.setMaxResults(1).uniqueResult();
	}

	public List<User> getByEmail(String email) {
		return findByProperty("email", email);
	}

	public int countByEmail(String email) {
		String hql = "select count(*) from User bean where bean.email=:email";
		Query query = getSession().createQuery(hql);
		query.setParameter("email", email);
		return ((Number) query.iterate().next()).intValue();
	}

	private final String getPageHql = "from User where 1=1 ";

	public Pagination getPage(UserDto userDto, int pageNo, int pageSize) {
		Finder finder = Finder.create(getPageHql);

		if (StringUtils.hasLength(userDto.getUsername())) {
			finder.append(" and username like :username");
			finder.setParam("username", "%" + userDto.getUsername() + "%");
		}
		if (StringUtils.hasLength(userDto.getEmail())) {
			finder.append(" and email like :email");
			finder.setParam("email", "%" + userDto.getEmail() + "%");
		}
		if (userDto.getIsDisabled() != null) {
			finder.append(" and isDisabled = :isDisabled");
			finder.setParam("isDisabled", userDto.getIsDisabled());
		}
		finder.append(" order by username asc");
		return find(finder, pageNo, pageSize);
	}

	public int countByUsername(String username) {
		String hql = "select count(*) from User bean where bean.username=:username";
		Query query = getSession().createQuery(hql);
		query.setParameter("username", username);
		return ((Number) query.iterate().next()).intValue();
	}
}
