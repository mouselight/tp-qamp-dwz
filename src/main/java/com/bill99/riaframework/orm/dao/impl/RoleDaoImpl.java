package com.bill99.riaframework.orm.dao.impl;

import java.util.List;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.dto.RoleQueryDto;
import com.bill99.riaframework.orm.dao.RoleDao;
import com.bill99.riaframework.orm.entity.Role;
import com.jeecms.common.hibernate3.BaseDaoImpl;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;

public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

	@SuppressWarnings("unchecked")
	public List<Role> getList() {
		String hql = "from Role bean order by bean.priority asc";
		return find(hql);
	}

	private final String getRoleListHql = "from Role where 1=1 ";

	public Pagination getRoleList(RoleQueryDto roleQueryDto, Integer pageNo, Integer pageSize) {
		Finder finder = Finder.create(getRoleListHql);
		if (StringUtils.hasLength(roleQueryDto.getName())) {
			finder.append(" and name like :name");
			finder.setParam("name", "%" + roleQueryDto.getName() + "%");
		}
		if (roleQueryDto.getStatus() != null) {
			finder.append(" and status = :status");
			finder.setParam("status", roleQueryDto.getStatus().booleanValue());
		}
		finder.append(" order by id desc");
		return find(finder, pageNo, pageSize);
	}

	@SuppressWarnings("unchecked")
	public List<Role> findByIds(Long[] ids) {
		String hql = "from Role where id in (:ids)";
		return getSession().createQuery(hql).setParameterList("ids", ids).list();
	}

	private final String getValidRoleHql = "from Role where status = true";

	@SuppressWarnings("unchecked")
	public List<Role> getValidRole() {
		return getSession().createQuery(getValidRoleHql).list();
	}

	private final String getCheckedRoleHql = "select r from Role r left outer  join r.users u where u.id = :idUser and r.status = true";

	@SuppressWarnings("unchecked")
	public List<Role> getCheckedRole(Long idUser) {
		return getSession().createQuery(getCheckedRoleHql).setLong("idUser", idUser).list();
	}

	private final String getCheckedRoleHql2 = "select r from Role r left outer join r.users u where u.id = :idUser and r.status = true and r.name = :name";

	@SuppressWarnings("unchecked")
	public List<Role> getCheckedRole(Long idUser, String roleName) {
		return getSession().createQuery(getCheckedRoleHql2).setLong("idUser", idUser).setString("name", roleName)
				.list();
	}
}
