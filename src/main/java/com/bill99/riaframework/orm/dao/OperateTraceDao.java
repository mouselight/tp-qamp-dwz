/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.riaframework.orm.dao;

import com.bill99.riaframework.orm.entity.OperateTrace;
import com.jeecms.common.hibernate3.BaseDao;

/**
 * @author leofung
 *
 */
public interface OperateTraceDao extends BaseDao<OperateTrace> {

}
