/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.riaframework.orm.dao;

import java.util.List;

import com.bill99.riaframework.orm.entity.ParamConfDetail;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.JeeCoreDao;

/**
 * @author leo
 * 
 */
public interface ParamConfDetalDao extends JeeCoreDao<ParamConfDetail> {

	public Pagination getConfDetailList(String classCode, int pageNo, int pageSize);

	public List<ParamConfDetail> getConfDetailList(String classCode);

	public ParamConfDetail getByCode(String classCode, String code);

	public ParamConfDetail getByName(String classCode, String code);

	public ParamConfDetail getByClassCodeCode(String classCode, String code);
}
