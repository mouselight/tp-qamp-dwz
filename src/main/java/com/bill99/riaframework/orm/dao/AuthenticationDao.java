package com.bill99.riaframework.orm.dao;

import java.util.Date;

import com.bill99.riaframework.orm.entity.Authentication;
import com.jeecms.common.hibernate3.BaseDao;

public interface AuthenticationDao extends BaseDao<Authentication> {

	public int deleteExpire(Date date);

	public Authentication getByUserId(String userId);

}
