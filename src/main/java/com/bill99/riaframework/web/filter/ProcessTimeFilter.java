package com.bill99.riaframework.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.bill99.rmca.common.util.Bill99Logger;

/**
 * 执行时间过滤器
 * 
 * @author
 * 
 */ 
public class ProcessTimeFilter implements Filter {
	protected final Bill99Logger logger = Bill99Logger.getLogger(ProcessTimeFilter.class);
	/**
	 * 请求执行开始时间
	 */
	public static final String START_TIME = "_start_time";

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		long time = System.currentTimeMillis();
		request.setAttribute(START_TIME, time);
		chain.doFilter(request, response);
		time = System.currentTimeMillis() - time;
		logger.debug("process in {} ms: {}" + time + request.getRequestURI());
	}

	public void init(FilterConfig arg0) throws ServletException {
	}
}
