package com.bill99.riaframework.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bill99.riaframework.domain.springmvc.SpringContextHolder;
import com.bill99.riaframework.orm.manager.ParamConfDetailService;
import com.bill99.riaframework.orm.manager.ParamConfService;

/**
 * 系统初始化监听器
 * 
 * @author leo
 * @date 2012-12-16 下午11:26:14
 */
public class SystemInitListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent sce) {

		ServletContext servletContext = sce.getServletContext();
		ParamConfDetailService paramConfDetailService = SpringContextHolder.getBean("paramConfDetailService");
		// 系统参数初始化.初始化至web容器中,页面以Application["sysPartModuleList"]获取
		// 模块类别
		servletContext.setAttribute("sysPartOptList", paramConfDetailService.getConfDetailList(ParamConfService.SYS_PART_OPT));
	}

	public void contextDestroyed(ServletContextEvent sce) {

	}

}
