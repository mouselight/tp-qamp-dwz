package com.bill99.riaframework.web.interseptor;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UrlPathHelper;

import com.bill99.riaframework.common.constants.Constants;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.domain.permission.RiaFrameworkThreadVariable;
import com.bill99.riaframework.domain.session.SessionProvider;
import com.bill99.riaframework.orm.entity.OperateTrace;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.AuthenticationService;
import com.bill99.riaframework.orm.manager.OperateTraceMng;
import com.bill99.riaframework.orm.manager.UserService;

/**
 * 上下文信息拦截器
 * 
 * 包括登录信息、权限信息、站点信息
 */
public class AdminContextInterceptor extends HandlerInterceptorAdapter {
	public static final String SITE_PARAM = "_site_id_param";
	public static final String SITE_COOKIE = "_site_id_cookie";
	public static final String PERMISSION_MODEL = "_permission_key";

	@Autowired
	private SessionProvider sessionProvider;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private UserService userService;
	@Autowired
	private OperateTraceMng operateTraceMng;

	private Long adminId;
	private boolean auth = true;
	private String[] excludeUrls;
	private String loginUrl;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		// 获得用户
		User user = null;
		if (adminId != null) {
			// 指定管理员（开发状态）
			user = userService.findById(adminId);
			if (user == null) {
				throw new IllegalStateException("User ID=" + adminId + " not found!");
			}
		} else {
			// 正常状态
			Long userId = authenticationService.retrieveUserIdFromSession(sessionProvider, request);
			if (userId != null) {
				user = userService.findById(userId);
			}
		}
		// 此时用户可以为null
		RiaFrameworkUtils.setUser(request, user);
		// User加入线程变量
		RiaFrameworkThreadVariable.setUser(user);

		String uri = getURI(request);
		// 不在验证的范围内
		if (exclude(uri)) {
			return true;
		}
		// 用户为null跳转到登陆页面
		if (user == null) {
			if (request.getRequestURI().contains("index.htm") || !request.getRequestURI().endsWith(".htm")) {
				response.sendRedirect(getLoginUrl(request));
			} else {
				AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("301", null, null, null, null, null);
				ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			}
			return false;
		}

		// 保存操作日志
		OperateTrace operateTrace = new OperateTrace();
		operateTrace.setUserName(user.getUsername());
		operateTrace.setUri(getLastUri(uri));
		operateTrace.setMethod(request.getMethod());
		operateTrace.setClientIp(request.getRemoteAddr());
		operateTrace.setSessionId(request.getRequestedSessionId());
		operateTraceMng.save(operateTrace);

		// admin不验证权限
		if (StringUtils.hasLength(user.getUsername()) && "admin".equals(user.getUsername())) {
			return true;
		}

		// 超级管理员不验证权限
		if (user.getIsAdmin() != null && user.getIsAdmin()) {
			// request.setAttribute(Constants.MESSAGE, "用户不是超级管理员");
			// response.sendError(HttpServletResponse.SC_FORBIDDEN);
			// return false;
			return true;
		}
		// 没有访问权限，提示无权限。
		if (auth && !permistionPass(uri, user.getPerms())) {
			request.setAttribute(Constants.MESSAGE, "用户无访问权限");
			AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("300", "用户无访问权限", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav) throws Exception {
		User user = RiaFrameworkUtils.getUser(request);
		// 不控制权限时perm为null，PermistionDirective标签将以此作为依据不处理权限问题。
		if (auth && user != null && !user.isSuper() && mav != null && mav.getModelMap() != null && mav.getViewName() != null && !mav.getViewName().startsWith("redirect:")) {
			mav.getModelMap().addAttribute(PERMISSION_MODEL, user.getPerms());
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		// Sevlet容器有可能使用线程池，所以必须手动清空线程变量。
		RiaFrameworkThreadVariable.removeUser();
	}

	private String getLoginUrl(HttpServletRequest request) {
		StringBuilder buff = new StringBuilder();
		if (loginUrl.startsWith("/")) {
			String ctx = request.getContextPath();
			if (StringUtils.hasLength(ctx)) {
				buff.append(ctx);
			}
		}
		buff.append(loginUrl);
		return buff.toString();
	}

	private boolean exclude(String uri) {
		if (excludeUrls != null) {
			for (String exc : excludeUrls) {
				if (uri.contains(exc)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean permistionPass(String uri, Set<String> perms) {
		String u = null;
		int i;
		for (String perm : perms) {
			if (uri.endsWith(perm)) {
				// 获得最后一个 '/' 的URI地址。
				i = uri.lastIndexOf("/");
				if (i == -1) {
					throw new RuntimeException("uri must start width '/':" + uri);
				}
				u = uri.substring(i + 1);
				// 操作型地址被禁止
				if (u.startsWith("o_")) {
					return false;
				}
				return true;
			}
		}
		return false;
	}

	private String getLastUri(String uri) {
		String url = null;
		int i = uri.lastIndexOf("/");
		if (i != -1) {
			url = uri.substring(i - 1 + 1);
		}
		return url;
	}

	/**
	 * 获得第三个路径分隔符的位置
	 * 
	 * @param request
	 * @throws IllegalStateException
	 *             访问路径错误，没有三(四)个'/'
	 */
	private static String getURI(HttpServletRequest request) throws IllegalStateException {
		UrlPathHelper helper = new UrlPathHelper();
		String uri = helper.getOriginatingRequestUri(request);
		return uri;
		// String ctxPath = helper.getOriginatingContextPath(request);
		// int start = 0, i = 0, count = 2;
		// if (!StringUtils.isBlank(ctxPath)) {
		// count++;
		// }
		// while (i < count && start != -1) {
		// start = uri.indexOf('/', start + 1);
		// i++;
		// }
		// if (start <= 0) {
		// throw new IllegalStateException("admin access path not like '/oms/jspgou/...' pattern: " + uri);
		// }
		// return uri.substring(start);
	}

	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public void setExcludeUrls(String[] excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

}
