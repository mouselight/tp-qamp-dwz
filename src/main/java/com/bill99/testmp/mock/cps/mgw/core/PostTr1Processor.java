package com.bill99.testmp.mock.cps.mgw.core;

public abstract interface PostTr1Processor
{
  public abstract String post(RequestTR1 paramRequestTR1)
    throws Exception;

  public abstract String post(String paramString1, String paramString2)
    throws Exception;
}