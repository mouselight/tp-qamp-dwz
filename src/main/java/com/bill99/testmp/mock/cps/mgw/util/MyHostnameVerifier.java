package com.bill99.testmp.mock.cps.mgw.util;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class MyHostnameVerifier implements HostnameVerifier {
	
	public boolean verify(String urlHostName, SSLSession session)
    {
        //System.out.println("Warning: URL Host: "+urlHostName+" vs. "+session.getPeerHost());
        return true;
 }

}
