package com.bill99.testmp.mock.cps.mgw.model;


public class ChangePswd {
	
    private String cardNo;

    private String merchantId;

    private String oldPswd;

    private String newPswd;

    private String responseCode;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}

	public String getOldPswd() {
		return oldPswd;
	}

	public void setOldPswd(String oldPswd) {
		if(oldPswd.trim().equals("")) { 
			return;
		}
		this.oldPswd = oldPswd;
	}

	public String getNewPswd() {
		return newPswd;
	}

	public void setNewPswd(String newPswd) {
		if(newPswd.trim().equals("")) { 
			return;
		}
		this.newPswd = newPswd;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		if(responseCode.trim().equals("")) { 
			return;
		}
		this.responseCode = responseCode;
	}
}
