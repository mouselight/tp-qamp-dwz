package com.bill99.testmp.mock.cps.mgw.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyStore;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import com.bill99.testmp.mock.cps.mgw.util.Base64Binrary;
import com.bill99.testmp.mock.cps.mgw.util.MyHostnameVerifier;
import com.bill99.testmp.mock.cps.mgw.util.MyX509TrustManager;
import com.bill99.testmp.mock.cps.mgw.util.PropFile;

public class PostTr1ProcessorImpl implements PostTr1Processor {

	static {
		MyHostnameVerifier hostnamever = new MyHostnameVerifier();
		HttpsURLConnection.setDefaultHostnameVerifier(hostnamever);
	}

	public String post(RequestTR1 req) throws Exception {
    File certFile = new File(req.getCertPath());
    KeyStore ks = KeyStore.getInstance("JKS");
    ks.load(new FileInputStream(certFile), 
      req.getCertPass().toCharArray());
    KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    kmf.init(ks, req.getCertPass().toCharArray());

    TrustManager[] tm = { new MyX509TrustManager() };

    SSLContext sslContext = SSLContext.getInstance("SSL");
    sslContext.init(kmf.getKeyManagers(), tm, null);

    SSLSocketFactory factory = sslContext.getSocketFactory();

    URL url = new URL(req.getUrl());
    
    HttpsURLConnection urlc = null;
    if(req.getUrl()!=null && req.getProxyPort()!=null) {
    	 InetSocketAddress addr = new InetSocketAddress(req.getProxyHost(),Integer.valueOf(req.getProxyPort()));  
	     Proxy proxy = new Proxy(Proxy.Type.HTTP, addr); 
	     urlc = (HttpsURLConnection)url.openConnection(proxy);
	     
	     if(req.getProxyPassword()!=null && req.getProxyUser()!=null) {
	    	 
	    	 final RequestTR1 reqClone = req;
	         // ���ô����������֤     
	         Authenticator auth = new Authenticator() {     
	             private PasswordAuthentication pa =      
	                 new PasswordAuthentication(reqClone.getProxyUser(), reqClone.getProxyPassword().toCharArray());     
	             @Override    
	             protected PasswordAuthentication getPasswordAuthentication() {     
	                 return pa;     
	             }     
	         };     
	         Authenticator.setDefault(auth);     
	     }
	     
    }else {
    	urlc = (HttpsURLConnection)url.openConnection();
    }
    
    urlc.setSSLSocketFactory(factory);
    urlc.setDoOutput(true);
    urlc.setDoInput(true);
    urlc.setReadTimeout(req.getTimeOut() * 10000);

    String authString = req.getMerchantId() + ":" + req.getPassword();
    String auth = "Basic " + Base64Binrary.encodeBase64Binrary(authString.getBytes());
    urlc.setRequestProperty("Authorization", auth);

    OutputStream out = urlc.getOutputStream();
    out.write(req.getXml().getBytes());
    out.flush();
    out.close();

    InputStream is = urlc.getInputStream();

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    byte[] receiveBuffer = new byte[2048];
    int readBytesSize = is.read(receiveBuffer);
    do {
      bos.write(receiveBuffer, 0, readBytesSize);
      readBytesSize = is.read(receiveBuffer); }
    while (readBytesSize != -1);

    String xmlRequest = new String(bos.toByteArray(), "UTF-8");
    return xmlRequest;
  }

  public String post(String propFile, String tr1XML) throws Exception
  {
    Properties props = PropFile.getProps(propFile);
    RequestTR1 req = new RequestTR1();

    req.setCertPass(props.getProperty("certPassword"));
    req.setCertPath(props.getProperty("certPath"));
    req.setMerchantId(props.getProperty("merchantId"));
    req.setUrl(props.getProperty("url"));
    req.setPassword(props.getProperty("password"));
    req.setTimeOut(Integer.valueOf(props.getProperty("timeout")).intValue());
    
    req.setXml(tr1XML);

    return post(req);
  }
}