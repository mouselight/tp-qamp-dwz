package com.bill99.testmp.mock.cps.mgw.model;

public class BatchRefund {
	
    private String interactiveStatus;
  
    private String amount;

    private String merchantId;
  
    private String settleMerchantId;
   
    private String terminalId;
 
    private String externalRefNumber;
  
    private String cardNo;

    private String customerId;

	public String getInteractiveStatus() {
		return interactiveStatus;
	}

	public void setInteractiveStatus(String interactiveStatus) {
		if(interactiveStatus.trim().equals("")) { 
			return;
		}
		this.interactiveStatus = interactiveStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		if(amount.trim().equals("")) { 
			return;
		}
		this.amount = amount;
	}

	public String getMerchantId() {
	
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}

	public String getSettleMerchantId() {
		return settleMerchantId;
	}

	public void setSettleMerchantId(String settleMerchantId) {
		if(settleMerchantId.trim().equals("")) { 
			return;
		}
		this.settleMerchantId = settleMerchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		if(terminalId.trim().equals("")) { 
			return;
		}
		this.terminalId = terminalId;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		if(externalRefNumber.trim().equals("")) { 
			return;
		}
		this.externalRefNumber = externalRefNumber;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		if(customerId.trim().equals("")) { 
			return;
		}
		this.customerId = customerId;
	}
  

}
