package com.bill99.testmp.mock.cps.mgw.model;

public class VaCard {
	
	private String cardNo;
	
	private String pin;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		if(pin.trim().equals("")) { 
			return;
		}
		this.pin = pin;
	}
}
