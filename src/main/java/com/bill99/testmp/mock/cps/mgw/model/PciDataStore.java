package com.bill99.testmp.mock.cps.mgw.model;

public class PciDataStore {
	private String merchantId;
	private String customerId;
	private String cardHolderName;
	private String idType;
	private String cardHolderId;
	private String cardHolderIdEn;
	private String pan;
	private String panEn;
	private String storablePan;
	private String bankId;
	private String expiredDate;
	private String phoneNO;
	private String shortPhone;
	private String cvv2;
	private String cvv2En;
	private String amount;
	private String validateFlag;
	private String sendCodeFlag;
	private String responseCode;
	private String responseTextMessage;
	private String externalRefNumber;
	private String supCardFlag;
	private String cardType;

	public String getSupCardFlag() {
		return supCardFlag;
	}

	public void setSupCardFlag(String supCardFlag) {
		this.supCardFlag = supCardFlag;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getStorablePan() {
		return storablePan;
	}

	public void setStorablePan(String storablePan) {
		this.storablePan = storablePan;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getPhoneNO() {
		return phoneNO;
	}

	public void setPhoneNO(String phoneNO) {
		this.phoneNO = phoneNO;
	}

	public String getShortPhone() {
		return shortPhone;
	}

	public void setShortPhone(String shortPhone) {
		this.shortPhone = shortPhone;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseTextMessage() {
		return responseTextMessage;
	}

	public void setResponseTextMessage(String responseTextMessage) {
		this.responseTextMessage = responseTextMessage;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getValidateFlag() {
		return validateFlag;
	}

	public void setValidateFlag(String validateFlag) {
		this.validateFlag = validateFlag;
	}

	public String getSendCodeFlag() {
		return sendCodeFlag;
	}

	public void setSendCodeFlag(String sendCodeFlag) {
		this.sendCodeFlag = sendCodeFlag;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardHolderIdEn() {
		return cardHolderIdEn;
	}

	public void setCardHolderIdEn(String cardHolderIdEn) {
		this.cardHolderIdEn = cardHolderIdEn;
	}

	public String getPanEn() {
		return panEn;
	}

	public void setPanEn(String panEn) {
		this.panEn = panEn;
	}

	public String getCvv2En() {
		return cvv2En;
	}

	public void setCvv2En(String cvv2En) {
		this.cvv2En = cvv2En;
	}
	
}
