package com.bill99.testmp.mock.cps.mgw.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TxnController {

	/*private MgwCertGenerator mgwCertGenerator;

	private PkiService pkiService;

	private MerchantService merchantService;*/
@RequestMapping("/cps/mgw/productpage/cpsproductlist.htm")	
public String productlist(HttpServletRequest request,HttpServletResponse response ,ModelMap model) throws Exception {

	return "mock/cps/mgw/productpage/cpsproductlist"; 
}
	
	
@RequestMapping("/cps/mgw/productpage/cpsproductlistlogin.htm")
public String login(HttpServletRequest request,HttpServletResponse response ,ModelMap model) throws Exception {
	HttpSession session = request.getSession();
	String merchant = request.getParameter("merchant");

	String password = request.getParameter("password");
	String envDir = request.getParameter("envDir");

	session.setAttribute("merchant", merchant);
	session.setAttribute("password", password);
	session.setAttribute("envDir", envDir);


	// 动态获取商户证书
	String reStr = "env/mock/cps/mgw/" + envDir;
	Resource resource = new ClassPathResource(reStr);
	File file = resource.getFile();
	File jksFile = null;
	if (file != null) {
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			File filetemp = files[i];
			if (filetemp.getName().length() >= 15) {
				String merchantId = filetemp.getName().substring(0, 15);
				String filePr = filetemp.getName().substring(
						filetemp.getName().indexOf(".") + 1,
						filetemp.getName().length());
				if (merchantId.equals(merchant) && "jks".equals(filePr)) {
					jksFile = filetemp;
					break;
				}
			}
		}
	}

	Resource resourceFile = null;
	if (jksFile == null) {
		resourceFile = new ClassPathResource(reStr + "/VPOS_cnp");
	} else {
		resourceFile = new ClassPathResource(reStr + "/"
				+ jksFile.getName());
	}

	Resource re = new ClassPathResource(reStr + "/mgwtest.props");

	FileReader read = new FileReader(re.getFile());
	BufferedReader bufRe = new BufferedReader(read);
	String tempString = null;
	while ((tempString = bufRe.readLine()) != null) {
		if (tempString.indexOf("host") >= 0) {
			String urlTemp = tempString.split("=")[1];
			session.setAttribute("url", urlTemp);
		}

		if (tempString.indexOf("certPassword") >= 0) {
			String certPasswordTemp = tempString.split("=")[1];
			session.setAttribute("certPassword", certPasswordTemp);
		}

		if (tempString.indexOf("port") >= 0) {
			String portTemp = tempString.split("=")[1];
			session.setAttribute("port", portTemp);
		}

		if (tempString.indexOf("timeout") >= 0) {
			String timeoutTemp = tempString.split("=")[1];
			session.setAttribute("timeout", timeoutTemp);
		}
	}

	session.setAttribute("jksfile", resourceFile);
	model.addAttribute("msg","商户信息设置成功");
	
	return "mock/cps/mgw/productpage/cpsproductlist";
}



}


