package com.bill99.testmp.mock.cps.mgw.model;

public class BindCard {
	
	private String cardNo;
	
	private String orgPartyId;
	
	private String phoneNo;

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		if(phoneNo.trim().equals("")) { 
			return;
		}
		this.phoneNo = phoneNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getOrgPartyId() {
		return orgPartyId;
	}

	public void setOrgPartyId(String orgPartyId) {
		if(orgPartyId.trim().equals("")) { 
			return;
		}
		this.orgPartyId = orgPartyId;
	}


}
