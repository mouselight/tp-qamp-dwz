package com.bill99.testmp.mock.cps.mgw.model;

public class SentQueryTxn {
	
	
	private String externalRefNumber;
	
	private String refNumber;
	
	private String merchantId;
	
	private String settleMerchantId;

	private String terminalId;
	
	private String txnDate;
	
	private String settleDate;
	
	private String cardNo;
	
	private String txnType;
	
	private String txnStatus;
	
	private String chanTypes;

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		if(txnStatus.trim().equals("")) { 
			return;
		}
		this.txnStatus = txnStatus;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		if(externalRefNumber.trim().equals("")) { 
			return;
		}
		this.externalRefNumber = externalRefNumber;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		if(refNumber.trim().equals("")) { 
			return;
		}
		this.refNumber = refNumber;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		if(txnType.trim().equals("")) { 
			return;
		}
		this.txnType = txnType;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}

	public String getSettleMerchantId() {
		return settleMerchantId;
	}

	public void setSettleMerchantId(String settleMerchantId) {
		if(settleMerchantId.trim().equals("")) { 
			return;
		}
		this.settleMerchantId = settleMerchantId;
	}
	
	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		if(terminalId.trim().equals("")) { 
			return;
		}
		this.terminalId = terminalId;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		if(txnDate.trim().equals("")) { 
			return;
		}
		this.txnDate = txnDate;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		if(settleDate.trim().equals("")) { 
			return;
		}
		this.settleDate = settleDate;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getChanTypes() {
		return chanTypes;
	}

	public void setChanTypes(String chanTypes) {
		this.chanTypes = chanTypes;
	}
}
