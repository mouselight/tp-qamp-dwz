package com.bill99.testmp.mock.cps.mgw.model;

public class SentPostTxn {
	//�̻�˽����
	private String extInfo;
	
	//У���
	private String schId;
	
	//��ˮ��
	private String orderId;
	
	//֧������
	private String payDate;
	
	//��������
	private String txnType;
	
	//���
	private String amount;
	
	//�̻���
	private String merchantId;
	
	//�ն˺�
	private String terminalId;
	
	//����ʱ��
	private String entryTime;
	
	//�ⲿ���ٱ��
	private String externalRefNumber;
	
	//ϵͳ�ο���
	private String origRefNumber;
	
	
	private String MerId;
	private String BusiId;
	private String GateId;
	private String Param3;
	private String Param4;
	private String Param5;
	private String CuryId;
	private String RefAmt;
	private String MerRefId;
	private String Priv1;
	
	public String toString1() {
		StringBuffer sb = new StringBuffer();
		sb.append("txnType=").append(this.txnType).append("&").append("amount=").append(this.amount).append("&")
				.append("terminalId=").append(this.terminalId).append("&").append("merchantId=")
				.append(this.merchantId).append("&").append("entryTime=").append(this.entryTime).append("&").append(
						"externalRefNumber=").append(this.externalRefNumber).append("&").append("origRefNumber=")
				.append(this.origRefNumber).append("&").append("schId=").append(this.schId).append("&").append(
						"orderId=").append(this.orderId).append("&").append("payDate=").append(this.payDate)
				.append("&").append("extInfo=").append(this.extInfo);

		return sb.toString();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("MerId=").append(this.MerId).append("&").append("BusiId=").append(this.BusiId).append("&").append(
				"GateId=").append(this.GateId).append("&").append("Param3=").append(this.Param3).append("&").append(
				"Param4=").append(this.Param4).append("&").append("Param5=").append(this.Param5).append("&").append(
				"CuryId=").append(this.CuryId).append("&").append("RefAmt=").append(this.RefAmt).append("&").append(
				"MerRefId=").append(this.MerRefId).append("&").append("Priv1=").append(this.Priv1);

		return sb.toString();
	}
	
	/**
	 * @return the extInfo
	 */
	public String getExtInfo() {
		return extInfo;
	}

	/**
	 * @param extInfo the extInfo to set
	 */
	public void setExtInfo(String extInfo) {
		this.extInfo = extInfo;
	}

	/**
	 * @return the schId
	 */
	public String getSchId() {
		return schId;
	}

	/**
	 * @param schId the schId to set
	 */
	public void setSchId(String schId) {
		this.schId = schId;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the payDate
	 */
	public String getPayDate() {
		return payDate;
	}

	/**
	 * @param payDate the payDate to set
	 */
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the terminalId
	 */
	public String getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the entryTime
	 */
	public String getEntryTime() {
		return entryTime;
	}

	/**
	 * @param entryTime the entryTime to set
	 */
	public void setEntryTime(String entryTime) {
		this.entryTime = entryTime;
	}

	/**
	 * @return the externalRefNumber
	 */
	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	/**
	 * @param externalRefNumber the externalRefNumber to set
	 */
	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}

	/**
	 * @return the origRefNumber
	 */
	public String getOrigRefNumber() {
		return origRefNumber;
	}

	/**
	 * @param origRefNumber the origRefNumber to set
	 */
	public void setOrigRefNumber(String origRefNumber) {
		this.origRefNumber = origRefNumber;
	}
	
	/**
	 * @return the merId
	 */
	public String getMerId() {
		return MerId;
	}

	/**
	 * @param merId the merId to set
	 */
	public void setMerId(String MerId) {
		this.MerId = MerId;
	}

	/**
	 * @return the busiId
	 */
	public String getBusiId() {
		return BusiId;
	}

	/**
	 * @param busiId the busiId to set
	 */
	public void setBusiId(String BusiId) {
		this.BusiId = BusiId;
	}

	/**
	 * @return the gateId
	 */
	public String getGateId() {
		return GateId;
	}

	/**
	 * @param gateId the gateId to set
	 */
	public void setGateId(String GateId) {
		this.GateId = GateId;
	}

	/**
	 * @return the param3
	 */
	public String getParam3() {
		return Param3;
	}

	/**
	 * @param param3 the param3 to set
	 */
	public void setParam3(String Param3) {
		this.Param3 = Param3;
	}

	/**
	 * @return the param4
	 */
	public String getParam4() {
		return Param4;
	}

	/**
	 * @param param4 the param4 to set
	 */
	public void setParam4(String Param4) {
		this.Param4 = Param4;
	}

	/**
	 * @return the param5
	 */
	public String getParam5() {
		return Param5;
	}

	/**
	 * @param param5 the param5 to set
	 */
	public void setParam5(String Param5) {
		this.Param5 = Param5;
	}

	/**
	 * @return the curyId
	 */
	public String getCuryId() {
		return CuryId;
	}

	/**
	 * @param curyId the curyId to set
	 */
	public void setCuryId(String CuryId) {
		this.CuryId = CuryId;
	}

	/**
	 * @return the refAmt
	 */
	public String getRefAmt() {
		return RefAmt;
	}

	/**
	 * @param refAmt the refAmt to set
	 */
	public void setRefAmt(String RefAmt) {
		this.RefAmt = RefAmt;
	}

	/**
	 * @return the merRefId
	 */
	public String getMerRefId() {
		return MerRefId;
	}

	/**
	 * @param merRefId the merRefId to set
	 */
	public void setMerRefId(String MerRefId) {
		this.MerRefId = MerRefId;
	}

	/**
	 * @return the priv1
	 */
	public String getPriv1() {
		return Priv1;
	}

	/**
	 * @param priv1 the priv1 to set
	 */
	public void setPriv1(String Priv1) {
		this.Priv1 = Priv1;
	}
}
