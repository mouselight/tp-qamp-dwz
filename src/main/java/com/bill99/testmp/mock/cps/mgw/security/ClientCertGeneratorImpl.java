package com.bill99.testmp.mock.cps.mgw.security;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERInputStream;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JDKPKCS12KeyStore;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.springframework.core.io.Resource;

import steel.share.clientcert.ClientCert;

/**
 * @project: Steel
 * @description:
 * @author: xingyu.zhang
 * @create_time: May 30, 2008
 * @modify_time: May 30, 2008
 */
public class ClientCertGeneratorImpl implements ClientCertGenerator {
	/**
	 * ��֤���ļ�·��
	 */
	private Resource rootCertFilePath;

	/**
	 * ����Կ�ļ�·��
	 */
	private Resource rootKeyFilePath;

	/**
	 * ֤�����к�
	 */
	private int serialNumber;

	public void genClientCert(ClientCert clientCert, OutputStream fileOutputStream) {
		try {
			// �����֤���ļ�
			X509Certificate rootCert = loadRootCertificate();
			// ������Կ�ļ�
			PrivateKey rootPrivateKey = loadRootPrivateKeyByKeyFile();
			// �����Կ��
			KeyPair clientKeyPair = genClientKeyPair();
			// ǩ��֤��
			X509Certificate signedCert = signCert(rootCert.getPublicKey(), rootPrivateKey, rootCert, clientKeyPair,
					clientCert);

			// ���PFX�ļ�
			PKCS12BagAttributeCarrier bagAttributeCarrier = (PKCS12BagAttributeCarrier) signedCert;
			bagAttributeCarrier.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName, new DERBMPString(
					clientCert.getCommonName() + "'s key"));
			bagAttributeCarrier.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
					new SubjectKeyIdentifierStructure(clientKeyPair.getPublic()));

			// ֤����
			Certificate[] certChain = new Certificate[1];
			certChain[0] = signedCert;

			JDKPKCS12KeyStore ks = new JDKPKCS12KeyStore.BCPKCS12KeyStore();
			ks.engineSetKeyEntry(clientCert.getCommonName(), clientKeyPair.getPrivate(), clientCert.getPassword()
					.toCharArray(), certChain);
			ks.engineStore(fileOutputStream, clientCert.getPassword().toCharArray());

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * ���ظ�֤���ļ�
	 *
	 * @param certFilePath
	 * @return
	 */
	private X509Certificate loadRootCertificate() {
		InputStream certIn = null;
		try {
			// ��ȡ��֤��
			CertificateFactory fac = CertificateFactory.getInstance("X.509");

			return (X509Certificate) fac.generateCertificate(rootCertFilePath.getInputStream());
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			if (certIn != null) {
				try {
					certIn.close();
				} catch (IOException ie) {
				}
			}
		}
	}

	/**
	 * ͨ�����Կ�ļ�������Կ
	 *
	 * @param keyFilePath
	 *            ��Կ�ļ�·��
	 * @return
	 */
	private PrivateKey loadRootPrivateKeyByKeyFile() {
		BufferedInputStream privateIn = null;
		ByteArrayOutputStream privateByteOut = null;
		try {
			// ��ȡ��֤��˽Կ�ļ���תΪ�ֽ�����
			privateIn = (BufferedInputStream) rootKeyFilePath.getInputStream();
			privateByteOut = new ByteArrayOutputStream();
			int len = 0;
			while ((len = privateIn.read()) != -1) {
				privateByteOut.write(len);
			}
			byte[] privateBytes = privateByteOut.toByteArray();
			// ����ת�벢��ȡ��˽Կ
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey rootPrivateKey = keyFactory.generatePrivate(keySpec);

			return rootPrivateKey;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			if (privateIn != null) {
				try {
					privateIn.close();
				} catch (IOException ie) {
				}
			}
			if (privateByteOut != null) {
				try {
					privateByteOut.close();
				} catch (IOException ie) {
				}
			}
		}
	}

	/**
	 * ��ɿͻ�����Կ��
	 *
	 * @return
	 */
	private KeyPair genClientKeyPair() {
		try {
			// ����ǩ��,���client˽����Կ
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(2048);

			return generator.generateKeyPair();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 *
	 * ǩ��֤��
	 *
	 * @param rootPublicKey��֤�鹫Կ
	 * @param rootPrivateKey
	 *            ��֤��˽Կ
	 * @param certificate
	 *            ��֤��
	 * @param clientKeyPair
	 *            �ͻ�����Կ��
	 * @param clientCert
	 *            ֤�����
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private X509Certificate signCert(PublicKey rootPublicKey, PrivateKey rootPrivateKey, X509Certificate certificate,
			KeyPair clientKeyPair, ClientCert clientCert) throws Exception {

		Security.addProvider(new BouncyCastleProvider());

		// �ͻ���ǩ����Ϣ
		Hashtable attrs = new Hashtable();
		Vector order = new Vector();

		attrs.put(X509Principal.C, clientCert.getCountryCode());
		attrs.put(X509Principal.O, clientCert.getOrganizationCode());
		attrs.put(X509Principal.OU, clientCert.getOrganizationalName());
		attrs.put(X509Principal.CN, clientCert.getCommonName());
		attrs.put(X509Principal.L, clientCert.getCityName());
		attrs.put(X509Principal.ST, clientCert.getProvinceName());

		order.addElement(X509Principal.CN);
		order.addElement(X509Principal.OU);
		order.addElement(X509Principal.O);
		order.addElement(X509Principal.L);
		order.addElement(X509Principal.ST);
		order.addElement(X509Principal.C);

		X509V3CertificateGenerator _v3CertGenerator = new X509V3CertificateGenerator();

		_v3CertGenerator.reset();

		Date beginDate = new Date();
		// ֤�����к�
		_v3CertGenerator.setSerialNumber(BigInteger.valueOf(serialNumber));
		// �䷢��
		_v3CertGenerator.setIssuerDN(certificate.getIssuerX500Principal());
		// ��Ч����
		_v3CertGenerator.setNotBefore(new Date());
		// ʧЧ����
		_v3CertGenerator.setNotAfter(new Date(beginDate.getTime() + 365*10 * 24 * 60 * 60 * 1000L));// 730����Ч��
		// ʹ����
		_v3CertGenerator.setSubjectDN(new X509Principal(order, attrs));
		_v3CertGenerator.setPublicKey(clientKeyPair.getPublic());
		_v3CertGenerator.setSignatureAlgorithm("SHA1withRSA");

		// �ͻ���֤����չ��Ϣ
		// ʹ������Ϣ
		_v3CertGenerator.addExtension(X509Extensions.SubjectKeyIdentifier, false,
				createSubjectKeyIdentifier(clientKeyPair.getPublic()));
		// �䷢����Ϣ
		_v3CertGenerator.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
				createAuthorityKeyIdentifier(rootPublicKey));
		_v3CertGenerator.addExtension(X509Extensions.BasicConstraints, false, new BasicConstraints(false));

		// ǩ�����֤��
		X509Certificate cert = _v3CertGenerator.generate(rootPrivateKey, "BC");

		// ����ɵ�֤�����У��
		cert.checkValidity(new Date());
		cert.verify(rootPublicKey);

		return cert;
	}

	/**
	 * ֤��䷢���ʶ
	 *
	 * @param rootPublicKey
	 *            ��֤�鹫Կ
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public AuthorityKeyIdentifier createAuthorityKeyIdentifier(PublicKey rootPublicKey) {
		try {
			ByteArrayInputStream bIn = new ByteArrayInputStream(rootPublicKey.getEncoded());
			SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new DERInputStream(bIn).readObject());
			return new AuthorityKeyIdentifier(info);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * ʹ���߱�ʶ
	 *
	 * @param clientPublicKey
	 *            �ͻ��˹�Կ
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public SubjectKeyIdentifier createSubjectKeyIdentifier(PublicKey clientPublicKey) {
		try {
			ByteArrayInputStream bIn = new ByteArrayInputStream(clientPublicKey.getEncoded());
			SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new DERInputStream(bIn).readObject());
			return new SubjectKeyIdentifier(info);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Resource getRootCertFilePath() {
		return rootCertFilePath;
	}

	public void setRootCertFilePath(Resource rootCertFilePath) {
		this.rootCertFilePath = rootCertFilePath;
	}

	public Resource getRootKeyFilePath() {
		return rootKeyFilePath;
	}

	public void setRootKeyFilePath(Resource rootKeyFilePath) {
		this.rootKeyFilePath = rootKeyFilePath;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

}

