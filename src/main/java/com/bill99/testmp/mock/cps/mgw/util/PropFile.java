package com.bill99.testmp.mock.cps.mgw.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropFile
{
  private static Properties props = null;

  public static synchronized Properties getProps(String propFile)
    throws Exception
  {
    if (props == null) {
      props = new Properties();
      InputStream in = null;
      try {
        in = new FileInputStream(propFile);
        props.load(in);
        return props;
      } finally {
        if (in != null) {
          in.close();
        }
      }
    }
    return props;
  }
}