package com.bill99.testmp.mock.cps.mgw.model;

public class DynamicPin {
	private String cardNo;
	
	private String phoneNo;

	private String amount;
	
	private String externalRefNumber;
	
	private String merchantId;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		if(phoneNo.trim().equals("")) { 
			return;
		}
		this.phoneNo = phoneNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		if(amount.trim().equals("")) { 
			return;
		}
		this.amount = amount;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		if(externalRefNumber.trim().equals("")) { 
			return;
		}
		this.externalRefNumber = externalRefNumber;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}
	
	
}
