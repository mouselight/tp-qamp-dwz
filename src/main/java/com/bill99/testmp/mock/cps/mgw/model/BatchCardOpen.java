package com.bill99.testmp.mock.cps.mgw.model;

public class BatchCardOpen {
	
	private String merchantId;
	
	
	private String terminalId;
	
	private String externalRefNumber;
	
	private String voucherNo;
	
	private String comments;
	
	private String cardHolderName;
	
	private String sex;
	
	private String cardholderId;
	
	private String idType;
	
	private String phoneNo;
	
	private String email;
	
	private String zipCode;
	
	private String address;
	
	private String ext1;
	
	private String ext2;
	
	private String cardOpen1;
	
	private String cardOpen2;
	
	private String cardOpen3;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		if(terminalId.trim().equals("")) { 
			return;
		}
		this.terminalId = terminalId;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		if(externalRefNumber.trim().equals("")) { 
			return;
		}
		this.externalRefNumber = externalRefNumber;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		if(voucherNo.trim().equals("")) { 
			return;
		}
		this.voucherNo = voucherNo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		if(comments.trim().equals("")) { 
			return;
		}
		this.comments = comments;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		if(cardHolderName.trim().equals("")) { 
			return;
		}
		this.cardHolderName = cardHolderName;
	}

	public String getSex() {
		return sex;
	}

	
	public void setSex(String sex) {
		if(sex.trim().equals("")) { 
			return;
		}
		this.sex = sex;
	}

	public String getCardholderId() {
		return cardholderId;
	}

	public void setCardholderId(String cardholderId) {
		if(cardholderId.trim().equals("")) { 
			return;
		}
		this.cardholderId = cardholderId;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		if(idType.trim().equals("")) { 
			return;
		}
		this.idType = idType;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		if(phoneNo.trim().equals("")) { 
			return;
		}
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(email.trim().equals("")) { 
			return;
		}
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		if(zipCode.trim().equals("")) { 
			return;
		}
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if(address.trim().equals("")) { 
			return;
		}
		this.address = address;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		if(ext1.trim().equals("")) { 
			return;
		}
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		if(ext2.trim().equals("")) { 
			return;
		}
		this.ext2 = ext2;
	}

	public String getCardOpen1() {
		return cardOpen1;
	}

	public void setCardOpen1(String cardOpen1) {
		if(cardOpen1.trim().equals("")) { 
			return;
		}
		this.cardOpen1 = cardOpen1;
	}

	public String getCardOpen2() {
		return cardOpen2;
	}

	public void setCardOpen2(String cardOpen2) {
		if(cardOpen2.trim().equals("")) { 
			return;
		}
		this.cardOpen2 = cardOpen2;
	}

	public String getCardOpen3() {
		return cardOpen3;
	}

	public void setCardOpen3(String cardOpen3) {
		if(cardOpen3.trim().equals("")) { 
			return;
		}
		this.cardOpen3 = cardOpen3;
	}
}
