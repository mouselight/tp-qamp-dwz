package com.bill99.testmp.mock.cps.mgw.model;

// TODO: Auto-generated Javadoc
/**
 * The Class SentRequest.
 */
public class SentRequest{
	
	/** ��������. */
	private String txnType;
	
	/** ��Ϣ״̬. */
	private String interactiveStatus;
	
	/** ���ܿ���. */
	private String cardNoEn;
	
	/** ����. */
	private String cardNo;
	
	/** ����Ч��. */
	private String expiredDateEn;
	
	/** Ч��. */
	private String expiredDate;
	
	/** The cvv2 en. */
	private String cvv2En;
	
	/** The cvv2. */
	private String cvv2;
	
	/** The amount. */
	private String amount;
	
	/** The merchant id. */
	private String  merchantId;
	
	/** The settle merchant id. */
	private String settleMerchantId;
	
	/** The terminal id. */
	private String terminalId;
	
	/** The card holder name. */
	private String cardHolderName;
	
	/** The card holder id. */
	private String cardHolderId;
	
	/** The entry time. */
	private String entryTime;
	
	/** The external ref number. */
	private String externalRefNumber;
	
	/** The customer id. */
	private String customerId;
	
	/** The issuer country. */
	private String issuerCountry;
	
	/** The site type. */
	private String siteType;
	
	/** The site id. */
	private String siteID;
	
	/** The term in months. */
	private String termInMonths;
	
	/** The tr3 url. */
	private String tr3Url;
	
	/** The orig ref number. */
	private String origRefNumber;
	
	/** The ref number. */
	private String refNumber;
	
	/** The authorization code. */
	private String authorizationCode;
	
	/** The orignal txn type. */
	private String orignalTxnType;
	
	/** The pin. */
	private String pin;
	
	/** The dyn pin. */
	private String dynPin;
	
	/** The sp flag. */
	private String spFlag;
	
	/** The serial no. */
	private String serialNo;

	/** The ext. */
	private String ext;
	
	/** The ext1. */
	private String ext1;
	
	
	/** The phone. */
	private String phone;
	
	/** The id type. */
	private String idType;
	
	/** The order secret no. */
	private String orderSecretNo;
	
	/** The code01. */
	private String code01;
	
	/** The prod info. */
	private String prodInfo;
	
	/** The org party id. */
	private String orgPartyId;
	
	/** The valid code. */
	private String validCode;
    
    /** The save pci flag. */
    private String savePciFlag;
    
    /** The token. */
    private String token;
    
    /** The pay batch. */
    private String payBatch;
    
    /** The storable card no. */
    private String storableCardNo;
    
    /** The prod info sms. */
    private String prodInfoSms;
    
    /** The rifle data. */
    private String rifleData;//������
    
    /** The sup card flag. */
    private String supCardFlag;
    
    /** The mi ext. */
    private String miExt;
    
    /** The card type. */
    private String cardType;
    
    /** The bank id. */
    private String bankId;
    
    /** The card holder id en. */
    private String cardHolderIdEn;
    
    /** The cmd code. */
    private String cmdCode;
    
    /** The point amt. */
    private String pointAmt;
    
    /** The point fee amt. */
    private String pointFeeAmt;
    
    /** The customer point account. */
    private String customerPointAccount;
    
    /** The point. */
    private String point;
    
    /** The point fee. */
    private String pointFee;
    
    /** extMap. */
    private String extMapStr;
    

	/**
	 * Gets the card holder id en.
	 *
	 * @return the card holder id en
	 */
	public String getCardHolderIdEn() {
		return cardHolderIdEn;
	}

	/**
	 * Sets the card holder id en.
	 *
	 * @param cardHolderIdEn the new card holder id en
	 */
	public void setCardHolderIdEn(String cardHolderIdEn) {
		this.cardHolderIdEn = cardHolderIdEn;
	}

	/**
	 * Gets the bank id.
	 *
	 * @return the bank id
	 */
	public String getBankId() {
		return bankId;
	}

	/**
	 * Sets the bank id.
	 *
	 * @param bankId the new bank id
	 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * Sets the card type.
	 *
	 * @param cardType the new card type
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * Gets the mi ext.
	 *
	 * @return the mi ext
	 */
	public String getMiExt() {
		return miExt;
	}

	/**
	 * Sets the mi ext.
	 *
	 * @param miExt the new mi ext
	 */
	public void setMiExt(String miExt) {
		this.miExt = miExt;
	}

	/**
	 * Gets the sup card flag.
	 *
	 * @return the sup card flag
	 */
	public String getSupCardFlag() {
		return supCardFlag;
	}

	/**
	 * Sets the sup card flag.
	 *
	 * @param supCardFlag the new sup card flag
	 */
	public void setSupCardFlag(String supCardFlag) {
		this.supCardFlag = supCardFlag;
	} 
    
	/**
	 * Gets the rifle data.
	 *
	 * @return the rifle data
	 */
	public String getRifleData() {
		return rifleData;
	}



	/**
	 * Sets the rifle data.
	 *
	 * @param rifleData the new rifle data
	 */
	public void setRifleData(String rifleData) {
		this.rifleData = rifleData;
	}



	/**
	 * Gets the org party id.
	 *
	 * @return the orgPartyId
	 */
	public String getOrgPartyId() {
		return orgPartyId;
	}
	
	

	/**
	 * Gets the valid code.
	 *
	 * @return the valid code
	 */
	public String getValidCode() {
		return validCode;
	}



	/**
	 * Sets the valid code.
	 *
	 * @param validCode the new valid code
	 */
	public void setValidCode(String validCode) {
		this.validCode = validCode;
	}



	/**
	 * Gets the save pci flag.
	 *
	 * @return the save pci flag
	 */
	public String getSavePciFlag() {
		return savePciFlag;
	}



	/**
	 * Sets the save pci flag.
	 *
	 * @param savePciFlag the new save pci flag
	 */
	public void setSavePciFlag(String savePciFlag) {
		this.savePciFlag = savePciFlag;
	}



	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}



	/**
	 * Sets the token.
	 *
	 * @param token the new token
	 */
	public void setToken(String token) {
		this.token = token;
	}



	/**
	 * Gets the pay batch.
	 *
	 * @return the pay batch
	 */
	public String getPayBatch() {
		return payBatch;
	}



	/**
	 * Sets the pay batch.
	 *
	 * @param payBatch the new pay batch
	 */
	public void setPayBatch(String payBatch) {
		this.payBatch = payBatch;
	}



	/**
	 * Gets the storable card no.
	 *
	 * @return the storable card no
	 */
	public String getStorableCardNo() {
		return storableCardNo;
	}



	/**
	 * Sets the storable card no.
	 *
	 * @param storableCardNo the new storable card no
	 */
	public void setStorableCardNo(String storableCardNo) {
		this.storableCardNo = storableCardNo;
	}



	/**
	 * Sets the org party id.
	 *
	 * @param orgPartyId the orgPartyId to set
	 */
	public void setOrgPartyId(String orgPartyId) {
		this.orgPartyId = orgPartyId;
	}


	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	
	/**
	 * Gets the id type.
	 *
	 * @return the id type
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Sets the id type.
	 *
	 * @param idType the new id type
	 */
	public void setIdType(String idType) {
		if(idType.trim().equals("")) { 
			return;
		}
		this.idType = idType;
	}

	/**
	 * Gets the ext.
	 *
	 * @return the ext
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * Sets the ext.
	 *
	 * @param ext the new ext
	 */
	public void setExt(String ext) {
		if(ext.trim().equals("")) { 
			return;
		}
		this.ext = ext;
	}

	/**
	 * Gets the ext1.
	 *
	 * @return the ext1
	 */
	public String getExt1() {
		return ext1;
	}

	/**
	 * Sets the ext1.
	 *
	 * @param ext1 the new ext1
	 */
	public void setExt1(String ext1) {
		if(ext1.trim().equals("")) { 
			return;
		}
		this.ext1 = ext1;
	}
	
	/**
	 * Gets the dyn pin.
	 *
	 * @return the dyn pin
	 */
	public String getDynPin() {
		return dynPin;
	}

	/**
	 * Sets the dyn pin.
	 *
	 * @param dynPin the new dyn pin
	 */
	public void setDynPin(String dynPin) {
		if(dynPin.trim().equals("")) { 
			return;
		}
		this.dynPin = dynPin;
	}

	/**
	 * Gets the sp flag.
	 *
	 * @return the sp flag
	 */
	public String getSpFlag() {
		return spFlag;
	}

	/**
	 * Sets the sp flag.
	 *
	 * @param spFlag the new sp flag
	 */
	public void setSpFlag(String spFlag) {
		if(spFlag.trim().equals("")) { 
			return;
		}
		this.spFlag = spFlag;
	}

	/**
	 * Gets the serial no.
	 *
	 * @return the serial no
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * Sets the serial no.
	 *
	 * @param serialNo the new serial no
	 */
	public void setSerialNo(String serialNo) {
		if(serialNo.trim().equals("")) { 
			return;
		}
		this.serialNo = serialNo;
	}

	/**
	 * Gets the pin.
	 *
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * Sets the pin.
	 *
	 * @param pin the new pin
	 */
	public void setPin(String pin) {
		if(pin.trim().equals("")) { 
			return;
		}
		this.pin = pin;
	}

	/**
	 * Gets the orignal txn type.
	 *
	 * @return the orignal txn type
	 */
	public String getOrignalTxnType() {
		return orignalTxnType;
	}

	/**
	 * Sets the orignal txn type.
	 *
	 * @param orignalTxnType the new orignal txn type
	 */
	public void setOrignalTxnType(String orignalTxnType) {
		if(orignalTxnType.trim().equals("")) { 
			return;
		}
		this.orignalTxnType = orignalTxnType;
	}
 
	/**
	 * Gets the authorization code.
	 *
	 * @return the authorization code
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	/**
	 * Sets the authorization code.
	 *
	 * @param authorizationCode the new authorization code
	 */
	public void setAuthorizationCode(String authorizationCode) {
		if(authorizationCode.trim().equals("")) { 
			return;
		}
		this.authorizationCode = authorizationCode;
	}

	/**
	 * Gets the orig ref number.
	 *
	 * @return the orig ref number
	 */
	public String getOrigRefNumber() {
		return origRefNumber;
	}

	/**
	 * Sets the orig ref number.
	 *
	 * @param origRefNumber the new orig ref number
	 */
	public void setOrigRefNumber(String origRefNumber) {
		if(origRefNumber.trim().equals("")) { 
			return;
		}
		this.origRefNumber = origRefNumber;
	}

	/**
	 * Gets the ref number.
	 *
	 * @return the ref number
	 */
	public String getRefNumber() {
		return refNumber;
	}

	/**
	 * Sets the ref number.
	 *
	 * @param refNumber the new ref number
	 */
	public void setRefNumber(String refNumber) {
		if(refNumber.trim().equals("")) { 
			return;
		}
		this.refNumber = refNumber;
	}

	/**
	 * Gets the txn type.
	 *
	 * @return the txn type
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the txn type.
	 *
	 * @param txnType the new txn type
	 */
	public void setTxnType(String txnType) {
		if(txnType.trim().equals("")) { 
			return;
		}
		this.txnType = txnType;
	}

	/**
	 * Gets the interactive status.
	 *
	 * @return the interactive status
	 */
	public String getInteractiveStatus() {
		return interactiveStatus;
	}

	/**
	 * Sets the interactive status.
	 *
	 * @param interactiveStatus the new interactive status
	 */
	public void setInteractiveStatus(String interactiveStatus) {
		if(interactiveStatus.trim().equals("")) { 
			return;
		}
		this.interactiveStatus = interactiveStatus;
	}

	/**
	 * Gets the card no en.
	 *
	 * @return the card no en
	 */
	public String getCardNoEn() {
		return cardNoEn;
	}

	/**
	 * Sets the card no en.
	 *
	 * @param cardNoEn the new card no en
	 */
	public void setCardNoEn(String cardNoEn) {
		if(cardNoEn.trim().equals("")) { 
			return;
		}
		this.cardNoEn = cardNoEn;
	}
	
	/**
	 * Gets the card no.
	 *
	 * @return the card no
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * Sets the card no.
	 *
	 * @param cardNo the new card no
	 */
	public void setCardNo(String cardNo) {
		if(cardNo.trim().equals("")) { 
			return;
		}
		this.cardNo = cardNo;
	}

	/**
	 * Gets the expired date en.
	 *
	 * @return the expired date en
	 */
	public String getExpiredDateEn() {
		return expiredDateEn;
	}

	/**
	 * Sets the expired date en.
	 *
	 * @param expiredDateEn the new expired date en
	 */
	public void setExpiredDateEn(String expiredDateEn) {
		if(expiredDateEn.trim().equals("")) { 
			return;
		}
		this.expiredDateEn = expiredDateEn;
	}
	
	/**
	 * Gets the expired date.
	 *
	 * @return the expired date
	 */
	public String getExpiredDate() {
		return expiredDate;
	}

	/**
	 * Sets the expired date.
	 *
	 * @param expiredDate the new expired date
	 */
	public void setExpiredDate(String expiredDate) {
		if(expiredDate.trim().equals("")) { 
			return;
		}
		this.expiredDate = expiredDate;
	}

	/**
	 * Gets the cvv2 en.
	 *
	 * @return the cvv2 en
	 */
	public String getCvv2En() {
		return cvv2En;
	}

	/**
	 * Sets the cvv2 en.
	 *
	 * @param cvv2En the new cvv2 en
	 */
	public void setCvv2En(String cvv2En) {
		if(cvv2En.trim().equals("")) { 
			return;
		}
		this.cvv2En = cvv2En;
	}

	/**
	 * Gets the cvv2.
	 *
	 * @return the cvv2
	 */
	public String getCvv2() {
		return cvv2;
	}

	/**
	 * Sets the cvv2.
	 *
	 * @param cvv2 the new cvv2
	 */
	public void setCvv2(String cvv2) {
		if(cvv2.trim().equals("")) { 
			return;
		}
		this.cvv2 = cvv2;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(String amount) {
		if(amount.trim().equals("")) { 
			return;
		}
		this.amount = amount;
	}

	/**
	 * Gets the merchant id.
	 *
	 * @return the merchant id
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * Sets the merchant id.
	 *
	 * @param merchantId the new merchant id
	 */
	public void setMerchantId(String merchantId) {
		if(merchantId.trim().equals("")) { 
			return;
		}
		this.merchantId = merchantId;
	}

	/**
	 * Gets the settle merchant id.
	 *
	 * @return the settle merchant id
	 */
	public String getSettleMerchantId() {
		return settleMerchantId;
	}

	/**
	 * Sets the settle merchant id.
	 *
	 * @param settleMerchantId the new settle merchant id
	 */
	public void setSettleMerchantId(String settleMerchantId) {
		if(settleMerchantId.trim().equals("")) { 
			return;
		}
		this.settleMerchantId = settleMerchantId;
	}

	/**
	 * Gets the terminal id.
	 *
	 * @return the terminal id
	 */
	public String getTerminalId() {
		return terminalId;
	}

	/**
	 * Sets the terminal id.
	 *
	 * @param terminalId the new terminal id
	 */
	public void setTerminalId(String terminalId) {
		if(terminalId.trim().equals("")) { 
			return;
		}
		this.terminalId = terminalId;
	}

	/**
	 * Gets the card holder name.
	 *
	 * @return the card holder name
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * Sets the card holder name.
	 *
	 * @param cardHolderName the new card holder name
	 */
	public void setCardHolderName(String cardHolderName) {
		if(cardHolderName.trim().equals("")) { 
			return;
		}
		this.cardHolderName = cardHolderName;
	}

	/**
	 * Gets the card holder id.
	 *
	 * @return the card holder id
	 */
	public String getCardHolderId() {
		return cardHolderId;
	}

	/**
	 * Sets the card holder id.
	 *
	 * @param cardHolderId the new card holder id
	 */
	public void setCardHolderId(String cardHolderId) {
		if(cardHolderId.trim().equals("")) { 
			return;
		}
		this.cardHolderId = cardHolderId;
	}

	/**
	 * Gets the entry time.
	 *
	 * @return the entry time
	 */
	public String getEntryTime() {
		return entryTime;
	}

	/**
	 * Sets the entry time.
	 *
	 * @param entryTime the new entry time
	 */
	public void setEntryTime(String entryTime) {
		if(entryTime.trim().equals("")) { 
			return;
		}
		this.entryTime = entryTime;
	}

	/**
	 * Gets the external ref number.
	 *
	 * @return the external ref number
	 */
	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	/**
	 * Sets the external ref number.
	 *
	 * @param externalRefNumber the new external ref number
	 */
	public void setExternalRefNumber(String externalRefNumber) {
		if(externalRefNumber.trim().equals("")) { 
			return;
		}
		this.externalRefNumber = externalRefNumber;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		if(customerId.trim().equals("")) { 
			return;
		}
		this.customerId = customerId;
	}

	/**
	 * Gets the issuer country.
	 *
	 * @return the issuer country
	 */
	public String getIssuerCountry() {
		return issuerCountry;
	}

	/**
	 * Sets the issuer country.
	 *
	 * @param issuerCountry the new issuer country
	 */
	public void setIssuerCountry(String issuerCountry) {
		if(issuerCountry.trim().equals("")) { 
			return;
		}
		this.issuerCountry = issuerCountry;
	}

	/**
	 * Gets the site type.
	 *
	 * @return the site type
	 */
	public String getSiteType() {
		return siteType;
	}

	/**
	 * Sets the site type.
	 *
	 * @param siteType the new site type
	 */
	public void setSiteType(String siteType) {
		if(siteType.trim().equals("")) { 
			return;
		}
		this.siteType = siteType;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the site id
	 */
	public String getSiteID() {
		return siteID;
	}

	/**
	 * Sets the site id.
	 *
	 * @param siteID the new site id
	 */
	public void setSiteID(String siteID) {
		if(siteID.trim().equals("")) { 
			return;
		}
		this.siteID = siteID;
	}

	/**
	 * Gets the tr3 url.
	 *
	 * @return the tr3 url
	 */
	public String getTr3Url() {
		return tr3Url;
	}

	/**
	 * Sets the tr3 url.
	 *
	 * @param tr3Url the new tr3 url
	 */
	public void setTr3Url(String tr3Url) {
		if(tr3Url.trim().equals("")) { 
			return;
		}
		this.tr3Url = tr3Url;
	}

	/**
	 * Gets the term in months.
	 *
	 * @return the term in months
	 */
	public String getTermInMonths() {
		return termInMonths;
	}

	/**
	 * Sets the term in months.
	 *
	 * @param termInMonths the new term in months
	 */
	public void setTermInMonths(String termInMonths) {
		this.termInMonths = termInMonths;
	}


	/**
	 * Gets the order secret no.
	 *
	 * @return the orderSecretNo
	 */
	public String getOrderSecretNo() {
		return orderSecretNo;
	}


	/**
	 * Sets the order secret no.
	 *
	 * @param orderSecretNo the orderSecretNo to set
	 */
	public void setOrderSecretNo(String orderSecretNo) {
		this.orderSecretNo = orderSecretNo;
	}


	/**
	 * Sets the phone.
	 *
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}


	/**
	 * Gets the code01.
	 *
	 * @return the code01
	 */
	public String getCode01() {
		return code01;
	}


	/**
	 * Sets the code01.
	 *
	 * @param code01 the code01 to set
	 */
	public void setCode01(String code01) {
		this.code01 = code01;
	}


	/**
	 * Gets the prod info.
	 *
	 * @return the prodInfo
	 */
	public String getProdInfo() {
		return prodInfo;
	}


	/**
	 * Sets the prod info.
	 *
	 * @param prodInfo the prodInfo to set
	 */
	public void setProdInfo(String prodInfo) {
		this.prodInfo = prodInfo;
	}



	/**
	 * Gets the prod info sms.
	 *
	 * @return the prod info sms
	 */
	public String getProdInfoSms() {
		return prodInfoSms;
	}



	/**
	 * Sets the prod info sms.
	 *
	 * @param prodInfoSms the new prod info sms
	 */
	public void setProdInfoSms(String prodInfoSms) {
		this.prodInfoSms = prodInfoSms;
	}

	/**
	 * Gets the cmd code.
	 *
	 * @return the cmd code
	 */
	public String getCmdCode() {
		return cmdCode;
	}

	/**
	 * Sets the cmd code.
	 *
	 * @param cmdCode the new cmd code
	 */
	public void setCmdCode(String cmdCode) {
		this.cmdCode = cmdCode;
	}

	/**
	 * Gets the point amt.
	 *
	 * @return the point amt
	 */
	public String getPointAmt() {
		return pointAmt;
	}

	/**
	 * Sets the point amt.
	 *
	 * @param pointAmt the new point amt
	 */
	public void setPointAmt(String pointAmt) {
		this.pointAmt = pointAmt;
	}

	/**
	 * Gets the point fee amt.
	 *
	 * @return the point fee amt
	 */
	public String getPointFeeAmt() {
		return pointFeeAmt;
	}

	/**
	 * Sets the point fee amt.
	 *
	 * @param pointFeeAmt the new point fee amt
	 */
	public void setPointFeeAmt(String pointFeeAmt) {
		this.pointFeeAmt = pointFeeAmt;
	}

	/**
	 * Gets the customer point account.
	 *
	 * @return the customer point account
	 */
	public String getCustomerPointAccount() {
		return customerPointAccount;
	}

	/**
	 * Sets the customer point account.
	 *
	 * @param customerPointAccount the new customer point account
	 */
	public void setCustomerPointAccount(String customerPointAccount) {
		this.customerPointAccount = customerPointAccount;
	}

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public String getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point the new point
	 */
	public void setPoint(String point) {
		this.point = point;
	}
	
	/**
	 * Gets the point fee.
	 *
	 * @return the point fee
	 */
	public String getPointFee() {
		return pointFee;
	}

	/**
	 * Sets the point fee.
	 *
	 * @param pointFee the new point fee
	 */
	public void setPointFee(String pointFee) {
		this.pointFee = pointFee;
	}

	public String getExtMapStr() {
		return extMapStr;
	}

	public void setExtMapStr(String extMapStr) {
		this.extMapStr = extMapStr;
	}
}