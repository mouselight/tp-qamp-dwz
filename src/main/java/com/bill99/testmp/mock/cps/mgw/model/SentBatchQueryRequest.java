package com.bill99.testmp.mock.cps.mgw.model;

public class SentBatchQueryRequest {
	
		
	    private String txnTypes;
	    
	    private String firstPageNo;

	    private String maxRec;

	    private String terminalId;
	 
	    private String merchantId;
	 
	    private String beginEntryTime;

		private String endEntryTime;
	 
	    private String txnTotal;
	 
	    private String txnListCount;
	    
	    private String txnStatus;
	    
	    private String validCode;
	    
	    private String savePciFlag;
	    
	    private String token;
	    
	    private String payBatch;

		public String getValidCode() {
			return validCode;
		}
		public void setValidCode(String validCode) {
			this.validCode = validCode;
		}
		public String getSavePciFlag() {
			return savePciFlag;
		}
		public void setSavePciFlag(String savePciFlag) {
			this.savePciFlag = savePciFlag;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public String getPayBatch() {
			return payBatch;
		}
		public void setPayBatch(String payBatch) {
			this.payBatch = payBatch;
		}
		public String getFirstPageNo() {
			return firstPageNo;
		}
	    public String getTxnStatus() {
			return txnStatus;
		}

		public void setTxnStatus(String txnStatus) {
			if(txnStatus.trim().equals("")) { 
				return;
			}
			this.txnStatus = txnStatus; 
		}
		
		public void setFirstPageNo(String firstPageNo) {
			if(firstPageNo.trim().equals("")) { 
				return;
			}
			this.firstPageNo = firstPageNo;
		}

		public String getMaxRec() {
			return maxRec;
		}

		public void setMaxRec(String maxRec) {
			if(maxRec.trim().equals("")) { 
				return;
			}
			this.maxRec = maxRec;
		}

		public String getTerminalId() {
			return terminalId;
		}

		public void setTerminalId(String terminalId) {
			if(terminalId.trim().equals("")) { 
				return;
			}
			this.terminalId = terminalId;
		}

		public String getMerchantId() {
			return merchantId;
		}

		public void setMerchantId(String merchantId) {
			if(merchantId.trim().equals("")) { 
				return;
			}
			this.merchantId = merchantId;
		}

		public String getBeginEntryTime() {
			return beginEntryTime;
		}

		public void setBeginEntryTime(String beginEntryTime) {
			if(beginEntryTime.trim().equals("")) { 
				return;
			}
			this.beginEntryTime = beginEntryTime;
		}

		public String getEndEntryTime() {
			return endEntryTime;
		}

		public void setEndEntryTime(String endEntryTime) {
			if(endEntryTime.trim().equals("")) { 
				return;
			}
			this.endEntryTime = endEntryTime;
		}

		public String getTxnTotal() {
			return txnTotal;
		}

		public void setTxnTotal(String txnTotal) {
			if(txnTotal.trim().equals("")) { 
				return;
			}
			this.txnTotal = txnTotal;
		}

		public String getTxnListCount() {
			return txnListCount;
		}

		public void setTxnListCount(String txnListCount) {
			if(txnListCount.trim().equals("")) { 
				return;
			}
			this.txnListCount = txnListCount;
		}

		public String getTxnTypes() {
			return txnTypes;
		}

		public void setTxnTypes(String txnTypes) {
			if(txnTypes.trim().equals("")) { 
				return;
			}
			this.txnTypes = txnTypes;
		}
	
}
