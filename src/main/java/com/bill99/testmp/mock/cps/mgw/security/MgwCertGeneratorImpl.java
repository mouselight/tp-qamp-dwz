package com.bill99.testmp.mock.cps.mgw.security;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.KeyStore;
import java.security.interfaces.RSAPrivateKey;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.bouncycastle.openssl.PEMWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import steel.share.clientcert.ClientCert;
import fshare.client.CreateFileRequest;
import fshare.client.FShareClient;
import fshare.client.FileOutHandler;


public class MgwCertGeneratorImpl implements MgwCertGenerator {

	/**
	 * �ӿ�֤����ɷ���
	 */
	private ClientCertGenerator clientCertGenerator;

	/**
	 * �ļ�����ͻ���
	 */
	private FShareClient fShareClient;
	
	public String geneerator(ClientCert clientCert) {
		String keyName = clientCert.getCommonName();
		CreateFileRequest cfr = new CreateFileRequest();
		cfr.setFileName(keyName + ".rar");
		cfr.setDeleteAfterRead(false);
		FileOutHandler fileOutHandler = fShareClient.createFile(cfr);

		clientCert.setCommonName(clientCert.getCommonName());

		ZipOutputStream zip = new ZipOutputStream(fileOutHandler.getOut());
		InputStream is = null;
		try {
			// 1 ����PFX�ļ�
			zip.putNextEntry(new ZipEntry(keyName + ".pfx"));
			OutputStream os = new ByteArrayOutputStream(1024);
			clientCertGenerator.genClientCert(clientCert, os);
			os.close();

			// ���湫Կ����ݿ�
//			is = new ByteArrayInputStream(((ByteArrayOutputStream) os)
//					.toByteArray());
//			savePublicKey(is, clientCert);
			is = new ByteArrayInputStream(((ByteArrayOutputStream) os)
					.toByteArray());
			int readLen;
			byte[] buf = new byte[1024];
			while ((readLen = is.read(buf, 0, 1024)) != -1) {
				zip.write(buf, 0, readLen);
			}
			zip.closeEntry();

			// ����PEM��ʽ��˽Կ
			is = new ByteArrayInputStream(((ByteArrayOutputStream) os)
					.toByteArray());
			KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
			keyStore.load(is, clientCert.getPassword().toCharArray());
			KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore
					.getEntry(clientCert.getCommonName(),
							new KeyStore.PasswordProtection(clientCert
									.getPassword().toCharArray()));
			RSAPrivateKey privateKey = (RSAPrivateKey) privateKeyEntry
					.getPrivateKey();
			os = new ByteArrayOutputStream();
			PEMWriter wr = new PEMWriter(new OutputStreamWriter(os));
			wr.writeObject(privateKey);
			wr.close();
			is = new ByteArrayInputStream(((ByteArrayOutputStream) os)
					.toByteArray());
			zip.putNextEntry(new ZipEntry(keyName + ".pem"));
			while ((readLen = is.read(buf, 0, 1024)) != -1) {
				zip.write(buf, 0, readLen);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			try {
				if (is != null)
					is.close();
				if (zip != null) {
					zip.closeEntry();
					zip.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		// fileOutHandler.close();
		String filePath = fileOutHandler.getFileInfo().getVirtualFilePath();
		return filePath;
	}
	
//	
//	private void savePublicKey(String filePath, ClientCert clientCert) {
//		InputStream fin = null;
//		try {
//			fin = fShareClient.openFile(filePath).getIn();
//			this.savePublicKey(fin, clientCert);
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage(), e);
//		} finally {
//			if (fin != null) {
//				try {
//					fin.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//	
//	
//	private void savePublicKey(InputStream fin, ClientCert clientCert) {
//		try {
//			KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
//			keyStore.load(fin, clientCert.getPassword().toCharArray());
//			X509Certificate cert = (X509Certificate) keyStore
//					.getCertificate(clientCert.getCommonName());
//
//			String alias = keyStore.aliases().nextElement().toString();
//			PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias,
//					clientCert.getPassword().toCharArray());
//
//			PublicKeyEntry publicKeyEntry = new PublicKeyEntry();
//			publicKeyEntry.setKeyName(clientCert.getCommonName());
//			publicKeyEntry.setPublicKeyData(cert.getPublicKey().getEncoded());
//			publicKeyEntry.setKeyAlgorithm("0");
//			publicKeyEntry.setCertType("0");
//			publicKeyEntry.setPrivateKeyData(privateKey.getEncoded());
//			publicKeyEntry.setCertData(cert.getEncoded());
//
//			pkiService.storePublicKey(publicKeyEntry);
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage(), e);
//		}
//	}
	
	public ClientCertGenerator getClientCertGenerator() {
		return clientCertGenerator;
	}
	
	@Autowired
	public void setClientCertGenerator(@Qualifier("mas.security.ClientCertGenerator")ClientCertGenerator clientCertGenerator) {
		this.clientCertGenerator = clientCertGenerator;
	}
	
	@Autowired
	public void setFShareClient(@Qualifier("fshare.FShareClient")
	FShareClient shareClient) {
		fShareClient = shareClient;
	}
	
	
}
