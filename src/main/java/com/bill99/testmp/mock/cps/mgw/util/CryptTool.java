package com.bill99.testmp.mock.cps.mgw.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class CryptTool
{
  public static SecretKey genDESKey(byte[] key_byte)
  {
    SecretKey k = null;
    k = new SecretKeySpec(key_byte, "DESede");
    return k;
  }

  public static byte[] desDecrypt(SecretKey key, byte[] crypt)
    throws Exception
  {
    Cipher cipher = Cipher.getInstance("DESede");
    cipher.init(2, key);
    return cipher.doFinal(crypt);
  }

  public static String desDecrypt(SecretKey key, String crypt)
    throws Exception
  {
    String a = urlDecode(crypt);
    byte[] ss = Base64Binrary.decodeBase64Binrary(a);
    System.err.println(new String(ss));
    return new String(desDecrypt(key, ss));
  }

  public static byte[] desEncrypt(SecretKey key, byte[] src)
    throws Exception
  {
    Cipher cipher = Cipher.getInstance("DESede");
    cipher.init(1, key);
    return cipher.doFinal(src);
  }

  public static String desEncrypt(SecretKey key, String src)
    throws Exception
  {
    return new String(desEncrypt(key, src.getBytes()));
  }

  public static String base64Encode(String src)
  {
    BASE64Encoder encoder = new BASE64Encoder();

    return encoder.encode(src.getBytes());
  }

  public static String base64Encode(byte[] src)
  {
    BASE64Encoder encoder = new BASE64Encoder();

    return encoder.encode(src);
  }

  public static String base64Decode(String src)
  {
    BASE64Decoder decoder = new BASE64Decoder();
    try
    {
      return new String(decoder.decodeBuffer(src)); } catch (Exception ex) {
    }
    return null;
  }

  public static byte[] base64DecodeToBytes(String src)
  {
    BASE64Decoder decoder = new BASE64Decoder();
    try
    {
      return decoder.decodeBuffer(src); } catch (Exception ex) {
    }
    return null;
  }

  public static String urlEncode(String src)
  {
    try
    {
      src = URLEncoder.encode(src, "GB2312");

      return src;
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return src;
  }

  public static String urlDecode(String value)
  {
    try
    {
      return URLDecoder.decode(value, "GB2312");
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return value;
  }

  public static void main(String[] args)
    throws Exception
  {
    byte[] key_byte = "123456781234567812345678".getBytes();
    try
    {
      SecretKey deskey = genDESKey(key_byte);
      System.out.println("Generator DES KEY OK");

      String s = "12345678";

      byte[] encrypt1 = desEncrypt(deskey, s.getBytes());
      System.out.println(new String(encrypt1));

      String ss = Base64Binrary.encodeBase64Binrary(encrypt1);
      byte[] de = Base64Binrary.decodeBase64Binrary(ss);
      byte[] decrypt2 = desDecrypt(deskey, de);
      System.out.println(new String(decrypt2));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

    System.out.println("BASE64 Test:" + base64Decode(base64Encode("1234")));
  }
}