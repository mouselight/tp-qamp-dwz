package com.bill99.testmp.testtools.web.controller.acceptability.bank;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.manager.BankPanInfoMng;

@Controller
public class BankPanInfoController {

	@Autowired
	private BankPanInfoMng bankPanInfoMng;

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/list_pan.htm")
	public String list_pan(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		model.addAttribute("bankPans", bankPanInfoMng.queryAll());

		return "testtools/acceptability/bank/paninfo/list_pan";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/add_pan.htm")
	public String add_pan(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		return "testtools/acceptability/bank/paninfo/add_pan";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/save_pan.htm")
	public void save_pan(BankPanInfo bankPanInfo, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		bankPanInfo.setCreateUser(RiaFrameworkUtils.getUserName(request));
		bankPanInfo.setState(Boolean.TRUE);
		bankPanInfoMng.save(bankPanInfo);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "添加成功", "list_pan", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/edit_pan.htm")
	public String edit_pan(Long idBankPan, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		model.addAttribute("item", bankPanInfoMng.queryById(idBankPan));
		return "testtools/acceptability/bank/paninfo/edit_pan";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/update_pan.htm")
	public void update_pan(BankPanInfoCmd bankPanInfoCmd, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		BankPanInfo bankPanInfo = bankPanInfoMng.queryById(bankPanInfoCmd.getIdBankPan());

		BeanUtils.copyProperties(bankPanInfoCmd, bankPanInfo, new String[] { "idBankPan", "state" });
		bankPanInfo.setUpdateDate(DateUtil.getTimeNow());
		bankPanInfo.setUpdateUser(RiaFrameworkUtils.getUserName(request));
		bankPanInfoMng.update(bankPanInfo);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "更新成功", "list_pan", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testtools/acceptability/bank/paninfo/del_pan.htm")
	public void del_pan(Long idBankPan, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		BankPanInfo bankPanInfo = bankPanInfoMng.queryById(idBankPan);
		bankPanInfo.setState(Boolean.FALSE);
		bankPanInfo.setUpdateDate(DateUtil.getTimeNow());
		bankPanInfo.setUpdateUser(RiaFrameworkUtils.getUserName(request));
		bankPanInfoMng.update(bankPanInfo);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", "list_pan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
