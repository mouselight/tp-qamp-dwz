package com.bill99.testmp.testtools.web.controller.acceptability.bank;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.testtools.common.utils.AcceptAbilityUtils;
import com.bill99.testmp.testtools.domain.AcceptabilityCNP;
import com.bill99.testmp.testtools.domain.AcceptabilityExpService;
import com.bill99.testmp.testtools.domain.AcceptabilityExtension;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.BankPanInfoMng;

@Controller
public class DetectController {
	static Log logger = LogFactory.getLog(DetectController.class);

	@Autowired
	private BankPanInfoMng bankPanInfoMng;
	@Autowired
	private AcceptAbilityTaskMng acceptAbilityTaskMng;
	@Autowired
	@Qualifier("acceptabilityCNP")
	private AcceptabilityCNP acceptabilityCNP;
	@Autowired
	@Qualifier("acceptabilityDDP")
	private AcceptabilityCNP acceptabilityDDP;
	@Autowired
	private TaskExecutor taskExecutor;
	@Autowired
	private AcceptabilityExpService acceptabilityExpService;
	@Autowired
	private  AcceptabilityExtension acceptabilityExtension;

	@RequestMapping(value = "/testtools/acceptability/bank/detect/index_detect.htm")
	public String index_detect(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		model.addAttribute("bankPans", bankPanInfoMng.queryAll());
		return "testtools/acceptability/bank/detect/index_detect";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/detect/combox/change_txntype.htm")
	public void change_txntype(String bizType, HttpServletRequest request, HttpServletResponse response) {
		String jsonStr = "[[\"\",\"选择交易/接口类型\"]]";
		if ("CNP".equals(bizType)) {
			//交易类型（CSP用，QUY 鉴权,PUR 消费,PRE 预授权,INP 分期）
			jsonStr = "[[\"QUY\",\"鉴权\"],[\"PUR\", \"消费\"],[\"PRE\", \"预授权\"],[\"INP\", \"分期\"]]";
		}
		if ("DDP".equals(bizType)) {
			jsonStr = "[[\"901\",\"901\"],[\"1001\", \"1001\"],[\"201\", \"201\"],[\"302\", \"302\"]]";
		}
		ResponseUtils.renderJson(response, jsonStr);
	}

	@RequestMapping(value = "/testtools/acceptability/bank/detect/combox/change_merchant.htm")
	public void change_merchant(String txnInterface, HttpServletRequest request, HttpServletResponse response) {
		String merchants = "[[\"\",\"选择测试商户\"]]";
		if (txnInterface.endsWith("1") || txnInterface.endsWith("2")) {
			merchants = "[[\"10020185410\",\"10020185410\"]]";
		} else {
			merchants = "[[\"812310045110084\",\"812310045110084\"],[\"812310045110066\", \"812310045110066\"],[\"812310045110085\", \"812310045110085\"],[\"999110042140002\", \"999110042140002\"]]";
		}
		ResponseUtils.renderJson(response, merchants);
	}

	@RequestMapping(value = "/testtools/acceptability/bank/detect/combox/change_terminal.htm")
	public void change_terminal(String merchantId, HttpServletRequest request, HttpServletResponse response) {
		String terminals = "[[\"\",\"选择测试终端\"]]";
		if (merchantId.equals("812310045110084")) {
			terminals = "[[\"15011560\",\"15011560\"]]";
		}
		if (merchantId.equals("812310045110066")) {
			terminals = "[[\"15011542\",\"15011542\"]]";
		}
		if (merchantId.equals("812310045110085")) {
			terminals = "[[\"15011561\",\"15011561\"]]";
		}
		if (merchantId.equals("999110042140002")) {
			terminals = "[[\"01000719\",\"01000719\"]]";
		}
		ResponseUtils.renderJson(response, terminals);
	}

	@RequestMapping(value = "/testtools/acceptability/bank/detect/detect_process.htm")
	public void detect_process(DetectCmd detectCmd, HttpServletRequest request, HttpServletResponse response) {
		AcceptAbilityTask acceptAbilitytTask = new AcceptAbilityTask();
		BeanUtils.copyProperties(detectCmd, acceptAbilitytTask);
		acceptAbilitytTask.setCreateUser(RiaFrameworkUtils.getUserName(request));
		acceptAbilitytTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_INIT);
		acceptAbilitytTask = acceptAbilityTaskMng.save(acceptAbilitytTask);//保存日志

		processByTread(acceptAbilitytTask, detectCmd.getPans());

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "执行成功,请等待!!!稍后可查询结果", "index_detect", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	private void processByTread(final AcceptAbilityTask acceptAbilitytTask, final String pans) {
		taskExecutor.execute(new Runnable() {
			public void run() {

				try {
					acceptAbilitytTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ING);
					acceptAbilityTaskMng.update(acceptAbilitytTask);
					try {
						if ("CNP".equals(acceptAbilitytTask.getProductName())) {
							acceptabilityCNP.exploring(acceptAbilitytTask, Arrays.asList(StringUtil.string2LongArray(pans)));//受理能力check
						}
						if ("DDP".equals(acceptAbilitytTask.getProductName())) {
							acceptabilityDDP.exploring(acceptAbilitytTask, Arrays.asList(StringUtil.string2LongArray(pans)));//受理能力check
						}
						acceptAbilitytTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_DONE);
					} catch (Exception e) {
						e.printStackTrace();
						acceptAbilitytTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
						logger.error("exploring--" + e);
					}
					acceptAbilityTaskMng.update(acceptAbilitytTask);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("processByTread--" + e);
				}
			}
		});
	}

	//导出验证结果
	@RequestMapping(value = "/testtools/acceptability/bank/detect/export_aareport.htm")
	public void export_aareport(Long idTask, HttpServletRequest request, HttpServletResponse response) {
		try {
			acceptabilityExtension.getExtFromDB(idTask);
			acceptabilityExpService.expAcceptabilityResult(idTask, response);
		} catch (Exception e) {
			e.printStackTrace();
			AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("300", "导出失败!", "index_detect", null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
		}
	}
}
