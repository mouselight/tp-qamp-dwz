package com.bill99.testmp.testtools.web.controller.acceptability.bank;

import java.io.Serializable;

public class BankPanInfoCmd implements Serializable {

	private static final long serialVersionUID = 7848264194926100914L;
	private Long idBankPan;
	private String bankName;
	private String pan;
	private String expireDate;
	private String cvv2;
	private String cardholder;
	private String noId;
	private String noCellPhone;
	private String memo;
	private Boolean state;
	private String noType;
	private String bankCode;
	private String cardType;

	public Long getIdBankPan() {
		return idBankPan;
	}

	public void setIdBankPan(Long idBankPan) {
		this.idBankPan = idBankPan;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getCardholder() {
		return cardholder;
	}

	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}

	public String getNoId() {
		return noId;
	}

	public void setNoId(String noId) {
		this.noId = noId;
	}

	public String getNoCellPhone() {
		return noCellPhone;
	}

	public void setNoCellPhone(String noCellPhone) {
		this.noCellPhone = noCellPhone;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public String getNoType() {
		return noType;
	}

	public void setNoType(String noType) {
		this.noType = noType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

}
