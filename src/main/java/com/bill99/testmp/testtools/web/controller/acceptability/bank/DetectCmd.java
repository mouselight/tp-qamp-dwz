package com.bill99.testmp.testtools.web.controller.acceptability.bank;

import java.io.Serializable;
import java.math.BigDecimal;

public class DetectCmd implements Serializable {

	private static final long serialVersionUID = 7162787346320448135L;
	private String pKey;
	private String productName;
	private String subtaskStatus;
	private BigDecimal amt;
	private String terminaId;
	private String merchantId;
	private String txnInterface;
	private String termInMonths;
	private String contractNo;
	private String reportNo;
	private String pans;
	private String attachInfos;
	private Integer sleepTime;
	private String bankRespCodeKey;
	private String bankRespCodeDescKey;
	

	public String getpKey() {
		return pKey;
	}

	public void setpKey(String pKey) {
		this.pKey = pKey;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubtaskStatus() {
		return subtaskStatus;
	}

	public void setSubtaskStatus(String subtaskStatus) {
		this.subtaskStatus = subtaskStatus;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public String getTerminaId() {
		return terminaId;
	}

	public void setTerminaId(String terminaId) {
		this.terminaId = terminaId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(String termInMonths) {
		this.termInMonths = termInMonths;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getPans() {
		return pans;
	}

	public void setPans(String pans) {
		this.pans = pans;
	}

	public String getTxnInterface() {
		return txnInterface;
	}

	public void setTxnInterface(String txnInterface) {
		this.txnInterface = txnInterface;
	}

	public String getAttachInfos() {
		return attachInfos;
	}

	public void setAttachInfos(String attachInfos) {
		this.attachInfos = attachInfos;
	}

	public Integer getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(Integer sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getBankRespCodeKey() {
		return bankRespCodeKey;
	}

	public void setBankRespCodeKey(String bankRespCodeKey) {
		this.bankRespCodeKey = bankRespCodeKey;
	}

	public String getBankRespCodeDescKey() {
		return bankRespCodeDescKey;
	}

	public void setBankRespCodeDescKey(String bankRespCodeDescKey) {
		this.bankRespCodeDescKey = bankRespCodeDescKey;
	}

}
