package com.bill99.testmp.testtools.web.controller.acceptability.bank;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilitySubTaskMng;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;

@Controller
public class DetectQueryController {

	@Autowired
	private AcceptAbilityTaskMng acceptAbilityTaskMng;
	@Autowired
	private AcceptAbilitySubTaskMng acceptAbilitySubTaskMng;
	@Autowired
	private TaskLogInfoMng taskLogInfoMng;

	@RequestMapping(value = "/testtools/acceptability/bank/query/list_detect.htm")
	public String index_query(HttpServletRequest request, DetectCmd detectCmd, Page page, Integer pageNum, Integer numPerPage, HttpServletResponse response, ModelMap model,
			HttpSession httpSession) {

		page.setPageSize(numPerPage == null ? 20 : numPerPage);
		page.setTargetPage(pageNum == null ? 1 : pageNum);

		AcceptAbilityTask acceptAbilityTask = new AcceptAbilityTask();
		BeanUtils.copyProperties(detectCmd, acceptAbilityTask);

		model.addAttribute("page", page);
		model.addAttribute("cmd", detectCmd);
		model.addAttribute("aaTasks", acceptAbilityTaskMng.queryForPage(acceptAbilityTask, page));
		return "testtools/acceptability/bank/query/list_detect";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/query/list_subdetect.htm")
	public String list_subdetect(Long idTask, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		model.addAttribute("aaSubTasks", acceptAbilitySubTaskMng.queryForList(idTask));
		return "testtools/acceptability/bank/query/list_subdetect";
	}

	@RequestMapping(value = "/testtools/acceptability/bank/query/list_subdetect_detail.htm")
	public String list_subdetect_detail(Long idSubTask, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		model.addAttribute("aaSubTaskDetails", taskLogInfoMng.queryBySubTaskId(idSubTask));
		return "testtools/acceptability/bank/query/list_subdetect_detail";
	}

}
