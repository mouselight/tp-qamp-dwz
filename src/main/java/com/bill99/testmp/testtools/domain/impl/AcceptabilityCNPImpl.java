package com.bill99.testmp.testtools.domain.impl;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.bill99.testmp.testtools.common.utils.AcceptAbilityUtils;
import com.bill99.testmp.testtools.domain.AcceptabilityCNP;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;
import com.bill99.testmp.testtools.orm.entity.CnpReponseMsg;
import com.bill99.testmp.testtools.orm.entity.CnpRequstMsg;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilitySubTaskMng;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.BankPanInfoMng;
import com.bill99.testmp.testtools.orm.manager.CheckedItemInfoMng;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;
import com.bill99.testmp.testtools.utils.BeanUtils;
import com.bill99.testmp.testtools.utils.Dict;
import com.bill99.testmp.testtools.utils.HttpFixture;
import com.bill99.testmp.testtools.utils.NofM;
import com.bill99.testmp.testtools.utils.TaskLogInfoBuilder;

public class AcceptabilityCNPImpl implements AcceptabilityCNP {
	
	private Integer sleepTime = 0;

	private TaskLogInfoMng taskLogInfoMng;

	private BankPanInfoMng bankPanInfoMng;

	private CheckedItemInfoMng checkedItemInfoMng;

	private AcceptAbilitySubTaskMng acceptAbilitySubTaskMng;

	private AcceptAbilityTaskMng acceptAbilityTaskMng;

	private String postUrl;
	
	private String tempPath;
	
	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}
	
	public TaskLogInfoMng getTaskLogInfoMng() {
		return taskLogInfoMng;
	}

	public void setTaskLogInfoMng(TaskLogInfoMng taskLogInfoMng) {
		this.taskLogInfoMng = taskLogInfoMng;
	}

	public BankPanInfoMng getBankPanInfoMng() {
		return bankPanInfoMng;
	}

	public void setBankPanInfoMng(BankPanInfoMng bankPanInfoMng) {
		this.bankPanInfoMng = bankPanInfoMng;
	}

	public CheckedItemInfoMng getCheckedItemInfoMng() {
		return checkedItemInfoMng;
	}

	public void setCheckedItemInfoMng(CheckedItemInfoMng checkedItemInfoMng) {
		this.checkedItemInfoMng = checkedItemInfoMng;
	}

	public AcceptAbilitySubTaskMng getAcceptAbilitySubTaskMng() {
		return acceptAbilitySubTaskMng;
	}

	public void setAcceptAbilitySubTaskMng(AcceptAbilitySubTaskMng acceptAbilitySubTaskMng) {
		this.acceptAbilitySubTaskMng = acceptAbilitySubTaskMng;
	}

	public AcceptAbilityTaskMng getAcceptAbilityTaskMng() {
		return acceptAbilityTaskMng;
	}

	public void setAcceptAbilityTaskMng(AcceptAbilityTaskMng acceptAbilityTaskMng) {
		this.acceptAbilityTaskMng = acceptAbilityTaskMng;
	}

	/**
	 * 探测CNP受理能力
	 * 
	 * @param cnpRequstMsg
	 *            包含基本信息的cnp请求
	 * @param bankPanInfo
	 *            正确的银行卡信息
	 * @param checkedItems
	 *            受理能力校验项
	 * @return
	 * 
	 */
	private boolean exploring(CnpRequstMsg cnpRequstMsg, BankPanInfo bankPanInfo, BankPanInfo errBankPanInfo, List<CheckedItemInfo> checkedItems,
			AcceptAbilitySubTask acceptAbilitySubTask) {
		try {
			Long subtaskId = acceptAbilitySubTask.getIdAaSubTask();
			CnpRequstMsg cnpRequstMsgCopy = new CnpRequstMsg();
			PropertyUtils.copyProperties(cnpRequstMsgCopy, cnpRequstMsg);
			// 必选项目
			Set<Integer> requiredItems = new LinkedHashSet<Integer>();
			// 可选项目
			Set<Integer> optionItems = new LinkedHashSet<Integer>();
			// 最小输入项目
			Set<CheckedItemInfo> minItems = new LinkedHashSet<CheckedItemInfo>();
			// 最小输入外的其他项目
			Set<CheckedItemInfo> otherItems = new LinkedHashSet<CheckedItemInfo>();
			// 禁止输入项目
			Set<CheckedItemInfo> nonAcceptanceItems = new LinkedHashSet<CheckedItemInfo>();
			// 上送且校验
			Set<CheckedItemInfo> checkItems = new LinkedHashSet<CheckedItemInfo>();
			// 上送但不校验
			Set<CheckedItemInfo> unCheckItems = new LinkedHashSet<CheckedItemInfo>();
			// 返回报文
			CnpReponseMsg cnpReponseMsg = new CnpReponseMsg();
			// 交易成功标志
			boolean tranFlag = false;
			String result = "";

			TaskLogInfo taskLogInfo;
			TaskLogInfo taskLogInfo2;
			// 第一轮探测

			for (int i = 0; i < checkedItems.size(); i++) {
				if (checkedItems.get(i).isRequired()) {
					requiredItems.add(i);
					// 先把必选项目加到最小输入项里
					minItems.add(checkedItems.get(i));
				} else
					optionItems.add(i);
			}
			// //System.out.println(requiredItems);
			// //System.out.println(optionItems);

			taskLogInfo = TaskLogInfoBuilder.instantiation("CNP", subtaskId, checkedItems);

			// 只发送必选参数
			for (int t : requiredItems) {
				PropertyUtils.setProperty(cnpRequstMsg, checkedItems.get(t).getMapingReq(), PropertyUtils.getProperty(bankPanInfo, checkedItems.get(t).getItemId()).toString());
				taskLogInfo.changeStatus(checkedItems.get(t).getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
			}
			cnpReponseMsg = SendRequet(cnpRequstMsg);
			this.changeTaskLogInfo(taskLogInfo, cnpReponseMsg, cnpRequstMsg);
			taskLogInfoMng.save(taskLogInfo);

			if (cnpReponseMsg.getResponseCode().equals(Dict.CNP_RESPONSECODE_SUCCESS)) {
				// 最小输入项为必选项
				tranFlag = true;
			} else {
				// 按照排列组合发送必选参数和可选参数的组合
				int max = optionItems.size();
				boolean successflag = false;
				for (int t = 1; t <= max; t++) {
					if (successflag)
						break;
					NofM nOfm = new NofM(t, optionItems.toArray(new Integer[0]));
					while (nOfm.hasNext()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("CNP", subtaskId, checkedItems);
						// Thread.sleep(2000);
						Object[] set = nOfm.next();
						cnpRequstMsg = new CnpRequstMsg();
						PropertyUtils.copyProperties(cnpRequstMsg, cnpRequstMsgCopy);
						cnpRequstMsg.setServiceChannelTraceNo();
						cnpRequstMsg.setEntryTime();
						// 设置必填参数
						for (int requiredItem : requiredItems) {
							PropertyUtils.setProperty(cnpRequstMsg, checkedItems.get(requiredItem).getMapingReq(),
									PropertyUtils.getProperty(bankPanInfo, checkedItems.get(requiredItem).getItemId()).toString());
							taskLogInfo.changeStatus(checkedItems.get(requiredItem).getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
						}
						// 设置可选参数
						for (Object s : set) {
							for (Integer optionItem : optionItems) {
								if (s.equals(optionItem)) {
									PropertyUtils.setProperty(cnpRequstMsg, checkedItems.get(optionItem.intValue()).getMapingReq(),
											PropertyUtils.getProperty(bankPanInfo, checkedItems.get(optionItem.intValue()).getItemId()).toString());
									taskLogInfo.changeStatus(checkedItems.get(optionItem).getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
								}
							}
						}
						// 发送数据
						cnpReponseMsg = SendRequet(cnpRequstMsg);
						this.changeTaskLogInfo(taskLogInfo, cnpReponseMsg, cnpRequstMsg);
						// 保存执行结果
						taskLogInfoMng.save(taskLogInfo);

						// //System.out.println(cnpRequstMsg);
						if (cnpReponseMsg.getResponseCode().equals(Dict.CNP_RESPONSECODE_SUCCESS)) {
							// 返回成功，得到可选项目的最小输入项目集合
							for (Object s : set) {
								for (Integer optionItem : optionItems) {
									if (s.equals(optionItem)) {
										minItems.add(checkedItems.get(optionItem.intValue()));
									}
								}
							}
							successflag = true;
							tranFlag = true;
							break;
						}
					}
				}
			}
			//System.out.println("第一轮校验完毕");
			if (!tranFlag) {
				//System.out.println("第一轮校验完毕，未发现返回正确的交易");
				acceptAbilitySubTask.setResult("第一轮校验完毕，未发现返回正确的交易，系统可能存在异常，请排查");
				acceptAbilitySubTask.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
				acceptAbilitySubTaskMng.save(acceptAbilitySubTask);
				return false;
			} else {
				// 第二轮校验
				if (checkedItems.size() == minItems.size()) {
					// 最小输入集合与全集相等，不需要执行第二轮校验
				} else {
					// 获取最小输入项以外的集合
					Iterator<CheckedItemInfo> checkedItems2 = checkedItems.iterator();
					while (checkedItems2.hasNext())
						otherItems.add(checkedItems2.next());

					otherItems.removeAll(minItems);
					Iterator<CheckedItemInfo> otherItems2 = otherItems.iterator();
					while (otherItems2.hasNext()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("CNP", subtaskId, checkedItems);
						taskLogInfo2 = TaskLogInfoBuilder.instantiation("CNP", subtaskId, checkedItems);
						PropertyUtils.copyProperties(cnpRequstMsg, cnpRequstMsgCopy);
						cnpRequstMsg.setServiceChannelTraceNo();
						cnpRequstMsg.setEntryTime();
						for (CheckedItemInfo minItem : minItems) {
							// 设置最小输入项的正确参数
							PropertyUtils.setProperty(cnpRequstMsg, minItem.getMapingReq(), // cnpRequstMsg
																							// 的某字段做替换
									PropertyUtils.getProperty(bankPanInfo, minItem.getItemId()).toString());
							taskLogInfo.changeStatus(minItem.getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
							taskLogInfo2.changeStatus(minItem.getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
						}
						CheckedItemInfo otherItem = otherItems2.next();
						boolean flagA = false, flagB = false;
						// 其他项目设置为正确值发送
						PropertyUtils.setProperty(cnpRequstMsg, otherItem.getMapingReq(), PropertyUtils.getProperty(bankPanInfo, otherItem.getItemId()).toString());
						taskLogInfo.changeStatus(otherItem.getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
						cnpReponseMsg = SendRequet(cnpRequstMsg);
						if (cnpReponseMsg.getResponseCode().equals(Dict.CNP_RESPONSECODE_SUCCESS)) {
							flagA = true;
						}
						this.changeTaskLogInfo(taskLogInfo, cnpReponseMsg, cnpRequstMsg);
						taskLogInfoMng.save(taskLogInfo);

						cnpRequstMsg.setServiceChannelTraceNo();
						cnpRequstMsg.setEntryTime();
						// 其他项目设置为错误值发送
						// PropertyUtils.copyProperties(taskLogInfo2,
						// taskLogInfo);
						// taskLogInfo= TaskLogInfoBuilder.instantiation("CNP",
						// subtaskId, checkedItems);
						// PropertyUtils.copyProperties(taskLogInfo,
						// taskLogInfo2);

						PropertyUtils.setProperty(cnpRequstMsg, otherItem.getMapingReq(), PropertyUtils.getProperty(errBankPanInfo, otherItem.getItemId()).toString());
						taskLogInfo2.changeStatus(otherItem.getItemId(), TaskLogInfo.STATUS_FAILVALUE);
						cnpReponseMsg = SendRequet(cnpRequstMsg);
						if (cnpReponseMsg.getResponseCode().equals(Dict.CNP_RESPONSECODE_SUCCESS)) {
							flagB = true;
						}
						this.changeTaskLogInfo(taskLogInfo2, cnpReponseMsg, cnpRequstMsg);
						taskLogInfoMng.save(taskLogInfo2);
						if (flagA && flagB) {
							// 上送，不校验
							unCheckItems.add(otherItem);
						}
						if (flagA && !flagB) {
							// 上送，校验
							checkItems.add(otherItem);
						}
						if (!flagA) {
							// 禁止上送
							nonAcceptanceItems.add(otherItem);
						}

					}

				}
				//System.out.println("第二轮校验完毕");

				Iterator<CheckedItemInfo> minItems2 = minItems.iterator();
				while (minItems2.hasNext()) {

					CheckedItemInfo minItem = minItems2.next();
					if (!minItem.isCorrectValue()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("CNP", subtaskId, checkedItems);
						// 没有必须正确的限制
						PropertyUtils.copyProperties(cnpRequstMsg, cnpRequstMsgCopy);
						cnpRequstMsg.setServiceChannelTraceNo();
						cnpRequstMsg.setEntryTime();
						for (CheckedItemInfo minItem2 : minItems) {
							// 设置最小输入项的正确参数
							PropertyUtils.setProperty(cnpRequstMsg, minItem2.getMapingReq(), // cnpRequstMsg
																								// 的某字段做替换
									PropertyUtils.getProperty(bankPanInfo, minItem2.getItemId()).toString());
							taskLogInfo.changeStatus(minItem2.getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
						}
						// 当前项目设置为错误值
						PropertyUtils.setProperty(cnpRequstMsg, minItem.getMapingReq(), PropertyUtils.getProperty(errBankPanInfo, minItem.getItemId()).toString());
						taskLogInfo.changeStatus(minItem.getItemId(), TaskLogInfo.STATUS_FAILVALUE);
						cnpReponseMsg = SendRequet(cnpRequstMsg);
						if (cnpReponseMsg.getResponseCode().equals(Dict.CNP_RESPONSECODE_SUCCESS)) {
							unCheckItems.add(minItem);
						} else {
							checkItems.add(minItem);
						}
						this.changeTaskLogInfo(taskLogInfo, cnpReponseMsg, cnpRequstMsg);
						taskLogInfoMng.save(taskLogInfo);
					}

				}

				//System.out.println("第三轮校验完毕");

				result += "最小输入项集合：<br/>";
				for (CheckedItemInfo ci : minItems) {
					result += Dict.getItemChineseName(ci.getItemId()) + "、";
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "上送，不校验项集合：<br/>";
				if (unCheckItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : unCheckItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "上送，校验项集合：<br/>";
				if (checkItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : checkItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "禁止上送项集合：<br/>";
				if (nonAcceptanceItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : nonAcceptanceItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				acceptAbilitySubTask.setResult(result);
			}
			//acceptAbilitySubTask.setResult(result);
			acceptAbilitySubTask.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_DONE);
			acceptAbilitySubTaskMng.save(acceptAbilitySubTask);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	private void changeTaskLogInfo(TaskLogInfo taskLogInfo, CnpReponseMsg cnpReponseMsg, CnpRequstMsg cnpRequstMsg) {
		taskLogInfo.setTxnStatus(cnpReponseMsg.getResponseCode());
		taskLogInfo.setTxnMsg(cnpReponseMsg.getResponseTextMessage());
		taskLogInfo.setExtTraceNo(cnpRequstMsg.getServiceChannelTraceNo());
		taskLogInfo.setIdTxn(cnpReponseMsg.getRRN());
	}

	private CnpReponseMsg SendRequet(CnpRequstMsg cnpRequstMsg) throws Exception {
		HttpFixture hf = new HttpFixture();
		//		hf.setUrl("http://192.168.52.219:18080/mock/internal/mtp/processCnp.htm");
		//hf.setUrl("http://cpay.99bill.com/mock/internal/mtp/processCnp.htm");
		hf.setUrl(this.postUrl);
		hf.addHeaderValue("Content-Type", "application/x-www-form-urlencoded");
		hf.addParamValue(cnpRequstMsg);
		hf.Post();
		CnpReponseMsg cnpReponseMsg = BeanUtils.str2CnpReponseMsg(hf.getResponseBody());

		//System.out.println("这笔交易的状态为：" + cnpReponseMsg.getResponseCode());
		//System.out.println("这笔交易的交易编号为：" + cnpReponseMsg.getRRN());
		//System.out.println("这笔交易的外部控制编号为：" + cnpRequstMsg.getServiceChannelTraceNo());
		//System.out.println(cnpRequstMsg);
		Thread.sleep(sleepTime*1000L);
		return cnpReponseMsg;
	}

	public void exploring(AcceptAbilityTask acceptAbilityTask, List<Long> idBankPans) throws Exception {
		this.sleepTime = acceptAbilityTask.getSleepTime();
		BankPanInfo errBankPanInfo = new BankPanInfo();
		errBankPanInfo.setPan("4380880000000007");
		errBankPanInfo.setCardholder("王小二");
		errBankPanInfo.setNoId("32028119880223303X");
		errBankPanInfo.setCvv2("000");
		errBankPanInfo.setExpireDate("1299");
		errBankPanInfo.setNoCellPhone("13000000000");
		acceptAbilityTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ING);
		for (Long idBankPan : idBankPans) {
			BankPanInfo bankPanInfo = bankPanInfoMng.queryById(idBankPan);
			AcceptAbilitySubTask acceptAbilitySubTask = new AcceptAbilitySubTask();
			acceptAbilitySubTask.setIdBankPan(bankPanInfo.getIdBankPan());
			acceptAbilitySubTask.setIdTask(acceptAbilityTask.getIdTask());
			acceptAbilitySubTask.setTaskStatus(Dict.TaskStatus_Running);

			acceptAbilitySubTask = acceptAbilitySubTaskMng.save(acceptAbilitySubTask);

			CnpRequstMsg cnpRequstMsg = new CnpRequstMsg();
			cnpRequstMsg.setAmt(acceptAbilityTask.getAmt().toString());
			cnpRequstMsg.setServiceChannelType("2");
			cnpRequstMsg.setTerminalId(acceptAbilityTask.getTerminaId());
			cnpRequstMsg.setMerchantId(acceptAbilityTask.getMerchantId());
			cnpRequstMsg.setTxnType(acceptAbilityTask.getTxnInterface());
			cnpRequstMsg.setTermInMonths(acceptAbilityTask.getTermInMonths());			
			if (acceptAbilityTask.getAttachInfos().trim().length()>0){
				cnpRequstMsg.setAttachInfos("{" + acceptAbilityTask.getAttachInfos() + "}");
			}
			else{
				cnpRequstMsg.setAttachInfos("{\"111\":\"222\",\"222\":\"333\",\"333\":\"444\"}");
			}

			List<CheckedItemInfo> checkedItems = checkedItemInfoMng.getQueryByKeyAndCardType(Dict.Key_CNP_Defalut, bankPanInfo.getCardType());
			Boolean falg1 = exploring(cnpRequstMsg, bankPanInfo, errBankPanInfo, checkedItems, acceptAbilitySubTask);
			if (!falg1) {
				acceptAbilityTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
			}

		}

		acceptAbilityTaskMng.save(acceptAbilityTask);
	}
}
