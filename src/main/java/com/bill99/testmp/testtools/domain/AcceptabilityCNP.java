package com.bill99.testmp.testtools.domain;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;

public interface AcceptabilityCNP {

	/**
	 * 受理能力验证
	 * 
	 * @param acceptAbilitytTask
	 *            受理能力验证任务
	 * @param pans
	 *            卡号列表
	 * @throws Exception
	 */
	public void exploring(AcceptAbilityTask acceptAbilitytTask, List<Long> idBankPans) throws Exception;
}
