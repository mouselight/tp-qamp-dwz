package com.bill99.testmp.testtools.domain;

import javax.servlet.http.HttpServletResponse;

public interface AcceptabilityExpService {

	public void expAcceptabilityResult(Long idTask, HttpServletResponse response) throws Exception;

}
