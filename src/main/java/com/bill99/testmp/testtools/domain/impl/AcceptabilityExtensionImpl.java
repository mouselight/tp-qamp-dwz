package com.bill99.testmp.testtools.domain.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testtools.domain.AcceptabilityExtension;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.DDPDbResult;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;
import com.bill99.testmp.testtools.utils.HttpFixture;

public class AcceptabilityExtensionImpl implements AcceptabilityExtension {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaskLogInfoMng taskLogInfoMng;
	private String tempPath;
	private AcceptAbilityTaskMng acceptAbilityTaskMng;

	private String bankRespCodeKey = "";
	private String bankRespCodeDescKey = "";

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public void setTaskLogInfoMng(TaskLogInfoMng taskLogInfoMng) {
		this.taskLogInfoMng = taskLogInfoMng;
	}

	public TaskLogInfoMng getTaskLogInfoMng() {
		return this.taskLogInfoMng;
	}

	public AcceptAbilityTaskMng getAcceptAbilityTaskMng() {
		return acceptAbilityTaskMng;
	}

	public void setAcceptAbilityTaskMng(
			AcceptAbilityTaskMng acceptAbilityTaskMng) {
		this.acceptAbilityTaskMng = acceptAbilityTaskMng;
	}

	public void getExtFromDB(Long idTask) throws Exception {
		AcceptAbilityTask acceptAbilityTask = acceptAbilityTaskMng
				.queryById(idTask);
		if (acceptAbilityTask.getBankRespCodeKey() != null)
			this.bankRespCodeKey = acceptAbilityTask.getBankRespCodeKey()
					.trim();
		if (acceptAbilityTask.getBankRespCodeDescKey() != null)
			this.bankRespCodeDescKey = acceptAbilityTask
					.getBankRespCodeDescKey().trim();

		// 拿到全部操作数据
		List<TaskLogInfo> taskLogInfoList = taskLogInfoMng
				.queryByTaskId(idTask);
		// 拿到idtxn集合
		List<String> idTxnList = getIdTxnList(taskLogInfoList);
		// 合并一个查询SQL
		String tempSql = splitJointSql(idTxnList);
		if (tempSql.length() > 0) {
			// 有数据，去备库查数据，保存到QAMP
			String sqlStr = "select a.id_txn, a.resp_code, b.name, a.tunnel_data from maspos.t_txn_ctrl@maspos_201 a left join  "
					+ "maspos.t_Dict_Code_Map@maspos_201 b on a.resp_code = b.code where a.id_txn in ("
					+ tempSql
					+ ")  and   b.Class_Code = 'mas.edc.iso8583_response_code'";
			this.getDateFromQueryDB(sqlStr, idTxnList, idTask);
		}

	}

	private List<String> getIdTxnList(List<TaskLogInfo> taskLogInfoList) {
		List<String> idTxnList = new ArrayList<String>();
		for (TaskLogInfo taskLogInfo : taskLogInfoList) {
			String idTxn = taskLogInfo.getIdTxn();
			if (idTxn != null && !idTxn.equals("[null]")) {
				idTxnList.add(idTxn);
			}
		}
		return idTxnList;
	}

	private String splitJointSql(List<String> idTxnList) {
		StringBuffer sqlSB = new StringBuffer();
		for (String idTxn : idTxnList) {
			if (idTxn != null && !idTxn.equals("[null]")
					&& idTxn.startsWith("[") && idTxn.endsWith("]")) {
				idTxn = idTxn.substring(1, idTxn.length() - 1);
				sqlSB.append("'");
				sqlSB.append(idTxn);
				sqlSB.append("',");
			}
		}
		String sqlStr = sqlSB.toString();
		if (sqlStr.endsWith(",")) {
			sqlStr = sqlStr.substring(0, sqlStr.length() - 1);
		}
		return sqlStr;
	}

	private void getDateFromQueryDB(String sqlStr, List<String> idTxnList,
			Long taskId) {

		String excelfilename = this.tempPath + "txnCtrl-" + taskId.toString()
				+ ".xls";
		HttpFixture hf = new HttpFixture();
		hf.setUrl("http://query.99bill.com/report/login.jsp");
		hf.addHeaderValue("Content-Type", "application/x-www-form-urlencoded");
		try {
			hf.Get();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return;
		}
		String cookie = hf.getResponseheader("Set-Cookie");
		hf.addHeaderValue("Cookie", cookie);
		hf.setUrl("http://query.99bill.com/report/login.do");
		hf.addParamValue("userid", "linda.lou@99bill.com");
		hf.addParamValue("passwd", "linda2134923");
		try {
			hf.Post();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return;
		}

		boolean saveflag = false;
		try {
			hf.nextRequest();
			hf.setUrl("http://query.99bill.com/report/excelResult.do");
			hf.addHeaderValue("Content-Type",
					"application/x-www-form-urlencoded");
			hf.addHeaderValue("Cookie", cookie);
			hf.addParamValue("category", "seashell");
			hf.addParamValue("id", "sqlSearch");
			hf.addParamValue("pageSize", "40");
			hf.addParamValue("targetPage", "");
			hf.addParamValue("sql", sqlStr);
			hf.Post();
			// 下载excel
			saveflag = hf.saveResponse2File(excelfilename);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return;
		}
		if (saveflag) {
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(excelfilename);
				POIFSFileSystem fs = new POIFSFileSystem(fis);
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);
				if (sheet.getPhysicalNumberOfRows() > 1) {
					for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
						HSSFRow row = sheet.getRow(i);
						String idTxn = row.getCell((short) 0)
								.getStringCellValue().trim();
						String respCode = row.getCell((short) 1)
								.getStringCellValue().trim();
						String respCodeDesc = row.getCell((short) 2)
								.getStringCellValue().trim();
						String tunnelData = row.getCell((short) 3)
								.getStringCellValue().trim();
						this.saveTaskLogInfo(idTxn, respCode, respCodeDesc,
								tunnelData);
					}
				} else {
					logger.error("CNP受理能力验证--没有从备库获取到");
				}
				fis.close();

			} catch (Exception e) {
				if (fis != null)
					try {
						fis.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				e.printStackTrace();
			}
		}

	}

	private void saveTaskLogInfo(String idTxn, String respCode,
			String respCodeDesc, String tunnelData) {

		TaskLogInfo taskLogInfo = taskLogInfoMng.getTaskLogInfoByIdTxn("["
				+ zeroize(idTxn) + "]");

		taskLogInfo.setRespCode(respCode);
		taskLogInfo.setRespCodeDesc(respCodeDesc);
		if (bankRespCodeKey != null && bankRespCodeKey.length() > 0)
			taskLogInfo.setBankRespcode(this.getKeyFromJsonString(tunnelData,
					bankRespCodeKey));
		if (bankRespCodeDescKey != null && bankRespCodeDescKey.length() > 0)
			taskLogInfo.setBankRespcodeDesc(this.getKeyFromJsonString(
					tunnelData, bankRespCodeDescKey));

		taskLogInfoMng.save(taskLogInfo);
	}

	private String zeroize(String idTxn) {
		if (idTxn.length() < 13) {
			for (int i = 13; i > idTxn.length() - 1; i--) {
				idTxn = "0" + idTxn;
			}
		}
		return idTxn;
	}

	private String getKeyFromJsonString(String json, String key) {
		try {
			JSONObject jasonObject = JSONObject.fromObject(json);
			Map<String, Object> map = (Map) jasonObject;
			return map.get(key).toString();
		} catch (Exception ex) {
			return "";
		}
	}

}
