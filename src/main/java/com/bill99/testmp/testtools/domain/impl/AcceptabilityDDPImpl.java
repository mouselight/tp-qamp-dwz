package com.bill99.testmp.testtools.domain.impl;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import com.bill99.testmp.testtools.common.utils.AcceptAbilityUtils;
import com.bill99.testmp.testtools.domain.AcceptabilityCNP;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;
import com.bill99.testmp.testtools.orm.entity.DDPDbResult;
import com.bill99.testmp.testtools.orm.entity.DDPReponseMsg;
import com.bill99.testmp.testtools.orm.entity.DDPSingleRequestMsg;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilitySubTaskMng;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.BankPanInfoMng;
import com.bill99.testmp.testtools.orm.manager.CheckedItemInfoMng;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;
import com.bill99.testmp.testtools.utils.Dict;
import com.bill99.testmp.testtools.utils.HttpFixture;
import com.bill99.testmp.testtools.utils.NofM;
import com.bill99.testmp.testtools.utils.TaskLogInfoBuilder;

public class AcceptabilityDDPImpl implements AcceptabilityCNP {

	private TaskLogInfoMng taskLogInfoMng;

	private BankPanInfoMng bankPanInfoMng;

	private CheckedItemInfoMng checkedItemInfoMng;

	private AcceptAbilitySubTaskMng acceptAbilitySubTaskMng;
	
	private AcceptAbilityTaskMng acceptAbilityTaskMng;
	
	private DDPDbResult ddpDbResult; 
	
	private String tempPath;
	
	private String environment="stage2";
	
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public TaskLogInfoMng getTaskLogInfoMng() {
		return taskLogInfoMng;
	}

	public void setTaskLogInfoMng(TaskLogInfoMng taskLogInfoMng) {
		this.taskLogInfoMng = taskLogInfoMng;
	}

	public BankPanInfoMng getBankPanInfoMng() {
		return bankPanInfoMng;
	}

	public void setBankPanInfoMng(BankPanInfoMng bankPanInfoMng) {
		this.bankPanInfoMng = bankPanInfoMng;
	}

	public CheckedItemInfoMng getCheckedItemInfoMng() {
		return checkedItemInfoMng;
	}

	public void setCheckedItemInfoMng(CheckedItemInfoMng checkedItemInfoMng) {
		this.checkedItemInfoMng = checkedItemInfoMng;
	}

	public AcceptAbilitySubTaskMng getAcceptAbilitySubTaskMng() {
		return acceptAbilitySubTaskMng;
	}

	public void setAcceptAbilitySubTaskMng(
			AcceptAbilitySubTaskMng acceptAbilitySubTaskMng) {
		this.acceptAbilitySubTaskMng = acceptAbilitySubTaskMng;
	}

	public AcceptAbilityTaskMng getAcceptAbilityTaskMng() {
		return acceptAbilityTaskMng;
	}

	public void setAcceptAbilityTaskMng(AcceptAbilityTaskMng acceptAbilityTaskMng) {
		this.acceptAbilityTaskMng = acceptAbilityTaskMng;
	}

	/**
	 * 探测DDP受理能力
	 * 
	 * @param ddpSingleRequestMsg
	 *            包含基本信息的cnp请求
	 * @param bankPanInfo
	 *            正确的银行卡信息
	 * @param checkedItems
	 *            受理能力校验项
	 * @return
	 * 
	 */
	private boolean exploring(DDPSingleRequestMsg ddpSingleRequestMsg,
			BankPanInfo bankPanInfo, BankPanInfo errBankPanInfo,
			List<CheckedItemInfo> checkedItems,
			AcceptAbilitySubTask acceptAbilitySubTask) {
		try {
			Long subtaskId = acceptAbilitySubTask.getIdAaSubTask();
			DDPSingleRequestMsg ddpSingleRequestMsgCopy = new DDPSingleRequestMsg();
			PropertyUtils.copyProperties(ddpSingleRequestMsgCopy, ddpSingleRequestMsg);
			// 必选项目
			Set<Integer> requiredItems = new LinkedHashSet<Integer>();
			// 可选项目
			Set<Integer> optionItems = new LinkedHashSet<Integer>();
			// 最小输入项目
			Set<CheckedItemInfo> minItems = new LinkedHashSet<CheckedItemInfo>();
			// 最小输入外的其他项目
			Set<CheckedItemInfo> otherItems = new LinkedHashSet<CheckedItemInfo>();
			// 禁止输入项目
			Set<CheckedItemInfo> nonAcceptanceItems = new LinkedHashSet<CheckedItemInfo>();
			// 上送且校验
			Set<CheckedItemInfo> checkItems = new LinkedHashSet<CheckedItemInfo>();
			// 上送但不校验
			Set<CheckedItemInfo> unCheckItems = new LinkedHashSet<CheckedItemInfo>();
			// 返回报文
			DDPReponseMsg ddpReponseMsg = new DDPReponseMsg();
			// 交易成功标志
			boolean tranFlag = false;
			//String result = "";

			TaskLogInfo taskLogInfo;
			TaskLogInfo taskLogInfo2;
			// 第一轮探测

			for (int i = 0; i < checkedItems.size(); i++) {
				if (checkedItems.get(i).isRequired()) {
					requiredItems.add(i);
					// 先把必选项目加到最小输入项里
					minItems.add(checkedItems.get(i));
				} else
					optionItems.add(i);
			}
			// //System.out.println(requiredItems);
			// //System.out.println(optionItems);

			taskLogInfo = TaskLogInfoBuilder.instantiation("DDP", subtaskId, checkedItems);

			// 只发送必选参数
			for (int t : requiredItems) {
				PropertyUtils.setProperty(
						ddpSingleRequestMsg,
						checkedItems.get(t).getMapingReq(),
						PropertyUtils.getProperty(bankPanInfo,
								checkedItems.get(t).getItemId()).toString());
				taskLogInfo.changeStatus(checkedItems.get(t).getItemId(),
						TaskLogInfo.STATUS_TRUEVALUE);
				// 目前只支持身份证
				if (checkedItems.get(t).getItemId().equals("noId"))
					ddpSingleRequestMsg.setIdType("101");
			}

			ddpReponseMsg = sendRequet(ddpSingleRequestMsg);
			if (!checkAndSave(ddpReponseMsg, ddpSingleRequestMsg, taskLogInfo,
					acceptAbilitySubTask)) {
				// 不满足预期，退出
				return false;
			}
			
			if (ddpReponseMsg.getDealResult().equals(Dict.DDP_RESPONSECODE_SUCCESS)
					||taskLogInfo.getWorkorderStatus().equals("6")) {
				// 最小输入项为必选项
				tranFlag = true;
			} else {
				// 按照排列组合发送必选参数和可选参数的组合
				int max = optionItems.size();
				boolean successflag = false;
				for (int t = 1; t <= max; t++) {
					if (successflag)
						break;
					NofM nOfm = new NofM(t, optionItems.toArray(new Integer[0]));
					while (nOfm.hasNext()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("DDP", subtaskId, checkedItems);
						// Thread.sleep(2000);
						Object[] set = nOfm.next();
						ddpSingleRequestMsg = new DDPSingleRequestMsg();
						PropertyUtils.copyProperties(ddpSingleRequestMsg,
								ddpSingleRequestMsgCopy);
						ddpSingleRequestMsg.setSeqId();
						ddpSingleRequestMsg.setReqSeqNo();
						ddpSingleRequestMsg.setIdType(null);
						// 设置必填参数
						for (int requiredItem : requiredItems) {
							PropertyUtils.setProperty(
									ddpSingleRequestMsg,
									checkedItems.get(requiredItem)
											.getMapingReq(),
									PropertyUtils.getProperty(
											bankPanInfo,
											checkedItems.get(requiredItem)
													.getItemId()).toString());
							taskLogInfo.changeStatus(
									checkedItems.get(requiredItem).getItemId(),
									TaskLogInfo.STATUS_TRUEVALUE);
							if (checkedItems.get(requiredItem).getItemId().equals("noId"))
								ddpSingleRequestMsg.setIdType("101");
						}
						// 设置可选参数
						for (Object s : set) {
							for (Integer optionItem : optionItems) {
								if (s.equals(optionItem)) {
									PropertyUtils.setProperty(
													ddpSingleRequestMsg,
													checkedItems.get(optionItem.intValue())
															.getMapingReq(),
													PropertyUtils.getProperty(
																	bankPanInfo,
																	checkedItems
																			.get(optionItem
																					.intValue())
																			.getItemId())
															.toString());
									taskLogInfo.changeStatus(
											checkedItems.get(optionItem)
													.getItemId(),
											TaskLogInfo.STATUS_TRUEVALUE);
									if (checkedItems.get(optionItem).getItemId().equals("noId"))
										ddpSingleRequestMsg.setIdType("101");
								}
							}
						}
						ddpReponseMsg = sendRequet(ddpSingleRequestMsg);
						if (!checkAndSave(ddpReponseMsg, ddpSingleRequestMsg, taskLogInfo,
								acceptAbilitySubTask)) {
							// 不满足预期，退出
							return false;
						}

						// //System.out.println(ddpSingleRequestMsg);
						if (ddpReponseMsg.getDealResult().equals(
								Dict.DDP_RESPONSECODE_SUCCESS)||taskLogInfo.getWorkorderStatus().equals("6")) {
							// 返回成功，得到可选项目的最小输入项目集合
							for (Object s : set) {
								for (Integer optionItem : optionItems) {
									if (s.equals(optionItem)) {
										minItems.add(checkedItems
												.get(optionItem.intValue()));
									}
								}
							}
							successflag = true;
							tranFlag = true;
							break;
						}
					}
				}
			}
			//System.out.println("第一轮校验完毕");
			if (!tranFlag) {
				//System.out.println("第一轮校验完毕，未发现返回正确的交易");
				acceptAbilitySubTask
						.setResult("第一轮校验完毕，未发现返回正确的交易，系统可能存在异常，请排查");
				acceptAbilitySubTask
						.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
				acceptAbilitySubTaskMng.save(acceptAbilitySubTask);
				return false;
			} else {
				// 第二轮校验
				if (checkedItems.size() == minItems.size()) {
					// 最小输入集合与全集相等，不需要执行第二轮校验
				} else {
					// 获取最小输入项以外的集合
					Iterator<CheckedItemInfo> checkedItems2 = checkedItems.iterator();
					while (checkedItems2.hasNext())
						otherItems.add(checkedItems2.next());

					otherItems.removeAll(minItems);
					Iterator<CheckedItemInfo> otherItems2 = otherItems
							.iterator();
					while (otherItems2.hasNext()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("DDP", subtaskId, checkedItems);
						taskLogInfo2 = TaskLogInfoBuilder.instantiation("DDP", subtaskId, checkedItems);
						PropertyUtils.copyProperties(ddpSingleRequestMsg,
								ddpSingleRequestMsgCopy);
						ddpSingleRequestMsg.setSeqId();
						ddpSingleRequestMsg.setReqSeqNo();
						ddpSingleRequestMsg.setIdType(null);
						
						for (CheckedItemInfo minItem : minItems) {
							// 设置最小输入项的正确参数
							PropertyUtils.setProperty(
									ddpSingleRequestMsg,
									minItem.getMapingReq(), // ddpSingleRequestMsg
															// 的某字段做替换
									PropertyUtils.getProperty(bankPanInfo,
											minItem.getItemId()).toString());
							taskLogInfo.changeStatus(minItem.getItemId(), TaskLogInfo.STATUS_TRUEVALUE);
							taskLogInfo2.changeStatus(minItem.getItemId(),TaskLogInfo.STATUS_TRUEVALUE);
							if (minItem.getItemId().equals("noId"))
								ddpSingleRequestMsg.setIdType("101");
						}
						CheckedItemInfo otherItem = otherItems2.next();
						boolean flagA = false, flagB = false;
						// 其他项目设置为正确值发送
						
						PropertyUtils.setProperty(
								ddpSingleRequestMsg,
								otherItem.getMapingReq(),
								PropertyUtils.getProperty(bankPanInfo,
										otherItem.getItemId()).toString());
						taskLogInfo.changeStatus(otherItem.getItemId(),
								TaskLogInfo.STATUS_TRUEVALUE);
						if (otherItem.getItemId().equals("noId"))
							ddpSingleRequestMsg.setIdType("101");
						
						ddpReponseMsg = sendRequet(ddpSingleRequestMsg);
						if (!checkAndSave(ddpReponseMsg, ddpSingleRequestMsg, taskLogInfo,
								acceptAbilitySubTask)) {
							// 不满足预期，退出
							return false;
						}

						if (ddpReponseMsg.getDealResult().equals(Dict.DDP_RESPONSECODE_SUCCESS)||
								ddpDbResult.getWorkorderStatus().equals("6") ) {
							flagA = true;
						}
						
						ddpSingleRequestMsg.setSeqId();
						ddpSingleRequestMsg.setReqSeqNo();
						ddpSingleRequestMsg.setIdType(null);
						// 其他项目设置为错误值发送
						// PropertyUtils.copyProperties(taskLogInfo2,
						// taskLogInfo);
						// taskLogInfo= TaskLogInfoBuilder.instantiation("DDP,
						// subtaskId, checkedItems);
						// PropertyUtils.copyProperties(taskLogInfo,
						// taskLogInfo2);

						PropertyUtils.setProperty(
								ddpSingleRequestMsg,
								otherItem.getMapingReq(),
								PropertyUtils.getProperty(errBankPanInfo,
										otherItem.getItemId()).toString());
						if (otherItem.getItemId().equals("noId"))
							ddpSingleRequestMsg.setIdType("101");
						taskLogInfo2.changeStatus(otherItem.getItemId(),
								TaskLogInfo.STATUS_FAILVALUE);
						
						ddpReponseMsg = sendRequet(ddpSingleRequestMsg);
						if (!checkAndSave(ddpReponseMsg, ddpSingleRequestMsg, taskLogInfo2,
								acceptAbilitySubTask)) {
							// 不满足预期，退出
							return false;
						}
						
						if (ddpReponseMsg.getDealResult().equals(Dict.DDP_RESPONSECODE_SUCCESS)||
								ddpDbResult.getWorkorderStatus().equals("6") ) {
							flagB = true;
						}
						if (flagA && flagB) {
							// 上送，不校验
							unCheckItems.add(otherItem);
						}
						if (flagA && !flagB) {
							// 上送，校验
							checkItems.add(otherItem);
						}
						if (!flagA) {
							// 禁止上送
							nonAcceptanceItems.add(otherItem);
						}
					}
				}
				//System.out.println("第二轮校验完毕");

				Iterator<CheckedItemInfo> minItems2 = minItems.iterator();
				while (minItems2.hasNext()) {

					CheckedItemInfo minItem = minItems2.next();
					if (!minItem.isCorrectValue()) {
						taskLogInfo = TaskLogInfoBuilder.instantiation("DDP", subtaskId, checkedItems);
						// 没有必须正确的限制
						PropertyUtils.copyProperties(ddpSingleRequestMsg,
								ddpSingleRequestMsgCopy);
						ddpSingleRequestMsg.setSeqId();
						ddpSingleRequestMsg.setReqSeqNo();
						ddpSingleRequestMsg.setIdType(null);
						if (minItem.getItemId().equals("noId"))
							ddpSingleRequestMsg.setIdType("101");
						for (CheckedItemInfo minItem2 : minItems) {
							// 设置最小输入项的正确参数
							PropertyUtils.setProperty(
									ddpSingleRequestMsg,
									minItem2.getMapingReq(), // ddpSingleRequestMsg
																// 的某字段做替换
									PropertyUtils.getProperty(bankPanInfo,
											minItem2.getItemId()).toString());
							taskLogInfo.changeStatus(minItem2.getItemId(),
									TaskLogInfo.STATUS_TRUEVALUE);
							if (minItem2.getItemId().equals("noId"))
								ddpSingleRequestMsg.setIdType("101");
						}
						
						// 当前项目设置为错误值
						PropertyUtils.setProperty(
								ddpSingleRequestMsg,
								minItem.getMapingReq(),
								PropertyUtils.getProperty(errBankPanInfo,
										minItem.getItemId()).toString());
						taskLogInfo.changeStatus(minItem.getItemId(),
								TaskLogInfo.STATUS_FAILVALUE);
						
						ddpReponseMsg = sendRequet(ddpSingleRequestMsg);
						if (!checkAndSave(ddpReponseMsg, ddpSingleRequestMsg, taskLogInfo,
								acceptAbilitySubTask)) {
							// 不满足预期，退出
							return false;
						}
						if (ddpReponseMsg.getDealResult().equals(Dict.DDP_RESPONSECODE_SUCCESS)||
								ddpDbResult.getWorkorderStatus().equals("6") ) {
							unCheckItems.add(minItem);
						} else {
							checkItems.add(minItem);
						}
					}

				}

				//System.out.println("第三轮校验完毕");
				String result = "";
				result += "最小输入项集合：<br/>";
				for (CheckedItemInfo ci : minItems) {
					result += Dict.getItemChineseName(ci.getItemId()) + "、";
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "上送，不校验项集合：<br/>";
				if (unCheckItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : unCheckItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "上送，校验项集合：<br/>";
				if (checkItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : checkItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				result += "禁止上送项集合：<br/>";
				if (nonAcceptanceItems.size() == 0) {
					result += "无";
				} else {
					for (CheckedItemInfo ci : nonAcceptanceItems) {
						result += Dict.getItemChineseName(ci.getItemId()) + "、";
					}
				}
				if (result.endsWith("、")){
					result =result.substring(0,result.lastIndexOf("、"));
				}
				result += "<br/>";
				acceptAbilitySubTask.setResult(result);
			}
			
			acceptAbilitySubTask
					.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_DONE);
			acceptAbilitySubTaskMng.save(acceptAbilitySubTask);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	private void changeTaskLogInfo(TaskLogInfo taskLogInfo,
			DDPReponseMsg ddpReponseMsg, DDPSingleRequestMsg ddpSingleRequestMsg) {
		if(StringUtils.isEmpty(ddpReponseMsg.getDealResult())){
			taskLogInfo.setMemoFail("返回报文无交易状态");
		}else{
			taskLogInfo.setTxnStatus(ddpReponseMsg.getDealResult());
		} 		
		taskLogInfo.setIdTxn(ddpSingleRequestMsg.getSeqId());
	}
	
	private DDPReponseMsg sendRequet(DDPSingleRequestMsg ddpSingleRequestMsg)
			throws Exception {
		HttpFixture hf = new HttpFixture();
		hf.setUrl("http://192.168.126.142:8084/AppPkiClient/merchantDebitDummyDebitSingle.htm?method=debitSingle");
		hf.addHeaderValue("Content-Type", "application/x-www-form-urlencoded");
		//System.out.println(ddpSingleRequestMsg);
		hf.addParamValue(ddpSingleRequestMsg);
		hf.Post();
		//System.out.println(hf.getResponseBody());
		DDPReponseMsg ddpReponseMsg = new DDPReponseMsg();
		ddpReponseMsg.setAmount(hf.saveParamLeftstrRightstr("<tns:amount>","</tns:amount>"));		
		ddpReponseMsg.setDealResult(hf.saveParamLeftstrRightstr("<tns:dealResult>","</tns:dealResult>"));
		ddpReponseMsg.setFailCode(hf.saveParamLeftstrRightstr("<tns:failCode>","</tns:failCode>"));
		ddpReponseMsg.setFailreason(hf.saveParamLeftstrRightstr("<tns:failreason>","</tns:failreason>"));
		ddpReponseMsg.setFee(hf.saveParamLeftstrRightstr("<tns:fee>","</tns:fee>"));
		ddpReponseMsg.setRemark(hf.saveParamLeftstrRightstr("<tns:remark>","</tns:remark>"));
		ddpReponseMsg.setSeqId(hf.saveParamLeftstrRightstr("<tns:seqId>","</tns:seqId>"));
		ddpReponseMsg.setSeqno(hf.saveParamLeftstrRightstr("<tns:seqno>","</tns:seqno>"));
		//System.out.println("这笔交易的状态为：" + ddpReponseMsg.getDealResult());		
		return ddpReponseMsg;
	}

	private void checkDB(String merchantordern) {
		if (this.environment.equals("stage2")) {
			merchantordern = "20130315102817N105";
		}
		this.ddpDbResult = new DDPDbResult();
		String sqlStr = "SELECT * FROM ( select w.id as 工单ID,o.status as 状态,w.failreason as 工单失败原因 From Ddp.Debitbatchrequest Br, Ddp.Debitbatchrequestitem i, Ddp.Directdebitorder o, Ddp.Debitrequest r, Ddp.Debitworkorder w Where Br.Id = i.Debitbatchrequestid And i.Debitorderid = o.Id And r.Id = w.Debitrequestid And r.Requestid = to_char(o.Id) And i.merchantorderno='"
				+ merchantordern + "') temp where rownum <= 3000";
		//System.out.println(sqlStr);
		String excelfilename = this.tempPath + merchantordern + ".xls";
		HttpFixture hf = new HttpFixture();
		hf.setUrl("http://query.99bill.com/report/login.jsp");
		hf.addHeaderValue("Content-Type", "application/x-www-form-urlencoded");
		try {
			hf.Get();
		} catch (Exception ex) {
			return;
		}
		String cookie = hf.getResponseheader("Set-Cookie");
		hf.addHeaderValue("Cookie", cookie);
		hf.setUrl("http://query.99bill.com/report/login.do");
		hf.addParamValue("userid", "linda.lou@99bill.com");
		hf.addParamValue("passwd", "linda2134923");
		try {
			hf.Post();
		} catch (Exception ex) {
			return;
		}

		boolean saveflag = false;		
		int i=30;
		while (i>0){
			try {
				i--;
				hf.nextRequest();
				hf.setUrl("http://query.99bill.com/report/excelResult.do");
				hf.addHeaderValue("Content-Type", "application/x-www-form-urlencoded");
				hf.addHeaderValue("Cookie", cookie);
				hf.addParamValue("category", "seashell");
				hf.addParamValue("id", "sqlSearch");
				hf.addParamValue("pageSize", "40");
				hf.addParamValue("targetPage", "1");
				hf.addParamValue("sql", sqlStr);
				hf.Post();
				saveflag = hf.saveResponse2File(excelfilename);
				if (saveflag) {
					try {
						FileInputStream fis = new FileInputStream(excelfilename);
						POIFSFileSystem fs = new POIFSFileSystem(fis);
						HSSFWorkbook wb = new HSSFWorkbook(fs);
						HSSFSheet sheet = wb.getSheetAt(0);
						HSSFRow row = sheet.getRow(1);
						String workorderStatus = row.getCell((short) 1).getStringCellValue().trim();
						if (workorderStatus.equals("4") || workorderStatus.equals("6") || i==0) {
							this.ddpDbResult.setWorkorderId(row.getCell((short) 0).getStringCellValue().trim());
							this.ddpDbResult.setWorkorderStatus(row.getCell((short) 1).getStringCellValue().trim());
							this.ddpDbResult.setFailreason(row.getCell((short) 2).getStringCellValue().trim());
							fis.close();
							return;
						}else{
							fis.close();
							Thread.sleep(10000);							
						}
					} catch (Exception ex) {	
						ex.printStackTrace();
						return;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}	
	}
	
	private boolean checkAndSave(DDPReponseMsg ddpReponseMsg,DDPSingleRequestMsg ddpSingleRequestMsg,
			TaskLogInfo taskLogInfo,AcceptAbilitySubTask acceptAbilitySubTask
			) throws Exception{
		this.ddpDbResult = new DDPDbResult();
		changeTaskLogInfo(taskLogInfo, ddpReponseMsg, ddpSingleRequestMsg);
		if(StringUtils.isEmpty(ddpReponseMsg.getDealResult()) || ddpReponseMsg.getDealResult().equals(Dict.DDP_RESPONSECODE_ERROR)){
			// 没有得到返回的数据或者返回01004，退出
			taskLogInfo.setMemoFail(ddpReponseMsg.getFailreason());
			taskLogInfoMng.save(taskLogInfo);
			acceptAbilitySubTask.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
			acceptAbilitySubTask.setResult("没有得到返回的数据或者无法通过DDP自身的校验");
			acceptAbilitySubTaskMng.save(acceptAbilitySubTask);				
			return false;
		}
		
		this.checkDB(ddpSingleRequestMsg.getSeqId());
		if(ddpDbResult==null){				
			// 下载出现异常，退出
			taskLogInfoMng.save(taskLogInfo);
			acceptAbilitySubTask.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
			acceptAbilitySubTask.setResult("校验数据库记录时出现异常");
			acceptAbilitySubTaskMng.save(acceptAbilitySubTask);				
			return false;
		}
		taskLogInfo.setWorkorderId(ddpDbResult.getWorkorderId());
		taskLogInfo.setMemoFail(ddpDbResult.getFailreason());
		taskLogInfo.setWorkorderStatus(ddpDbResult.getWorkorderStatus());
		taskLogInfo.setTxnMsg(ddpReponseMsg.getFailCode()+":"+ddpReponseMsg.getFailreason());
		taskLogInfoMng.save(taskLogInfo);
		
		if(!ddpDbResult.getWorkorderStatus().equals("4")&&!ddpDbResult.getWorkorderStatus().equals("6")){
			// 查询结果不是4、6，退出
			acceptAbilitySubTask.setTaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
			acceptAbilitySubTask.setResult("工单状态不是成功或失败，无法继续");
			acceptAbilitySubTaskMng.save(acceptAbilitySubTask);
			return false;
		}
		
		return true;
		
	}
	
	public void exploring(AcceptAbilityTask acceptAbilityTask,
			List<Long> idBankPans) throws Exception {
		BankPanInfo errBankPanInfo = new BankPanInfo();
		errBankPanInfo.setPan("4380880000000007");
		errBankPanInfo.setCardholder("王二麻子");
		errBankPanInfo.setNoId("320102197806041616");
		errBankPanInfo.setCvv2("000");
		errBankPanInfo.setExpireDate("0101");
		errBankPanInfo.setNoCellPhone("13000000000");
		acceptAbilityTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_DONE);
		for (Long idBankPan : idBankPans) {
			BankPanInfo bankPanInfo = bankPanInfoMng.queryById(idBankPan);
			AcceptAbilitySubTask acceptAbilitySubTask = new AcceptAbilitySubTask();
			acceptAbilitySubTask.setIdBankPan(bankPanInfo.getIdBankPan());
			acceptAbilitySubTask.setIdTask(acceptAbilityTask.getIdTask());
			acceptAbilitySubTask.setTaskStatus(Dict.TaskStatus_Running);

			acceptAbilitySubTask = acceptAbilitySubTaskMng
					.save(acceptAbilitySubTask);

			DDPSingleRequestMsg ddpSingleRequestMsg = new DDPSingleRequestMsg();
			ddpSingleRequestMsg.setAmount(acceptAbilityTask.getAmt().toString());
			ddpSingleRequestMsg.setMemberCode(acceptAbilityTask.getMerchantId());
			ddpSingleRequestMsg.setContractId(acceptAbilityTask.getContractNo());
			ddpSingleRequestMsg.setEnvironment(this.environment);
			ddpSingleRequestMsg.setBankId(bankPanInfo.getBankCode());			
			if (bankPanInfo.getCardType().equals(Dict.CardType_CreditCard))
				ddpSingleRequestMsg.setAccType(Dict.CardType_CreditCard_DDP);
			if (bankPanInfo.getCardType().equals(Dict.CardType_DebitCard))
				ddpSingleRequestMsg.setAccType(Dict.CardType_DebitCard_DDP);	
				
			List<CheckedItemInfo> checkedItems;
			if (acceptAbilityTask.getTxnInterface().equals("901")){
				checkedItems = checkedItemInfoMng.getQueryByKeyAndCardType(Dict.Key_DDP_901,bankPanInfo.getCardType());
			}
			else if (acceptAbilityTask.getTxnInterface().equals("302")){
				checkedItems = checkedItemInfoMng.getQueryByKeyAndCardType(Dict.Key_DDP_302,bankPanInfo.getCardType());
			}
			else{	
				checkedItems = checkedItemInfoMng.getQueryByKeyAndCardType(Dict.Key_DDP_Defalut,bankPanInfo.getCardType());
			}
			Boolean falg1 = exploring(ddpSingleRequestMsg, bankPanInfo, errBankPanInfo, checkedItems,
					acceptAbilitySubTask);
			if (!falg1){
				acceptAbilityTask.setSubtaskStatus(AcceptAbilityUtils.ACCEPTABILITY_TASK_STATE_ERROR);
			}
			
		}
		
		acceptAbilityTaskMng.save(acceptAbilityTask);
	}

	
	

}
