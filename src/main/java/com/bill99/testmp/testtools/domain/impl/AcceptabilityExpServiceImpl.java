package com.bill99.testmp.testtools.domain.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.bill99.testmp.testtools.domain.AcceptabilityExpService;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilitySubTaskMng;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;
import com.bill99.testmp.testtools.utils.CardTypeEnum;
import com.bill99.testmp.testtools.utils.TxnInterfaceEnum;

public class AcceptabilityExpServiceImpl implements AcceptabilityExpService {
	private static Log logger = LogFactory.getLog(AcceptabilityExpServiceImpl.class);

	private AcceptAbilityTaskMng acceptAbilityTaskMng;
	private AcceptAbilitySubTaskMng acceptAbilitySubTaskMng;
	private TaskLogInfoMng taskLogInfoMng;

	private String getFlagByState(Character type) {
		String flag = type.toString();
		switch (type) {
		case '0'://0 不校验
			flag = "";
			break;
		case '1'://1 输入正确值
			flag = "√";
			break;
		case '2'://2 输入错误值
			flag = "×";
			break;
		case '3'://3 未输入
			flag = "";
			break;
		}
		return flag;
	}

	public void expAcceptabilityResult(Long idTask, HttpServletResponse response) throws Exception {

		WritableWorkbook workbook = null;
		ServletOutputStream os = null;
		try {
			AcceptAbilityTask acceptAbilityTask = acceptAbilityTaskMng.queryById(idTask);

			String fileName = "银行受理能力验证报告" + "_" + acceptAbilityTask.getpKey();
			fileName = new String(fileName.getBytes(), "iso-8859-1");
			response.setCharacterEncoding("gb2312");
			response.reset();
			response.setHeader("pragma", "no-cache");
			//设置下载格式
			response.setHeader("Content-Type", "application/vnd.ms-excel.numberformat:@;charset=gb2312");
			response.addHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".xls\"");// 点击导出excle按钮时候页面显示的默认名称
			response.setHeader("Connection", "close");
			//获取输出流
			os = response.getOutputStream();
			workbook = Workbook.createWorkbook(os);
			//设置单元格样式
			WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK));//字体,大小,加粗,斜体,下划线,颜色
			titleFormat.setBackground(Colour.ICE_BLUE);//背景色
			//设置分隔符样式
			WritableCellFormat splitFormat = new WritableCellFormat();
			splitFormat.setAlignment(Alignment.CENTRE);//水平居中
			//设置字段类型
			WritableCellFormat defaultFormat = new WritableCellFormat(NumberFormats.TEXT);

			WritableCellFormat wrappedText = new WritableCellFormat(WritableWorkbook.HYPERLINK_FONT);
			wrappedText.setWrap(true);//可换行的label样式

			List<AcceptAbilitySubTask> acceptAbilitySubTasks = acceptAbilitySubTaskMng.queryForList(idTask);

			StringBuilder resultSb = new StringBuilder();

			int sheetIndex = 0;
			for (AcceptAbilitySubTask acceptAbilitySubTask : acceptAbilitySubTasks) {
				int line = 1;
				List<TaskLogInfo> taskLogInfos = taskLogInfoMng.queryBySubTaskId(acceptAbilitySubTask.getIdAaSubTask());

				//创建Sheet
				WritableSheet sheet = workbook.createSheet(sheetIndex + "." + acceptAbilitySubTask.getBankPanInfo().getMemo(), sheetIndex);
				//			设置Head
				sheet.addCell(new Label(0, 0, "测试编号", titleFormat));
				sheet.addCell(new Label(1, 0, "银行名称", titleFormat));
				//sheet.addCell(new Label(2, 0, "测试项输入", titleFormat));
				sheet.addCell(new Label(2, 0, "交易类型(必填)", titleFormat));
				sheet.addCell(new Label(3, 0, "卡类型", titleFormat));
				sheet.addCell(new Label(4, 0, "缩略卡号(必填)", titleFormat));
				//			sheet.addCell(new Label(5, 0, "金额（必填）", titleFormat));
				sheet.addCell(new Label(5, 0, "有效期", titleFormat));
				sheet.addCell(new Label(6, 0, "CVV2", titleFormat));
				sheet.addCell(new Label(7, 0, "证件号码", titleFormat));
				sheet.addCell(new Label(8, 0, "姓名", titleFormat));
				sheet.addCell(new Label(9, 0, "电话号码", titleFormat));
				sheet.addCell(new Label(10, 0, "交易编号", titleFormat));
				sheet.addCell(new Label(11, 0, "交易结果", titleFormat));
				sheet.addCell(new Label(12, 0, "受理时长", titleFormat));
				sheet.addCell(new Label(13, 0, "外部跟踪号", titleFormat));
				
				sheet.addCell(new Label(14, 0, "授权通道原始返回码", titleFormat));
				sheet.addCell(new Label(15, 0, "授权通道原始返回码中文解释", titleFormat));
				sheet.addCell(new Label(16, 0, "快钱返回码", titleFormat));
				sheet.addCell(new Label(17, 0, "快钱返回码中文解释", titleFormat));

				sheet.setColumnView(0, 12);//设置列宽
				sheet.setColumnView(1, 12);//设置列宽
				//sheet.setColumnView(2, 20);//设置列宽
				sheet.setColumnView(2, 18);//设置列宽
				sheet.setColumnView(3, 9);//设置列宽
				sheet.setColumnView(4, 18);//设置列宽
				sheet.setColumnView(5, 9);//设置列宽
				sheet.setColumnView(6, 11);//设置列宽
				sheet.setColumnView(7, 11);//设置列宽
				sheet.setColumnView(8, 11);//设置列宽
				sheet.setColumnView(9, 11);//设置列宽
				sheet.setColumnView(10, 20);//设置列宽
				sheet.setColumnView(11, 11);//设置列宽
				sheet.setColumnView(12, 11);//设置列宽
				sheet.setColumnView(13, 26);//设置列宽
				sheet.setColumnView(14, 11);//设置列宽
				sheet.setColumnView(15, 26);//设置列宽
				sheet.setColumnView(16, 11);//设置列宽
				sheet.setColumnView(17, 26);//设置列宽

				for (TaskLogInfo taskLogInfo : taskLogInfos) {
					//测试编号
					sheet.addCell(new Label(0, line, String.valueOf(line), defaultFormat));
					//银行名称
					sheet.addCell(new Label(1, line, acceptAbilitySubTask.getBankPanInfo().getBankName(), defaultFormat));
					//测试项输入
					//sheet.addCell(new Label(2, line, String.valueOf(line), defaultFormat));
					//交易类型(必填)
					sheet.addCell(new Label(2, line, TxnInterfaceEnum.getTxnTypeVal(acceptAbilityTask.getTxnInterface()), defaultFormat));
					//卡类型
					sheet.addCell(new Label(3, line, CardTypeEnum.getTxnTypeVal(acceptAbilitySubTask.getBankPanInfo().getCardType()), defaultFormat));
					//缩略卡号(必填)

					String pan = acceptAbilitySubTask.getBankPanInfo().getPan();
					if (StringUtils.hasLength(pan) && pan.length() > 6) {
						pan = pan.substring(0, 6) + "****" + pan.substring(pan.length() - 4, pan.length());
					}

					sheet.addCell(new Label(4, line, pan, defaultFormat));
					//有效期
					sheet.addCell(new Label(5, line, getFlagByState(taskLogInfo.getExpireDate().toCharArray()[0]), defaultFormat));
					//CVV2
					sheet.addCell(new Label(6, line, getFlagByState(taskLogInfo.getCvv2().toCharArray()[0]), defaultFormat));
					//证件号码
					sheet.addCell(new Label(7, line, getFlagByState(taskLogInfo.getNoId().toCharArray()[0]), defaultFormat));
					//姓名
					sheet.addCell(new Label(8, line, getFlagByState(taskLogInfo.getCardholder().toCharArray()[0]), defaultFormat));
					//电话号码
					sheet.addCell(new Label(9, line, getFlagByState(taskLogInfo.getNoCellPhone().toCharArray()[0]), defaultFormat));
					//交易编号
					sheet.addCell(new Label(10, line, taskLogInfo.getIdTxn(), defaultFormat));
					//交易结果
					sheet.addCell(new Label(11, line, taskLogInfo.getTxnMsg(), defaultFormat));
					//受理时长
					sheet.addCell(new Label(12, line, "实时", defaultFormat));
					//外部跟踪号
					sheet.addCell(new Label(13, line, taskLogInfo.getExtTraceNo(), defaultFormat));
					
					sheet.addCell(new Label(14, line, taskLogInfo.getBankRespcode(), defaultFormat));
					sheet.addCell(new Label(15, line, taskLogInfo.getBankRespcodeDesc(), defaultFormat));
					sheet.addCell(new Label(16, line, taskLogInfo.getRespCode(), defaultFormat));
					sheet.addCell(new Label(17, line, taskLogInfo.getRespCodeDesc(), defaultFormat));
					line++;
				}
				if (StringUtils.hasLength(acceptAbilitySubTask.getResult())) {
					resultSb.append(acceptAbilitySubTask.getResult().replace("<br/>", "\r\n"));
				}
				resultSb.append("\r\n");
				sheet.addCell(new Label(1, line + 3, resultSb.toString(), defaultFormat));// 测试结果

				sheetIndex++;
			}
			workbook.write();
		} catch (IOException e) {
			logger.error("-- ConfirmReport , Workbook.Write Error --", e);
			throw new Exception(e.getMessage());
		} catch (WriteException e) {
			logger.error("-- ConfirmReport , Create Writable Error --", e);
			throw new Exception(e.getMessage());
		} finally {
			try {
				if (null != workbook) {
					workbook.close();
				}
				if (null != os) {
					os.close();
				}
			} catch (WriteException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			} catch (IOException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			}

		}

	}

	public void setAcceptAbilityTaskMng(AcceptAbilityTaskMng acceptAbilityTaskMng) {
		this.acceptAbilityTaskMng = acceptAbilityTaskMng;
	}

	public void setAcceptAbilitySubTaskMng(AcceptAbilitySubTaskMng acceptAbilitySubTaskMng) {
		this.acceptAbilitySubTaskMng = acceptAbilitySubTaskMng;
	}

	public void setTaskLogInfoMng(TaskLogInfoMng taskLogInfoMng) {
		this.taskLogInfoMng = taskLogInfoMng;
	}

	public static void main(String[] args) {

		String pan = "PPP111OOO";
		pan = pan.substring(0, 6) + "****" + pan.substring(pan.length() - 4, pan.length());
		System.out.println(pan);
	}
}
