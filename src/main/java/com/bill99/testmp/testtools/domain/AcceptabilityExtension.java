package com.bill99.testmp.testtools.domain;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;

/**
 * 受理能力扩展
 * @author hongzhi.zheng
 *
 */
public interface AcceptabilityExtension {

	/**
	 * 从数据库获取扩展数据
	 * 
	 * @param idTask
	 *            任务ID
	 * @throws Exception
	 */
	public void getExtFromDB(Long idTask) throws Exception;
}
