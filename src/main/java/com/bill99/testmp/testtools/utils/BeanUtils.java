package com.bill99.testmp.testtools.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.bill99.testmp.testtools.orm.entity.CnpReponseMsg;



public class BeanUtils {
	public static CnpReponseMsg str2CnpReponseMsg(String str) {
		String[] cnpReponseParaList = str.split("<br>");
		CnpReponseMsg cnpReponseMsgobj = new CnpReponseMsg();
		try {
			Class<?> clazz = cnpReponseMsgobj.getClass();
			cnpReponseMsgobj = (CnpReponseMsg) clazz.newInstance();
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				PropertyDescriptor pd = new PropertyDescriptor(f.getName(),
						clazz);
				Method wM = pd.getWriteMethod();
				for (String cnpReponsePara : cnpReponseParaList) {
					String s[] = cnpReponsePara.split("=");
					if (s.length != 2) {
						break;
					}
					if (s[0].trim().equals(f.getName())) {
						wM.invoke(cnpReponseMsgobj, s[1]);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnpReponseMsgobj;
	}
	
	
}
