package com.bill99.testmp.testtools.utils;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

public class TaskLogInfoBuilder {

	public static TaskLogInfo instantiation(String productId, Long subtaskId, List<CheckedItemInfo> checkedItems) {
		TaskLogInfo taskLogInfo = new TaskLogInfo();
		taskLogInfo.setProductId(productId);
		taskLogInfo.setSubtaskId(subtaskId);
		taskLogInfo.init();
		for (CheckedItemInfo checkedItem : checkedItems) {
			if (checkedItem.getItemId().equals("pan"))
				taskLogInfo.setPan(TaskLogInfo.STATUS_NONVALUE.toString());
			if (checkedItem.getItemId().equals("expireDate"))
				taskLogInfo.setExpireDate(TaskLogInfo.STATUS_NONVALUE.toString());
			if (checkedItem.getItemId().equals("cvv2"))
				taskLogInfo.setCvv2(TaskLogInfo.STATUS_NONVALUE.toString());
			if (checkedItem.getItemId().equals("cardholder"))
				taskLogInfo.setCardholder(TaskLogInfo.STATUS_NONVALUE.toString());
			if (checkedItem.getItemId().equals("noId"))
				taskLogInfo.setNoId(TaskLogInfo.STATUS_NONVALUE.toString());
			if (checkedItem.getItemId().equals("noCellPhone"))
				taskLogInfo.setNoCellPhone(TaskLogInfo.STATUS_NONVALUE.toString());
		}
		return taskLogInfo;
	}
}
