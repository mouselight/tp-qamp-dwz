package com.bill99.testmp.testtools.utils;

/**
 * 在1-M中找出N个元素的组合
 * 
 * @author hongzhi.zheng
 * 
 */
public class NofM {

	private int m;
	private Object[] array;
	private int[] set;
	private boolean first = true;
	private int position;

	public NofM(int n, Object[] array) {
		this.array = array;
		this.m = array.length;
		first = true;
		position = n - 1;
		set = new int[n];
		for (int i = 0; i < n; i++)
			set[i] = i + 1;
	}

	public boolean hasNext() {
		if (m == set.length && first) {
			return true;
		} else {
			return set[0] < m - set.length + 1;
		}
	}

	public Object[] next() {
		if (first) {
			first = false;
		} else {
			if (set[set.length - 1] == m)
				position--;
			else
				position = set.length - 1;
			set[position]++;
			for (int i = position + 1; i < set.length; i++)
				set[i] = set[i - 1] + 1;
		}
		Object re[] = new Object[set.length];
		for (int i = 0; i < re.length; i++) {
			re[i] = array[set[i]-1];
		}
		return re;
	}

	public static void main(String[] args) {
		String [] testArray = new String[]{"卡号","姓名","证件号码","有效期","CVV2","手机号"};
		for (int t = 1; t <= testArray.length; t++) {
			NofM nOfm = new NofM(t, testArray);
			while (nOfm.hasNext()) {
				Object[] set = nOfm.next();
				for(Object s:set){
					System.out.print(s+", ");
				}
				System.out.println();
			}
		}
	}
}