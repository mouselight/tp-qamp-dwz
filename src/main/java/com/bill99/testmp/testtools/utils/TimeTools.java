package com.bill99.testmp.testtools.utils;

import java.text.SimpleDateFormat;

public class TimeTools {
	/**
	 * 根据时间得到序列
	 * 
	 * @return
	 */
	public static String getTimeMillisSequence() {
		long nanoTime = System.nanoTime();
		String preFix = "";
		if (nanoTime < 0) {
			preFix = "A";// 负数补位A保证负数排在正数Z前面,解决正负临界值(如A9223372036854775807至Z0000000000000000000)问题。
			nanoTime = nanoTime + Long.MAX_VALUE + 1;
		} else {
			preFix = "Z";
		}
		String nanoTimeStr = String.valueOf(nanoTime);
		nanoTimeStr = nanoTimeStr.substring(9, nanoTimeStr.length());

		nanoTimeStr = preFix + nanoTimeStr;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS"); // 24小时制
		String timeMillisSequence = sdf.format(System.currentTimeMillis())
				+ nanoTimeStr;
		return timeMillisSequence;
	}

	/**
	 * generate sequence
	 * 
	 * @return
	 */
	public static String getGenerateSequence(String format) {
		SimpleDateFormat sf = new SimpleDateFormat(format);
		return sf.format(System.currentTimeMillis());
	}
}
