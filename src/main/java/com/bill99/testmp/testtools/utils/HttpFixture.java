package com.bill99.testmp.testtools.utils;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;


class MySSLSocketFactory implements SecureProtocolSocketFactory {
	private SSLContext sslcontext = null;

	private SSLContext createSSLContext() {
		SSLContext sslcontext = null;
		try {
			sslcontext = SSLContext.getInstance("SSL");
			sslcontext.init(null,
					new TrustManager[] { new TrustAnyTrustManager() },
					new java.security.SecureRandom());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return sslcontext;
	}

	private SSLContext getSSLContext() {
		if (this.sslcontext == null) {
			this.sslcontext = createSSLContext();
		}
		return this.sslcontext;
	}

	public Socket createSocket(Socket socket, String host, int port,
			boolean autoClose) throws IOException, UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(socket, host,
				port, autoClose);
	}

	public Socket createSocket(String host, int port) throws IOException,
			UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(host, port);
	}

	public Socket createSocket(String host, int port, InetAddress clientHost,
			int clientPort) throws IOException, UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(host, port,
				clientHost, clientPort);
	}

	public Socket createSocket(String host, int port, InetAddress localAddress,
			int localPort, HttpConnectionParams params) throws IOException,
			UnknownHostException, ConnectTimeoutException {
		if (params == null) {
			throw new IllegalArgumentException("Parameters may not be null");
		}
		int timeout = params.getConnectionTimeout();
		SocketFactory socketfactory = getSSLContext().getSocketFactory();
		if (timeout == 0) {
			return socketfactory.createSocket(host, port, localAddress,
					localPort);
		} else {
			Socket socket = socketfactory.createSocket();
			SocketAddress localaddr = new InetSocketAddress(localAddress,
					localPort);
			SocketAddress remoteaddr = new InetSocketAddress(host, port);
			socket.bind(localaddr);
			socket.connect(remoteaddr, timeout);
			return socket;
		}
	}

	private static class TrustAnyTrustManager implements X509TrustManager {

		public void checkClientTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[] {};
		}
	}

}

/**
 * @author Peter.sun
 */
public class HttpFixture {

	protected String url = null;
	protected Map<String, String> paramlist = new HashMap<String, String>();
	protected Map<String, String> headerlist = new HashMap<String, String>();
	protected int status = 0;
	protected byte[] responseBody = null;
	protected String Body = "";
	protected Header[] headers = null;
	protected int fileno = 1;
	protected String requestbody = "";
	protected String encode = "utf-8";
	HttpClient client = null;
	protected int requesttimeout = 120000;
	protected int connecttimeout = 20000;
	private boolean DoAuth = false;

	public void setRequesttimeout(int time) {
		this.requesttimeout = time;
	}

	public void setConnecttimeout(int time) {
		this.connecttimeout = time;
	}

	/**
	 * 获得请求返回的code
	 */
	public int getStatus() {
		return this.status;
	}

	public void nextRequest() {
		url = null;
		paramlist = new HashMap<String, String>();
		status = 0;
		responseBody = null;
		Body = "";
		headers = null;
		requestbody = "";
	}

	public void clearDefinedheaders() {
		headerlist = new HashMap<String, String>();
	}

	public HttpFixture() {
		client = new HttpClient();
	}

	/**
	 * 设置请求的url
	 */
	public void setUrl(String url) {		
		if (url.toLowerCase().startsWith("https://")){
			String suburl = url.substring(8, url.length());
			
			String[] hostport = suburl.substring(0, suburl.indexOf("/")).split(":");
			String host = hostport[0];
			suburl = suburl.substring(host.length());
			int port = 443;
			if (hostport.length>1){
				port = Integer.valueOf(hostport[1]);
				suburl = suburl.substring((":"+String.valueOf(port)).length());
			}
			@SuppressWarnings("deprecation")
			Protocol myhttps = new Protocol("https", new MySSLSocketFactory(), port);
			client.getHostConfiguration().setHost(host, port, myhttps);
			this.url  = suburl;
		}else{
			this.url = url;
		}
		
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}

	/**
	 * 添加请求参数的键值对
	 */
	public void addParamValue(String paramname, String value) {
		try {
			// paramlist.put(paramname, value);
			if (paramname.length() != paramname.getBytes().length
					&& value.length() != value.getBytes().length) {
				this.requestbody += URLEncoder.encode(paramname, this.encode)
						+ "=" + URLEncoder.encode(value, this.encode) + "&";
			}
			if (paramname.length() == paramname.getBytes().length
					&& value.length() != value.getBytes().length) {
				this.requestbody += paramname + "="
						+ URLEncoder.encode(value, this.encode) + "&";
			}
			if (paramname.length() != paramname.getBytes().length
					&& value.length() == value.getBytes().length) {
				this.requestbody += URLEncoder.encode(paramname, this.encode)
						+ "=" + value + "&";
			}
			if (paramname.length() == paramname.getBytes().length
					&& value.length() == value.getBytes().length) {
				this.requestbody += paramname + "=" + value + "&";
			}
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
	}

	public void addHeaderValue(String headername, String headervalue) {
		headerlist.put(headername, headervalue);
	}

	public String getHeaderValue(String headername) {
		return headerlist.get(headername);
	}

	/**
	 * 添加请求体
	 */
	public void addRequestBody(String reqbody) {
		this.requestbody += reqbody;
	}

	public String getResponseheader(String headername) {
		for (Header header : headers) {
			if (header.getName().equals(headername)) {
				return header.getValue();
			}
		}
		return "";
	}

	public String getResponseheaders() {
		String headerstr = "";
		for (Header header : headers) {
			headerstr = headerstr + header.getName() + "=" + header.getValue()
					+ ";";
		}
		return headerstr;
	}

	/**
	 * 发get
	 * 
	 * @throws Exception
	 */
	public void Get() throws Exception {
		String paramstring;
		if (!this.url.contains("?")) {
			paramstring = this.url + "?";
		} else {
			paramstring = this.url + "&";
		}
		paramstring += this.requestbody;
		GetMethod method = new GetMethod(paramstring);
		if (DoAuth) {
			method.setDoAuthentication(DoAuth);
		}
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());
		method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,
				requesttimeout);
		client.getHttpConnectionManager().getParams()
				.setConnectionTimeout(connecttimeout);
		Iterator<?> iter1 = headerlist.entrySet().iterator();
		while (iter1.hasNext()) {
			Map.Entry entry = (Map.Entry) iter1.next();
			try {
				method.addRequestHeader((String) entry.getKey(),
						(String) entry.getValue());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		try {
			status = client.executeMethod(method);
			if (status != HttpStatus.SC_OK) {
				throw new Exception("Method failed: " + method.getStatusLine());
			}
			// responseBody = method.getResponseBody();
			responseBody = IOUtils
					.toByteArray(method.getResponseBodyAsStream());
			headers = method.getResponseHeaders();

			// Body=responseBody.toString();
			Body = new String(responseBody, encode);

		} catch (HttpException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			method.releaseConnection();
		}
	}

	/**
	 * 发post
	 * 
	 * @throws Exception
	 */

	public void Post() throws Exception {
		PostMethod method = new PostMethod(url);
		if (DoAuth) {
			method.setDoAuthentication(DoAuth);
		}
		Iterator<?> iter1 = headerlist.entrySet().iterator();
		while (iter1.hasNext()) {
			Map.Entry entry = (Map.Entry) iter1.next();
			try {
				method.addRequestHeader((String) entry.getKey(),
						(String) entry.getValue());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());
		method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,
				requesttimeout);
		client.getHttpConnectionManager().getParams()
				.setConnectionTimeout(connecttimeout);

		// method.setRequestBody(this.requestbody);
		// 用StringRequestEntity代替setRequestBody
		try {
			String ct = "";
			if (this.getHeaderValue("contentType") != null) {
				ct = this.getHeaderValue("contentType");
			}
			StringRequestEntity a1 = new StringRequestEntity(this.requestbody,
					ct, this.encode);
			method.setRequestEntity(a1);
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		try {
			status = client.executeMethod(method);
			if (status != HttpStatus.SC_OK) {
				throw new Exception("Method failed: " + method.getStatusLine());
			}
			// responseBody = method.getResponseBody();
			responseBody = IOUtils
					.toByteArray(method.getResponseBodyAsStream());
			headers = method.getResponseHeaders();
			// Body=responseBody.toString();
			Body = new String(responseBody, encode);
			
		} catch (HttpException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			method.releaseConnection();
		}
	}

	public boolean saveResponse2File(String filename)
			throws FileNotFoundException {
		FileOutputStream fis = new FileOutputStream(filename, false);

		try {
			fis.write(responseBody);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		try {
			fis.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}
		// System.out.println(Body);
		return true;
	}

	/**
	 * 将对象转化为参数
	 * 
	 * @param obj
	 */
	public void addParamValue(Object obj) {
		try {
			Class<?> clazz = obj.getClass();
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				PropertyDescriptor pd = new PropertyDescriptor(f.getName(),
						clazz);
				Method wM = pd.getReadMethod();
				String x = (String) wM.invoke(obj);
				if (null == x) {
					// continue;
					addParamValue(f.getName(), "");
				} else {
					addParamValue(f.getName(), (String) wM.invoke(obj));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 检查reponse中是否包含指定文件中的内容。
	 */
	public boolean findFileStringinResponse(String filename) {
		BufferedInputStream bufferedInputStream = null;
		try {
			File file = new File(filename);
			if (filename == null || filename.equals("")) {
				throw new NullPointerException("无效的文件路径");
			}
			long len = file.length();
			byte[] bytes = new byte[(int) len];
			bufferedInputStream = new BufferedInputStream(new FileInputStream(
					file));
			int r = bufferedInputStream.read(bytes);
			if (r != len) {
				throw new IOException("读取文件不正确");
			}
			bufferedInputStream.close();
			String content = new String(bytes, encode);

			if (content.equals("MYHTTPCLIENT_ZERORESPONSE")) {
				boolean kongresult = (this.responseBody.length == 0);

				return this.responseBody.length == 0;
			} else {

				byte[] des;
				try {

					des = bytes;
					for (int a = 0; a < (responseBody.length - des.length + 1); a++) {
						boolean result = true;
						for (int b = 0; b < des.length; b++) {
							if (responseBody[a + b] != des[b]) {
								result = false;
								break;
							}
						}
						if (result) {
							return true;
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				return false;
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			try {
				bufferedInputStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				return false;
			}
		}

	}

	/**
	 * 检查reponse是否同指定的文件中所包含的正则表达式匹配
	 */
	public boolean FileMatchResponse(String filename) {
		BufferedInputStream bufferedInputStream = null;
		try {
			File file = new File(filename);
			if (filename == null || filename.equals("")) {
				throw new NullPointerException("无效的文件路径");
			}
			long len = file.length();
			byte[] bytes = new byte[(int) len];
			bufferedInputStream = new BufferedInputStream(new FileInputStream(
					file));
			int r = bufferedInputStream.read(bytes);
			if (r != len) {
				throw new IOException("读取文件不正确");
			}
			bufferedInputStream.close();
			String content = new String(bytes, encode);

			if (content.equals("MYHTTPCLIENT_ZERORESPONSE")) {
				return this.responseBody.length == 0;
			} else {
				String responseaim = new String(responseBody, encode);
				boolean matchresult = responseaim.matches(content);

				return matchresult;
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			try {
				bufferedInputStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				return false;
			}
		}

	}

	/**
	 * 检查reponse是否同参数的正则表达式匹配
	 */
	public boolean ResponseMatch(String content) {
		if (content.equals("MYHTTPCLIENT_ZERORESPONSE")) {
			boolean kongresult = (this.responseBody.length == 0);

			return kongresult;
		} else {
			try {
				String responseaim = new String(responseBody, encode);
				boolean result = responseaim.matches(content);

				return result;
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}
	}

	/**
	 * 检查reponse是否包含参数的字符串
	 */
	public boolean findStringinResponse(String content) {
		if (content.equals("MYHTTPCLIENT_ZERORESPONSE")) {
			return this.responseBody.length == 0;
		} else {

			byte[] des;
			try {
				des = content.getBytes(encode);
				for (int a = 0; a < (responseBody.length - des.length + 1); a++) {
					boolean result = true;
					for (int b = 0; b < des.length; b++) {
						if (responseBody[a + b] != des[b]) {
							result = false;
							break;
						}
					}
					if (result) {

						return true;
					}
				}
			} catch (UnsupportedEncodingException ex) {
				ex.printStackTrace();
			}

			return false;
		}
	}

	/**
	 * 返回参数在reponse中出现的次数
	 */
	public int findNumberofStringinResponse(String content) {
		if (content != null && !content.equals("")) {
			int count = 0;
			byte[] des;
			try {
				des = content.getBytes(encode);
				for (int a = 0; a < (responseBody.length - des.length + 1); a++) {
					boolean result = true;
					for (int b = 0; b < des.length; b++) {
						if (responseBody[a + b] != des[b]) {
							result = false;
							break;
						}
					}
					if (result) {
						count++;
					}
				}
			} catch (UnsupportedEncodingException ex) {
				ex.printStackTrace();
			}
			return count;
		} else {
			return 0;
		}

	}

	/**
	 * 通过左右边界抓出目标字符串返回
	 */
	public String saveParamLeftstrRightstr(String leftstr, String rightstr) {
		byte[] left;
		byte[] right;
		byte[] content = null;
		int start;
		int end;
		try {
			left = leftstr.getBytes(encode);
			right = rightstr.getBytes(encode);
			for (int a = 0; a < (responseBody.length - left.length
					- right.length + 1); a++) {
				boolean result = true;
				for (int b = 0; b < left.length; b++) {
					if (responseBody[a + b] != left[b]) {
						result = false;
						break;
					}
				}

				if (result) {
					// 注意
					start = a + left.length;
					for (int a1 = start; a1 < (responseBody.length
							- right.length + 1); a1++) {
						boolean result2 = true;
						for (int b1 = 0; b1 < right.length; b1++) {
							if (responseBody[a1 + b1] != right[b1]) {
								result2 = false;
								break;
							}
						}
						if (result2) {
							end = a1 - 1;
							if (start > end) {
								return "";
							} else {
								content = new byte[end - start + 1];
								int j = 0;
								for (int a2 = start; a2 <= end; a2++) {
									content[j] = responseBody[a2];
									j++;
								}
								// System.out.println("content");
								String collstr = new String(content, encode);

								return collstr;
							}
						}
					}
				}
			}
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		return "";
	}

	public String getResponseBody() {
		return this.Body;
	}
}
