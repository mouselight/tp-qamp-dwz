package com.bill99.testmp.testtools.utils;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum TxnInterfaceEnum {

	TXNTYPE0("QUY"), TXNTYPE1("PUR"), TXNTYPE2("PRE"), TXNTYPE3("INP"), TXNTYPE4("901"), TXNTYPE5("1001"), TXNTYPE6("201"), TXNTYPE7("302");

	private String value;

	TxnInterfaceEnum(String status) {
		this.value = status;
	}

	public String getValue() {
		return value;
	}

	public final static Map<TxnInterfaceEnum, String> txnTypeMap;

	static {
		txnTypeMap = new EnumMap<TxnInterfaceEnum, String>(TxnInterfaceEnum.class);
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE0, "鉴权");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE1, "消费");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE2, "预授权");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE3, "分期");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE4, "901");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE5, "1001");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE6, "201");
		txnTypeMap.put(TxnInterfaceEnum.TXNTYPE7, "302");
	}

	/**
	 * 返回TxnWayTypeMap
	 */
	public static Map<String, String> getTxnTypeMap() {
		Map<String, String> txnTypeMap = new HashMap<String, String>();
		Iterator<TxnInterfaceEnum> txnTypeKeyIt = TxnInterfaceEnum.txnTypeMap.keySet().iterator();
		while (txnTypeKeyIt.hasNext()) {
			String txnTypeKey = txnTypeKeyIt.next().getValue();
			for (TxnInterfaceEnum tmpEnum : TxnInterfaceEnum.values()) {
				if (tmpEnum.value.equals(txnTypeKey)) {
					txnTypeMap.put(txnTypeKey, TxnInterfaceEnum.txnTypeMap.get(tmpEnum));
				}
			}
		}
		return txnTypeMap;
	}

	/**
	 * 返回TxnWayTypeMap对应的描述.
	 */
	public static String getTxnTypeVal(final String value) {
		return TxnInterfaceEnum.txnTypeMap.get(TxnInterfaceEnum.getTxnTypeValueByKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static TxnInterfaceEnum getTxnTypeValueByKey(String value) {
		TxnInterfaceEnum tmpKey = null;
		for (TxnInterfaceEnum tmpEnum : TxnInterfaceEnum.values()) {
			if (tmpEnum.value.equals(value)) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static String getTxnTypeKeyByValue(String value) {
		String txnWayType = null;
		for (TxnInterfaceEnum tmpEnum : TxnInterfaceEnum.values()) {
			if (TxnInterfaceEnum.txnTypeMap.get(tmpEnum).equals(value)) {
				txnWayType = tmpEnum.value;
				break;
			}
		}
		return txnWayType;
	}

	public static void main(String[] args) {

		System.out.println(getTxnTypeVal("PUR"));
	}

}
