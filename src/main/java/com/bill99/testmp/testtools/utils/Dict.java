package com.bill99.testmp.testtools.utils;

import java.util.HashMap;
import java.util.Map;

public class Dict {
	public static final String CNP_RESPONSECODE_SUCCESS = "[00]";
	
	public static final String DDP_RESPONSECODE_SUCCESS = "01001";
	public static final String DDP_RESPONSECODE_FAIL = "01002";
	public static final String DDP_RESPONSECODE_EXECUTING = "01003";
	public static final String DDP_RESPONSECODE_ERROR = "01004";
	
	public static final String Key_CNP_Defalut = "CNP-DEF";
	public static final String Key_DDP_Defalut = "DDP-DEF";
	public static final String Key_DDP_901 = "DDP-901";
	public static final String Key_DDP_302 = "DDP-302";
	
	// 借记卡
	public static final String CardType_DebitCard = "0002";
	// 贷记卡
	public static final String CardType_CreditCard = "0001";
	// 借记卡
	public static final String CardType_DebitCard_DDP = "0201";
	// 贷记卡
	public static final String CardType_CreditCard_DDP = "0203";
	

	//任务状态  - 初始状态
	public static final String TaskStatus_Start = "00";
	//任务状态  -  执行中
	public static final String TaskStatus_Running = "01";
	//任务状态  -  执行完毕
	public static final String TaskStatus_Finished = "02";
	//任务状态  -  执行中出现异常并停止
	public static final String TaskStatus_Error = "99";
			
//	public static final String Item_PAN="pan";
//	public static final String Item_EXPIRED_DATE="expireDate";
//	public static final String Item_CVV2="cvv2";
//	public static final String Item_CARDHOLDER="cardholder";
//	public static final String Item_CARDHOLDER_NO_ID="noId";
//	public static final String Item_CARDHOLDER_CELL_PHONE="noCellPhone";
	
	
	
	public static String getItemChineseName(String itemId){
		Map<String,String> m = new HashMap<String,String>();
		m.put("pan", "卡号");
		m.put("expireDate", "有效期");
		m.put("cvv2", "CVV2");
		m.put("cardholder", "姓名");
		m.put("noId", "证件号码");
		m.put("noCellPhone", "电话号码");
		
		try{	
			return m.get(itemId.trim());
		}
		catch(Exception ex){
			return itemId;
		}
	}
	
}
