package com.bill99.testmp.testtools.utils;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum CardTypeEnum {

	TXNTYPE0("0000"), TXNTYPE1("0001"), TXNTYPE2("0002"), TXNTYPE3("0003");

	private String value;

	CardTypeEnum(String status) {
		this.value = status;
	}

	public String getValue() {
		return value;
	}

	public final static Map<CardTypeEnum, String> txnTypeMap;

	static {
		txnTypeMap = new EnumMap<CardTypeEnum, String>(CardTypeEnum.class);
		txnTypeMap.put(CardTypeEnum.TXNTYPE0, "银行卡");
		txnTypeMap.put(CardTypeEnum.TXNTYPE1, "信用卡");
		txnTypeMap.put(CardTypeEnum.TXNTYPE2, "借记卡");
		txnTypeMap.put(CardTypeEnum.TXNTYPE3, "私有卡");
	}

	/**
	 * 返回TxnWayTypeMap
	 */
	public static Map<String, String> getTxnTypeMap() {
		Map<String, String> txnTypeMap = new HashMap<String, String>();
		Iterator<CardTypeEnum> txnTypeKeyIt = CardTypeEnum.txnTypeMap.keySet().iterator();
		while (txnTypeKeyIt.hasNext()) {
			String txnTypeKey = txnTypeKeyIt.next().getValue();
			for (CardTypeEnum tmpEnum : CardTypeEnum.values()) {
				if (tmpEnum.value.equals(txnTypeKey)) {
					txnTypeMap.put(txnTypeKey, CardTypeEnum.txnTypeMap.get(tmpEnum));
				}
			}
		}
		return txnTypeMap;
	}

	/**
	 * 返回TxnWayTypeMap对应的描述.
	 */
	public static String getTxnTypeVal(final String value) {
		return CardTypeEnum.txnTypeMap.get(CardTypeEnum.getTxnTypeValueByKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static CardTypeEnum getTxnTypeValueByKey(String value) {
		CardTypeEnum tmpKey = null;
		for (CardTypeEnum tmpEnum : CardTypeEnum.values()) {
			if (tmpEnum.value.equals(value)) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static String getTxnTypeKeyByValue(String value) {
		String txnWayType = null;
		for (CardTypeEnum tmpEnum : CardTypeEnum.values()) {
			if (CardTypeEnum.txnTypeMap.get(tmpEnum).equals(value)) {
				txnWayType = tmpEnum.value;
				break;
			}
		}
		return txnWayType;
	}

	public static void main(String[] args) {
		System.out.println(getTxnTypeVal("0001"));

	}

}
