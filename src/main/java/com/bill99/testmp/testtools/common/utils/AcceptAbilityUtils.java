package com.bill99.testmp.testtools.common.utils;

public class AcceptAbilityUtils {

	//任务状态，00 初始状态 01 执行中 02 执行完毕 99 执行中出现异常并停止

	public final static String ACCEPTABILITY_TASK_STATE_INIT = "00";
	public final static String ACCEPTABILITY_TASK_STATE_ING = "01";
	public final static String ACCEPTABILITY_TASK_STATE_DONE = "02";
	public final static String ACCEPTABILITY_TASK_STATE_ERROR = "99";

}
