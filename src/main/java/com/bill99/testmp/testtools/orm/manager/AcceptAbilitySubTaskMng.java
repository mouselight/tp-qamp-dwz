package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;

public interface AcceptAbilitySubTaskMng {

	public List<AcceptAbilitySubTask> queryForList(Long idTask);
	
	public AcceptAbilitySubTask save(AcceptAbilitySubTask acceptAbilitySubTask);

	public void update(AcceptAbilitySubTask acceptAbilitySubTask);

}
