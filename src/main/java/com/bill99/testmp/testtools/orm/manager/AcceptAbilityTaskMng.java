package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;

public interface AcceptAbilityTaskMng {

	public AcceptAbilityTask save(AcceptAbilityTask acceptAbilitytTask);

	public void update(AcceptAbilityTask acceptAbilitytTask);
	
	public AcceptAbilityTask queryById(Long idTask);

	public List<AcceptAbilityTask> queryForPage(AcceptAbilityTask acceptAbilityTask, Page page);

}
