package com.bill99.testmp.testtools.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

public interface TaskLogInfoDao extends BaseDao<TaskLogInfo, Long> {
	public List<TaskLogInfo> queryBySubTaskId(long subtaskId);
	
	public TaskLogInfo getTaskLogInfoByIdTxn(String idTxn);
}
