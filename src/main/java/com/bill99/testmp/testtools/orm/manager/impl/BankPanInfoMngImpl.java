package com.bill99.testmp.testtools.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testtools.orm.dao.BankPanInfoDao;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.manager.BankPanInfoMng;

public class BankPanInfoMngImpl implements BankPanInfoMng {

	private BankPanInfoDao bankPanInfoDao;

	public void setBankPanInfoDao(BankPanInfoDao bankPanInfoDao) {
		this.bankPanInfoDao = bankPanInfoDao;
	}

	public List<BankPanInfo> queryAll() {
		return bankPanInfoDao.queryAll();
	}

	public void save(BankPanInfo bankPanInfo) {
		bankPanInfoDao.save(bankPanInfo);
	}

	public BankPanInfo queryById(Long idBankPan) {
		return bankPanInfoDao.get(idBankPan);
	}

	public void update(BankPanInfo bankPanInfo) {
		bankPanInfoDao.update(bankPanInfo);
	}

	public BankPanInfo getBankPanInfoByPan(String pan) {
		return bankPanInfoDao.getBankPanInfoByPan(pan);
	}

}
