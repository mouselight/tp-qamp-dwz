package com.bill99.testmp.testtools.orm.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.ibatis.dao.TaskLogInfoIbatisDao;

public class TaskLogInfoIbatisDaoImpl extends QueryDaoSupport  implements TaskLogInfoIbatisDao {

	public List<TaskLogInfo> getTaskLogList(Long idAatask) {
//		Map map = new HashMap();
//		map.put("idAatask", idAatask);
		return queryForList("TaskLogInfo.getTaskLogInfoByidAatask", idAatask);
	}

}
