package com.bill99.testmp.testtools.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testtools.orm.dao.TaskLogInfoDao;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;
import com.bill99.testmp.testtools.orm.ibatis.dao.TaskLogInfoIbatisDao;
import com.bill99.testmp.testtools.orm.manager.TaskLogInfoMng;

public class TaskLogInfoMngImpl implements TaskLogInfoMng {

	private TaskLogInfoDao taskLogInfoDao;
	private TaskLogInfoIbatisDao taskLogInfoIbatisDao;

	public TaskLogInfoDao getTaskLogInfoDao() {
		return taskLogInfoDao;
	}
	
	public void setTaskLogInfoDao(TaskLogInfoDao taskLogInfoDao) {
		this.taskLogInfoDao = taskLogInfoDao;
	}	
	
	public TaskLogInfoIbatisDao getTaskLogInfoIbatisDao() {
		return taskLogInfoIbatisDao;
	}

	public void setTaskLogInfoIbatisDao(TaskLogInfoIbatisDao taskLogInfoIbatisDao) {
		this.taskLogInfoIbatisDao = taskLogInfoIbatisDao;
	}

	public void save(TaskLogInfo taskLogInfo) {
		taskLogInfoDao.save(taskLogInfo);		
	}

	public List<TaskLogInfo> queryBySubTaskId(long subtaskId) {
		return taskLogInfoDao.queryBySubTaskId(subtaskId);
	}

	public List<TaskLogInfo> queryByTaskId(long taskId) {		
		return taskLogInfoIbatisDao.getTaskLogList(taskId);
	}

	public TaskLogInfo getTaskLogInfoByIdTxn(String idTxn) {		
		return taskLogInfoDao.getTaskLogInfoByIdTxn(idTxn);
	}	

}
