package com.bill99.testmp.testtools.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testtools.orm.dao.TaskLogInfoDao;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

public class TaskLogInfoDaoImpl extends BaseDaoSupport<TaskLogInfo, Long>
		implements TaskLogInfoDao {
	
	private final String queryBySubTaskIdHql = "from TaskLogInfo where subtaskId=?";
	
	private final String queryByIdTxnHql = "from TaskLogInfo where id_txn=?";

	public List<TaskLogInfo> queryBySubTaskId(long subtaskId) {
		return find(queryBySubTaskIdHql, subtaskId);
	}

	public TaskLogInfo getTaskLogInfoByIdTxn(String idTxn) {
		return (TaskLogInfo)this.findUnique(queryByIdTxnHql, idTxn);
	}

}
