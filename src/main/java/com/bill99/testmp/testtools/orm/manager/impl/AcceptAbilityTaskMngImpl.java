package com.bill99.testmp.testtools.orm.manager.impl;

import java.util.List;

import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.dao.AcceptAbilityTaskDao;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;

public class AcceptAbilityTaskMngImpl implements AcceptAbilityTaskMng {

	private AcceptAbilityTaskDao acceptAbilityTaskDao;

	public AcceptAbilityTask save(AcceptAbilityTask acceptAbilitytTask) {
		return acceptAbilityTaskDao.save(acceptAbilitytTask);
	}

	public void update(AcceptAbilityTask acceptAbilitytTask) {
		acceptAbilityTaskDao.update(acceptAbilitytTask);
	}

	public void setAcceptAbilityTaskDao(AcceptAbilityTaskDao acceptAbilityTaskDao) {
		this.acceptAbilityTaskDao = acceptAbilityTaskDao;
	}

	public List<AcceptAbilityTask> queryForPage(AcceptAbilityTask acceptAbilityTask, Page page) {
		return acceptAbilityTaskDao.queryForPage(acceptAbilityTask, page);
	}

	public AcceptAbilityTask queryById(Long idTask) {		
		return acceptAbilityTaskDao.get(idTask);
	}

}
