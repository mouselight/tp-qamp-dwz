package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

public interface TaskLogInfoMng {

	public List<TaskLogInfo> queryBySubTaskId(long subtaskId);

	public void save(TaskLogInfo taskLogInfo);
	
	public List<TaskLogInfo> queryByTaskId(long taskId);
	
	public TaskLogInfo getTaskLogInfoByIdTxn(String idTxn);
}
