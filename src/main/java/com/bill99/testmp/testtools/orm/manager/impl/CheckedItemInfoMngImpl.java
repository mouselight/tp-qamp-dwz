package com.bill99.testmp.testtools.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testtools.orm.dao.CheckedItemInfoDao;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;
import com.bill99.testmp.testtools.orm.manager.CheckedItemInfoMng;

public class CheckedItemInfoMngImpl implements CheckedItemInfoMng{
	
	private CheckedItemInfoDao checkedItemInfoDao;
	
	public CheckedItemInfoDao getCheckedItemInfoDao() {
		return checkedItemInfoDao;
	}

	public void setCheckedItemInfoDao(CheckedItemInfoDao checkedItemInfoDao) {
		this.checkedItemInfoDao = checkedItemInfoDao;
	}

	public List<CheckedItemInfo> getQueryByKeyAndCardType(String key,String cardType){
		return checkedItemInfoDao.getQueryByKeyAndCardType(key, cardType);
	}
}
