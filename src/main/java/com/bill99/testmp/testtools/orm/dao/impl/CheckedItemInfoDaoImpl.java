package com.bill99.testmp.testtools.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testtools.orm.dao.CheckedItemInfoDao;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;

public class CheckedItemInfoDaoImpl extends BaseDaoSupport<CheckedItemInfo, Long>  implements CheckedItemInfoDao {

	private final String getQueryByKeyAndCardTypeHql = "from CheckedItemInfo where key = ? and cardType=? order by orderfield";
	
	public List<CheckedItemInfo> getQueryByKeyAndCardType(String key,String cardType){
			return find(this.getQueryByKeyAndCardTypeHql, key, cardType);	
	}

}
