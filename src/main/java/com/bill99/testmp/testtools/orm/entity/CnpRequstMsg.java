package com.bill99.testmp.testtools.orm.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.bill99.testmp.testtools.utils.TimeTools;

public class CnpRequstMsg {
	//
	//	private String postUrl = "http://192.168.52.217:8082/mas/internal/WsMtpService";
	private String postUrl = "http://172.16.21.180:8080/mas/internal/WsMtpService";
	private String merchantId;
	private String merchantMemberCode;
	private String terminalId;
	private String txnType;
	private String PAN;
	private String expiredDate;
	private String cvv2;
	private String cur = "CNY";
	private String amt = "0.01";
	private String cardHolderId;
	private String catdHolderName;
	private String rewardPoints;
	private String termInMonths;
	private String productId;
	private String convCur;
	private String authCode;
	private String originalRRN;
	private String termTraceNo;
	private String termBatchNo;
	private String termInvoiceNo;
	private String externalRefNumber;
	private String customerId;
	private String serviceChannelType = "2";
	private String serviceChannelTraceNo = TimeTools.getTimeMillisSequence();
	private String entryTime = TimeTools.getGenerateSequence("yyyy-MM-dd HH:mm:ss");
	private String siteType;
	private String siteId;
	private String appType;
	private String referenceDataId = "123456";
	private String extTraceId;
	private String storablePAN;
	private String phoneNO;
	private String validCode;
	private String savePciFlag;
	private String token;
	private String payBatch;
	private String attachInfos = "{\"111\":\"222\",\"222\":\"333\",\"333\":\"444\"}";
	private String supCardFlag;

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantMemberCode() {
		return merchantMemberCode;
	}

	public void setMerchantMemberCode(String merchantMemberCode) {
		this.merchantMemberCode = merchantMemberCode;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getPAN() {
		return PAN;
	}

	public void setPAN(String pAN) {
		PAN = pAN;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getCur() {
		return cur;
	}

	public void setCur(String cur) {
		this.cur = cur;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getCatdHolderName() {
		return catdHolderName;
	}

	public void setCatdHolderName(String catdHolderName) {
		this.catdHolderName = catdHolderName;
	}

	public String getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public String getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(String termInMonths) {
		this.termInMonths = termInMonths;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getConvCur() {
		return convCur;
	}

	public void setConvCur(String convCur) {
		this.convCur = convCur;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getOriginalRRN() {
		return originalRRN;
	}

	public void setOriginalRRN(String originalRRN) {
		this.originalRRN = originalRRN;
	}

	public String getTermTraceNo() {
		return termTraceNo;
	}

	public void setTermTraceNo(String termTraceNo) {
		this.termTraceNo = termTraceNo;
	}

	public String getTermBatchNo() {
		return termBatchNo;
	}

	public void setTermBatchNo(String termBatchNo) {
		this.termBatchNo = termBatchNo;
	}

	public String getTermInvoiceNo() {
		return termInvoiceNo;
	}

	public void setTermInvoiceNo(String termInvoiceNo) {
		this.termInvoiceNo = termInvoiceNo;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceChannelType() {
		return serviceChannelType;
	}

	public void setServiceChannelType(String serviceChannelType) {
		this.serviceChannelType = serviceChannelType;
	}

	public String getServiceChannelTraceNo() {
		return serviceChannelTraceNo;
	}

	public void setServiceChannelTraceNo(String serviceChannelTraceNo) {
		this.serviceChannelTraceNo = serviceChannelTraceNo;
	}

	public void setServiceChannelTraceNo() {
		this.serviceChannelTraceNo = TimeTools.getTimeMillisSequence();
	}

	public String getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(String entryTime) {
		this.entryTime = entryTime;
	}

	public void setEntryTime() {
		this.entryTime = TimeTools.getGenerateSequence("yyyy-MM-dd HH:mm:ss");
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getReferenceDataId() {
		return referenceDataId;
	}

	public void setReferenceDataId(String referenceDataId) {
		this.referenceDataId = referenceDataId;
	}

	public String getExtTraceId() {
		return extTraceId;
	}

	public void setExtTraceId(String extTraceId) {
		this.extTraceId = extTraceId;
	}

	public String getStorablePAN() {
		return storablePAN;
	}

	public void setStorablePAN(String storablePAN) {
		this.storablePAN = storablePAN;
	}

	public String getPhoneNO() {
		return phoneNO;
	}

	public void setPhoneNO(String phoneNO) {
		this.phoneNO = phoneNO;
	}

	public String getValidCode() {
		return validCode;
	}

	public void setValidCode(String validCode) {
		this.validCode = validCode;
	}

	public String getSavePciFlag() {
		return savePciFlag;
	}

	public void setSavePciFlag(String savePciFlag) {
		this.savePciFlag = savePciFlag;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPayBatch() {
		return payBatch;
	}

	public void setPayBatch(String payBatch) {
		this.payBatch = payBatch;
	}

	public String getAttachInfos() {
		return attachInfos;
	}

	public void setAttachInfos(String attachInfos) {
		this.attachInfos = attachInfos;
	}

	public String getSupCardFlag() {
		return supCardFlag;
	}

	public void setSupCardFlag(String supCardFlag) {
		this.supCardFlag = supCardFlag;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
