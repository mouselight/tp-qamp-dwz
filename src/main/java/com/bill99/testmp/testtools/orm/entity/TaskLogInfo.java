package com.bill99.testmp.testtools.orm.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 记录交易日志
 * 
 * @author hongzhi.zheng
 * 
 */
public class TaskLogInfo implements Serializable {

	private static final long serialVersionUID = -2980739660886515436L;

	public static final Character STATUS_NONCHECKED = '0';
	public static final Character STATUS_TRUEVALUE = '1';
	public static final Character STATUS_FAILVALUE = '2';
	public static final Character STATUS_NONVALUE = '3';

	private long logId;
	// 子任务ID
	private long subtaskId;
	// 产品编号，如：CNP、DDP
	private String productId;
	// 交易编号（CNP）
	private String idTxn;
	// 外部跟踪编号（CNP）
	private String extTraceNo;
	// 交易状态
	private String txnStatus;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String pan;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String expireDate;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String cvv2;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String cardholder;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String noId;
	// 0 不校验，1 输入正确值，2 输入错误值，3 未输入
	private String noCellPhone;
	// 创建时间
	private Date createDate;
	// 工单号（DDP）
	private String workorderId;
	// 失败原因（DDP）
	private String memoFail;

	// 隧道数据中银行应答码
	private String bankRespcode;
	// 隧道数据中银行应答码解释
	private String bankRespcodeDesc;
	// 数据库中记录的交易返回码
	private String respCode;
	// 数据库中记录的交易返回码的中文解释
	private String respCodeDesc;
	
	private String workorderStatus;
	private String txnMsg;

	public long getLogId() {
		return logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public long getSubtaskId() {
		return subtaskId;
	}

	public void setSubtaskId(long subtaskId) {
		this.subtaskId = subtaskId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getIdTxn() {
		return idTxn;
	}

	public void setIdTxn(String idTxn) {
		this.idTxn = idTxn;
	}

	public String getExtTraceNo() {
		return extTraceNo;
	}

	public void setExtTraceNo(String extTraceNo) {
		this.extTraceNo = extTraceNo;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getCardholder() {
		return cardholder;
	}

	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}

	public String getNoId() {
		return noId;
	}

	public void setNoId(String noId) {
		this.noId = noId;
	}

	public String getNoCellPhone() {
		return noCellPhone;
	}

	public void setNoCellPhone(String noCellPhone) {
		this.noCellPhone = noCellPhone;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getWorkorderId() {
		return workorderId;
	}

	public void setWorkorderId(String workorderId) {
		this.workorderId = workorderId;
	}

	public String getMemoFail() {
		return memoFail;
	}

	public void setMemoFail(String memoFail) {
		this.memoFail = memoFail;
	}

	public String getWorkorderStatus() {
		return workorderStatus;
	}

	public void setWorkorderStatus(String workorderStatus) {
		this.workorderStatus = workorderStatus;
	}

	public String getBankRespcode() {
		return bankRespcode;
	}

	public void setBankRespcode(String bankRespcode) {
		this.bankRespcode = bankRespcode;
	}

	public String getBankRespcodeDesc() {
		return bankRespcodeDesc;
	}

	public void setBankRespcodeDesc(String bankRespcodeDesc) {
		this.bankRespcodeDesc = bankRespcodeDesc;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespCodeDesc() {
		return respCodeDesc;
	}

	public void setRespCodeDesc(String respCodeDesc) {
		this.respCodeDesc = respCodeDesc;
	}

	public void init() {
		this.pan = TaskLogInfo.STATUS_NONCHECKED.toString();
		this.expireDate = TaskLogInfo.STATUS_NONCHECKED.toString();
		this.cvv2 = TaskLogInfo.STATUS_NONCHECKED.toString();
		this.cardholder = TaskLogInfo.STATUS_NONCHECKED.toString();
		this.noId = TaskLogInfo.STATUS_NONCHECKED.toString();
		this.noCellPhone = TaskLogInfo.STATUS_NONCHECKED.toString();
	}

	public void changeStatus(String field, Character status) {
		if (field.equals("pan"))
			this.pan = status.toString();
		if (field.equals("expireDate"))
			this.expireDate = status.toString();
		if (field.equals("cvv2"))
			this.cvv2 = status.toString();
		if (field.equals("cardholder"))
			this.cardholder = status.toString();
		if (field.equals("noId"))
			this.noId = status.toString();
		if (field.equals("noCellPhone"))
			this.noCellPhone = status.toString();
	}

	public String getTxnMsg() {
		return txnMsg;
	}

	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}

}
