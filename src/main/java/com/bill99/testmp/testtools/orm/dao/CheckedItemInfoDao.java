package com.bill99.testmp.testtools.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;

public interface CheckedItemInfoDao extends BaseDao<CheckedItemInfo, Long> {
	public List<CheckedItemInfo> getQueryByKeyAndCardType(String key , String cardType);
}
