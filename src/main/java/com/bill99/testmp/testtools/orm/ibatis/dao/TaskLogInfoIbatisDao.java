package com.bill99.testmp.testtools.orm.ibatis.dao;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.NodesImportDto;
import com.bill99.testmp.testmanage.common.dto.TestStepProgressDto;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

public interface TaskLogInfoIbatisDao {

	public List<TaskLogInfo> getTaskLogList(Long idAatask);


}
