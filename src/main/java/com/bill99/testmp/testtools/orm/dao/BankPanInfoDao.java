package com.bill99.testmp.testtools.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;

public interface BankPanInfoDao extends BaseDao<BankPanInfo, Long> {

	public List<BankPanInfo> queryAll();
	
	public BankPanInfo getBankPanInfoByPan(String pan);

}
