package com.bill99.testmp.testtools.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testtools.orm.dao.AcceptAbilitySubTaskDao;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilitySubTaskMng;

public class AcceptAbilitySubTaskMngImpl implements AcceptAbilitySubTaskMng {

	private AcceptAbilitySubTaskDao acceptAbilitySubTaskDao;

	public AcceptAbilitySubTaskDao getAcceptAbilitySubTaskDao() {
		return acceptAbilitySubTaskDao;
	}

	public void setAcceptAbilitySubTaskDao(AcceptAbilitySubTaskDao acceptAbilitySubTaskDao) {
		this.acceptAbilitySubTaskDao = acceptAbilitySubTaskDao;
	}

	public List<AcceptAbilitySubTask> queryForList(Long idTask) {
		return acceptAbilitySubTaskDao.queryForList(idTask);
	}

	public AcceptAbilitySubTask save(AcceptAbilitySubTask acceptAbilitytTask) {
		return acceptAbilitySubTaskDao.save(acceptAbilitytTask);
	}

	public void update(AcceptAbilitySubTask acceptAbilitytTask) {
		acceptAbilitySubTaskDao.update(acceptAbilitytTask);
	}

}
