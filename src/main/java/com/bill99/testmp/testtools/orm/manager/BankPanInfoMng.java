package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.BankPanInfo;

public interface BankPanInfoMng {

	public List<BankPanInfo> queryAll();

	public void save(BankPanInfo bankPanInfo);

	public BankPanInfo queryById(Long idBankPan);
	
	public BankPanInfo getBankPanInfoByPan(String pan);

	public void update(BankPanInfo bankPanInfo);

}
