package com.bill99.testmp.testtools.orm.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.bill99.testmp.testtools.utils.TimeTools;

public class DDPSingleRequestMsg {

	private String accType ;
	private String amount;
	private String bankAcctId;
	private String bankAcctName;
	private String bankId;
	private String bgUrl;
	private String contractId;
	private String curType = "CNY";
	private String cvv2;
	// env 生产环境
	// stage2 测试环境
	private String debit;
	private String environment;
	private String expiredDate;
	private String ext1;
	private String ext2;
	private String extraType;
	private String featureCode="F168_5";
	private String idCode;
	private String idType;
	private String inputCharset = "1";
	private String memberCode;
	private String merchantAcctId;
	private String mobile;
	private String openAccDept;
	private String remark;
	private String reqSeqNo = TimeTools.getTimeMillisSequence();
	private String seqId = TimeTools.getTimeMillisSequence();
	private String subMerchantNo;
	private String usage = "代扣";	
	
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBankAcctId() {
		return bankAcctId;
	}
	public void setBankAcctId(String bankAcctId) {
		this.bankAcctId = bankAcctId;
	}
	public String getBankAcctName() {
		return bankAcctName;
	}
	public void setBankAcctName(String bankAcctName) {
		this.bankAcctName = bankAcctName;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBgUrl() {
		return bgUrl;
	}
	public void setBgUrl(String bgUrl) {
		this.bgUrl = bgUrl;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getCurType() {
		return curType;
	}
	public void setCurType(String curType) {
		this.curType = curType;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
		this.environment = debit;
	}
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
		this.debit = environment;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExtraType() {
		return extraType;
	}
	public void setExtraType(String extraType) {
		this.extraType = extraType;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getInputCharset() {
		return inputCharset;
	}
	public void setInputCharset(String inputCharset) {
		this.inputCharset = inputCharset;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
		this.merchantAcctId = memberCode + "01";
	}
	public String getMerchantAcctId() {
		return merchantAcctId;
	}
	public void setMerchantAcctId(String merchantAcctId) {
		this.merchantAcctId = merchantAcctId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOpenAccDept() {
		return openAccDept;
	}
	public void setOpenAccDept(String openAccDept) {
		this.openAccDept = openAccDept;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getReqSeqNo() {
		return reqSeqNo;
	}
	public void setReqSeqNo(String reqSeqNo) {
		this.reqSeqNo = reqSeqNo;
	}
	public void setReqSeqNo() {
		this.reqSeqNo = TimeTools.getTimeMillisSequence();
	}
	public String getSeqId() {
		return seqId;
	}
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	public void setSeqId() {
		this.seqId = TimeTools.getTimeMillisSequence();
	}
	public String getSubMerchantNo() {
		return subMerchantNo;
	}
	public void setSubMerchantNo(String subMerchantNo) {
		this.subMerchantNo = subMerchantNo;
	}
	public String getUsage() {
		return usage;
	}
	public void setUsage(String usage) {
		this.usage = usage;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
