package com.bill99.testmp.testtools.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;

public interface AcceptAbilitySubTaskDao extends BaseDao<AcceptAbilitySubTask, Long> {

	public List<AcceptAbilitySubTask> queryForList(Long idTask);

}
