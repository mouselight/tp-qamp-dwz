package com.bill99.testmp.testtools.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testtools.orm.dao.BankPanInfoDao;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;

public class BankPanInfoDaoImpl extends BaseDaoSupport<BankPanInfo, Long> implements BankPanInfoDao {

	private final String queryAllHql = "from BankPanInfo where state=true";
	private final String getBankPanInfoByPanHql = "from BankPanInfo where pan=? and state=true";


	public List<BankPanInfo> queryAll() {
		return find(queryAllHql);
	}

	public BankPanInfo getBankPanInfoByPan(String pan) {		
		return findUnique(getBankPanInfoByPanHql,pan);
	}

}
