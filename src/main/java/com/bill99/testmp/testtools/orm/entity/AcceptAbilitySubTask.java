package com.bill99.testmp.testtools.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class AcceptAbilitySubTask implements Serializable {

	private static final long serialVersionUID = 6261058078680273216L;
	private Long idAaSubTask;
	private Long idTask;
	private Long idBankPan;
	private String taskStatus;
	private String result;
	private Date createDate;

	private BankPanInfo bankPanInfo;

	public Long getIdAaSubTask() {
		return idAaSubTask;
	}

	public void setIdAaSubTask(Long idAaSubTask) {
		this.idAaSubTask = idAaSubTask;
	}

	public Long getIdTask() {
		return idTask;
	}

	public void setIdTask(Long idTask) {
		this.idTask = idTask;
	}

	public Long getIdBankPan() {
		return idBankPan;
	}

	public void setIdBankPan(Long idBankPan) {
		this.idBankPan = idBankPan;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BankPanInfo getBankPanInfo() {
		return bankPanInfo;
	}

	public void setBankPanInfo(BankPanInfo bankPanInfo) {
		this.bankPanInfo = bankPanInfo;
	}

}
