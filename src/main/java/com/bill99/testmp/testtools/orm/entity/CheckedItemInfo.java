package com.bill99.testmp.testtools.orm.entity;

import java.io.Serializable;

/**
 * 受理能力验证中需要校验的项目
 * 
 * @author hongzhi.zheng
 * 
 */
public class CheckedItemInfo  implements Serializable{

	private static final long serialVersionUID = 4005422422335923741L;
	

	private Long id;
	
	// 关键标志
	private String key;
	// 项目编号
	private String itemId;
	// 卡种
	private String cardType;
	// 是否必填
	private Boolean required;
	// 是否必须正确
	private Boolean correctValue;
	// 与请求类的映射关系
	private String mapingReq;
	


//	public CheckedItemInfo(String key, String itemId, String cardtype,
//			Boolean required, Boolean correctValue,String mapingReq) {
//		this.key = key;
//		this.itemId = itemId;
//		this.cardtype = cardtype;
//		this.required = required;
//		this.correctValue = correctValue;
//		this.mapingReq = mapingReq;
//	}

	public String getKey() {
		return key;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Boolean isRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean isCorrectValue() {
		return correctValue;
	}

	public void setCorrectValue(Boolean correctValue) {
		this.correctValue = correctValue;
	}

	public String getMapingReq() {
		return mapingReq;
	}

	public void setMapingReq(String mapingReq) {
		this.mapingReq = mapingReq;
	}

}
