package com.bill99.testmp.testtools.orm.entity;

public class DDPDbResult {
	private String workorderId;
	private String workorderStatus;
	private String failreason;
	public String getWorkorderId() {
		return workorderId;
	}
	public void setWorkorderId(String workorderId) {
		this.workorderId = workorderId;
	}
	public String getWorkorderStatus() {
		return workorderStatus;
	}
	public void setWorkorderStatus(String workorderStatus) {
		this.workorderStatus = workorderStatus;
	}
	public String getFailreason() {
		return failreason;
	}
	public void setFailreason(String failreason) {
		this.failreason = failreason;
	}
	
}
