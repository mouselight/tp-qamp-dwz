package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;

public interface CheckedItemInfoMng {
	public List<CheckedItemInfo> getQueryByKeyAndCardType(String key,String cardType);
}
