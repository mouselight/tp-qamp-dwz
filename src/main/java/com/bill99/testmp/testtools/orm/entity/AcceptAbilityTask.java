package com.bill99.testmp.testtools.orm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AcceptAbilityTask implements Serializable {

	private static final long serialVersionUID = 7393021963427772331L;
	private Long idTask;
	private String pKey;
	private String productName;
	private String subtaskStatus;
	private BigDecimal amt;
	private String terminaId;
	private String merchantId;
	private String txnInterface;
	private String termInMonths;
	private String contractNo;
	private String reportNo;
	private Date createDate;
	private String createUser;
	private String attachInfos;
	private String bankRespCodeKey;
	private String bankRespCodeDescKey;
	private Integer sleepTime;

	public Long getIdTask() {
		return idTask;
	}

	public void setIdTask(Long idTask) {
		this.idTask = idTask;
	}

	public String getpKey() {
		return pKey;
	}

	public void setpKey(String pKey) {
		this.pKey = pKey;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubtaskStatus() {
		return subtaskStatus;
	}

	public void setSubtaskStatus(String subtaskStatus) {
		this.subtaskStatus = subtaskStatus;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public String getTerminaId() {
		return terminaId;
	}

	public void setTerminaId(String terminaId) {
		this.terminaId = terminaId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTxnInterface() {
		return txnInterface;
	}

	public void setTxnInterface(String txnInterface) {
		this.txnInterface = txnInterface;
	}

	public String getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(String termInMonths) {
		this.termInMonths = termInMonths;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getAttachInfos() {
		return attachInfos;
	}

	public void setAttachInfos(String attachInfos) {
		this.attachInfos = attachInfos;
	}

	public Integer getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(Integer sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getBankRespCodeKey() {
		return bankRespCodeKey;
	}

	public void setBankRespCodeKey(String bankRespCodeKey) {
		this.bankRespCodeKey = bankRespCodeKey;
	}

	public String getBankRespCodeDescKey() {
		return bankRespCodeDescKey;
	}

	public void setBankRespCodeDescKey(String bankRespCodeDescKey) {
		this.bankRespCodeDescKey = bankRespCodeDescKey;
	}

}
