package com.bill99.testmp.testtools.orm.dao.impl;

import java.util.List;

import org.hibernate.criterion.Example;
import org.hibernate.criterion.Example.PropertySelector;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.type.Type;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.dao.AcceptAbilityTaskDao;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;

public class AcceptAbilityTaskDaoImpl extends BaseDaoSupport<AcceptAbilityTask, Long> implements AcceptAbilityTaskDao {

	public List<AcceptAbilityTask> queryForPage(AcceptAbilityTask acceptAbilityTask, Page page) {

		Example example = Example.create(acceptAbilityTask).excludeNone().excludeZeroes().ignoreCase().enableLike(MatchMode.ANYWHERE).setPropertySelector(new PropertySelector() {
			private static final long serialVersionUID = 1L;

			public boolean include(Object propertyValue, String propertyName, Type type) {
				if (propertyValue == null)
					return false;
				if (propertyValue instanceof Number)
					if (((Number) propertyValue).longValue() == 0)
						return false;
				if (propertyValue instanceof String)
					if (((String) propertyValue).length() == 0)
						return false;
				return true;
			}
		});

		return find(page, new Order[] { Order.desc("createDate") }, example);
	}
}
