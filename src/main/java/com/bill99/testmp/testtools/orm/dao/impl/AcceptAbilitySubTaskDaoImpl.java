package com.bill99.testmp.testtools.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testtools.orm.dao.AcceptAbilitySubTaskDao;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;

public class AcceptAbilitySubTaskDaoImpl extends BaseDaoSupport<AcceptAbilitySubTask, Long> implements AcceptAbilitySubTaskDao {

	private final String queryForListHql = "from AcceptAbilitySubTask where idTask = ? order by createDate desc";

	public List<AcceptAbilitySubTask> queryForList(Long idTask) {
		return find(queryForListHql, idTask);
	}

}
