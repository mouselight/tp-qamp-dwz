package com.bill99.testmp.testtools.orm.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

public class CnpReponseMsg {
	private String followResponse;
	private String txnType;
	private String merchantId;
	private String merchantName;
	private String settleMerchantId;
	private String settleMerchantName;
	private String MCC;
	private String terminalId;
	private String storablePAN;
	private String cardType;
	private String cardName;
	private String cardOrg;
	private String foreignCardOrg;
	private String issuerId;
	private String issuerName;
	private String appType;
	private String appName;
	private String cur;
	private String amt;
	private String convCur;
	private String convRate;
	private String convAmt;
	private String rewardPoints;
	private String balance;
	private String postRewardPoints;
	private String transTime;
	private String reconDate;
	private String termTraceNo;
	private String termInvoiceNo;
	private String termBatchNo;
	private String RRN;
	private String originalRRN;
	private String externalRefNumber;
	private String customerId;
	private String serviceChannelType;
	private String serviceChannelTraceNo;
	private String termTxnTime;
	private String authCode;
	private String preAuthRlsDate;
	private String shortPhone;
	private String responseCode;
	private String responseTextMessage;
	private String signMsg;

	public String getFollowResponse() {
		return followResponse;
	}

	public void setFollowResponse(String followResponse) {
		this.followResponse = followResponse;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getSettleMerchantId() {
		return settleMerchantId;
	}

	public void setSettleMerchantId(String settleMerchantId) {
		this.settleMerchantId = settleMerchantId;
	}

	public String getSettleMerchantName() {
		return settleMerchantName;
	}

	public void setSettleMerchantName(String settleMerchantName) {
		this.settleMerchantName = settleMerchantName;
	}

	public String getMCC() {
		return MCC;
	}

	public void setMCC(String mCC) {
		MCC = mCC;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getStorablePAN() {
		return storablePAN;
	}

	public void setStorablePAN(String storablePAN) {
		this.storablePAN = storablePAN;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardOrg() {
		return cardOrg;
	}

	public void setCardOrg(String cardOrg) {
		this.cardOrg = cardOrg;
	}

	public String getForeignCardOrg() {
		return foreignCardOrg;
	}

	public void setForeignCardOrg(String foreignCardOrg) {
		this.foreignCardOrg = foreignCardOrg;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getCur() {
		return cur;
	}

	public void setCur(String cur) {
		this.cur = cur;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getConvCur() {
		return convCur;
	}

	public void setConvCur(String convCur) {
		this.convCur = convCur;
	}

	public String getConvRate() {
		return convRate;
	}

	public void setConvRate(String convRate) {
		this.convRate = convRate;
	}

	public String getConvAmt() {
		return convAmt;
	}

	public void setConvAmt(String convAmt) {
		this.convAmt = convAmt;
	}

	public String getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getPostRewardPoints() {
		return postRewardPoints;
	}

	public void setPostRewardPoints(String postRewardPoints) {
		this.postRewardPoints = postRewardPoints;
	}

	public String getTransTime() {
		return transTime;
	}

	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}

	public String getReconDate() {
		return reconDate;
	}

	public void setReconDate(String reconDate) {
		this.reconDate = reconDate;
	}

	public String getTermTraceNo() {
		return termTraceNo;
	}

	public void setTermTraceNo(String termTraceNo) {
		this.termTraceNo = termTraceNo;
	}

	public String getTermInvoiceNo() {
		return termInvoiceNo;
	}

	public void setTermInvoiceNo(String termInvoiceNo) {
		this.termInvoiceNo = termInvoiceNo;
	}

	public String getTermBatchNo() {
		return termBatchNo;
	}

	public void setTermBatchNo(String termBatchNo) {
		this.termBatchNo = termBatchNo;
	}

	public String getRRN() {
		return RRN;
	}

	public void setRRN(String rRN) {
		RRN = rRN;
	}

	public String getOriginalRRN() {
		return originalRRN;
	}

	public void setOriginalRRN(String originalRRN) {
		this.originalRRN = originalRRN;
	}

	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceChannelType() {
		return serviceChannelType;
	}

	public void setServiceChannelType(String serviceChannelType) {
		this.serviceChannelType = serviceChannelType;
	}

	public String getServiceChannelTraceNo() {
		return serviceChannelTraceNo;
	}

	public void setServiceChannelTraceNo(String serviceChannelTraceNo) {
		this.serviceChannelTraceNo = serviceChannelTraceNo;
	}

	public String getTermTxnTime() {
		return termTxnTime;
	}

	public void setTermTxnTime(String termTxnTime) {
		this.termTxnTime = termTxnTime;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getPreAuthRlsDate() {
		return preAuthRlsDate;
	}

	public void setPreAuthRlsDate(String preAuthRlsDate) {
		this.preAuthRlsDate = preAuthRlsDate;
	}

	public String getShortPhone() {
		return shortPhone;
	}

	public void setShortPhone(String shortPhone) {
		this.shortPhone = shortPhone;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseTextMessage() {
		return responseTextMessage;
	}

	public void setResponseTextMessage(String responseTextMessage) {
		this.responseTextMessage = responseTextMessage;
	}

	public String getSignMsg() {
		return signMsg;
	}

	public void setSignMsg(String signMsg) {
		this.signMsg = signMsg;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
