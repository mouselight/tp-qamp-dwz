package com.bill99.testmp.testtools.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;

public interface AcceptAbilityTaskDao extends BaseDao<AcceptAbilityTask, Long> {

	public List<AcceptAbilityTask> queryForPage(AcceptAbilityTask acceptAbilityTask, Page page);

}
