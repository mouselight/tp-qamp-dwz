package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.testmp.testmanage.orm.entity.TcCloud;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;
import com.jeecms.common.hibernate3.BaseDao;
import com.jeecms.common.page.Pagination;

public interface TestCaseCloudDao extends BaseDao<TcCloud> {

	public Pagination query(TestCaseCloudCommand cmd);

	public Integer getTcCount(Long cloudId);

}
