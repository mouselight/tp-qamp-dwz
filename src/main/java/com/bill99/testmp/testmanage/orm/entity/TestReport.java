package com.bill99.testmp.testmanage.orm.entity;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;

import com.bill99.testmp.framework.utils.PrimaryDataFactory;

public class TestReport {

	private Long stepId;
	private Long issueId;

	//TC执行统计
	private Integer allCnt;
	private Integer doneCnt;
	private Integer undoneCnt;
	private Integer succCnt;
	private Integer failCnt;
	private String coverPer;
	private String passPer;

	//需求信息
	private String teamName;
	private String tester;
	private String coder;
	private String charger;
	private Integer qaResult;
	private String qaResultName;
	private String memo;

	//QA信息
	private String realname;
	private String moble;
	private String phone;

	//缺陷信息
	private String status;
	private String priority;
	private String pkey;
	private String environment;
	private String assignedTo;
	private String detectedBy;

	private Map<String, String> testReportMap;
	private Map<String, String> bugReportMap;
	private Map<String, String> tcListReportMap;
	private Map<String, String> tcStatisReportMap;
	private Map<String, String> qaReportMap;

	private String cyclename;
	private String summary;
	private String releaseId;//上线ID

	private String statusName;
	private String priorityName;

	private Integer stepType;
	private String userName;

	public Map<String, String> getTestReportMap() {
		testReportMap = new LinkedHashMap<String, String>();
		testReportMap.put("Cycle Name", "");//Jira中查询
		testReportMap.put("需求名称", summary);
		testReportMap.put("上线ID", releaseId);
		testReportMap.put("领域", teamName);
		testReportMap.put("修改范围", "");//未找到匹配字段- tingting.ao 20130207
		testReportMap.put("测试人员", tester);
		testReportMap.put("验证QA", tester);
		testReportMap.put("测试结果", getQaResultName());//输入参数
		testReportMap.put("开发人员", coder);
		testReportMap.put("项目负责人", charger);
		testReportMap.put("有无遗留问题", memo);//输入参数
		return testReportMap;
	}

	public Map<String, String> getQaReportMap() {
		qaReportMap = new LinkedHashMap<String, String>();
		qaReportMap.put("姓名", realname);
		qaReportMap.put("分机", phone);
		qaReportMap.put("联系方式", moble);
		return qaReportMap;
	}

	public Map<String, String> getBugReportMap() {
		bugReportMap = new LinkedHashMap<String, String>();
		bugReportMap.put("缺陷编号", pkey);
		bugReportMap.put("上线ID", releaseId);
		bugReportMap.put("Status", getStatusName());
		bugReportMap.put("Severity", getPriorityName());
		bugReportMap.put("Summary", summary);
		bugReportMap.put("Environment", environment);
		bugReportMap.put("Assigned To", assignedTo);
		bugReportMap.put("Detected By", detectedBy);
		return bugReportMap;
	}

	public Map<String, String> getListTcReportMap() {
		tcListReportMap = new LinkedHashMap<String, String>();
		tcListReportMap.put("模块", "");
		tcListReportMap.put("功能集", "");
		tcListReportMap.put("测试用例", "");
		tcListReportMap.put("测试结果", "");
		tcListReportMap.put("关联时间", "");
		tcListReportMap.put("关联人", "");
		tcListReportMap.put("执行时间", "");
		tcListReportMap.put("执行人", "");
		return tcListReportMap;
	}

	public Map<String, String> getTcStatisReportMap() {
		tcStatisReportMap = new LinkedHashMap<String, String>();
		tcStatisReportMap.put("测试员", getUserName());
		tcStatisReportMap.put("Case总数", getAllCnt().toString());
		tcStatisReportMap.put("实际执行数", getDoneCnt().toString());
		tcStatisReportMap.put("执行通过", getSuccCnt().toString());
		tcStatisReportMap.put("执行失败", getFailCnt().toString());
		tcStatisReportMap.put("未执行", getUndoneCnt().toString());
		tcStatisReportMap.put("通过率", getPassPer());
		tcStatisReportMap.put("覆盖率", getCoverPer());
		return tcStatisReportMap;
	}

	public void setTcStatisReportMap(Map<String, String> tcStatisReportMap) {
		this.tcStatisReportMap = tcStatisReportMap;
	}

	public void setTestReportMap(Map<String, String> testReportMap) {
		this.testReportMap = testReportMap;
	}

	public void setTcListReportMap(Map<String, String> tcListReportMap) {
		this.tcListReportMap = tcListReportMap;
	}

	public void setCyclename(String cyclename) {
		this.cyclename = cyclename;
	}

	public String getCyclename() {
		return cyclename;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSummary() {
		return summary;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public Integer getAllCnt() {
		return (null == allCnt) ? 0 : allCnt;
	}

	public void setAllCnt(Integer allCnt) {
		this.allCnt = allCnt;
	}

	public Integer getDoneCnt() {
		return (null == doneCnt) ? 0 : doneCnt;
	}

	public void setDoneCnt(Integer doneCnt) {
		this.doneCnt = doneCnt;
	}

	public Integer getUndoneCnt() {
		return (null == undoneCnt) ? 0 : undoneCnt;
	}

	public void setUndoneCnt(Integer undoneCnt) {
		this.undoneCnt = undoneCnt;
	}

	public Integer getSuccCnt() {
		return (null == succCnt) ? 0 : succCnt;
	}

	public void setSuccCnt(Integer succCnt) {
		this.succCnt = succCnt;
	}

	public Integer getFailCnt() {
		return (null == failCnt) ? 0 : failCnt;
	}

	public void setFailCnt(Integer failCnt) {
		this.failCnt = failCnt;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Long getStepId() {
		return stepId;
	}

	public void setCoverPer(String coverPer) {
		this.coverPer = coverPer;
	}

	public String getCoverPer() {
		if (getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			coverPer = df1.format(getDoneCnt().floatValue() / getAllCnt().floatValue());
		}
		return coverPer;
	}

	public void setPassPer(String passPer) {
		this.passPer = passPer;
	}

	public String getPassPer() {
		if (getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			passPer = df1.format(getSuccCnt().floatValue() / getAllCnt().floatValue());
		}
		return passPer;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Long getIssueId() {
		return issueId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTester() {
		return tester;
	}

	public void setTester(String tester) {
		this.tester = tester;
	}

	public String getCoder() {
		return coder;
	}

	public void setCoder(String coder) {
		this.coder = coder;
	}

	public String getCharger() {
		return charger;
	}

	public void setCharger(String charger) {
		this.charger = charger;
	}

	public void setQaReportMap(Map<String, String> qaReportMap) {
		this.qaReportMap = qaReportMap;
	}

	public String getMoble() {
		return moble;
	}

	public void setMoble(String moble) {
		this.moble = moble;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getRealname() {
		return realname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPkey() {
		return pkey;
	}

	public void setPkey(String pkey) {
		this.pkey = pkey;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getDetectedBy() {
		return detectedBy;
	}

	public void setDetectedBy(String detectedBy) {
		this.detectedBy = detectedBy;
	}

	public String getStatusName() {
		if (null != status && !status.equals("")) {
			statusName = PrimaryDataFactory.issueStatusMap.get(status);
		}
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPriorityName() {
		if (null != priority && !priority.equals("")) {
			priorityName = PrimaryDataFactory.priorityMap.get(priority);
		}
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	public void setQaResult(Integer qaResult) {
		this.qaResult = qaResult;
	}

	public Integer getQaResult() {
		return qaResult;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemo() {
		return memo;
	}

	public void setQaResultName(String qaResultName) {
		this.qaResultName = qaResultName;
	}

	public String getQaResultName() {
		if (null != qaResult) {
			if (qaResult.compareTo(0) == 0) {
				qaResultName = "待确认";
			} else if (qaResult.compareTo(1) == 0) {
				qaResultName = "测试通过";
			} else if (qaResult.compareTo(2) == 0) {
				qaResultName = "测试不通过";
			}
		}
		return qaResultName;
	}

	public Integer getStepType() {
		return stepType;
	}

	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
