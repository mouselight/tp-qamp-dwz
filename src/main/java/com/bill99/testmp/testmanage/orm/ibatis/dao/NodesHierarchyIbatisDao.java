package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bill99.riaframework.common.dto.CaseStateDto;
import com.bill99.seashell.orm.QueryDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public interface NodesHierarchyIbatisDao extends QueryDao {

	public void initTestCase(String sqlMap, Map<String, Long> map);

	public List<NodesHierarchy> getTaTaskTcs(String taTaskId);

	public void update4InvalidTc(Long id, Integer status);

	public List<Long> getAssoNodes(Long planStepId);

	public List<TestProjects> getProjectGroup(Long planStepId);

	public List<TestProjects> getGroup(Set<Long> pids);

	public CaseStateDto getTcStateByStep(Long pid, Long stepId);

	public List<NodesHierarchy> getTcSuiteByStep(Long pid, Long stepId);
	
	public Map<Long,Long> getNodeTcCount(Long nodeId);

}
