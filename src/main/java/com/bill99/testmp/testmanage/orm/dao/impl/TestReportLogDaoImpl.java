package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.testmp.testmanage.orm.dao.TestReportLogDao;
import com.bill99.testmp.testmanage.orm.entity.TestReportLog;
import com.jeecms.common.hibernate3.BaseDaoImpl;

public class TestReportLogDaoImpl extends BaseDaoImpl<TestReportLog> implements TestReportLogDao {

}
