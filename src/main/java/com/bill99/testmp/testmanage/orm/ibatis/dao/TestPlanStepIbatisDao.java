package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.common.dto.MustStepDto;

public interface TestPlanStepIbatisDao {
	public Map<Long, String> getEnvStep(Long stepId);

	public Long insertMustStep(Map mustStep);
	
	public List<MustStepDto> findMustStep();
	
	public void insertMustTc(List<MustStepDto> mustStepList);
	
	public void copyTc(String startStepId,String endStepId);
}
