package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public interface NodesHierarchyMng {

	public NodesHierarchy getById(Long id);

	public List<NodesHierarchy> getTestsuiteById(Long id);

	// 获得所有根节点
	public List<NodesHierarchy> getRoots();

	// 获得子节点
	public List<NodesHierarchy> getChild(Long parentId);

	// 获得子目录节点
	public List<NodesHierarchy> getChildSuite(Long parentId);

	//获取子节点ID
	public Map<Integer, List<Long>> getAllChildIds(List<Long> parentIds);

	public List<Long> getNoAssoChildIds(List<Long> parentIds, Long stepId);

	public List<Long> getChildHql4Count(Long parentId);

	// 获得核心用例子节点
	public List<NodesHierarchy> getCoreTestCaseChild(Long parentId);

	public List<NodesHierarchy> findAll();

	public long getTcQuantity(Long pid);

	public NodesHierarchy save(NodesHierarchy nodesHierarchy);

	public NodesHierarchy update(NodesHierarchy nodesHierarchy);

	public void initTestCase(Long testprojectId, Long parentId);

	/**
	 * 查询未关联到Step的TC及目录- 关联时左侧展现
	 * @param parentId 页面请求的父目录pid
	 * @param planStepId 用于排除已关联的Tc目录
	 * @param tcIds 根据查询条件获取的tcId 用于限定tc集
	 * @param partIds 根据查询条件获取的partIds 用于限定目录集
	 * @return
	 */
	public List<NodesHierarchy> getNoAssoChild(Long parentId, Long planStepId, List<Long> tcIds, List<Long> partIds);

	public List<NodesHierarchy> getAssoChildWithFilter(Long parentId, Long planStepId, Integer executionType, List<Long> tcIds, List<Long> partIds);

	public List<NodesHierarchy> getAssoChild(Long parentId, List<Long> allTcParentIds, Long planStepId, Integer executionType, Integer exeStatus);

	public List<Long> getAllParentIds(List<Long> childIds);

	public void delete(Long nodeId);

	public List<NodesHierarchy> getChild4Cloud(Long parentId, Long idCloud);

	public List<NodesHierarchy> getChild4Ta(Long parentId, Long idSvnMethod);

	public List<Long> getChildFileIds(List<Long> parentIds);

	public void rvCaseAndSuite(Long projectId);

	public List<NodesHierarchy> getChild4TaTaskTc(Long pid, String taTaskId);

	public List<NodesHierarchy> getNoAssoChild4Ta(Long pid, List<Long> tcIds, List<Long> partIds);

	public void recursive4InvalidTc(NodesHierarchy nodesHierarchy);

	public void save4CopyNodesHierarchy(Long testSuiteIdOld, Long testSuiteIdNew);

	public List<NodesHierarchy> getParents(List<Long> childs);

	public List<Long> getAssoNodes(Long planStepId);

	public List<TestProjects> getProjectGroup(Long planStepId);

	public List<TestProjects> getGroup(Set<Long> pids);

	public Long getTcQuantity(Long pid, Integer exeType);

	public List<Long> getChildHql4Count(Long pid, Integer exeType);

}
