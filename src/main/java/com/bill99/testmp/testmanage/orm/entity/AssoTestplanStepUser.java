package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

import com.bill99.riaframework.orm.entity.User;

public class AssoTestplanStepUser implements Serializable {

	private static final long serialVersionUID = -7006669542259072567L;

	private StepUserPk stepUserPk;
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setStepUserPk(StepUserPk stepUserPk) {
		this.stepUserPk = stepUserPk;
	}

	public StepUserPk getStepUserPk() {
		return stepUserPk;
	}

}
