package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TestIssueField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2469796887451496037L;

	private Long id;
	private Long issueid;
	private Long customfield;
	private String stringvalue;
	private BigDecimal numbervalue;
	private String textvalue;
	private Date datevalue;
	private Date crttime;

	public Long getIssueid() {
		return issueid;
	}

	public void setIssueid(Long issueid) {
		this.issueid = issueid;
	}

	public Long getCustomfield() {
		return customfield;
	}

	public void setCustomfield(Long customfield) {
		this.customfield = customfield;
	}

	public String getStringvalue() {
		return stringvalue;
	}

	public void setStringvalue(String stringvalue) {
		this.stringvalue = stringvalue;
	}

	public BigDecimal getNumbervalue() {
		return numbervalue;
	}

	public void setNumbervalue(BigDecimal numbervalue) {
		this.numbervalue = numbervalue;
	}

	public String getTextvalue() {
		return textvalue;
	}

	public void setTextvalue(String textvalue) {
		this.textvalue = textvalue;
	}

	public Date getDatevalue() {
		return datevalue;
	}

	public void setDatevalue(Date datevalue) {
		this.datevalue = datevalue;
	}

	public Date getCrttime() {
		return crttime;
	}

	public void setCrttime(Date crttime) {
		this.crttime = crttime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
