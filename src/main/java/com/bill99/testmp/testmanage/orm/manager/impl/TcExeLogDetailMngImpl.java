package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TcExeLogDetailDao;
import com.bill99.testmp.testmanage.orm.entity.TcExeLogDetail;
import com.bill99.testmp.testmanage.orm.manager.TcExeLogDetailMng;

public class TcExeLogDetailMngImpl implements TcExeLogDetailMng {

	private TcExeLogDetailDao tcExeLogDetailDao;

	public void saveExeLogDetail(TcExeLogDetail tcExeLogDetail) {
		tcExeLogDetailDao.save(tcExeLogDetail);
	}

	public List<TcExeLogDetail> findByPlanStepId(Long planStepId) {
		return tcExeLogDetailDao.findBy("planStepId",planStepId);
	}

	public void setTcExeLogDetailDao(TcExeLogDetailDao tcExeLogDetailDao) {
		this.tcExeLogDetailDao = tcExeLogDetailDao;
	}

}
