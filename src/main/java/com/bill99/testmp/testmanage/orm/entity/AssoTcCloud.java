package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class AssoTcCloud implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7388209504293805172L;

	private AssoPk assoPk;
	private Integer nodeTypeId;

	public AssoPk getAssoPk() {
		return assoPk;
	}

	public void setAssoPk(AssoPk assoPk) {
		this.assoPk = assoPk;
	}

	public Integer getNodeTypeId() {
		return nodeTypeId;
	}

	public void setNodeTypeId(Integer nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}
}
