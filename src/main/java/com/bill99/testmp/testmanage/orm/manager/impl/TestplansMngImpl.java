package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.bill99.testmp.testmanage.orm.dao.TestplansDao;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;
import com.jeecms.common.page.Pagination;

public class TestplansMngImpl implements TestplansMng {

	private TestplansDao testplansDao;
	private TestPlanIbatisDao testPlanIbatisDao;

	public void setTestplansDao(TestplansDao testplansDao) {
		this.testplansDao = testplansDao;
	}

	public void setTestPlanIbatisDao(TestPlanIbatisDao testPlanIbatisDao) {
		this.testPlanIbatisDao = testPlanIbatisDao;
	}

	public Testplans findByIssueId(Long issueId) {
		Testplans testPlan = testplansDao.findByIssueId(issueId);
		if (null == testPlan) {
			testPlan = new Testplans();
		}
		return testPlan;
	}

	public Testplans save(Testplans testplans) {
		return testplansDao.save(testplans);
	}

	public Testplans update(Testplans testplans) {
		return testplansDao.update(testplans);
	}

	public Pagination query(Testplans testPlan, Integer pageNo, Integer pageSize) {
		Pagination pagination = new Pagination();
		List<Testplans> testPlanList = new ArrayList<Testplans>();

		int count = testPlanIbatisDao.queryCount(testPlan);
		if (count > 0) {
			testPlan.setStartRow((pageNo - 1) * pageSize + 1);
			testPlan.setEndRow(pageNo * pageSize);
			testPlanList = testPlanIbatisDao.query(testPlan);
		}

		pagination.setList(testPlanList);
		pagination.setPageNo(pageNo);
		pagination.setPageSize(pageSize);
		pagination.setTotalCount(count);
		return pagination;
	}

	public List<Testplans> getByIds(List<Long> ids) {
		Assert.notEmpty(ids);
		return testplansDao.get(ids);
	}

	public List<Testplans> query(Testplans testPlan) {
		return testPlanIbatisDao.query(testPlan);
	}

	public Integer queryCount(Testplans testPlan) {
		return testPlanIbatisDao.queryCount(testPlan);
	}

	public Testplans findById(Long testplansId) {
		return testplansDao.get(testplansId);
	}
}
