package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TestReportLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6981359246381598421L;

	private Long id;
	private Long planId;
	private Long stepId;
	private String releasId;
	private Integer qaResult;
	private String svnPath;
	private String memo;
	private Boolean ctmpSyncResult;
	private Boolean svnCommitResult;
	private String createUser;
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getStepId() {
		return stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public String getReleasId() {
		return releasId;
	}

	public void setReleasId(String releasId) {
		this.releasId = releasId;
	}

	public Integer getQaResult() {
		return qaResult;
	}

	public void setQaResult(Integer qaResult) {
		this.qaResult = qaResult;
	}

	public String getSvnPath() {
		return svnPath;
	}

	public void setSvnPath(String svnPath) {
		this.svnPath = svnPath;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Boolean getCtmpSyncResult() {
		return ctmpSyncResult;
	}

	public void setCtmpSyncResult(Boolean ctmpSyncResult) {
		this.ctmpSyncResult = ctmpSyncResult;
	}

	public Boolean getSvnCommitResult() {
		return svnCommitResult;
	}

	public void setSvnCommitResult(Boolean svnCommitResult) {
		this.svnCommitResult = svnCommitResult;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
