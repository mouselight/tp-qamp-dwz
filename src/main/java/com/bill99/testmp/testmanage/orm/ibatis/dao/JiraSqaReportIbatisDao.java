package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.common.dto.TaSqaReportDto;
import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;
import com.bill99.testmp.testmanage.orm.entity.TestReport;

public interface JiraSqaReportIbatisDao {

	

	public List<TaSqaReportDto> getProjectName(TaSqaReportDto taSqaReportDto);

	public List<TaSqaReportDto> getBugLevelDev(TaSqaReportDto taSqaReportDto);

	public List<TaSqaReportDto> getBugLevelStage02(TaSqaReportDto taSqaReportDto);
	
	public List<TaSqaReportDto> getBugLevelPro(TaSqaReportDto taSqaReportDto);
	
	public List<TaSqaReportDto> getBugcategoryDev(TaSqaReportDto taSqaReportDto);
	
	public List<TaSqaReportDto> getBugcategoryStage02(TaSqaReportDto taSqaReportDto);
	
	public List<TaSqaReportDto> getBugcategoryPro(TaSqaReportDto taSqaReportDto);
	public List<TaSqaReportDto> getBugstatus(TaSqaReportDto taSqaReportDto);
	
	public List<TaSqaReportDto> getEnvBug(TaSqaReportDto taSqaReportDto);
	
	
	
	

}
