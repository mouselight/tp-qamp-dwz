package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.framework.enums.TestPlanStepEnum;

public class TestplanSteps implements Serializable {

	private static final long serialVersionUID = 8039560277318943789L;

	private Long stepId;
	private Integer stepNum;
	private Integer type;
	private Long planId;
	private Date beginTime;
	private Date endTime;
	private String output;
	private String memo;
	private Integer state;
	private Date createTime;
	private Date updateTime;
	private String createUser;
	private String updateUser;
	private String realnames;
	private String userids;
	private String assoCloud;

	private String typeName;
	private String stateName;

	private Set<AssoTestplanStepUser> assoTestplanStepUsers;
	private Integer assoTcCount;
	private Integer assoFinishTcCount;
	private String coverPer;
	private boolean writer;
	private List<Long> assoCloudIds;

	private TestplanSteps parent;
	private String unitType;
	private Long parentId;

	private Boolean isMust;

	public Long getStepId() {
		return stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Integer getStepNum() {
		return stepNum;
	}

	public void setStepNum(Integer stepNum) {
		this.stepNum = stepNum;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Set<AssoTestplanStepUser> getAssoTestplanStepUsers() {
		return assoTestplanStepUsers;
	}

	public void setAssoTestplanStepUsers(Set<AssoTestplanStepUser> assoTestplanStepUsers) {
		this.assoTestplanStepUsers = assoTestplanStepUsers;
	}

	public void setUserids(String userids) {
		this.userids = userids;
	}

	public String getUserids() {
		return userids;
	}

	public void setRealnames(String realnames) {
		this.realnames = realnames;
	}

	public String getRealnames() {
		return realnames;
	}

	public void setAssoTcCount(Integer assoTcCount) {
		this.assoTcCount = assoTcCount;
	}

	public Integer getAssoTcCount() {
		return assoTcCount;
	}

	public void setAssoFinishTcCount(Integer assoFinishTcCount) {
		this.assoFinishTcCount = assoFinishTcCount;
	}

	public Integer getAssoFinishTcCount() {
		return assoFinishTcCount;
	}

	public void setWriter(boolean writer) {
		this.writer = writer;
	}

	public boolean isWriter() {
		return writer;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		if (null != type) {
			typeName = TestPlanStepEnum.getTestPlanStepValue(type);
		}

		return typeName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateName() {
		if (null != state) {
			stateName = TestPlanStatusEnum.getTestPlanStatusValue(state);
		}
		return stateName;
	}

	public void setCoverPer(String coverPer) {
		this.coverPer = coverPer;
	}

	public String getCoverPer() {
		if (getAssoTcCount().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			//			df1.setRoundingMode(RoundingMode.HALF_DOWN);
			coverPer = df1.format(getAssoFinishTcCount().floatValue() / getAssoTcCount().floatValue());
		}
		return coverPer;
	}

	public void setAssoCloud(String assoCloud) {
		this.assoCloud = assoCloud;
	}

	public String getAssoCloud() {
		return assoCloud;
	}

	public void setAssoCloudIds(List<Long> assoCloudIds) {
		this.assoCloudIds = assoCloudIds;
	}

	public List<Long> getAssoCloudIds() {
		assoCloudIds = StringUtil.strToListObj(assoCloud);
		return assoCloudIds;
	}

	public TestplanSteps getParent() {
		return parent;
	}

	public void setParent(TestplanSteps parent) {
		this.parent = parent;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public static void main(String[] args) {
		DecimalFormat df1 = new DecimalFormat("#0.##%");
		//		df1.setRoundingMode(RoundingMode.HALF_DOWN);
		String coverPer = df1.format(new Integer(4823).floatValue() / new Integer(4844).floatValue());
		//		coverPer = new BigDecimal(4823).divide(new BigDecimal(4844), 0, BigDecimal.ROUND_HALF_UP).toString();
		System.out.println(coverPer);
	}

	public Boolean getIsMust() {
		return isMust;
	}

	public void setIsMust(Boolean isMust) {
		this.isMust = isMust;
	}
}
