package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;

import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.orm.dao.NodesHierarchyDao;
import com.bill99.testmp.testmanage.orm.dao.TcStepsDao;
import com.bill99.testmp.testmanage.orm.dao.TestCasesDao;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.ibatis.dao.NodesHierarchyIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

public class NodesHierarchyMngImpl implements NodesHierarchyMng {

	private NodesHierarchyDao nodesHierarchyDao;
	private NodesHierarchyIbatisDao nodesHierarchyIbatisDao;
	private TestCasesDao testCasesDao;
	private TcStepsDao tcStepsDao;

	public void setNodesHierarchyDao(NodesHierarchyDao nodesHierarchyDao) {
		this.nodesHierarchyDao = nodesHierarchyDao;
	}

	public List<NodesHierarchy> getRoots() {
		return nodesHierarchyDao.getRoots();
	}

	public List<NodesHierarchy> getChild(Long parentId) {
		return nodesHierarchyDao.getChild(parentId);
	}

	public NodesHierarchy getById(Long id) {
		return nodesHierarchyDao.getById(id);
	}

	public List<NodesHierarchy> getTestsuiteById(Long id) {
		return nodesHierarchyDao.getTestsuiteById(id);
	}

	public List<NodesHierarchy> getCoreTestCaseChild(Long parentId) {
		return nodesHierarchyDao.getCoreTestCaseChild(parentId);
	}

	public List<NodesHierarchy> findAll() {
		return nodesHierarchyDao.findAll();
	}

	public long getTcQuantity(Long pid) {
		return nodesHierarchyDao.getTcQuantity(pid);
	}

	public void setNodesHierarchyIbatisDao(NodesHierarchyIbatisDao nodesHierarchyIbatisDao) {
		this.nodesHierarchyIbatisDao = nodesHierarchyIbatisDao;
	}

	public NodesHierarchy save(NodesHierarchy nodesHierarchy) {
		return nodesHierarchyDao.save(nodesHierarchy);
	}

	public NodesHierarchy update(NodesHierarchy nodesHierarchy) {
		return nodesHierarchyDao.update(nodesHierarchy);
	}

	public List<Long> getChildHql4Count(Long parentId) {
		return nodesHierarchyDao.getChildHql4Count(parentId);
	}

	public void initTestCase(Long testprojectId, Long parentId) {
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("testprojectId", testprojectId);
		map.put("parentId", parentId);
		nodesHierarchyIbatisDao.initTestCase("TestCase.initTestCaseHql", map);
	}

	public List<NodesHierarchy> getChildSuite(Long parentId) {
		return nodesHierarchyDao.getChildSuite(parentId);
	}

	public List<NodesHierarchy> getAssoChild(Long parentId, List<Long> allTcParentIds, Long planStepId, Integer executionType, Integer exeStatus) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("stepId", planStepId);
		map.put("allTcParentIds", allTcParentIds);
		map.put("executionType", executionType);
		map.put("exeStatus", exeStatus);
		return nodesHierarchyIbatisDao.queryForList("TestCase.getAssoChild", map);
	}

	public List<NodesHierarchy> getNoAssoChild(Long parentId, Long planStepId, List<Long> tcIds, List<Long> partIds) {
		Assert.notNull(parentId);
		Assert.notNull(planStepId);

		List<NodesHierarchy> list = new ArrayList<NodesHierarchy>();

		/*
		 * size=0:选择过滤条件后-无结果.不再展现
		 * list==null:没有进行过滤 
		 */
		if (null != tcIds && null != partIds && tcIds.size() == 0 && partIds.size() == 0) {
			return list;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("stepId", planStepId);
		map.put("partIds", partIds);

		//node
		List<NodesHierarchy> listNode = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoNode", map);
		list.addAll(listNode);

		//防止TC总数超过1000
		if (null != tcIds && tcIds.size() > 0) {
			for (int i = 0; i < tcIds.size(); i = i + 1000) {
				int endIndex = i + 1000;
				if (i + 1000 > tcIds.size()) {
					endIndex = tcIds.size();
				}
				map.put("tcIds", tcIds.subList(i, endIndex));
				//tc
				List<NodesHierarchy> list_ = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoChild", map);
				list.addAll(list_);
			}
		} else {
			List<NodesHierarchy> list_ = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoChild", map);
			list.addAll(list_);
		}
		return list;
	}

	public List<NodesHierarchy> getNoAssoChild4Ta(Long parentId, List<Long> tcIds, List<Long> partIds) {
		Assert.notNull(parentId);

		List<NodesHierarchy> list = new ArrayList<NodesHierarchy>();

		/*
		 * size=0:选择过滤条件后-无结果.不再展现
		 * list==null:没有进行过滤 
		 */
		if (null != tcIds && null != partIds && tcIds.size() == 0 && partIds.size() == 0) {
			return list;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("partIds", partIds);

		//node
		List<NodesHierarchy> listNode = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoNode4Ta", map);
		list.addAll(listNode);

		//防止TC总数超过1000
		if (null != tcIds && tcIds.size() > 0) {
			for (int i = 0; i < tcIds.size(); i = i + 1000) {
				int endIndex = i + 1000;
				if (i + 1000 > tcIds.size()) {
					endIndex = tcIds.size();
				}
				map.put("tcIds", tcIds.subList(i, endIndex));
				//tc
				List<NodesHierarchy> list_ = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoChild4Ta", map);
				list.addAll(list_);
			}
		} else {
			List<NodesHierarchy> list_ = nodesHierarchyIbatisDao.queryForList("TestCase.getNoAssoChild4Ta", map);
			list.addAll(list_);
		}
		return list;
	}

	public void delete(Long nodeId) {
		nodesHierarchyDao.delete(nodeId);
	}

	//获取子节点的所有项目节点
	public List<Long> getAllProjectIds(List<Long> childIds) {
		List<Long> allParentIds = new ArrayList<Long>();
		if (null != childIds && childIds.size() > 0) {
			while (true) {
				List<Long> parentIds = nodesHierarchyDao.getParentIds(childIds);
				if (null == parentIds || parentIds.size() <= 0) {
					break;
				}
				allParentIds.addAll(parentIds);
				childIds = parentIds;
			}
		}
		return (null != allParentIds ? allParentIds : new ArrayList<Long>());
	}

	//获取子节点的所有父节点
	public List<Long> getAllParentIds(List<Long> childIds) {
		List<Long> allParentIds = new ArrayList<Long>();
		if (null != childIds && childIds.size() > 0) {
			while (true) {
				List<Long> parentIds = nodesHierarchyDao.getParentIds(childIds);
				if (null == parentIds || parentIds.size() <= 0) {
					break;
				}
				allParentIds.addAll(parentIds);
				childIds = parentIds;
			}
		}
		return (null != allParentIds ? allParentIds : new ArrayList<Long>());
	}

	//获取父节点的所有子节点
	public Map<Integer, List<Long>> getAllChildIds(List<Long> parentIds) {
		Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();

		List<Long> childIds = new ArrayList<Long>();
		List<Long> childFileIds = new ArrayList<Long>();

		List<Long> childIds_ = new ArrayList<Long>();
		List<Long> childFileIds_ = new ArrayList<Long>();
		if (null != parentIds && parentIds.size() > 0) {
			childFileIds.addAll(parentIds);//原父节点同时返回
			while (true) {
				//叶子节点
				childIds_ = nodesHierarchyDao.getChildIds(parentIds);
				childIds.addAll((null != childIds_) ? childIds_ : new ArrayList<Long>());

				//父节点
				childFileIds_ = nodesHierarchyDao.getChildFileIds(parentIds);
				childFileIds.addAll((null != childFileIds_) ? childFileIds_ : new ArrayList<Long>());

				parentIds = childFileIds_;
				if (null == parentIds || parentIds.size() == 0) {
					break;
				}
			}

		}
		nodeMap.put(TreeUtils.CHILD_NODE, childIds);
		nodeMap.put(TreeUtils.FATHER_NODE, childFileIds);
		return nodeMap;
	}

	//获取父节点的所有未关联的子节点
	public List<Long> getNoAssoChildIds(List<Long> parentIds, Long stepId) {
		List<Long> childIds = new ArrayList<Long>();
		List<Long> childIds_ = new ArrayList<Long>();
		List<Long> childFileIds_ = new ArrayList<Long>();
		if (null != parentIds && parentIds.size() > 0) {
			while (true) {
				//叶子节点
				childIds_ = nodesHierarchyDao.getNoAssoChildIds(parentIds, stepId);
				childIds.addAll((null != childIds_) ? childIds_ : new ArrayList<Long>());
				//父节点
				childFileIds_ = nodesHierarchyDao.getChildFileIds(parentIds);
				parentIds = childFileIds_;
				if (null == parentIds || parentIds.size() == 0) {
					break;
				}
			}
		}
		return childIds;
	}

	public List<NodesHierarchy> getChild4Ta(Long parentId, Long idSvnMethod) {

		List<Long> childIds = nodesHierarchyIbatisDao.queryForList("TestAutomation.getTaTcIds", idSvnMethod);
		if (childIds == null || childIds.size() < 1) {
			return new ArrayList<NodesHierarchy>();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("idSvnMethod", idSvnMethod);
		map.put("allTcParentIds", getAllParentIds(childIds));
		return nodesHierarchyIbatisDao.queryForList("TestCase.getAssoChild4Ta", map);
	}

	public List<Long> getChildFileIds(List<Long> parentIds) {
		return nodesHierarchyDao.getChildFileIds(parentIds);
	}

	public static List<NodesHierarchy> nhs4bath;

	public void rvCaseAndSuite(Long projectId) {
		nhs4bath = new ArrayList<NodesHierarchy>();
		recursion4rvCaseAndSuite(projectId);
		delete(projectId);
		if (nhs4bath.size() > 0) {
			nodesHierarchyDao.batch4rvCaseAndSuite(nhs4bath);
		}
	}

	public void recursion4rvCaseAndSuite(Long pid) {
		List<NodesHierarchy> nhs = getChild(pid);
		if (nhs != null) {
			for (NodesHierarchy nh : nhs) {
				delete(nh.getId());
				//				nhs4bath.add(nh);
				recursion4rvCaseAndSuite(nh.getId());
			}
		}
	}

	public List<NodesHierarchy> getChild4Cloud(Long parentId, Long idCloud) {

		List<Long> childIds = nodesHierarchyIbatisDao.queryForList("TestCaseCloud.getCloudTcIds", idCloud);
		if (childIds == null || childIds.size() < 1) {
			return new ArrayList<NodesHierarchy>();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", parentId);
		map.put("idCloud", idCloud);
		map.put("allTcParentIds", getAllParentIds(childIds));
		return nodesHierarchyIbatisDao.queryForList("TestCaseCloud.getAssoChild4Cloud", map);
	}

	public List<NodesHierarchy> getChild4TaTaskTc(Long pid, String taTaskId) {

		List<Long> childIds = nodesHierarchyIbatisDao.queryForList("TestAutomation.getTaTaskTcIds", taTaskId);
		if (childIds == null || childIds.size() < 1) {
			return new ArrayList<NodesHierarchy>();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", pid);
		map.put("taTaskId", taTaskId);
		map.put("allTcParentIds", getAllParentIds(childIds));
		return nodesHierarchyIbatisDao.queryForList("TestCase.getAssoChild4TaTask", map);
	}

	//实时递归
	public void recursive4InvalidTc(NodesHierarchy nodesHierarchy) {
		nodesHierarchyIbatisDao.update4InvalidTc(nodesHierarchy.getId(), nodesHierarchy.getStatus());
		List<NodesHierarchy> nhs = nodesHierarchyDao.getChildSuite4InvaildTc(nodesHierarchy.getId());
		if (nhs != null) {
			for (NodesHierarchy nh : nhs) {
				recursive4InvalidTc(nh);
			}
		}
	}

	public void save4CopyNodesHierarchy(Long testSuiteIdOld, Long testSuiteIdNew) {
		save4CopyNodesHierarchy(nodesHierarchyDao.getById(testSuiteIdOld), testSuiteIdNew);
	}

	//实时递归
	public void save4CopyNodesHierarchy(NodesHierarchy nodesHierarchy, Long testSuiteIdNew) {
		//复制用例集合
		NodesHierarchy nhCurrent = new NodesHierarchy();
		BeanUtils.copyProperties(nodesHierarchy, nhCurrent, new String[] { "id", "parent" });
		nhCurrent.setParentId(testSuiteIdNew);
		nhCurrent = nodesHierarchyDao.save(nhCurrent);

		List<TestCases> tcs = nodesHierarchyDao.getTcsByNhId(nodesHierarchy.getId());
		if (tcs != null && tcs.size() > 0) {
			//复制用例
			for (TestCases testCase : tcs) {
				NodesHierarchy nhChild = new NodesHierarchy();
				nhChild.setName(testCase.getTestCaseName());
				nhChild.setNodeTypeId(NodeTypeUtils.TESTCASE);
				nhChild.setStatus(1);
				nhChild.setParentId(nhCurrent.getId());
				nhChild = nodesHierarchyDao.save(nhChild);

				//复制关键字
				Set<Keywords> keywords = testCase.getKeywordsSet();
				Set<Keywords> keywordsNew = null;
				if (keywords != null) {
					keywordsNew = new HashSet<Keywords>();
					for (Keywords keyword : keywords) {
						keywordsNew.add(keyword);
					}
				}

				TestCases testCaseNew = new TestCases();
				BeanUtils.copyProperties(testCase, testCaseNew, new String[] { "testCaseId", "createDate", "tcStepsList", "keywordsSet" });
				testCaseNew.setTestCaseId(nhChild.getId());
				testCaseNew.setKeywordsSet(keywordsNew);
				testCaseNew = testCasesDao.save(testCaseNew);

				//复制测试步骤
				List<TcSteps> tcSteps = testCase.getTcStepsList();
				if (tcSteps != null) {
					for (TcSteps tcStep : tcSteps) {
						TcSteps tcStepNew = new TcSteps();
						BeanUtils.copyProperties(tcStep, tcStepNew, new String[] { "testCaseId", "id" });
						tcStepNew.setTestCaseId(testCaseNew.getTestCaseId());
						tcStepsDao.save(tcStepNew);
					}
				}

			}
		}

		List<NodesHierarchy> nodesHierarchySuites = nodesHierarchyDao.getChildSuite(nodesHierarchy.getId());
		if (nodesHierarchySuites != null && nodesHierarchySuites.size() > 0) {
			for (NodesHierarchy nodesHierarchy2 : nodesHierarchySuites) {
				save4CopyNodesHierarchy(nodesHierarchy2, nhCurrent.getId());
			}

		}

	}

	public void setTestCasesDao(TestCasesDao testCasesDao) {
		this.testCasesDao = testCasesDao;
	}

	public void setTcStepsDao(TcStepsDao tcStepsDao) {
		this.tcStepsDao = tcStepsDao;
	}

	public List<NodesHierarchy> getParents(List<Long> childs) {
		return null;
	}

	public List<Long> getAssoNodes(Long planStepId) {
		return nodesHierarchyIbatisDao.getAssoNodes(planStepId);
	}

	public List<TestProjects> getProjectGroup(Long planStepId) {
		return nodesHierarchyIbatisDao.getProjectGroup(planStepId);
	}

	public List<TestProjects> getGroup(Set<Long> pids) {
		return nodesHierarchyIbatisDao.getGroup(pids);
	}

	public List<NodesHierarchy> getAssoChildWithFilter(Long parentId, Long planStepId, Integer executionType, List<Long> tcIds, List<Long> partIds) {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getTcQuantity(Long pid, Integer exeType) {
		return nodesHierarchyDao.getTcQuantity(pid, exeType);
	}

	public List<Long> getChildHql4Count(Long pid, Integer exeType) {
		return nodesHierarchyDao.getChildHql4Count(pid, exeType);
	}

}