package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TestTaTaskLogDao;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;

public class TestTaTaskLogDaoImpl extends BaseDaoSupport<TestTaTaskLog, Long> implements TestTaTaskLogDao {

	private final String getByPlanIdHql = "from TestTaTaskLog where planStepId=? and taTaskId is not null  order by updateDate desc";

	public List<TestTaTaskLog> getByPlanId(Long planStepId) {
		return find(getByPlanIdHql, planStepId);
	}

	public TestTaTaskLog getLastTask(Long planId) {
		return findUnique(getByPlanIdHql, planId);
	}

	private final String getByPlanStepIdHql = "from TestTaTaskLog where planStepId=? order by createDate desc";

	public List<TestTaTaskLog> getByPlanStepId(Long planStepId) {
		return find(getByPlanStepIdHql, planStepId);
	}

}
