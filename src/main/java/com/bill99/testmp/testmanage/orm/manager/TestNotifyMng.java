package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TestNotifyIssuePlanDto;

public interface TestNotifyMng {

	public List<TestNotifyIssuePlanDto> getNotifyIssuePlan(String cycleId, Long teamId);

}
