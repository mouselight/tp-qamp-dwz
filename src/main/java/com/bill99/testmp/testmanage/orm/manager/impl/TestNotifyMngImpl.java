package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TestNotifyIssuePlanDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestNotifyIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestNotifyMng;

public class TestNotifyMngImpl implements TestNotifyMng {

	private TestNotifyIbatisDao testNotifyIbatisDao;

	public void setTestNotifyIbatisDao(TestNotifyIbatisDao testNotifyIbatisDao) {
		this.testNotifyIbatisDao = testNotifyIbatisDao;
	}

	public List<TestNotifyIssuePlanDto> getNotifyIssuePlan(String cycleId, Long teamId) {
		return testNotifyIbatisDao.getNotifyIssuePlan(cycleId, teamId);
	}

}
