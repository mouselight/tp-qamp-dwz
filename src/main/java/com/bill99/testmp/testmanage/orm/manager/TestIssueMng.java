package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.jeecms.common.page.Pagination;

public interface TestIssueMng {

	TestIssue findById(Long id);

	List<TestIssue> query(TestIssue testIssue);

	Integer queryCount(TestIssue testIssue);

	Pagination query(TestIssue testIssue, Integer pageNo, Integer pageSize);

	TestIssue getIssueDetail(Long issueId);

	List<User> queryUser(String realname, List<Long> teamIdList);

	Map<String, String> queryCycle(Long teamId);
	
	List<Long>querytemIdByUserId(List<Integer> userIdList);
	
	

}
