package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.framework.utils.PrimaryDataFactory;

public class TestIssue implements Serializable {

	private static final long serialVersionUID = -1881546080483857251L;

	private Long id;
	private Long issueId;
	private String pkey;
	private Long teamId;
	private String teamName;
	private String issueType;
	private String summary;
	private String description;
	private String issueStatus;
	private String reporter;
	private String assignee;
	private Date created;
	private Date updated;
	private Date crttime;
	private Long cycleId;
	private Boolean isMust;

	//cond
	private String releaseId;//上线ID
	private Date createdStart;
	private Date createdEnd;
	private Integer startRow;
	private Integer endRow;
	private Date releaseDate;//期望上线日期
	private Integer isReleaseDate;//是否有上线时间作为条件
	private String srsSvnPath;//需求文档SVN路径
	private Date releaseDateStart;//预计上线时间始
	private Date releaseDateEnd;//预计上线时间始止
	private String issueStatusName;
	private String issueTypeName;

	private Long[] issueStatusCos;
	private Long[] planStatusCos;
	private Set<TestIssueField> testIssueField;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getPkey() {
		return pkey;
	}

	public void setPkey(String pkey) {
		this.pkey = pkey;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIssueStatus() {
		return issueStatus;
	}

	public void setIssueStatus(String issueStatus) {
		this.issueStatus = issueStatus;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Date getCrttime() {
		return crttime;
	}

	public void setCrttime(Date crttime) {
		this.crttime = crttime;
	}

	public void setCreatedStart(Date createdStart) {
		this.createdStart = createdStart;
	}

	public Date getCreatedStart() {
		return createdStart;
	}

	public void setCreatedEnd(Date createdEnd) {
		this.createdEnd = createdEnd;
	}

	public Date getCreatedEnd() {
		return createdEnd;
	}

	public void setTestIssueField(Set<TestIssueField> testIssueField) {
		this.testIssueField = testIssueField;
	}

	public Set<TestIssueField> getTestIssueField() {
		return testIssueField;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public void setReleaseDateStart(Date releaseDateStart) {
		this.releaseDateStart = releaseDateStart;
	}

	public Date getReleaseDateStart() {
		return releaseDateStart;
	}

	public void setReleaseDateEnd(Date releaseDateEnd) {
		this.releaseDateEnd = releaseDateEnd;
	}

	public Date getReleaseDateEnd() {
		return releaseDateEnd;
	}

	public void setIsReleaseDate(Integer isReleaseDate) {
		this.isReleaseDate = isReleaseDate;
	}

	public Integer getIsReleaseDate() {
		if (null != releaseDateStart || null != releaseDateEnd) {
			isReleaseDate = new Integer(1);
		}
		return isReleaseDate;
	}

	public void setIssueStatusName(String issueStatusName) {
		this.issueStatusName = issueStatusName;
	}

	public String getIssueStatusName() {
		if (!StringUtil.isNull(issueStatus)) {
			issueStatusName = PrimaryDataFactory.issueStatusMap.get(issueStatus);
		}
		return issueStatusName;
	}

	public void setSrsSvnPath(String srsSvnPath) {
		this.srsSvnPath = srsSvnPath;
	}

	public String getSrsSvnPath() {
		return srsSvnPath;
	}

	public Long[] getIssueStatusCos() {
		return issueStatusCos;
	}

	public void setIssueStatusCos(Long[] issueStatusCos) {
		this.issueStatusCos = issueStatusCos;
	}

	public Long[] getPlanStatusCos() {
		return planStatusCos;
	}

	public void setPlanStatusCos(Long[] planStatusCos) {
		this.planStatusCos = planStatusCos;
	}

	public void setIssueTypeName(String issueTypeName) {
		this.issueTypeName = issueTypeName;
	}

	public String getIssueTypeName() {
		if (!StringUtil.isNull(issueType)) {
			issueTypeName = PrimaryDataFactory.issueTypeMap.get(issueType);
		}
		return issueTypeName;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public Boolean getIsMust() {
		return isMust;
	}

	public void setIsMust(Boolean isMust) {
		this.isMust = isMust;
	}
}
