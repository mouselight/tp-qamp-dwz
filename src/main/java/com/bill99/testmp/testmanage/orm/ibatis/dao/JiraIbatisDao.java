package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;
import com.bill99.testmp.testmanage.orm.entity.TestReport;

public interface JiraIbatisDao {

	public List<Map> queryIssue();

	public List<Map> queryAllCycle();

	public List<Map> queryIssueCycle();

	public List<Map> queryIssueFiledValue();

	public Map<String, String> queryCycleByIssue(Long issueId);

	public List<TestReport> getBugList(Long issueId);

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds);

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId);

	public List<TestNotifyBugDto> getNotifyIssueBugs(Long issueId);

	public List<TestNotifyBugDto> getNotifyBugs(Long teemId, List<Long> notifiedBugIds);

	public List<TestNotifyBugDto> getNotifyIssues(Long teemId, List<Long> notifiedBugIds);

}
