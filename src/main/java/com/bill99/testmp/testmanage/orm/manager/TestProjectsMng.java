package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public interface TestProjectsMng {

	public TestProjects getById(Long testProjectId);

	public TestProjects save(TestProjects testProjects);

	public TestProjects update(TestProjects testProjects);

	public List<TestProjects> findALl();

	public List<TestProjects> findValidList();

	public List<TestProjects> findByIds(String ids);

	public TestProjects findByTeemId(Long teemId);

}
