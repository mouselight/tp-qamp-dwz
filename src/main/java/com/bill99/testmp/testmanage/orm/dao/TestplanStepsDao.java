package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;

public interface TestplanStepsDao extends BaseDao<TestplanSteps, Long> {

	public List<TestplanSteps> getByPlan(Long planId);

	public List<Long> getTcId(Long stepId);

	public List<Long> getTcId(Long stepId, Integer executionType, Integer exeStatus);

	public List<TestplanSteps> getUnitStepsByStepId(Long stepId);
}
