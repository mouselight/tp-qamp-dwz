package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.NotifyIssueLogDao;
import com.bill99.testmp.testmanage.orm.entity.NotifyIssueLog;
import com.bill99.testmp.testmanage.orm.manager.NotifyIssueLogMng;

public class NotifyIssueLogMngImpl implements NotifyIssueLogMng {

	private NotifyIssueLogDao notifyIssueLogDao;

	public void setNotifyIssueLogDao(NotifyIssueLogDao notifyIssueLogDao) {
		this.notifyIssueLogDao = notifyIssueLogDao;
	}

	public List<Long> getNeedNotifyIssues(List<Long> issueIds, Integer issueType) {
		issueIds.removeAll(notifyIssueLogDao.getNeedNotifyIssues(issueIds, issueType));
		return issueIds;
	}

	public List<Long> getNotifiedIssues(Long teemId, Integer issueType) {
		return notifyIssueLogDao.getNotifiedIssues(teemId, issueType);
	}

	public void insertNotifiedIssues(NotifyIssueLog notifyIssueLog) {
		notifyIssueLogDao.save(notifyIssueLog);
	}
}
