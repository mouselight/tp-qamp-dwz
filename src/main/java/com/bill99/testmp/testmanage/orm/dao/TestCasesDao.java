package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.common.dto.ExcelImportDto;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public interface TestCasesDao extends BaseDao<TestCases, Long> {

	public void importBatch(boolean isNew, ExcelImportDto excelImportDto);

	public int bath4Update(List<TestCases> listTestCases);

	public int bath4Del(List<TestCases> listTestCases);

	public List<TestCases> queryAll();

	public List<TestCases> queryByProjectId(Long testprojectId);
	
	public List<TestCases> getPlanTrackCasesDao(Long planTrackId);
	

	
	
	

}
