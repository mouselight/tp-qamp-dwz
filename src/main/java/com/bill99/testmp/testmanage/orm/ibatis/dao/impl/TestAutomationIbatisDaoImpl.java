package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.TaMethodDto;
import com.bill99.testmp.testmanage.common.dto.TaProjectDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.common.dto.TestTaTaskLogDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestAutomationIbatisDao;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class TestAutomationIbatisDaoImpl extends QueryDaoSupport implements TestAutomationIbatisDao {

	public List<TaMethodDto> getTaMethods(TaMethodDto taMethodDto) {
		return queryForList("TestAutomation.getTaMethods", taMethodDto);
	}

	public TaMethodDto queryTaMethod(Long idSvnMethod) {
		return queryForObject("TestAutomation.queryTaMethod", idSvnMethod);
	}

	public Object queryTcByMethod(Long idSvnMethod) {
		return queryForObject("TestAutomation.queryTcByMethod", idSvnMethod);
	}

	public void saveAssoTaTc(Map<String, Long> condMap) {
		getSqlMapClientTemplate().insert("TestAutomation.saveAssoTaTc", condMap);
	}

	public void removeAssoTaTc(Map<String, Object> condMap) {
		getSqlMapClientTemplate().delete("TestAutomation.removeAssoTaTc", condMap);
		getSqlMapClientTemplate().update("TestAutomation.removeAssoTaTc4Update", null);
	}

	public List<TaProjectDto> getTaProject(Long testProjectId) {
		return queryForList("TestAutomation.getTaProject", testProjectId);
	}

	public void saveTaProject(TaProjectDto taProjectDto) {
		getSqlMapClientTemplate().insert("TestAutomation.saveTaProject", taProjectDto);

	}

	public TaProjectDto getTaProjectById(Long idTaProject) {
		return queryForObject("TestAutomation.getTaProjectById", idTaProject);
	}

	public void updateTaProject(TaProjectDto taProjectDto) {
		getSqlMapClientTemplate().update("TestAutomation.updateTaProject", taProjectDto);

	}

	public void deleteTaProject(TaProjectDto taProjectDto) {
		getSqlMapClientTemplate().delete("TestAutomation.deleteTaProject", taProjectDto);

	}

	public List<Long> getTcIds(Long idSvnMethod) {
		return queryForList("TestAutomation.getTcIds", idSvnMethod);
	}

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(Map paraMap) {
		return queryForList("TestAutomation.getTaTaskMethodResult", paraMap);
	}

	public List<TaResultDto> getTaResult(Map paraMap) {
		return queryForList("TestAutomation.getTaResult", paraMap);
	}

	public List<Long> getTestPlanTcs(Long planStepId) {
		return queryForList("TestAutomation.getTestPlanTcs", planStepId);
	}

	public List<TestTaTaskLogDto> getTaTaskLogById(Long idTestTaTask) {
		return queryForList("TestAutomation.getTaTaskLogById", idTestTaTask);
	}

	public List<TestTaTaskLogDto> getTaTaskLogByPlanStepId(Long planStepId) {
		return queryForList("TestAutomation.getTaTaskLogByPlanStepId", planStepId);
	}

	public List<TaMethodDto> queryTaMethodByTcId(Long idTestCase) {
		return queryForList("TestAutomation.queryTaMethodByTcId", idTestCase);
	}

	public List<TestTaTaskLogDto> getTaTaskLog() {
		return queryForList("TestAutomation.getTaTaskLog", null);
	}

	public List<Long> getStepTaFailTcs(Long idPlanStep) {
		return queryForList("TestAutomation.getStepTaFailTcs", idPlanStep);
	}

	public void insertTestTaTaskTcs4Batch(final Long idTestTaTask, final Long[] idTestCases) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				if (idTestCases != null) {
					Map paramMap = new HashMap();
					paramMap.put("idTestTaTask", idTestTaTask);
					for (Long idTestCase : idTestCases) {
						paramMap.put("idTestCase", idTestCase);
						executor.insert("TestAutomation.insertTestTaTaskTcs4Batch", paramMap);
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

}
