package com.bill99.testmp.testmanage.orm.entity;

import java.util.Date;

import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.testmp.testmanage.common.utils.StateUtils;

public class TestCaseTree {

	private Long tcId;
	private String name;
	private Integer status;
	private String updateUser;
	private Date updateTime;

	private String statusName;
	private String updateTimeStr;

	private String createUser;
	private Date createDate;
	private String createDateStr;
	private int parentId;

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public Long getTcId() {
		return tcId;
	}

	public void setTcId(Long tcId) {
		this.tcId = tcId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusName() {
		if (null != status) {
			statusName = StateUtils.getTcStatusMap().get(status);
		}
		return statusName;
	}

	public void setUpdateTimeStr(String updateTimeStr) {
		this.updateTimeStr = updateTimeStr;
	}

	public String getUpdateTimeStr() {
		if (null != updateTime) {
			updateTimeStr = DateUtil.parseFromDate(updateTime);
		}
		return updateTimeStr;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateDateStr() {
		if (null != createDate) {
			createDateStr = DateUtil.parseFromDate(createDate);
		}
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

}
