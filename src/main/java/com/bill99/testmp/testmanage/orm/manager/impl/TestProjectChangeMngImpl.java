package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TestProjectChangeDao;
import com.bill99.testmp.testmanage.orm.entity.TestProjectChange;
import com.bill99.testmp.testmanage.orm.manager.TestProjectChangeMng;

public class TestProjectChangeMngImpl implements TestProjectChangeMng {

	private TestProjectChangeDao testProjectChangeDao;

	public void setTestProjectChangeDao(TestProjectChangeDao testProjectChangeDao) {
		this.testProjectChangeDao = testProjectChangeDao;
	}

	public List<TestProjectChange> getByIssueId(Long issueid) {
		return testProjectChangeDao.findByIssueId(issueid);
	}

	public TestProjectChange getByChangeId(Long changeid) {
		return testProjectChangeDao.findByChangeId(changeid);
	}

	public TestProjectChange save(TestProjectChange testProjectChange) {
		return testProjectChangeDao.save(testProjectChange);
	}

	public TestProjectChange update(TestProjectChange testProjectChange) {
		return testProjectChangeDao.update(testProjectChange);
	}

	public void delete(Long changeId) {
		testProjectChangeDao.delete(changeId);
	}
}
