package com.bill99.testmp.testmanage.orm.manager;

import com.bill99.testmp.testmanage.orm.entity.TestReportLog;

public interface TestReportLogMng {

	public void save(TestReportLog testReportLog);
}
