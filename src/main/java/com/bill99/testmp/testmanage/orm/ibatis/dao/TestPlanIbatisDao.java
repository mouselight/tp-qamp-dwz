package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.entity.Testplans;

public interface TestPlanIbatisDao {

	public List<Testplans> query(Testplans testPlan);

	public void mergeStep(TestplanSteps testplanStep);

	public List<Long> getPrivyPlanId(Long userId, Long teamId);

	public Integer queryCount(Testplans testplan);

	public TestplanSteps getTcCountByStep(Long stepId);

	public List<TestplanSteps> getStepInfoByPlan(Long planId);

	public List<Long> getStepAllChilds(List<Long> tcIds, Long stepId);

	public void saveAssoStepUser(Long stepId, List<Long> userIds);

	public void saveAssoStepTc(Long stepId, List<Long> tcIds, String userName);

	public void removeAssoStepTc(Long stepId, List<Long> removeTcIds);

	public void finishAssoTc(Long stepId, List<Long> tcIds, List<Long> parentIds, Integer state, String updateUser);

	public void updateSvnUrl(Long stepId, String svnUrl);

	public List<Long> getFullAssoParentIds(List<Long> parentIds, Long stepId);

	public void saveTcByCloud(Long stepId, List<Long> cloudId);

	public void insertMustPlan(final List<Map> issue);

	public List<Map> findMustPlan();

}
