package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;

public interface MonitorManager {

	public String getRmmonitorBPByTypeId(List<TaReportChartDto> taChartDto) throws Exception;
	
}
