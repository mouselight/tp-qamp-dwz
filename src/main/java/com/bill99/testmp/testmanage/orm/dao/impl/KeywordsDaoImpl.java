package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.KeywordsDao;
import com.bill99.testmp.testmanage.orm.entity.Keywords;

public class KeywordsDaoImpl extends BaseDaoSupport<Keywords, Long> implements KeywordsDao {

	private final String getByTestProjectIdHql = "from Keywords where testProjectId = ?";

	public List<Keywords> getByTestProjectId(Long testProjectId) {
		return find(getByTestProjectIdHql, testProjectId);
	}

}
