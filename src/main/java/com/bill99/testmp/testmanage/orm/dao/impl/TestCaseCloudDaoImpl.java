package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.testmp.testmanage.orm.dao.TestCaseCloudDao;
import com.bill99.testmp.testmanage.orm.entity.TcCloud;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;
import com.jeecms.common.hibernate3.BaseDaoImpl;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;

public class TestCaseCloudDaoImpl extends BaseDaoImpl<TcCloud> implements TestCaseCloudDao {

	public Pagination query(TestCaseCloudCommand cmd) {
		Finder f = Finder.create("from TcCloud where teamId = :teamId");
		f.setParam("teamId", cmd.getTeamId());
		return find(f, cmd.getPageNum(), cmd.getNumPerPage());
	}

	private static final String GET_TCCNT_HQL = "from AssoTcCloud t,TestCases tc where tc.testCaseId=t.assoPk.tcId and tc.status=1 and t.assoPk.tcCloudId = :cloudId and t.nodeTypeId = 3";

	public Integer getTcCount(Long cloudId) {
		return (Integer) getSession().createQuery(GET_TCCNT_HQL).setLong("cloudId", cloudId).list().size();
	}
}
