package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TcExeLogDetail implements Serializable {

	private static final long serialVersionUID = -7568798498009491021L;

	private Long idTcExeLogDetail;

	private Long testPlanId;
	private Long planStepId;
	private Long testProjectId;
	private Integer exeType;
	private Integer optCnt;
	private Integer state;
	private String createUser;
	private Date createDate;
	private String testCaseName;

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public Long getIdTcExeLogDetail() {
		return idTcExeLogDetail;
	}

	public void setIdTcExeLogDetail(Long idTcExeLogDetail) {
		this.idTcExeLogDetail = idTcExeLogDetail;
	}

	public Long getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(Long testPlanId) {
		this.testPlanId = testPlanId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Integer getExeType() {
		return exeType;
	}

	public void setExeType(Integer exeType) {
		this.exeType = exeType;
	}

	public Integer getOptCnt() {
		return optCnt;
	}

	public void setOptCnt(Integer optCnt) {
		this.optCnt = optCnt;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
