package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaSqaReportDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraSqaReportIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.JiraSqaReportMng;

public class JiraSqaReportMngImpl implements JiraSqaReportMng {
	
	private JiraSqaReportIbatisDao jiraSqaReportIbatisDao;

	public void setJiraSqaReportIbatisDao(JiraSqaReportIbatisDao jiraSqaReportIbatisDao)
			 {
		this.jiraSqaReportIbatisDao = jiraSqaReportIbatisDao;
	}

	public List<TaSqaReportDto> getProjectName(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getProjectName(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelDev(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugLevelDev(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelStage02(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugLevelStage02(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelPro(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugLevelPro(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryDev(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugcategoryDev(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryStage02(
			TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugcategoryStage02(taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryPro(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return jiraSqaReportIbatisDao.getBugcategoryPro(taSqaReportDto);
	}
	
	public List<TaSqaReportDto> getBugstatus(TaSqaReportDto taSqaReportDto){
		
		return jiraSqaReportIbatisDao.getBugstatus(taSqaReportDto);
	}
	
	public List<TaSqaReportDto> getEnvBug(TaSqaReportDto taSqaReportDto){
		return jiraSqaReportIbatisDao.getEnvBug(taSqaReportDto);
	}

}
