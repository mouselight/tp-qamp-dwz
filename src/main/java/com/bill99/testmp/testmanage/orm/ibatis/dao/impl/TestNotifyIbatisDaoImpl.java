package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.TestNotifyIssuePlanDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestNotifyIbatisDao;

public class TestNotifyIbatisDaoImpl extends QueryDaoSupport implements TestNotifyIbatisDao {

	public List<TestNotifyIssuePlanDto> getNotifyIssuePlan(String cycleId, Long teamId) {
		Map map = new HashMap();
		map.put("cycleId", cycleId);
		map.put("teamId", teamId);
		return queryForList("testNotify.getNotifyIssuePlan", map);
	}

}
