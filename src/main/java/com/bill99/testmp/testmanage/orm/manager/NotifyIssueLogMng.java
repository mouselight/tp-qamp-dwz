package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.NotifyIssueLog;

public interface NotifyIssueLogMng {

	public List<Long> getNeedNotifyIssues(List<Long> issueIds, Integer issueType);

	public List<Long> getNotifiedIssues(Long teemId, Integer issueType);

	public void insertNotifiedIssues(NotifyIssueLog notifyIssueLog);
}
