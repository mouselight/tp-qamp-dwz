package com.bill99.testmp.testmanage.orm.manager.impl;

import com.bill99.testmp.testmanage.orm.dao.TestPlanAssUserDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTestplanStepUser;
import com.bill99.testmp.testmanage.orm.manager.TestPlanAssUserMng;

public class TestPlanAssUserMngImpl implements TestPlanAssUserMng {

	private TestPlanAssUserDao testPlanAssUserDao;

	public AssoTestplanStepUser save(AssoTestplanStepUser assoTestplanStepUser) {
		return testPlanAssUserDao.save(assoTestplanStepUser);
	}

	public void setTestPlanAssUserDao(TestPlanAssUserDao testPlanAssUserDao) {
		this.testPlanAssUserDao = testPlanAssUserDao;
	}
}
