package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.NotifyIssueLogDao;
import com.bill99.testmp.testmanage.orm.entity.NotifyIssueLog;

public class NotifyIssueLogDaoImpl extends BaseDaoSupport<NotifyIssueLog, Long> implements NotifyIssueLogDao {

	private final String getNeedNotifyIssuesHql = "from NotifyIssueLog where issueId in (?) and issueType=?";

	public List<Long> getNeedNotifyIssues(List<Long> issueIds, Integer issueType) {
		return find(getNeedNotifyIssuesHql, issueIds, issueType);
	}

	private final String getNotifiedIssuesSql = "select issue_id from notify_issue_log where teem_Id= ? and issue_type=? and  create_date >= NOW() - INTERVAL 10 minute";

	public List<Long> getNotifiedIssues(Long teemId, Integer issueType) {
		return executeSql(getNotifiedIssuesSql, teemId, issueType);
	}
}
