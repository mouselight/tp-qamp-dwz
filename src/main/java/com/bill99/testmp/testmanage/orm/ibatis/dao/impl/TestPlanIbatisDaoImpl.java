package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanIbatisDao;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class TestPlanIbatisDaoImpl extends QueryDaoSupport implements TestPlanIbatisDao {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public List<Testplans> query(Testplans testPlan) {
		return queryForList("testPlan.queryPlan", testPlan);
	}

	public Integer queryCount(Testplans testplan) {
		return queryForObject("testPlan.queryPlanCount", testplan);
	}

	public void saveAssoStepTc(Long stepId, List<Long> tcIds, String userName) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("stepId", stepId);
		condMap.put("tcIds", tcIds);
		condMap.put("userName", userName);
		getSqlMapClientTemplate().insert("testPlan.assoStepTc", condMap);
	}

	public void finishAssoTc(Long stepId, List<Long> tcIds, List<Long> parentIds, Integer state, String updateUser) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		if (null != parentIds && parentIds.size() > 0) {
			tcIds.addAll(parentIds);
		}
		condMap.put("stepId", stepId);
		condMap.put("tcIds", tcIds);
		condMap.put("state", state);
		condMap.put("updateUser", updateUser);
		getSqlMapClientTemplate().update("testPlan.finishAssoTc", condMap);
	}

	public void removeAssoStepTc(Long stepId, List<Long> removeTcIds) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("stepId", stepId);
		condMap.put("tcIds", removeTcIds);
		getSqlMapClientTemplate().delete("testPlan.removeAsso", condMap);
	}

	public TestplanSteps getTcCountByStep(Long stepId) {
		return (TestplanSteps) getSqlMapClientTemplate().queryForObject("testPlan.getTcCountByStep", stepId);
	}

	public List<Long> getPrivyPlanId(Long userId, Long teamId) {
		Map<String, Long> condMap = new HashMap<String, Long>();
		condMap.put("userId", userId);
		condMap.put("teamId", teamId);
		return getSqlMapClientTemplate().queryForList("testPlan.getPrivyPlanId", condMap);
	}

	public void mergeStep(TestplanSteps testplanStep) {
		getSqlMapClientTemplate().insert("testPlan.mergePlanStep", testplanStep);
	}

	public void saveAssoStepUser(Long stepId, List<Long> userIds) {
		Map<String, Long> condMap = new HashMap<String, Long>();
		condMap.put("stepId", stepId);
		for (Long userId : userIds) {
			condMap.put("userId", userId);
			getSqlMapClientTemplate().insert("testPlan.assoStepUser", condMap);
		}
	}

	public List<TestplanSteps> getStepInfoByPlan(Long planId) {
		return (List<TestplanSteps>) getSqlMapClientTemplate().queryForList("testPlan.getStepInfoByPlan", planId);
	}

	public List<Long> getStepAllChilds(List<Long> tcIds, Long stepId) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("tcIds", tcIds);
		condMap.put("stepId", stepId);
		return queryForList("testPlan.getStepAllChilds", condMap);
	}

	public void updateSvnUrl(Long stepId, String svnUrl) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("stepId", stepId);
		condMap.put("svnUrl", svnUrl);
		getSqlMapClientTemplate().update("testPlan.updateSvnUrl", condMap);
	}

	public List<Long> getFullAssoParentIds(List<Long> parentIds, Long stepId) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("parentIds", parentIds);
		condMap.put("stepId", stepId);
		return getSqlMapClientTemplate().queryForList("testPlan.getFullAssoParentIds", condMap);
	}

	public void saveTcByCloud(Long stepId, List<Long> cloudIds) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("stepId", stepId);
		condMap.put("cloudIds", cloudIds);
		getSqlMapClientTemplate().insert("testPlan.saveTcByCloud", condMap);
	}

	public void insertMustPlan(final List<Map> issue) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issue.size= " + issue.size());
				if (issue != null && issue.size() > 0) {
					for (int i = 0; i < issue.size(); i++) {
						executor.insert("testPlan.insertMustPlan", issue.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public List<Map> findMustPlan() {
		return queryForList("testPlan.findMustPlan", null);
	}
}
