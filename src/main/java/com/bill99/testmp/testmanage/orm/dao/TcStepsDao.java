package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;

public interface TcStepsDao extends BaseDao<TcSteps, Long> {

	public int batch4ReBuildTcSteps(Long testCaseId, List<TcSteps> tcStepList);

	public List<TcSteps> queryAll();

	public int batch4Update(List<TcSteps> tcStepList);

}
