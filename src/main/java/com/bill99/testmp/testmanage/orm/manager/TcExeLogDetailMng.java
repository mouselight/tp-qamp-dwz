package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TcExeLogDetail;

public interface TcExeLogDetailMng {
	public void saveExeLogDetail(TcExeLogDetail tcExeLogDetail);
	public List<TcExeLogDetail> findByPlanStepId(Long planStepId);
}
