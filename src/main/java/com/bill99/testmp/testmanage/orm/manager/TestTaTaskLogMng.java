package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;

public interface TestTaTaskLogMng {

	public TestTaTaskLog save(TestTaTaskLog testTaTaskLog);

	public TestTaTaskLog update(TestTaTaskLog testTaTaskLog);

	public List<TestTaTaskLog> getByPlanId(Long planId);

	public List<TestTaTaskLog> getByPlanStepId(Long planStepId);

	public TestTaTaskLog getLastTask(Long planStepId);

	public TestTaTaskLog get(Long idTestTaTask);

}
