package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReportChart implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2451345107448353947L;
	private ReportChartPk reportChartPk;
	private long task_num;   //上线项目数
	private long total_casenum;  //所有环境case总数
	private long ar_casenum;  //所有环境人工执行case总数
	private long auto_casenumstage;//02环境自动化执行成功case总数
	private long total_casenumstage;//02环境所有case总数
	private Date cycledate;//cycle时间
	private Date crt_date;
	private Date update_date;
	private BigDecimal rate;//02环境自动化执行成功占02环境case总数比例
	
	
	
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public ReportChartPk getReportChartPk() {
		return reportChartPk;
	}
	public void setReportChartPk(ReportChartPk reportChartPk) {
		this.reportChartPk = reportChartPk;
	}
	public long getTask_num() {
		return task_num;
	}
	public void setTask_num(long task_num) {
		this.task_num = task_num;
	}
	public long getTotal_casenum() {
		return total_casenum;
	}
	public void setTotal_casenum(long total_casenum) {
		this.total_casenum = total_casenum;
	}
	public long getAr_casenum() {
		return ar_casenum;
	}
	public void setAr_casenum(long ar_casenum) {
		this.ar_casenum = ar_casenum;
	}
	public long getAuto_casenumstage() {
		return auto_casenumstage;
	}
	public void setAuto_casenumstage(long auto_casenumstage) {
		this.auto_casenumstage = auto_casenumstage;
	}
	public long getTotal_casenumstage() {
		return total_casenumstage;
	}
	public void setTotal_casenumstage(long total_casenumstage) {
		this.total_casenumstage = total_casenumstage;
	}
	
	public Date getCycledate() {
		return cycledate;
	}
	public void setCycledate(Date cycledate) {
		this.cycledate = cycledate;
	}
	public Date getCrt_date() {
		return crt_date;
	}
	public void setCrt_date(Date crt_date) {
		this.crt_date = crt_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	

	

}
