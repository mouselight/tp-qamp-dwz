package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.jeecms.common.hibernate3.BaseDao;
import com.jeecms.common.page.Pagination;

public interface TestIssueDao extends BaseDao<TestIssue> {

	Pagination query(TestIssue testIssue, Integer pageNo, Integer pageSize);
	
}
