package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public interface TestProjectsDao extends BaseDao<TestProjects, Long> {

	public List<TestProjects> findALl();

	public List<TestProjects> findValidList();

	public List<TestProjects> findByIds(String ids);

	public TestProjects findByTeemId(Long teemId);

}
