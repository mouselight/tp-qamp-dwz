package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.orm.entity.TcCloud;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;
import com.jeecms.common.page.Pagination;

public interface TestCaseCloudMng {

	public TcCloud findById(Long id);

	public Pagination query(TestCaseCloudCommand cmd);

	public void delete(Long id);

	public Long saveOrUpdate(TestCaseCloudCommand cmd);

	public void saveAsso(Long id, List<Long> tcIds);

	public void deleteAsso(Long id, List<Long> tcIds);

	public Map<String, String> getCloudNameMap(Long teamId);

	public void update(TestCaseCloudCommand cmd);
}
