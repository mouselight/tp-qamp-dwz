package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.NodesImportDto;
import com.bill99.testmp.testmanage.common.dto.TestStepProgressDto;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestReportIbatisDao;

public class TestReportIbatisDaoImpl extends QueryDaoSupport implements TestReportIbatisDao {

	public List<TestReport> getCaseStatis(Long stepId, Long groupId) {
		Map map = new HashMap();
		map.put("stepId", stepId);
		map.put("groupId", groupId);
		return queryForList("testReport.getCaseStatis", map);
	}

	public List<TestCaseTree> getCaseList(Long stepId, Long groupId) {
		Map map = new HashMap();
		map.put("stepId", stepId);
		map.put("groupId", groupId);
		return queryForList("testReport.getCaseList", map);
	}
	public String getTeamIdByStepId(String issueId) {
		return(String)queryForObject("testReport.getTeamIdByStepId", issueId);
	}
	
	public List<TestCaseTree> getMustCaseList(String teamId) {
		return  queryForList("testReport.getMustCaseList", teamId);
	}

	public TestCaseTree getAllParentTc(Long tcId) {
		return queryForObject("testReport.getParentTc", tcId);
	}

	public TestReport getIssue(Long issueId) {
		return queryForObject("testReport.queryIssueReport", issueId);
	}

	public List<TestReport> getQaInfo(Long stepId) {
		return queryForList("testReport.queryQaInfo", stepId);
	}

	public NodesImportDto getParentId(Long tcId) {
		return queryForObject("testReport.getParentId", tcId);

	}

	public List<TestStepProgressDto> getTestStepProgress(Long stepId) {
		return queryForList("testReport.getTestStepProgress", stepId);
	}

	public String isMustByPlanStep(Long planStep) {  
		return (String)queryForObject("testReport.isMustByPlanStep", planStep);
	}

	public List<TestCaseTree> getMustCaseList(Long stepId) {

		return queryForList("testReport.getMustCaseList", stepId);
	}




}
