package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TestProjectChangeDao;
import com.bill99.testmp.testmanage.orm.entity.TestProjectChange;

public class TestProjectChangeDaoImpl extends BaseDaoSupport<TestProjectChange, Long> implements TestProjectChangeDao {

	private final String findByIssueIdHql = "from TestProjectChange where issueid=?  order by  changeid asc";

	public List<TestProjectChange> findByIssueId(Long issueid) {
		return find(findByIssueIdHql, issueid);
	}

	private final String findByChangeIdHql = "from TestProjectChange where changeid=?";

	public TestProjectChange findByChangeId(Long changeid) {
		return findUnique(findByChangeIdHql, changeid);
	}

}
