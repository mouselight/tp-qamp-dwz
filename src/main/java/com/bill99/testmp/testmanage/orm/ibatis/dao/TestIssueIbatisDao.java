package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;

public interface TestIssueIbatisDao {

	public void insertIssue(List<Map> issue);

	public void insertIssueFiledValue(List<Map> issueField);

	public TestIssue getIssueDetail(Long issueId);

	public List<TestIssue> queryIssue(TestIssue queryModel);

	public Integer queryIssueCount(TestIssue queryModel);

	public List<User> queryUser(String realname, List<Long> teamIdList);

	public void insertCycle(List<Map> cycle);

	public void updateIssueCycle(List<Map> issueCycle);

	public Map<String, String> queryCycle(Long teamId);

	public List<Long> querytemIdByUserId(List userIdList);

	public List<Map> findMustIssue();

	public void insertMustIssue(List<Map> issue);
	
	public void insertMustCycle(Long cycleId);

}
