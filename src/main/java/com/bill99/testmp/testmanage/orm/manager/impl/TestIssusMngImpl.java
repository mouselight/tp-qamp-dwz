package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.orm.dao.TestIssueDao;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestIssueIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.jeecms.common.page.Pagination;

public class TestIssusMngImpl implements TestIssueMng {

	private TestIssueDao testIssueDao;
	private TestIssueIbatisDao testIssueIbatisDao;

	public void setTestIssueDao(TestIssueDao testIssueDao) {
		this.testIssueDao = testIssueDao;
	}

	public void setTestIssueIbatisDao(TestIssueIbatisDao testIssueIbatisDao) {
		this.testIssueIbatisDao = testIssueIbatisDao;
	}

	public List<TestIssue> query(TestIssue testIssue) {
		return testIssueIbatisDao.queryIssue(testIssue);
	}

	public Pagination query(TestIssue testIssue, Integer pageNo, Integer pageSize) {
		Pagination pagination = new Pagination();

		testIssue.setStartRow((pageNo - 1) * pageSize + 1);
		testIssue.setEndRow(pageNo * pageSize);

		Integer totalCount = testIssueIbatisDao.queryIssueCount(testIssue);

		if (null != totalCount && totalCount.intValue() > 0) {
			List<TestIssue> list = testIssueIbatisDao.queryIssue(testIssue);
			pagination.setList(list);
			pagination.setTotalCount(totalCount);
			pagination.setPageNo(pageNo);
			pagination.setPageSize(pageSize);
		}
		return pagination;
	}

	public TestIssue findById(Long id) {
		Assert.notNull(id);
		return testIssueDao.get(id);
	}

	public TestIssue getIssueDetail(Long issueId) {
		Assert.notNull(issueId);
		return testIssueIbatisDao.getIssueDetail(issueId);
	}

	public List<User> queryUser(String realname, List<Long> teamIdList) {
		return testIssueIbatisDao.queryUser(realname, teamIdList);
	}

	public Integer queryCount(TestIssue testIssue) {
		return testIssueIbatisDao.queryIssueCount(testIssue);
	}

	public Map<String, String> queryCycle(Long teamId) {
		return testIssueIbatisDao.queryCycle(teamId);
	}

	public List<Long> querytemIdByUserId(List userIdList) {
		return testIssueIbatisDao.querytemIdByUserId(userIdList);
	}
}
