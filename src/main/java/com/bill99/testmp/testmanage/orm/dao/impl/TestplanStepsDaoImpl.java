package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TestplanStepsDao;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;

public class TestplanStepsDaoImpl extends BaseDaoSupport<TestplanSteps, Long> implements TestplanStepsDao {

	private final String FIND_BYPLAN_HQL = "from TestplanSteps where planId =:planId and (state!=99 or state is null) and parentId is null";
	private final String FIND_TC_ID = "select t.stepTcPk.tcId from AssoPlanStepTc t where t.stepTcPk.stepId = :stepId and t.status != 0";
	private final String FIND_TC_ID_EXECUTION = "select t.stepTcPk.tcId from AssoPlanStepTc t where t.stepTcPk.stepId = :stepId and t.status != :exeStatus and exists (select 0 from TestCases l where l.testCaseId = t.stepTcPk.tcId and l.executionType = :executionType)";

	public List<TestplanSteps> getByPlan(Long planId) {
		return (List<TestplanSteps>) getSession().createQuery(FIND_BYPLAN_HQL).setLong("planId", planId).list();
	}

	public List<Long> getTcId(Long stepId) {
		return (List<Long>) getSession().createQuery(FIND_TC_ID).setLong("stepId", stepId).list();
	}

	public List<Long> getTcId(Long stepId, Integer executionType, Integer exeStatus) {
		return (List<Long>) getSession().createQuery(FIND_TC_ID_EXECUTION).setLong("stepId", stepId).setInteger("exeStatus", exeStatus).setInteger("executionType", executionType)
				.list();
	}

	private final String getUnitStepsByStepIdHql = "from TestplanSteps where parentId = ? order by unitType asc";

	public List<TestplanSteps> getUnitStepsByStepId(Long stepId) {
		return find(getUnitStepsByStepIdHql, stepId);
	}
}