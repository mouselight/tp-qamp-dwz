package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.SysSwitch;

public interface SysSwitchDao extends BaseDao<SysSwitch, Long> {

	public SysSwitch getSysSwitch(Long type);

}
