package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TcExeLogDetail;

public interface TcExeLogDetailDao extends BaseDao<TcExeLogDetail, Long> {

}
