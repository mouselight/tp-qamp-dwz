package com.bill99.testmp.testmanage.orm.ibatis.dao;



import java.util.Date;
import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;
import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.common.dto.TestCycleDto;

public interface TestAutolistOnlineReportIbatisDao {
	
	public List<TaReportDto> getTaonlineNum(TaReportDto taReportDto);
	
	public List<TestCycleDto> getCycleList();
    
	public List<TaReportDto>  getCaseTotalnum(TaReportDto taReportDto);
	
	public List<TaReportDto>  getStage02CaseTotalnum(TaReportDto taReportDto);
	public List<TaReportDto> getArtficialCaseTotalnum(TaReportDto taReportDto);
	public List<TaReportDto> getAutoCaseTotalnum(TaReportDto taReportDto);
	public Date getMaxCycleDate();
	public List<TaReportDto> getStage02AutoSuccessCasenum(TaReportDto taReportDto);
	public List<TaReportDto> getIssueNum(TaReportDto taReportDto);
	public List<TaReportChartDto> getChartData(TaReportChartDto taReportChartDto);
	
	public List<TaReportDto> getAllproject();
	

}
