package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class Keywords implements Serializable {

	private static final long serialVersionUID = 2855817420703382686L;

	private Long id;
	private String keyword;
	private Long testProjectId;
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

}
