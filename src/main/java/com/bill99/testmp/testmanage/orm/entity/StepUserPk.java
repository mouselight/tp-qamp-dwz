package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class StepUserPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5200036189658946467L;

	private Long testplanStepId;
	private Long userId;

	public Long getTestplanStepId() {
		return testplanStepId;
	}

	public void setTestplanStepId(Long testplanStepId) {
		this.testplanStepId = testplanStepId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((testplanStepId == null) ? 0 : testplanStepId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StepUserPk other = (StepUserPk) obj;
		if (testplanStepId == null) {
			if (other.testplanStepId != null)
				return false;
		} else if (!testplanStepId.equals(other.testplanStepId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
}
