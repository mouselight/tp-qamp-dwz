package com.bill99.testmp.testmanage.orm.manager.impl;

import com.bill99.testmp.testmanage.orm.dao.TestReportLogDao;
import com.bill99.testmp.testmanage.orm.entity.TestReportLog;
import com.bill99.testmp.testmanage.orm.manager.TestReportLogMng;

public class TestReportLogMngImpl implements TestReportLogMng {

	private TestReportLogDao testReportLogDao;

	public void setTestReportLogDao(TestReportLogDao testReportLogDao) {
		this.testReportLogDao = testReportLogDao;
	}

	public void save(TestReportLog testReportLog) {
		testReportLogDao.save(testReportLog);
	}
}
