package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.bill99.testmp.testmanage.orm.dao.TestCaseCloudDao;
import com.bill99.testmp.testmanage.orm.entity.TcCloud;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestCaseCloudIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestCaseCloudMng;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;
import com.jeecms.common.page.Pagination;

public class TestCaseCloudMngImpl implements TestCaseCloudMng {

	private TestCaseCloudDao testCaseCloudDao;
	private TestCaseCloudIbatisDao testCaseCloudIbatisDao;

	public void setTestCaseCloudDao(TestCaseCloudDao testCaseCloudDao) {
		this.testCaseCloudDao = testCaseCloudDao;
	}

	public void setTestCaseCloudIbatisDao(TestCaseCloudIbatisDao testCaseCloudIbatisDao) {
		this.testCaseCloudIbatisDao = testCaseCloudIbatisDao;
	}

	public Pagination query(TestCaseCloudCommand cmd) {
		Assert.notNull(cmd.getTeamId());
		return testCaseCloudDao.query(cmd);
	}

	public void delete(Long id) {
		Assert.notNull(id);
		testCaseCloudDao.deleteById(id);
	}

	public TcCloud findById(Long id) {
		return testCaseCloudDao.get(id);
	}

	public Long saveOrUpdate(TestCaseCloudCommand cmd) {
		TcCloud tcCloud = new TcCloud();
		if (null != cmd.getId()) {
			//update
			tcCloud = testCaseCloudDao.get(cmd.getId());
			tcCloud.setName(cmd.getName());
			tcCloud.setUptTime(new Date());
			tcCloud.setUptUser(cmd.getUserName());
			tcCloud.setIsMust(cmd.getIsMust());
			tcCloud.setTcCnt(testCaseCloudDao.getTcCount(cmd.getId()));
			testCaseCloudDao.update(tcCloud);
			return cmd.getId();
		}

		//save
		tcCloud.setName(cmd.getName());
		tcCloud.setTeamId(cmd.getTeamId());
		tcCloud.setCrtTime(new Date());
		tcCloud.setCrtUser(cmd.getUserName());
		tcCloud.setIsMust(cmd.getIsMust());
		tcCloud = testCaseCloudDao.save(tcCloud);
		return tcCloud.getId();
	}

	public void saveAsso(Long id, List<Long> tcIds) {
		testCaseCloudIbatisDao.saveAsso(id, tcIds);
	}

	public void deleteAsso(Long id, List<Long> tcIds) {
		testCaseCloudIbatisDao.deleteAsso(id, tcIds);
	}

	public Map<String, String> getCloudNameMap(Long teamId) {
		return testCaseCloudIbatisDao.getCloudNameMap(teamId);
	}

	public void update(TestCaseCloudCommand cmd) {
		TcCloud tcCloud = testCaseCloudDao.get(cmd.getId());
		tcCloud.setUptTime(new Date());
		tcCloud.setUptUser(cmd.getUserName());
		tcCloud.setTcCnt(testCaseCloudDao.getTcCount(cmd.getId()));
		testCaseCloudDao.update(tcCloud);
	}
}
