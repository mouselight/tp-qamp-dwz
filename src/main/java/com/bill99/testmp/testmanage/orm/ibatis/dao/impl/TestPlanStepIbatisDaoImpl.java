package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.MustStepDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanStepIbatisDao;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class TestPlanStepIbatisDaoImpl extends QueryDaoSupport implements TestPlanStepIbatisDao {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public Map<Long, String> getEnvStep(Long stepId) {
		return getSqlMapClientTemplate().queryForMap("TestPlanStep.getEnvStep", stepId, "stepId", "unitType");
	}

	public void insertMustStep(final List<Map> mustStep) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issue.size= " + mustStep.size());
				if (mustStep != null && mustStep.size() > 0) {
					for (int i = 0; i < mustStep.size(); i++) {
						executor.insert("TestPlanStep.insertMustStep", mustStep.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public List<MustStepDto> findMustStep() {
		return (List<MustStepDto>) getSqlMapClientTemplate().queryForList("TestPlanStep.findMustStep");
	}

	public void insertMustTc(List<MustStepDto> mustStepList) {
		for (MustStepDto mustStepDto : mustStepList) {
			
//			System.out.println(mustStepDto.getStepId());
			getSqlMapClientTemplate().insert("TestPlanStep.insertMustTc", mustStepDto);
		}

	}

	public void copyTc(String startStepId, String endStepId) {
		String stepId = startStepId;

		Map<String, String> startStepIdMap = getSqlMapClientTemplate().queryForMap("TestPlanStep.getEnvStep", stepId, "stepId", "unitType");

		stepId = endStepId;
		Map<String, String> endStepIdMap = getSqlMapClientTemplate().queryForMap("TestPlanStep.getEnvStep", stepId, "stepId", "unitType");
		Map<String, String> copyMap = null;

		for (Map.Entry startEntry : startStepIdMap.entrySet()) {
			for (Map.Entry endEntry : endStepIdMap.entrySet()) {
				if (endEntry.getValue().equals(startEntry.getValue())) {
					copyMap = new HashMap<String, String>();
					System.out.println(startEntry.getKey().toString());
					copyMap.put("startStepId", startEntry.getKey().toString());
					copyMap.put("endStepId", endEntry.getKey().toString());
					getSqlMapClientTemplate().insert("TestPlanStep.copyTc", copyMap);

				}
			}
		}

	}

	public Long insertMustStep(Map mustStepMap) {
		return (Long) getSqlMapClientTemplate().insert("TestPlanStep.insertMustStep",mustStepMap);
	}

}
