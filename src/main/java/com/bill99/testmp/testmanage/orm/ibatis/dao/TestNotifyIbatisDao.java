package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TestNotifyIssuePlanDto;

public interface TestNotifyIbatisDao {

	public List<TestNotifyIssuePlanDto> getNotifyIssuePlan(String cycleId, Long teamId);

}
