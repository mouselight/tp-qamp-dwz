package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.Keywords;

public interface KeywordsDao extends BaseDao<Keywords, Long> {

	public List<Keywords> getByTestProjectId(Long testProjectId);

}
