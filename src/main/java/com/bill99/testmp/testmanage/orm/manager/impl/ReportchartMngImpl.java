package com.bill99.testmp.testmanage.orm.manager.impl;




import java.math.BigDecimal;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.orm.dao.TestReportChartDao;
import com.bill99.testmp.testmanage.orm.entity.ReportChart;
import com.bill99.testmp.testmanage.orm.entity.ReportChartPk;
import com.bill99.testmp.testmanage.orm.manager.ReportchartMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutolistOnlineReportMng;

public class ReportchartMngImpl implements ReportchartMng {
	
	private TestReportChartDao testReportChartDao;

	public void setTestReportChartDao(TestReportChartDao testReportChartDao) {
		this.testReportChartDao = testReportChartDao;
	}

	private TestAutolistOnlineReportMng testAutolistOnlineReportMng;

	public void setTestAutolistOnlineReportMng(
			TestAutolistOnlineReportMng testAutolistOnlineReportMng) {
		this.testAutolistOnlineReportMng = testAutolistOnlineReportMng;
	}

	public void saveReportdata() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		// Date nowDate = null;
	     Date date1 = testAutolistOnlineReportMng.getMaxCycleDate();
	     if(date1 != null){
		  String maxDate = df.format(date1.getTime());
		   System.out.println("maxDate====" + maxDate);
		
			int a = 0;
			while(true){
			/*for (int a = 0; maxDate.equals(df.format(nowDate.getTime())); a++) {*/
			   
				Calendar nowDatedto1 = Calendar.getInstance();
				nowDatedto1.set(Calendar.DAY_OF_MONTH,
						nowDatedto1.get(Calendar.DAY_OF_MONTH) - a);
			         a++;
			         System.out.println("a=" +a);
				 if(maxDate.equals(df.format(nowDatedto1.getTime()))){
				    	break;
				    }
				List<TaReportDto> listreport = this.combinData(df
						.format(nowDatedto1.getTime()));
				ParsePosition pos = new ParsePosition(0);	
				String strdate = df.format(nowDatedto1.getTime());
				Date date = df.parse(strdate, pos);
				if (listreport != null && listreport.size() > 0) {
					for (TaReportDto dto : listreport) {
						ReportChart reportChart = new ReportChart();
						ReportChartPk reportChartPk = new ReportChartPk();
						System.out.println("getid===" + dto.getId());
						reportChartPk.setPrefix_id(dto.getId());
						reportChartPk.setCycle(dto.getCycles().get(0));

						System.out.println("getCycle"
								+ reportChartPk.getCycle());
						System.out.println("getPrefix_id"
								+ reportChartPk.getPrefix_id());

						reportChart.setReportChartPk(reportChartPk);
						if (dto.getCasetotalnum() != null) {
							reportChart.setTotal_casenum(dto.getCasetotalnum());
						} else {
							reportChart.setTotal_casenum(0);
						}
						if (dto.getArcasetotalnum() != null) {
							reportChart.setAr_casenum(dto.getArcasetotalnum());
						} else {
							reportChart.setAr_casenum(0);
						}
						if (dto.getStage02autocasesuccnum() != null) {
							reportChart.setAuto_casenumstage(dto
									.getStage02autocasesuccnum());
						} else {
							reportChart.setAuto_casenumstage(0);
						}
						if (dto.getStage02casetotalnum() != null) {
							reportChart.setTotal_casenumstage(dto
									.getStage02casetotalnum());
						} else {
							reportChart.setTotal_casenumstage(0);
						}
					

						reportChart.setCycledate(date);

						reportChart.setTask_num(dto.getTcplannum());
						
						if(reportChart.getTotal_casenumstage()==0||reportChart.getAuto_casenumstage()==0){
							reportChart.setRate(new BigDecimal(0.00).setScale(2));
						}
						else{
							BigDecimal bigStage02casetotalnum = new BigDecimal(reportChart.getTotal_casenumstage());
							BigDecimal bigStage02autocasenum = new BigDecimal(reportChart.getAuto_casenumstage());
						reportChart.setRate( bigStage02autocasenum.divide(bigStage02casetotalnum, 2, BigDecimal.ROUND_HALF_UP));
						}
						System.out.println("rate===" + reportChart.getRate());
					
						testReportChartDao.save(reportChart);}
				    		
				
			}
			}
		} else {

			for (int a = 0; a <90; a++) {
				Calendar nowDatedto2 = Calendar.getInstance();
				ParsePosition pos = new ParsePosition(0);	
				nowDatedto2.set(Calendar.DAY_OF_MONTH,
						nowDatedto2.get(Calendar.DAY_OF_MONTH) - a);
				System.out.println("nowDatedto" + a + "===="
						+ df.format(nowDatedto2.getTime()));
				String strdate = df.format(nowDatedto2.getTime());
				Date date = df.parse(strdate, pos);
				List<TaReportDto> listreport = this.combinData(df
						.format(nowDatedto2.getTime()));
				System.out.println("listreport=====" + listreport.size());
				if (listreport != null && listreport.size() > 0) {
					for (TaReportDto dto : listreport) {
						ReportChart reportChart = new ReportChart();
						ReportChartPk reportChartPk = new ReportChartPk();
						System.out.println("getid===" + dto.getId());
						reportChartPk.setPrefix_id(dto.getId());
						reportChartPk.setCycle(dto.getCycles().get(0));

						System.out.println("getCycle"
								+ reportChartPk.getCycle());
						System.out.println("getPrefix_id"
								+ reportChartPk.getPrefix_id());

						reportChart.setReportChartPk(reportChartPk);
						if (dto.getCasetotalnum() != null) {
							reportChart.setTotal_casenum(dto.getCasetotalnum());
						} else {
							reportChart.setTotal_casenum(0);
						}
						if (dto.getArcasetotalnum() != null) {
							reportChart.setAr_casenum(dto.getArcasetotalnum());
						} else {
							reportChart.setAr_casenum(0);
						}
						if (dto.getStage02autocasesuccnum() != null) {
							reportChart.setAuto_casenumstage(dto
									.getStage02autocasesuccnum());
						} else {
							reportChart.setAuto_casenumstage(0);
						}
						if (dto.getStage02casetotalnum() != null) {
							reportChart.setTotal_casenumstage(dto
									.getStage02casetotalnum());
						} else {
							reportChart.setTotal_casenumstage(0);
						}

						reportChart.setCycledate(date);
						
						if(reportChart.getTotal_casenumstage()==0||reportChart.getAuto_casenumstage()==0){
							reportChart.setRate(new BigDecimal(0.00));
						}
						else{
						BigDecimal bigStage02casetotalnum = new BigDecimal(reportChart.getTotal_casenumstage());
						BigDecimal bigStage02autocasenum = new BigDecimal(reportChart.getAuto_casenumstage());
						reportChart.setRate( bigStage02autocasenum.divide(bigStage02casetotalnum, 2, BigDecimal.ROUND_HALF_UP));
						}
						System.out.println("rate===" + reportChart.getRate());
				

						reportChart.setTask_num(dto.getTcplannum());
						testReportChartDao.save(reportChart);
					}
				}
			}

		}

	}


	public List<TaReportDto> combinData(String date) {
		TaReportDto taReportDto = new TaReportDto();
		List<String> cyclelist = new ArrayList<String>();
		StringBuffer buffer = new StringBuffer();
		cyclelist.add(buffer.append("#").append(date).toString());
		System.out.println("size=" + cyclelist.size());
		System.out.println("size=" + cyclelist.get(0));
		taReportDto.setCycles(cyclelist);
		System.out.println("tareportDto=" + taReportDto.getCycles().size());
		List<TaReportDto> dto = testAutolistOnlineReportMng
				.getTaonlineNum(taReportDto);
		List<TaReportDto> dto4View = new ArrayList<TaReportDto>();
		List<TaReportDto> dto2 = testAutolistOnlineReportMng
				.getCaseTotalnum(taReportDto);
		List<TaReportDto> dto3 = testAutolistOnlineReportMng
				.getArtficialCaseTotalnum(taReportDto);
		List<TaReportDto> dto4 = testAutolistOnlineReportMng
				.getStage02AutoSuccessCasenum(taReportDto);
		List<TaReportDto> dto5 = testAutolistOnlineReportMng
				.getStage02CaseTotalnum(taReportDto);

		for (TaReportDto taReportDto22 : dto) {

			TaReportDto taReportDtonew = new TaReportDto();
			taReportDtonew.setPrefix(taReportDto22.getPrefix());
			taReportDtonew.setId(taReportDto22.getId());
			taReportDtonew.setCycles(cyclelist);
			taReportDtonew.setTcplannum(taReportDto22.getTcplannum());

			for (TaReportDto taReport : dto2) {
				if (taReport.getPrefix().equals(taReportDtonew.getPrefix())) {
					taReportDtonew.setCasetotalnum(taReport.getCasetotalnum());
				}
			}
			for (TaReportDto taReport : dto3) {
				if (taReport.getPrefix().equals(taReportDtonew.getPrefix())) {
					taReportDtonew.setArcasetotalnum(taReport
							.getArcasetotalnum());
				}
			}
			for (TaReportDto taReport : dto4) {
				if (taReport.getPrefix().equals(taReportDtonew.getPrefix())) {
					taReportDtonew.setStage02autocasesuccnum(taReport
							.getStage02autocasesuccnum());
				}
			}
			for (TaReportDto taReport : dto5) {
				if (taReport.getPrefix().equals(taReportDtonew.getPrefix())) {
					taReportDtonew.setStage02casetotalnum(taReport
							.getStage02casetotalnum());
				}
			}
			
		   
			dto4View.add(taReportDtonew);

		}
		return dto4View;
	}

}
