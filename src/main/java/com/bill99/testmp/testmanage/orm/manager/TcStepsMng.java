package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TcSteps;

public interface TcStepsMng {

	public TcSteps getById(Long id);

	public TcSteps update(TcSteps tcSteps);

	public TcSteps merge(TcSteps tcSteps);

	public TcSteps insert(TcSteps tcSteps);

	public TcSteps save(TcSteps tcSteps);

	public Long getMaxStepNum(Long tcvId);

	public void delete(TcSteps tcSteps);

	public int batch4ReBuildTcSteps(Long testCaseId, List<TcSteps> tcStepList);

	public List<TcSteps> queryAll();

	public int batch4Update(List<TcSteps> tcStepList);
}
