package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.jeecms.common.page.Pagination;

public interface TestplansMng {

	public Testplans findByIssueId(Long issueId);

	public Testplans save(Testplans testplans);

	public Testplans update(Testplans testplans);

	public List<Testplans> getByIds(List<Long> ids);

	public Pagination query(Testplans testPlan, Integer pageNo, Integer pageSize);

	public List<Testplans> query(Testplans testPlan);

	public Integer queryCount(Testplans testPlan);

	public Testplans findById(Long testplansId);

}
