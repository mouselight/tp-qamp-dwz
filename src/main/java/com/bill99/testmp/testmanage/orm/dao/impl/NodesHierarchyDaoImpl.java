package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.NodesHierarchyDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public class NodesHierarchyDaoImpl extends BaseDaoSupport<NodesHierarchy, Long> implements NodesHierarchyDao {

	private final String getRootsHql = "from NodesHierarchy where parent.id is null  order by nodeOrder,id asc ";

	public List<NodesHierarchy> getRoots() {
		return find(getRootsHql);
	}

	private final String getChildHql = "select new NodesHierarchy(id,name,parentId,nodeTypeId) from NodesHierarchy where parentId = ? and nodeTypeId  in ( 2,3 ) and status=1  ";

	public List<NodesHierarchy> getChild(Long parentId) {
		return find(getChildHql, parentId);
	}

	private final String getChildHql4Count = "select id from NodesHierarchy where parentId = ? and nodeTypeId  = 2 and status=1 ";

	public List<Long> getChildHql4Count(Long parentId) {
		return find(getChildHql4Count, parentId);
	}

	private final String getTestsuiteByIdHql = " from NodesHierarchy where id = ?  order by nodeOrder,id asc ";

	public List<NodesHierarchy> getTestsuiteById(Long id) {
		return find(getTestsuiteByIdHql, id);
	}

	private final String getByIdHql = " from NodesHierarchy where id = ? ";

	public NodesHierarchy getById(Long id) {
		return findUnique(getByIdHql, id);
	}

	private final String getCoreTestCaseChildHql = " from NodesHierarchy nh left join nh.keywords k  where k.keyword like '%核心%' and nh.parent.id = ? and nh.parent.nodeTypeId=2 and nh.nodeTypeId=3 order by nh.nodeOrder,id asc ";

	public List<NodesHierarchy> getCoreTestCaseChild(Long parentId) {
		return find(getCoreTestCaseChildHql, parentId);
	}

	private final String findAllHql = " from NodesHierarchy";

	public List<NodesHierarchy> findAll() {
		return find(findAllHql);
	}

	private final String getTcQuantityHql = " select count(tc.id) from NodesHierarchy nh,TestCases tc where nh.parentId = ? and tc.importance=3 and nh.nodeTypeId=3 and nh.status=1 and nh.id=tc.id and tc.status=nh.status ";

	public long getTcQuantity(Long pid) {
		return ((Long) findUnique(getTcQuantityHql, pid)).longValue();
	}

	private final String getChildSuite = "from NodesHierarchy where parentId = ? and nodeTypeId = 2 and status=1 order by nodeOrder,id asc ";

	public List<NodesHierarchy> getChildSuite(Long parentId) {
		return find(getChildSuite, parentId);
	}

	private final String getChildFileIds = "select id from NodesHierarchy where status=1 and parentId in (:parentIds) and nodeTypeId=2";

	public List<Long> getChildFileIds(List<Long> parentIds) {
		return getSession().createQuery(getChildFileIds).setParameterList("parentIds", parentIds).list();
	}

	private final String getChildIds = "select id from NodesHierarchy where status=1 and parentId in (:parentIds) and nodeTypeId=3";

	public List<Long> getChildIds(List<Long> parentIds) {
		return getSession().createQuery(getChildIds).setParameterList("parentIds", parentIds).list();
	}

	private final String getParentIds = "select distinct parentId from NodesHierarchy where status=1 and id in (:ids) and parentId is not null";

	public List<Long> getParentIds(List<Long> childs) {
		return getSession().createQuery(getParentIds).setParameterList("ids", childs).list();
	}

	private final String getParents = "from NodesHierarchy where status=1 and id in (:ids) ";

	public List<NodesHierarchy> getParents(List<Long> childs) {
		return getSession().createQuery(getParents).setParameterList("ids", childs).list();
	}

	private final String getNoAssoChildIds = "select t.id from NodesHierarchy t where t.status=1 and t.parentId in (:parentIds) and t.nodeTypeId=3 and not exists(select 0 from AssoPlanStepTc where stepTcPk.tcId=t.id and stepTcPk.stepId =:stepId)";

	public List<Long> getNoAssoChildIds(List<Long> parentIds, Long stepId) {
		return getSession().createQuery(getNoAssoChildIds).setParameterList("parentIds", parentIds).setLong("stepId", stepId).list();
	}

	private final String getChild4TaSql = "select nh.id,nh.`name`,nh.parent_id,nh.node_type_id from Nodes_Hierarchy nh ,asso_ta_svn_testcase att where nh.node_type_id in (2,3) and nh.`status`=1 and nh.id=att.id_testcase and nh.parent_id=? and att.id_svn_method=? ORDER BY nh.nodeOrder,id asc";

	public List<NodesHierarchy> getChild4Ta(Long pid, Long idSvnMethod) {
		return executeSqlModel(getChild4TaSql, pid, idSvnMethod);
	}

	private final static int BATCH_IMPORT_SIZE = 500;

	private final String delHql = "delete from NodesHierarchy where id= :id";

	public int batch4rvCaseAndSuite(List<NodesHierarchy> nhs4bath) {

		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		for (NodesHierarchy nf : nhs4bath) {
			try {
				session.beginTransaction();
				session.createQuery(delHql).setLong("id", nf.getId());
				//				session.delete(nf);//del
				rows++;
			} catch (Exception e) {
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("class rows = " + rows);
		return rows;
	}

	private final String getChildSuite4InvaildTcHql = "from NodesHierarchy where parentId = ? and nodeTypeId = 2 ";

	public List<NodesHierarchy> getChildSuite4InvaildTc(Long parentId) {
		return find(getChildSuite4InvaildTcHql, parentId);
	}

	private final String getTcsByNhIdHql = "select tc from NodesHierarchy nh,TestCases tc where nh.parentId = ? and nh.nodeTypeId = 3 and nh.id=tc.testCaseId";

	public List<TestCases> getTcsByNhId(Long parentId) {
		return find(getTcsByNhIdHql, parentId);
	}

	private final String getTcQuantityWithExeTypeHql = " select count(tc.id) from NodesHierarchy nh,TestCases tc where nh.parentId = ? and nh.nodeTypeId=3 and nh.status=1 and nh.id=tc.id and tc.status=nh.status and tc.executionType=?";

	public Long getTcQuantity(Long pid, Integer exeType) {
		return ((Long) findUnique(getTcQuantityWithExeTypeHql, pid, exeType)).longValue();
	}

	private final String getChildHqlWithExeType4Count = "select id from NodesHierarchy where parentId = ? and nodeTypeId  = 2 and status=1 ";

	public List<Long> getChildHql4Count(Long pid, Integer exeType) {
		return find(getChildHqlWithExeType4Count, pid, exeType);
	}
}
