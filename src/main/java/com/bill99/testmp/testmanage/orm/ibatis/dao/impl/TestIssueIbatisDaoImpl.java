package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.bill99.riaframework.common.dto.CycleDto;
import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestIssueIbatisDao;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class TestIssueIbatisDaoImpl extends QueryDaoSupport implements TestIssueIbatisDao {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public void insertIssue(final List<Map> issue) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issue.size= " + issue.size());
				if (issue != null && issue.size() > 0) {
					for (int i = 0; i < issue.size(); i++) {
						executor.insert("testIssue.insertIssue", issue.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public void insertIssueFiledValue(final List<Map> issueField) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issueField.size= " + issueField.size());
				if (null != issueField && issueField.size() > 0) {
					for (Map issueMap : issueField) {
						executor.insert("testIssue.insertIssueFieldValue", issueMap);
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public TestIssue getIssueDetail(Long issueId) {
		return (TestIssue) getSqlMapClientTemplate().queryForObject("testIssue.getIssueDetail", issueId);
	}

	public Integer queryIssueCount(TestIssue queryModel) {
		return (Integer) getSqlMapClientTemplate().queryForObject("testIssue.queryIssueCount", queryModel);
	}

	public List<TestIssue> queryIssue(TestIssue queryModel) {
		return (List<TestIssue>) getSqlMapClientTemplate().queryForList("testIssue.queryIssue", queryModel);
	}

	public Map<String, String> queryCycle(Long teamId) {
		//		return getSqlMapClientTemplate().queryForMap("testIssue.getCycleMap", teamId, "id", "cycleName");

		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CycleDto> cycles = getSqlMapClientTemplate().queryForList("testIssue.getCycleMap", teamId);

		if (cycles != null) {
			for (CycleDto cycleDto : cycles) {
				map.put(cycleDto.getId(), cycleDto.getCycleName());
			}
		}

		return map;

	}

	public List<User> queryUser(String realname, List<Long> teamIdList) {
		Map map = new HashMap();
		map.put("realname", realname);
		map.put("teamIdList", teamIdList);
		return (List<User>) getSqlMapClientTemplate().queryForList("testIssue.queryUser", map);
	}

	public void insertCycle(final List<Map> cycle) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("cycle.size= " + cycle.size());
				if (cycle != null && cycle.size() > 0) {
					for (int i = 0; i < cycle.size(); i++) {
						executor.insert("testIssue.insertCycle", cycle.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public void updateIssueCycle(final List<Map> issueCycle) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issueCycle.size= " + issueCycle.size());
				if (issueCycle != null && issueCycle.size() > 0) {
					for (int i = 0; i < issueCycle.size(); i++) {
						executor.insert("testIssue.updateIssueCycle", issueCycle.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public List<Long> querytemIdByUserId(List userIdList) {
		return queryForList("testIssue.querytemIdByUserId", userIdList);
	}

	public List<Map> findMustIssue() {
		return queryForList("testIssue.findMustIssue", null);
	}

	public void insertMustIssue(final List<Map> issue) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				logger.info("issue.size= " + issue.size());
				if (issue != null && issue.size() > 0) {
					for (int i = 0; i < issue.size(); i++) {
						executor.insert("testIssue.insertMustIssue", issue.get(i));
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public void insertMustCycle(Long cycleId) {
		this.getSqlMapClientTemplate().insert("testIssue.insertMustCycle",cycleId);
	}
}
