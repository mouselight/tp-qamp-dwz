package com.bill99.testmp.testmanage.orm.manager;

import com.bill99.testmp.testmanage.orm.entity.AssoTestplanStepUser;

public interface TestPlanAssUserMng {

	public AssoTestplanStepUser save(AssoTestplanStepUser assoTestplanStepUser);
}
