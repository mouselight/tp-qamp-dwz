package com.bill99.testmp.testmanage.orm.manager;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;

public interface TestplanStepsMng {

	public TestplanSteps get(Long id);

	public TestplanSteps save(TestplanSteps testPlanSteps);

	public void deleteById(Long stepId);

	public void saveAssoTcIds(Long stepId, List<Long> tcIds, String userName);

	public void saveFullAssoParentIds(Long stepId, List<Long> parentIds, String userName);

	public void finishAssoTc(Long stepId, List<Long> tcIds, List<Long> parentIds, Integer state, String updateUser);

	public void removeAssoTc(Long stepId, List<Long> removeTcIds);

	public List<Long> getTcId(Long stepId);

	public List<Long> getTcId(Long stepId, Integer executionType, Integer exeStatus);

	public <T extends Collection<TestplanSteps>> void getAssoTcCount(T t);

	public void saveAssoCloud(Long stepId, String assoCloudStr);

	public List<TestplanSteps> getUnitStepsByStepId(Long stepId);

	public Map<Long, String> getEnvStep(Long stepId);
	
	public void copyTc(String startStepId,String endStepId);

}
