package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.testmp.testmanage.orm.entity.AssoTestplanStepUser;
import com.jeecms.common.hibernate3.BaseDao;

public interface TestPlanAssUserDao extends BaseDao<AssoTestplanStepUser> {

}
