package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.TaSqaReportDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraSqaReportIbatisDao;

public class JiraSqaReportIbatisDaoImpl extends QueryDaoSupport implements JiraSqaReportIbatisDao {

	public List<TaSqaReportDto> getProjectName(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getProjectName", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelDev(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugLevelDev", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelStage02(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugLevelStage02", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugLevelPro(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugLevelPro", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryDev(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugcategoryDev", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryStage02(
			TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugcategoryStage02", taSqaReportDto);
	}

	public List<TaSqaReportDto> getBugcategoryPro(TaSqaReportDto taSqaReportDto) {
		// TODO Auto-generated method stub
		return queryForList("TestSqaReport.getBugcategoryPro", taSqaReportDto);
	}
	public List<TaSqaReportDto> getBugstatus(TaSqaReportDto taSqaReportDto){
		return queryForList("TestSqaReport.getBugstatus", taSqaReportDto);
	}
	
	public List<TaSqaReportDto> getEnvBug(TaSqaReportDto taSqaReportDto){
		return queryForList("TestSqaReport.getEnvBug", taSqaReportDto);
	}
}
