package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class NodesHierarchy implements Serializable {

	private static final long serialVersionUID = 6350654238111292584L;
	private Long id;
	private String name;
	private Long parentId;
	private Long nodeTypeId;
	private Integer nodeOrder;
	private Integer status;

	private boolean leaf;

	//	many-to-one
	private NodesHierarchy parent;

	//	//	one-to-many
	//	private Set<NodesHierarchy> childs;

	private String orderMark;
	private String treeMark;

	public Long getId() {
		return id;
	}

	public NodesHierarchy() {
		super();
	}

	public NodesHierarchy(Long id, Long parentId, Long nodeTypeId) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.nodeTypeId = nodeTypeId;
	}

	public NodesHierarchy(Long id, String name, Long parentId, Long nodeTypeId) {
		super();
		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.nodeTypeId = nodeTypeId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Long getNodeTypeId() {
		return nodeTypeId;
	}

	public void setNodeTypeId(Long nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}

	public Integer getNodeOrder() {
		return nodeOrder;
	}

	public void setNodeOrder(Integer nodeOrder) {
		this.nodeOrder = nodeOrder;
	}

	public NodesHierarchy getParent() {
		return parent;
	}

	public void setParent(NodesHierarchy parent) {
		this.parent = parent;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOrderMark() {
		return orderMark;
	}

	public void setOrderMark(String orderMark) {
		this.orderMark = orderMark;
	}

	public String getTreeMark() {
		return treeMark;
	}

	public void setTreeMark(String treeMark) {
		this.treeMark = treeMark;
	}

}
