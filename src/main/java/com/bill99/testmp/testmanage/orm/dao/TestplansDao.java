package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.Testplans;

public interface TestplansDao extends BaseDao<Testplans, Long> {

	public Testplans findByIssueId(Long issueId);
	
}
