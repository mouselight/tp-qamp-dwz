package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class ReportChartPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2451345107448353947L;
	private long prefix_id;
	private String cycle;
	
	public long getPrefix_id() {
		return prefix_id;
	}
	public void setPrefix_id(long prefix_id) {
		this.prefix_id = prefix_id;
	}
	
	public String getCycle() {
		return cycle;
	}
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cycle == null) ? 0 : cycle.hashCode());
		result = prime * result + (int) (prefix_id ^ (prefix_id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportChartPk other = (ReportChartPk) obj;
		if (cycle == null) {
			if (other.cycle != null)
				return false;
		} else if (!cycle.equals(other.cycle))
			return false;
		if (prefix_id != other.prefix_id)
			return false;
		return true;
	}
	
	

	

}
