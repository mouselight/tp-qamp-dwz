package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class ReleaseCycle implements Serializable {

	private static final long serialVersionUID = 86999854255583785L;

	private Long id;
	private String cycleSet;
	private String cycleName;
	private String releaseDate;
	private Date createTime;
	private Date updateTime;
	private Long state;
	private String orderField;

	public String getStateName() {
		if (this.state == null)
			return "none";
		if (this.state.intValue() == 1)
			return "有效";
		if (this.state.intValue() == 2)
			return "已关闭";
		return "none";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCycleSet() {
		return cycleSet;
	}

	public void setCycleSet(String cycleSet) {
		this.cycleSet = cycleSet;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public java.util.Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

}