package com.bill99.testmp.testmanage.orm.dao;


import com.bill99.testmp.testmanage.orm.entity.ReportChart;
import com.jeecms.common.hibernate3.BaseDao;

public interface TestReportChartDao extends BaseDao<ReportChart>{
}
