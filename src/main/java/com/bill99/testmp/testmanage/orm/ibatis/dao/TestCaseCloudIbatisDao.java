package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.QueryDao;

public interface TestCaseCloudIbatisDao extends QueryDao {

	public Map<String, String> getCloudNameMap(Long teamId);

	public void saveAsso(Long id, List<Long> tcIds);

	public void deleteAsso(Long id, List<Long> tcIds);

}
