package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.testmp.testmanage.orm.entity.TestReportLog;
import com.jeecms.common.hibernate3.BaseDao;

public interface TestReportLogDao extends BaseDao<TestReportLog> {

}
