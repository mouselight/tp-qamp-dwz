package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class StepTcPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3098592759290015688L;
	private Long stepId;
	private Long tcId;

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Long getStepId() {
		return stepId;
	}

	public void setTcId(Long tcId) {
		this.tcId = tcId;
	}

	public Long getTcId() {
		return tcId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stepId == null) ? 0 : stepId.hashCode());
		result = prime * result + ((tcId == null) ? 0 : tcId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StepTcPk other = (StepTcPk) obj;
		if (stepId == null) {
			if (other.stepId != null)
				return false;
		} else if (!stepId.equals(other.stepId))
			return false;
		if (tcId == null) {
			if (other.tcId != null)
				return false;
		} else if (!tcId.equals(other.tcId))
			return false;
		return true;
	}
	
}
