package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.common.dto.TaMethodDto;
import com.bill99.testmp.testmanage.common.dto.TaProjectDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.common.dto.TestTaTaskLogDto;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestAutomationIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;

public class TestAutomationMngImpl implements TestAutomationMng {

	private TestAutomationIbatisDao testAutomationIbatisDao;

	private TestCasesMng testCasesMng;

	public void setTestAutomationIbatisDao(TestAutomationIbatisDao testAutomationIbatisDao) {
		this.testAutomationIbatisDao = testAutomationIbatisDao;
	}

	public List<TaMethodDto> getTaMethods(TaMethodDto taMethodDto) {
		return testAutomationIbatisDao.getTaMethods(taMethodDto);
	}

	public Object queryTcByMethod(Long idSvnMethod) {
		return testAutomationIbatisDao.queryTcByMethod(idSvnMethod);
	}

	public TaMethodDto queryTaMethod(Long idSvnMethod) {
		return testAutomationIbatisDao.queryTaMethod(idSvnMethod);
	}

	public void saveAssoTaTc(Long idSvnMethod, List<Long> tcIds, List<Long> parentIds) {

		Map<String, Long> condMap = new HashMap<String, Long>();
		condMap.put("idSvnMethod", idSvnMethod);
		if (null != tcIds && tcIds.size() > 0) {
			condMap.put("nodeTypeId", 3L);
			TestCases testCase = null;
			for (Long tcId : tcIds) {
				condMap.put("tcId", tcId);
				testAutomationIbatisDao.saveAssoTaTc(condMap);

				//自动更改用例为自动
				testCase = testCasesMng.getById(tcId);
				if (testCase != null) {
					testCase.setTaStatus(1);
					testCase.setExecutionType(2);
					testCasesMng.update(testCase);
				}

			}
		}
		if (null != parentIds && parentIds.size() > 0) {
			condMap.put("nodeTypeId", 2L);
			for (Long parentId : parentIds) {
				condMap.put("tcId", parentId);
				testAutomationIbatisDao.saveAssoTaTc(condMap);
			}
		}
	}

	public void removeAssoTaTc(Long idSvnMethod, List<Long> tcIds, List<Long> parentIds) {

		Map<String, Object> condMap = new HashMap<String, Object>();
		if (null != parentIds && parentIds.size() > 0) {
			tcIds.addAll(parentIds);
		}

		List<Long> tcIdsMethod = testAutomationIbatisDao.getTcIds(idSvnMethod);

		if (tcIdsMethod != null && tcIdsMethod.size() > 0) {
			tcIdsMethod.retainAll(tcIds);

			TestCases testCase = null;
			for (Long tcId : tcIdsMethod) {
				//自动更改用例为手动
				testCase = testCasesMng.getById(tcId);
				if (testCase != null) {
					testCase.setTaStatus(0);
					testCase.setExecutionType(1);
					testCasesMng.update(testCase);
				}
			}
		} else {
			return;
		}

		condMap.put("idSvnMethod", idSvnMethod);
		condMap.put("tcIds", tcIdsMethod);
		testAutomationIbatisDao.removeAssoTaTc(condMap);

	}

	public List<TaProjectDto> getTaProject(Long testProjectId) {
		return testAutomationIbatisDao.getTaProject(testProjectId);
	}

	public void saveTaProject(TaProjectDto taProjectDto) {
		testAutomationIbatisDao.saveTaProject(taProjectDto);
	}

	public TaProjectDto getTaProjectById(Long idTaProject) {
		return testAutomationIbatisDao.getTaProjectById(idTaProject);
	}

	public void updateTaProject(TaProjectDto taProjectDto) {
		testAutomationIbatisDao.updateTaProject(taProjectDto);

	}

	public void deleteTaProject(TaProjectDto taProjectDto) {
		testAutomationIbatisDao.deleteTaProject(taProjectDto);

	}

	public void setTestCasesMng(TestCasesMng testCasesMng) {
		this.testCasesMng = testCasesMng;
	}

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(Long idTestTaTask, Long planStepId, Integer type) {
		Map paraMap = new HashMap();
		paraMap.put("idTestTaTask", idTestTaTask);
		paraMap.put("planStepId", planStepId);
		paraMap.put("type", type);
		return testAutomationIbatisDao.getTaTaskMethodResult(paraMap);

	}

	public List<TaResultDto> getTaResult(Long idTaTask, Long idSvnMethod) {
		Map paraMap = new HashMap();
		paraMap.put("idTaTask", idTaTask);
		paraMap.put("idSvnMethod", idSvnMethod);
		return testAutomationIbatisDao.getTaResult(paraMap);
	}

	public List<TestTaTaskLogDto> getTaTaskLogByPlanStepId(Long planStepId) {
		return testAutomationIbatisDao.getTaTaskLogByPlanStepId(planStepId);
	}

	public List<Long> getTestPlanTcs(Long planStepId) {
		return testAutomationIbatisDao.getTestPlanTcs(planStepId);
	}

	public List<TestTaTaskLogDto> getTaTaskLogById(Long idTestTaTask) {
		return testAutomationIbatisDao.getTaTaskLogById(idTestTaTask);
	}

	public List<TaMethodDto> queryTaMethodByTcId(Long idTestCase) {
		return testAutomationIbatisDao.queryTaMethodByTcId(idTestCase);
	}

	public List<TestTaTaskLogDto> getTaTaskLog() {
		return testAutomationIbatisDao.getTaTaskLog();
	}

	public List<Long> getStepTaFailTcs(Long idPlanStep) {
		return testAutomationIbatisDao.getStepTaFailTcs(idPlanStep);
	}

	public void insertTestTaTaskTcs4Batch(Long idTestTaTask, Long[] idTestCases) {
		testAutomationIbatisDao.insertTestTaTaskTcs4Batch(idTestTaTask, idTestCases);
	}

}
