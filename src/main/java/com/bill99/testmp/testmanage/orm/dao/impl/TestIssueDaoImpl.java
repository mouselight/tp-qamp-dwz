package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.testmanage.orm.dao.TestIssueDao;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.jeecms.common.hibernate3.BaseDaoImpl;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.page.Pagination;

public class TestIssueDaoImpl extends BaseDaoImpl<TestIssue> implements TestIssueDao {

	public Pagination query(TestIssue testIssue, Integer pageNo, Integer pageSize) {
		Finder f = Finder.create("from TestIssue t0 left outer join TestIssueField t1 on t0.issueId = t1.issueId ");

		if (null != testIssue.getTeamId()) {
			f.append("and teamId =:teamId ");
			f.setParam("teamId", testIssue.getTeamId());
		}

		if (!StringUtil.isNull(testIssue.getSummary())) {
			f.append("and summary like :summary ");
			f.setParam("summary", "%" + testIssue.getSummary() + "%");
		}

		if (null != testIssue.getCreatedStart()) {
			f.append("and created >= :createdStart ");
			f.setParam("createdStart", testIssue.getCreatedStart());
		}

		if (null != testIssue.getCreatedEnd()) {
			f.append("and created <= :createdEnd ");
			f.setParam("createdEnd", testIssue.getCreatedEnd());
		}
		f.append("order by updated desc");
		return find(f, pageNo, pageSize);
	}
}
