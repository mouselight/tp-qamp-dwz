package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TestProjectChange;

public interface TestProjectChangeMng {

	public List<TestProjectChange> getByIssueId(Long issueid);

	public TestProjectChange getByChangeId(Long changeId);

	public TestProjectChange save(TestProjectChange testProjectChange);

	public TestProjectChange update(TestProjectChange testProjectChange);

	public void delete(Long changeId);

}
