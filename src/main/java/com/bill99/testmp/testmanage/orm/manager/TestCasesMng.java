package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import sun.security.util.BigInt;

import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testmanage.common.dto.ExcelImportDto;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public interface TestCasesMng {

	public List<TestCases> queryTestcasesList(TestCasesQueryDto testCasesQueryDto, Page page);

	public List<Long> queryTcIds(TestCasesQueryDto testCasesQueryDto);

	public List<Long> getFilterTcIds(List<Long> parentIds, TestCasesQueryDto testCasesQueryDto);

	public TestCases getById(Long testCaseId);

	public TestCases update(TestCases testCases);

	public TestCases save(TestCases testCases);

	public void bath4Save(ExcelImportDto excelImportDto);

	public void bath4Update(ExcelImportDto excelImportDto);

	public int bath4Update(List<TestCases> listTestCases);

	public void delete(Long testCaseId);

	public List<TestCases> queryAll();

	public List<NodesHierarchy> getTaTaskTcs(String taTaskId);
	
	public List<TestCases> getPlanTrackCases(Long planTrackId);

	
	
	
	
	

}
