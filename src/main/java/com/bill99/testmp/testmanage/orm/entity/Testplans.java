package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class Testplans implements Serializable {

	private static final long serialVersionUID = -836265683331661370L;
	private Long planId;
	private Long issueId;
	private Long teamId;
	private Date planBeginDate;
	private Date planEndDate;
	private String fastsummary;
	private String memo;
	private Integer state;
	private Date createTime;
	private Date updateTime;
	private String createUser;
	private String updateUser;
	private Long cycleId;
	private Boolean isMust;

	private String summary;
	private String releaseId;//上线ID
	private String pkey;
	private Date releaseDate;//期望上线日期
	private Date releaseDateStart;//预计上线时间始
	private Date releaseDateEnd;//预计上线时间始止
	private Integer startRow;
	private Integer endRow;
	private Integer[] states;//状态条件
	private Integer[] stepStateCos;//子任务状态

	private Integer isReleaseDate;//是否有上线时间条件
	private Long userId;
	private TestIssue testIssue;

	private Set<TestplanSteps> testplanSteps;

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Set<TestplanSteps> getTestplanSteps() {
		return testplanSteps;
	}

	public void setTestplanSteps(Set<TestplanSteps> testplanSteps) {
		this.testplanSteps = testplanSteps;
	}

	public Date getPlanBeginDate() {
		return planBeginDate;
	}

	public void setPlanBeginDate(Date planBeginDate) {
		this.planBeginDate = planBeginDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSummary() {
		return summary;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setIsReleaseDate(Integer isReleaseDate) {
		this.isReleaseDate = isReleaseDate;
	}

	public Integer getIsReleaseDate() {
		if (null != getReleaseDateStart() || null != getReleaseDateEnd()) {
			isReleaseDate = new Integer(1);
		}
		return isReleaseDate;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public void setReleaseDateStart(Date releaseDateStart) {
		this.releaseDateStart = releaseDateStart;
	}

	public Date getReleaseDateStart() {
		return releaseDateStart;
	}

	public void setReleaseDateEnd(Date releaseDateEnd) {
		this.releaseDateEnd = releaseDateEnd;
	}

	public Date getReleaseDateEnd() {
		return releaseDateEnd;
	}

	public void setTestIssue(TestIssue testIssue) {
		this.testIssue = testIssue;
	}

	public TestIssue getTestIssue() {
		//ibatis查询时可能会返回null,为避免在freemarker反复判断对象是否存在,此处直接新建对象
		if (null == testIssue) {
			testIssue = new TestIssue();
		}
		return testIssue;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setStepStateCos(Integer[] stepStateCos) {
		this.stepStateCos = stepStateCos;
	}

	public Integer[] getStepStateCos() {
		return stepStateCos;
	}

	public void setStates(Integer[] states) {
		this.states = states;
	}

	public Integer[] getStates() {
		return states;
	}

	public void setPkey(String pkey) {
		this.pkey = pkey;
	}

	public String getPkey() {
		return pkey;
	}

	public void setFastsummary(String fastsummary) {
		this.fastsummary = fastsummary;
	}

	public String getFastsummary() {
		return fastsummary;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public Boolean getIsMust() {
		return isMust;
	}

	public void setIsMust(Boolean isMust) {
		this.isMust = isMust;
	}
}
