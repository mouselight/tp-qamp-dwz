package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaMethodDto;
import com.bill99.testmp.testmanage.common.dto.TaProjectDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.common.dto.TestTaTaskLogDto;

public interface TestAutomationMng {

	public List<TaMethodDto> getTaMethods(TaMethodDto taMethodDto);

	public TaMethodDto queryTaMethod(Long idSvnMethod);

	public Object queryTcByMethod(Long idSvnMethod);

	public void saveAssoTaTc(Long idSvnMethod, List<Long> tcIds, List<Long> parentIds);

	public void removeAssoTaTc(Long idSvnMethod, List<Long> tcIds, List<Long> parentIds);

	public List<TaProjectDto> getTaProject(Long testProjectId);

	public void saveTaProject(TaProjectDto taProjectDto);

	public TaProjectDto getTaProjectById(Long idTaProject);

	public void updateTaProject(TaProjectDto taProjectDto);

	public void deleteTaProject(TaProjectDto taProjectDto);

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(Long idTestTaTask, Long planStepId, Integer type);

	public List<TaResultDto> getTaResult(Long idTaTask, Long idSvnMethod);

	public List<TestTaTaskLogDto> getTaTaskLogByPlanStepId(Long planStepId);

	public List<TestTaTaskLogDto> getTaTaskLogById(Long idTestTaTask);

	public List<Long> getTestPlanTcs(Long planStepId);

	public List<TaMethodDto> queryTaMethodByTcId(Long idTestCase);

	public List<TestTaTaskLogDto> getTaTaskLog();

	public List<Long> getStepTaFailTcs(Long idPlanStep);

	public void insertTestTaTaskTcs4Batch(final Long idTestTaTask, final Long[] idTestCases);

}
