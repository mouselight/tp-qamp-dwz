package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class NotifyIssueLog implements Serializable {

	private static final long serialVersionUID = 5753712916856653952L;
	private Long idIssueLog;
	private Long issueId;
	private Integer issueType;
	private Long teemId;
	private Date createDate;

	public Long getIdIssueLog() {
		return idIssueLog;
	}

	public void setIdIssueLog(Long idIssueLog) {
		this.idIssueLog = idIssueLog;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Integer getIssueType() {
		return issueType;
	}

	public void setIssueType(Integer issueType) {
		this.issueType = issueType;
	}

	public Long getTeemId() {
		return teemId;
	}

	public void setTeemId(Long teemId) {
		this.teemId = teemId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
