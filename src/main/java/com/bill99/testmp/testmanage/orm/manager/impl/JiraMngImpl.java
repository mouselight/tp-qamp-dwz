package com.bill99.testmp.testmanage.orm.manager.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.common.dto.MustStepDto;
import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestIssueIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanStepIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

public class JiraMngImpl implements JiraMng {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TestIssueIbatisDao testIssueIbatisDao;
	private JiraIbatisDao jiraIbatisDao;
	private TestPlanIbatisDao testPlanIbatisDao;
	private TestPlanStepIbatisDao testPlanStepIbatisDao;
	private TestProjectsMng testProjectsMng;

	public void initJiraData() {
		logger.info("JiraService---------Start---------- ");

		List<Map> issue = jiraIbatisDao.queryIssue();
		testIssueIbatisDao.insertIssue(issue);
		logger.info("JiraService---------insertIssue-OK ");

		List<Map> issueField = jiraIbatisDao.queryIssueFiledValue();
		testIssueIbatisDao.insertIssueFiledValue(issueField);
		logger.info("JiraService---------insertIssueFiledValue-OK ");

		List<Map> cycle = jiraIbatisDao.queryAllCycle();
		testIssueIbatisDao.insertCycle(cycle);
		logger.info("JiraService---------insertCycle-OK ");

		List<Map> issueCycle = jiraIbatisDao.queryIssueCycle();
		testIssueIbatisDao.updateIssueCycle(issueCycle);
		logger.info("JiraService---------updateIssueCycle-OK ");

		//插入必测需求&计划
		List<Map> mustIssue = testIssueIbatisDao.findMustIssue();
		testIssueIbatisDao.insertMustIssue(mustIssue);
		logger.info("JiraService---------insertMustIssue-OK ");

		testPlanIbatisDao.insertMustPlan(mustIssue);

		List<Map> mustPlan = testPlanIbatisDao.findMustPlan();
		//插入中间表,记录以生成的cycle
		for (int i = 0; i < mustIssue.size(); i++) {
			testIssueIbatisDao.insertMustCycle(Long.valueOf(mustIssue.get(i).get("cycleId").toString()));
		}
		Map mustPlanmap = null;
		TestProjects testProjects = null;
		Map mustStepMap=null;
		for (int i = 0; i < mustPlan.size(); i++) {
			mustPlanmap = mustPlan.get(i);
			testProjects = testProjectsMng.findByTeemId(((BigDecimal) mustPlanmap.get("team_id")).longValue());
            
			String exeEnv = "6,10";
			for (String env : exeEnv.split(",")) {
		
				if (testProjects != null && StringUtils.hasLength(testProjects.getUnitType())) {
					Long stepTId=null;
					int j=1;
					for (String unitType : testProjects.getUnitType().split(",")) {
						mustStepMap=new HashMap();
						mustStepMap.put("unit_type", unitType);
						mustStepMap.put("is_must", 1);
						mustStepMap.put("step_num", (Long) mustPlanmap.get("plan_id") + 1);
						
						//mustStepMap.put("type", Long.valueOf(env));
						mustStepMap.put("type", Integer.valueOf(env));
						mustStepMap.put("state", 1);
						
						if ("T".equals(unitType)) {//多机组支持
							mustStepMap.remove("parent_id");
							mustStepMap.put("plan_id", mustPlanmap.get("plan_id"));
							stepTId=testPlanStepIbatisDao.insertMustStep(mustStepMap);
						} else {
							mustStepMap.put("parent_id",stepTId);
							mustStepMap.put("step_id",stepTId+j);
							mustStepMap.put("plan_id",stepTId+j);
							testPlanStepIbatisDao.insertMustStep(mustStepMap);
							j++;
						}
					
					}
					
				}
			}
			
		}
		if(mustPlan!=null&&mustPlan.size()>0){
		List<MustStepDto> mustStepList = testPlanStepIbatisDao.findMustStep();
		testPlanStepIbatisDao.insertMustTc(mustStepList);
		}
	}

	public Map<String, String> queryCycleByIssue(Long issueId) {
		return jiraIbatisDao.queryCycleByIssue(issueId);
	}

	public void setTestIssueIbatisDao(TestIssueIbatisDao testIssueIbatisDao) {
		this.testIssueIbatisDao = testIssueIbatisDao;
	}

	public void setJiraIbatisDao(JiraIbatisDao jiraIbatisDao) {
		this.jiraIbatisDao = jiraIbatisDao;
	}

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds) {
		return jiraIbatisDao.getEmail4Role(roleIds, teemIds);
	}

	public List<TestNotifyBugDto> getNotifyIssueBugs(Long issueId) {
		return jiraIbatisDao.getNotifyIssueBugs(issueId);
	}

	public Map<Long, List<TestNotifyBugDto>> getNotifyBugs(Long teemId, List<Long> notifiedBugIds) {

		Map<Long, List<TestNotifyBugDto>> testNotifyBugMap = new HashMap<Long, List<TestNotifyBugDto>>();
		List<TestNotifyBugDto> testNotifyBugs = jiraIbatisDao.getNotifyBugs(teemId, notifiedBugIds);

		List<TestNotifyBugDto> testNotifyBugsTemp = null;
		Long key = null;
		if (testNotifyBugs != null && testNotifyBugs.size() > 0) {
			for (TestNotifyBugDto testNotifyBugDto : testNotifyBugs) {
				key = testNotifyBugDto.getIssueId();
				testNotifyBugsTemp = testNotifyBugMap.get(key);
				if (testNotifyBugsTemp == null) {
					testNotifyBugsTemp = new ArrayList<TestNotifyBugDto>();
				}
				testNotifyBugsTemp.add(testNotifyBugDto);
				testNotifyBugMap.put(key, testNotifyBugsTemp);
			}

		}
		return testNotifyBugMap;
	}

	public List<TestNotifyBugDto> getNotifyIssues(Long teemId, List<Long> notifiedBugIds) {
		return jiraIbatisDao.getNotifyIssues(teemId, notifiedBugIds);
	}

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId) {
		return jiraIbatisDao.getEmail4RoleIssue(roleIssueIds, issueId);
	}

	public TestPlanIbatisDao getTestPlanIbatisDao() {
		return testPlanIbatisDao;
	}

	public void setTestPlanIbatisDao(TestPlanIbatisDao testPlanIbatisDao) {
		this.testPlanIbatisDao = testPlanIbatisDao;
	}

	public TestPlanStepIbatisDao getTestPlanStepIbatisDao() {
		return testPlanStepIbatisDao;
	}

	public void setTestPlanStepIbatisDao(TestPlanStepIbatisDao testPlanStepIbatisDao) {
		this.testPlanStepIbatisDao = testPlanStepIbatisDao;
	}

	public TestProjectsMng getTestProjectsMng() {
		return testProjectsMng;
	}

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}

}
