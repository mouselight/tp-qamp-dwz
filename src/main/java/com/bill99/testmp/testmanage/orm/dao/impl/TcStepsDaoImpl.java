package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.Session;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TcStepsDao;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;

public class TcStepsDaoImpl extends BaseDaoSupport<TcSteps, Long> implements TcStepsDao {
	private static final Log logger = LogFactory.getLog(TcStepsDaoImpl.class);

	private final static int BATCH_IMPORT_SIZE = 1000;

	private final String deleteSetpHql = "delete TcSteps where testCaseId = ?";

	public int batch4ReBuildTcSteps(Long testCaseId, List<TcSteps> tcStepList) {

		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		execute(deleteSetpHql, testCaseId);
		if (tcStepList.size() == 0) {
			return rows;
		}
		for (TcSteps tcSteps : tcStepList) {
			try {
				session.beginTransaction();
				tcSteps.setId(null);
				tcSteps.setTestCaseId(testCaseId);
				session.save(tcSteps);//save
				rows++;
			} catch (Exception e) {
				logger.error(e);
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("commit, rows = " + rows);
		return rows;
	}

	private static final String queryAllHql = "from TcSteps";

	public List<TcSteps> queryAll() {
		return find(queryAllHql);
	}

	public int batch4Update(List<TcSteps> tcStepList) {

		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		if (tcStepList.size() == 0) {
			return rows;
		}
		for (TcSteps tcSteps : tcStepList) {
			try {
				session.beginTransaction();
				session.update(tcSteps);//update
				rows++;
			} catch (Exception e) {
				logger.error(e);
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("commit, rows = " + rows);
		return rows;

	}

}
