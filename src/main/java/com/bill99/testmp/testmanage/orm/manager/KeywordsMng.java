package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.Keywords;

public interface KeywordsMng {

	public List<Keywords> getByTestProjectId(Long testProjectId);

	public Keywords getById(Long keywordId);

	public Keywords save(Keywords keywords);

	public Keywords update(Keywords keywords);

	public void delete(Long keywordsId);

}
