package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;

public interface TestTaTaskLogDao extends BaseDao<TestTaTaskLog, Long> {

	List<TestTaTaskLog> getByPlanId(Long planId);

	TestTaTaskLog getLastTask(Long planStepId);

	List<TestTaTaskLog> getByPlanStepId(Long planStepId);

}
