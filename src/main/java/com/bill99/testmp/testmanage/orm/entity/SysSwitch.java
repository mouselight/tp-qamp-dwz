package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class SysSwitch implements Serializable {

	private static final long serialVersionUID = -8304820591697025907L;

	private Long idSysSwitch;

	private Boolean notifyIssueState;
	private String notifyIssueRecvaddress;
	private String notifyIssueCcaddress;

	private Boolean notifyBugState;
	private String notifyBugRecvaddress;
	private String notifyBugCcaddress;

	private Boolean notifyScheduleState;
	private String notifyScheduleRecvaddress;
	private String notifyScheduleCcaddress;

	private Boolean notifyTaState;
	private String notifyTaRecvaddress;
	private String notifyTaCcaddress;

	public Long getIdSysSwitch() {
		return idSysSwitch;
	}

	public void setIdSysSwitch(Long idSysSwitch) {
		this.idSysSwitch = idSysSwitch;
	}

	public Boolean getNotifyIssueState() {
		return notifyIssueState;
	}

	public void setNotifyIssueState(Boolean notifyIssueState) {
		this.notifyIssueState = notifyIssueState;
	}

	public String getNotifyIssueRecvaddress() {
		return notifyIssueRecvaddress;
	}

	public void setNotifyIssueRecvaddress(String notifyIssueRecvaddress) {
		this.notifyIssueRecvaddress = notifyIssueRecvaddress;
	}

	public String getNotifyIssueCcaddress() {
		return notifyIssueCcaddress;
	}

	public void setNotifyIssueCcaddress(String notifyIssueCcaddress) {
		this.notifyIssueCcaddress = notifyIssueCcaddress;
	}

	public Boolean getNotifyBugState() {
		return notifyBugState;
	}

	public void setNotifyBugState(Boolean notifyBugState) {
		this.notifyBugState = notifyBugState;
	}

	public String getNotifyBugRecvaddress() {
		return notifyBugRecvaddress;
	}

	public void setNotifyBugRecvaddress(String notifyBugRecvaddress) {
		this.notifyBugRecvaddress = notifyBugRecvaddress;
	}

	public String getNotifyBugCcaddress() {
		return notifyBugCcaddress;
	}

	public void setNotifyBugCcaddress(String notifyBugCcaddress) {
		this.notifyBugCcaddress = notifyBugCcaddress;
	}

	public Boolean getNotifyScheduleState() {
		return notifyScheduleState;
	}

	public void setNotifyScheduleState(Boolean notifyScheduleState) {
		this.notifyScheduleState = notifyScheduleState;
	}

	public String getNotifyScheduleRecvaddress() {
		return notifyScheduleRecvaddress;
	}

	public void setNotifyScheduleRecvaddress(String notifyScheduleRecvaddress) {
		this.notifyScheduleRecvaddress = notifyScheduleRecvaddress;
	}

	public String getNotifyScheduleCcaddress() {
		return notifyScheduleCcaddress;
	}

	public void setNotifyScheduleCcaddress(String notifyScheduleCcaddress) {
		this.notifyScheduleCcaddress = notifyScheduleCcaddress;
	}

	public Boolean getNotifyTaState() {
		return notifyTaState;
	}

	public void setNotifyTaState(Boolean notifyTaState) {
		this.notifyTaState = notifyTaState;
	}

	public String getNotifyTaRecvaddress() {
		return notifyTaRecvaddress;
	}

	public void setNotifyTaRecvaddress(String notifyTaRecvaddress) {
		this.notifyTaRecvaddress = notifyTaRecvaddress;
	}

	public String getNotifyTaCcaddress() {
		return notifyTaCcaddress;
	}

	public void setNotifyTaCcaddress(String notifyTaCcaddress) {
		this.notifyTaCcaddress = notifyTaCcaddress;
	}

}
