package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TestProjectChange;

public interface TestProjectChangeDao extends BaseDao<TestProjectChange, Long> {

	public List<TestProjectChange> findByIssueId(Long issueid);

	public TestProjectChange findByChangeId(Long changeid);

}
