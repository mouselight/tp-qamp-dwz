package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.KeywordsDao;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;

public class KeywordsMngImpl implements KeywordsMng {

	private KeywordsDao keywordsDao;

	public List<Keywords> getByTestProjectId(Long testProjectId) {
		return keywordsDao.getByTestProjectId(testProjectId);
	}

	public void setKeywordsDao(KeywordsDao keywordsDao) {
		this.keywordsDao = keywordsDao;
	}

	public Keywords getById(Long keywordId) {
		return keywordsDao.get(keywordId);
	}

	public Keywords save(Keywords keywords) {
		return keywordsDao.save(keywords);
	}

	public Keywords update(Keywords keywords) {
		return keywordsDao.update(keywords);
	}

	public void delete(Long keywordsId) {
		keywordsDao.delete(keywordsId);
	}
}
