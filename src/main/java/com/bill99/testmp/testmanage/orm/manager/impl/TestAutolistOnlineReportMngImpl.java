package com.bill99.testmp.testmanage.orm.manager.impl;


import java.util.Date;
import java.util.List;

import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;
import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.common.dto.TestCycleDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestAutolistOnlineReportIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestAutolistOnlineReportMng;

public class TestAutolistOnlineReportMngImpl implements TestAutolistOnlineReportMng {
	private TestAutolistOnlineReportIbatisDao testAutolistOnlineReportIbatisDao;
	
	public void setTestAutolistOnlineReportIbatisDao(TestAutolistOnlineReportIbatisDao testAutolistOnlineReportIbatisDao){
		this.testAutolistOnlineReportIbatisDao = testAutolistOnlineReportIbatisDao;
	}
	
    public List<TaReportDto> getTaonlineNum(TaReportDto taReportDto){
	      return testAutolistOnlineReportIbatisDao.getTaonlineNum(taReportDto);
}
	
    public List<TestCycleDto> getCycleList(){
    	return testAutolistOnlineReportIbatisDao.getCycleList();
    }
 
  
	
	public List<TaReportDto>  getCaseTotalnum(TaReportDto taReportDto)
	{
		return testAutolistOnlineReportIbatisDao.getCaseTotalnum(taReportDto);
	}
	
	
  
	public List<TaReportDto> getArtficialCaseTotalnum(TaReportDto taReportDto)
	   {
		   return testAutolistOnlineReportIbatisDao.getArtficialCaseTotalnum(taReportDto);
	   }
	
	public List<TaReportDto>  getStage02CaseTotalnum(TaReportDto taReportDto)
	{
		return testAutolistOnlineReportIbatisDao.getStage02CaseTotalnum(taReportDto);
	}
	
	public List<TaReportDto>  getAutoCaseTotalnum(TaReportDto taReportDto)
	{
		return testAutolistOnlineReportIbatisDao.getAutoCaseTotalnum(taReportDto);
	}
	
	public Date getMaxCycleDate(){
		return (Date) testAutolistOnlineReportIbatisDao.getMaxCycleDate();
	}
	public List<TaReportDto>  getStage02AutoSuccessCasenum(TaReportDto taReportDto){
		return testAutolistOnlineReportIbatisDao.getStage02AutoSuccessCasenum(taReportDto);
	}
	
	public List<TaReportChartDto> getChartData(TaReportChartDto taReportChartDto){
		return testAutolistOnlineReportIbatisDao.getChartData(taReportChartDto);

	}
	public List<TaReportDto> getIssueNum(TaReportDto taReportDto){
		return testAutolistOnlineReportIbatisDao.getIssueNum(taReportDto);

	}
	public List<TaReportDto> getAllproject(){
		return testAutolistOnlineReportIbatisDao.getAllproject();
	}


}
