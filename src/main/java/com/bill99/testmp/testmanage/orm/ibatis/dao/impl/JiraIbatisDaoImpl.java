package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraIbatisDao;

public class JiraIbatisDaoImpl extends QueryDaoSupport implements JiraIbatisDao {

	public List<Map> queryIssue() {
		return super.queryForList("jira.queryIssue", null);
	}

	public List<Map> queryIssueFiledValue() {
		return super.queryForList("jira.queryIssueFieldValue", null);
	}

	public Map<String, String> queryCycleByIssue(Long issueId) {
		return super.queryForObject("jira.queryCycleByIssue", issueId);
	}

	public List<TestReport> getBugList(Long issueId) {
		return queryForList("jira.queryBugList", issueId);
	}

	public List<Map> queryAllCycle() {
		return super.queryForList("jira.queryAllCycle", null);
	}

	public List<Map> queryIssueCycle() {
		return super.queryForList("jira.queryIssusCycle", null);
	}

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds) {
		Map<String, Long[]> map = new HashMap<String, Long[]>();
		map.put("roleIds", roleIds);
		map.put("teemIds", teemIds);
		return super.queryForList("jira.getEmail4Role", map);
	}

	public List<TestNotifyBugDto> getNotifyIssueBugs(Long issueId) {
		return queryForList("jira.getNotifyIssueBugs", issueId);
	}

	public List<TestNotifyBugDto> getNotifyBugs(Long teemId, List<Long> notifiedBugIds) {
		Map map = new HashMap();
		map.put("teemId", teemId);
		map.put("notifiedBugIds", notifiedBugIds);

		if (notifiedBugIds != null && notifiedBugIds.size() > 0) {
			List<BigInteger> notifiedBugIdss = (List<BigInteger>) map.get("notifiedBugIds");
			Long[] bugIds = new Long[notifiedBugIds.size()];
			if (notifiedBugIdss != null) {
				int i = 0;
				for (BigInteger long1 : notifiedBugIdss) {
					bugIds[i] = Long.valueOf(long1.toString());
					i++;
				}
			}
			map.put("notifiedBugIds", bugIds);
		}

		return queryForList("jira.getNotifyBugs", map);
	}

	public List<TestNotifyBugDto> getNotifyIssues(Long teemId, List<Long> notifiedBugIds) {
		Map map = new HashMap();
		map.put("teemId", teemId);
		map.put("notifiedBugIds", notifiedBugIds);

		if (notifiedBugIds != null && notifiedBugIds.size() > 0) {
			List<BigInteger> notifiedBugIdss = (List<BigInteger>) map.get("notifiedBugIds");
			Long[] bugIds = new Long[notifiedBugIds.size()];
			if (notifiedBugIdss != null) {
				int i = 0;
				for (BigInteger long1 : notifiedBugIdss) {
					bugIds[i] = Long.valueOf(long1.toString());
					i++;
				}
			}
			map.put("notifiedBugIds", bugIds);
		}

		return queryForList("jira.getNotifyIssues", map);
	}

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId) {
		Map map = new HashMap();
		map.put("roleIssueIds", roleIssueIds);
		map.put("issueId", issueId);
		return super.queryForList("jira.getEmail4RoleIssue", map);
	}
}
