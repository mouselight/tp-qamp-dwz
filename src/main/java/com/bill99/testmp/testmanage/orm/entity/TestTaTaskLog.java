package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class TestTaTaskLog implements Serializable {

	private static final long serialVersionUID = -8389672165667931323L;

	private Long idTestTaTask;
	private String taTaskId;
	private Long testPlanId;
	private Long testProjectId;
	private Boolean isActual;
	private Date triggerTime;
	private Boolean state;
	private String memo;
	private String createUser;
	private Date createDate;
	private Boolean taResult;

	private Long planStepId;
	//TC执行统计
	private Integer allCnt;
	private Integer doneCnt;
	private Integer undoneCnt;
	private Integer succCnt;
	private Integer failCnt;
	private String coverPer;
	private String passPer;

	private Date updateDate;

	private Integer result;
	private String tcTds;

	public void setCoverPer(String coverPer) {
		this.coverPer = coverPer;
	}

	public String getCoverPer() {
		if (getAllCnt() != null && getDoneCnt() != null && getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			coverPer = df1.format(getDoneCnt().floatValue() / getAllCnt().floatValue());
		}
		return coverPer;
	}

	public void setPassPer(String passPer) {
		this.passPer = passPer;
	}

	public String getPassPer() {
		if (getAllCnt() != null && getSuccCnt() != null && getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			passPer = df1.format(getSuccCnt().floatValue() / getAllCnt().floatValue());
		}
		return passPer;
	}

	public Long getIdTestTaTask() {
		return idTestTaTask;
	}

	public void setIdTestTaTask(Long idTestTaTask) {
		this.idTestTaTask = idTestTaTask;
	}

	public Long getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(Long testPlanId) {
		this.testPlanId = testPlanId;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Boolean getIsActual() {
		return isActual;
	}

	public void setIsActual(Boolean isActual) {
		this.isActual = isActual;
	}

	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getTaTaskId() {
		return taTaskId;
	}

	public void setTaTaskId(String taTaskId) {
		this.taTaskId = taTaskId;
	}

	public Boolean getTaResult() {
		return taResult;
	}

	public void setTaResult(Boolean taResult) {
		this.taResult = taResult;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Integer getAllCnt() {
		return allCnt;
	}

	public void setAllCnt(Integer allCnt) {
		this.allCnt = allCnt;
	}

	public Integer getDoneCnt() {
		return doneCnt;
	}

	public void setDoneCnt(Integer doneCnt) {
		this.doneCnt = doneCnt;
	}

	public Integer getUndoneCnt() {
		return undoneCnt;
	}

	public void setUndoneCnt(Integer undoneCnt) {
		this.undoneCnt = undoneCnt;
	}

	public Integer getSuccCnt() {
		return succCnt;
	}

	public void setSuccCnt(Integer succCnt) {
		this.succCnt = succCnt;
	}

	public Integer getFailCnt() {
		return failCnt;
	}

	public void setFailCnt(Integer failCnt) {
		this.failCnt = failCnt;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getTcTds() {
		return tcTds;
	}

	public void setTcTds(String tcTds) {
		this.tcTds = tcTds;
	}

}
