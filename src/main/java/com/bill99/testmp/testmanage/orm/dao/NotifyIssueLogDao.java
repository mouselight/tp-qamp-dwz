package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.NotifyIssueLog;

public interface NotifyIssueLogDao extends BaseDao<NotifyIssueLog, Long> {

	public List<Long> getNeedNotifyIssues(List<Long> issueIds, Integer issueType);

	public List<Long> getNotifiedIssues(Long teemId, Integer issueType);

}
