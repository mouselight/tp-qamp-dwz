package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;

import com.bill99.testmp.testmanage.common.dto.NodesImportDto;
import com.bill99.testmp.testmanage.common.dto.TestStepProgressDto;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.entity.TestReport;

public interface TestReportIbatisDao {

	public List<TestReport> getCaseStatis(Long stepId, Long groupId);

	public List<TestCaseTree> getCaseList(Long stepId, Long groupId);
	
	public List<TestCaseTree> getMustCaseList(Long stepId);

	public TestCaseTree getAllParentTc(Long tcId);

	public TestReport getIssue(Long issueId);

	public List<TestReport> getQaInfo(Long stepId);

	public NodesImportDto getParentId(Long tcId);

	public List<TestStepProgressDto> getTestStepProgress(Long stepId);
	
	public List<TestCaseTree> getMustCaseList(String teamId);
	
	public String getTeamIdByStepId(String issueId);
	
	public String isMustByPlanStep(Long planStep);

}
