package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;

public interface JiraMng {

	public void initJiraData();

	public Map<String, String> queryCycleByIssue(Long issueId);

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds);

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId);

	public List<TestNotifyBugDto> getNotifyIssueBugs(Long issueId);

	public Map<Long, List<TestNotifyBugDto>> getNotifyBugs(Long teemId, List<Long> notifiedBugIds);

	public List<TestNotifyBugDto> getNotifyIssues(Long teemId, List<Long> notifiedBugIds);
}
