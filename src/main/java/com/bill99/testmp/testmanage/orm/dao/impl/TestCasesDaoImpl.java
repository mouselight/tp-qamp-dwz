package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.Session;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.framework.helper.ExcelImportHelper;
import com.bill99.testmp.testmanage.common.dto.ExcelImportDto;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.orm.dao.TestCasesDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public class TestCasesDaoImpl extends BaseDaoSupport<TestCases, Long> implements TestCasesDao {
	private static Log logger = LogFactory.getLog(TestCasesDaoImpl.class);

	private final static int BATCH_IMPORT_SIZE = 1000;

	private final String deleteSetpHql = "delete TcSteps where testCaseId = :testCaseId";

	private final String updateHql = "update NodesHierarchy set name = :testCaseName,status= :status where id =:testCaseId";
	private final String bath4UpdateHql = "update NodesHierarchy set name = :testCaseName,status= :status,parentId=:parentId where id =:testCaseId";
	private final String bath4UpdateHqlWithOrder = "update NodesHierarchy set name = :testCaseName,status= :status,parentId=:parentId,nodeOrder = :nodeOrder  where id =:testCaseId";

	public void importBatch(boolean isNew, ExcelImportDto excelImportDto) {
		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		for (TestCases testCases : excelImportDto.getTestCasesList()) {
			try {
				session.beginTransaction();
				if (isNew) {
					NodesHierarchy node = new NodesHierarchy();
					node.setStatus(testCases.getStatus());
					node.setName(testCases.getTestCaseName());
					node.setNodeTypeId(NodeTypeUtils.TESTCASE);
					node.setParentId(excelImportDto.getTestSuiteId());
					Long testCaseId = (Long) session.save(node);
					testCases.setTestCaseId(testCaseId);
					tcStepsRefress(testCases, session);
					session.save(testCases);// save
				} else {
					session.createQuery(updateHql).setString("testCaseName", testCases.getTestCaseName()).setLong("testCaseId", testCases.getTestCaseId())
							.setInteger("status", testCases.getStatus()).executeUpdate();
					tcStepsRefress(testCases, session);
					session.update(testCases);// update
				}
				// 注册下次扫描日期end
				rows++;
			} catch (Exception e) {
				logger.error(e);
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("commit, rows = " + rows);
	}

	private void tcStepsRefress(TestCases testCases, Session session) {
		session.createQuery(deleteSetpHql).setLong("testCaseId", testCases.getTestCaseId()).executeUpdate();
		for (TcSteps tcSteps : ExcelImportHelper.getTcStepsList(testCases.getActions(), testCases.getExpectedResults())) {
			tcSteps.setTestCaseId(testCases.getTestCaseId());
			session.save(tcSteps);
		}
	}

	public int bath4Update(List<TestCases> listTestCases) {

		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		for (TestCases testCases : listTestCases) {
			try {
				session.beginTransaction();
				if (testCases.getNodeOrder() != null) {
					session.createQuery(bath4UpdateHqlWithOrder).setString("testCaseName", testCases.getTestCaseName()).setLong("testCaseId", testCases.getTestCaseId())
							.setInteger("status", testCases.getStatus()).setLong("parentId", testCases.getTestSuiteId()).setInteger("nodeOrder", testCases.getNodeOrder())
							.executeUpdate();
				} else {
					session.createQuery(bath4UpdateHql).setString("testCaseName", testCases.getTestCaseName()).setLong("testCaseId", testCases.getTestCaseId())
							.setInteger("status", testCases.getStatus()).setLong("parentId", testCases.getTestSuiteId()).executeUpdate();
				}

				session.update(testCases);// update
				rows++;
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
				session.getTransaction().rollback();
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("commit, rows = " + rows);
		return rows;
	}

	public int bath4Del(List<TestCases> listTestCases) {

		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		for (TestCases testCases : listTestCases) {
			try {
				session.beginTransaction();
				session.delete(testCases);// del
				rows++;
			} catch (Exception e) {
				logger.error(e);
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
				logger.info("commit, rows = " + rows);
			}
		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("commit, rows = " + rows);
		return rows;
	}

	private static final String queryAllHql = "from TestCases";

	public List<TestCases> queryAll() {
		return find(queryAllHql);
	}

	private static final String queryByProjectIdHql = "from TestCases where testprojectId=?";

	public List<TestCases> queryByProjectId(Long testprojectId) {
		return find(queryByProjectIdHql, testprojectId);
	}

	private final String queryPlanTrackCaseHql = "select tc from AssoPlanStepTc atst , TestCases tc  where atst.stepTcPk.tcId=tc.testCaseId  and atst.stepTcPk.stepId=?";

	public List<TestCases> getPlanTrackCasesDao(Long planTrackId) {

		return find(queryPlanTrackCaseHql, planTrackId);
	}



}
