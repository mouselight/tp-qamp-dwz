package com.bill99.testmp.testmanage.orm.entity;

import java.util.Date;

public class SvnChangeItem implements Comparable<SvnChangeItem> {

	public static final char TYPE_ADDED = 65;
	public static final char TYPE_DELETED = 68;
	public static final char TYPE_MODIFIED = 77;
	public static final char TYPE_REPLACED = 82;

	private Long id;
	private Long changeSetId;
	private String filePath;
	private char opType;
	private long revision;
	private Date submitTime;
	private String logMsg;
	private String author;
	private String nodeType = "unknow";

	private boolean mergedOK = false;
	private long newRevision = 0;
	private String newAuthor;

	public byte[] fromData;
	public byte[] toOldData;
	private boolean isProcessed = false;
	private boolean isNewFile = false;

	public void copyFrom(SvnChangeItem si) {
		this.filePath = si.filePath;
		this.opType = si.opType;
		this.revision = si.revision;
		this.submitTime = si.submitTime;
		this.logMsg = si.logMsg;
		this.author = si.author;
		this.nodeType = si.nodeType;
	}

	public String getDirPath() {
		return this.getDirPath(this.filePath);
	}

	public String getDirPath(String fp) {
		if ("dir".equals(this.nodeType))
			return fp;
		if ("file".equals(this.nodeType)) {
			int n = fp.lastIndexOf("/");
			if (n > 0) {
				return fp.substring(0, n);
			} else {
				return null;
			}
		}
		if ("none".equals(this.nodeType)) {
			if (fp.lastIndexOf(".") > 0) {
				int n = fp.lastIndexOf("/");
				if (n > 0) {
					return fp.substring(0, n);
				} else {
					return null;
				}
			} else {
				return fp;
			}
		}
		return null;
	}

	public String getFileName() {
		if ("file".equals(this.nodeType)) {
			int n = this.filePath.lastIndexOf("/");
			if (n > 0) {
				return this.filePath.substring(n + 1);
			} else {
				return null;
			}
		} else {
			if (this.filePath.lastIndexOf(".") > 0) {
				int n = this.filePath.lastIndexOf("/");
				if (n > 0) {
					return this.filePath.substring(n + 1);
				} else {
					return null;
				}
			}
		}
		return null;
	}

	public String getNewFilePath(String fromApp) {
		int n = this.filePath.lastIndexOf(fromApp);
		if (n > 0) {
			String path = this.filePath.substring(n + fromApp.length());
			return path;
		}
		return null;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public char getOpType() {
		return opType;
	}

	public void setOpType(char opType) {
		this.opType = opType;
	}

	public long getRevision() {
		return revision;
	}

	public void setRevision(long revision) {
		this.revision = revision;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public String getLogMsg() {
		return logMsg;
	}

	public void setLogMsg(String logMsg) {
		this.logMsg = logMsg;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "SvnChangeItem [filePath=" + filePath + ", nodeType=" + this.nodeType + ", opType=" + opType
				+ ", revision=" + revision + ", submitTime=" + submitTime + ", logMsg=" + logMsg + ", author=" + author
				+ "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getChangeSetId() {
		return changeSetId;
	}

	public void setChangeSetId(Long changeSetId) {
		this.changeSetId = changeSetId;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public long getNewRevision() {
		return newRevision;
	}

	public void setNewRevision(long newRevision) {
		this.newRevision = newRevision;
	}

	public boolean isMergedOK() {
		return mergedOK;
	}

	public void setMergedOK(boolean mergedOK) {
		this.mergedOK = mergedOK;
	}

	public String getNewAuthor() {
		return newAuthor;
	}

	public void setNewAuthor(String newAuthor) {
		this.newAuthor = newAuthor;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public boolean isNewFile() {
		return isNewFile;
	}

	public void setNewFile(boolean isNewFile) {
		this.isNewFile = isNewFile;
	}

	public int compareTo(SvnChangeItem o) {
		return (int) (this.revision - o.revision);
	}

}
