package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class TestCases implements Serializable {

	private static final long serialVersionUID = -238320255454063735L;
	private Long testSuiteId;
	private String testSuiteName;
	private Long testCaseId;
	private String testCaseName;
	private String summary;//摘要
	private String preconditions;//前置条件
	private Date createDate;//创建时间
	private Date updateDate;//修改时间
	private Integer executionType;//测试方式
	private Integer importance;//用例等级
	private Integer status;//状态
	private Integer taStatus;//自动化
	private String updateUser;
	private String createUser;

	private Long testprojectId;

	private List<TcSteps> tcStepsList;
	private Set<Keywords> keywordsSet;

	private String actions;
	private String expectedResults;

	private NodesHierarchy nodesHierarchy;
	private Integer nodeOrder;

	public Long getTestSuiteId() {
		return testSuiteId;
	}

	public void setTestSuiteId(Long testSuiteId) {
		this.testSuiteId = testSuiteId;
	}

	public String getTestSuiteName() {
		return testSuiteName;
	}

	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPreconditions() {
		return preconditions;
	}

	public void setPreconditions(String preconditions) {
		this.preconditions = preconditions;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getExecutionType() {
		return executionType;
	}

	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	public Integer getImportance() {
		return importance;
	}

	public void setImportance(Integer importance) {
		this.importance = importance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTaStatus() {
		return taStatus;
	}

	public void setTaStatus(Integer taStatus) {
		this.taStatus = taStatus;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public List<TcSteps> getTcStepsList() {
		return tcStepsList;
	}

	public void setTcStepsList(List<TcSteps> tcStepsList) {
		this.tcStepsList = tcStepsList;
	}

	public Set<Keywords> getKeywordsSet() {
		return keywordsSet;
	}

	public void setKeywordsSet(Set<Keywords> keywordsSet) {
		this.keywordsSet = keywordsSet;
	}

	public Long getTestprojectId() {
		return testprojectId;
	}

	public void setTestprojectId(Long testprojectId) {
		this.testprojectId = testprojectId;
	}

	public String getActions() {
		return actions;
	}

	public void setActions(String actions) {
		this.actions = actions;
	}

	public String getExpectedResults() {
		return expectedResults;
	}

	public void setExpectedResults(String expectedResults) {
		this.expectedResults = expectedResults;
	}

	public Integer getNodeOrder() {
		return nodeOrder;
	}

	public void setNodeOrder(Integer nodeOrder) {
		this.nodeOrder = nodeOrder;
	}

	public NodesHierarchy getNodesHierarchy() {
		return nodesHierarchy;
	}

	public void setNodesHierarchy(NodesHierarchy nodesHierarchy) {
		this.nodesHierarchy = nodesHierarchy;
	}

}
