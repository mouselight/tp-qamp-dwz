package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TestTaTaskLogDao;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;
import com.bill99.testmp.testmanage.orm.manager.TestTaTaskLogMng;

public class TestTaTaskLogMngImpl implements TestTaTaskLogMng {

	private TestTaTaskLogDao testTaTaskLogDao;

	public void setTestTaTaskLogDao(TestTaTaskLogDao testTaTaskLogDao) {
		this.testTaTaskLogDao = testTaTaskLogDao;
	}

	public TestTaTaskLog save(TestTaTaskLog testTaTaskLog) {
		return testTaTaskLogDao.save(testTaTaskLog);
	}

	public List<TestTaTaskLog> getByPlanId(Long planId) {
		return testTaTaskLogDao.getByPlanId(planId);
	}

	public TestTaTaskLog getLastTask(Long planStepId) {
		return testTaTaskLogDao.getLastTask(planStepId);
	}

	public TestTaTaskLog get(Long idTestTaTask) {
		return testTaTaskLogDao.get(idTestTaTask);
	}

	public List<TestTaTaskLog> getByPlanStepId(Long planStepId) {
		return testTaTaskLogDao.getByPlanStepId(planStepId);
	}

	public TestTaTaskLog update(TestTaTaskLog testTaTaskLog) {
		return testTaTaskLogDao.update(testTaTaskLog);
	}

}
