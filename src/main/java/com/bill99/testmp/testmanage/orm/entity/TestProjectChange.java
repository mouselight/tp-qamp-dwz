package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TestProjectChange implements Serializable {

	private static final long serialVersionUID = 2836005031668345214L;

	private Long changeid;
	private Long issueid;
	private String changename;
	private String changenotes;
	private String changetype;
	private String changeattach; //附件
	private Date changetime;
	private Date createdate;
	private String create_user;
	private String update_user;
	private String changeremark;

	public Long getChangeid() {
		return changeid;
	}

	public void setChangeid(Long changeid) {
		this.changeid = changeid;
	}

	public Long getIssueid() {
		return issueid;
	}

	public void setIssueid(Long issueid) {
		this.issueid = issueid;
	}

	public String getChangename() {
		return changename;
	}

	public void setChangename(String changename) {
		this.changename = changename;
	}

	public String getChangenotes() {
		return changenotes;
	}

	public void setChangenotes(String changenotes) {
		this.changenotes = changenotes;
	}

	public String getChangetype() {
		return changetype;
	}

	public void setChangetype(String changetype) {
		this.changetype = changetype;
	}

	public String getChangeattach() {
		return changeattach;
	}

	public void setChangeattach(String changeattach) {
		this.changeattach = changeattach;
	}

	public Date getChangetime() {
		return changetime;
	}

	public void setChangetime(Date changetime) {
		this.changetime = changetime;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getCreate_user() {
		return create_user;
	}

	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}

	public String getUpdate_user() {
		return update_user;
	}

	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}

	public String getChangeremark() {
		return changeremark;
	}

	public void setChangeremark(String changeremark) {
		this.changeremark = changeremark;
	}

}
