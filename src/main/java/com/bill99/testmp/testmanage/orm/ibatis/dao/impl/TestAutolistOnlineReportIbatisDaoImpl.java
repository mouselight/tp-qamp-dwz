package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.Date;
import java.util.List;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;
import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.common.dto.TestCycleDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestAutolistOnlineReportIbatisDao;

public class TestAutolistOnlineReportIbatisDaoImpl extends QueryDaoSupport implements TestAutolistOnlineReportIbatisDao {

	public List<TaReportDto> getTaonlineNum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getTaonlineNum", taReportDto);
	}

	public List<TestCycleDto> getCycleList() {

		return queryForList("TestReportNew.getCyclelist", null);
	}

	public List<TaReportDto> getCaseTotalnum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getCaseTotalnum", taReportDto);
		//return getSqlMapClientTemplate().queryForMap("TestReportNew.getAutoCaseTotalnum", taReportDto, "prefix", "caseTotnum");	
	}

	public List<TaReportDto> getStage02CaseTotalnum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getStage02CaseTotalnum", taReportDto);
	}
	   
	   public List<TaReportChartDto> getChartData(TaReportChartDto taReportChartDto){
		   return queryForList("TestReportNew.getChartData", taReportChartDto);
	   }


	public List<TaReportDto> getArtficialCaseTotalnum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getArtficialCaseTotalnum", taReportDto);
	}

	public List<TaReportDto> getAutoCaseTotalnum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getStage02AutoCaseTotalnum", taReportDto);
	}

	public Date getMaxCycleDate() {
		return queryForObject("TestReportNew.getMaxCycleDate", null);
	}

	public List<TaReportDto> getStage02AutoSuccessCasenum(TaReportDto taReportDto) {
		return queryForList("TestReportNew.getAutoCaseSuccessnum", taReportDto);
	}
	public List<TaReportDto> getIssueNum(TaReportDto taReportDto){
		return queryForList("TestReportNew.getIssueNum", taReportDto);
	}
	
	public List<TaReportDto> getAllproject(){
		return queryForList("TestReportNew.getAllproject", null);
	}

	/*
	 * public List<TaReportDto> getTaAutoNum(TaReportDto taReportDto){ return
	 * queryForList("TestReportNew.getTaAutoNum", taReportDto); }
	 * 
	 * public long getTaAutoAllNum(Long idTestProject){ return
	 * queryForObject("TestAutomation.getTaAutoAllNum", idTestProject); }
	 */


}
