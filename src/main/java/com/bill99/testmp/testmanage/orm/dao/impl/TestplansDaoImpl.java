package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TestplansDao;
import com.bill99.testmp.testmanage.orm.entity.Testplans;

public class TestplansDaoImpl extends BaseDaoSupport<Testplans, Long> implements TestplansDao {

	private final String findByIssueIdHql = "from Testplans where issueId=? ";

	public Testplans findByIssueId(Long issueId) {
		return findUnique(findByIssueIdHql, issueId);
	}

}
