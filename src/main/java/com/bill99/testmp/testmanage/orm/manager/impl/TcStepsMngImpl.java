package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TcStepsDao;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.ibatis.dao.NodesHierarchyIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanStepIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TcStepsMng;

public class TcStepsMngImpl implements TcStepsMng {

	private TcStepsDao tcStepsDao;
	private NodesHierarchyIbatisDao nodesHierarchyIbatisDao;
	private TestPlanStepIbatisDao testPlanStepIbatisDao;

	

	public void setTcStepsDao(TcStepsDao tcStepsDao) {
		this.tcStepsDao = tcStepsDao;
	}

	public TcSteps getById(Long id) {
		return tcStepsDao.get(id);
	}

	public TcSteps update(TcSteps tcSteps) {
		return tcStepsDao.update(tcSteps);
	}

	public TcSteps merge(TcSteps tcSteps) {
		return tcStepsDao.merge(tcSteps);
	}

	public TcSteps insert(TcSteps tcSteps) {
		return tcStepsDao.save(tcSteps);
	}

	public TcSteps save(TcSteps tcSteps) {
		return tcStepsDao.save(tcSteps);
	}

	public void setNodesHierarchyIbatisDao(NodesHierarchyIbatisDao nodesHierarchyIbatisDao) {
		this.nodesHierarchyIbatisDao = nodesHierarchyIbatisDao;
	}

	public Long getMaxStepNum(Long tcvId) {
		return nodesHierarchyIbatisDao.queryForObject("TestCase.getMaxStepNumByTcVId", tcvId);
	}

	public void delete(TcSteps tcSteps) {
		tcStepsDao.delete(tcSteps);
	}

	public int batch4ReBuildTcSteps(Long testCaseId, List<TcSteps> tcStepList) {
		return tcStepsDao.batch4ReBuildTcSteps(testCaseId, tcStepList);
	}

	public List<TcSteps> queryAll() {
		return tcStepsDao.queryAll();
	}

	public int batch4Update(List<TcSteps> tcStepList) {
		return tcStepsDao.batch4Update(tcStepList);
	}

	public void setTestPlanStepIbatisDao(TestPlanStepIbatisDao testPlanStepIbatisDao) {
		this.testPlanStepIbatisDao = testPlanStepIbatisDao;
	}
}
