package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.Assert;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.testmanage.orm.dao.TestplanStepsDao;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanStepIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;

public class TestplanStepsMngImpl implements TestplanStepsMng {

	private TestplanStepsDao testplanStepsDao;
	private TestPlanIbatisDao testPlanIbatisDao;
	private TestPlanStepIbatisDao testPlanStepIbatisDao;

	public void setTestPlanStepIbatisDao(TestPlanStepIbatisDao testPlanStepIbatisDao) {
		this.testPlanStepIbatisDao = testPlanStepIbatisDao;
	}

	public void setTestPlanIbatisDao(TestPlanIbatisDao testPlanIbatisDao) {
		this.testPlanIbatisDao = testPlanIbatisDao;
	}

	public void setTestplanStepsDao(TestplanStepsDao testplanStepsDao) {
		this.testplanStepsDao = testplanStepsDao;
	}

	public TestplanSteps save(TestplanSteps testPlanSteps) {
		return testplanStepsDao.save(testPlanSteps);
	}

	public void deleteById(Long stepId) {
		Assert.notNull(stepId);
		testplanStepsDao.delete(stepId);
	}

	public void saveAssoTcIds(Long stepId, List<Long> tcIds, String userName) {
		Assert.notEmpty(tcIds);
		Assert.notNull(stepId);
		testPlanIbatisDao.saveAssoStepTc(stepId, tcIds, userName);
	}

	public void saveFullAssoParentIds(Long stepId, List<Long> parentIds, String userName) {
		List<Long> fullAssoParentIds = testPlanIbatisDao.getFullAssoParentIds(parentIds, stepId);
		if (null != fullAssoParentIds && fullAssoParentIds.size() > 0) {
			this.saveAssoTcIds(stepId, fullAssoParentIds, userName);
		}
	}

	public void finishAssoTc(Long stepId, List<Long> tcIds, List<Long> parentIds, Integer state, String updateUser) {
		Assert.notEmpty(tcIds);
		Assert.notNull(stepId);
		testPlanIbatisDao.finishAssoTc(stepId, tcIds, parentIds, state, updateUser);
	}

	public List<Long> getTcId(Long stepId) {
		return testplanStepsDao.getTcId(stepId);
	}

	public List<Long> getTcId(Long stepId, Integer executionType, Integer exeStatus) {
		return testplanStepsDao.getTcId(stepId, executionType, exeStatus);
	}

	public void removeAssoTc(Long stepId, List<Long> removeTcIds) {
		Assert.notEmpty(removeTcIds);
		Assert.notNull(stepId);
		testPlanIbatisDao.removeAssoStepTc(stepId, removeTcIds);
	}

	public <T extends Collection<TestplanSteps>> void getAssoTcCount(T t) {
		List<TestplanSteps> testPlanSteps = new ArrayList<TestplanSteps>();
		if (t instanceof List) {
			testPlanSteps = (List<TestplanSteps>) t;
		}
		if (t instanceof Set) {
			testPlanSteps = new ArrayList<TestplanSteps>((Set<TestplanSteps>) t);
		}
		if (null != testPlanSteps && testPlanSteps.size() > 0) {
			for (TestplanSteps testPlanStep : testPlanSteps) {
				TestplanSteps tcCount = testPlanIbatisDao.getTcCountByStep(testPlanStep.getStepId());
				if (null != tcCount) {
					testPlanStep.setAssoTcCount(tcCount.getAssoTcCount());
					testPlanStep.setAssoFinishTcCount(tcCount.getAssoFinishTcCount());
				}
				List<TestplanSteps> unitSteps = getUnitStepsByStepId(testPlanStep.getStepId());
				if (unitSteps != null && unitSteps.size() > 0) {
					int tcCounts = 0;
					int finishTcCount = 0;
					for (TestplanSteps unitStep : unitSteps) {
						TestplanSteps tcCountTmp = testPlanIbatisDao.getTcCountByStep(unitStep.getStepId());
						if (tcCountTmp != null) {
							tcCounts = tcCounts + (tcCountTmp.getAssoTcCount() == null ? 0 : tcCountTmp.getAssoTcCount());
							finishTcCount = finishTcCount + (tcCountTmp.getAssoFinishTcCount() == null ? 0 : tcCountTmp.getAssoFinishTcCount());
						}
					}
					testPlanStep.setAssoTcCount(tcCounts + (testPlanStep.getAssoTcCount() == null ? 0 : testPlanStep.getAssoTcCount()));
					testPlanStep.setAssoFinishTcCount(finishTcCount + (testPlanStep.getAssoFinishTcCount() == null ? 0 : testPlanStep.getAssoFinishTcCount()));
				}
			}
		}
	}

	public void saveAssoCloud(Long stepId, String assoCloudStr) {
		Assert.notNull(stepId);
		//save - cloudTc
		if (!StringUtil.isNull(assoCloudStr)) {
			testPlanIbatisDao.saveTcByCloud(stepId, StringUtil.strToListObj(assoCloudStr));
		}
		//update -  cloudIdStr
		TestplanSteps testStep = testplanStepsDao.get(stepId);
		if (null != testStep) {
			testStep.setAssoCloud(assoCloudStr);
		}
		testplanStepsDao.update(testStep);
	}

	public TestplanSteps get(Long id) {
		return testplanStepsDao.get(id);
	}

	public List<TestplanSteps> getUnitStepsByStepId(Long stepId) {
		return testplanStepsDao.getUnitStepsByStepId(stepId);
	}

	public Map<Long, String> getEnvStep(Long stepId) {

		return testPlanStepIbatisDao.getEnvStep(stepId);
	}
	public void copyTc(String startStepId, String endStepId) {
		
		testPlanStepIbatisDao.copyTc(startStepId, endStepId);	
	}
}
