package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TestProjectsDao;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

public class TestProjectsMngImpl implements TestProjectsMng {

	private TestProjectsDao testProjectsDao;

	public void setTestProjectsDao(TestProjectsDao testProjectsDao) {
		this.testProjectsDao = testProjectsDao;
	}

	public TestProjects getById(Long testProjectId) {
		if (testProjectId == null) {
			return null;
		}
		return testProjectsDao.get(testProjectId);
	}

	public TestProjects save(TestProjects testProjects) {
		return testProjectsDao.save(testProjects);
	}

	public TestProjects update(TestProjects testProjects) {
		return testProjectsDao.update(testProjects);
	}

	public List<TestProjects> findALl() {
		return testProjectsDao.findALl();
	}

	public List<TestProjects> findValidList() {
		return testProjectsDao.findValidList();
	}

	public List<TestProjects> findByIds(String ids) {
		return testProjectsDao.findByIds(ids);
	}

	public TestProjects findByTeemId(Long teemId) {
		return testProjectsDao.findByTeemId(teemId);
	}

}
