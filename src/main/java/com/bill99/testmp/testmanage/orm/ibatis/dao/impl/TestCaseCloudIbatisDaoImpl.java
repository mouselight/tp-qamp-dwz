package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestCaseCloudIbatisDao;

public class TestCaseCloudIbatisDaoImpl extends QueryDaoSupport implements TestCaseCloudIbatisDao {

	public void saveAsso(Long id, List<Long> tcIds) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("tcIds", tcIds);
		condMap.put("id", id);
		getSqlMapClientTemplate().insert("TestCaseCloud.saveAsso", condMap);
	}

	public void deleteAsso(Long id, List<Long> tcIds) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("tcIds", tcIds);
		condMap.put("id", id);
		getSqlMapClientTemplate().delete("TestCaseCloud.removeAsso", condMap);
	}

	public Map<String, String> getCloudNameMap(Long teamId) {
		return getSqlMapClientTemplate().queryForMap("TestCaseCloud.getCloudMap", teamId, "id", "name");
	}
}
