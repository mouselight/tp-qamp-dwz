package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.SysSwitchDao;
import com.bill99.testmp.testmanage.orm.entity.SysSwitch;

public class SysSwitchDaoImpl extends BaseDaoSupport<SysSwitch, Long> implements SysSwitchDao {

	private final String getSysSwitchHql = " from SysSwitch where idSysSwitch = ?";

	public SysSwitch getSysSwitch(Long type) {
		return findUnique(getSysSwitchHql, type);
	}

}
