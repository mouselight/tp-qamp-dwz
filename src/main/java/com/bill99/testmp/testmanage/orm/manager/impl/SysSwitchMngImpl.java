package com.bill99.testmp.testmanage.orm.manager.impl;

import com.bill99.testmp.testmanage.orm.dao.SysSwitchDao;
import com.bill99.testmp.testmanage.orm.entity.SysSwitch;
import com.bill99.testmp.testmanage.orm.manager.SysSwitchMng;

public class SysSwitchMngImpl implements SysSwitchMng {

	private SysSwitchDao sysSwitchDao;

	public void setSysSwitchDao(SysSwitchDao sysSwitchDao) {
		this.sysSwitchDao = sysSwitchDao;
	}

	public SysSwitch getSysSwitch(Long type) {
		return sysSwitchDao.getSysSwitch(type);
	}

	public void update(SysSwitch sysSwitch) {
		sysSwitchDao.update(sysSwitch);
	}

}
