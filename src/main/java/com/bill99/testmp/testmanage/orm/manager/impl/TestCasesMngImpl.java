package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testmanage.common.dto.ExcelImportDto;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.orm.dao.TestCasesDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.ibatis.dao.NodesHierarchyIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;

public class TestCasesMngImpl implements TestCasesMng {

	private NodesHierarchyIbatisDao nodesHierarchyIbatisDao;

	private TestCasesDao testCasesDao;

	public void setNodesHierarchyIbatisDao(NodesHierarchyIbatisDao nodesHierarchyIbatisDao) {
		this.nodesHierarchyIbatisDao = nodesHierarchyIbatisDao;
	}

	public List<TestCases> queryTestcasesList(TestCasesQueryDto testCasesQueryDto, Page page) {
		return nodesHierarchyIbatisDao.queryForList("TestSuite.query-testcases-list", page, testCasesQueryDto);
	}

	public List<Long> queryTcIds(TestCasesQueryDto testCasesQueryDto) {
		return nodesHierarchyIbatisDao.queryForList("TestSuite.queryTcId", testCasesQueryDto);
	}

	public List<Long> getFilterTcIds(List<Long> parentIds, TestCasesQueryDto testCasesQueryDto) {
		testCasesQueryDto.setParentIds(parentIds);
		testCasesQueryDto.setTestProjectIds(StringUtil.string2LongArray(testCasesQueryDto.getTestProjectIdCosStr()));
		return nodesHierarchyIbatisDao.queryForList("TestSuite.queryTcIds", testCasesQueryDto);
	}

	public void setTestCasesDao(TestCasesDao testCasesDao) {
		this.testCasesDao = testCasesDao;
	}

	public TestCases getById(Long testCaseId) {
		return testCasesDao.get(testCaseId);
	}

	public TestCases update(TestCases testCases) {
		return testCasesDao.update(testCases);
	}

	public TestCases save(TestCases testCases) {
		return testCasesDao.save(testCases);
	}

	public void bath4Save(ExcelImportDto excelImportDto) {
		testCasesDao.importBatch(true, excelImportDto);
	}

	public void bath4Update(ExcelImportDto excelImportDto) {
		testCasesDao.importBatch(false, excelImportDto);
	}

	public int bath4Update(List<TestCases> listTestCases) {
		return testCasesDao.bath4Update(listTestCases);
	}

	public void delete(Long testCaseId) {
		testCasesDao.delete(testCaseId);
	}

	public List<TestCases> queryAll() {
		return testCasesDao.queryAll();
	}

	public List<NodesHierarchy> getTaTaskTcs(String taTaskId) {
		return nodesHierarchyIbatisDao.getTaTaskTcs(taTaskId);
	}

	public List<TestCases> getPlanTrackCases(Long planTrackId) {

		return testCasesDao.getPlanTrackCasesDao(planTrackId);
	}

}
