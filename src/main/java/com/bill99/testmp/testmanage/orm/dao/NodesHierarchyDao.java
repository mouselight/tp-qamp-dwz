package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public interface NodesHierarchyDao extends BaseDao<NodesHierarchy, Long> {

	// 获得所有根节点
	public List<NodesHierarchy> getRoots();

	// 获得子节点
	public List<NodesHierarchy> getChild(Long parentId);

	public List<Long> getChildIds(List<Long> parentIds);

	public List<Long> getNoAssoChildIds(List<Long> parentIds, Long stepId);

	public List<Long> getChildFileIds(List<Long> parentIds);

	public List<Long> getParentIds(List<Long> childs);

	public List<Long> getChildHql4Count(Long parentId);

	public List<NodesHierarchy> getTestsuiteById(Long id);

	public NodesHierarchy getById(Long id);

	public List<NodesHierarchy> getCoreTestCaseChild(Long parentId);

	public List<NodesHierarchy> findAll();

	public long getTcQuantity(Long pid);

	public List<NodesHierarchy> getChildSuite(Long parentId);

	public List<NodesHierarchy> getChild4Ta(Long pid, Long idSvnMethod);

	public int batch4rvCaseAndSuite(List<NodesHierarchy> nhs4bath);

	public List<NodesHierarchy> getChildSuite4InvaildTc(Long parentId);

	public List<TestCases> getTcsByNhId(Long parentId);

	public List<NodesHierarchy> getParents(List<Long> childs);

	public Long getTcQuantity(Long pid, Integer exeType);

	public List<Long> getChildHql4Count(Long pid, Integer exeType);

}
