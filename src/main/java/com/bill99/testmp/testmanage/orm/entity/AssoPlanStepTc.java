package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class AssoPlanStepTc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3782866645453293044L;

	private StepTcPk stepTcPk;
	private Integer status;
	private Integer nodeTypeId;

	public void setStepTcPk(StepTcPk stepTcPk) {
		this.stepTcPk = stepTcPk;
	}

	public StepTcPk getStepTcPk() {
		return stepTcPk;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setNodeTypeId(Integer nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}

	public Integer getNodeTypeId() {
		return nodeTypeId;
	}
}
