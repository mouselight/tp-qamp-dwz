package com.bill99.testmp.testmanage.orm.manager;

import com.bill99.testmp.testmanage.orm.entity.SysSwitch;

public interface SysSwitchMng {

	public SysSwitch getSysSwitch(Long type);

	public void update(SysSwitch sysSwitch);

}
