package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bill99.riaframework.common.dto.CaseStateDto;
import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.ibatis.dao.NodesHierarchyIbatisDao;

public class NodesHierarchyIbatisDaoImpl extends QueryDaoSupport implements NodesHierarchyIbatisDao {

	public void initTestCase(String sqlMap, Map<String, Long> map) {
		getSqlMapClientTemplate().update(sqlMap, map);
		// getSqlMapClientTemplate().delete(sqlMap, map);
	}

	public List<NodesHierarchy> getTaTaskTcs(String taTaskId) {
		return queryForList("TestCase.getTaTaskTcs", taTaskId);
	}

	public void update4InvalidTc(Long id, Integer status) {
		Map map = new HashMap();
		map.put("parentId", id);
		map.put("status", status);
		getSqlMapClientTemplate().update("TestCase.update4InvalidTc", map);
		getSqlMapClientTemplate().update("TestCase.update4InvalidNhTc", map);
		getSqlMapClientTemplate().update("TestCase.update4InvalidNhSuite", map);
	}

	public List<Long> getAssoNodes(Long planStepId) {

		return queryForList("TestCase.getAssoNodes", planStepId);
	}

	public List<TestProjects> getProjectGroup(Long planStepId) {
		return queryForList("TestCase.getProjectGroup", planStepId);
	}

	public List<TestProjects> getGroup(Set<Long> pids) {
		List<Long> pidList = new ArrayList<Long>(pids);
		// return getSqlMapClientTemplate().queryForList("TestCase.getGroup", pidList);
		return queryForList("TestCase.getGroup", pidList);
	}

	public CaseStateDto getTcStateByStep(Long pid, Long stepId) {
		Map<String, Long> paramMap = new HashMap<String, Long>();
		paramMap.put("pid", pid);
		paramMap.put("stepId", stepId);
		return queryForObject("TestCase.getTcStateByStep", paramMap);
	}

	public List<NodesHierarchy> getTcSuiteByStep(Long pid, Long stepId) {
		Map<String, Long> paramMap = new HashMap<String, Long>();
		paramMap.put("pid", pid);
		paramMap.put("stepId", stepId);
		return queryForList("TestCase.getTcSuiteByStep", paramMap);
	}

	public Map<Long,Long> getNodeTcCount(Long nodeId) {
		return getSqlMapClientTemplate().queryForMap("TestCase.getNodeTcCount", nodeId,"nodeId","nodeTypeId");
	}

}
