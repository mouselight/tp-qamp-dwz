package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class TaSvn implements Serializable {

	private static final long serialVersionUID = 223986982549503311L;

	private Long idSvn;
	private Long testProjectId;
	private String filePath;
	private String methodTa;
	private String memo;

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getMethodTa() {
		return methodTa;
	}

	public void setMethodTa(String methodTa) {
		this.methodTa = methodTa;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getIdSvn() {
		return idSvn;
	}

	public void setIdSvn(Long idSvn) {
		this.idSvn = idSvn;
	}

}
