package com.bill99.testmp.testmanage.domain;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.entity.Testplans;

public interface TestPlanService {

	public void mergeTestPlans(TestPlansDto plansDto, String currentEmail);

	public List<Long> getAllParentIds(Long planStepId);

	public List<Long> getParentIdsExecution(Long planStepId, Integer executionType, Integer exeStatus);

	public Map<Integer, List<Long>> getAllChildIds(List<Long> parentIds);

	public List<Long> getStepAllChilds(List<Long> parentIds, Long stepId);

	public void updateTestPlan(TestPlansDto plansDto);

	public List<Testplans> getPrivyPlan(Long userId, Long teamId);

	public void hasWriteAuth(Long userId, Set<TestplanSteps> testplanSteps);

	public void updatePlanSummary(Long planId);

	public void updateSvnUrl(Long stepId, String svnUrl);

}
