package com.bill99.testmp.testmanage.domain;

import com.bill99.testmp.testmanage.common.dto.TpTaskRequestBo;
import com.bill99.testmp.testmanage.common.dto.TpTaskResponesBo;

public interface TpTaskService {

	public TpTaskResponesBo triggerTpTask(TpTaskRequestBo tpTaskRequestBo);
}
