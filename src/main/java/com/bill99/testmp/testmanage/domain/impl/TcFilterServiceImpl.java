package com.bill99.testmp.testmanage.domain.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

public class TcFilterServiceImpl implements TcFilterService {

	private NodesHierarchyMng nodesHierarchyMng;
	private TestCasesMng testCasesMng;
	private TestProjectsMng testProjectsMng;

	public void setNodesHierarchyMng(NodesHierarchyMng nodesHierarchyMng) {
		this.nodesHierarchyMng = nodesHierarchyMng;
	}

	public void setTestCasesMng(TestCasesMng testCasesMng) {
		this.testCasesMng = testCasesMng;
	}

	//将查询的TC Node结果保存起来 
	public void engineFilter(HttpSession httpSession, TestCasesQueryDto testCasesQueryDto) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		String userTcKey = user.getId().toString() + TreeUtils.TC_FILTER_KEY_SUFFIX;
		String userNodeKey = user.getId().toString() + TreeUtils.NODE_FILTER_KEY_SUFFIX;
		String userProjectKey = user.getId().toString() + TreeUtils.PROJECT_FILTER_KEY_SUFFIX;

		TestProjects currentTestProject = user.getCurrentTestProject();
		if (currentTestProject != null) {
			if (StringUtils.hasLength(testCasesQueryDto.getKeywordsCosStr())) {
				testCasesQueryDto.setKeywordsCos(StringUtil.string2LongArray(testCasesQueryDto.getKeywordsCosStr()));
			}
			if (StringUtils.hasLength(testCasesQueryDto.getStatusCosStr())) {
				testCasesQueryDto.setStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getStatusCosStr()));
			}
			if (StringUtils.hasLength(testCasesQueryDto.getImportanceCosStr())) {
				testCasesQueryDto.setImportanceCos(StringUtil.string2LongArray(testCasesQueryDto.getImportanceCosStr()));
			}
			if (StringUtils.hasLength(testCasesQueryDto.getExecutionTypeCosStr())) {
				testCasesQueryDto.setExecutionTypeCos(StringUtil.string2LongArray(testCasesQueryDto.getExecutionTypeCosStr()));
			}
			if (StringUtils.hasLength(testCasesQueryDto.getTaStatusCosStr())) {
				testCasesQueryDto.setTaStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getTaStatusCosStr()));
			}
			if (StringUtils.hasLength(testCasesQueryDto.getTestProjectIdCosStr())) {
				testCasesQueryDto.setTestProjectIds(StringUtil.string2LongArray(testCasesQueryDto.getTestProjectIdCosStr()));
			}
			testCasesQueryDto.setTestProjectId(currentTestProject.getId());
		}

		List<Long> tcIds = testCasesMng.queryTcIds(testCasesQueryDto);
		List<Long> partIds = nodesHierarchyMng.getAllParentIds(tcIds);

		if (!StringUtils.hasLength(testCasesQueryDto.getTestProjectIdCosStr())) {
			testCasesQueryDto.setTestProjectIdCosStr(user.getCurrentTestProject().getId().toString());
		}
		List<TestProjects> testProjects = testProjectsMng.findByIds(testCasesQueryDto.getTestProjectIdCosStr());

		TreeUtils.tcFilterMap.put(userTcKey, tcIds);
		TreeUtils.nodeFilterMap.put(userNodeKey, partIds);
		TreeUtils.nodeTestProjectMap.put(userProjectKey, testProjects);
	}

	public void clearFilter(Long userId, HttpSession httpSession) {
		TreeUtils.tcFilterMap.put(userId.toString() + TreeUtils.TC_FILTER_KEY_SUFFIX, null);
		TreeUtils.nodeFilterMap.put(userId.toString() + TreeUtils.NODE_FILTER_KEY_SUFFIX, null);
		TreeUtils.nodeTestProjectMap.put(userId.toString() + TreeUtils.PROJECT_FILTER_KEY_SUFFIX, null);
		//		RiaFrameworkUtils.setTcExeTypeFromSession(httpSession, null);
	}

	public Map<Integer, List<Long>> getFilterChildIds(List<Long> parentIds, TestCasesQueryDto testCasesQueryDto) {
		Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();

		List<Long> childIds = new ArrayList<Long>();
		List<Long> childFileIds = new ArrayList<Long>();

		List<Long> childIds_ = new ArrayList<Long>();
		List<Long> childFileIds_ = new ArrayList<Long>();
		if (null != parentIds && parentIds.size() > 0) {
			childFileIds.addAll(parentIds);//原父节点同时返回
			while (true) {
				//叶子节点
				childIds_ = testCasesMng.getFilterTcIds(parentIds, testCasesQueryDto);
				childIds.addAll((null != childIds_) ? childIds_ : new ArrayList<Long>());

				//父节点
				childFileIds_ = nodesHierarchyMng.getChildFileIds(parentIds);
				childFileIds.addAll((null != childFileIds_) ? childFileIds_ : new ArrayList<Long>());

				parentIds = childFileIds_;
				if (null == parentIds || parentIds.size() == 0) {
					break;
				}
			}

		}
		nodeMap.put(TreeUtils.CHILD_NODE, childIds);
		nodeMap.put(TreeUtils.FATHER_NODE, childFileIds);
		return nodeMap;
	}

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}
}
