package com.bill99.testmp.testmanage.domain.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.wc.SVNFileUtil;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.bill99.golden.inf.sso.util.CM;
import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.common.utils.SvnUtils;
import com.bill99.testmp.testmanage.domain.SvnService;
import com.bill99.testmp.testmanage.orm.entity.SvnChangeItem;

public class SvnServiceImpl implements SvnService {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public boolean createSvnPath(String svnUrl, String svnPath, String svnUserName, String svnPassword, String releaseId, String effectApp) {
		String logMessage = "ID:" + releaseId + "\neffect:" + effectApp + "\ndescription:系统自动创建目录\nbuildMark:false";
		return createSvnPath(svnUrl, svnPath, svnUserName, svnPassword, logMessage);
	}

	public boolean createSvnPath(String svnUrl, String svnPath, String svnUserName, String svnPassword, String logMessage) {
		if (CM.stringIsEmpty(svnPath) || svnPath.trim().equals("/"))
			return true;
		logger.info(logMessage);
		logger.info("user:" + svnUserName);
		logger.info("pwd:" + svnPassword);
		try {
			logger.debug("svnUrl=" + svnUrl + " svnPath=" + svnPath);
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(svnPath, -1);
			if (nodeKind == SVNNodeKind.DIR) {
				logger.info("the dir exists now:svnUrl=" + svnUrl + " path=" + svnPath);
				return true;
			} else if (nodeKind == SVNNodeKind.FILE) {
				logger.error("error:the node is file:svnUrl=" + svnUrl + " path=" + svnPath);
				return false;
			} else if (nodeKind == SVNNodeKind.NONE) {
				String[] pt = CM.splitString(svnPath, "/", false);
				String path = "";
				String parentDir = "";
				for (int i = 0; i < pt.length; i++) {
					if (i > 0)
						path += "/";
					path += pt[i];
					if (i > 1)
						parentDir += "/";
					if (i > 0)
						parentDir += pt[i - 1];
					nodeKind = repository.checkPath(path, -1);
					if (nodeKind == SVNNodeKind.DIR) {
						continue;
					} else if (nodeKind == SVNNodeKind.FILE) {
						logger.error("error:the node is file:svnUrl=" + svnUrl + " path=" + path);
						return false;
					} else if (nodeKind == SVNNodeKind.NONE) {
						ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);
						editor.openRoot(-1);
						if (parentDir.length() > 0)
							editor.openDir(parentDir, -1);

						editor.addDir(path, null, -1);
						editor.closeDir();
						if (parentDir.length() > 0)
							editor.closeDir();
						editor.closeDir();
						SVNCommitInfo cinfo = editor.closeEdit();
						if (cinfo == null) {
							repository.closeSession();
							return false;
						}
					}
				}

				repository.closeSession();
				return true;
			} else {
				repository.closeSession();
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}

	}

	public boolean checkSvnFile(String svnPath, String fileName, String svnUserName, String svnPassword) {
		if (CM.stringIsEmpty(svnPath) || svnPath.trim().equals("/"))
			return true;
		logger.debug("user:" + svnUserName);
		logger.debug("pwd:" + svnPassword);
		if (StringUtils.isBlank(fileName)) {
			int n = svnPath.lastIndexOf("/");
			if (n > 0) {
				fileName = svnPath.substring(n + 1);
				svnPath = svnPath.substring(0, n);
			}
		}
		try {
			logger.debug("svnPath=" + svnPath + " fileName=" + fileName);
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnPath));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(fileName, -1);
			boolean bf = false;
			if (nodeKind == SVNNodeKind.FILE) {
				bf = true;
			}
			repository.closeSession();
			return bf;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	public long getSvnLatestRevision(String svnUrl, String svnUserName, String svnPassword) {
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);
			long maxS = repository.getLatestRevision();
			repository.closeSession();
			return maxS;
		} catch (SVNException e) {
			logger.error(e.getMessage(), e);
			return Long.MAX_VALUE;
		}
	}

	public long getMaxSvnRevision(String svnUrl, String svnPath, String svnUserName, String svnPassword, long startRevision) {
		try {
			logger.debug("getMaxSvnRevision:svnUrl=" + svnUrl + " svnPath=" + svnPath);
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);
			long maxS = repository.getLatestRevision();
			if (startRevision >= maxS)
				return maxS;
			long maxRevision = -1;
			SVNNodeKind nodeKind = repository.checkPath(svnPath, -1);
			if (nodeKind == SVNNodeKind.DIR) {
				Collection cc = repository.log(new String[] { svnPath }, null, startRevision, -1, false, true);
				for (Iterator it = cc.iterator(); it.hasNext();) {
					SVNLogEntry logEntry = (SVNLogEntry) it.next();
					if (logEntry.getRevision() > maxRevision)
						maxRevision = logEntry.getRevision();
				}
				repository.closeSession();
				return maxRevision;
			} else {
				repository.closeSession();
				logger.error("getMaxSvnRevision---the path error:svnUrl=" + svnUrl + " path=" + svnPath + " nodeKind=" + nodeKind.toString());
				return -1;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1;
		}
	}

	public List<SvnChangeItem> getChangeSetFromSVN(String svnUrl, String svnPath, String svnUserName, String svnPassword, boolean includeAll, long startRevision, long endRevision) {
		try {
			logger.debug("getChangeSetFromSVN:svnUrl=" + svnUrl + " svnPath=" + svnPath);
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(svnPath, -1);
			if (nodeKind == SVNNodeKind.DIR) {
				List<SvnChangeItem> list = new ArrayList<SvnChangeItem>();
				long maxRV = repository.getLatestRevision();
				if (startRevision > maxRV) {
					repository.closeSession();
					return list;
				}
				int n = svnPath.indexOf("PMD/");
				String path = null;
				if (n >= 0) {
					path = svnPath.substring(n);
				}
				Collection cc = repository.log(new String[] { svnPath }, null, startRevision, endRevision, true, true);
				for (Iterator it = cc.iterator(); it.hasNext();) {
					SVNLogEntry logEntry = (SVNLogEntry) it.next();
					logger.debug("---------------------------------------------");
					logger.debug("revision: " + logEntry.getRevision());
					logger.debug("author: " + logEntry.getAuthor());
					logger.debug("date: " + logEntry.getDate());
					logger.debug("log message: " + logEntry.getMessage());

					if (logEntry.getChangedPaths().size() > 0) {
						logger.debug("");
						logger.debug("changed paths:");
						Set changedPathsSet = logEntry.getChangedPaths().keySet();

						for (Iterator changedPaths = changedPathsSet.iterator(); changedPaths.hasNext();) {
							SVNLogEntryPath entryPath = (SVNLogEntryPath) logEntry.getChangedPaths().get(changedPaths.next());
							logger.debug(" " + entryPath.getType() + " " + entryPath.getPath()
									+ ((entryPath.getCopyPath() != null) ? " (from " + entryPath.getCopyPath() + " revision " + entryPath.getCopyRevision() + ")" : ""));
							if (includeAll || svnUserName.equals(logEntry.getAuthor())) {
								SvnChangeItem si = new SvnChangeItem();
								si.setAuthor(logEntry.getAuthor());
								si.setRevision(logEntry.getRevision());
								si.setLogMsg(logEntry.getMessage());
								si.setSubmitTime(logEntry.getDate());
								si.setOpType(entryPath.getType());
								si.setFilePath(entryPath.getPath());
								if (path != null) {
									if (si.getFilePath().indexOf(path) < 0)
										continue;
								}
								nodeKind = repository.checkPath(entryPath.getPath(), logEntry.getRevision());
								si.setNodeType(nodeKind.toString());
								if (mergerSvnChangeItem(list, si) == false) {
									list.add(si);
								}
							}
						}
					}
				}
				repository.closeSession();
				return list;
			} else {
				repository.closeSession();
				logger.error("the path error:svnUrl=" + svnUrl + " path=" + svnPath + " nodeKind=" + nodeKind.toString());
				return null;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	private boolean mergerSvnChangeItem(List<SvnChangeItem> list, SvnChangeItem target) {
		for (int i = 0; i < list.size(); i++) {
			SvnChangeItem si = list.get(i);
			if (si.getFilePath().equals(target.getFilePath())) {
				if (target.getRevision() > si.getRevision()) {
					si.copyFrom(target);
				}
				return true;
			}
		}
		return false;
	}

	public boolean mergeSvnFile(List<SvnChangeItem> fromSvnList, String fromSvnUrl, String toSvnUrl, String svnUserName, String svnPassword, String fromApp) {
		int tn = toSvnUrl.indexOf("/PMD/");
		if (tn < 1) {
			return false;
		}
		if (fromSvnList.size() < 1)
			return true;

		Collections.sort(fromSvnList);

		// 处理检查是否已经被删除，如果已经被删除则可以忽略，不做处理
		List<SvnChangeItem> lt = new ArrayList<SvnChangeItem>();
		for (int i = 0; i < fromSvnList.size(); i++) {
			SvnChangeItem si = fromSvnList.get(i);
			boolean isOK = true;
			for (int k = i + 1; k < fromSvnList.size(); k++) {
				SvnChangeItem d = fromSvnList.get(k);
				if (d.getOpType() == SvnChangeItem.TYPE_DELETED) {
					if (si.getFilePath().startsWith(d.getFilePath())) {
						isOK = false;
						break;
					}
				}
			}
			if (isOK) {
				lt.add(si);
			}
		}

		fromSvnList = lt;

		try {
			long start = System.currentTimeMillis();

			FSRepositoryFactory.setup();
			SVNRepository fromRS = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(fromSvnUrl));
			ISVNAuthenticationManager authManager1 = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			fromRS.setAuthenticationManager(authManager1);

			SVNRepository toRS = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(toSvnUrl));
			ISVNAuthenticationManager authManager2 = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			toRS.setAuthenticationManager(authManager2);

			SvnChangeItem sci = fromSvnList.get(0);
			String logMsg = sci.getLogMsg();

			logger.info("merge cdoe: size=" + fromSvnList.size());

			// 处理删除和路径检查和创建工作
			List<svnTempItem> createPathLt = new ArrayList<svnTempItem>();
			List<svnTempItem> deletePathLt = new ArrayList<svnTempItem>();

			for (SvnChangeItem si : fromSvnList) {
				String newFilePath = SvnUtils.leftTrimPath(si.getNewFilePath(fromApp));
				SVNNodeKind toKind = toRS.checkPath(newFilePath, -1);
				if (si.getOpType() == SvnChangeItem.TYPE_DELETED && toKind != SVNNodeKind.NONE) {
					// 删除
					svnTempItem s = new svnTempItem();
					s.path = newFilePath;
					s.si = si;
					if (deletePathLt.contains(s) == false)
						deletePathLt.add(s);
				}
			}

			for (svnTempItem sti : deletePathLt) {
				ISVNEditor editorD = toRS.getCommitEditor(logMsg, null /* locks */, true /* keepLocks */, null /* mediator */);
				logger.info("delete entry:entry=" + sti.path + " sid=" + sti.si.getId());
				editorD.openRoot(-1);
				editorD.deleteEntry(sti.path, -1);
				editorD.closeDir();
				sti.si.setProcessed(true);
				long st = System.currentTimeMillis();
				SVNCommitInfo cinfo = editorD.closeEdit();
				long ed = System.currentTimeMillis();
				logger.info("delete entry:::::use time=" + (ed - st) + "ms");
				if (cinfo == null) {
					fromRS.closeSession();
					toRS.closeSession();
					return false;
				} else {
					sti.si.setNewRevision(cinfo.getNewRevision());
					sti.si.setNewAuthor(cinfo.getAuthor());
				}
			}

			int nn = 1;
			for (SvnChangeItem si : fromSvnList) {
				SVNNodeKind fromKind = fromRS.checkPath(si.getFilePath(), si.getRevision());
				String newFilePath = SvnUtils.leftTrimPath(si.getNewFilePath(fromApp));
				String newDirPath = si.getDirPath(newFilePath);
				SVNNodeKind toKind = toRS.checkPath(newFilePath, -1);
				logger.info((nn++) + " " + si.getOpType() + "--newPath=" + newFilePath + " newDir=" + newDirPath + " ---oldPath=" + si.getFilePath() + " toKind=" + toKind
						+ " fileName=" + si.getFileName() + " revision=" + si.getRevision() + " sid=" + si.getId());
				if (toKind == SVNNodeKind.NONE && CM.stringIsNotEmpty(newDirPath) && si.getOpType() != SvnChangeItem.TYPE_DELETED) {
					// 创建目录
					String[] pt = CM.splitString(newDirPath, "/", false);
					String path = "";
					String parentDir = "";
					for (int i = 0; i < pt.length; i++) {
						if (i > 0)
							path += "/";
						path += pt[i];
						if (i > 1)
							parentDir += "/";
						if (i > 0)
							parentDir += pt[i - 1];
						SVNNodeKind nodeKind = toRS.checkPath(path, -1);
						if (nodeKind == SVNNodeKind.DIR) {
							continue;
						} else if (nodeKind == SVNNodeKind.FILE) {
							logger.error("error:the node is file:svnUrl=" + toSvnUrl + " path=" + path);
							return false;
						} else if (nodeKind == SVNNodeKind.NONE) {
							svnTempItem s = new svnTempItem();
							s.parentDir = parentDir;
							s.path = path;
							s.si = si;
							if (createPathLt.contains(s) == false)
								createPathLt.add(s);
						}
					}
					if (fromKind == SVNNodeKind.DIR) {
						si.setProcessed(true);
					}
				}
			}

			for (svnTempItem sti : createPathLt) {
				ISVNEditor editor1 = toRS.getCommitEditor(logMsg, null /* locks */, true /* keepLocks */, null /* mediator */);
				editor1.openRoot(-1);
				if (sti.parentDir.length() > 0)
					editor1.openDir(sti.parentDir, -1);
				editor1.addDir(sti.path, null, -1);
				editor1.closeDir();
				if (sti.parentDir.length() > 0)
					editor1.closeDir();
				editor1.closeDir();
				long st = System.currentTimeMillis();
				SVNCommitInfo cinfo = editor1.closeEdit();
				long ed = System.currentTimeMillis();
				logger.info("create path:::::use time=" + (ed - st) + "ms  path=" + sti.path + " sid=" + sti.si.getId());
				if (cinfo == null) {
					fromRS.closeSession();
					toRS.closeSession();
					return false;
				} else {
					sti.si.setNewRevision(cinfo.getNewRevision());
					sti.si.setNewAuthor(cinfo.getAuthor());
				}
			}

			// 合并文件之前的准备工作
			for (SvnChangeItem si : fromSvnList) {
				if (si.isProcessed())
					continue;
				SVNNodeKind fromKind = fromRS.checkPath(si.getFilePath(), si.getRevision());
				String newFilePath = SvnUtils.leftTrimPath(si.getNewFilePath(fromApp));
				String newDirPath = si.getDirPath(newFilePath);
				SVNNodeKind toKind = toRS.checkPath(newFilePath, -1);
				if (fromKind == SVNNodeKind.FILE) {
					si.setNewFile(true);
					if (toKind == SVNNodeKind.FILE) {
						ByteArrayOutputStream oldbs = new ByteArrayOutputStream();
						SVNProperties fpOld = new SVNProperties();
						toRS.getFile(newFilePath, -1, fpOld, oldbs);
						si.toOldData = oldbs.toByteArray();
						si.setNewFile(false);
					}
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					SVNProperties fileProperties = new SVNProperties();
					fromRS.getFile(si.getFilePath(), si.getRevision(), fileProperties, baos);
					si.fromData = baos.toByteArray();
				} else if (fromKind == SVNNodeKind.DIR) {
					si.setProcessed(true);
					continue;
				} else {
					logger.error(si.getOpType() + "-----omitted:fromKind=" + fromKind + " toKind=" + toKind + " oldPath=" + si.getFilePath() + " newPath=" + newFilePath
							+ " newDir=" + newDirPath + " fileName=" + si.getFileName() + " revision=" + si.getRevision());
					si.setProcessed(true);
				}
			}

			int no = 0;
			for (SvnChangeItem si : fromSvnList) {
				if (si.isProcessed())
					continue;
				String newFilePath = SvnUtils.leftTrimPath(si.getNewFilePath(fromApp));
				String newDirPath = si.getDirPath(newFilePath);
				String fileName = si.getFileName();
				no++;
				logger.debug(no + " " + si.getOpType() + "--newPath=" + newFilePath + " oldPath=" + si.getFilePath() + " newDir=" + newDirPath + " fileName=" + fileName
						+ " revision=" + si.getRevision() + " nodeType=" + si.getNodeType());
				ISVNEditor editor2 = toRS.getCommitEditor(logMsg, null /* locks */, true /* keepLocks */, null /* mediator */);
				editor2.openRoot(-1);
				if (CM.stringIsNotEmpty(newDirPath))
					editor2.openDir(newDirPath, -1);
				if (si.isNewFile()) {
					editor2.addFile(newFilePath, null, -1);
				} else {
					editor2.openFile(newFilePath, -1);
				}
				editor2.applyTextDelta(newFilePath, null);
				SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
				String checksum = "";
				if (si.isNewFile()) {
					checksum = deltaGenerator.sendDelta(newFilePath, si.fromData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(si.fromData), editor2, true);
				} else {
					checksum = deltaGenerator.sendDelta(newFilePath, si.toOldData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(si.toOldData), 0,
							si.fromData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(si.fromData), editor2, true);
				}
				editor2.closeFile(newFilePath, checksum);
				if (CM.stringIsNotEmpty(newDirPath))
					editor2.closeDir();
				editor2.closeDir();
				long st = System.currentTimeMillis();
				SVNCommitInfo cinfo = editor2.closeEdit();
				long ed = System.currentTimeMillis();
				logger.info("process file:::::use time=" + (ed - st) + "ms");
				if (cinfo == null) {
					fromRS.closeSession();
					toRS.closeSession();
					return false;
				} else {
					si.setNewRevision(cinfo.getNewRevision());
					si.setNewAuthor(cinfo.getAuthor());
				}
			}

			long end = System.currentTimeMillis();
			logger.info("code merged OK!!!!!!!!!! use total time=" + (end - start) + "ms");

			fromRS.closeSession();
			toRS.closeSession();
			return true;
		} catch (SVNException e) {
			logger.error(e.getMessage(), e);
			return false;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	public SVNCommitInfo uploadFileToSvn(String svnUrl, String svnPath, String fileName, byte[] fileData, String svnUserName, String svnPassword, String logMessage) {
		logger.debug("svnUrl=" + svnUrl + " svnPath=" + svnPath + " fileName=" + fileName);
		if (CM.stringIsEmpty(fileName)) {
			return null;
		}
		if (createSvnPath(svnUrl, svnPath, svnUserName, svnPassword, logMessage) == false) {
			logger.error("svnPath error!!!!");
			return null;
		}
		svnPath = SvnUtils.leftTrimPath(svnPath);
		String newSvnUrl = SvnUtils.concatPath(svnUrl, svnPath);
		logger.debug("newSvnUrl=" + newSvnUrl);
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(newSvnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(fileName, -1);
			boolean isNewFile = true;
			byte[] oldData = null;
			if (nodeKind == SVNNodeKind.NONE) {
				logger.error("not find file:svnPath=" + newSvnUrl + " fileName=" + fileName);
			} else if (nodeKind == SVNNodeKind.DIR) {
				logger.error("this is not a file but a directory:svnPath=" + newSvnUrl + " fileName=" + fileName);
				return null;
			} else if (nodeKind == SVNNodeKind.FILE) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				SVNProperties fileProperties = new SVNProperties();
				repository.getFile(fileName, -1, fileProperties, baos);
				oldData = baos.toByteArray();
				isNewFile = false;
			}
			logger.debug("isNewFile:" + isNewFile + " fileName:" + fileName);

			ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);

			editor.openRoot(-1);
			if (isNewFile) {
				editor.addFile(fileName, null, -1);
			} else {
				editor.openFile(fileName, -1);
			}
			editor.applyTextDelta(fileName, null);
			SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
			String checksum = "";
			if (isNewFile) {
				checksum = deltaGenerator.sendDelta(fileName, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData), editor, true);
			} else {
				checksum = deltaGenerator.sendDelta(fileName, new ByteArrayInputStream(oldData), 0, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData),
						editor, true);
			}
			editor.closeFile(fileName, checksum);
			editor.closeDir();
			SVNCommitInfo cinfo = editor.closeEdit();
			repository.closeSession();
			logger.info("upload file OK! newVersion=" + cinfo.getNewRevision() + "  author=" + cinfo.getAuthor());
			return cinfo;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public SVNCommitInfo addFileToSvn(String svnUrl, String svnPath, String fileName, byte[] fileData, String svnUserName, String svnPassword, String logMessage) {
		logger.debug("svnUrl=" + svnUrl + " svnPath=" + svnPath + " fileName=" + fileName);
		if (CM.stringIsEmpty(fileName)) {
			return null;
		}
		if (createSvnPath(svnUrl, svnPath, svnUserName, svnPassword, logMessage) == false) {
			logger.error("svnPath error!!!!");
			return null;
		}
		svnPath = SvnUtils.leftTrimPath(svnPath);
		String newSvnUrl = SvnUtils.concatPath(svnUrl, svnPath);
		logger.debug("newSvnUrl=" + newSvnUrl);
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(newSvnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			//			SVNNodeKind nodeKind = repository.checkPath(fileName, -1);
			boolean isNewFile = true;
			byte[] oldData = null;

			//			if (nodeKind == SVNNodeKind.NONE) {
			//				logger.error("not find file:svnPath=" + newSvnUrl + " fileName=" + fileName);
			//			} else if (nodeKind == SVNNodeKind.DIR) {
			//				logger.error("this is not a file but a directory:svnPath=" + newSvnUrl + " fileName=" + fileName);
			//				return null;
			//			} else if (nodeKind == SVNNodeKind.FILE) {
			//				ByteArrayOutputStream baos = new ByteArrayOutputStream();
			//				SVNProperties fileProperties = new SVNProperties();
			//				repository.getFile(fileName, -1, fileProperties, baos);
			//				oldData = baos.toByteArray();
			//				isNewFile = false;
			//			}
			logger.debug("isNewFile:" + isNewFile + " fileName:" + fileName);

			ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);

			editor.openRoot(-1);
			if (isNewFile) {
				editor.addFile(fileName, null, -1);
			} else {
				editor.openFile(fileName, -1);
			}
			editor.applyTextDelta(fileName, null);
			SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
			String checksum = "";
			if (isNewFile) {
				checksum = deltaGenerator.sendDelta(fileName, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData), editor, true);
			} else {
				checksum = deltaGenerator.sendDelta(fileName, new ByteArrayInputStream(oldData), 0, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData),
						editor, true);
			}
			editor.closeFile(fileName, checksum);
			editor.closeDir();
			SVNCommitInfo cinfo = editor.closeEdit();
			repository.closeSession();
			logger.info("add file OK! newVersion=" + cinfo.getNewRevision() + "  author=" + cinfo.getAuthor());
			return cinfo;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public byte[] getFileFromSvn(String svnUrl, String svnPath, String fileName, String svnUserName, String svnPassword) {
		logger.debug("svnUrl=" + svnUrl + " svnPath=" + svnPath + " fileName=" + fileName);

		svnPath = SvnUtils.leftTrimPath(svnPath);
		String newFilePath = SvnUtils.concatPath(svnPath, fileName);
		logger.debug("newFilePath=" + newFilePath);
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(newFilePath, -1);
			byte[] oldData = null;
			if (nodeKind == SVNNodeKind.NONE) {
				logger.error("not find file:svnPath=" + svnPath + " fileName=" + newFilePath);
			} else if (nodeKind == SVNNodeKind.DIR) {
				logger.error("this is not a file but a directory:svnPath=" + svnPath + " fileName=" + newFilePath);
				return null;
			} else if (nodeKind == SVNNodeKind.FILE) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				SVNProperties fileProperties = new SVNProperties();
				repository.getFile(newFilePath, -1, fileProperties, baos);
				oldData = baos.toByteArray();
			}
			repository.closeSession();
			return oldData;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public List<SVNDirEntry> getSubEntry(String svnUrl, String svnPath, String svnUserName, String svnPassword) {
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);
			Collection entries = repository.getDir(svnPath, -1, null, (Collection) null);
			Iterator iterator = entries.iterator();
			List<SVNDirEntry> list = new ArrayList<SVNDirEntry>();
			while (iterator.hasNext()) {
				SVNDirEntry entry = (SVNDirEntry) iterator.next();
				list.add(entry);
			}
			repository.closeSession();
			return list;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public boolean deleteSvnFile(String svnUrl, String svnPath, String fileName, String svnUserName, String svnPassword, String logMessage) {
		logger.debug("deleteSvnFile:::svnUrl=" + svnUrl + " svnPath=" + svnPath + " fileName=" + fileName);
		svnPath = SvnUtils.leftTrimPath(svnPath);
		String newFilePath = SvnUtils.concatPath(svnPath, fileName);
		if (CM.stringIsEmpty(newFilePath)) {
			return false;
		}
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);

			SVNNodeKind nodeKind = repository.checkPath(newFilePath, -1);
			if (nodeKind == SVNNodeKind.NONE) {
				logger.info("not find file:svnPath=" + svnPath + " fileName=" + newFilePath);
				return true;
			} else {
				logger.debug("delete file===" + newFilePath);
				ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);
				editor.openRoot(-1);
				editor.deleteEntry(newFilePath, -1);
				editor.closeDir();
				SVNCommitInfo cinfo = editor.closeEdit();
				if (cinfo == null) {
					repository.closeSession();
					return false;
				}
			}
			repository.closeSession();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	public boolean uploadThirdJar(String svnUrl, List<String> svnPathList, String org, String fileName, String svnUserName, String svnPassword, byte[] fileData,
			StringBuilder errorInfo) {
		logger.debug("svnUrl=" + svnUrl + " org=" + org + " fileName=" + fileName);
		if (CM.stringIsEmpty(fileName)) {
			return false;
		}
		if (svnPathList == null || svnPathList.size() < 1) {
			errorInfo.append("svn路径没有配置！");
			return false;
		}
		try {
			FSRepositoryFactory.setup();
			SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(svnUrl));
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(svnUserName, svnPassword);
			repository.setAuthenticationManager(authManager);
			String logMessage = "工程化平台自动上传第三方JAR包！";

			for (String path : svnPathList) {
				String svnPath = SvnUtils.leftTrimPath(path);
				String newFilePath = SvnUtils.concatPath(svnPath, org);
				SVNNodeKind nodeKind = repository.checkPath(newFilePath, -1);
				if (nodeKind == SVNNodeKind.DIR) {
					// 找到路径
					String newFileName = newFilePath + "/" + fileName;
					nodeKind = repository.checkPath(newFileName, -1);
					if (nodeKind == SVNNodeKind.NONE || nodeKind == SVNNodeKind.UNKNOWN) {
						ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);

						editor.openRoot(-1);
						editor.openDir(newFilePath, -1);
						editor.addFile(newFileName, null, -1);
						editor.applyTextDelta(newFileName, null);
						SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
						String checksum = deltaGenerator.sendDelta(newFileName, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData), editor, true);
						editor.closeFile(newFileName, checksum);
						editor.closeDir();
						editor.closeDir();
						SVNCommitInfo cinfo = editor.closeEdit();
						if (cinfo != null) {
							repository.closeSession();
							return true;
						} else {
							repository.closeSession();
							errorInfo.append("上传到SVN失败！");
							return false;
						}
					} else {
						repository.closeSession();
						errorInfo.append("该文件在SVN中已经存在！");
						return false;
					}
				}
			}
			// 没有找到路径，则新建路径
			String svnPath = SvnUtils.leftTrimPath(svnPathList.get(0));
			String newFilePath = SvnUtils.concatPath(svnPath, org);

			ISVNEditor editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);
			editor.openRoot(-1);
			editor.openDir(svnPath, -1);
			editor.addDir(newFilePath, null, -1);
			editor.closeDir();
			editor.closeDir();
			editor.closeDir();
			SVNCommitInfo cinfo = editor.closeEdit();
			if (cinfo == null) {
				repository.closeSession();
				errorInfo.append("创建路径失败");
				return false;
			}

			editor = repository.getCommitEditor(logMessage, null /* locks */, true /* keepLocks */, null /* mediator */);
			String newFileName = newFilePath + "/" + fileName;
			editor.openRoot(-1);
			editor.openDir(newFilePath, -1);
			editor.addFile(newFileName, null, -1);
			editor.applyTextDelta(newFileName, null);
			SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
			String checksum = deltaGenerator.sendDelta(newFileName, fileData == null ? SVNFileUtil.DUMMY_IN : new ByteArrayInputStream(fileData), editor, true);
			editor.closeFile(newFileName, checksum);
			editor.closeDir();
			editor.closeDir();
			cinfo = editor.closeEdit();
			if (cinfo == null) {
				repository.closeSession();
				errorInfo.append("创建路径失败");
				return false;
			}

			repository.closeSession();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			errorInfo.append("将文件上传到SVN失败！");
			return false;
		}
	}

	class svnTempItem {
		String parentDir;
		String path;
		SvnChangeItem si;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((path == null) ? 0 : path.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			svnTempItem other = (svnTempItem) obj;
			if (path == null) {
				if (other.path != null)
					return false;
			} else if (!path.equals(other.path))
				return false;
			return true;
		}
	}

	public static void main(String[] args) {
		SvnServiceImpl svnService = new SvnServiceImpl();
		try {
			String svnUrl = "http://svn.99bill.net/opt/99billdoc/PMD/DOC/RM/工作日报";
			String svnPath = "2013";
			String fileName = "1.txt";
			byte[] fileData = FileUtils.readFileToByteArray(new File("d:\\tingting.ao\\桌面\\1.txt"));
			String svnUserName = "tingting.ao@99bill.com";
			String svnPassword = "99bill";
			String logMessage = "qamp_commit_svn";
			svnService.uploadFileToSvn(svnUrl, svnPath, fileName, fileData, svnUserName, svnPassword, logMessage);
			System.out.println("===OK===");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}