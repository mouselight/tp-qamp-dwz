package com.bill99.testmp.testmanage.domain.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.domain.mail.EmailSender;
import com.bill99.riaframework.orm.manager.UserService;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.framework.enums.TestPlanStepEnum;
import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.orm.dao.TestplanStepsDao;
import com.bill99.testmp.testmanage.orm.dao.TestplansDao;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestPlanIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;

public class TestPlanServiceImpl implements TestPlanService {

	private TestplansMng testplansMng;
	private TestplanStepsMng testplanStepsMng;
	private NodesHierarchyMng nodesHierarchyMng;
	private TestIssueMng testIssueMng;
	private TestplanStepsDao testplanStepsDao;
	private TestplansDao testplansDao;
	private TestPlanIbatisDao testPlanIbatisDao;

	private EmailSender outMailSender;
	private UserService userService;

	private TestProjectsMng testProjectsMng;

	//Plan
	public void mergeTestPlans(TestPlansDto plansDto, String currentEmail) {
		Assert.notNull(plansDto.getIssueId());
		Testplans testplans = testplansMng.findByIssueId(plansDto.getIssueId());
		if (null == testplans || null == testplans.getPlanId()) {
			//save-plan
			testplans = new Testplans();
			BeanUtils.copyProperties(plansDto, testplans);
			testplans = testplansMng.save(testplans);
			//merge-step
			mergeTestPlanStep(plansDto, testplans.getPlanId(), currentEmail, testplans.getTeamId());
		} else {
			//update plan - 主键不覆盖.
			BeanUtils.copyProperties(plansDto, testplans, new String[] { "planId" });
			testplans.setUpdateTime(new Date());
			testplans.setUpdateUser(plansDto.getCreateUser());
			testplans = testplansMng.update(testplans);
			//merge-step
			mergeTestPlanStep(plansDto, testplans.getPlanId(), currentEmail, testplans.getTeamId());
		}
		//update - summary
		updatePlanSummary(testplans.getPlanId());
	}

	//Step
	private void mergeTestPlanStep(TestPlansDto plansDto, Long planId, String currentEmail, Long teemId) {
		Assert.notNull(planId);
		List<Long> stepIds = plansDto.getStepIds();
		List<Integer> types = plansDto.getTypes();
		List<Date> planBeginDates = plansDto.getPlanBeginDates();
		List<Date> planEndDates = plansDto.getPlanEndDates();
		List<String> memos = plansDto.getMemos();
		List<String> resources = plansDto.getResources();
		List<String> realnames = plansDto.getRealnames();
		List<Integer> stepStatus = plansDto.getStepStatus();

		Set<String> userIds = new HashSet<String>();

		TestIssue testIssue = testIssueMng.getIssueDetail(plansDto.getIssueId());
		StringBuilder contentSb = new StringBuilder();
		contentSb.append("<table style=\"font: 90% Arial, Helvetica, sans-serif;width:900; text-align:left; border:solid #add9c0; border-width:1px 0px 0px 1px; \" >");
		contentSb.append("<tr>");
		contentSb.append("<td style=\"background:#328aa4;color: #fff; border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px;\">关键字</td>");
		contentSb.append("<td style=\"border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px; \">" + testIssue.getPkey() + "</td>");
		contentSb.append("</tr>");
		contentSb.append("<tr>");
		contentSb.append("<td style=\"background:#328aa4;color: #fff;width:80; border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px;\">测试需求</td>");
		contentSb.append("<td style=\"border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px; \">" + testIssue.getSummary() + "</td>");
		contentSb.append("</tr>");
		contentSb.append("<tr>");
		contentSb.append("<td style=\"background:#328aa4;color: #fff; border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px;\">描述</td>");
		contentSb.append("<td style=\"border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px; \">" + testIssue.getDescription() + "</td>");
		contentSb.append("</tr>");
		TestplanSteps testplanStep = null;
		TestProjects testProjects = null;

		if (null != stepIds && stepIds.size() > 0) {
			for (int i = 0; i < stepIds.size(); i++) {
				//step
				testplanStep = new TestplanSteps();
				testplanStep.setStepId(stepIds.get(i));
				testplanStep.setPlanId(planId);
				testplanStep.setStepNum(i);
				testplanStep.setType(types.get(i));
				testplanStep.setBeginTime(planBeginDates.get(i));
				testplanStep.setEndTime(planEndDates.get(i));
				testplanStep.setMemo(memos.get(i));
				testplanStep.setState(stepStatus.get(i));
				testplanStep.setRealnames(realnames.get(i));
				testplanStep.setUserids(resources.get(i));
				testplanStep.setCreateUser(plansDto.getCreateUser());
				testplanStep.setUnitType("T");
				testPlanIbatisDao.mergeStep(testplanStep);

				if (types.get(i) == 6 || types.get(i) == 8||types.get(i) == 9||types.get(i) == 10) {
					testProjects = testProjectsMng.findByTeemId(teemId);
					int j = 1;
					for (String unitType : testProjects.getUnitType().split(",")) {
						if (!"T".equals(unitType)) {
							testplanStep = new TestplanSteps();
							testplanStep.setStepId(stepIds.get(i) + j);
							testplanStep.setPlanId(stepIds.get(i) + j);
							testplanStep.setType(types.get(i));
							testplanStep.setParentId(stepIds.get(i));
							testplanStep.setUnitType(unitType);
							testPlanIbatisDao.mergeStep(testplanStep);
							j++;
						}
					}
				}

				//step-User
				mergeAssoTestPlanUser(resources.get(i), stepIds.get(i));

				StringBuilder userNamesSb = new StringBuilder("【");
				userNamesSb.append(testplanStep.getRealnames());
				userNamesSb.append("】");

				contentSb.append("<tr>");
				contentSb.append("<td style=\"background:#328aa4;color: #fff;width:120; border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px;\">任务和时间</td>");
				contentSb.append("<td style=\"border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px; \">" + "【" + testplanStep.getTypeName() + "】"
						+ userNamesSb.toString() + "【" + testplanStep.getBeginTime() + " 至 " + testplanStep.getEndTime() + "】</td>");
				contentSb.append("</tr>");

				if (StringUtils.hasLength(testplanStep.getUserids())) {
					userIds.addAll(Arrays.asList(testplanStep.getUserids().split(",")));
				}

			}
			if (userIds.size() > 0) {

				contentSb.append("</table>");
				List<String> recvAddress = new ArrayList<String>();
				List<String> ccAddress = new ArrayList<String>();

				for (String userId : userIds) {
					recvAddress.add(userService.findById(Long.valueOf(userId)).getEmail());
				}
				//				ccAddress.add("poa_qa_tp@99bill.com");

				if (StringUtils.hasLength(currentEmail)) {
					ccAddress.add(currentEmail);
				}
				contentSb.append("<br />请及时登录测试管理平台处理!");

				try {
					outMailSender.sendEmail(recvAddress, ccAddress, "【测试任务通知】-【" + testIssue.getPkey() + "】-" + testIssue.getSummary(), contentSb.toString(), "fxliory@163.com");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	//Step-User
	private void mergeAssoTestPlanUser(String resources, Long stepId) {
		Assert.notNull(stepId);
		if (!StringUtil.isNull(resources)) {
			testPlanIbatisDao.saveAssoStepUser(stepId, StringUtil.strToLongList(resources));
		}
	}

	//获取指定子任务的所有TC父节点
	public List<Long> getAllParentIds(Long planStepId) {
		List<Long> childIds = testplanStepsMng.getTcId(planStepId);
		return nodesHierarchyMng.getAllParentIds(childIds);
	}

	public List<Long> getParentIdsExecution(Long planStepId, Integer executionType, Integer exeStatus) {
		List<Long> childIds = testplanStepsMng.getTcId(planStepId, executionType, exeStatus);
		return nodesHierarchyMng.getAllParentIds(childIds);
	}

	//获取父节点所有的子节点
	public Map<Integer, List<Long>> getAllChildIds(List<Long> parentIds) {
		return nodesHierarchyMng.getAllChildIds(parentIds);
	}

	//指定stepId 获取父节点所有子节点
	public List<Long> getStepAllChilds(List<Long> parentIds, Long stepId) {
		Map<Integer, List<Long>> nodeMap = nodesHierarchyMng.getAllChildIds(parentIds);
		List<Long> allChilds = nodeMap.get(TreeUtils.CHILD_NODE);
		return testPlanIbatisDao.getStepAllChilds(allChilds, stepId);
	}

	//更新测试计划
	public void updateTestPlan(TestPlansDto plansDto) {
		List<Long> stepIds = plansDto.getStepIds();
		List<String> memos = plansDto.getMemos();
		List<String> outputs = plansDto.getOutputs();
		List<Integer> stepStatus = plansDto.getStepStatus();

		if (null != stepIds && stepIds.size() > 0) {
			for (int i = 0; i < stepIds.size(); i++) {
				TestplanSteps planStep = testplanStepsDao.get(stepIds.get(i));
				planStep.setState(stepStatus.get(i));
				planStep.setOutput(outputs.get(i));
				planStep.setMemo(StringUtil.appendContent(planStep.getMemo(), memos.get(i)));
				planStep.setUpdateTime(new Date());
				planStep.setUpdateUser(plansDto.getUpdateUser());
				testplanStepsDao.update(planStep);
			}
		}
		//更新计划主表
		Testplans testplans = testplansDao.get(plansDto.getPlanId());
		testplans.setState(plansDto.getState());
		testplans.setUpdateTime(new Date());
		testplans.setUpdateUser(plansDto.getUpdateUser());
		testplansDao.update(testplans);
	}

	public List<Testplans> getPrivyPlan(Long userId, Long teamId) {
		List<Testplans> testPlanList = new ArrayList<Testplans>();
		//个人的所有计划ID
		List<Long> planIds = testPlanIbatisDao.getPrivyPlanId(userId, teamId);
		//个人的所有计划(包含计划下所有子任务)
		if (null != planIds && planIds.size() > 0) {
			testPlanList = testplansMng.getByIds(planIds);
			for (Testplans testPlan : testPlanList) {
				//需求信息
				testPlan.setTestIssue(testIssueMng.getIssueDetail(testPlan.getIssueId()));
				//各子任务的TC数及完成数
				testplanStepsMng.getAssoTcCount(testPlan.getTestplanSteps());
				//子任务的写权限
				hasWriteAuth(userId, testPlan.getTestplanSteps());
			}
		}
		//所有计划的关联需求
		return testPlanList;
	}

	//验证子任务的修改权限. 未完成且资源包含指定用户
	public void hasWriteAuth(Long userId, Set<TestplanSteps> testplanSteps) {
		if (null != userId && null != testplanSteps && testplanSteps.size() > 0) {
			for (TestplanSteps planStep : testplanSteps) {
				if (!StringUtil.isNull(planStep.getUserids()) && null != planStep.getState() && planStep.getState().intValue() != TestPlanStatusEnum.STATUS_2.getValue().intValue()) {
					List<Long> userIdList = StringUtil.strToLongList(planStep.getUserids());
					planStep.setWriter(userIdList.contains(userId));
				}
			}
		}
	}

	public void updatePlanSummary(Long planId) {
		String summary = "";
		StringBuffer sbf = new StringBuffer();
		List<TestplanSteps> stepList = testPlanIbatisDao.getStepInfoByPlan(planId);
		testplanStepsMng.getAssoTcCount(stepList);
	
		if (null != stepList && stepList.size() > 0) {
			for (TestplanSteps step : stepList) {
				Integer type = step.getType();
				if (null != type
						&& (type.compareTo(TestPlanStepEnum.TEST_PLANN_5.getValue()) == 0 || type.compareTo(TestPlanStepEnum.TEST_PLANN_6.getValue()) == 0 || type
								.compareTo(TestPlanStepEnum.TEST_PLANN_8.getValue()) == 0|| type
								.compareTo(TestPlanStepEnum.TEST_PLANN_9.getValue()) == 0)|| type
								.compareTo(TestPlanStepEnum.TEST_PLANN_10.getValue()) == 0) {
					sbf.append(step.getTypeName()).append("：").append(step.getStateName());
					if (!StringUtil.isNull(step.getCoverPer())) {
						sbf.append(" (").append(step.getCoverPer()).append(")");
					}
					sbf.append("<br>");
				} else {
					sbf.append(step.getTypeName()).append("：").append(step.getStateName()).append("<br>");
				}
			}
			summary = sbf.substring(0, sbf.length() - 4);
		}
		//update
		Testplans testPlans = testplansDao.get(planId);
		testPlans.setFastsummary(summary);
		testplansDao.update(testPlans);
	}

	public void updateSvnUrl(Long stepId, String svnUrl) {
		testPlanIbatisDao.updateSvnUrl(stepId, svnUrl);
	}

	public void setTestplansMng(TestplansMng testplansMng) {
		this.testplansMng = testplansMng;
	}

	public void setTestplanStepsMng(TestplanStepsMng testplanStepsMng) {
		this.testplanStepsMng = testplanStepsMng;
	}

	public void setNodesHierarchyMng(NodesHierarchyMng nodesHierarchyMng) {
		this.nodesHierarchyMng = nodesHierarchyMng;
	}

	public void setTestplanStepsDao(TestplanStepsDao testplanStepsDao) {
		this.testplanStepsDao = testplanStepsDao;
	}

	public void setTestplansDao(TestplansDao testplansDao) {
		this.testplansDao = testplansDao;
	}

	public void setTestPlanIbatisDao(TestPlanIbatisDao testPlanIbatisDao) {
		this.testPlanIbatisDao = testPlanIbatisDao;
	}

	public void setTestIssueMng(TestIssueMng testIssueMng) {
		this.testIssueMng = testIssueMng;
	}

	public void setOutMailSender(EmailSender outMailSender) {
		this.outMailSender = outMailSender;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}
}
