package com.bill99.testmp.testmanage.domain.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.enums.TestPlanStepEnum;
import com.bill99.testmp.framework.utils.PropertiesMapping;
import com.bill99.testmp.testmanage.common.dto.NodesImportDto;
import com.bill99.testmp.testmanage.common.dto.TestCaseCountDto;
import com.bill99.testmp.testmanage.common.utils.XlsUtils;
import com.bill99.testmp.testmanage.domain.TestManageService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestReportIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;

public class TestReportServiceImpl implements TestReportService {

	private JiraIbatisDao jiraIbatisDao;
	private TestReportIbatisDao testReportIbatisDao;
	private JiraMng jiraMng;
	private TestIssueMng testIssueMng;
	private PropertiesMapping propertiesMapping;
	private TestManageService testManageService;
	private TestplanStepsMng testplanStepsMng;
	private TestCaseCountDto testCaseCountDto = new TestCaseCountDto();
	private int q = 2;//定义Sheet顺序   

	//	private List<TestCaseCountDto> testCaseCountDtos = new ArrayList<TestCaseCountDto>();

	public void setTestplanStepsMng(TestplanStepsMng testplanStepsMng) {
		this.testplanStepsMng = testplanStepsMng;
	}

	public void setTestReportIbatisDao(TestReportIbatisDao testReportIbatisDao) {
		this.testReportIbatisDao = testReportIbatisDao;
	}

	public void setTestManageService(TestManageService testManageService) {
		this.testManageService = testManageService;
	}

	public void setJiraMng(JiraMng jiraMng) {
		this.jiraMng = jiraMng;
	}

	public void setTestIssueMng(TestIssueMng testIssueMng) {
		this.testIssueMng = testIssueMng;
	}

	public void setJiraIbatisDao(JiraIbatisDao jiraIbatisDao) {
		this.jiraIbatisDao = jiraIbatisDao;
	}

	public void setPropertiesMapping(PropertiesMapping propertiesMapping) {
		this.propertiesMapping = propertiesMapping;
	}

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private void writeExcel(WritableWorkbook workbook, TestReport testReport) throws Exception {
		try {
			String isMust = testReportIbatisDao.isMustByPlanStep(testReport.getStepId());

			if (isMust != null && isMust.equals("1")) {
				// 必测回归集导出
				tcTestEnviroSheet(workbook, testReport,isMust);
			} else {
				//项目导出
				// sheet2
				bugReportSheet(workbook, testReport);
				// sheet3
				tcTestEnviroSheet(workbook, testReport,null);
				// sheet1
				testReportSheet(workbook, testReport);
			}
			// write
			workbook.write();
		} catch (IOException e) {
			logger.error("-- TestReport , Workbook.Write Error --", e);
			throw new Exception(e.getMessage());
		} catch (WriteException e) {
			logger.error("-- TestReport , Create Writable Error --", e);
			throw new Exception(e.getMessage());
		} finally {
			try {
				if (null != workbook) {
					workbook.close();
				}
			} catch (WriteException e) {
				logger.error("-- TestReport , Workbook.Close Error --", e);
				throw new Exception(e.getMessage());
			} catch (IOException e) {
				logger.error("-- TestReport , Workbook.Close Error --", e);
				throw new Exception(e.getMessage());
			}
		}
	}

	private String getTestReportName(TestReport testReport) throws UnsupportedEncodingException {
		// 文件名
		TestIssue testIssue = testIssueMng.getIssueDetail(testReport.getIssueId());

		// 由上线ID或pkey区分不同文件
		String reportNameKey = StringUtil.isNull(testIssue.getReleaseId()) ? testIssue.getPkey() : testIssue.getReleaseId();

		String filename = null;
		if (testReport.getStepType().compareTo(TestPlanStepEnum.TEST_PLANN_6.getValue()) == 0) {
			filename = DateUtil.getDate("yyyyMMdd") + "-" + URLEncoder.encode(testIssue.getTeamName(), "UTF-8") + "-集成测试报告(" + reportNameKey + ").xls";
		} else if (testReport.getStepType().compareTo(TestPlanStepEnum.TEST_PLANN_8.getValue()) == 0) {
			filename = DateUtil.getDate("yyyyMMdd") + "-" + URLEncoder.encode(testIssue.getTeamName(), "UTF-8") + "-生产验收报告(" + reportNameKey + ").xls";
		} else {
			filename = DateUtil.getDate("yyyyMMdd") + "-" + URLEncoder.encode(testIssue.getTeamName(), "UTF-8") + "-开发测试报告(" + reportNameKey + ").xls";
		}
		return filename;
	}

	public String testReport(HttpServletResponse response, TestReport testReport) throws Exception {
		// 文件路径
				String filePath = propertiesMapping.getReportTempDir() + getTestReportName(testReport);
	//	String filePath = "d://" + getTestReportName(testReport);
		File file = new File(filePath);
		WritableWorkbook workbook = Workbook.createWorkbook(file);
		writeExcel(workbook, testReport);
		return filePath;
	}

	// 测试报告
	private void testReportSheet(WritableWorkbook workbook, TestReport testReport) throws RowsExceededException, WriteException {
		// 创建Sheet
		WritableSheet sheet = workbook.createSheet("测试总表", 0);

		sheet.addCell(new Label(0, 0, "上线项目清单", XlsUtils.getHeadStyle()));
		// 设置Head
		Set<String> setKey = new TestReport().getTestReportMap().keySet();
		Iterator<String> itKey = setKey.iterator();
		// 合并首行
		sheet.mergeCells(0, 0, setKey.size() - 1, 0);

		TestReport issueReport = new TestReport();
		try {
			// 数据库获取
			issueReport = testReportIbatisDao.getIssue(testReport.getIssueId());
			// 页面传入参数
			issueReport.setQaResult(testReport.getQaResult());
			issueReport.setMemo(testReport.getMemo());
			for (int k = 0; itKey.hasNext(); k++) {
				String key = itKey.next().toString();
				// Head
				sheet.addCell(new Label(k, 1, key, XlsUtils.getThStyle()));
				// Content -- 多个上线ID 循环这里 2+i
				if (null != issueReport) {
					sheet.addCell(new Label(k, 2, issueReport.getTestReportMap().get(key), XlsUtils.getContentStyle()));
				}
				// 设置列宽
				sheet.setColumnView(k, 20);
			}
		} catch (Exception e) {
			logger.error("-- get issue error --", e);
		}

		// cycle
		try {
			Map<String, String> cycleMap = jiraMng.queryCycleByIssue(testReport.getIssueId());
			String jiraset = cycleMap.get("jiraset");
			String cycle = cycleMap.get("cycle");
			sheet.addCell(new Label(0, 2, jiraset + " " + cycle, XlsUtils.getContentStyle()));
		} catch (Exception e) {
			logger.error("-- get issue cycle error --", e);
		}

		// QA-info
		// 从关联资源上获取 可能与jira中不一致
		int s = 0;
		try {
			sheet.addCell(new Label(0, 6, "QA验证人员：", XlsUtils.getTileStyle()));
			sheet.mergeCells(0, 6, 2, 6);

			List<TestReport> qaReportList = testReportIbatisDao.getQaInfo(testReport.getStepId());
			Iterator<String> qaKey = new TestReport().getQaReportMap().keySet().iterator();

			for (int k = 0; qaKey.hasNext(); k++) {
				String key = (String) qaKey.next();
				// Head
				sheet.addCell(new Label(k, 7, key, XlsUtils.getThStyle()));

				if (null != qaReportList && qaReportList.size() > 0) {
					for (; s < qaReportList.size(); s++) {
						sheet.addCell(new Label(k, 8 + s, qaReportList.get(s).getQaReportMap().get(key), XlsUtils.getContentStyle()));
					}
				}
			}
		} catch (Exception e) {
			logger.error("-- get qa info error --", e);
		}
		sheet.addCell(new Label(0, s + 12, "Case总数", XlsUtils.getThStyle()));
		sheet.addCell(new Label(1, s + 12, "实际执行数", XlsUtils.getThStyle()));
		sheet.addCell(new Label(2, s + 12, "执行通过", XlsUtils.getThStyle()));
		sheet.addCell(new Label(3, s + 12, "执行失败", XlsUtils.getThStyle()));
		sheet.addCell(new Label(4, s + 12, "未执行", XlsUtils.getThStyle()));
		sheet.addCell(new Label(5, s + 12, "通过率", XlsUtils.getThStyle()));
		sheet.addCell(new Label(6, s + 12, "覆盖率", XlsUtils.getThStyle()));

		sheet.addCell(new Label(0, s + 13, testCaseCountDto.getAllCont(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(1, s + 13, testCaseCountDto.getDoneCnt(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(2, s + 13, testCaseCountDto.getSuccCnt(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(3, s + 13, testCaseCountDto.getFailCnt(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(4, s + 13, testCaseCountDto.getUndoneCnt(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(5, s + 13, testCaseCountDto.getPassPer(), XlsUtils.getContentStyle()));
		sheet.addCell(new Label(6, s + 13, testCaseCountDto.getCoverPer(), XlsUtils.getContentStyle()));
		// 自定义列宽
		sheet.setColumnView(0, 25);
		sheet.setColumnView(1, 40);
		sheet.setColumnView(2, 25);
	}

	// bug report
	private void bugReportSheet(WritableWorkbook workbook, TestReport testReport) throws RowsExceededException, WriteException {
		// 创建Sheet
		WritableSheet sheet = workbook.createSheet("缺陷清单", 1);
		sheet.addCell(new Label(0, 0, "BUG列表", XlsUtils.getHeadStyle()));

		// 设置Head
		Set<String> setKey = new TestReport().getBugReportMap().keySet();
		Iterator<String> itKey = setKey.iterator();
		// 合并首行
		sheet.mergeCells(0, 0, setKey.size() - 1, 0);
		// Content
		List<TestReport> testReportList = jiraIbatisDao.getBugList(testReport.getIssueId());
		for (int k = 0; itKey.hasNext(); k++) {
			sheet.setColumnView(k, 15);// 设置列宽
			String key = itKey.next().toString();
			sheet.addCell(new Label(k, 1, key, XlsUtils.getThStyle()));
			if (null != testReportList && testReportList.size() > 0) {
				for (int i = 0; i < testReportList.size(); i++) {
					sheet.addCell(new Label(k, 2 + i, testReportList.get(i).getBugReportMap().get(key), XlsUtils.getContentStyle()));
				}
			}
		}
		sheet.setColumnView(1, 20);// 设置列宽
		sheet.setColumnView(4, 50);// 设置列宽
		sheet.setColumnView(6, 20);// 设置列宽
		sheet.setColumnView(7, 20);// 设置列宽
	}

	public void tcTestEnviroSheet(WritableWorkbook workbook, TestReport testReport,String isMust) throws RowsExceededException, WriteException {
		Integer allCont = 0; //Case总数
		Integer doneCnt = 0;//实际执行数
		Integer succCnt = 0; //执行通过
		Integer failCnt = 0;//执行失败
		Integer undoneCnt = 0; //未执行

		String tester = "";

		Map<Long, String> testEnvSteps = testplanStepsMng.getEnvStep(testReport.getStepId());
		if (testEnvSteps != null) {

			for (Entry<Long, String> entry : testEnvSteps.entrySet()) {
				Long envStepId = Long.valueOf(entry.getKey().toString());
				String envType = entry.getValue().toString();
				List<TestProjects> testGroups = testManageService.getProjectGroup(envStepId);
				if (testGroups != null) {
					Collections.sort(testGroups, new Comparator<TestProjects>() {
						public int compare(TestProjects arg0, TestProjects arg1) {
							return arg0.getPrefix().compareTo(arg1.getPrefix());
						}
					});
					for (TestProjects testGroup : testGroups) {
						 WritableSheet sheet;
						int d = 2;
                       if(isMust!=null&&isMust.equals(1)){
                    	// 创建必测Sheet
                    	    sheet = workbook.createSheet(envType + "@" + testGroup.getPrefix()+"必测回归集", q);
                       }else{
                    	// 创建Sheet
   						 sheet = workbook.createSheet(envType + "@" + testGroup.getPrefix(), q);
                       }
						
						q++;
						sheet.addCell(new Label(0, 0, "Total Testing Results", XlsUtils.getHeadStyle()));
						// 设置Head
						Set<String> setKey_1 = new TestReport().getTcStatisReportMap().keySet();
						Iterator<String> itKey_1 = setKey_1.iterator();

						// 合并首行
						sheet.mergeCells(0, 0, setKey_1.size() - 1, 0);

						// Tc统计信息 Content
						try {

							List<TestReport> tcStatiss = testReportIbatisDao.getCaseStatis(envStepId, testGroup.getId());
							//测试数量统计表head
							for (int k = 0; itKey_1.hasNext(); k++) {
								String key = itKey_1.next();
								sheet.addCell(new Label(k, 1, key, XlsUtils.getThStyle()));
							}
							//测试数量统计表 body

							for (TestReport tcStatis : tcStatiss) {
								Iterator<String> itKey_3 = setKey_1.iterator();
								for (int k = 0; itKey_3.hasNext(); k++) {
									String key = itKey_3.next();

									sheet.addCell(new Label(k, d, tcStatis.getTcStatisReportMap().get(key), XlsUtils.getContentStyle()));

									if (key.equals("测试员")) {
										tester = tcStatis.getTcStatisReportMap().get(key);
									}
									if (key.equals("Case总数") && d == 2) {
										allCont += Integer.valueOf(tcStatis.getTcStatisReportMap().get(key));
										System.out.println(allCont);
									}
									if (key.equals("实际执行数") && d == 2) {
										doneCnt += Integer.valueOf(tcStatis.getTcStatisReportMap().get(key));
									}
									if (key.equals("执行通过") && d == 2) {
										succCnt += Integer.valueOf(tcStatis.getTcStatisReportMap().get(key));
									}
									if (key.equals("执行失败") && d == 2) {
										failCnt += Integer.valueOf(tcStatis.getTcStatisReportMap().get(key));
									}
									if (key.equals("未执行") && d == 2) {
										undoneCnt += Integer.valueOf(tcStatis.getTcStatisReportMap().get(key));
									}
									sheet.setColumnView(k, 15);// 默认列宽

									testCaseCountDto.setTester(tester);
									testCaseCountDto.setAllCont(allCont.toString());
									testCaseCountDto.setDoneCnt(doneCnt.toString());
									testCaseCountDto.setFailCnt(failCnt.toString());
									testCaseCountDto.setSuccCnt(succCnt.toString());
									testCaseCountDto.setUndoneCnt(undoneCnt.toString());

									if (allCont.intValue() > 0 && d == 2) {
										DecimalFormat df1 = new DecimalFormat("#0.##%");
										String passPer = df1.format(succCnt.floatValue() / allCont.floatValue());
										testCaseCountDto.setPassPer(passPer);
									}
									if (allCont.intValue() > 0 && d == 2) {
										DecimalFormat df1 = new DecimalFormat("#0.##%");
										String coverPer = df1.format(doneCnt.floatValue() / allCont.floatValue());
										testCaseCountDto.setCoverPer(coverPer);
									}
								}
								d++;

							}

						} catch (Exception e) {
							logger.error("-- get tc statis error -", e);
						}
						// 自定义列宽
						sheet.setColumnView(0, 30);// TC模块
						sheet.setColumnView(1, 30);// TC集
						sheet.setColumnView(2, 60);// TC用例

						// Tc列表 Head
						Iterator<String> itKey_2 = new TestReport().getListTcReportMap().keySet().iterator();
						for (int k = 0; itKey_2.hasNext(); k++) {
							sheet.addCell(new Label(k, d + 1, itKey_2.next().toString(), XlsUtils.getThStyle()));
						}

						try {
							// Tc列表 Content
							List<TestCaseTree> caseList = testReportIbatisDao.getCaseList(envStepId, testGroup.getId());
							if (caseList != null) {
								int j = d + 2;
								int t = d + 2;
								List<NodesImportDto> nodesList = null;
								List<NodesImportDto> nodesList1 = null;
								for (int i = 0; i < caseList.size(); i++) {
									sheet.addCell(new Label(2, i + d + 2, caseList.get(i).getName(), XlsUtils.getContentStyle()));
									sheet.addCell(new Label(3, i + d + 2, caseList.get(i).getStatusName(), XlsUtils.getContentStyle()));
									sheet.addCell(new Label(4, i + d + 2, caseList.get(i).getCreateDateStr(), XlsUtils.getContentStyle()));//关联时间
									sheet.addCell(new Label(5, i + d + 2, caseList.get(i).getCreateUser(), XlsUtils.getContentStyle()));//关联人
									sheet.addCell(new Label(6, i + d + 2, caseList.get(i).getUpdateTimeStr(), XlsUtils.getContentStyle()));//执行时间
									sheet.addCell(new Label(7, i + d + 2, caseList.get(i).getUpdateUser(), XlsUtils.getContentStyle()));//执行人
									//判断叶子节点是否相同
									if (i == (caseList.size() - 1)) {
										nodesList = getNodesId(caseList.get(i).getTcId());
										String nodes = null;
										for (int m = 0; m < nodesList.size() - 1; m++) {
											if (nodes == null) {
												nodes = nodesList.get(m).getNodeName();
											} else {
												nodes = nodesList.get(m).getNodeName() + "-" + nodes;
											}
										}
										sheet.addCell(new Label(1, j, nodes, XlsUtils.getHeadContentStyle()));
										sheet.mergeCells(1, j, 1, i + d + 2);
										String topNode = nodesList.get(nodesList.size() - 1).getNodeId().toString();
										sheet.addCell(new Label(0, t, nodesList.get(nodesList.size() - 1).getNodeName(), XlsUtils.getHeadContentStyle()));
										sheet.mergeCells(0, t, 0, i + d + 2);
									} else {
										if (caseList.get(i).getParentId() != caseList.get(i + 1).getParentId()) {
											nodesList = getNodesId(caseList.get(i).getTcId());
											nodesList1 = getNodesId(caseList.get(i + 1).getTcId());
											String nodes = null;
											for (int m = 0; m < nodesList.size() - 1; m++) {
												if (nodes == null) {
													nodes = nodesList.get(m).getNodeName();
												} else {
													nodes = nodesList.get(m).getNodeName() + "-" + nodes;
												}
											}
											sheet.addCell(new Label(1, j, nodes, XlsUtils.getHeadContentStyle()));
											sheet.mergeCells(1, j, 1, i + d + 2);
											j = i + d + 3;
											//判断根节点是否相同
											String topNode = nodesList.get(nodesList.size() - 1).getNodeId().toString();
											String topNode1 = nodesList1.get(nodesList1.size() - 1).getNodeId().toString();

											if (i == (caseList.size() - 1) || !topNode.equals(topNode1)) {
												sheet.addCell(new Label(0, t, nodesList.get(nodesList.size() - 1).getNodeName(), XlsUtils.getHeadContentStyle()));
												sheet.mergeCells(0, t, 0, i + d + 2);
												t = i + d + 3;
											}
										}
									}
								}
							}

						} catch (Exception e) {
							logger.error("-- get tc list error --", e);
						}
					}
				}
			}
		}

	}

	public void tcMustTest(WritableWorkbook workbook, TestReport testReport) throws RowsExceededException, WriteException {
		Integer allCont = 0; //Case总数
		Integer doneCnt = 0;//实际执行数
		Integer succCnt = 0; //执行通过
		Integer failCnt = 0;//执行失败
		Integer undoneCnt = 0; //未执行

		String tester = "";
		int d = 0;

		// 创建Sheet
		WritableSheet sheet = workbook.createSheet("必测回归集", 1);

		sheet.addCell(new Label(0, d, "Total Testing Results", XlsUtils.getHeadStyle()));

		// 设置Head
		Set<String> setKey_1 = new TestReport().getTcStatisReportMap().keySet();
		Iterator<String> itKey_1 = setKey_1.iterator();

		// 合并首行
		sheet.mergeCells(0, 0, setKey_1.size() - 1, 0);

		try {

			List<TestReport> tcStatiss = testReportIbatisDao.getCaseStatis(testReport.getStepId(), null);
			d++;
			//测试数量统计表head
			for (int k = 0; itKey_1.hasNext(); k++) {
				String key = itKey_1.next();
				sheet.addCell(new Label(k, d, key, XlsUtils.getThStyle()));
			}
			//测试数量统计表 body

			for (TestReport tcStatis : tcStatiss) {
				d++;
				Iterator<String> itKey_3 = setKey_1.iterator();
				for (int k = 0; itKey_3.hasNext(); k++) {
					String key = itKey_3.next();

					sheet.addCell(new Label(k, d, tcStatis.getTcStatisReportMap().get(key), XlsUtils.getContentStyle()));
				}

			}

		} catch (Exception e) {
			logger.error("-- get tc statis error -", e);
		}

		// 自定义列宽
		sheet.setColumnView(0, 30);// TC模块
		sheet.setColumnView(1, 30);// TC集
		sheet.setColumnView(2, 60);// TC用例

		// Tc列表 Head
		Iterator<String> itKey_2 = new TestReport().getListTcReportMap().keySet().iterator();
		d++;
		for (int k = 0; itKey_2.hasNext(); k++) {
			sheet.addCell(new Label(k, d, itKey_2.next().toString(), XlsUtils.getThStyle()));
		}

		try {
			// Tc列表 Content
			List<TestCaseTree> caseList = testReportIbatisDao.getMustCaseList(testReport.getStepId());
			if (caseList != null) {
				d--;
				int j = d + 2;
				int t = d + 2;
				List<NodesImportDto> nodesList = null;
				List<NodesImportDto> nodesList1 = null;
				for (int i = 0; i < caseList.size(); i++) {
					sheet.addCell(new Label(2, i + d + 2, caseList.get(i).getName(), XlsUtils.getContentStyle()));
					sheet.addCell(new Label(3, i + d + 2, caseList.get(i).getStatusName(), XlsUtils.getContentStyle()));
					sheet.addCell(new Label(4, i + d + 2, caseList.get(i).getCreateDateStr(), XlsUtils.getContentStyle()));//关联时间
					sheet.addCell(new Label(5, i + d + 2, caseList.get(i).getCreateUser(), XlsUtils.getContentStyle()));//关联人
					sheet.addCell(new Label(6, i + d + 2, caseList.get(i).getUpdateTimeStr(), XlsUtils.getContentStyle()));//执行时间
					sheet.addCell(new Label(7, i + d + 2, caseList.get(i).getUpdateUser(), XlsUtils.getContentStyle()));//执行人
					//判断叶子节点是否相同
					if (i == (caseList.size() - 1)) {
						nodesList = getNodesId(caseList.get(i).getTcId());
						String nodes = null;
						for (int m = 0; m < nodesList.size() - 1; m++) {
							if (nodes == null) {
								nodes = nodesList.get(m).getNodeName();
							} else {
								nodes = nodesList.get(m).getNodeName() + "-" + nodes;
							}
						}
						sheet.addCell(new Label(1, j, nodes, XlsUtils.getHeadContentStyle()));
						sheet.mergeCells(1, j, 1, i + d + 2);
						String topNode = nodesList.get(nodesList.size() - 1).getNodeId().toString();
						sheet.addCell(new Label(0, t, nodesList.get(nodesList.size() - 1).getNodeName(), XlsUtils.getHeadContentStyle()));
						sheet.mergeCells(0, t, 0, i + d + 2);
					} else {
						if (caseList.get(i).getParentId() != caseList.get(i + 1).getParentId()) {
							nodesList = getNodesId(caseList.get(i).getTcId());
							nodesList1 = getNodesId(caseList.get(i + 1).getTcId());
							String nodes = null;
							for (int m = 0; m < nodesList.size() - 1; m++) {
								if (nodes == null) {
									nodes = nodesList.get(m).getNodeName();
								} else {
									nodes = nodesList.get(m).getNodeName() + "-" + nodes;
								}
							}
							sheet.addCell(new Label(1, j, nodes, XlsUtils.getHeadContentStyle()));
							sheet.mergeCells(1, j, 1, i + d + 2);
							j = i + d + 3;
							//判断根节点是否相同
							String topNode = nodesList.get(nodesList.size() - 1).getNodeId().toString();
							String topNode1 = nodesList1.get(nodesList1.size() - 1).getNodeId().toString();

							if (i == (caseList.size() - 1) || !topNode.equals(topNode1)) {
								sheet.addCell(new Label(0, t, nodesList.get(nodesList.size() - 1).getNodeName(), XlsUtils.getHeadContentStyle()));
								sheet.mergeCells(0, t, 0, i + d + 2);
								t = i + d + 3;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("-- get tc list error --", e);
		}

	}

	
	public List<NodesImportDto> getNodesId(Long tcId) {
		NodesImportDto node = testReportIbatisDao.getParentId(tcId);
		List<NodesImportDto> nodesList = new ArrayList<NodesImportDto>();
		nodesList.add(node);
		while (true) {
			node = testReportIbatisDao.getParentId(node.getNodeId());
			if (node == null) {
				return nodesList;
			}
			nodesList.add(node);
		}
	}

	public Map<String, Map<String, List<TestCaseTree>>> getAssoTcTreeReport(Long stepId) {
		Map<String, Map<String, List<TestCaseTree>>> result = new TreeMap<String, Map<String, List<TestCaseTree>>>();
		List<TestCaseTree> caseList = testReportIbatisDao.getCaseList(stepId, null);
		if (null != caseList && caseList.size() > 0) {
			for (TestCaseTree tc : caseList) {

				getAllParentTc(tc.getTcId());
			}
		}
		return result;
	}

	public Map<String, String> getAllParentTc(Long tcId) {
		List<TestCaseTree> tcList = new LinkedList<TestCaseTree>();
		while (true) {
			TestCaseTree tc = testReportIbatisDao.getAllParentTc(tcId);
			if (null == tc || null == tc.getTcId()) {
				break;
			}
			tcList.add(tc);
			tcId = tc.getTcId();
		}

		// 目录展现方式改为由大到小
		Map<String, String> linkedHashMap = new LinkedHashMap<String, String>();
		if (null != tcList && tcList.size() > 0) {
			for (int i = tcList.size() - 1; i >= 0; i--) {
				linkedHashMap.put(tcList.get(i).getTcId().toString(), tcList.get(i).getName());
			}
		}
		return linkedHashMap;
	}

	public TestReport getCaseStatis(Long stepId, Long groupId) {
		return testReportIbatisDao.getCaseStatis(stepId, groupId).get(0);
	}

	public void exportPlanTrackCase(HttpServletResponse response, TestReport testReport) throws Exception {

		WritableWorkbook workbook = null;
		ServletOutputStream os = null;
		try {

			response.setCharacterEncoding("gb2312");
			response.reset();
			response.setHeader("pragma", "no-cache");
			//设置下载格式
			response.setHeader("Content-Type", "application/vnd.ms-excel.numberformat:@;charset=gb2312");
			response.addHeader("Content-Disposition", "attachment;filename=\"" + new String(getTestReportName(testReport).getBytes(), "iso-8859-1") + ".xls\"");// 点击导出excle按钮时候页面显示的默认名称
			response.setHeader("Connection", "close");
			//获取输出流
			os = response.getOutputStream();
			workbook = Workbook.createWorkbook(os);

			writeExcel(workbook, testReport);

		} catch (IOException e) {
			logger.error("-- ConfirmReport , Workbook.Write Error --", e);
			throw new Exception(e.getMessage());
		} catch (WriteException e) {
			logger.error("-- ConfirmReport , Create Writable Error --", e);
			throw new Exception(e.getMessage());
		}

	}
}
