package com.bill99.testmp.testmanage.domain.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.helper.StringHelper;
import com.bill99.rmca.common.util.MyBeanUtils;
import com.bill99.testmp.framework.enums.ExecutionTypeEnum;
import com.bill99.testmp.framework.enums.ImportanceEnum;
import com.bill99.testmp.framework.enums.StatusEnum;
import com.bill99.testmp.framework.enums.TaStatusEnum;
import com.bill99.testmp.framework.helper.ExcelExportHelper;
import com.bill99.testmp.framework.helper.ExcelImportHelper;
import com.bill99.testmp.testmanage.common.dto.ExcelImportDto;
import com.bill99.testmp.testmanage.domain.ExcelImportService;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.jeecms.utils.FileUtil;

public class ExcelImportServiceImpl implements ExcelImportService {

	private static Log logger = LogFactory.getLog(ExcelExportHelper.class);

	private static final int FILE_MAX_SIZE = 20 * 1024 * 1024;
	private static final int MAX_LENGTH = 500;

	private TestCasesMng testCasesMng;

	public void importExcelTemplateData(TestManageCmd testManageCmd, User user) throws Exception {
		//附件大小验证
		if (!FileUtil.validFileSize(testManageCmd.getExcelfile(), FILE_MAX_SIZE)) {
			throw new RuntimeException("size");
		}
		InputStream is = null;
		Workbook work = null;
		Sheet sh = null;
		String actions = null;
		String expectedResults = null;
		String testCaseName = null;
		String summary = null;//摘要
		String preconditions = null;//前置条件

		ExcelImportDto excelImportDto = null;
		List<TestCases> newTestCasesList = new ArrayList<TestCases>();
		List<TestCases> oldTestCasesList = new ArrayList<TestCases>();
		try {
			is = testManageCmd.getExcelfile().getInputStream();
			work = Workbook.getWorkbook(is);
			sh = work.getSheet(0);
			for (int i = 1; i <= sh.getRows() - 1; i++) {
				//转换对象
				//0, 0, "用例编号",
				//1, 0, "用例名称",
				//2, 0, "摘要", 
				//3, 0, "前置条件",
				//4, 0, "测试步骤",
				//5, 0, "预期结果",
				//6, 0, "用例等级",
				//7, 0, "测试方式",
				//8, 0, "自动化覆盖
				//9, 0, "状态", 
				//10,0,"特殊用例解析"
				TestCases testCase = new TestCases();
				testCase.setTestCaseId(ExcelImportHelper.getTcCaseId(sh.getCell(0, i).getContents()));

				testCaseName = sh.getCell(1, i).getContents();//用例名称
				if (StringUtils.hasLength(testCaseName)) {
					if (testCaseName.length() > MAX_LENGTH) {
						testCase.setTestCaseName(testCaseName.substring(0, MAX_LENGTH));
					} else {
						testCase.setTestCaseName(testCaseName);
					}
				} else {
					continue;
				}

				summary = sh.getCell(2, i).getContents();//摘要
				if (StringUtils.hasLength(summary) && summary.length() > MAX_LENGTH) {
					testCase.setSummary(summary.substring(0, MAX_LENGTH));
				} else {
					testCase.setSummary(summary);
				}
				preconditions = sh.getCell(3, i).getContents();//前置条件
				if (StringUtils.hasLength(preconditions) && preconditions.length() > MAX_LENGTH) {
					testCase.setPreconditions(preconditions.substring(0, MAX_LENGTH));
				} else {
					testCase.setPreconditions(preconditions);
				}

				actions = sh.getCell(4, i).getContents();//测试步骤
				if (StringUtils.hasLength(actions)) {
					actions = StringHelper.full2HalfChange(actions);
				}
				expectedResults = sh.getCell(5, i).getContents();//预期结果
				if (StringUtils.hasLength(expectedResults)) {
					expectedResults = StringHelper.full2HalfChange(expectedResults);
				}

				testCase.setActions(actions);
				testCase.setExpectedResults(expectedResults);

				if (MyBeanUtils.checkBeanEmpty(testCase)) {
					continue;
				}

				if (!StringUtils.hasLength(testCaseName)) {
					testCase.setTestCaseName(StringUtils.hasLength(testCaseName) == true ? testCaseName : "空白名称");
				}

				testCase.setImportance(ImportanceEnum.getAuditKeyByValue(sh.getCell(6, i).getContents()));// 用例等级
				testCase.setExecutionType(ExecutionTypeEnum.getAuditKeyByValue(sh.getCell(7, i).getContents()));// 测试方式
				testCase.setTaStatus(TaStatusEnum.getAuditKeyByValue(sh.getCell(8, i).getContents()));// 自动化覆盖				
				testCase.setStatus(StatusEnum.getAuditKeyByValue(sh.getCell(9, i).getContents()));// 状态

				if (isNewCase(testCase.getTestCaseId())) {
					testCase.setCreateUser(user.getUsername());
					testCase.setTestprojectId(user.getCurrentTestProject().getId());
					newTestCasesList.add(testCase);
				} else {
					TestCases entity = testCasesMng.getById(testCase.getTestCaseId());
					BeanUtils.copyProperties(testCase, entity, new String[] { "testCaseId", "createDate", "createUser", "keywordsSet", "testprojectId" });
					entity.setUpdateUser(user.getUsername());
					entity.setUpdateDate(DateUtil.getTimeNow());
					oldTestCasesList.add(entity);
				}
			}
			excelImportDto = new ExcelImportDto();

			excelImportDto.setTestSuiteId(testManageCmd.getTestSuiteId());
			if (newTestCasesList.size() > 0) {
				excelImportDto.setTestCasesList(newTestCasesList);
				testCasesMng.bath4Save(excelImportDto);
			}
			if (oldTestCasesList.size() > 0) {
				excelImportDto.setTestCasesList(oldTestCasesList);
				testCasesMng.bath4Update(excelImportDto);
			}
		} catch (Exception e) {
			logger.error("-- index out of bound error --", e);
		} finally {
			try {
				if (null != work) {
					work.close();
				}
				if (null != is) {
					is.close();
				}
			} catch (IOException e) {
				logger.error("<-- close inputStream error -->", e);
			}
		}
	}

	private boolean isNewCase(Long testCasesId) {
		if (testCasesId == null || testCasesMng.getById(testCasesId) == null) {
			return true;
		}
		return false;
	}

	public void setTestCasesMng(TestCasesMng testCasesMng) {
		this.testCasesMng = testCasesMng;
	}

}
