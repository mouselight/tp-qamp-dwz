package com.bill99.testmp.testmanage.domain.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.rmca.common.util.DateUtil;
import com.bill99.testmp.testmanage.common.dto.TestNotifyBugDto;
import com.bill99.testmp.testmanage.common.utils.IssueTypeUtils;
import com.bill99.testmp.testmanage.common.utils.SysSwitchTypeUtil;
import com.bill99.testmp.testmanage.domain.TestNotifier;
import com.bill99.testmp.testmanage.orm.entity.NotifyIssueLog;
import com.bill99.testmp.testmanage.orm.entity.SysSwitch;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public class BugsNotifierImpl extends AbstractTestNotifier implements TestNotifier {

	private SysSwitch sysSwitch;
	private SysSwitch sysSwitch4Issue;

	public void reportorProcess() {

		if (!isNeedNotity()) {
			return;
		}

		//获取所有业务组
		List<TestProjects> testProjects = testProjectsMng.findValidList();
		List<String> recvEmailAddress = null;
		for (TestProjects testProject : testProjects) {

			if (testProject.getJiraTeemId() == null) {
				continue;
			}

			List<Long> notifiedBugIds = notifyIssueLogMng.getNotifiedIssues(testProject.getJiraTeemId(), IssueTypeUtils.ISSUE_TYPE_BUG);
			Map<Long, List<TestNotifyBugDto>> testNotifyBugMaps = jiraMng.getNotifyBugs(testProject.getJiraTeemId(), notifiedBugIds);

			if (testNotifyBugMaps == null || testNotifyBugMaps.size() == 0) {
				continue;
			}
			for (Entry<Long, List<TestNotifyBugDto>> entry : testNotifyBugMaps.entrySet()) {

				TestNotifyBugDto issue = entry.getValue().get(0);

				//初始化收件人地址
				recvEmailAddress = initRecvEmailAddress(testProject.getJiraTeemId(), issue.getIssueId());
				if (recvEmailAddress == null || recvEmailAddress.size() == 0) {
					continue;
				}

				StringBuilder contentSb = new StringBuilder();
				contentSb.append("<style>    ");
				contentSb.append("/* Changing the layout to use less space for mobiles */                         ");
				contentSb
						.append("@media screen and (max-device-width: 480px), screen and (-webkit-min-device-pixel-ratio: 2) {                                                             ");
				contentSb.append(" #email-body {                                                                  ");
				contentSb.append("min-width: 30em !important;                                                     ");
				contentSb.append("}          ");
				contentSb.append(" #email-page {                                                                  ");
				contentSb.append("padding: 8px !important;                                                        ");
				contentSb.append("}          ");
				contentSb.append(" #email-banner {                                                                ");
				contentSb.append("padding: 8px 8px 0 8px !important;                                              ");
				contentSb.append("}          ");
				contentSb.append(" #email-avatar {                                                                ");
				contentSb.append("margin: 1px 8px 8px 0 !important;                                               ");
				contentSb.append("padding: 0 !important;                                                          ");
				contentSb.append("}          ");
				contentSb.append(" #email-fields {                                                                ");
				contentSb.append("padding: 0 8px 8px 8px !important;                                              ");
				contentSb.append("}          ");
				contentSb.append(" #email-gutter {                                                                ");
				contentSb.append("width: 0 !important;                                                            ");
				contentSb.append("}          ");
				contentSb.append("}          ");
				contentSb.append("</style>   ");
				contentSb.append("<div id=\"email-body\">                                                         ");
				contentSb
						.append("  <table id=\"email-wrap\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#f0f0f0;color:#000000;width:100%;\">");
				contentSb.append("    <tr valign=\"top\">                                                         ");
				contentSb
						.append("      <td id=\"email-page\" style=\"padding:16px !important;\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff;border:1px solid #bbbbbb;color:#000000;width:100%;\"> ");
				contentSb.append("          <tr valign=\"top\">                                                   ");
				contentSb
						.append("            <td bgcolor=\"#003366\" style=\"background-color:#003366;color:#ffffff;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;line-height:1;\">BUG新增通知</td>                                           ");
				contentSb.append("          </tr>                                                                 ");
				contentSb.append("          <tr valign=\"top\">                                                   ");
				contentSb
						.append("            <td id=\"email-fields\" style=\"padding:10px 10px 10px 10px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding:0;text-align:left;width:100%;\" width=\"100%\">                             ");
				contentSb.append("                <tr valign=\"top\">                                             ");
				contentSb
						.append("                  <td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">                                                             ");
				contentSb.append("                      <tr valign=\"top\">                                       ");

				contentSb
						.append("<td width=\"20%\" style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 10px 10px 0;white-space:nowrap;\"><strong>项目名称</strong></td>");
				contentSb
						.append("<td width=\"5%\" style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\"><strong>关键字</strong></td>");
				contentSb
						.append("<td width=\"8%\" style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\"><strong>预计上线时间</strong></td>");
				contentSb.append("<td style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\"><strong>BUG清单</strong></td>");

				contentSb.append("                      </tr>                                                     ");

				contentSb.append("                      <tr valign=\"top\">                                       ");
				contentSb.append("<td style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 10px 10px 0;white-space:nowrap;\">");
				contentSb.append(issue.getIssueSummary() == null ? "" : issue.getIssueSummary());
				contentSb.append("</td>");

				contentSb.append("<td style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\">");
				contentSb.append(issue.getIssuePKey() == null ? "" : issue.getIssuePKey());
				contentSb.append("</td>");

				contentSb.append("<td style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\">");
				contentSb.append("</td>");
				contentSb.append("<td style=\"color:#000000;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:12px;padding:0 0 10px 0;\">");

				int index = 0;
				for (TestNotifyBugDto testNotifyBugDto : entry.getValue()) {
					contentSb.append("【" + testNotifyBugDto.getBugEnv() + "┃" + testNotifyBugDto.getBugPriority() + "┃" + testNotifyBugDto.getBugStatus() + "┃"
							+ DateUtil.getStringFromDate(testNotifyBugDto.getBugCreateDate()) + "】" + "<br />");
					contentSb.append(testNotifyBugDto.getBugSummary());
					index++;
					if (index < entry.getValue().size()) {
						contentSb.append("<br />" + "-----------------------------------------------------------------------" + "<br />");
					}

					NotifyIssueLog notifyIssueLog = new NotifyIssueLog();
					notifyIssueLog.setIssueId(testNotifyBugDto.getBugId());
					notifyIssueLog.setIssueType(IssueTypeUtils.ISSUE_TYPE_BUG);
					notifyIssueLog.setTeemId(testProject.getJiraTeemId());
					notifyIssueLogMng.insertNotifiedIssues(notifyIssueLog);

				}
				contentSb.append("</td>");
				contentSb.append("                      </tr>                                                     ");

				contentSb.append("                    </table></td>                                               ");
				contentSb.append("                </tr>                                                           ");
				contentSb.append("              </table></td>                                                     ");
				contentSb.append("          </tr>                                                                 ");
				contentSb.append("        </table></td>                                                           ");
				contentSb.append("      <!-- End #email-page -->                                                  ");
				contentSb.append("    </tr>  ");
				contentSb.append("    <tr valign=\"top\">                                                         ");
				contentSb
						.append("      <td style=\"color:#505050;font-family:Arial,FreeSans,Helvetica,sans-serif;font-size:10px;line-height:14px;padding: 0 16px 16px 16px;text-align:center;\"> 此邮件由测试管理平台自动发送.<br />                            ");
				contentSb.append("        如果你认为这封邮件不正确, 请联系 <a style='color:#326ca6;' href='mailto:poa_qa_tp@99bill.com'>系统管理员</a>。</td>                               ");
				contentSb.append("    </tr>  ");
				contentSb.append("  </table> ");
				contentSb.append("  <!-- End #email-wrap -->                                                      ");
				contentSb.append("</div>     ");
				contentSb.append("<!-- End #email-body -->                                                        ");

				List<String> ccAddress = sysSwitch.getNotifyBugCcaddress() == null ? new ArrayList<String>() : Arrays.asList(sysSwitch.getNotifyBugCcaddress().split(","));

				try {
					outMailSender.sendEmail(recvEmailAddress, ccAddress, "【BUG新增通知】【" + testProject.getPrefix() + "】" + "【" + DateUtil.getTime() + "】", contentSb.toString(),
							"poa_qa_tp@99bill.com");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	//获取邮箱列表
	@Override
	protected List<String> initRecvEmailAddress(Long teemId, Long issueId) {

		List<String> emailList = new ArrayList<String>();

		if (StringUtils.hasLength(sysSwitch4Issue.getNotifyBugRecvaddress())) {
			emailList = jiraMng.getEmail4RoleIssue(StringUtil.string2LongArray(sysSwitch4Issue.getNotifyBugRecvaddress()), issueId);
		}
		if (StringUtils.hasLength(sysSwitch.getNotifyBugRecvaddress())) {
			emailList.addAll(jiraMng.getEmail4Role(StringUtil.string2LongArray(sysSwitch.getNotifyBugRecvaddress()), new Long[] { teemId }));
		} else if (StringUtils.hasLength(sysSwitch.getNotifyBugCcaddress())) {
			emailList.addAll(Arrays.asList(sysSwitch.getNotifyBugCcaddress().split(",")));
		}
		return emailList;
	}

	@Override
	protected boolean isNeedNotity() {
		sysSwitch = sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLESYS);
		sysSwitch4Issue = sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLEISSUE);
		if (sysSwitch != null && sysSwitch.getNotifyBugState() != null && sysSwitch.getNotifyBugState()) {
			return true;
		}
		return false;
	}
}
