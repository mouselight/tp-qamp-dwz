package com.bill99.testmp.testmanage.domain.impl;

import javax.servlet.http.HttpSession;

import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.testmanage.domain.IndexMsgService;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;

public class IndexMsgServiceImpl implements IndexMsgService {

	private TestIssueMng testIssueMng;
	private TestplansMng testplansMng;

	public void loadCountMsg(HttpSession session, Long teemId, String userName) {
		//待排期的需求
		TestIssue queryModel = new TestIssue();
		queryModel.setTeamId(teemId);
		queryModel.setIssueStatusCos(new Long[] { 1L, 10022L, 10023L, 10024L, 10025L, 10026L, 10027L, 10028L });
		Integer issueCount = testIssueMng.queryCount(queryModel);

		//待跟踪的计划
		Testplans testPlan = new Testplans();
		testPlan.setTeamId(teemId);
		testPlan.setCreateUser(userName);
		testPlan.setStates(new Integer[]{TestPlanStatusEnum.STATUS_0.getValue(),TestPlanStatusEnum.STATUS_1.getValue()});
		Integer planCount = testplansMng.queryCount(testPlan);

		session.setAttribute("testplansCount", issueCount);
		session.setAttribute("testIssueCount", planCount);
	}

	public void setTestIssueMng(TestIssueMng testIssueMng) {
		this.testIssueMng = testIssueMng;
	}

	public void setTestplansMng(TestplansMng testplansMng) {
		this.testplansMng = testplansMng;
	}
}
