package com.bill99.testmp.testmanage.domain;

import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

public interface ExcelImportService {

	public void importExcelTemplateData(TestManageCmd testManageCmd, User user) throws Exception;

}
