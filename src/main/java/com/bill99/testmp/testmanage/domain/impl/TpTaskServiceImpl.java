package com.bill99.testmp.testmanage.domain.impl;

import org.springframework.util.StringUtils;

import com.bill99.testmp.framework.helper.HttpFixtureHelper;
import com.bill99.testmp.testmanage.common.dto.TpTaskRequestBo;
import com.bill99.testmp.testmanage.common.dto.TpTaskResponesBo;
import com.bill99.testmp.testmanage.common.utils.TpJobUtil;
import com.bill99.testmp.testmanage.domain.TpTaskService;

public class TpTaskServiceImpl implements TpTaskService {
	private String jenKinsTpUrl;

	private String jenKinsTpJobDelaySec;

	public TpTaskResponesBo triggerTpTask(TpTaskRequestBo tpTaskRequestBo) {
		TpTaskResponesBo tpTaskResponesBo = new TpTaskResponesBo();

		if (!StringUtils.hasLength(tpTaskRequestBo.getJobName()) || !StringUtils.hasLength(tpTaskRequestBo.getTpUsers()) || !StringUtils.hasLength(tpTaskRequestBo.getTpRuntime())
				|| !StringUtils.hasLength(tpTaskRequestBo.getPublishVersion()) || !StringUtils.hasLength(tpTaskRequestBo.getTpTestinterface())) {
			return new TpTaskResponesBo(Boolean.FALSE, "必填项未填写!!!");
		}
		requestJob(tpTaskRequestBo.getJobName(), null, tpTaskRequestBo);
		return tpTaskResponesBo;
	}

	private void requestJob(String jobName, Long idTaTask, TpTaskRequestBo tpTaskRequestBo) {
		HttpFixtureHelper requery = new HttpFixtureHelper();
		StringBuilder jenKinsUrlBuilder = new StringBuilder();
		jenKinsUrlBuilder.append(jenKinsTpUrl);
		jenKinsUrlBuilder.append("job/");
		jenKinsUrlBuilder.append(jobName);//动态获取
		jenKinsUrlBuilder.append("/buildWithParameters?");
		//		jenKinsUrlBuilder.append(TpJobUtil.ID_TA_TASK);
		//		jenKinsUrlBuilder.append("=");
		//		jenKinsUrlBuilder.append(idTaTask);
		jenKinsUrlBuilder.append("delay=");
		jenKinsUrlBuilder.append(jenKinsTpJobDelaySec);
		jenKinsUrlBuilder.append("sec");

		jenKinsUrlBuilder.append("&");
		jenKinsUrlBuilder.append(TpJobUtil.TP_USERS);
		jenKinsUrlBuilder.append("=");
		jenKinsUrlBuilder.append(tpTaskRequestBo.getTpUsers());

		jenKinsUrlBuilder.append("&");
		jenKinsUrlBuilder.append(TpJobUtil.TP_RUNTIME);
		jenKinsUrlBuilder.append("=");
		jenKinsUrlBuilder.append(tpTaskRequestBo.getTpRuntime());

		jenKinsUrlBuilder.append("&");
		jenKinsUrlBuilder.append(TpJobUtil.PUBLISH_VERSION);
		jenKinsUrlBuilder.append("=");
		jenKinsUrlBuilder.append(tpTaskRequestBo.getPublishVersion());

		jenKinsUrlBuilder.append("&");
		jenKinsUrlBuilder.append(TpJobUtil.TP_TESTINTERFACE);
		jenKinsUrlBuilder.append("=");
		jenKinsUrlBuilder.append(tpTaskRequestBo.getTpTestinterface());

		if (StringUtils.hasLength(tpTaskRequestBo.getTpTesttype())) {
			jenKinsUrlBuilder.append("&");
			jenKinsUrlBuilder.append(TpJobUtil.TP_TESTTYPE);
			jenKinsUrlBuilder.append("=");
			jenKinsUrlBuilder.append(tpTaskRequestBo.getTpTesttype());
		}
		if (StringUtils.hasLength(tpTaskRequestBo.getTpTestdesc())) {
			jenKinsUrlBuilder.append("&");
			jenKinsUrlBuilder.append(TpJobUtil.TP_TESTDESC);
			jenKinsUrlBuilder.append("=");
			jenKinsUrlBuilder.append(tpTaskRequestBo.getTpTestdesc());
		}
		if (StringUtils.hasLength(tpTaskRequestBo.getTpRemark())) {
			jenKinsUrlBuilder.append("&");
			jenKinsUrlBuilder.append(TpJobUtil.TP_REMARK);
			jenKinsUrlBuilder.append("=");
			jenKinsUrlBuilder.append(tpTaskRequestBo.getTpRemark());
		}

		requery.setUrl(jenKinsUrlBuilder.toString());
		requery.Post();
	}

	public void setJenKinsTpUrl(String jenKinsTpUrl) {
		this.jenKinsTpUrl = jenKinsTpUrl;
	}

	public void setJenKinsTpJobDelaySec(String jenKinsTpJobDelaySec) {
		this.jenKinsTpJobDelaySec = jenKinsTpJobDelaySec;
	}
}
