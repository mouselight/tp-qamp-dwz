package com.bill99.testmp.testmanage.domain;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;

public interface TcFilterService {

	public void engineFilter(HttpSession httpSession, TestCasesQueryDto testCasesQueryDto);

	public void clearFilter(Long userId, HttpSession httpSession);

	public Map<Integer, List<Long>> getFilterChildIds(List<Long> parentIds, TestCasesQueryDto testCasesQueryDto);

}
