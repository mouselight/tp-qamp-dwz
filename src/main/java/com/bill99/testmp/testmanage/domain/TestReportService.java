package com.bill99.testmp.testmanage.domain;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.bill99.testmp.testmanage.common.dto.NodesImportDto;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.entity.TestReport;

public interface TestReportService {

	public String testReport(HttpServletResponse response, TestReport testReport) throws Exception;

	public Map<String, Map<String, List<TestCaseTree>>> getAssoTcTreeReport(Long stepId);

	public Map<String, String> getAllParentTc(Long tcId);

	public TestReport getCaseStatis(Long stepId, Long groupId);

	public List<NodesImportDto> getNodesId(Long tcId);

	public void exportPlanTrackCase(HttpServletResponse response, TestReport testReport) throws Exception;

}
