package com.bill99.testmp.testmanage.domain.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TestManageService;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TcStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

public class TestManageServiceImpl implements TestManageService {
	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private NodesHierarchyMng nodesHierarchyMng;
	private TcStepsMng tcStepsMng;
	private TestCasesMng testCasesMng;

	public void setNodesHierarchyMng(NodesHierarchyMng nodesHierarchyMng) {
		this.nodesHierarchyMng = nodesHierarchyMng;
	}

	public void setTcStepsMng(TcStepsMng tcStepsMng) {
		this.tcStepsMng = tcStepsMng;
	}

	public void setTestCasesMng(TestCasesMng testCasesMng) {
		this.testCasesMng = testCasesMng;
	}

	public void reBuildTestCase(TestCases testCases, TestManageCmd testManageCmd) {
		List<TcSteps> reSortList = new ArrayList<TcSteps>();
		int i = 1;
		for (Serializable tcStepId : testManageCmd.getTcStepsItems()) {
			TcSteps tcSteps = (TcSteps) testManageCmd.getTcStepsItems().get((Integer) tcStepId);
			tcSteps.setStepNumber(i);
			reSortList.add(tcSteps);
		}
		testCasesMng.update(testCases);
		tcStepsMng.batch4ReBuildTcSteps(testCases.getTestCaseId(), reSortList);
	}

	public void initTreeTcQuantity() {
		logger.info("::::::::::initTreeTcQuantity start::::::::::");
		List<NodesHierarchy> roots = nodesHierarchyMng.getRoots();
		if (roots != null && roots.size() > 0) {
			for (NodesHierarchy nodesHierarchy : roots) {
				TreeUtils.TC_QUANTITY_MAP.put(nodesHierarchy.getId().toString(), "(" + calcTcQuantity(nodesHierarchy.getId()) + ")");// 根节点总数
			}
		}
		logger.info("::::::::::initTreeTcQuantity end::::::::::");
	}

	public long calcTcQuantity(Long pid) {
		long tcQuantity = 0;
		// nodesHierarchyMng.initTestCase(testprojectId, pid);//初始化数据用
		tcQuantity += nodesHierarchyMng.getTcQuantity(pid);
		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				tcQuantity += calcTcQuantity(suiteId);
			}
		}
		TreeUtils.TC_QUANTITY_MAP.put(pid.toString(), "(" + tcQuantity + ")");
		return tcQuantity;
	}

	public List<NodesHierarchy> getNodesTree(Long pid) {
		List<NodesHierarchy> nodesTree = new ArrayList<NodesHierarchy>();
		// nodesTree.add(nodesHierarchyMng.getById(pid));
		calcTcTree(pid, nodesTree);
		return nodesTree;
	}

	public List<NodesHierarchy> calcTcTree(Long pid, List<NodesHierarchy> nodesTree) {
		int i = 0;
		List<NodesHierarchy> nodesTreeTmp = nodesHierarchyMng.getChildSuite(pid);
		if (nodesTreeTmp != null && nodesTreeTmp.size() > 0) {
			for (NodesHierarchy node : nodesTreeTmp) {
				int j = 0;
				node.setOrderMark(i + "-" + j);
				i++;
				j++;
				nodesTree.addAll(calcTcTree(node.getId(), nodesTree));
			}
		}
		return nodesTreeTmp;
	}

	public List<TestProjects> getProjectGroup(Long planStepId) {
		return nodesHierarchyMng.getProjectGroup(planStepId);

	}

	//	public List<TestProjects> getProjectGroup(Long planStepId) {
	//		List<Long> nodes = nodesHierarchyMng.getAssoNodes(planStepId);
	//		if (nodes.isEmpty()) {
	//			return null;
	//		}
	//		Long projectId = null;
	//		Set<Long> pids = new HashSet<Long>();
	//		for (Long node : nodes) {
	//			projectId = getNodes(nodesHierarchyMng.getProjectGroup(node), projectId);
	//			pids.add(projectId);
	//		}
	//		return nodesHierarchyMng.getGroup(pids);
	//
	//	}

	//	public Long getNodes(Long node, Long pid) {
	//		if (null == node) {
	//			return pid;
	//		} else {
	//			pid = node;
	//			node = nodesHierarchyMng.getProjectGroup(node);
	//			return getNodes(node, pid);
	//		}
	//	}

	public List<TestProjects> getAssoProjects(Long planStepId) {

		return null;
	}

}
