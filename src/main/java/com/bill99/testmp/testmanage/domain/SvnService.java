package com.bill99.testmp.testmanage.domain;

import java.util.List;

import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDirEntry;

import com.bill99.testmp.testmanage.orm.entity.SvnChangeItem;

public interface SvnService {

	public boolean createSvnPath(String svnUrl, String svnPath, String svnUserName, String svnPassword, String releaseId, String effectApp);

	public boolean createSvnPath(String svnUrl, String svnPath, String svnUserName, String svnPassword, String logMessage);

	public boolean checkSvnFile(String svnPath, String fileName, String svnUserName, String svnPassword);

	public long getSvnLatestRevision(String svnUrl, String svnUserName, String svnPassword);

	public long getMaxSvnRevision(String svnUrl, String svnPath, String svnUserName, String svnPassword, long startRevision);

	public List<SvnChangeItem> getChangeSetFromSVN(String svnUrl, String svnPath, String svnUserName, String svnPassword, boolean includeAll, long startRevision, long endRevision);

	public boolean mergeSvnFile(List<SvnChangeItem> fromSvnList, String fromSvnUrl, String toSvnUrl, String svnUserName, String svnPassword, String fromApp);

	public SVNCommitInfo uploadFileToSvn(String svnUrl, String svnPath, String fileName, byte[] fileData, String svnUserName, String svnPassword, String logMessage);

	public SVNCommitInfo addFileToSvn(String svnUrl, String svnPath, String fileName, byte[] fileData, String svnUserName, String svnPassword, String logMessage);

	public byte[] getFileFromSvn(String svnUrl, String svnPath, String fileName, String svnUserName, String svnPassword);

	public List<SVNDirEntry> getSubEntry(String svnUrl, String svnPath, String svnUserName, String svnPassword);

	public boolean deleteSvnFile(String svnUrl, String svnPath, String fileName, String svnUserName, String svnPassword, String logMessage);

	public boolean uploadThirdJar(String svnUrl, List<String> svnPathList, String org, String fileName, String svnUserName, String svnPassword, byte[] fileData,
			StringBuilder errorInfo);
}
