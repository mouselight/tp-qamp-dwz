package com.bill99.testmp.testmanage.domain.impl;

import java.util.List;

import com.bill99.riaframework.domain.mail.EmailSender;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;
import com.bill99.testmp.testmanage.orm.manager.NotifyIssueLogMng;
import com.bill99.testmp.testmanage.orm.manager.SysSwitchMng;
import com.bill99.testmp.testmanage.orm.manager.TestNotifyMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

public abstract class AbstractTestNotifier {

	protected EmailSender outMailSender;
	protected TestProjectsMng testProjectsMng;
	protected JiraMng jiraMng;
	protected TestNotifyMng testNotifyMng;
	protected NotifyIssueLogMng notifyIssueLogMng;
	protected SysSwitchMng sysSwitchMng;

	protected abstract boolean isNeedNotity();

	protected abstract List<String> initRecvEmailAddress(Long teemId, Long issueId);

	public void setOutMailSender(EmailSender outMailSender) {
		this.outMailSender = outMailSender;
	}

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}

	public void setJiraMng(JiraMng jiraMng) {
		this.jiraMng = jiraMng;
	}

	public void setTestNotifyMng(TestNotifyMng testNotifyMng) {
		this.testNotifyMng = testNotifyMng;
	}

	public void setNotifyIssueLogMng(NotifyIssueLogMng notifyIssueLogMng) {
		this.notifyIssueLogMng = notifyIssueLogMng;
	}

	public void setSysSwitchMng(SysSwitchMng sysSwitchMng) {
		this.sysSwitchMng = sysSwitchMng;
	}

}
