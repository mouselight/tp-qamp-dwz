package com.bill99.testmp.testmanage.domain;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public interface TestManageService {

	public void initTreeTcQuantity();

	public long calcTcQuantity(Long pid);

	public List<NodesHierarchy> getNodesTree(Long pid);

	public List<TestProjects> getProjectGroup(Long planStepId);

	public List<TestProjects> getAssoProjects(Long planStepId);

}
