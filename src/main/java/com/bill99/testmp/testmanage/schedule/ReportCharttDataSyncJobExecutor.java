package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.orm.manager.ReportchartMng;

public class ReportCharttDataSyncJobExecutor {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());
	
	private ReportchartMng reportchartMng;
	
	public void execute(){
		
		logger.info("ReportCharttDataSyncJobExecutor Start++++++++++");
		reportchartMng.saveReportdata();	
		logger.info("ReportCharttDataSyncJobExecutor success");
	}



	public void setReportchartMng(ReportchartMng reportchartMng) {
		this.reportchartMng = reportchartMng;
	}
			

}
