package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;

public class JiraDataSyncJobExecutor {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private JiraMng jiraMng;

	public void execute() {
		jiraMng.initJiraData();
		logger.info("JiraDataSyncJobExecute success");
	}

	public void setJiraMng(JiraMng jiraMng) {
		this.jiraMng = jiraMng;
	}

}
