package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.domain.TestNotifier;

public class NotifyBugsJobExecutor {

	private TestNotifier bugsNotifier;
	private TestNotifier testIssueNotifier;

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public void execute() {
		logger.info("===@ReportBugs@==>		[start]		<==@@===");
		bugsNotifier.reportorProcess();
		logger.info("===@ReportBugs@==>		[success]	<==@@===");

		logger.info("===@ReportIssues@==>		[start]		<==@@===");
		testIssueNotifier.reportorProcess();
		logger.info("===@ReportIssues@==>		[success]	<==@@===");
	}

	public void setBugsNotifier(TestNotifier bugsNotifier) {
		this.bugsNotifier = bugsNotifier;
	}

	public void setTestIssueNotifier(TestNotifier testIssueNotifier) {
		this.testIssueNotifier = testIssueNotifier;
	}

}
