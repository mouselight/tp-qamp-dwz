package com.bill99.testmp.testmanage.schedule;

import com.bill99.testmp.testmanage.domain.TestManageService;

public class TreeTcQuantitySyncJobExecutor {

	private TestManageService testManageService;

	public void execute() {
		testManageService.initTreeTcQuantity();
	}

	public void setTestManageService(TestManageService testManageService) {
		this.testManageService = testManageService;
	}

}
