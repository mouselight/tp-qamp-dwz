package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.domain.TestNotifier;

public class NotifyTestSchedulesJobExecutor {

	private TestNotifier testSchedulesNotifier;

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	public void execute() {
		logger.info("===@ReportTestSchedules@==>	[start]		<==@@===");
		testSchedulesNotifier.reportorProcess();
		logger.info("===@ReportTestSchedules@==>	[success]	<==@@===");
	}

	public void setTestSchedulesNotifier(TestNotifier testSchedulesNotifier) {
		this.testSchedulesNotifier = testSchedulesNotifier;
	}

}
