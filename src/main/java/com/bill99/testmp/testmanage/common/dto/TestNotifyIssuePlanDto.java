package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

import com.bill99.testmp.framework.enums.PlanStateEnum;

public class TestNotifyIssuePlanDto implements Serializable {

	private static final long serialVersionUID = -8115363026093704081L;
	private Long issueId;
	private String issueSummary;
	private String issueState;
	private Integer planState;
	private String planSummary;
	private String planPkey;
	private String releaseId;
	private String issuePKey;

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getIssueSummary() {
		return issueSummary;
	}

	public void setIssueSummary(String issueSummary) {
		this.issueSummary = issueSummary;
	}

	public String getIssueState() {
		return issueState;
	}

	public void setIssueState(String issueState) {
		this.issueState = issueState;
	}

	public String getPlanState() {
		return PlanStateEnum.getPlanStateStrValue(planState);
	}

	public void setPlanState(Integer planState) {
		this.planState = planState;
	}

	public String getPlanSummary() {
		return planSummary;
	}

	public void setPlanSummary(String planSummary) {
		this.planSummary = planSummary;
	}

	public String getPlanPkey() {
		return planPkey;
	}

	public void setPlanPkey(String planPkey) {
		this.planPkey = planPkey;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public String getIssuePKey() {
		return issuePKey;
	}

	public void setIssuePKey(String issuePKey) {
		this.issuePKey = issuePKey;
	}

}
