package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.StringUtils;

public class TestCasesQueryDto implements Serializable {

	private static final long serialVersionUID = -6634641968432591433L;

	private Long pid;
	private Long planStepId;

	private Long testSuiteId;
	private Long testProjectId;

	private String testCaseSn;//测试标识
	private String testCaseName;//测试名称
	private String summary;//测试摘要
	private Long testCaseId;

	private Integer testSuiteStatus;
	//	private Long[] testSuiteCos;//选中测试集
	private Long[] keywordsCos;//选中关键字
	private Long[] statusCos;//选中状态
	private Long[] importanceCos;//选中等级
	private Long[] executionTypeCos;//选中测试方式
	private Long[] taStatusCos;//选中自动化覆盖状态

	private String keywordsCosStr;//选中关键字
	private String statusCosStr;//选中状态
	private String importanceCosStr;//选中等级
	private String executionTypeCosStr;//选中测试方式
	private String taStatusCosStr;//选中自动化覆盖状态

	private String beginCreateDate; //更新时间起始
	private String endCreateDate; //更新时间截止	
	private String beginUpdateDate; //更新时间起始
	private String endUpdateDate; //更新时间截止	

	private String updateUser;
	private String createUser;

	private String testSuiteIdStrs;
	private Integer num;
	private List<Long> parentIds;

	private Long[] testProjectIds;//选中测试项目
	private String testProjectIdCosStr;//选中测试项目

	public TestCasesQueryDto() {
	}

	public TestCasesQueryDto(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Long[] getKeywordsCos() {
		return keywordsCos;
	}

	public void setKeywordsCos(Long[] keywordsCos) {
		this.keywordsCos = keywordsCos;
	}

	public Long[] getStatusCos() {
		return statusCos;
	}

	public void setStatusCos(Long[] statusCos) {
		this.statusCos = statusCos;
	}

	public Long[] getImportanceCos() {
		return importanceCos;
	}

	public void setImportanceCos(Long[] importanceCos) {
		this.importanceCos = importanceCos;
	}

	public Long[] getExecutionTypeCos() {
		return executionTypeCos;
	}

	public void setExecutionTypeCos(Long[] executionTypeCos) {
		this.executionTypeCos = executionTypeCos;
	}

	public Long[] getTaStatusCos() {
		return taStatusCos;
	}

	public void setTaStatusCos(Long[] taStatusCos) {
		this.taStatusCos = taStatusCos;
	}

	public String getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(String beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}

	public String getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(String endCreateDate) {
		this.endCreateDate = endCreateDate;
	}

	public String getBeginUpdateDate() {
		return beginUpdateDate;
	}

	public void setBeginUpdateDate(String beginUpdateDate) {
		this.beginUpdateDate = beginUpdateDate;
	}

	public String getEndUpdateDate() {
		return endUpdateDate;
	}

	public void setEndUpdateDate(String endUpdateDate) {
		this.endUpdateDate = endUpdateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser.trim();
	}

	public String getTestCaseName() {
		if (StringUtils.hasLength(testCaseName)) {
			return testCaseName.trim();
		}
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public Long getTestSuiteId() {
		return testSuiteId;
	}

	public void setTestSuiteId(Long testSuiteId) {
		this.testSuiteId = testSuiteId;
	}

	public Integer getTestSuiteStatus() {
		return testSuiteStatus;
	}

	public void setTestSuiteStatus(Integer testSuiteStatus) {
		this.testSuiteStatus = testSuiteStatus;
	}

	public String getTestSuiteIdStrs() {
		return testSuiteIdStrs;
	}

	public void setTestSuiteIdStrs(String testSuiteIdStrs) {
		this.testSuiteIdStrs = testSuiteIdStrs;
	}

	public String getKeywordsCosStr() {
		return keywordsCosStr;
	}

	public void setKeywordsCosStr(String keywordsCosStr) {
		this.keywordsCosStr = keywordsCosStr;
	}

	public String getStatusCosStr() {
		return statusCosStr;
	}

	public void setStatusCosStr(String statusCosStr) {
		this.statusCosStr = statusCosStr;
	}

	public String getImportanceCosStr() {
		return importanceCosStr;
	}

	public void setImportanceCosStr(String importanceCosStr) {
		this.importanceCosStr = importanceCosStr;
	}

	public String getExecutionTypeCosStr() {
		return executionTypeCosStr;
	}

	public void setExecutionTypeCosStr(String executionTypeCosStr) {
		this.executionTypeCosStr = executionTypeCosStr;
	}

	public String getTaStatusCosStr() {
		return taStatusCosStr;
	}

	public void setTaStatusCosStr(String taStatusCosStr) {
		this.taStatusCosStr = taStatusCosStr;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getNum() {
		return num;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTestCaseSn() {
		return testCaseSn;
	}

	public void setTestCaseSn(String testCaseSn) {
		this.testCaseSn = testCaseSn;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getPid() {
		return pid;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setParentIds(List<Long> parentIds) {
		this.parentIds = parentIds;
	}

	public List<Long> getParentIds() {
		return parentIds;
	}

	public Long[] getTestProjectIds() {
		return testProjectIds;
	}

	public void setTestProjectIds(Long[] testProjectIds) {
		this.testProjectIds = testProjectIds;
	}

	public String getTestProjectIdCosStr() {
		return testProjectIdCosStr;
	}

	public void setTestProjectIdCosStr(String testProjectIdCosStr) {
		this.testProjectIdCosStr = testProjectIdCosStr;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

}
