package com.bill99.testmp.testmanage.common.dto;

public class TestCaseCountDto {
	//Case总数
	private String allCont;

	public String getAllCont() {
		return allCont;
	}

	public void setAllCont(String allCont) {
		this.allCont = allCont;
	}

	public String getDoneCnt() {
		return doneCnt;
	}

	public void setDoneCnt(String doneCnt) {
		this.doneCnt = doneCnt;
	}

	public String getSuccCnt() {
		return succCnt;
	}

	public void setSuccCnt(String succCnt) {
		this.succCnt = succCnt;
	}

	public String getFailCnt() {
		return failCnt;
	}

	public void setFailCnt(String failCnt) {
		this.failCnt = failCnt;
	}

	public String getUndoneCnt() {
		return undoneCnt;
	}

	public void setUndoneCnt(String undoneCnt) {
		this.undoneCnt = undoneCnt;
	}

	public String getPassPer() {
		return passPer;
	}

	public void setPassPer(String passPer) {
		this.passPer = passPer;
	}

	public String getCoverPer() {
		return coverPer;
	}

	public void setCoverPer(String coverPer) {
		this.coverPer = coverPer;
	}

	public String getTester() {
		return tester;
	}

	public void setTester(String tester) {
		this.tester = tester;
	}

	//实际执行数
	private String doneCnt;
	//执行通过
	private String succCnt;
	//执行失败
	private String failCnt;
	//未执行
	private String undoneCnt;
	//通过率
	private String passPer;
	//覆盖率
	private String coverPer;

	private String tester;

}
