package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TaMethodDto implements Serializable {

	private static final long serialVersionUID = -8352912942301382083L;

	private Long idTestProject;
	private String taSvnUrl;
	private String packageName;
	private String className;
	private String methodName;
	private String methodMemo;
	private String dependsOnMethods;
	private Long idSvnMethod;
	private Long tcCount;

	public Long getIdTestProject() {
		return idTestProject;
	}

	public void setIdTestProject(Long idTestProject) {
		this.idTestProject = idTestProject;
	}

	public String getTaSvnUrl() {
		return taSvnUrl;
	}

	public void setTaSvnUrl(String taSvnUrl) {
		this.taSvnUrl = taSvnUrl;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodMemo() {
		return methodMemo;
	}

	public void setMethodMemo(String methodMemo) {
		this.methodMemo = methodMemo;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

	public Long getTcCount() {
		return tcCount;
	}

	public void setTcCount(Long tcCount) {
		this.tcCount = tcCount;
	}

	public String getDependsOnMethods() {
		return dependsOnMethods;
	}

	public void setDependsOnMethods(String dependsOnMethods) {
		this.dependsOnMethods = dependsOnMethods;
	}

}
