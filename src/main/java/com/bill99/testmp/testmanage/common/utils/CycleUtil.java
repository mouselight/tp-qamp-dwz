package com.bill99.testmp.testmanage.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.bill99.riaframework.common.utils.DateUtil;

public class CycleUtil {

	/*
	* 取本周7天的第一天（周一的日期）
	*/
	public static String getCurrentCycleId() {
		int mondayPlus;
		Calendar cd = Calendar.getInstance();
		// 获得今天是一周的第几天，星期日是第一天，星期二是第二天......
		int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek <= 3) {//星期二或之前 上线日期是星期二
			mondayPlus = 3 - dayOfWeek;
		} else if (dayOfWeek <= 5) {//星期二之后星期四或之前 上线日期是星期四
			mondayPlus = 5 - dayOfWeek;
		} else {//星期四之后上线日期是下星期二
			mondayPlus = 7 - dayOfWeek + 3;
		}
		GregorianCalendar currentDate = new GregorianCalendar();
		currentDate.add(GregorianCalendar.DATE, mondayPlus);
		Date monday = currentDate.getTime();

		return "#" + DateUtil.parseFromDate(monday);

	}

}
