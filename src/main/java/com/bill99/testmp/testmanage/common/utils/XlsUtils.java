package com.bill99.testmp.testmanage.common.utils;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;

public class XlsUtils {

	//Head样式
	public static WritableCellFormat getHeadStyle() {
		WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.createFont("Calibri"),
				14, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK));//字体,大小,加粗,斜体,下划线,颜色
		try {
			titleFormat.setBackground(Colour.PALE_BLUE);//背景色
			titleFormat.setBorder(Border.ALL, BorderLineStyle.THIN);//表格线
			titleFormat.setAlignment(Alignment.CENTRE);//居中
		} catch (WriteException e) {
		}
		return titleFormat;
	}

	//Head样式
	public static WritableCellFormat getThStyle() {
		WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.createFont("Calibri"),
				12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK));//字体,大小,加粗,斜体,下划线,颜色
		try {
			titleFormat.setBackground(Colour.PALE_BLUE);//背景色
			titleFormat.setBorder(Border.ALL, BorderLineStyle.THIN);//表格线
		} catch (WriteException e) {
		}
		return titleFormat;
	}

	//Title样式
	public static WritableCellFormat getTileStyle() {
		WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.createFont("Calibri"),
				14, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLUE));//字体,大小,加粗,斜体,下划线,颜色
		return titleFormat;
	}

	//内容类型
	public static WritableCellFormat getContentStyle(Colour colour) {
		WritableCellFormat contentFrmat = new WritableCellFormat(NumberFormats.TEXT);
		try {
			contentFrmat.setFont(new WritableFont(WritableFont.createFont("Arial"), 10, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, colour));
			contentFrmat.setBorder(Border.ALL, BorderLineStyle.THIN);//表格线
		} catch (WriteException e) {
		}
		return contentFrmat;
	}

	//内容类型
	public static WritableCellFormat getContentStyle() {
		WritableCellFormat contentFrmat = new WritableCellFormat(NumberFormats.TEXT);
		try {
			contentFrmat.setBorder(Border.ALL, BorderLineStyle.THIN);//表格线
		} catch (WriteException e) {
		}
		return contentFrmat;
	}

	//内容类型
	public static WritableCellFormat getHeadContentStyle() {
		WritableCellFormat contentFrmat = new WritableCellFormat(NumberFormats.TEXT);
		try {
			contentFrmat.setAlignment(Alignment.CENTRE);
			contentFrmat.setVerticalAlignment(VerticalAlignment.CENTRE);
			contentFrmat.setBorder(Border.ALL, BorderLineStyle.THIN);//表格线
		} catch (WriteException e) {
		}
		return contentFrmat;
	}

}
