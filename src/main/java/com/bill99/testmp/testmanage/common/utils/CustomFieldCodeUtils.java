package com.bill99.testmp.testmanage.common.utils;

public class CustomFieldCodeUtils {

	public static final Integer CUSTOMFIELE_10404 = 10404;//上线ID
	public static final Integer CUSTOMFIELE_10122 = 10122;//产品文档URL
	public static final Integer CUSTOMFIELE_10123 = 10123;//产品文档REV
	public static final Integer CUSTOMFIELE_10124 = 10124;//需求文档URL
	public static final Integer CUSTOMFIELE_10125 = 10125;//测试文档 URL
	public static final Integer CUSTOMFIELE_10126 = 10126;//需求文档REV	
	public static final Integer CUSTOMFIELE_10127 = 10127;//测试文档REV	
	public static final Integer CUSTOMFIELE_10514 = 10514;//期望上线时间
	public static final Integer CUSTOMFIELE_10802 = 10802;//需求文档SVN路径
	public static final Integer CUSTOMFIELE_10508 = 10508;//开发人员
	public static final Integer CUSTOMFIELE_10515 = 10515;//测试人员
	public static final Integer CUSTOMFIELE_10519 = 10519;//需求确认人
	public static final Integer CUSTOMFIELE_10520 = 10520;//项目负责人
	public static final Integer CUSTOMFIELE_10400 = 10400;//修改范围

}
