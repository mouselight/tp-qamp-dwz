package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TpTaskResponesBo implements Serializable {

	private static final long serialVersionUID = 6511656810554573258L;
	private Boolean result;
	private String memo;

	public TpTaskResponesBo() {
	}

	public TpTaskResponesBo(Boolean result, String memo) {
		this.result = result;
		this.memo = memo;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}
