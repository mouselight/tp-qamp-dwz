package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TpTaskRequestBo implements Serializable {

	private static final long serialVersionUID = 52646171700294161L;

	private String jobName;
	private String tpUsers;
	private String tpRuntime;
	private String publishVersion;
	private String tpTestinterface;
	//以上必填

	private String tpTesttype;
	private String tpTestdesc;
	private String tpRemark;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getTpUsers() {
		return tpUsers;
	}

	public void setTpUsers(String tpUsers) {
		this.tpUsers = tpUsers;
	}

	public String getTpRuntime() {
		return tpRuntime;
	}

	public void setTpRuntime(String tpRuntime) {
		this.tpRuntime = tpRuntime;
	}

	public String getPublishVersion() {
		return publishVersion;
	}

	public void setPublishVersion(String publishVersion) {
		this.publishVersion = publishVersion;
	}

	public String getTpTesttype() {
		return tpTesttype;
	}

	public void setTpTesttype(String tpTesttype) {
		this.tpTesttype = tpTesttype;
	}

	public String getTpTestinterface() {
		return tpTestinterface;
	}

	public void setTpTestinterface(String tpTestinterface) {
		this.tpTestinterface = tpTestinterface;
	}

	public String getTpTestdesc() {
		return tpTestdesc;
	}

	public void setTpTestdesc(String tpTestdesc) {
		this.tpTestdesc = tpTestdesc;
	}

	public String getTpRemark() {
		return tpRemark;
	}

	public void setTpRemark(String tpRemark) {
		this.tpRemark = tpRemark;
	}

}
