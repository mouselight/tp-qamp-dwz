package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class MustStepDto implements Serializable {

	private static final long serialVersionUID = -2273064748341888800L;
	private Long stepId;
	private float teamId;
	public Long getStepId() {
		return stepId;
	}
	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}
	public float getTeamId() {
		return teamId;
	}
	public void setTeamId(float teamId) {
		this.teamId = teamId;
	}




}
