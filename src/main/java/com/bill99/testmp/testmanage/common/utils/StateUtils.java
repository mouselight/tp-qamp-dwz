package com.bill99.testmp.testmanage.common.utils;

import java.util.HashMap;
import java.util.Map;

public class StateUtils {

	public static final int STATUS_ASSOTC_INIT = 1;//未执行
	public static final int STATUS_ASSOTC_SUCC = 2;//执行成功
	public static final int STATUS_ASSOTC_FAIL = 3;//执行失败

	public static final String STATUS_ASSOTC_INIT_DESC = "未执行";
	public static final String STATUS_ASSOTC_SUCC_DESC = "成功";
	public static final String STATUS_ASSOTC_FAIL_DESC = "失败";

	public static Map<Integer, String> getTcStatusMap() {
		Map<Integer, String> tcStatusMap = new HashMap<Integer, String>();
		tcStatusMap.put(STATUS_ASSOTC_INIT, STATUS_ASSOTC_INIT_DESC);
		tcStatusMap.put(STATUS_ASSOTC_SUCC, STATUS_ASSOTC_SUCC_DESC);
		tcStatusMap.put(STATUS_ASSOTC_FAIL, STATUS_ASSOTC_FAIL_DESC);
		return tcStatusMap;
	}
}
