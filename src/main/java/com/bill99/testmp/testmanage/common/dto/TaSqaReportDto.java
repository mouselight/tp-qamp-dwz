package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.util.List;

public class TaSqaReportDto implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6125737312375813722L;
	
	private List<String> cycles;/* cycle 号 */
	private String projectname;/* 上线项目组 */
	private Long projectnameid;/*上线项目组id*/
	private Long levelallissue;
	
	private Long alllevelbugsnum;   /*bug级别总数*/
	private Long alllevelbugsnumdev; /*bug级别stage02总数*/
	private Long alllevelbugsnumstage02; /*bug级别stage02总数*/
	private Long alllevelbugsnumpro; /*bug级别pro总数*/
	
	private Long blockerbugsnum;/*block bug 数*/
	private Long criticalbugsnum;/*严重 bug 数*/
	private Long majorbugsnum;/*重要bug数*/
	private Long minorbugnum;/*小bug数*/
	private Long trivialbugnum;/*琐碎bug数*/
	
	private Long blockerbugsnumdev;/*block bug 数*/
	private Long criticalbugsnumdev;/*严重 bug 数*/
	private Long majorbugsnumdev;/*重要bug数*/
	private Long minorbugnumdev;/*小bug数*/
	private Long trivialbugnumdev;/*琐碎bug数*/
	
	private Long blockerbugsnumstage02;/*block bug 数*/
	private Long criticalbugsnumstage02;/*严重 bug 数*/
	private Long majorbugsnumstage02;/*重要bug数*/
	private Long minorbugnumstage02;/*小bug数*/
	private Long trivialbugnumstage02;/*琐碎bug数*/
	
	private Long blockerbugsnumpro;/*block bug 数*/
	private Long criticalbugsnumpro;/*严重 bug 数*/
	private Long majorbugsnumpro;/*重要bug数*/
	private Long minorbugnumpro;/*小bug数*/
	private Long trivialbugnumpro;/*琐碎bug数*/
	
	
/*	private Long categoryallissue;*/
	
	private Long allcategorybugsnum;   /*bug级别总数*/
	
	private Long allcategorybugsnumdev;   /*bug级别总数*/
	private Long allcategorybugsnumstage02;   /*bug级别总数*/
	private Long allcategorybugsnumpro;   /*bug级别总数*/
	
	
	private Long reqbugsnum;/*需求类bug 数*/
	private Long codebugsnum;/*代码类bug数*/
	private Long designbugsnum;/*设计类 bug 数*/
	private Long envbugsnum;/*环境类bug数*/
	private Long otherbugsnum;/*其他类bug数*/
	

	
	private Long reqbugsnumdev;/*需求类bug 数*/
	private Long codebugsnumdev;/*代码类bug数*/
	private Long designbugsnumdev;/*设计类 bug 数*/
	private Long envbugsnumdev;/*环境类bug数*/
	private Long otherbugsnumdev;/*其他类bug数*/
	
	
	
	private Long reqbugsnumstage02;/*需求类bug 数*/
	private Long codebugsnumstage02;/*代码类bug数*/
	private Long designbugsnumstage02;/*设计类 bug 数*/
	private Long envbugsnumstage02;/*环境类bug数*/
	private Long otherbugsnumstage02;/*其他类bug数*/
	
	
	private Long reqbugsnumpro;/*需求类bug 数*/
	private Long codebugsnumpro;/*代码类bug数*/
	private Long designbugsnumpro;/*设计类 bug 数*/
	private Long envbugsnumpro;/*环境类bug数*/
	private Long otherbugsnumpro;/*其他类bug数*/
	
	
	private Long  allissuestatusnum; /**/
	private Long  allbugsstatusnum;
	private Long  openbugsnum;
	private Long  rejectbugsnum;
	private Long  fixedbugsnum;
	private Long  reopenbugsnum;
	private Long  closedbugsnum;
	private Long  laterbugsnum;

	private Long  allbugsenvnum;
	private Long  devbugsnum;
	private Long  stage02bugsnum;
	private Long  probugsnum;
	
	
	public List<String> getCycles() {
		return cycles;
	}
	public void setCycles(List<String> cycles) {
		this.cycles = cycles;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public Long getProjectnameid() {
		return projectnameid;
	}
	public void setProjectnameid(Long projectnameid) {
		this.projectnameid = projectnameid;
	}
	public Long getBlockerbugsnum() {
		
		return blockerbugsnum;
	}
	public void setBlockerbugsnum(Long blockerbugsnum) {
		this.blockerbugsnum = blockerbugsnum;
	}
	public Long getCriticalbugsnum() {
		return criticalbugsnum;
	}
	public void setCriticalbugsnum(Long criticalbugsnum) {
		this.criticalbugsnum = criticalbugsnum;
	}
	public Long getMajorbugsnum() {
		return majorbugsnum;
	}
	public void setMajorbugsnum(Long majorbugsnum) {
		this.majorbugsnum = majorbugsnum;
	}
	public Long getAlllevelbugsnum() {
		if(alllevelbugsnum == null){
			alllevelbugsnum = 0l;
		}
		return alllevelbugsnum;
	}
	public void setAlllevelbugsnum(Long alllevelbugsnum) {
		this.alllevelbugsnum = alllevelbugsnum;
	}
	public Long getReqbugsnum() {
		return reqbugsnum;
	}
	public void setReqbugsnum(Long reqbugsnum) {
		this.reqbugsnum = reqbugsnum;
	}
	public Long getCodebugsnum() {
		return codebugsnum;
	}
	public void setCodebugsnum(Long codebugsnum) {
		this.codebugsnum = codebugsnum;
	}
	public Long getDesignbugsnum() {
		return designbugsnum;
	}
	public void setDesignbugsnum(Long designbugsnum) {
		this.designbugsnum = designbugsnum;
	}
	public Long getEnvbugsnum() {
		return envbugsnum;
	}
	public void setEnvbugsnum(Long envbugsnum) {
		this.envbugsnum = envbugsnum;
	}
	public Long getOtherbugsnum() {
		return otherbugsnum;
	}
	public void setOtherbugsnum(Long otherbugsnum) {
		this.otherbugsnum = otherbugsnum;
	}
	public Long getAllcategorybugsnum() {
		return allcategorybugsnum;
	}
	public void setAllcategorybugsnum(Long allcategorybugsnum) {
		this.allcategorybugsnum = allcategorybugsnum;
	}
	public Long getBlockerbugsnumdev() {
		if(blockerbugsnumdev == null){
			blockerbugsnumdev = 0l;
		}
		return blockerbugsnumdev;
	}
	public void setBlockerbugsnumdev(Long blockerbugsnumdev) {
		this.blockerbugsnumdev = blockerbugsnumdev;
	}
	public Long getCriticalbugsnumdev() {
		if(criticalbugsnumdev == null){
			criticalbugsnumdev = 0l;
		}
		return criticalbugsnumdev;
	}
	public void setCriticalbugsnumdev(Long criticalbugsnumdev) {
		this.criticalbugsnumdev = criticalbugsnumdev;
	}
	public Long getMajorbugsnumdev() {
		if(majorbugsnumdev == null){
			majorbugsnumdev = 0l;
		}
		return majorbugsnumdev;
	}
	public void setMajorbugsnumdev(Long majorbugsnumdev) {
		this.majorbugsnumdev = majorbugsnumdev;
	}
	public Long getBlockerbugsnumstage02() {
		if(blockerbugsnumstage02 == null){
			blockerbugsnumstage02 = 0l;
		}
		return blockerbugsnumstage02;
	}
	public void setBlockerbugsnumstage02(Long blockerbugsnumstage02) {
		this.blockerbugsnumstage02 = blockerbugsnumstage02;
	}
	public Long getCriticalbugsnumstage02() {
		if(criticalbugsnumstage02 == null){
			criticalbugsnumstage02 = 0l;
		}
		return criticalbugsnumstage02;
	}
	public void setCriticalbugsnumstage02(Long criticalbugsnumstage02) {
		this.criticalbugsnumstage02 = criticalbugsnumstage02;
	}
	public Long getMajorbugsnumstage02() {
		if(majorbugsnumstage02 == null){
			majorbugsnumstage02 = 0l;
		}
		return majorbugsnumstage02;
	}
	public void setMajorbugsnumstage02(Long majorbugsnumstage02) {
		this.majorbugsnumstage02 = majorbugsnumstage02;
	}
	public Long getBlockerbugsnumpro() {
		if(blockerbugsnumpro == null){
			blockerbugsnumpro = 0l;
		}
		return blockerbugsnumpro;
	}
	public void setBlockerbugsnumpro(Long blockerbugsnumpro) {
		this.blockerbugsnumpro = blockerbugsnumpro;
	}
	public Long getCriticalbugsnumpro() {
		if(criticalbugsnumpro == null){
			criticalbugsnumpro = 0l;
		}
		return criticalbugsnumpro;
	}
	public void setCriticalbugsnumpro(Long criticalbugsnumpro) {
		this.criticalbugsnumpro = criticalbugsnumpro;
	}
	public Long getMajorbugsnumpro() {
		if(majorbugsnumpro == null){
			majorbugsnumpro = 0l;
		}
		return majorbugsnumpro;
	}
	public void setMajorbugsnumpro(Long majorbugsnumpro) {
		this.majorbugsnumpro = majorbugsnumpro;
	}
	public Long getReqbugsnumdev() {
		return reqbugsnumdev;
	}
	public void setReqbugsnumdev(Long reqbugsnumdev) {
		this.reqbugsnumdev = reqbugsnumdev;
	}
	public Long getCodebugsnumdev() {
		return codebugsnumdev;
	}
	public void setCodebugsnumdev(Long codebugsnumdev) {
		this.codebugsnumdev = codebugsnumdev;
	}
	public Long getDesignbugsnumdev() {
		return designbugsnumdev;
	}
	public void setDesignbugsnumdev(Long designbugsnumdev) {
		this.designbugsnumdev = designbugsnumdev;
	}
	public Long getEnvbugsnumdev() {
		return envbugsnumdev;
	}
	public void setEnvbugsnumdev(Long envbugsnumdev) {
		this.envbugsnumdev = envbugsnumdev;
	}
	
	/***/
	public Long getOtherbugsnumdev() {
		if(otherbugsnumdev == null){
			otherbugsnumdev = 0l;
		}
		return otherbugsnumdev;
	}
	public void setOtherbugsnumdev(Long otherbugsnumdev) {
		this.otherbugsnumdev = otherbugsnumdev;
	}
	public Long getReqbugsnumstage02() {
		return reqbugsnumstage02;
	}
	public void setReqbugsnumstage02(Long reqbugsnumstage02) {
		this.reqbugsnumstage02 = reqbugsnumstage02;
	}
	public Long getCodebugsnumstage02() {
		return codebugsnumstage02;
	}
	public void setCodebugsnumstage02(Long codebugsnumstage02) {
		this.codebugsnumstage02 = codebugsnumstage02;
	}
	public Long getDesignbugsnumstage02() {
		return designbugsnumstage02;
	}
	public void setDesignbugsnumstage02(Long designbugsnumstage02) {
		this.designbugsnumstage02 = designbugsnumstage02;
	}
	public Long getEnvbugsnumstage02() {
		return envbugsnumstage02;
	}
	public void setEnvbugsnumstage02(Long envbugsnumstage02) {
		this.envbugsnumstage02 = envbugsnumstage02;
	}
	
	
	public Long getOtherbugsnumstage02() {
		if(otherbugsnumstage02 == null){
			otherbugsnumstage02 = 0l;
		}
		return otherbugsnumstage02;
	}
	public void setOtherbugsnumstage02(Long otherbugsnumstage02) {
		this.otherbugsnumstage02 = otherbugsnumstage02;
	}
	public Long getReqbugsnumpro() {
		return reqbugsnumpro;
	}
	public void setReqbugsnumpro(Long reqbugsnumpro) {
		this.reqbugsnumpro = reqbugsnumpro;
	}
	public Long getCodebugsnumpro() {
		return codebugsnumpro;
	}
	public void setCodebugsnumpro(Long codebugsnumpro) {
		this.codebugsnumpro = codebugsnumpro;
	}
	public Long getDesignbugsnumpro() {
		return designbugsnumpro;
	}
	public void setDesignbugsnumpro(Long designbugsnumpro) {
		this.designbugsnumpro = designbugsnumpro;
	}
	public Long getEnvbugsnumpro() {
		return envbugsnumpro;
	}
	public void setEnvbugsnumpro(Long envbugsnumpro) {
		this.envbugsnumpro = envbugsnumpro;
	}
	
	public Long getOtherbugsnumpro() {
		if(otherbugsnumpro == null){
			otherbugsnumpro = 0l;
		}
		return otherbugsnumpro;
	}
	public void setOtherbugsnumpro(Long otherbugsnumpro) {
		this.otherbugsnumpro = otherbugsnumpro;
	}
	public Long getAlllevelbugsnumdev() {
		return alllevelbugsnumdev;
	}
	public void setAlllevelbugsnumdev(Long alllevelbugsnumdev) {
		this.alllevelbugsnumdev = alllevelbugsnumdev;
	}
	public Long getAlllevelbugsnumstage02() {
		return alllevelbugsnumstage02;
	}
	public void setAlllevelbugsnumstage02(Long alllevelbugsnumstage02) {
		this.alllevelbugsnumstage02 = alllevelbugsnumstage02;
	}
	public Long getAlllevelbugsnumpro() {
		return alllevelbugsnumpro;
	}
	public void setAlllevelbugsnumpro(Long alllevelbugsnumpro) {
		this.alllevelbugsnumpro = alllevelbugsnumpro;
	}
	public Long getAllcategorybugsnumdev() {
		return allcategorybugsnumdev;
	}
	public void setAllcategorybugsnumdev(Long allcategorybugsnumdev) {
		this.allcategorybugsnumdev = allcategorybugsnumdev;
	}
	public Long getAllcategorybugsnumstage02() {
		return allcategorybugsnumstage02;
	}
	public void setAllcategorybugsnumstage02(Long allcategorybugsnumstage02) {
		this.allcategorybugsnumstage02 = allcategorybugsnumstage02;
	}
	public Long getAllcategorybugsnumpro() {
		return allcategorybugsnumpro;
	}
	public void setAllcategorybugsnumpro(Long allcategorybugsnumpro) {
		this.allcategorybugsnumpro = allcategorybugsnumpro;
	}
	public Long getLevelallissue() {
		return levelallissue;
	}
	public void setLevelallissue(Long levelallissue) {
		this.levelallissue = levelallissue;
	}
	public Long getMinorbugnum() {
		return minorbugnum;
	}
	public void setMinorbugnum(Long minorbugnum) {
		this.minorbugnum = minorbugnum;
	}
	public Long getTrivialbugnum() {
		return trivialbugnum;
	}
	public void setTrivialbugnum(Long trivialbugnum) {
		this.trivialbugnum = trivialbugnum;
	}
	public Long getMinorbugnumdev() {
		return minorbugnumdev;
	}
	public void setMinorbugnumdev(Long minorbugnumdev) {
		this.minorbugnumdev = minorbugnumdev;
	}
	public Long getTrivialbugnumdev() {
		return trivialbugnumdev;
	}
	public void setTrivialbugnumdev(Long trivialbugnumdev) {
		this.trivialbugnumdev = trivialbugnumdev;
	}
	public Long getMinorbugnumstage02() {
		return minorbugnumstage02;
	}
	public void setMinorbugnumstage02(Long minorbugnumstage02) {
		this.minorbugnumstage02 = minorbugnumstage02;
	}
	public Long getTrivialbugnumstage02() {
		return trivialbugnumstage02;
	}
	public void setTrivialbugnumstage02(Long trivialbugnumstage02) {
		this.trivialbugnumstage02 = trivialbugnumstage02;
	}
	public Long getMinorbugnumpro() {
		return minorbugnumpro;
	}
	public void setMinorbugnumpro(Long minorbugnumpro) {
		this.minorbugnumpro = minorbugnumpro;
	}
	public Long getTrivialbugnumpro() {
		return trivialbugnumpro;
	}
	public void setTrivialbugnumpro(Long trivialbugnumpro) {
		this.trivialbugnumpro = trivialbugnumpro;
	}
	public Long getAllissuestatusnum() {
		return allissuestatusnum;
	}
	public void setAllissuestatusnum(Long allissuestatusnum) {
		this.allissuestatusnum = allissuestatusnum;
	}
	public Long getAllbugsstatusnum() {
		return allbugsstatusnum;
	}
	public void setAllbugsstatusnum(Long allbugsstatusnum) {
		this.allbugsstatusnum = allbugsstatusnum;
	}
	public Long getOpenbugsnum() {
		return openbugsnum;
	}
	public void setOpenbugsnum(Long openbugsnum) {
		this.openbugsnum = openbugsnum;
	}
	public Long getRejectbugsnum() {
		return rejectbugsnum;
	}
	public void setRejectbugsnum(Long rejectbugsnum) {
		this.rejectbugsnum = rejectbugsnum;
	}
	public Long getFixedbugsnum() {
		return fixedbugsnum;
	}
	public void setFixedbugsnum(Long fixedbugsnum) {
		this.fixedbugsnum = fixedbugsnum;
	}
	public Long getReopenbugsnum() {
		return reopenbugsnum;
	}
	public void setReopenbugsnum(Long reopenbugsnum) {
		this.reopenbugsnum = reopenbugsnum;
	}
	public Long getClosedbugsnum() {
		return closedbugsnum;
	}
	public void setClosedbugsnum(Long closedbugsnum) {
		this.closedbugsnum = closedbugsnum;
	}
	public Long getLaterbugsnum() {
		return laterbugsnum;
	}
	public void setLaterbugsnum(Long laterbugsnum) {
		this.laterbugsnum = laterbugsnum;
	}
	public Long getAllbugsenvnum() {
		return allbugsenvnum;
	}
	public void setAllbugsenvnum(Long allbugsenvnum) {
		this.allbugsenvnum = allbugsenvnum;
	}
	public Long getDevbugsnum() {
		return devbugsnum;
	}
	public void setDevbugsnum(Long devbugsnum) {
		this.devbugsnum = devbugsnum;
	}
	public Long getStage02bugsnum() {
		return stage02bugsnum;
	}
	public void setStage02bugsnum(Long stage02bugsnum) {
		this.stage02bugsnum = stage02bugsnum;
	}
	public Long getProbugsnum() {
		return probugsnum;
	}
	public void setProbugsnum(Long probugsnum) {
		this.probugsnum = probugsnum;
	}
	

}
