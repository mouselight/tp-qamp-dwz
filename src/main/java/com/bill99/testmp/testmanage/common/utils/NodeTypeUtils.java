package com.bill99.testmp.testmanage.common.utils;

public class NodeTypeUtils {

	public final static long TESTPROJECT = 1L;
	public final static long TESTSUITE = 2L;
	public final static long TESTCASE = 3L;
	public final static long TESTCASE_VERSION = 4L;
	public final static long TESTPLAN = 5L;
	public final static long REQUIREMENT_SPEC = 6L;
	public final static long REQUIREMENT = 7L;
	public final static long REQUIREMENT_VERSION = 8L;
	public final static long TESTCASE_STEP = 9L;
	public final static long REQUIREMENT_REVISION = 10L;

}
