package com.bill99.testmp.testmanage.common.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.testmp.testmanage.orm.entity.TestProjects;

public class TreeUtils {

	static {
		tcFilterMap = new HashMap<String, List<Long>>();
		nodeFilterMap = new HashMap<String, List<Long>>();
		nodeTestProjectMap = new HashMap<String, List<TestProjects>>();
	}

	public static Map<String, String> TC_QUANTITY_MAP = new HashMap<String, String>();

	public static Integer CHILD_NODE = 1;
	public static Integer FATHER_NODE = 2;
	public static Integer FATHER_TOP_NODE = 3;//根节点下第一层
	public static Integer ROOT_NODE = 4;//根节点

	//节点名称 分隔
	public static String NODE_NAME_SPILT = "-";

	//TC-Filter
	public static final String TC_FILTER_KEY_SUFFIX = ".t";
	public static final String NODE_FILTER_KEY_SUFFIX = ".f";
	public static final String PROJECT_FILTER_KEY_SUFFIX = ".p";
	public static final String TC_FILTER_EXETYPE_KEY_SUFFIX = ".exetype";

	public static Map<String, List<Long>> tcFilterMap;
	public static Map<String, List<Long>> nodeFilterMap;
	public static Map<String, List<TestProjects>> nodeTestProjectMap;

	public static final String TC_FILETER_COMMAND_KEY = "tc_filter_key";
}
