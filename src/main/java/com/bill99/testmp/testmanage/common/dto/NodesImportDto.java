package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class NodesImportDto implements Serializable {
	private static final long serialVersionUID = -438031682263802985L;
	private Long nodeId;
	private String nodeName;

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

}
