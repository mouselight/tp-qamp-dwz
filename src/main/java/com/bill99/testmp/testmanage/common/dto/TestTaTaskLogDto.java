package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class TestTaTaskLogDto implements Serializable {

	private static final long serialVersionUID = -8389672165667931323L;

	private Long idTestTaTask;
	private String taTaskId;
	private Long testPlanId;
	private Long testProjectId;
	private Boolean isActual;
	private Date triggerTime;
	private Boolean state;
	private String memo;
	private String createUser;
	private Date createDate;

	private Long idBuild;
	private Long idTaTask;
	private String jobName;//job
	private String buildNum;
	private String buildId;
	private String buildMemo;
	private String errorMsg;//错误信息
	private Date finishDate;
	private Integer buildState;
	private Long idMethod;

	private Long costMillis;

	//TC执行统计
	private Integer allCnt;
	private Integer doneCnt;
	private Integer undoneCnt;
	private Integer succCnt;
	private Integer failCnt;

	private String coverPer;
	private String passPer;
	private Long planStepId;

	private String projectName;
	private String planMemo;
	private String issueMemo;

	public void setCoverPer(String coverPer) {
		this.coverPer = coverPer;
	}

	public String getCoverPer() {
		if (getAllCnt() != null && getDoneCnt() != null && getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			coverPer = df1.format(getDoneCnt().floatValue() / getAllCnt().floatValue());
		}
		return coverPer;
	}

	public void setPassPer(String passPer) {
		this.passPer = passPer;
	}

	public String getPassPer() {
		if (getAllCnt() != null && getSuccCnt() != null && getAllCnt().intValue() > 0) {
			DecimalFormat df1 = new DecimalFormat("#0.##%");
			passPer = df1.format(getSuccCnt().floatValue() / getAllCnt().floatValue());
		}
		return passPer;
	}

	public Long getIdTestTaTask() {
		return idTestTaTask;
	}

	public Integer getAllCnt() {
		return allCnt;
	}

	public void setAllCnt(Integer allCnt) {
		this.allCnt = allCnt;
	}

	public Integer getDoneCnt() {
		return doneCnt;
	}

	public void setDoneCnt(Integer doneCnt) {
		this.doneCnt = doneCnt;
	}

	public Integer getUndoneCnt() {
		return undoneCnt;
	}

	public void setUndoneCnt(Integer undoneCnt) {
		this.undoneCnt = undoneCnt;
	}

	public Integer getSuccCnt() {
		return succCnt;
	}

	public void setSuccCnt(Integer succCnt) {
		this.succCnt = succCnt;
	}

	public Integer getFailCnt() {
		return failCnt;
	}

	public void setFailCnt(Integer failCnt) {
		this.failCnt = failCnt;
	}

	public void setIdTestTaTask(Long idTestTaTask) {
		this.idTestTaTask = idTestTaTask;
	}

	public Long getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(Long testPlanId) {
		this.testPlanId = testPlanId;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Boolean getIsActual() {
		return isActual;
	}

	public void setIsActual(Boolean isActual) {
		this.isActual = isActual;
	}

	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getTaTaskId() {
		return taTaskId;
	}

	public void setTaTaskId(String taTaskId) {
		this.taTaskId = taTaskId;
	}

	public Long getIdBuild() {
		return idBuild;
	}

	public void setIdBuild(Long idBuild) {
		this.idBuild = idBuild;
	}

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getBuildNum() {
		return buildNum;
	}

	public void setBuildNum(String buildNum) {
		this.buildNum = buildNum;
	}

	public String getBuildId() {
		return buildId;
	}

	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public String getBuildMemo() {
		return buildMemo;
	}

	public void setBuildMemo(String buildMemo) {
		this.buildMemo = buildMemo;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public Integer getBuildState() {
		return buildState;
	}

	public void setBuildState(Integer buildState) {
		this.buildState = buildState;
	}

	public Long getIdMethod() {
		return idMethod;
	}

	public void setIdMethod(Long idMethod) {
		this.idMethod = idMethod;
	}

	public Long getCostMillis() {
		if (createDate != null && finishDate != null) {
			return (finishDate.getTime() - createDate.getTime()) / 1000;
		}

		return costMillis;
	}

	public void setCostMillis(Long costMillis) {
		this.costMillis = costMillis;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPlanMemo() {
		return planMemo;
	}

	public void setPlanMemo(String planMemo) {
		this.planMemo = planMemo;
	}

	public String getIssueMemo() {
		return issueMemo;
	}

	public void setIssueMemo(String issueMemo) {
		this.issueMemo = issueMemo;
	}

}
