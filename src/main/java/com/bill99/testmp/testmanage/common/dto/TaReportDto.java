package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public class TaReportDto implements Serializable {

	private static final long serialVersionUID = 3648489259860928721L;
	private List<String> cycles;/* cycle 号 */
	private String prefix;/* 上线项目组 */
	private Long id;/*上线项目组id*/
	private Long teamid;/*jiar上线项目组id*/
//	private Float precent; /*02环境自动化执行比例*/
	private Long tcplannum;/* 上线项目数 */
	private Long issuenum;/* 上线需求数 */
	private List<TestCycleDto> testCycleDto;
	
	private Long casetotalnum;/*case 总数*/
	private Long arcasetotalnum;/*人工执行case 总数*/
	private Long stage02autocasesuccnum;/*02环境自动化case 执行成功总数*/
	private Long stage02casetotalnum;/*02环境环境case 总数*/
	private Long stage02autocasenum;/*02环境自动化case总数*/

	private Date cycleDate;
	
	
	/*private String casetotalnum;case 总数
	private String arcasetotalnum;人工执行case 总数
	private String stage02autocasenum;自动化case 总数
	private String stage02casetotalnum;集成环境case 总数*/

	
	
	

	/*public Float getPrecent() {	
		if(stage02casetotalnum == 0||stage02casetotalnum == null||stage02autocasenum == null){
			precent =0.00f;
		}
		else{
			BigDecimal bigstage02autocasenum = new BigDecimal(stage02autocasenum);
			BigDecimal bigstage02casetotalnum = new BigDecimal(stage02casetotalnum);
			precent =  bigstage02autocasenum.divide(bigstage02casetotalnum, 2, 2).floatValue();
		}
		return precent;
	}
	public void setPrecent(float precent) {
		this.precent = precent;
	}*/
	
	public Long getTeamid() {
		return teamid;
	}

	public void setTeamid(Long teamid) {
		this.teamid = teamid;
	}

	public Long getIssuenum() {
		return issuenum;
	}

	public void setIssuenum(Long issuenum) {
		this.issuenum = issuenum;
	}

	
	

	public Long getStage02autocasesuccnum() {
		return stage02autocasesuccnum;
	}

	public void setStage02autocasesuccnum(Long stage02autocasesuccnum) {
		this.stage02autocasesuccnum = stage02autocasesuccnum;
	}
	public Date getCycleDate() {
		return cycleDate;
	}

	public void setCycleDate(Date cycleDate) {
		this.cycleDate = cycleDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCasetotalnum() {
		return casetotalnum;
	}

	public void setCasetotalnum(Long casetotalnum) {
		this.casetotalnum = casetotalnum;
	}

	public Long getArcasetotalnum() {
		return arcasetotalnum;
	}

	public void setArcasetotalnum(Long arcasetotalnum) {
		this.arcasetotalnum = arcasetotalnum;
	}

	public Long getStage02autocasenum() {
		return stage02autocasenum;
	}

	public void setStage02autocasenum(Long stage02autocasenum) {
		this.stage02autocasenum = stage02autocasenum;
	}

	public Long getStage02casetotalnum() {
		return stage02casetotalnum;
	}

	public void setStage02casetotalnum(Long stage02casetotalnum) {
		this.stage02casetotalnum = stage02casetotalnum;
	}


	

	public List<String> getCycles() {
		return cycles;
	}

	public void setCycles(List<String> cycles) {
		this.cycles = cycles;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Long getTcplannum() {
		return tcplannum;
	}

	public void setTcplannum(Long tcplannum) {
		this.tcplannum = tcplannum;
	}

	public List<TestCycleDto> getTestCycleDto() {
		return testCycleDto;
	}

	public void setTestCycleDto(List<TestCycleDto> testCycleDto) {
		this.testCycleDto = testCycleDto;
	}
    
	/*public static void main(String[] args) {
		TaReportDto dto = new TaReportDto();
		long stage02autocasenum =652;
		long stage02casetotalnum = 325;
		dto.setStage02autocasenum(stage02autocasenum);
		dto.setStage02casetotalnum(stage02casetotalnum);
		System.out.println("" + dto.getStage02autocasenum());
		System.out.println("" + dto.getStage02casetotalnum());

	}*/
	

}
