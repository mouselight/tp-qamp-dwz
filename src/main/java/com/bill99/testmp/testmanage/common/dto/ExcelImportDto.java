package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TestCases;

public class ExcelImportDto implements Serializable {

	private static final long serialVersionUID = -1550023536097945884L;
	private Long testSuiteId;
	private String actions;
	private String expectedResults;

	private List<TestCases> testCasesList;

	public Long getTestSuiteId() {
		return testSuiteId;
	}

	public void setTestSuiteId(Long testSuiteId) {
		this.testSuiteId = testSuiteId;
	}

	public String getActions() {
		return actions;
	}

	public void setActions(String actions) {
		this.actions = actions;
	}

	public String getExpectedResults() {
		return expectedResults;
	}

	public void setExpectedResults(String expectedResults) {
		this.expectedResults = expectedResults;
	}

	public List<TestCases> getTestCasesList() {
		return testCasesList;
	}

	public void setTestCasesList(List<TestCases> testCasesList) {
		this.testCasesList = testCasesList;
	}

}
