package com.bill99.testmp.testmanage.common.utils;

public class ProjectRoleUtils {

	public static final Long USERS = 10000L;//A project role that represents users in a project
	public static final Long DEVELOPERS = 10001L;//开发人员
	public static final Long ADMINISTRATORS = 10002L;//A project role that represents administrators in a project
	public static final Long TESTERS = 10200L;//测试人员
	public static final Long BUSINESS_ANALYSER = 10201L;//业务分析人员
	public static final Long PRODUCT_MANAGER = 10202L;//产品经理
	public static final Long SQA = 10203L;//软件质量保证人员
	public static final Long PROJECT_MANAGER = 10204L;//项目经理
	public static final Long TEST_LEADER = 10205L;//测试组长
	public static final Long UI_DESIGNER = 10206L;//用户界面设计人员
	public static final Long SCM = 10300L;//出包部署人员
	public static final Long RELEASE_MANAGER = 10400L;//Release Manager
	public static final Long LAB = 10401L;//

}
