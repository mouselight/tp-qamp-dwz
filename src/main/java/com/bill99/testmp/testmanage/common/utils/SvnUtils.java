package com.bill99.testmp.testmanage.common.utils;

import com.bill99.golden.inf.sso.util.CM;

public class SvnUtils {

	public static String concatPath(String p1, String p2) {
		if (CM.stringIsEmpty(p1))
			return p2;
		if (CM.stringIsEmpty(p2))
			return p1;
		if (p1.endsWith("/"))
			p1 = p1.substring(0, p1.length() - 1);
		if (p2.startsWith("/"))
			p2 = p2.substring(1);
		return p1 + "/" + p2;
	}

	public static String leftTrimPath(String p1) {
		if (CM.stringIsEmpty(p1))
			return "";
		if (p1.startsWith("/"))
			return p1.substring(1);
		else
			return p1;
	}

}
