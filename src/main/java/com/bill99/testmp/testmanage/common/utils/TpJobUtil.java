package com.bill99.testmp.testmanage.common.utils;

public class TpJobUtil {

	public static final String JOB_NAME = "job_name";
	public static final String TP_USERS = "tp.users";
	public static final String TP_RUNTIME = "tp.runtime";
	public static final String PUBLISH_VERSION = "publish.version";
	public static final String TP_TESTINTERFACE = "tp.testinterface";
	//以上必填

	public static final String TP_TESTTYPE = "tp.testtype";
	public static final String TP_TESTDESC = "tp.testdesc";
	public static final String TP_REMARK = "tp.remark";

}
