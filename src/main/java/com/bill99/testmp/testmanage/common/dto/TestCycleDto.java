package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TestCycleDto implements Serializable {

	private static final long serialVersionUID = -2573064748341887700L;
	private String cycle;
	private String jarset;


	

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getJarset() {
		return jarset;
	}

	public void setJarset(String jarset) {
		this.jarset = jarset;
	}

}
