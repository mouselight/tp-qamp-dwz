package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.util.Date;

public class TestNotifyBugDto implements Serializable {

	private static final long serialVersionUID = -5688763020686467781L;
	private Long issueId;
	private String issueSummary;
	private String issueDesc;
	private String issueStatus;
	private String issuePKey;
	private Integer issuePriority;
	private Long bugId;
	private String bugSummary;
	private String bugDesc;
	private String bugStatus;
	private String bugPriority;
	private String bugEnv;
	private Date bugCreateDate;

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getIssueSummary() {
		return issueSummary;
	}

	public void setIssueSummary(String issueSummary) {
		this.issueSummary = issueSummary;
	}

	public String getIssueDesc() {
		return issueDesc;
	}

	public void setIssueDesc(String issueDesc) {
		this.issueDesc = issueDesc;
	}

	public String getIssueStatus() {
		return issueStatus;
	}

	public void setIssueStatus(String issueStatus) {
		this.issueStatus = issueStatus;
	}

	public Integer getIssuePriority() {
		return issuePriority;
	}

	public void setIssuePriority(Integer issuePriority) {
		this.issuePriority = issuePriority;
	}

	public String getBugSummary() {
		return bugSummary;
	}

	public void setBugSummary(String bugSummary) {
		this.bugSummary = bugSummary;
	}

	public String getBugDesc() {
		return bugDesc;
	}

	public void setBugDesc(String bugDesc) {
		this.bugDesc = bugDesc;
	}

	public String getBugStatus() {
		return bugStatus;
	}

	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}

	public String getBugPriority() {
		return bugPriority;
	}

	public void setBugPriority(String bugPriority) {
		this.bugPriority = bugPriority;
	}

	public String getBugEnv() {
		return bugEnv;
	}

	public void setBugEnv(String bugEnv) {
		this.bugEnv = bugEnv;
	}

	public Date getBugCreateDate() {
		return bugCreateDate;
	}

	public void setBugCreateDate(Date bugCreateDate) {
		this.bugCreateDate = bugCreateDate;
	}

	public Long getBugId() {
		return bugId;
	}

	public void setBugId(Long bugId) {
		this.bugId = bugId;
	}

	public String getIssuePKey() {
		return issuePKey;
	}

	public void setIssuePKey(String issuePKey) {
		this.issuePKey = issuePKey;
	}

}
