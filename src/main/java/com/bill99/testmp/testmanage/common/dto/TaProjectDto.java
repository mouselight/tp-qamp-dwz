package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;
import java.util.Date;

public class TaProjectDto implements Serializable {

	private static final long serialVersionUID = -438031686303802985L;

	private Long idTaProject;
	private Long idTestProject;
	private String taSvnUrl;
	private String jobName;
	private Date createDate;
	private Date updateDate;

	public Long getIdTestProject() {
		return idTestProject;
	}

	public void setIdTestProject(Long idTestProject) {
		this.idTestProject = idTestProject;
	}

	public String getTaSvnUrl() {
		return taSvnUrl;
	}

	public void setTaSvnUrl(String taSvnUrl) {
		this.taSvnUrl = taSvnUrl;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Long getIdTaProject() {
		return idTaProject;
	}

	public void setIdTaProject(Long idTaProject) {
		this.idTaProject = idTaProject;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
