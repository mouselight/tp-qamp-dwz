package com.bill99.testmp.testmanage.common.dto;

import java.util.Date;
import java.util.List;

public class TestPlansDto {

	private Long planId;
	private Long issueId;
	private Date planBeginDate;
	private Date planEndDate;
	private Integer state;
	private String memo;
	private Long teamId;

	private List<Long> stepIds;
	private List<Integer> types;//审核步骤
	private List<Date> planBeginDates;//计划开始时间
	private List<Date> planEndDates;//计划结束时间
	private List<String> resources;//分配资源 userids
	private List<String> realnames;//分配资源
	private List<String> memos;//备注
	private List<String> outputs;//产出文档
	private List<Integer> stepStatus;

	private Date createTime;
	private Date updateTime;
	private String createUser;
	private String updateUser;

	private String unitType;

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Date getPlanBeginDate() {
		return planBeginDate;
	}

	public void setPlanBeginDate(Date planBeginDate) {
		this.planBeginDate = planBeginDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public List<Integer> getTypes() {
		return types;
	}

	public void setTypes(List<Integer> types) {
		this.types = types;
	}

	public List<Date> getPlanBeginDates() {
		return planBeginDates;
	}

	public void setPlanBeginDates(List<Date> planBeginDates) {
		this.planBeginDates = planBeginDates;
	}

	public List<Date> getPlanEndDates() {
		return planEndDates;
	}

	public void setPlanEndDates(List<Date> planEndDates) {
		this.planEndDates = planEndDates;
	}

	public List<String> getResources() {
		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

	public List<String> getMemos() {
		return memos;
	}

	public void setMemos(List<String> memos) {
		this.memos = memos;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemo() {
		return memo;
	}

	public void setRealnames(List<String> realnames) {
		this.realnames = realnames;
	}

	public List<String> getRealnames() {
		return realnames;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setStepStatus(List<Integer> stepStatus) {
		this.stepStatus = stepStatus;
	}

	public List<Integer> getStepStatus() {
		return stepStatus;
	}

	public void setOutputs(List<String> outputs) {
		this.outputs = outputs;
	}

	public List<String> getOutputs() {
		return outputs;
	}

	public void setStepIds(List<Long> stepIds) {
		this.stepIds = stepIds;
	}

	public List<Long> getStepIds() {
		return stepIds;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getPlanId() {
		return planId;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
}
