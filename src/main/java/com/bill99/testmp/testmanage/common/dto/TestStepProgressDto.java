package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TestStepProgressDto implements Serializable {

	private static final long serialVersionUID = 1683794487073246070L;

	private String userName;
	private Integer unDoneTcCount;
	private Integer successTcCount;
	private Integer failTcCount;
	private Integer allTcCount;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUnDoneTcCount() {
		return unDoneTcCount;
	}

	public void setUnDoneTcCount(Integer unDoneTcCount) {
		this.unDoneTcCount = unDoneTcCount;
	}

	public Integer getSuccessTcCount() {
		return successTcCount;
	}

	public void setSuccessTcCount(Integer successTcCount) {
		this.successTcCount = successTcCount;
	}

	public Integer getFailTcCount() {
		return failTcCount;
	}

	public void setFailTcCount(Integer failTcCount) {
		this.failTcCount = failTcCount;
	}

	public Integer getAllTcCount() {
		return allTcCount;
	}

	public void setAllTcCount(Integer allTcCount) {
		this.allTcCount = allTcCount;
	}

}
