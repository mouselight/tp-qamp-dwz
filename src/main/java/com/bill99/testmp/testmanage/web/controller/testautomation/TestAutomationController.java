package com.bill99.testmp.testmanage.web.controller.testautomation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.testmanage.common.dto.TaMethodDto;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestCaseCloudMng;
import com.bill99.testmp.testmanage.web.controller.command.TaAssoCaseCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestAutomationController {
	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestAutomationMng testAutomationMng;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TcFilterService tcFilterService;
	@Autowired
	private KeywordsMng keywordsMng;
	@Autowired
	private TestCaseCloudMng testCaseCloudMng;

	//测试用例SVN路径
	@RequestMapping(value = "/testautomation/list_method.htm")
	public String list_method(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession,
			TaMethodDto taMethodDto) {
		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		if (currentTestProject != null) {
			taMethodDto.setIdTestProject(currentTestProject.getId());

			List<TaMethodDto> taMethodDtos = testAutomationMng.getTaMethods(taMethodDto);
			Map<String, List<TaMethodDto>> taMethodMap = new TreeMap<String, List<TaMethodDto>>();
			for (TaMethodDto taMethod : taMethodDtos) {
				String className = taMethod.getClassName();
				List<TaMethodDto> taMethodsTmp = taMethodMap.get(className);
				if (taMethodsTmp != null) {
					taMethodsTmp.add(taMethod);
				} else {
					taMethodsTmp = new ArrayList<TaMethodDto>();
					taMethodsTmp.add(taMethod);
				}
				taMethodMap.put(className, taMethodsTmp);
			}
			//			model.addAttribute("listItems", testAutomationMng.getTaMethods(taMethodDto));
			model.addAttribute("mapItems", taMethodMap);
		}
		model.addAttribute("cmd", taMethodDto);

		return "testmanage/testautomation/list_method";
	}

	//测试用例SVN路径关联TC
	@RequestMapping(value = "/testautomation/list_method_tc.htm")
	public String list_method_tc(HttpServletRequest request, HttpServletResponse response, ModelMap model, Long idSvnMethod, HttpSession httpSession, TestManageCmd cmd) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		//清空session中条件限制
		httpSession.setAttribute(TreeUtils.TC_FILETER_COMMAND_KEY, new TestCasesQueryDto(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId()));
		//初始进入页面后清空 tc-filter
		tcFilterService.clearFilter(user.getId(), httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		Map<String, String> cloudNameMap = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
			cloudNameMap = testCaseCloudMng.getCloudNameMap(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getJiraTeemId());
		}

		model.addAttribute("keywordsList", keywordsList);
		model.addAttribute("cloudNameMap", cloudNameMap);
		model.addAttribute("cmd", cmd);
		model.addAttribute("taSvn", testAutomationMng.queryTaMethod(idSvnMethod));
		return "testmanage/testautomation/list_method_tc";
	}

	//添加关联操作
	@RequestMapping(value = "/testautomation/testcase_asso.htm")
	public void testcase_asso(HttpServletResponse response, HttpSession httpSession, TaAssoCaseCmd cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();
			//			Map<Integer, List<Long>> nodeMap = nodesHierarchyMng.getAllChildIds(StringUtil.strToLongList(cmd.getParentIds()));

			TestCasesQueryDto testCaseDto = (TestCasesQueryDto) httpSession.getAttribute(TreeUtils.TC_FILETER_COMMAND_KEY);
			nodeMap = tcFilterService.getFilterChildIds(StringUtil.strToLongList(cmd.getParentIds()), testCaseDto);

			testAutomationMng.saveAssoTaTc(cmd.getIdSvnMethod(), StringUtil.appendList(nodeMap.get(TreeUtils.CHILD_NODE), StringUtil.strToLongList(cmd.getTestCaseIds())),
					nodeMap.get(TreeUtils.FATHER_NODE));
		} catch (Exception e) {
			logger.error("--tc asso error , stepId = " + cmd.getIdSvnMethod() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "list_method_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testautomation/testcase_remove.htm")
	//移除操作-较特殊
	public void testcase_rv(HttpServletResponse response, TaAssoCaseCmd cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			//移除-所选所有节点的所有上层节点-(选中某个节点移除,意味着其上层节点不再是全选,需从关联树中移除,以便在未关联树中显示)
			List<Long> parentIds = nodesHierarchyMng.getAllParentIds(StringUtil.appendList(StringUtil.strToLongList(cmd.getTestCaseIds()),
					StringUtil.strToLongList(cmd.getParentIds())));
			//移除-所选节点的所有下层节点 - (文件夹全选后的所有子节点)
			Map<Integer, List<Long>> nodeMap = nodesHierarchyMng.getAllChildIds(StringUtil.strToLongList(cmd.getParentIds()));
			//移除-by tcId
			testAutomationMng.removeAssoTaTc(cmd.getIdSvnMethod(), StringUtil.appendList(nodeMap.get(TreeUtils.CHILD_NODE), StringUtil.strToLongList(cmd.getTestCaseIds())),
					StringUtil.appendList(nodeMap.get(TreeUtils.FATHER_NODE), parentIds));
		} catch (Exception e) {
			logger.error("--rv tc asso error , stepId = " + cmd.getIdSvnMethod() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "移除失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "移除成功！", "list_method_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
