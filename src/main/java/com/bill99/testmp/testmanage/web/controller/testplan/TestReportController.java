package com.bill99.testmp.testmanage.web.controller.testplan;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.enums.TestPlanStepEnum;
import com.bill99.testmp.framework.hession.QaInterface;
import com.bill99.testmp.framework.utils.PropertiesMapping;
import com.bill99.testmp.testmanage.domain.SvnService;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.entity.TestReportLog;
import com.bill99.testmp.testmanage.orm.manager.TestReportLogMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.caucho.hessian.client.HessianProxyFactory;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestReportController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestReportService testReportService;
	@Autowired
	private SvnService svnService;
	@Autowired
	private TestReportLogMng testReportLogMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private PropertiesMapping propertiesMapping;

	@RequestMapping("/testplan/test_report_index.htm")
	public String testReportIndex(TestManageCmd cmd, ModelMap model) {
		model.addAttribute("cmd", cmd);
		return "testmanage/testplan/report/report";
	}

	@RequestMapping("/testplan/test_report.htm")
	public void createTestReport(HttpServletResponse response, HttpSession httpSession, TestManageCmd cmd) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		AjaxDoneUtil ajaxDoneUtil = null;
		TestReportLog testReportLog = new TestReportLog();
		try {
			//clear tmp
			forceClear();
			//mkdir
			mkFileDir();
			TestReport testReport = new TestReport();
			BeanUtils.copyProperties(cmd, testReport);

			testReport.setStepType(cmd.getStepType());
			//tmp report
			String filePath = testReportService.testReport(response, testReport);
			//commit svn
			boolean svnCommitResult = svnCommit(filePath);
			//ctmp

			String svnPath = propertiesMapping.getSvnReportUrl() + "/" + propertiesMapping.getSvnReportPath() + "/"
					+ filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
			boolean result = sendCtmp(cmd.getReleaseId(), svnPath, cmd.getQaResult(), cmd.getMemo(), user.getUsername(), cmd.getStepType());
			//save log
			testReportLog.setSvnPath(svnPath);
			testReportLog.setCtmpSyncResult(result);
			testReportLog.setMemo(cmd.getMemo());
			testReportLog.setPlanId(cmd.getPlanId());
			testReportLog.setQaResult(cmd.getQaResult());
			testReportLog.setReleasId(cmd.getReleaseId());
			testReportLog.setStepId(cmd.getStepId());
			testReportLog.setSvnCommitResult(svnCommitResult);
			testReportLog.setCreateUser(user.getUsername());
			testReportLog.setCreateTime(new Date());
			testReportLogMng.save(testReportLog);
			//save url
			testPlanService.updateSvnUrl(cmd.getStepId(), svnPath);
			if (!result) {
				ajaxDoneUtil = new AjaxDoneUtil("300", "同步CTMP失败！", null, null, null, null);
				ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
				return;
			}
		} catch (Exception e) {
			logger.error("-- createTestReport error , stepId = " + cmd.getStepId() + " , issueId = " + cmd.getIssueId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "操作失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "报告已生成并同步CTMP！", "list_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	private void forceClear() {
		try {
			File file_ = new File(propertiesMapping.getReportTempJnaDir());
			if (!file_.exists()) {
				return;
			}
			FileUtils.deleteDirectory(file_);
		} catch (Exception e) {
			logger.info("delete " + propertiesMapping.getReportTempJnaDir() + " fail");
		}
	}

	private void mkFileDir() {
		File file_ = new File(propertiesMapping.getReportTempDir());
		if (!file_.exists()) {
			file_.mkdir();
		}
	}

	private boolean svnCommit(String filePath) {
		boolean result = false;
		try {
			//commit
			File file = new File(filePath);
			byte[] b = FileUtils.readFileToByteArray(file);
			svnService.uploadFileToSvn(propertiesMapping.getSvnReportUrl(), propertiesMapping.getSvnReportPath(),
					filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length()), b, propertiesMapping.getSvnUser(), propertiesMapping.getSvnPwd(),
					propertiesMapping.getSvnCommitDescription());
			//delete tmp
			file.delete();
			result = true;
		} catch (Exception e) {
			logger.error("-- commit svn error--", e);
		}
		return result;
	}

	private boolean sendCtmp(String releaseId, String qaFileSvnUrl, int qaTestResult, String qaMemo, String submitUserId, Integer stepType) {
		boolean result = false;
		try {
			HessianProxyFactory proxy = new HessianProxyFactory();
			QaInterface qaInterface = (QaInterface) proxy.create(QaInterface.class, propertiesMapping.getHessianUrl());

			if (stepType.compareTo(TestPlanStepEnum.TEST_PLANN_6.getValue()) == 0) {
				result = qaInterface.saveQaTestResult(releaseId, qaFileSvnUrl, qaTestResult, qaMemo, submitUserId + propertiesMapping.getComBill99());
			} else if (stepType.compareTo(TestPlanStepEnum.TEST_PLANN_8.getValue()) == 0) {
				result = qaInterface.saveQaPrdTestResult(releaseId, qaFileSvnUrl, qaTestResult, qaMemo, submitUserId + propertiesMapping.getComBill99());
			}

		} catch (Exception e) {
			logger.error("-- send ctmp error ,releaseId = " + releaseId + "--", e);
		}
		return result;
	}
}
