package com.bill99.testmp.testmanage.web.controller.command;

public class TaAssoCaseCmd {

	private Long idSvnMethod;
	private String testCaseIds;
	private String parentIds;
	private Integer state;

	public void setTestCaseIds(String testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public String getTestCaseIds() {
		return testCaseIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getState() {
		return state;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}
}
