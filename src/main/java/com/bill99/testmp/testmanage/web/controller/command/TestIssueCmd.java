package com.bill99.testmp.testmanage.web.controller.command;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.bill99.riaframework.common.command.PageCmd;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.framework.utils.PrimaryDataFactory;

public class TestIssueCmd extends PageCmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6349467010854303700L;

	private Long teamId;
	private Long userId;//用户ID
	private String pkey;//关键字
	private String summary;//需求描述
	private Date createdStart;//需求创建时间始
	private Date createdEnd;//需求创建时间止
	private String releaseId;//上线ID
	private Date releaseDateStart;//预计上线时间始
	private Date releaseDateEnd;//预计上线时间始止
	private Long cycleId;
	private Boolean isMust;

	private Map<String, String> issueStatusMap;
	private Map<String, String> testPlanStatusMap;

	private Integer[] states;//状态条件
	private Long[] issueStatusCos;
	private Long[] planStatusCos;
	private Integer[] stepStateCos;

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getCreatedStart() {
		return createdStart;
	}

	public void setCreatedStart(Date createdStart) {
		this.createdStart = createdStart;
	}

	public void setCreatedEnd(Date createdEnd) {
		this.createdEnd = createdEnd;
	}

	public Date getCreatedEnd() {
		return createdEnd;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public void setReleaseDateStart(Date releaseDateStart) {
		this.releaseDateStart = releaseDateStart;
	}

	public Date getReleaseDateStart() {
		return releaseDateStart;
	}

	public void setReleaseDateEnd(Date releaseDateEnd) {
		this.releaseDateEnd = releaseDateEnd;
	}

	public Date getReleaseDateEnd() {
		return releaseDateEnd;
	}

	public void setIssueStatusMap(Map<String, String> issueStatusMap) {
		this.issueStatusMap = issueStatusMap;
	}

	public Map<String, String> getIssueStatusMap() {
		issueStatusMap = PrimaryDataFactory.issueStatusMap;
		return issueStatusMap;
	}

	public void setTestPlanStatusMap(Map<String, String> testPlanStatusMap) {
		this.testPlanStatusMap = testPlanStatusMap;
	}

	public Map<String, String> getTestPlanStatusMap() {
		testPlanStatusMap = TestPlanStatusEnum.getTestPlanMap();
		return testPlanStatusMap;
	}

	public void setIssueStatusCos(Long[] issueStatusCos) {
		this.issueStatusCos = issueStatusCos;
	}

	public Long[] getIssueStatusCos() {
		return issueStatusCos;
	}

	public void setPlanStatusCos(Long[] planStatusCos) {
		this.planStatusCos = planStatusCos;
	}

	public Long[] getPlanStatusCos() {
		return planStatusCos;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setStepStateCos(Integer[] stepStateCos) {
		this.stepStateCos = stepStateCos;
	}

	public Integer[] getStepStateCos() {
		return stepStateCos;
	}

	public void setStates(Integer[] states) {
		this.states = states;
	}

	public Integer[] getStates() {
		return states;
	}

	public void setPkey(String pkey) {
		this.pkey = pkey;
	}

	public String getPkey() {
		return pkey;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public Boolean getIsMust() {
		return isMust;
	}

	public void setIsMust(Boolean isMust) {
		this.isMust = isMust;
	}
}
