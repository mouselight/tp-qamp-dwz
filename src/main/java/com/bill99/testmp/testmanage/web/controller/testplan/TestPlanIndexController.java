package com.bill99.testmp.testmanage.web.controller.testplan;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.RoleService;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.testmanage.common.utils.PermissonUtils;
import com.bill99.testmp.testmanage.domain.IndexMsgService;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;

@Controller
public class TestPlanIndexController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestIssueMng testIssueMng;
	@Autowired
	private TestplansMng testplansMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private IndexMsgService indexMsgService;

	@RequestMapping(value = "/testplan/index_plan_content.htm")
	public String index_plan_content(String moduleName, ModelMap model, HttpSession httpSession) {
		try {
			User user = RiaFrameworkUtils.getUseFromSession(httpSession);
			//我的任务
			if (null != moduleName && moduleName.equals(PermissonUtils.NAVIGATE_MYTASK)) {
				if (roleService.getCheckedRole(user.getId(), PermissonUtils.PERMISSION_TL)) {
					//测试TL
					return tl_index_plan(model, httpSession);
				} else if (roleService.getCheckedRole(user.getId(), PermissonUtils.PERMISSION_QA)) {
					//测试执行QA
					return qa_index_plan(model, httpSession);
				}
			}
		} catch (Exception e) {
			logger.error("-- index_plan error --", e);
		}
		return null;
	}

	//所有的统计信息
	@RequestMapping(value = "/testplan/index_msg.htm")
	public void indexMsg(HttpSession httpSession) {

		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();

		indexMsgService.loadCountMsg(httpSession, currentTestProject.getJiraTeemId(), user.getUsername());
	}

	private String tl_index_plan(ModelMap model, HttpSession httpSession) {
		List<TestIssue> testIssueList = new ArrayList<TestIssue>();
		List<Testplans> testplansList = new ArrayList<Testplans>();
		try {
			User user = RiaFrameworkUtils.getUseFromSession(httpSession);
			TestProjects currentTestProject = user.getCurrentTestProject();

			//待排期的需求
			TestIssue queryModel = new TestIssue();
			queryModel.setTeamId(currentTestProject.getJiraTeemId());
			queryModel.setIssueStatusCos(new Long[] { 1L, 10022L, 10023L, 10024L, 10025L, 10026L, 10027L, 10028L });
			testIssueList = testIssueMng.query(queryModel);

			//待跟踪的计划
			Testplans testPlan = new Testplans();
			testPlan.setTeamId(currentTestProject.getJiraTeemId());
			testPlan.setStates(new Integer[] { TestPlanStatusEnum.STATUS_0.getValue(), TestPlanStatusEnum.STATUS_1.getValue() });//未开始、进行中
			testplansList = testplansMng.query(testPlan);
		} catch (Exception e) {
			logger.error("-- query tl_index_plan error --", e);
		}
		model.addAttribute("testIssueList", testIssueList).addAttribute("testplansList", testplansList);

		return "testmanage/testplan/indexplan/tl_index_plan";
	}

	private String qa_index_plan(ModelMap model, HttpSession httpSession) {
		List<Testplans> testPlanList = new ArrayList<Testplans>();
		User user = new User();
		try {
			user = (User) httpSession.getAttribute(RiaFrameworkUtils.USER_KEY);
			TestProjects currentTestProject = user.getCurrentTestProject();
			//测试计划
			testPlanList = testPlanService.getPrivyPlan(user.getId(), currentTestProject.getJiraTeemId());
		} catch (Exception e) {
			logger.error("-- get privy plan error , userId = " + user.getId() + " --", e);
		}
		model.addAttribute("testPlanList", testPlanList).addAttribute("user", user);
		return "testmanage/testplan/indexplan/qa_index_plan";
	}
}
