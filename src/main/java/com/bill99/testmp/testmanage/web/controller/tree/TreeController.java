package com.bill99.testmp.testmanage.web.controller.tree;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.framework.utils.PrimaryDataFactory;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.common.utils.StateUtils;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@Controller
public class TreeController {

	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private TcFilterService tcFilterService;

	//	private static Long testprojectId;

	@RequestMapping(value = "/testcase/initTree.htm")
	public void initTree(HttpServletResponse response, HttpServletRequest request, Long pid, HttpSession httpSession) throws Exception {

		//		testprojectId = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId();

		List<NodesHierarchy> nodes = nodesHierarchyMng.getChild(pid);
		String json = createRespJSON(nodes, 0, null);
		ResponseUtils.renderText(response, json);
		return;
	}

	@RequestMapping(value = "/testcase/initAssoTree.htm")
	public void initAssoTree(HttpServletResponse response, HttpServletRequest request, Long pid, Long planStepId, Integer exeType, HttpSession httpSession) {

		Integer userTcExeStatus = RiaFrameworkUtils.getTcExeTypeFromSession(httpSession);

		List<Long> allTcParentIds = testPlanService.getParentIdsExecution(planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
		if (null != allTcParentIds && allTcParentIds.size() > 0) {
			List<NodesHierarchy> nodes = nodesHierarchyMng.getAssoChild(pid, allTcParentIds, planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
			String json = createRespJSON(nodes, 1, planStepId);
			ResponseUtils.renderText(response, json);
		}
		return;
	}

	@RequestMapping(value = "/testcase/initAssoAutoTree.htm")
	public void initAssoAutoTree(HttpServletResponse response, HttpServletRequest request, Long pid, Long planStepId, Integer exeType, HttpSession httpSession) {
		Integer userTcExeStatus = RiaFrameworkUtils.getTcExeTypeFromSession(httpSession);

		List<Long> allTcParentIds = testPlanService.getParentIdsExecution(planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
		if (null != allTcParentIds && allTcParentIds.size() > 0) {
			List<NodesHierarchy> nodes = nodesHierarchyMng.getAssoChild(pid, allTcParentIds, planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
			String json = createRespJSON(nodes, 3, planStepId);
			ResponseUtils.renderText(response, json);
		}
		return;
	}

	@RequestMapping(value = "/testcase/initAssoPerformanceTree.htm")
	public void initAssoPerformanceTree(HttpServletResponse response, HttpServletRequest request, Long pid, Long planStepId, Integer exeType, HttpSession httpSession) {
		Integer userTcExeStatus = RiaFrameworkUtils.getTcExeTypeFromSession(httpSession);

		List<Long> allTcParentIds = testPlanService.getParentIdsExecution(planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
		if (null != allTcParentIds && allTcParentIds.size() > 0) {
			List<NodesHierarchy> nodes = nodesHierarchyMng.getAssoChild(pid, allTcParentIds, planStepId, exeType, userTcExeStatus == null ? 0 : userTcExeStatus);
			String json = createRespJSON(nodes, 7, planStepId);
			ResponseUtils.renderText(response, json);
		}
		return;
	}

	@RequestMapping(value = "/testcase/filterNoAssoTree.htm")
	public void filterNoAssoTree(HttpServletResponse response, HttpServletRequest request, TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {
		//查询后会更新该用户下的filter-list
		tcFilterService.engineFilter(httpSession, testCasesQueryDto);
		//查询条件保存在session中
		httpSession.setAttribute(TreeUtils.TC_FILETER_COMMAND_KEY, testCasesQueryDto);
	}

	@RequestMapping(value = "/testcase/filterAssoTree.htm")
	public void filterAssoTree(HttpServletResponse response, HttpServletRequest request, TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {
		//查询后会更新该用户下的filter-list
		tcFilterService.engineFilter(httpSession, testCasesQueryDto);
		//查询条件保存在session中
		httpSession.setAttribute(TreeUtils.TC_FILETER_COMMAND_KEY, testCasesQueryDto);
	}

	@RequestMapping(value = "/testcase/initNoAssoTree.htm")
	public void initNoAssoTree(HttpServletResponse response, HttpServletRequest request, TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {
		//展开树时 不再重复查询
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		String userTcKey = user.getId().toString() + TreeUtils.TC_FILTER_KEY_SUFFIX;
		String userNodeKey = user.getId().toString() + TreeUtils.NODE_FILTER_KEY_SUFFIX;

		List<Long> tcIds = TreeUtils.tcFilterMap.get(userTcKey);
		List<Long> partIds = TreeUtils.nodeFilterMap.get(userNodeKey);

		List<NodesHierarchy> nodes = nodesHierarchyMng.getNoAssoChild(testCasesQueryDto.getPid(), testCasesQueryDto.getPlanStepId(), tcIds, partIds);
		String json = createRespJSON(nodes, 2, testCasesQueryDto.getPlanStepId());
		ResponseUtils.renderText(response, json);
	}

	@RequestMapping(value = "/testcase/initNoAssoTree4Ta.htm")
	public void initNoAssoTree4Ta(HttpServletResponse response, HttpServletRequest request, TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {
		//展开树时 不再重复查询
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		String userTcKey = user.getId().toString() + TreeUtils.TC_FILTER_KEY_SUFFIX;
		String userNodeKey = user.getId().toString() + TreeUtils.NODE_FILTER_KEY_SUFFIX;

		List<Long> tcIds = TreeUtils.tcFilterMap.get(userTcKey);
		List<Long> partIds = TreeUtils.nodeFilterMap.get(userNodeKey);

		List<NodesHierarchy> nodes = nodesHierarchyMng.getNoAssoChild4Ta(testCasesQueryDto.getPid(), tcIds, partIds);
		String json = createRespJSON(nodes, 6, 0L);
		ResponseUtils.renderText(response, json);
	}

	//Cloud-TC集已关联树
	@RequestMapping(value = "/testcase/initCloudAssoTree.htm")
	public void initCloudAssoTree(HttpServletResponse response, HttpServletRequest request, Long pid, Long cloudId, HttpSession httpSession) {
		List<NodesHierarchy> nodes = nodesHierarchyMng.getChild4Cloud(pid, cloudId);
		String json = createRespJSON(nodes, 5, cloudId);
		ResponseUtils.renderText(response, json);
		return;
	}

	//自动化脚本已关联树
	@RequestMapping(value = "/testcase/initTaAssoTree.htm")
	public void initTaAssoTree(HttpServletResponse response, HttpServletRequest request, Long pid, Long idSvnMethod, HttpSession httpSession) {
		List<NodesHierarchy> nodes = nodesHierarchyMng.getChild4Ta(pid, idSvnMethod);
		String json = createRespJSON(nodes, 4, idSvnMethod);
		ResponseUtils.renderText(response, json);
		return;
	}

	//自动化脚本已关联树
	@RequestMapping(value = "/testcase/taTaskTcAssoTree.htm")
	public void taTaskTcAssoTree(HttpServletResponse response, HttpServletRequest request, Long pid, String taTaskId, HttpSession httpSession) {
		List<NodesHierarchy> nodes = nodesHierarchyMng.getChild4TaTaskTc(pid, taTaskId);
		String json = createRespJSON(nodes, 0, null);
		ResponseUtils.renderText(response, json);
		return;
	}

	private String createRespJSON(List<NodesHierarchy> nodes, int type, Long planStepId) {
		String resultStr = null;
		StringBuilder resultStrBld = new StringBuilder();

		if (nodes != null) {
			resultStrBld.append("<ul>");
			for (NodesHierarchy node : nodes) {
				resultStrBld.append(" <li id=\"" + node.getId() + "\">");//这里可以解释前台代码为何要.substr(2);
				//onmouseout=\"javascript:$(this).removeClass('active');\" 
				//onclick=\"javascript:$(this).addClass('active');\"
				resultStrBld.append(" <span");

				if (type != 0) {
					resultStrBld.append(" onmouseover=\"javascript:$(this).addClass('active');\"");
				}

				resultStrBld.append(" id=\"1_" + node.getId() + "\"");
				resultStrBld.append(" nodeTypeId=\"" + node.getNodeTypeId() + "\"");
				resultStrBld.append(" >");
				resultStrBld.append(getTxt(node, planStepId, type));
				resultStrBld.append(" </span>");
				resultStrBld.append(getUrl(node, type, planStepId));
				resultStrBld.append(" </li>");
			}
			resultStr = resultStrBld.toString();
			resultStr = resultStr.toString().substring(0, resultStr.length() - 1);
			resultStr += "></ul>";
		}
		return resultStr;
	}

	private String getUrl(NodesHierarchy node, int type, Long id) {
		if (node.getNodeTypeId() != null && node.getNodeTypeId().longValue() != NodeTypeUtils.TESTCASE) {
			if (type == 1) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initAssoTree.htm?pid=" + node.getId() + "&planStepId=" + id + "&exeType=1}</li></ul>";
			} else if (type == 0) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initTree.htm?pid=" + node.getId() + "}</li></ul>";
			} else if (type == 2) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initNoAssoTree.htm?pid=" + node.getId() + "&planStepId=" + id + "}</li></ul>";
			} else if (type == 4) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initTaAssoTree.htm?pid=" + node.getId() + "&idSvnMethod=" + id + "}</li></ul>";
			} else if (type == 3) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initAssoAutoTree.htm?pid=" + node.getId() + "&planStepId=" + id + "&exeType=2}</li></ul>";
			} else if (type == 7) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initAssoPerformanceTree.htm?pid=" + node.getId() + "&planStepId=" + id + "&exeType=3}</li></ul>";
			} else if (type == 5) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initCloudAssoTree.htm?pid=" + node.getId() + "&cloudId=" + id + "}</li></ul>";
			} else if (type == 6) {
				return "<ul class='ajax'><li>{url:/qamp/testcase/initNoAssoTree4Ta.htm?pid=" + node.getId() + "}</li></ul>";
			}
		}
		return "";
	}

	private String getTxt(NodesHierarchy node, Long planStepId, int type) {
		StringBuilder textStrBld = new StringBuilder();
		if (null == planStepId && node.getNodeTypeId() != null && node.getNodeTypeId().longValue() != NodeTypeUtils.TESTCASE) {
			textStrBld.append(node.getName() + getTcQuantity(node, type));
		} else {
			if (null != node.getStatus() && node.getStatus().intValue() == StateUtils.STATUS_ASSOTC_SUCC) {
				//成功
				textStrBld.append(node.getName() + "<b><font color='green'>(" + PrimaryDataFactory.assoTcStatusMap.get(node.getStatus()) + ")</font></b>");
			} else if (null != node.getStatus() && node.getStatus().intValue() == StateUtils.STATUS_ASSOTC_FAIL) {
				//失败
				textStrBld.append(node.getName() + "<b><font color='red'>(" + PrimaryDataFactory.assoTcStatusMap.get(node.getStatus()) + ")</font></b>");
				//			} else if (node.getNodeTypeId() != null && node.getNodeTypeId().longValue() != NodeTypeUtils.TESTCASE) {
				//				textStrBld.append(node.getName() + getTcQuantity(node, type));
			} else {
				textStrBld.append(node.getName());
			}
		}
		return textStrBld.toString();
	}

	private String getTcQuantity(NodesHierarchy node, int type) {
		String tcQuantity = "";
		if (node.getNodeTypeId() != null && node.getNodeTypeId().longValue() != NodeTypeUtils.TESTCASE) {

			//			nodesHierarchyMng.initTestCase(testprojectId, node.getParentId());//初始化teemID数据用
			//			tcQuantity = "(" + calcTcQuantity(node.getId()) + ")"; //实时递归,测试时注释,有性能消耗
			//			tcQuantity = "(TC数)";
			//			if (type == 3 || type == 7) {
			//				int exeType;
			//				if (type == 3) {
			//					exeType = 2;
			//				} else {
			//					exeType = 3;
			//				}
			//				tcQuantity = "(" + calcTcQuantityWithExeType(node.getId(), exeType) + ")"; //实时递归,测试时注释,有性能消耗
			//			} else {
			tcQuantity = TreeUtils.TC_QUANTITY_MAP.get(node.getId().toString());
			//			}
			if (!StringUtils.hasLength(tcQuantity)) {
				tcQuantity = "(0)";
			}
		}
		return tcQuantity;
	}

	//实时递归
	private long calcTcQuantity(Long pid) {
		long tcQuantity = 0;
		//		nodesHierarchyMng.initTestCase(testprojectId, pid);//初始化teemID数据用
		//		nodesHierarchyMng.recursive4InvalidTc(nodesHierarchyMng.getById(pid));//递归无效TC
		tcQuantity += nodesHierarchyMng.getTcQuantity(pid);
		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				tcQuantity += calcTcQuantity(suiteId);
			}
		}
		return tcQuantity;
	}

	//实时递归&用例类型
	private long calcTcQuantityWithExeType(Long pid, int type) {
		long tcQuantity = 0;
		//		nodesHierarchyMng.initTestCase(testprojectId, pid);//初始化teemID数据用
		//		nodesHierarchyMng.recursive4InvalidTc(nodesHierarchyMng.getById(pid));//递归无效TC
		tcQuantity += nodesHierarchyMng.getTcQuantity(pid, type);
		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				tcQuantity += calcTcQuantityWithExeType(suiteId, type);
			}
		}
		return tcQuantity;
	}
}
