package com.bill99.testmp.testmanage.web.controller.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DemoController {

	@RequestMapping(value = "/demo/testshow.htm")
	public String test(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {
		//		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		//		List<Keywords> keywordsList = null;
		//		if (currentTestProject != null) {
		//			model.addAttribute("listItems", keywordsList);
		//		}

		String stringTest = "hello !!!";
		model.addAttribute("stringTest", stringTest);

		List<String> stringTestList = new ArrayList<String>();
		stringTestList.add("H");
		stringTestList.add("E");
		stringTestList.add("L");
		stringTestList.add("L");
		stringTestList.add("O");
		stringTestList.add("!");
		model.addAttribute("stringTestList", stringTestList);
		model.addAttribute("demoCmd", new DemoCmd());
		return "demo/testshow";
	}

	@RequestMapping(value = "/demo/testconmmit.htm")
	public String conmmit(DemoCmd demoCmd, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		System.out.println("==name====" + demoCmd.getName());
		System.out.println("==select====" + demoCmd.getSelect());

		model.addAttribute("demoCmd", demoCmd);
		return "demo/testshow";
	}
}
