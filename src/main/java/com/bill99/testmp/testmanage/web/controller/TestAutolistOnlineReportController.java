package com.bill99.testmp.testmanage.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;
import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.orm.manager.TestAutolistOnlineReportMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;
import com.bill99.testmp.testmanage.orm.manager.impl.MonitorManagerImpl;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestAutolistOnlineReportController {

	@Autowired
	private TestAutolistOnlineReportMng testAutolistOnlineReportMng;
	@Autowired
	private TestProjectsMng testProjectsMng;
	@Autowired
	private MonitorManagerImpl monitorManager;


	@RequestMapping("/testreport/reportchartprocess.htm")
	public String reportchartprocess11(HttpServletResponse response ,ModelMap model,TaReportChartDto taReportChartDto) throws Exception {
		
		StringBuffer XML = new StringBuffer();   
		List<TaReportChartDto> dtoNew = new ArrayList<TaReportChartDto>();
		
		if (taReportChartDto.getPrefixidlist() != null && taReportChartDto.getPrefixidlist().size() > 0) {
			List<TaReportChartDto> dto = testAutolistOnlineReportMng.getChartData(taReportChartDto);	
			for (String id : taReportChartDto.getPrefixidlist()) {
				long ID = Long.parseLong(id);
				TaReportChartDto dto2 = new TaReportChartDto();
				dto2.setPrefix(testProjectsMng.getById(ID).getPrefix());
				List<TaReportChartDto> dtoin = new ArrayList<TaReportChartDto>();
				for (TaReportChartDto charDto : dto) {
					if (ID == charDto.getPrefix_id()) {
						dtoin.add(charDto);
					}
				}
				dto2.setChartDtos(dtoin);
				dtoNew.add(dto2);
			}
		}
	        String asString = createXmlData(dtoNew);
	        XML.append("<?xml version='1.0' encoding='UTF-8'?>"); 
	        XML.append(asString);
	       // System.out.println("asString=" + XML.toString());
			//model.addAttribute("chart11", XML);
			// model.addAttribute("reportchart",dto);
		
		   ResponseUtils.renderText(response, asString);
		    
		 return "testmanage/testreport/reportchart";
	}	
		
	

	@RequestMapping("/testreport/reportchart.htm")
	public String reportChart(ModelMap model, TaReportChartDto taReportChartDto)
			throws Exception {

		model.addAttribute("projectslist", testProjectsMng.findALl());

		
		 //System.out.println(""+ taReportChartDto.getPrefixidlist().size());
		  //System.out.println("" + taReportChartDto.getCycledateStart());
		// System.out.println("" + taReportChartDto.getCycledateEnd());
		 

		if (taReportChartDto.getPrefixidlist() != null
				&& taReportChartDto.getPrefixidlist().size() > 0) {
			List<TaReportChartDto> dto = testAutolistOnlineReportMng
					.getChartData(taReportChartDto);
			List<TaReportChartDto> dtoNew = new ArrayList<TaReportChartDto>();
			for (String id : taReportChartDto.getPrefixidlist()) {
				long ID = Long.parseLong(id);
				TaReportChartDto dto2 = new TaReportChartDto();
				dto2.setPrefix(testProjectsMng.getById(ID).getPrefix());
				List<TaReportChartDto> dtoin = new ArrayList<TaReportChartDto>();
				for (TaReportChartDto charDto : dto) {
					if (ID == charDto.getPrefix_id()) {
						dtoin.add(charDto);
					}
				}
				dto2.setChartDtos(dtoin);
				dtoNew.add(dto2);
			}

			model.addAttribute("chart", createXmlData(dtoNew));
			// model.addAttribute("reportchart",dto);
		}

		return "testmanage/testreport/reportchart";

	}

	@RequestMapping("/testreport/index_onlineprojectreport.htm")
	public String index_onlineprojectreport(TaReportDto taReportDto, ModelMap model) {

		model.addAttribute("cyclelist", testAutolistOnlineReportMng.getCycleList());// 上线cycle
		if (taReportDto.getCycles() != null && taReportDto.getCycles().size() > 0) {
			List<TaReportDto> dto01 = testAutolistOnlineReportMng.getAllproject();
			List<TaReportDto> dto = testAutolistOnlineReportMng.getTaonlineNum(taReportDto);
			List<TaReportDto> dto4View = new ArrayList<TaReportDto>();
			List<TaReportDto> dto2 = testAutolistOnlineReportMng.getCaseTotalnum(taReportDto);
			List<TaReportDto> dto3 = testAutolistOnlineReportMng.getArtficialCaseTotalnum(taReportDto);
			List<TaReportDto> dto4 = testAutolistOnlineReportMng.getAutoCaseTotalnum(taReportDto);
			List<TaReportDto> dto5 = testAutolistOnlineReportMng.getStage02CaseTotalnum(taReportDto);
			List<TaReportDto> dto6 = testAutolistOnlineReportMng.getStage02AutoSuccessCasenum(taReportDto);
			List<TaReportDto> dto7 = testAutolistOnlineReportMng.getIssueNum(taReportDto);

			for (TaReportDto taReportDto22 : dto01) {

				TaReportDto taReportDtonew = new TaReportDto();
				taReportDtonew.setPrefix(taReportDto22.getPrefix());
				taReportDtonew.setId(taReportDto22.getId());
				taReportDtonew.setTeamid(taReportDto22.getTeamid());
				taReportDtonew.setCycles(taReportDto.getCycles());
				
				for(TaReportDto taReport : dto){
					if(taReport.getId().equals(taReportDtonew.getId())){
						taReportDtonew.setTcplannum(taReport.getTcplannum());	
					}
				}

				for (TaReportDto taReport : dto2) {
					if (taReport.getId().equals(taReportDtonew.getId())) {
						taReportDtonew.setCasetotalnum(taReport.getCasetotalnum());
					}
				}
				for (TaReportDto taReport : dto3) {
					if (taReport.getId().equals(taReportDtonew.getId())) {
						taReportDtonew.setArcasetotalnum(taReport.getArcasetotalnum());
					}
				}
				for (TaReportDto taReport : dto4) {
					if (taReport.getId().equals(taReportDtonew.getId())) {
						taReportDtonew.setStage02autocasenum(taReport.getStage02autocasenum());
					}
				}
				for (TaReportDto taReport : dto5) {
					if (taReport.getId().equals(taReportDtonew.getId())) {
						taReportDtonew.setStage02casetotalnum(taReport.getStage02casetotalnum());
					}
				}
				for (TaReportDto taReport : dto6) {
					if (taReport.getId().equals(taReportDtonew.getId())) {
						taReportDtonew.setStage02autocasesuccnum(taReport.getStage02autocasesuccnum());
					}
				}
				for (TaReportDto taReport : dto7) {
					if (taReport.getTeamid().equals(taReportDtonew.getTeamid())) {
						taReportDtonew.setIssuenum(taReport.getIssuenum());
					}
				}

				dto4View.add(taReportDtonew);

			}

			model.addAttribute("tareportlist", dto4View);
		}

		return "testmanage/testreport/list_onlineprojectnum";
	}

		public String createXmlData(List<TaReportChartDto> dto) {
			String xml = null;
			try {
				xml = monitorManager.getRmmonitorBPByTypeId(dto);
				//System.out.println("xml==" + xml.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return xml;
		}
}
