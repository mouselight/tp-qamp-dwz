package com.bill99.testmp.testmanage.web.controller.testplan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.web.controller.user.UserCmd;
import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;
import com.bill99.testmp.testmanage.web.controller.command.TestIssueCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestIssueController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestIssueMng testIssueMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private TestplansMng testplansMng;
	@Autowired
	private TestplanStepsMng testplanStepsMng;

	@RequestMapping(value = "/testissue/index_testissue.htm")
	public String index_testissue(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {
		//页面初始条件
		cmd.setIssueStatusCos(new Long[] { 1L, 10022L, 10023L, 10024L, 10025L, 10026L, 10027L, 10028L });
		return list_testissue(request, response, model, cmd, httpSession);
	}

	@RequestMapping(value = "/testissue/list_testissue.htm")
	public String list_testissue(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {
		Pagination pagination = new Pagination();
		Map<String, String> cycleMap = new LinkedHashMap<String, String>();
		try {
			User user = RiaFrameworkUtils.getUseFromSession(httpSession);
			TestProjects currentTestProject = user.getCurrentTestProject();

			TestIssue queryModel = new TestIssue();
			//页面条件
			BeanUtils.copyProperties(cmd, queryModel);
			//Team_ID
			queryModel.setTeamId(currentTestProject.getJiraTeemId());

			pagination = testIssueMng.query(queryModel, cmd.getPageNum(), cmd.getNumPerPage());

			//cycle
			cycleMap = testIssueMng.queryCycle(currentTestProject.getJiraTeemId());

		} catch (Exception e) {
			logger.error("-- query testissue error --", e);
		}
		model.addAttribute("pagination", pagination).addAttribute("cmd", cmd).addAttribute("cycleMap", cycleMap);
		return "testmanage/testissue/list_testissue";
	}

	@RequestMapping(value = "/testplan/edit_testplan.htm")
	public String edit_testplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		TestIssue testIssue = new TestIssue();
		Testplans testplan = new Testplans();
		try {
			//需求相关信息
			testIssue = testIssueMng.getIssueDetail(cmd.getIssueId());
			//计划信息
			testplan = testplansMng.findByIssueId(cmd.getIssueId());
		} catch (Exception e) {
			logger.error("--issue findById error , issueId = " + cmd.getIssueId() + " --", e);
		}

		model.addAttribute("testIssue", testIssue).addAttribute("testplan", testplan).addAttribute("testPlanStepList", testplan.getTestplanSteps()).addAttribute("cmd", cmd);
		return "testmanage/testissue/edit_testplan";
	}

	@RequestMapping(value = "/testplan/merge_testplan.htm")
	public void merge_testplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			TestPlansDto plansDto = new TestPlansDto();
			BeanUtils.copyProperties(cmd, plansDto);
			plansDto.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
			testPlanService.mergeTestPlans(plansDto, RiaFrameworkUtils.getUseFromSession(httpSession).getEmail());
		} catch (Exception e) {
			logger.error("-- merge testplan error --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "edit_testplan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testplan/delete_planstep.htm")
	public void delete_planstep(HttpServletResponse response, TestManageCmd cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			testplanStepsMng.deleteById(cmd.getPlanStepId());
		} catch (Exception e) {
			ajaxDoneUtil = new AjaxDoneUtil("300", "删除失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功！", "edit_testplan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testplan/assign_res_testplan.htm")
	public String assign_res_testplan(ModelMap model, UserCmd cmd, HttpSession session) {
		List<User> userList = new ArrayList<User>();
		List<Long> teamIdList = new ArrayList<Long>();
		Set<Integer> userIdSet = new HashSet<Integer>();
		Map<String, String> jiraTeemMap = new HashMap<String, String>();

		User user = RiaFrameworkUtils.getUseFromSession(session);
		Set<TestProjects> us = user.getTestprojects();
		if (us != null) {
			for (TestProjects testprojects : us) {
				if(null!=testprojects.getJiraTeemId()){
				jiraTeemMap.put(testprojects.getJiraTeemId().toString(), testprojects.getPrefix());
				}
			
			}
		}

		if (StringUtils.hasLength(cmd.getUserIds())) {
			String usIds[] = cmd.getUserIds().split(",");
			for (String usId : usIds) {
				userIdSet.add(Integer.parseInt(usId));
			}

			List<Integer> userIdList = new ArrayList<Integer>(userIdSet);
			teamIdList = testIssueMng.querytemIdByUserId(userIdList);

		} else {
			if (cmd.getTeamId() != null) {
				teamIdList.add(cmd.getTeamId());
			}

			String teamIds = cmd.getTeamIds();
			if (StringUtils.hasLength(teamIds)) {
				String tmIds[] = teamIds.split(",");
				for (String teamId : tmIds) {
					teamIdList.add(Long.parseLong(teamId));

				}

			}

		}
		userList = testIssueMng.queryUser(cmd.getRealname(), teamIdList);

		model.addAttribute("userList", userList).addAttribute("jiraTeemMap", jiraTeemMap).addAttribute("teamIdList", teamIdList).addAttribute("cmd", cmd);

		return "testmanage/testissue/asso_res_testplan";
	}
}
