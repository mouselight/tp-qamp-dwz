package com.bill99.testmp.testmanage.web.controller.testautomation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;

@Controller
public class TestTaTaskTraceController {
	@Autowired
	private TestAutomationMng testAutomationMng;

	//任务监控
	@RequestMapping(value = "/testautomation/list_tatasktrace.htm")
	public String list_tatasktrace(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		model.addAttribute("items", testAutomationMng.getTaTaskLog());
		return "testmanage/testautomation/list_tatasktrace";
	}

}
