package com.bill99.testmp.testmanage.web.controller.command;

public class TestProjectCmd {

	private Long id;
	private String prefix;
	private String notes;
	private Boolean isPublic;
	private Boolean active;
	private Long tcCounter;
	private Long jiraTeemId;
	private String unitType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public Long getTcCounter() {
		return tcCounter;
	}

	public void setTcCounter(Long tcCounter) {
		this.tcCounter = tcCounter;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getJiraTeemId() {
		return jiraTeemId;
	}

	public void setJiraTeemId(Long jiraTeemId) {
		this.jiraTeemId = jiraTeemId;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

}
