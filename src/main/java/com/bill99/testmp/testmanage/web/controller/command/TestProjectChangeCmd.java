package com.bill99.testmp.testmanage.web.controller.command;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class TestProjectChangeCmd {

	private long changeid;
	private long issueid;
	private String changename;
	private String changenotes;
	private String changetype;
	private String changeattach;
	private MultipartFile uploadfile;
	private Date changedate;
	private Date createdate;
	private String create_user;
	private String update_user;
	private String changeremark;
	private String uploadfileName;

	public long getChangeid() {
		return changeid;
	}

	public long getIssueid() {
		return issueid;
	}

	public void setIssueid(long issueid) {
		this.issueid = issueid;
	}

	public void setChangeid(long changeid) {
		this.changeid = changeid;
	}

	public String getChangename() {
		return changename;
	}

	public void setChangename(String changename) {
		this.changename = changename;
	}

	public String getChangenotes() {
		return changenotes;
	}

	public void setChangenotes(String changenotes) {
		this.changenotes = changenotes;
	}

	public String getChangetype() {
		return changetype;
	}

	public void setChangetype(String changetype) {
		this.changetype = changetype;
	}

	public String getChangeattach() {
		return changeattach;
	}

	public void setChangeattach(String changeattach) {
		this.changeattach = changeattach;
	}

	public MultipartFile getUploadfile() {
		return uploadfile;
	}

	public void setUploadfile(MultipartFile uploadfile) {
		this.uploadfile = uploadfile;
	}

	public Date getChangedate() {
		return changedate;
	}

	public void setChangedate(Date changedate) {
		this.changedate = changedate;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getCreate_user() {
		return create_user;
	}

	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}

	public String getUpdate_user() {
		return update_user;
	}

	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}

	public String getChangeremark() {
		return changeremark;
	}

	public void setChangeremark(String changeremark) {
		this.changeremark = changeremark;
	}

	public String getUploadfileName() {
		return uploadfileName;
	}

	public void setUploadfileName(String uploadfileName) {
		this.uploadfileName = uploadfileName;
	}

}
