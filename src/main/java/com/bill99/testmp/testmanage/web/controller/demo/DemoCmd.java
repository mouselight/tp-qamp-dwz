package com.bill99.testmp.testmanage.web.controller.demo;

public class DemoCmd {

	private String name;
	private String select;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

}
