package com.bill99.testmp.testmanage.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.TcCloud;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestCaseCloudMng;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestCaseCloudController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestCaseCloudMng testCaseCloudMng;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TcFilterService tcFilterService;
	@Autowired
	private KeywordsMng keywordsMng;

	@RequestMapping("/property/tc_cloud/index.htm")
	public String query(ModelMap model, HttpSession httpSession, TestCaseCloudCommand cmd) {
		cmd.setTeamId(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getJiraTeemId());
		Pagination pagination = testCaseCloudMng.query(cmd);
		model.addAttribute("pagination", pagination);
		return "testmanage/testcloud/list_tccloud";
	}

	@RequestMapping("/property/tc_cloud/delete.htm")
	public void delete(HttpServletResponse response, Long id) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			testCaseCloudMng.delete(id);
			ajaxDoneUtil = new AjaxDoneUtil("200", "操作成功！", "list_tc_cloud", null, null, null);
		} catch (Exception e) {
			ajaxDoneUtil = new AjaxDoneUtil("300", "操作失败！", "list_tc_cloud", null, null, null);
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/property/tc_cloud/edit.htm")
	public String editPage(ModelMap model, HttpSession httpSession, Long id) {
		//搜索条件

		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		//清空session中条件限制
		httpSession.setAttribute(TreeUtils.TC_FILETER_COMMAND_KEY, new TestCasesQueryDto(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId()));
		//初始进入页面后清空 tc-filter
		tcFilterService.clearFilter(user.getId(), httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		Map<String, String> cloudNameMap = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
			cloudNameMap = testCaseCloudMng.getCloudNameMap(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getJiraTeemId());
		}
		model.addAttribute("keywordsList", keywordsList);
		model.addAttribute("cloudNameMap", cloudNameMap);

		TcCloud tcCloud = new TcCloud();
		if (null != id) {
			tcCloud = testCaseCloudMng.findById(id);
		}
		model.addAttribute("tcCloud", tcCloud);
		return "testmanage/testcloud/edit_tccloud";
	}

	@RequestMapping("/property/tc_cloud/update.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, TestCaseCloudCommand cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			cmd.setUserName(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
			cmd.setTeamId(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getJiraTeemId());
			Long id = testCaseCloudMng.saveOrUpdate(cmd);
			String url = request.getContextPath() + "/property/tc_cloud/edit.htm?id=" + id;
			ajaxDoneUtil = new AjaxDoneUtil("200", "操作成功！", "edit_tc_cloud", null, "forward", url);
		} catch (Exception e) {
			ajaxDoneUtil = new AjaxDoneUtil("300", "操作失败！", null, null, null, null);
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/property/tc_cloud/tc_asso.htm")
	public void saveAsso(HttpSession httpSession, HttpServletRequest request, HttpServletResponse response, TestCaseCloudCommand cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			List<Long> addTcIds = new ArrayList<Long>();
			Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();

			nodeMap = nodesHierarchyMng.getAllChildIds(StringUtil.strToListObj(cmd.getParentIds()));

			//tc_id
			List<Long> tcIds = StringUtil.appendList(nodeMap.get(TreeUtils.CHILD_NODE), StringUtil.strToListObj(cmd.getTestCaseIds()));
			addTcIds.addAll(tcIds);

			//parent_id
			List<Long> parentIds = StringUtil.appendList(nodeMap.get(TreeUtils.FATHER_NODE), StringUtil.strToListObj(cmd.getParentIds()));
			addTcIds.addAll(parentIds);

			//save asso
			testCaseCloudMng.saveAsso(cmd.getId(), addTcIds);

			//update
			cmd.setUserName(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
			testCaseCloudMng.update(cmd);
		} catch (Exception e) {
			logger.error("-- tc_cloud asso error , cloudId = " + cmd.getId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "edit_tc_cloud", null, "forward", request.getContextPath() + "/property/tc_cloud/edit.htm?id=" + cmd.getId());
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/property/tc_cloud/tc_remove.htm")
	public void testcase_rv(HttpSession httpSession, HttpServletResponse response, TestCaseCloudCommand cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			List<Long> removeTcIds = new ArrayList<Long>();
			//页面选择tc
			removeTcIds.addAll(StringUtil.strToListObj(cmd.getTestCaseIds()));
			//页面选择node
			removeTcIds.addAll(StringUtil.strToListObj(cmd.getParentIds()));
			//页面节点的父节点
			List<Long> parentIds = nodesHierarchyMng.getAllParentIds(StringUtil.appendList(StringUtil.strToLongList(cmd.getTestCaseIds()),
					StringUtil.strToLongList(cmd.getParentIds())));
			removeTcIds.addAll(parentIds);
			//页面节点的子节点
			Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();

			if (!StringUtil.isNull(cmd.getParentIds())) {
				nodeMap = nodesHierarchyMng.getAllChildIds(StringUtil.strToListObj(cmd.getParentIds()));
				if (null != nodeMap) {
					removeTcIds.addAll(nodeMap.get(TreeUtils.FATHER_NODE));
					removeTcIds.addAll(nodeMap.get(TreeUtils.CHILD_NODE));
				}
			}

			//移除-by tcId
			testCaseCloudMng.deleteAsso(cmd.getId(), removeTcIds);

			//update
			cmd.setUserName(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
			testCaseCloudMng.update(cmd);
		} catch (Exception e) {
			logger.error("--rv tc asso error , cloudId = " + cmd.getId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "移除失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "移除成功！", "edit_tc_cloud", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
