package com.bill99.testmp.testmanage.web.controller.testplan;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.dto.TestTaTaskLogDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.domain.TestManageService;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.TcExeLogDetail;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TcExeLogDetailMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestCaseCloudMng;
import com.bill99.testmp.testmanage.orm.manager.TestTaTaskLogMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestPlanAssoCaseCmd;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestPlanAssoTcController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestplanStepsMng testplanStepsMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TestReportService testReportService;
	@Autowired
	private KeywordsMng keywordsMng;
	@Autowired
	private TcFilterService tcFilterService;
	@Autowired
	private TestCaseCloudMng testCaseCloudMng;
	@Autowired
	private TestTaTaskLogMng testTaTaskLogMng;
	@Autowired
	private TestAutomationMng testAutomationMng;
	@Autowired
	private TestManageService testManageService;
	@Autowired
	private TcExeLogDetailMng tcExeLogDetailMng;

	@RequestMapping(value = "/testplan/testcase_manage.htm")
	public String testcase_manage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestManageCmd cmd) {

		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		List<TestplanSteps> unitSteps = testplanStepsMng.getUnitStepsByStepId(cmd.getParentStepId() == null ? cmd.getPlanStepId() : cmd.getParentStepId());
		model.addAttribute("unitSteps", unitSteps);

		List<TestProjects> testProjects = null;
		if (TreeUtils.nodeTestProjectMap != null && TreeUtils.nodeTestProjectMap.size() > 0) {
			testProjects = TreeUtils.nodeTestProjectMap.get(user.getId().toString() + TreeUtils.PROJECT_FILTER_KEY_SUFFIX);
		}
		if (testProjects == null) {
			testProjects = new ArrayList<TestProjects>();
		}

		List<TestProjects> testProjectSelecteds = testManageService.getProjectGroup(cmd.getPlanStepId());
		//		List<TestProjects> testProjectSelecteds = testManageService.getAssoProjects(cmd.getPlanStepId());
		if (testProjectSelecteds == null) {
			testProjectSelecteds = new ArrayList<TestProjects>();
		}

		testProjects.addAll(testProjectSelecteds);
		model.addAttribute("testProjects", testProjects);
		model.addAttribute("testProjectSelecteds", testProjectSelecteds);

		//
		StringBuilder testProjectSelectedIds = new StringBuilder();
		for (TestProjects testProjects2 : testProjects) {
			testProjectSelectedIds.append(testProjects2.getId());
			testProjectSelectedIds.append(",");
		}
		cmd.setTestProjectSelectedIds(testProjectSelectedIds.toString());

		//清空session中条件限制
		httpSession.setAttribute(TreeUtils.TC_FILETER_COMMAND_KEY, new TestCasesQueryDto(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId()));
		//初始进入页面后清空 tc-filter
		tcFilterService.clearFilter(user.getId(), httpSession);

		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		Map<String, String> cloudNameMap = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
			cloudNameMap = testCaseCloudMng.getCloudNameMap(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getJiraTeemId());
		}
		TestplanSteps testPlanStep = testplanStepsMng.get(cmd.getPlanStepId());

		TestTaTaskLog testTaTaskLog = testTaTaskLogMng.getLastTask(cmd.getPlanStepId());
		if (testTaTaskLog != null) {
			model.addAttribute("taTaskId", testTaTaskLog.getTaTaskId());
		}
		List<TcExeLogDetail> tcExeLogList=tcExeLogDetailMng.findByPlanStepId(cmd.getPlanStepId());
		model.addAttribute("cloudNameMap", cloudNameMap);
		model.addAttribute("keywordsList", keywordsList);
		model.addAttribute("testPlanStep", testPlanStep);
		model.addAttribute("cmd", cmd);
		model.addAttribute("tcExeLogList",tcExeLogList);

		return "testmanage/testplan/trackplan/asso_tc_testplan";
	}

	@RequestMapping(value = "/testplan/privy_testcase.htm")
	public String privy_testcase_manage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestManageCmd cmd) {
		model.addAttribute("cmd", cmd);
		return "testmanage/testplan/privyplan/asso_tc_privyplan";
	}

	@RequestMapping(value = "/testplan/testcase_asso.htm")
	public void testcase_asso(HttpSession httpSession, HttpServletResponse response, TestPlanAssoCaseCmd cmd) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			Map<Integer, List<Long>> nodeMap = new HashMap<Integer, List<Long>>();

			TestCasesQueryDto testCaseDto = (TestCasesQueryDto) httpSession.getAttribute(TreeUtils.TC_FILETER_COMMAND_KEY);
			testCaseDto.setTestProjectIdCosStr(cmd.getTestProjectSelectedIds());

			nodeMap = tcFilterService.getFilterChildIds(StringUtil.strToLongList(cmd.getParentIds()), testCaseDto);

			//tc_id保存asso
			List<Long> tcIds = StringUtil.appendList(nodeMap.get(TreeUtils.CHILD_NODE), StringUtil.strToListObj(cmd.getTestCaseIds()));
			if (null != tcIds && tcIds.size() > 0) {
				testplanStepsMng.saveAssoTcIds(cmd.getPlanStepId(), tcIds, user.getUsername());
			}

			//parent_id通过判断是否已经全选,决定是否保存
			List<Long> parentIds = StringUtil.appendList(nodeMap.get(TreeUtils.FATHER_NODE), StringUtil.strToListObj(cmd.getParentIds()));
			if (null != parentIds && parentIds.size() > 0) {
				testplanStepsMng.saveFullAssoParentIds(cmd.getPlanStepId(), parentIds, user.getUsername());
			}

			//asso-Cloud
			testplanStepsMng.saveAssoCloud(cmd.getPlanStepId(), cmd.getCloudIdStr());

			//update summary
			testPlanService.updatePlanSummary(cmd.getPlanId());
		} catch (Exception e) {
			logger.error("--tc asso error , stepId = " + cmd.getPlanStepId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "list_plan_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testplan/testcase_remove.htm")
	//移除操作-较特殊
	public void testcase_rv(HttpSession httpSession, HttpServletResponse response, TestPlanAssoCaseCmd cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			List<Long> removeTcIds = new ArrayList<Long>();
			removeTcIds.addAll(StringUtil.strToListObj(cmd.getTestCaseIds()));
			removeTcIds.addAll(StringUtil.strToListObj(cmd.getParentIds()));

			/* 
			- 移除
			- 所选所有节点的所有上层节点
			- 选中某个节点移除,意味着其上层节点不再是全选,需从关联树中移除,以便在未关联树中显示 
			- 不区分自动/手动模块
			*/
			List<Long> parentIds = nodesHierarchyMng.getAllParentIds(StringUtil.appendList(StringUtil.strToLongList(cmd.getTestCaseIds()),
					StringUtil.strToLongList(cmd.getParentIds())));
			removeTcIds.addAll(parentIds);

			/*
			 - 移除
			 - 所选节点的所有下层节点 ,文件夹全选后的所有子节点
			 - 区分自动/手动模块
			*/
			Map<Integer, List<Long>> nodeMapAuto = new HashMap<Integer, List<Long>>();
			Map<Integer, List<Long>> nodeMapManual = new HashMap<Integer, List<Long>>();

			//树-手动
			if (!StringUtil.isNull(cmd.getParentIdsManual())) {
				TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
				testCaseDto.setExecutionTypeCos(new Long[] { 1L });
				testCaseDto.setTestProjectId(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId());
				nodeMapManual = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsManual()), testCaseDto);
			}

			//树-自动
			if (!StringUtil.isNull(cmd.getParentIdsAuto())) {
				TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
				testCaseDto.setExecutionTypeCos(new Long[] { 2L });
				testCaseDto.setTestProjectId(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId());
				nodeMapAuto = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsAuto()), testCaseDto);
			}

			//子节点
			if (null != nodeMapAuto) {
				List<Long> tcIds = StringUtil.appendList(nodeMapAuto.get(TreeUtils.CHILD_NODE), nodeMapManual.get(TreeUtils.CHILD_NODE));
				removeTcIds.addAll(tcIds);
			}

			//父节点
			if (null != nodeMapManual) {
				List<Long> fileIds = StringUtil.appendList(nodeMapAuto.get(TreeUtils.FATHER_NODE), nodeMapManual.get(TreeUtils.FATHER_NODE));
				removeTcIds.addAll(fileIds);
			}

			//移除-by tcId
			testplanStepsMng.removeAssoTc(cmd.getPlanStepId(), removeTcIds);
			//update summary
			testPlanService.updatePlanSummary(cmd.getPlanId());
		} catch (Exception e) {
			logger.error("--rv tc asso error , stepId = " + cmd.getPlanStepId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "移除失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "移除成功！", "list_plan_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testplan/testcase_finish.htm")
	public void testcase_finish(HttpServletResponse response, TestPlanAssoCaseCmd cmd, HttpSession session) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			User user = RiaFrameworkUtils.getUseFromSession(session);

			Map<Integer, List<Long>> nodeMapAuto = new HashMap<Integer, List<Long>>();
			Map<Integer, List<Long>> nodeMapManual = new HashMap<Integer, List<Long>>();

			//树-手动
			if (!StringUtil.isNull(cmd.getParentIdsManual())) {
				TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
				testCaseDto.setExecutionTypeCos(new Long[] { 1L });
				testCaseDto.setTestProjectId(user.getCurrentTestProject().getId());
				nodeMapManual = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsManual()), testCaseDto);
			}

			//树-自动
			if (!StringUtil.isNull(cmd.getParentIdsAuto())) {
				TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
				testCaseDto.setExecutionTypeCos(new Long[] { 2L });
				testCaseDto.setTestProjectId(user.getCurrentTestProject().getId());
				nodeMapAuto = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsAuto()), testCaseDto);
			}

			//子节点
			List<Long> tcIds = new ArrayList<Long>();
			if (null != nodeMapAuto) {
				tcIds = StringUtil.appendList(nodeMapAuto.get(TreeUtils.CHILD_NODE), nodeMapManual.get(TreeUtils.CHILD_NODE));
			}

			//父节点
			List<Long> fileIds = new ArrayList<Long>();
			if (null != nodeMapManual) {
				fileIds = StringUtil.appendList(nodeMapAuto.get(TreeUtils.FATHER_NODE), nodeMapManual.get(TreeUtils.FATHER_NODE));
			}

			testplanStepsMng.finishAssoTc(cmd.getPlanStepId(), StringUtil.appendList(tcIds, StringUtil.strToLongList(cmd.getTestCaseIds())), fileIds, cmd.getState(),
					user.getUsername());
			//update summary
			testPlanService.updatePlanSummary(cmd.getPlanId());

			TestTaTaskLog testTaTaskLog = new TestTaTaskLog();
			TestReport testReport = testReportService.getCaseStatis(cmd.getPlanStepId(), null);
			BeanUtils.copyProperties(testReport, testTaTaskLog);

			testTaTaskLog.setPlanStepId(cmd.getPlanStepId());
			testTaTaskLog.setTestPlanId(cmd.getPlanId());
			testTaTaskLog.setCreateUser(user.getUsername());
			testTaTaskLog.setState(Boolean.TRUE);
			testTaTaskLog.setUpdateDate(new Date());
			testTaTaskLog.setResult(cmd.getState());

			testTaTaskLog.setTcTds(JSONArray.fromObject(StringUtil.appendList(tcIds, StringUtil.strToLongList(cmd.getTestCaseIds()))).toString());
			testTaTaskLogMng.save(testTaTaskLog);

		} catch (Exception e) {
			logger.error("--finish tc asso error , stepId = " + cmd.getPlanStepId() + " --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "更新失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "更新成功！", "list_plan_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/testplan/testcase_statis.htm")
	public String getTcStatis(ModelMap model, Long stepId) {
		//		List<TestTaTaskLog> testTaTaskLogs = testTaTaskLogMng.getByPlanStepId(stepId);
		List<TestTaTaskLogDto> testTaTaskLogs = testAutomationMng.getTaTaskLogByPlanStepId(stepId);
		if (testTaTaskLogs == null || testTaTaskLogs.size() == 0) {
			TestReport testReport = testReportService.getCaseStatis(stepId, null);
			testTaTaskLogs = new ArrayList<TestTaTaskLogDto>();
			TestTaTaskLogDto testTaTaskLog = new TestTaTaskLogDto();
			BeanUtils.copyProperties(testReport, testTaTaskLog);
			testTaTaskLog.setPlanStepId(stepId);
			testTaTaskLogs.add(testTaTaskLog);
		}
		model.addAttribute("itemlist", testTaTaskLogs);
		return "testmanage/testcase/stat_testcase";
	}

	@RequestMapping(value = "/testplan/switch_remain_tc.htm")
	public void switch_remain_tc(HttpServletResponse response, Integer exeStatus, HttpSession httpSession, TestPlanAssoCaseCmd cmd, ModelMap model) {
		RiaFrameworkUtils.setTcExeTypeFromSession(httpSession, exeStatus);
		model.addAttribute("cmd", cmd);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "list_plan_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
