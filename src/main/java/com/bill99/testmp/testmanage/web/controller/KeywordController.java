package com.bill99.testmp.testmanage.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

@Controller
public class KeywordController {
	@Autowired
	private KeywordsMng keywordsMng;

	//查询
	@RequestMapping(value = "/keyword/list_keyword.htm")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
			model.addAttribute("listItems", keywordsList);
		}
		return "testmanage/keyword/list_keyword";
	}

	//新增
	@RequestMapping(value = "/keyword/add_keyword.htm")
	public String add(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		model.addAttribute("testProjectId", RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId());
		return "testmanage/keyword/add_keyword";
	}

	//保存
	@RequestMapping(value = "/keyword/save_keyword.htm")
	public void save(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession, Keywords keywords) {
		keywordsMng.save(keywords);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_keyword", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//编辑
	@RequestMapping(value = "/keyword/edit_keyword.htm")
	public String edit(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		model.addAttribute("item", keywordsMng.getById(testManageCmd.getKeywordId()));
		return "testmanage/keyword/edit_keyword";
	}

	//更新
	@RequestMapping(value = "/keyword/update_keyword.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, Keywords keywords) {
		keywordsMng.update(keywords);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_keyword", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//删除
	@RequestMapping(value = "/keyword/del_keyword.htm")
	public void del(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, Keywords keywords) {
		keywordsMng.delete(keywords.getId());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", "list_keyword", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
