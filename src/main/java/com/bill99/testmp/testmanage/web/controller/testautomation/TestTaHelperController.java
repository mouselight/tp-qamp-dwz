package com.bill99.testmp.testmanage.web.controller.testautomation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.testmp.ta.mdp.api.service.TaTaskService;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestTaHelperController {
	@Autowired
	private TaTaskService taTaskService;

	//工具集合
	@RequestMapping(value = "/testautomation/index_tahelper.htm")
	public String index_helper(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "testmanage/testautomation/index_helper";
	}

	//触发所有组自动化任务(下个上线日)
	@RequestMapping(value = "/testautomation/auto_tiggertask.htm")
	public void auto_tiggertask(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxDoneUtil ajaxDoneUtil = null;
		taTaskService.autoTiggerTask();
		ajaxDoneUtil = new AjaxDoneUtil("200", "触发成功！", "index_tahelper", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
