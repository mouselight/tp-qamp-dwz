package com.bill99.testmp.testmanage.web.controller.command;

import com.bill99.riaframework.common.command.PageCmd;

public class TestCaseCloudCommand extends PageCmd {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7618713537530901172L;

	private Long teamId;
	private String name;
	private Long id;
	private String userName;

	private String testCaseIds;
	private String parentIds;

	private Boolean isMust;

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setTestCaseIds(String testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public String getTestCaseIds() {
		return testCaseIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getParentIds() {
		return parentIds;
	}

	public Boolean getIsMust() {
		return isMust;
	}

	public void setIsMust(Boolean isMust) {
		this.isMust = isMust;
	}
}
