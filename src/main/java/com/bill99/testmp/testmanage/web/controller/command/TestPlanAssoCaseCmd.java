package com.bill99.testmp.testmanage.web.controller.command;

import java.util.Date;

public class TestPlanAssoCaseCmd {

	private Long pid;
	private Long planStepId;
	private Long planId;
	private String testCaseIds;
	private String parentIds;
	private String parentIdsAuto;
	private String parentIdsManual;
	private String parentIdsPerformance;
	private Integer state;
	private Integer exeType;
	private String cloudIdStr;

	private String testCaseName;
	private String keywordsCosStr;
	private String taStatusCosStr;
	private String executionTypeCosStr;

	private String taTaskId;//任务ID(手动指定)
	private Boolean isActual;//是否实时
	private Date triggerTime;//非实时时 需要配置触发时间
	private Long idTaTask;

	private Long idTestTaTask;

	private String testProjectSelectedIds;

	private String unitType;
	private Integer stepType;

	private Boolean isTaFail;

	public String getTaTaskId() {
		return taTaskId;
	}

	public void setTaTaskId(String taTaskId) {
		this.taTaskId = taTaskId;
	}

	public Boolean getIsActual() {
		return isActual;
	}

	public void setIsActual(Boolean isActual) {
		this.isActual = isActual;
	}

	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setTestCaseIds(String testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public String getTestCaseIds() {
		return testCaseIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getState() {
		return state;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setExeType(Integer exeType) {
		this.exeType = exeType;
	}

	public Integer getExeType() {
		return exeType;
	}

	public String getParentIdsAuto() {
		return parentIdsAuto;
	}

	public void setParentIdsAuto(String parentIdsAuto) {
		this.parentIdsAuto = parentIdsAuto;
	}

	public String getParentIdsManual() {
		return parentIdsManual;
	}

	public void setParentIdsManual(String parentIdsManual) {
		this.parentIdsManual = parentIdsManual;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getKeywordsCosStr() {
		return keywordsCosStr;
	}

	public void setKeywordsCosStr(String keywordsCosStr) {
		this.keywordsCosStr = keywordsCosStr;
	}

	public String getTaStatusCosStr() {
		return taStatusCosStr;
	}

	public void setTaStatusCosStr(String taStatusCosStr) {
		this.taStatusCosStr = taStatusCosStr;
	}

	public String getExecutionTypeCosStr() {
		return executionTypeCosStr;
	}

	public void setExecutionTypeCosStr(String executionTypeCosStr) {
		this.executionTypeCosStr = executionTypeCosStr;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public void setCloudIdStr(String cloudIdStr) {
		this.cloudIdStr = cloudIdStr;
	}

	public String getCloudIdStr() {
		return cloudIdStr;
	}

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public Long getIdTestTaTask() {
		return idTestTaTask;
	}

	public void setIdTestTaTask(Long idTestTaTask) {
		this.idTestTaTask = idTestTaTask;
	}

	public String getTestProjectSelectedIds() {
		return testProjectSelectedIds;
	}

	public void setTestProjectSelectedIds(String testProjectSelectedIds) {
		this.testProjectSelectedIds = testProjectSelectedIds;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public Integer getStepType() {
		return stepType;
	}

	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}

	public Boolean getIsTaFail() {
		return isTaFail;
	}

	public void setIsTaFail(Boolean isTaFail) {
		this.isTaFail = isTaFail;
	}

	public String getParentIdsPerformance() {
		return parentIdsPerformance;
	}

	public void setParentIdsPerformance(String parentIdsPerformance) {
		this.parentIdsPerformance = parentIdsPerformance;
	}
}
