package com.bill99.testmp.testmanage.web.controller.testplan;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.framework.helper.ExcelExportPlanHelper;
import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectChangeMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;
import com.bill99.testmp.testmanage.web.controller.command.TestIssueCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestPlanController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestplansMng testplansMng;
	@Autowired
	private TestIssueMng testIssueMng;
	@Autowired
	private TestplanStepsMng testplanStepsMng;
	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private TestReportService testReportService;
	@Autowired
	private TestProjectChangeMng testProjectChangeMng;

	@RequestMapping(value = "/testplan/index_testplan.htm")
	public String index_testplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {
		cmd.setStates(new Integer[] { TestPlanStatusEnum.STATUS_0.getValue(), TestPlanStatusEnum.STATUS_1.getValue() });
		return list_testplan(request, response, model, cmd, httpSession);
	}

	//计划列表
	@RequestMapping(value = "/testplan/list_testplan.htm")
	public String list_testplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {

		Pagination pagination = new Pagination();
		Map<String, String> cycleMap = new LinkedHashMap<String, String>();
		try {
			User user = RiaFrameworkUtils.getUseFromSession(httpSession);
			TestProjects currentTestProject = user.getCurrentTestProject();
			cmd.setTeamId(currentTestProject.getJiraTeemId());

			Testplans queryModel = new Testplans();
			//页面条件
			BeanUtils.copyProperties(cmd, queryModel);

			pagination = testplansMng.query(queryModel, cmd.getPageNum(), cmd.getNumPerPage());

			//cycle
			cycleMap = testIssueMng.queryCycle(currentTestProject.getJiraTeemId());
		} catch (Exception e) {
			logger.error("-- query testissue error --", e);
		}
		model.addAttribute("pagination", pagination).addAttribute("cmd", cmd).addAttribute("cycleMap", cycleMap);

		return "testmanage/testplan/trackplan/list_testplan";
	}

	//计划跟踪-明细信息
	@RequestMapping(value = "/testplan/track_testplan.htm")
	public String track_plan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		TestIssue testIssue = new TestIssue();
		Testplans testplan = new Testplans();
		try {
			//需求相关信息
			testIssue = testIssueMng.getIssueDetail(cmd.getIssueId());
			//计划信息
			testplan = testplansMng.findByIssueId(cmd.getIssueId());
			//TC数统计
			testplanStepsMng.getAssoTcCount(testplan.getTestplanSteps());
		} catch (Exception e) {
			logger.error("--issue findById error , issueId = " + cmd.getIssueId() + " --", e);
		}

		model.addAttribute("testIssue", testIssue).addAttribute("testplan", (null == testplan) ? new Testplans() : testplan)
				.addAttribute("testPlanStepList", testplan.getTestplanSteps()).addAttribute("cmd", cmd);
		//项目变更记录页面
		model.addAttribute("testprojectchangeItems", testProjectChangeMng.getByIssueId(testIssue.getIssueId()));

		return "testmanage/testplan/trackplan/track_testplan";
	}

	//计划跟踪-导出用例
	@RequestMapping(value = "/testsuite/export_plantrackcase.htm")
	public void export_testcase(HttpServletResponse response, TestManageCmd cmd) {

		try {
			//			ExcelExportHelper.testSuiteExport4Excel(response, testCasesList, currentTestProject.getPrefix(), excelName);//项目名称  测试需求+步骤描述

			TestReport testReport = new TestReport();

			testReport.setIssueId(cmd.getIssueId());
			testReport.setStepId(cmd.getStepId());
			testReport.setStepType(cmd.getStepType());

			testReportService.exportPlanTrackCase(response, testReport);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}

	}

	//计划跟踪-导出计划
	@RequestMapping(value = "/testsuite/export_plantrack.htm")
	public void export_testcase1(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		//获取组项目名称
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();

		TestIssue testIssue = new TestIssue();
		Testplans testplan = new Testplans();
		try {
			//需求相关信息
			testIssue = testIssueMng.getIssueDetail(cmd.getIssueId());
			//计划信息
			testplan = testplansMng.findByIssueId(cmd.getIssueId());
			//TC数统计
			testplanStepsMng.getAssoTcCount(testplan.getTestplanSteps());

			ExcelExportPlanHelper.planExport4Excel(response, testIssue, testplan, currentTestProject.getPrefix(), "dd");

		} catch (Exception e) {
			logger.error("--issue findById error , issueId = " + cmd.getIssueId() + " --", e);
		}
	}

	//更新计划
	@RequestMapping(value = "/testplan/update_testplan.htm")
	public void update_plan(HttpServletRequest request, HttpServletResponse response, TestManageCmd cmd, HttpSession httpSession) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			TestPlansDto plansDto = new TestPlansDto();
			BeanUtils.copyProperties(cmd, plansDto);
			plansDto.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());

			plansDto.setUnitType(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getUnitType());

			testPlanService.updateTestPlan(plansDto);
			//update summary
			testPlanService.updatePlanSummary(cmd.getPlanId());
		} catch (Exception e) {
			logger.error("-- update testplan error --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "track_testplan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//计划步骤间复制用例
	@RequestMapping(value = "/testplan/copy_testplansteptc.htm")
	public void copy(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			if (testManageCmd.getStartStepId() != null && testManageCmd.getEndStepId() != null) {
				testplanStepsMng.copyTc(testManageCmd.getStartStepId(), testManageCmd.getEndStepId());
			}
		} catch (Exception e) {
			e.getStackTrace();
			ajaxDoneUtil = new AjaxDoneUtil("300", "复制失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "复制成功！", "track_testplan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));

	}
}
