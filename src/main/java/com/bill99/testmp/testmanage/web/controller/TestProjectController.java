package com.bill99.testmp.testmanage.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.testmp.testmanage.common.dto.TaProjectDto;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestProjectCmd;

@Controller
public class TestProjectController {

	@Autowired
	private TestProjectsMng testProjectsMng;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TestAutomationMng testAutomationMng;

	//查询
	@RequestMapping(value = "/testproject/list_testproject.htm")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {

		model.addAttribute("listItems", testProjectsMng.findALl());
		return "testmanage/testproject/list_testproject";
	}

	//新增
	@RequestMapping(value = "/testproject/add_testproject.htm")
	public String add(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {

		return "testmanage/testproject/add_testproject";
	}

	//保存
	@RequestMapping(value = "/testproject/save_testproject.htm")
	public void save(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession, TestProjects testProjects) {
		NodesHierarchy nodesHierarchy = new NodesHierarchy();
		nodesHierarchy.setName(testProjects.getPrefix());
		nodesHierarchy.setStatus(testProjects.getActive() == true ? 1 : 0);
		nodesHierarchy.setParentId(0L);
		nodesHierarchy.setNodeTypeId(NodeTypeUtils.TESTPROJECT);
		nodesHierarchy = nodesHierarchyMng.save(nodesHierarchy);
		testProjects.setId(nodesHierarchy.getId());
		testProjectsMng.save(testProjects);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_testproject", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//编辑
	@RequestMapping(value = "/testproject/edit_testproject.htm")
	public String edit(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		model.addAttribute("item", testProjectsMng.getById(testManageCmd.getTestProjectId()));
		return "testmanage/testproject/edit_testproject";
	}

	//更新
	@RequestMapping(value = "/testproject/update_testproject.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestProjectCmd testProjectCmd) {

		NodesHierarchy nodesHierarchy = nodesHierarchyMng.getById(testProjectCmd.getId());
		nodesHierarchy.setName(testProjectCmd.getPrefix());
		nodesHierarchy.setStatus(testProjectCmd.getActive() == true ? 1 : 0);
		nodesHierarchyMng.update(nodesHierarchy);

		TestProjects testProjects = testProjectsMng.getById(testProjectCmd.getId());
		BeanUtils.copyProperties(testProjectCmd, testProjects, new String[] { "jiraTeemId", "id" });
		testProjectsMng.update(testProjects);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_testproject", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//编辑项目脚本对应关系
	@RequestMapping(value = "/testproject/list_tpta.htm")
	public String list_tpta(ModelMap model, TestManageCmd testManageCmd) {
		model.addAttribute("testProject", testProjectsMng.getById(testManageCmd.getTestProjectId()));
		model.addAttribute("listItems", testAutomationMng.getTaProject(testManageCmd.getTestProjectId()));
		return "testmanage/testproject/list_tpta";
	}

	//新增
	@RequestMapping(value = "/testproject/add_tpta.htm")
	public String add_tpta(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {

		model.addAttribute("cmd", testManageCmd);
		return "testmanage/testproject/add_tpta";
	}

	//保存
	@RequestMapping(value = "/testproject/save_tpta.htm")
	public void save_tpta(TaProjectDto taProjectDto, HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd,
			HttpSession httpSession, TestProjects testProjects) {

		testAutomationMng.saveTaProject(taProjectDto);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功", "list_tpta", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//编辑
	@RequestMapping(value = "/testproject/edit_tpta.htm")
	public String edit_tpta(TaProjectDto taProjectDto, HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd,
			HttpSession httpSession) {
		model.addAttribute("item", testAutomationMng.getTaProjectById(taProjectDto.getIdTaProject()));
		return "testmanage/testproject/edit_tpta";
	}

	//更新
	@RequestMapping(value = "/testproject/update_tpta.htm")
	public void update_tpta(TaProjectDto taProjectDto, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestProjects testProjects) {

		testAutomationMng.updateTaProject(taProjectDto);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_tpta", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//删除
	@RequestMapping(value = "/testproject/del_tpta.htm")
	public void del_tpta(TaProjectDto taProjectDto, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestProjects testProjects) {

		testAutomationMng.deleteTaProject(taProjectDto);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", "list_tpta", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
