package com.bill99.testmp.testmanage.web.controller.command;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.bill99.testmp.framework.Tools.AutoArrayList;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.framework.enums.TestPlanStepEnum;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;

public class TestManageCmd implements Serializable {

	private static final long serialVersionUID = 2232474224994329769L;

	private Long testCaseId;
	private Long tcStepsId;
	private Long testSuiteId;
	private Long testProjectId;
	private Long keywordId;

	private Integer maxStepNum;
	private String testCaseName;
	private Long[] keywordIds;
	private String ids;

	private Long[] testCaseIds;

	private Integer executionType;//测试方式
	private Integer importance;//用例等级
	private Integer status;//状态
	private Integer taStatus;//自动化

	private List<Serializable> tcStepsItems = new AutoArrayList(TcSteps.class);

	private MultipartFile excelfile;

	private String releaseName;
	private String releaseId;
	private Long currentCycleId;

	//计划管理
	private Long stepId;
	private Long planId;
	private Long teamId;
	private Long issueId;
	private Date planBeginDate;
	private Date planEndDate;
	private Integer state;
	private String memo;
	private Long planStepId;

	private List<Long> stepIds;//步骤ID
	private List<Integer> types;//审核步骤
	private List<Date> planBeginDates;//计划开始时间
	private List<Date> planEndDates;//计划结束时间
	private List<String> resources;//分配资源
	private List<String> realnames;//分配资源
	private List<String> memos;//备注
	private List<String> outputs;//产出文档
	private List<Integer> stepStatus;//状态

	private Map<Integer, String> testPlanStepMap;
	private Map<String, String> testPlanStatusMap;

	private Long testSuiteId4Bath;

	//计划跟踪
	private Integer num;
	private Integer qaResult;

	private String testProjectIdCosStr;//选中测试项目

	private Integer nodeOrder;//排序编号

	private Integer stepType;

	private String testProjectSelectedIds;
	private Long parentStepId;

	private String unitType;
	//复制用例开始
	private String startStepId;
	//复制用例结束
	private String endStepId;

	public String getEndStepId() {
		return endStepId;
	}

	public void setEndStepId(String endStepId) {
		this.endStepId = endStepId;
	}

	public String getStartStepId() {
		return startStepId;
	}

	public void setStartStepId(String startStepId) {
		this.startStepId = startStepId;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public Integer getMaxStepNum() {
		return maxStepNum;
	}

	public void setMaxStepNum(Integer maxStepNum) {
		this.maxStepNum = maxStepNum;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public Long getTcStepsId() {
		return tcStepsId;
	}

	public void setTcStepsId(Long tcStepsId) {
		this.tcStepsId = tcStepsId;
	}

	public Long getTestSuiteId() {
		return testSuiteId;
	}

	public void setTestSuiteId(Long testSuiteId) {
		this.testSuiteId = testSuiteId;
	}

	public MultipartFile getExcelfile() {
		return excelfile;
	}

	public void setExcelfile(MultipartFile excelfile) {
		this.excelfile = excelfile;
	}

	public Long[] getKeywordIds() {
		return keywordIds;
	}

	public void setKeywordIds(Long[] keywordIds) {
		this.keywordIds = keywordIds;
	}

	public Integer getExecutionType() {
		return executionType;
	}

	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	public Integer getImportance() {
		return importance;
	}

	public void setImportance(Integer importance) {
		this.importance = importance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTaStatus() {
		return taStatus;
	}

	public void setTaStatus(Integer taStatus) {
		this.taStatus = taStatus;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Long[] getTestCaseIds() {
		return testCaseIds;
	}

	public void setTestCaseIds(Long[] testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public List<Serializable> getTcStepsItems() {
		return tcStepsItems;
	}

	public void setTcStepsItems(List<Serializable> tcStepsItems) {
		this.tcStepsItems = tcStepsItems;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Long getKeywordId() {
		return keywordId;
	}

	public void setKeywordId(Long keywordId) {
		this.keywordId = keywordId;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getReleaseName() {
		return releaseName;
	}

	public void setReleaseName(String releaseName) {
		this.releaseName = releaseName;
	}

	public String getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}

	public Long getCurrentCycleId() {
		return currentCycleId;
	}

	public void setCurrentCycleId(Long currentCycleId) {
		this.currentCycleId = currentCycleId;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanBeginDate(Date planBeginDate) {
		this.planBeginDate = planBeginDate;
	}

	public Date getPlanBeginDate() {
		return planBeginDate;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getState() {
		return state;
	}

	public void setTestPlanStepMap(Map<Integer, String> testPlanStepMap) {
		this.testPlanStepMap = testPlanStepMap;
	}

	public Map<Integer, String> getTestPlanStepMap() {
		return TestPlanStepEnum.getTestPlanStepMap();
	}

	public List<Integer> getTypes() {
		return types;
	}

	public void setTypes(List<Integer> types) {
		this.types = types;
	}

	public List<Date> getPlanBeginDates() {
		return planBeginDates;
	}

	public void setPlanBeginDates(List<Date> planBeginDates) {
		this.planBeginDates = planBeginDates;
	}

	public List<Date> getPlanEndDates() {
		return planEndDates;
	}

	public void setPlanEndDates(List<Date> planEndDates) {
		this.planEndDates = planEndDates;
	}

	public List<String> getResources() {
		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

	public List<String> getMemos() {
		return memos;
	}

	public void setMemos(List<String> memos) {
		this.memos = memos;
	}

	public void setRealnames(List<String> realnames) {
		this.realnames = realnames;
	}

	public List<String> getRealnames() {
		return realnames;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemo() {
		return memo;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTestPlanStatusMap(Map<String, String> testPlanStatusMap) {
		this.testPlanStatusMap = testPlanStatusMap;
	}

	public Map<String, String> getTestPlanStatusMap() {
		return TestPlanStatusEnum.getTestPlanMap();
	}

	public void setStepStatus(List<Integer> stepStatus) {
		this.stepStatus = stepStatus;
	}

	public List<Integer> getStepStatus() {
		return stepStatus;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getNum() {
		return num;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setOutputs(List<String> outputs) {
		this.outputs = outputs;
	}

	public List<String> getOutputs() {
		return outputs;
	}

	public void setStepIds(List<Long> stepIds) {
		this.stepIds = stepIds;
	}

	public List<Long> getStepIds() {
		return stepIds;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Long getStepId() {
		return stepId;
	}

	public void setQaResult(Integer qaResult) {
		this.qaResult = qaResult;
	}

	public Integer getQaResult() {
		return qaResult;
	}

	public Long getTestSuiteId4Bath() {
		return testSuiteId4Bath;
	}

	public void setTestSuiteId4Bath(Long testSuiteId4Bath) {
		this.testSuiteId4Bath = testSuiteId4Bath;
	}

	public String getTestProjectIdCosStr() {
		return testProjectIdCosStr;
	}

	public void setTestProjectIdCosStr(String testProjectIdCosStr) {
		this.testProjectIdCosStr = testProjectIdCosStr;
	}

	public Integer getNodeOrder() {
		return nodeOrder;
	}

	public void setNodeOrder(Integer nodeOrder) {
		this.nodeOrder = nodeOrder;
	}

	public Integer getStepType() {
		return stepType;
	}

	public void setStepType(Integer stepType) {
		this.stepType = stepType;
	}

	public String getTestProjectSelectedIds() {
		return testProjectSelectedIds;
	}

	public void setTestProjectSelectedIds(String testProjectSelectedIds) {
		this.testProjectSelectedIds = testProjectSelectedIds;
	}

	public Long getParentStepId() {
		return parentStepId;
	}

	public void setParentStepId(Long parentStepId) {
		this.parentStepId = parentStepId;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

}
