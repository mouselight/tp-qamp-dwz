package com.bill99.testmp.testmanage.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.manager.ParamConfDetailService;
import com.bill99.riaframework.orm.manager.ParamConfService;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.utils.PropertiesMapping;
import com.bill99.testmp.testmanage.domain.SvnService;
import com.bill99.testmp.testmanage.orm.entity.TestProjectChange;
import com.bill99.testmp.testmanage.orm.manager.TestProjectChangeMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestProjectChangeCmd;

@Controller
public class TestProjectChangeController {
	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());
	@Autowired
	private TestProjectChangeMng testProjectChangeMng;
	@Autowired
	private ParamConfDetailService paramConfDetailService;
	@Autowired
	private PropertiesMapping propertiesMapping;
	@Autowired
	private SvnService svnService;

	//	//查询
	//	@RequestMapping(value = "/testprojectchange/list_projectchange.htm")
	//	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
	//
	//		model.addAttribute("testprojectchangeItems", testProjectChangeMng.getByProjectId(RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId()));
	//		return "/testprojectchange/list_projectchange";
	//	}

	//新增
	@RequestMapping(value = "/testprojectchange/add_projectchange.htm")
	public String add(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		model.addAttribute("issueid", cmd.getIssueId());
		model.addAttribute("create_user", RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
		model.addAttribute("projectchangetype", paramConfDetailService.getConfDetailList(ParamConfService.PROJECT_CHANGE_TYPE));
		return "testmanage/testprojectchange/add_projectchange";
	}

	//保存
	@RequestMapping(value = "/testprojectchange/save_projectchange.htm")
	public void save(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestProjectChangeCmd cmd,
			TestProjectChange testProjectChange, @RequestParam("uploadfile") MultipartFile uploadfile) {

		//MultipartFile是对当前上传的文件的封装，当要同时上传多个文件时，可以给定多个MultipartFile参数
		if (!uploadfile.isEmpty()) {

			//保存文件路径
			testProjectChange.setChangeattach(svnCommit(cmd, uploadfile));

		}

		testProjectChangeMng.save(testProjectChange);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功", "track_testplan", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//编辑
	@RequestMapping(value = "/testprojectchange/edit_projectchange.htm")
	public String edit(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestProjectChangeCmd cmd, HttpSession httpSession) {
		model.addAttribute("projectchangetype", paramConfDetailService.getConfDetailList(ParamConfService.PROJECT_CHANGE_TYPE));
		model.addAttribute("update_user", RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
		model.addAttribute("item", testProjectChangeMng.getByChangeId(cmd.getChangeid()));
		return "testmanage/testprojectchange/edit_projectchange";
	}

	//	更新
	@RequestMapping(value = "/testprojectchange/update_projectchange.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession, TestProjectChangeCmd cmd,
			TestProjectChange testProjectChange, @RequestParam("uploadfile") MultipartFile uploadfile) {
		if (!uploadfile.isEmpty()) {

			//保存文件路径
			testProjectChange.setChangeattach(svnCommit(cmd, uploadfile));

		}

		testProjectChangeMng.update(testProjectChange);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "track_testplan", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//删除
	@RequestMapping(value = "/testprojectchange/del_projectchange.htm")
	public void del_change(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, TestProjectChange testProjectChange) {
		testProjectChangeMng.delete(testProjectChange.getChangeid());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", "track_testplan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	/**上传文件至SVN
	 * http://svn.99bill.net/opt/99billdoc/PUBLIC/POA/QA/QAMP-ProjectChangeList/
	 * @param cmd
	 * @param uploadfile
	 * @return
	 */
	private String svnCommit(TestProjectChangeCmd cmd, MultipartFile uploadfile) {
		//具体每一条记录放一个文件夹
		String folder = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String path = propertiesMapping.getSnvProjectChangeList();
		//		Issueid
		String svnPath = String.valueOf(cmd.getIssueid());
		try {
			mkFileDir(path, svnPath);
			//commit
			byte[] bytes = uploadfile.getBytes();
			//			System.out.println(bytes.length);
			svnService.addFileToSvn(path + "/" + svnPath, folder, cmd.getUploadfileName()
					.substring(cmd.getUploadfileName().lastIndexOf("\\") + 1, cmd.getUploadfileName().length()), bytes, propertiesMapping.getSvnUser(), propertiesMapping
					.getSvnPwd(), propertiesMapping.getSvnCommitDescription());
			return path + "/" + svnPath + "/" + folder;
		} catch (Exception e) {
			logger.error("-- commit svn error--", e);
		}
		return "";
	}

	private void mkFileDir(String path, String svnPath) {
		File file_ = new File(path + "/" + svnPath);
		if (!file_.exists()) {
			svnService.createSvnPath(path, svnPath, propertiesMapping.getSvnUser(), propertiesMapping.getSvnPwd(), "创建文件夹" + svnPath);
		}
	}
}
