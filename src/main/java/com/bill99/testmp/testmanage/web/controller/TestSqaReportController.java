package com.bill99.testmp.testmanage.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.testmp.testmanage.common.dto.TaReportDto;
import com.bill99.testmp.testmanage.common.dto.TaSqaReportDto;
import com.bill99.testmp.testmanage.orm.manager.JiraSqaReportMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutolistOnlineReportMng;

@Controller
public class TestSqaReportController {

	@Autowired
	private TestAutolistOnlineReportMng testAutolistOnlineReportMng;
	
	@Autowired
	private JiraSqaReportMng jiraSqaReportMng;

	@RequestMapping("/testreport/index_sqabugcategoryreport.htm")
	public String index_onlineprojectreport(TaSqaReportDto taSqaReportDto, ModelMap model) {

		model.addAttribute("cyclelist", testAutolistOnlineReportMng.getCycleList());// 上线cycle
		List<TaSqaReportDto> dtonewDtos = new ArrayList<TaSqaReportDto>();
		if(taSqaReportDto.getCycles() != null && taSqaReportDto.getCycles().size()>0){
		//	List<TaSqaReportDto> dtoProjectNameDtos = jiraSqaReportMng.getProjectName(taSqaReportDto);
			List<TaReportDto> dtoProjectNameDtos = testAutolistOnlineReportMng.getAllproject();
			
			List<TaSqaReportDto> dtoBugcategoryDevDtos = jiraSqaReportMng.getBugcategoryDev(taSqaReportDto);
			List<TaSqaReportDto> dtoBugcategoryStage02Dtos = jiraSqaReportMng.getBugcategoryStage02(taSqaReportDto);
			List<TaSqaReportDto> dtoBugcategoryProDtos = jiraSqaReportMng.getBugcategoryPro(taSqaReportDto);
			
		
			for(TaReportDto dto:dtoProjectNameDtos){
				TaSqaReportDto dto1 = new TaSqaReportDto();
				dto1.setProjectname(dto.getPrefix());
				dto1.setProjectnameid(dto.getTeamid());
				dto1.setCycles(taSqaReportDto.getCycles());
				
				for(TaSqaReportDto dto5: dtoBugcategoryDevDtos){
					if(dto1.getProjectnameid().equals(dto5.getProjectnameid())){
						dto1.setReqbugsnumdev(dto5.getReqbugsnum());
						dto1.setCodebugsnumdev(dto5.getCodebugsnum());
						dto1.setDesignbugsnumdev(dto5.getDesignbugsnum());
						dto1.setEnvbugsnumdev(dto5.getEnvbugsnum());
						dto1.setOtherbugsnumdev(dto5.getOtherbugsnum());
						if(dto5.getAllcategorybugsnumdev()!=null){
							dto1.setAllcategorybugsnumdev(dto5.getAllcategorybugsnumdev());
							}
							else{
								dto1.setAllcategorybugsnumdev(0l);
							}
					}
				}
				for(TaSqaReportDto dto6: dtoBugcategoryStage02Dtos){
					if(dto1.getProjectnameid().equals(dto6.getProjectnameid())){
						
						dto1.setReqbugsnumstage02(dto6.getReqbugsnum());
						dto1.setDesignbugsnumstage02(dto6.getDesignbugsnum());
						dto1.setCodebugsnumstage02(dto6.getCodebugsnum());
						dto1.setEnvbugsnumstage02(dto6.getEnvbugsnum());
						dto1.setOtherbugsnumstage02(dto6.getOtherbugsnum());
						
						if(dto6.getAllcategorybugsnumstage02()!=null){
							dto1.setAllcategorybugsnumstage02(dto6.getAllcategorybugsnumstage02());
							}
							else{
								dto1.setAllcategorybugsnumstage02(0l);
							}
					}
				}
				
				for(TaSqaReportDto dto7: dtoBugcategoryProDtos){
					if(dto7.getProjectnameid().equals(dto1.getProjectnameid())){
					
						dto1.setReqbugsnumpro(dto7.getReqbugsnum());
						dto1.setDesignbugsnumpro(dto7.getDesignbugsnum());
						dto1.setEnvbugsnumpro(dto7.getEnvbugsnum());	
						dto1.setOtherbugsnumpro(dto7.getOtherbugsnum());
						dto1.setCodebugsnumpro(dto7.getCodebugsnum());
						if(dto7.getAllcategorybugsnumpro()!=null){
							dto1.setAllcategorybugsnumpro(dto7.getAllcategorybugsnumpro());
							}
							else{
								dto1.setAllcategorybugsnumpro(0l);
							}
					}
				}
		
				
				if(dto1.getAllcategorybugsnumdev() != null ){
					dto1.setAllcategorybugsnumdev(dto1.getAllcategorybugsnumdev());
				}
				else{
					dto1.setAllcategorybugsnumdev(0l);
				}
				if( dto1.getAllcategorybugsnumstage02() != null ){
					dto1.setAllcategorybugsnumstage02(dto1.getAllcategorybugsnumstage02());
				}
				else{
					dto1.setAllcategorybugsnumstage02(0l);
				}
				
				if( dto1.getAllcategorybugsnumpro() != null){
					dto1.setAllcategorybugsnumpro(dto1.getAllcategorybugsnumpro());
				}
				else{
					dto1.setAllcategorybugsnumpro(0l);
				}
				
				dto1.setAllcategorybugsnum(dto1.getAllcategorybugsnumdev()+dto1.getAllcategorybugsnumstage02()+dto1.getAllcategorybugsnumpro());
				
				dtonewDtos.add(dto1);	
				
		}
                model.addAttribute("bugreportlist", dtonewDtos);	
		
			
			}
		return "testmanage/testreport/sqabugcategoryreport";	
		
		
			
		}

		

	
	
	@RequestMapping("/testreport/index_sqabuglevelreport.htm")
	public String SqaReportLevelBug(TaSqaReportDto taSqaReportDto, ModelMap model) {

		model.addAttribute("cyclelist", testAutolistOnlineReportMng.getCycleList());// 上线cycle
		if(taSqaReportDto.getCycles() != null && taSqaReportDto.getCycles().size()>0){
		//	List<TaSqaReportDto> dtoProjectNameDtos = jiraSqaReportMng.getProjectName(taSqaReportDto);
			List<TaReportDto> dtoProjectNameDtos = testAutolistOnlineReportMng.getAllproject();
			//System.out.println("1size====" + dtoProjectNameDtos.size());
			List<TaSqaReportDto> dtoBugLevelDevDtos = jiraSqaReportMng.getBugLevelDev(taSqaReportDto);
			List<TaSqaReportDto> dtoBugLeverStage02Dtos = jiraSqaReportMng.getBugLevelStage02(taSqaReportDto);
			List<TaSqaReportDto> dtoBugLeverProDtos = jiraSqaReportMng.getBugLevelPro(taSqaReportDto);
			
			List<TaSqaReportDto> dtonewDtos = new ArrayList<TaSqaReportDto>();
			for(TaReportDto dto:dtoProjectNameDtos){
				TaSqaReportDto dto1 = new TaSqaReportDto();
				dto1.setProjectname(dto.getPrefix());
				dto1.setProjectnameid(dto.getTeamid());
				dto1.setCycles(taSqaReportDto.getCycles());
				for(TaSqaReportDto dto2:dtoBugLevelDevDtos){
					
					if(dto2.getProjectnameid().equals(dto1.getProjectnameid())){
						dto1.setBlockerbugsnumdev(dto2.getBlockerbugsnum());
					    dto1.setCriticalbugsnumdev(dto2.getCriticalbugsnum());
					    dto1.setMajorbugsnumdev(dto2.getMajorbugsnum());
					    dto1.setMinorbugnumdev(dto2.getMinorbugnum());
					    dto1.setTrivialbugnumdev(dto2.getTrivialbugnum());
						if(dto2.getAlllevelbugsnumdev()!=null){
							dto1.setAlllevelbugsnumdev(dto2.getAlllevelbugsnumdev());
						}
						else{
							dto1.setAlllevelbugsnumdev(0l);
						}
						if(dto2.getLevelallissue()!=null){
							dto1.setLevelallissue(dto2.getLevelallissue());
						}
						else{
							dto1.setLevelallissue(0l);
						}
					}
				}
				for(TaSqaReportDto dto3:dtoBugLeverStage02Dtos){
					if(dto3.getProjectnameid().equals(dto1.getProjectnameid())){
						
						dto1.setBlockerbugsnumstage02(dto3.getBlockerbugsnum());
					    dto1.setCriticalbugsnumstage02(dto3.getCriticalbugsnum());
						dto1.setMajorbugsnumstage02(dto3.getMajorbugsnum());
						dto1.setMinorbugnumstage02(dto3.getMinorbugnum());
					    dto1.setTrivialbugnumstage02(dto3.getTrivialbugnum());
							
						if(dto3.getAlllevelbugsnumstage02()!=null){
							dto1.setAlllevelbugsnumstage02(dto3.getAlllevelbugsnumstage02());
						}
						else{
							dto1.setAlllevelbugsnumstage02(0l);
						}
					}
				}
				
				for(TaSqaReportDto dto4:dtoBugLeverProDtos){
					if(dto4.getProjectnameid().equals(dto1.getProjectnameid())){
						dto1.setBlockerbugsnumpro(dto4.getBlockerbugsnum());
						dto1.setCriticalbugsnumpro(dto4.getCriticalbugsnum());
						dto1.setMajorbugsnumpro(dto4.getMajorbugsnum());
						dto1.setMinorbugnumpro(dto4.getMinorbugnum());
					    dto1.setTrivialbugnumpro(dto4.getTrivialbugnum());
							
						if(dto4.getAlllevelbugsnumpro()!=null){
							dto1.setAlllevelbugsnumpro(dto4.getAlllevelbugsnumpro());
						}
						else{
							dto1.setAlllevelbugsnumpro(0l);
						}
					}
				}
				
				
				if(dto1.getAlllevelbugsnumdev() != null){
					dto1.setAlllevelbugsnumdev(dto1.getAlllevelbugsnumdev());
					
				}
				else{
					dto1.setAlllevelbugsnumdev(0l);
				}
				
				if(dto1.getAlllevelbugsnumstage02() != null )
				{
					dto1.setAlllevelbugsnumstage02(dto1.getAlllevelbugsnumstage02());
				}
				else{
					dto1.setAlllevelbugsnumstage02(0l);
				}
				if( dto1.getAlllevelbugsnumpro() != null)
				{
					dto1.setAlllevelbugsnumpro(dto1.getAlllevelbugsnumpro());
				}
				else{
					dto1.setAlllevelbugsnumpro(0l);
				}
				dto1.setAlllevelbugsnum(dto1.getAlllevelbugsnumdev() + dto1.getAlllevelbugsnumstage02() + dto1.getAlllevelbugsnumpro());
				
				dtonewDtos.add(dto1);	
				
				
			}
			//System.out.println("=======size======" + dtonewDtos.size());
			model.addAttribute("bugreportlist", dtonewDtos);
			
		}
	
		return "testmanage/testreport/sqabuglevelreport";
	}

	@RequestMapping("/testreport/index_sqabugstatusreport.htm")
	public String SqaBugStatusReport(TaSqaReportDto taSqaReportDto, ModelMap model){
		model.addAttribute("cyclelist", testAutolistOnlineReportMng.getCycleList());// 上线cycle
		if(taSqaReportDto.getCycles() != null && taSqaReportDto.getCycles().size()>0){
			List<TaReportDto> dtoProjectNameDtos = testAutolistOnlineReportMng.getAllproject();
			List<TaSqaReportDto> dtoBugStatusDtos = jiraSqaReportMng.getBugstatus(taSqaReportDto);
			List<TaSqaReportDto> dtonewDtos = new ArrayList<TaSqaReportDto>();
			for(TaReportDto dto:dtoProjectNameDtos){
				TaSqaReportDto dto1 = new TaSqaReportDto();
				dto1.setProjectname(dto.getPrefix());
				dto1.setProjectnameid(dto.getTeamid());
				dto1.setCycles(taSqaReportDto.getCycles());
				for(TaSqaReportDto dto2:dtoBugStatusDtos){
					if(dto1.getProjectnameid().equals(dto2.getProjectnameid())){
						dto1.setAllissuestatusnum(dto2.getAllissuestatusnum());
						dto1.setAllbugsstatusnum(dto2.getAllbugsstatusnum());
						dto1.setOpenbugsnum(dto2.getOpenbugsnum());
						dto1.setRejectbugsnum(dto2.getRejectbugsnum());
						dto1.setReopenbugsnum(dto2.getReopenbugsnum());
						dto1.setFixedbugsnum(dto2.getFixedbugsnum());
						dto1.setClosedbugsnum(dto2.getClosedbugsnum());
						dto1.setLaterbugsnum(dto2.getLaterbugsnum());
						
					}
					
				}
				dtonewDtos.add(dto1);
			}
		model.addAttribute("bugstatuslist", dtonewDtos);
		}
		
		return "testmanage/testreport/sqabugstatusreport";	
	}
	
	@RequestMapping("/testreport/index_sqabugenvreport.htm")
	public String SqaBugEnvReport(TaSqaReportDto taSqaReportDto, ModelMap model){
		model.addAttribute("cyclelist", testAutolistOnlineReportMng.getCycleList());// 上线cycle
		if(taSqaReportDto.getCycles() != null && taSqaReportDto.getCycles().size()>0){
			List<TaReportDto> dtoProjectNameDtos = testAutolistOnlineReportMng.getAllproject();
			List<TaSqaReportDto> dtoEnvBugDtos = jiraSqaReportMng.getEnvBug(taSqaReportDto);
			List<TaSqaReportDto> dtonewDtos = new ArrayList<TaSqaReportDto>();
			for(TaReportDto dto:dtoProjectNameDtos){
				TaSqaReportDto dto1 = new TaSqaReportDto();
				dto1.setProjectname(dto.getPrefix());
				dto1.setProjectnameid(dto.getTeamid());
				dto1.setCycles(taSqaReportDto.getCycles());
				for(TaSqaReportDto dto2:dtoEnvBugDtos){
					if(dto1.getProjectnameid().equals(dto2.getProjectnameid())){
						dto1.setAllbugsenvnum(dto2.getAllbugsenvnum());
						dto1.setDevbugsnum(dto2.getDevbugsnum());
						dto1.setStage02bugsnum(dto2.getStage02bugsnum());
						dto1.setProbugsnum(dto2.getProbugsnum());
					}
					
				}
				dtonewDtos.add(dto1);
			}
		model.addAttribute("bugenvlist", dtonewDtos);
		
		
	}
		return "testmanage/testreport/sqabugenvreport";
}
	
}
