package com.bill99.testmp.testmanage.web.controller.testplan;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
import com.bill99.testmp.testmanage.domain.TestPlanService;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.Testplans;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectChangeMng;
import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestplansMng;
import com.bill99.testmp.testmanage.web.controller.command.TestIssueCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestPlanPrivyController {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private TestPlanService testPlanService;
	@Autowired
	private TestplansMng testplansMng;
	@Autowired
	private TestIssueMng testIssueMng;
	@Autowired
	private TestplanStepsMng testplanStepsMng;
	@Autowired
	private TestProjectChangeMng testProjectChangeMng;

	@RequestMapping(value = "/testplan/index_privy_testplan.htm")
	public String index_privytestplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {
		//页面初始条件
		cmd.setStepStateCos(new Integer[] { TestPlanStatusEnum.STATUS_0.getValue(), TestPlanStatusEnum.STATUS_1.getValue() });
		return list_privytestplan(request, response, model, cmd, httpSession);
	}

	//计划列表
	@RequestMapping(value = "/testplan/list_privy_testplan.htm")
	public String list_privytestplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestIssueCmd cmd, HttpSession httpSession) {
		Pagination pagination = new Pagination();
		try {
			User user = RiaFrameworkUtils.getUseFromSession(httpSession);
			TestProjects currentTestProject = user.getCurrentTestProject();
			//默认组名
			//支持跨组 查看任务
			cmd.setTeamId(currentTestProject.getJiraTeemId());
			//默认用户ID
			cmd.setUserId(user.getId());

			Testplans queryModel = new Testplans();
			//页面条件
			BeanUtils.copyProperties(cmd, queryModel);
			pagination = testplansMng.query(queryModel, cmd.getPageNum(), cmd.getNumPerPage());
		} catch (Exception e) {
			logger.error("-- query testissue error --", e);
		}
		model.addAttribute("pagination", pagination).addAttribute("cmd", cmd);

		return "testmanage/testplan/privyplan/list_privyplan";
	}

	//更新个人计划
	@RequestMapping(value = "/testplan/update_privyplan.htm")
	public void update_privy_plan(HttpServletRequest request, HttpServletResponse response, TestManageCmd cmd) {
		AjaxDoneUtil ajaxDoneUtil = null;
		try {
			TestPlansDto plansDto = new TestPlansDto();
			BeanUtils.copyProperties(cmd, plansDto);
			plansDto.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
			testPlanService.updateTestPlan(plansDto);
		} catch (Exception e) {
			logger.error("-- update privy plan error --", e);
			ajaxDoneUtil = new AjaxDoneUtil("300", "保存失败！", null, null, null, null);
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", "保存成功！", "privy_plan", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//计划明细信息
	@RequestMapping(value = "/testplan/detail_privyplan.htm")
	public String detail_privyplan(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd cmd, HttpSession httpSession) {
		TestIssue testIssue = new TestIssue();
		Testplans testplan = new Testplans();
		try {
			//需求相关信息
			testIssue = testIssueMng.getIssueDetail(cmd.getIssueId());
			//计划信息
			testplan = testplansMng.findByIssueId(cmd.getIssueId());
			//TC数统计
			testplanStepsMng.getAssoTcCount(testplan.getTestplanSteps());
			//子任务的写权限
			testPlanService.hasWriteAuth(RiaFrameworkUtils.getUseFromSession(httpSession).getId(), testplan.getTestplanSteps());
		} catch (Exception e) {
			logger.error("--issue findById error , issueId = " + cmd.getIssueId() + " --", e);
		}
		model.addAttribute("testIssue", testIssue).addAttribute("testplan", (null == testplan) ? new Testplans() : testplan)
				.addAttribute("testPlanStepList", testplan.getTestplanSteps()).addAttribute("cmd", cmd);

		//项目变更记录
		model.addAttribute("testprojectchangeItems", testProjectChangeMng.getByIssueId(testIssue.getIssueId()));

		return "testmanage/testplan/privyplan/detail_privyplan";
	}
}
