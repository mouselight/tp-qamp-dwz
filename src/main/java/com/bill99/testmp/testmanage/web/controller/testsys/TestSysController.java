package com.bill99.testmp.testmanage.web.controller.testsys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.orm.manager.ParamConfDetailService;
import com.bill99.riaframework.orm.manager.ParamConfService;
import com.bill99.testmp.testmanage.common.utils.SysSwitchTypeUtil;
import com.bill99.testmp.testmanage.orm.entity.SysSwitch;
import com.bill99.testmp.testmanage.orm.manager.SysSwitchMng;

@Controller
public class TestSysController {

	@Autowired
	private SysSwitchMng sysSwitchMng;
	@Autowired
	private ParamConfDetailService paramConfDetailService;

	@RequestMapping(value = "/testsys/index_switch.htm")
	public String index_switch(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		model.addAttribute("item", sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLESYS));
		model.addAttribute("item4issue", sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLEISSUE));
		model.addAttribute("jiraProjectRoles", paramConfDetailService.getConfDetailList(ParamConfService.JIRA_PROJECT_ROLE));
		model.addAttribute("jiraIssueRoles", paramConfDetailService.getConfDetailList(ParamConfService.JIRA_ISSUE_ROLE));
		return "testmanage/testsys/index_switch";
	}

	@RequestMapping(value = "/testsys/update_switch.htm")
	public void update_switch(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestSysCmd testSysCmd, HttpSession httpSession) {
		SysSwitch sysSwitch = sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLESYS);
		BeanUtils.copyProperties(testSysCmd, sysSwitch);
		sysSwitchMng.update(sysSwitch);
		model.addAttribute("item", sysSwitch);

		SysSwitch sysSwitch4Issue = sysSwitchMng.getSysSwitch(SysSwitchTypeUtil.SYSSWITCHTYPE_ROLEISSUE);
		sysSwitch4Issue.setNotifyBugRecvaddress(testSysCmd.getNotifyBugRecvaddress4Issue());
		sysSwitch4Issue.setNotifyIssueRecvaddress(testSysCmd.getNotifyIssueRecvaddress4Issue());
		sysSwitch4Issue.setNotifyScheduleRecvaddress(testSysCmd.getNotifyScheduleRecvaddress4Issue());
		sysSwitch4Issue.setNotifyTaRecvaddress(testSysCmd.getNotifyTaRecvaddress4Issue());

		sysSwitchMng.update(sysSwitch4Issue);
		model.addAttribute("item4issue", sysSwitch4Issue);

		model.addAttribute("jiraProjectRoles", paramConfDetailService.getConfDetailList(ParamConfService.JIRA_PROJECT_ROLE));
		model.addAttribute("jiraIssueRoles", paramConfDetailService.getConfDetailList(ParamConfService.JIRA_ISSUE_ROLE));

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "index_switch", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
