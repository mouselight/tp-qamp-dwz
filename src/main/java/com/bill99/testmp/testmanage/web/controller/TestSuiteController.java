package com.bill99.testmp.testmanage.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.framework.helper.ExcelExportHelper;
import com.bill99.testmp.framework.helper.FileWriteHelper;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.domain.ExcelImportService;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

@Controller
public class TestSuiteController {
	private static Log logger = LogFactory.getLog(TestSuiteController.class);

	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private ExcelImportService excelImportService;
	@Autowired
	private KeywordsMng keywordsMng;
	@Autowired
	private TestCasesMng testCasesMng;

	private void initKeywordsList(ModelMap model, HttpSession httpSession) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
		}
		model.addAttribute("keywordsList", keywordsList);
	}

	//查询
	@RequestMapping(value = "/testsuite/list_testcase.htm")
	public String list_testcase(HttpServletRequest request, HttpServletResponse response, ModelMap model, Page page, Integer pageNum, Integer numPerPage,
			TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {

		if (StringUtils.hasLength(testCasesQueryDto.getTestSuiteIdStrs())) {
			testCasesQueryDto.setTestSuiteId(StringUtil.string2LongArray(testCasesQueryDto.getTestSuiteIdStrs())[0]);
		}
		//默认设置--是否针对当前测试集
		if (testCasesQueryDto.getTestSuiteStatus() == null) {
			testCasesQueryDto.setTestSuiteStatus(1);
		}
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		if (currentTestProject != null) {
			testCasesQueryDto.setTestProjectId(currentTestProject.getId());
		}
		initKeywordsList(model, httpSession);

		page.setPageSize(numPerPage == null ? 20 : numPerPage);
		page.setTargetPage(pageNum == null ? 1 : pageNum);

		if (StringUtils.hasLength(testCasesQueryDto.getKeywordsCosStr())) {
			testCasesQueryDto.setKeywordsCos(StringUtil.string2LongArray(testCasesQueryDto.getKeywordsCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getStatusCosStr())) {
			testCasesQueryDto.setStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getStatusCosStr()));
		} else {//默认设置--状态
			testCasesQueryDto.setStatusCos(new Long[] { 1L });
			testCasesQueryDto.setStatusCosStr("1");
		}
		if (StringUtils.hasLength(testCasesQueryDto.getImportanceCosStr())) {
			testCasesQueryDto.setImportanceCos(StringUtil.string2LongArray(testCasesQueryDto.getImportanceCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getExecutionTypeCosStr())) {
			testCasesQueryDto.setExecutionTypeCos(StringUtil.string2LongArray(testCasesQueryDto.getExecutionTypeCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getTaStatusCosStr())) {
			testCasesQueryDto.setTaStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getTaStatusCosStr()));
		}

		List<TestCases> testCasesList = testCasesMng.queryTestcasesList(testCasesQueryDto, page);
		model.addAttribute("testCasesList", testCasesList);
		model.addAttribute("page", page);
		model.addAttribute("queryCmd", testCasesQueryDto);
		return "testmanage/testsuite/list_testcase";
	}

	//批量更改
	@RequestMapping(value = "/testsuite/batch_assign_testcase.htm")
	public String batch_assign_testcase(HttpServletRequest request, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
		}
		model.addAttribute("keywordsList", keywordsList);

		model.addAttribute("testManageCmd", testManageCmd);
		return "testmanage/testsuite/batch_assign";
	}

	//批量更改process
	@RequestMapping(value = "/testsuite/batch_assign_testcase_process.htm")
	public void batch_assign_testcase_process(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {

		Date now = DateUtil.getTimeNow();
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		List<TestCases> testCaseList = new ArrayList<TestCases>();
		Long[] keywordIds = null;
		int rows = 0;
		if (StringUtils.hasLength(testManageCmd.getIds())) {
			for (Long testCaseId : StringUtil.string2LongArray(testManageCmd.getIds())) {
				TestCases testCases = testCasesMng.getById(testCaseId);

				testCases.setNodeOrder(testCases.getNodesHierarchy().getNodeOrder());
				testCases.setTestSuiteId(testCases.getNodesHierarchy().getParentId());

				if (testManageCmd.getExecutionType() != null) {
					testCases.setExecutionType(testManageCmd.getExecutionType());
				}
				if (testManageCmd.getImportance() != null) {
					testCases.setImportance(testManageCmd.getImportance());
				}
				if (testManageCmd.getStatus() != null) {
					testCases.setStatus(testManageCmd.getStatus());
				}
				if (testManageCmd.getTaStatus() != null) {
					testCases.setTaStatus(testManageCmd.getTaStatus());
				}
				if (testManageCmd.getTestSuiteId4Bath() != null) {
					testCases.setTestSuiteId(testManageCmd.getTestSuiteId4Bath());
				}
				if (testManageCmd.getNodeOrder() != null) {
					testCases.setNodeOrder(testManageCmd.getNodeOrder());
				}
				testCases.setUpdateDate(now);
				testCases.setUpdateUser(user.getUsername());

				keywordIds = testManageCmd.getKeywordIds();
				if (keywordIds != null && keywordIds.length > 0) {
					Set<Keywords> keywordsSet = new HashSet<Keywords>();
					for (Long keywordId : keywordIds) {
						keywordsSet.add(keywordsMng.getById(keywordId));
					}
					testCases.setKeywordsSet(keywordsSet);
				}
				testCaseList.add(testCases);
			}
			if (testCaseList.size() > 0) {
				rows = testCasesMng.bath4Update(testCaseList);
			}
		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "批量更新成功(" + rows + ")条!", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	/**
	 * 获取模板
	 */
	@RequestMapping("/testsuite/export_testcase_tmplate.htm")
	public void getTemplate(HttpServletRequest request, HttpServletResponse response) {
		InputStream is = null;
		ClassPathResource templateResource = null;
		try {
			templateResource = new ClassPathResource("/template/testcase_import_template.xls");
			if (null != templateResource) {
				is = templateResource.getInputStream();
				FileWriteHelper.write(response, is, templateResource.getFilename());
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (null != is)
				try {
					is.close();
				} catch (IOException e) {
					logger.error(e);
				}
		}
	}

	/**
	 * 导出测试用例
	 */
	@RequestMapping(value = "/testsuite/export_testcase.htm")
	public void export_testcase(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {
		Page page = new Page();
		page.setPageSize(1000000);
		page.setTargetPage(1);
		if (StringUtils.hasLength(testCasesQueryDto.getTestSuiteIdStrs())) {
			testCasesQueryDto.setTestSuiteId(StringUtil.string2LongArray(testCasesQueryDto.getTestSuiteIdStrs())[0]);
		}
		if (StringUtils.hasLength(testCasesQueryDto.getKeywordsCosStr())) {
			testCasesQueryDto.setKeywordsCos(StringUtil.string2LongArray(testCasesQueryDto.getKeywordsCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getStatusCosStr())) {
			testCasesQueryDto.setStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getStatusCosStr()));
		} else {//默认设置--状态
			testCasesQueryDto.setStatusCos(new Long[] { 1L });
			testCasesQueryDto.setStatusCosStr("1");
		}
		if (StringUtils.hasLength(testCasesQueryDto.getImportanceCosStr())) {
			testCasesQueryDto.setImportanceCos(StringUtil.string2LongArray(testCasesQueryDto.getImportanceCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getExecutionTypeCosStr())) {
			testCasesQueryDto.setExecutionTypeCos(StringUtil.string2LongArray(testCasesQueryDto.getExecutionTypeCosStr()));
		}
		if (StringUtils.hasLength(testCasesQueryDto.getTaStatusCosStr())) {
			testCasesQueryDto.setTaStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getTaStatusCosStr()));
		}

		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		if (currentTestProject != null) {
			testCasesQueryDto.setTestProjectId(currentTestProject.getId());
		}

		List<TestCases> testCasesList = testCasesMng.queryTestcasesList(testCasesQueryDto, page);
		try {
			ExcelExportHelper.testSuiteExport4Excel(response, testCasesList, currentTestProject.getPrefix(), testCasesQueryDto.getTestSuiteId() == null ? "ALL" : nodesHierarchyMng
					.getById(testCasesQueryDto.getTestSuiteId()).getName());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}

	/**
	 * 导入测试用例
	 */
	@RequestMapping(value = "/testsuite/import_testcase.htm")
	public void import_testcase(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		try {
			excelImportService.importExcelTemplateData(testManageCmd, RiaFrameworkUtils.getUseFromSession(httpSession));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "导入成功", null, null, "forward", "testsuite/list_testcase.htm?testSuiteId=" + testManageCmd.getTestSuiteId());
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	/**
	 * 添加测试集
	 */
	@RequestMapping(value = "/testsuite/add_testsuite.htm")
	public String add_testsuite(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		model.addAttribute("testManageCmd", testManageCmd);
		return "testmanage/testsuite/add_testsuite";
	}

	/**
	 * 添加测试集process
	 */
	@RequestMapping(value = "/testsuite/save_testsuite.htm")
	public void save_testsuite(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession, NodesHierarchy node) {

		node.setParentId(testManageCmd.getTestSuiteId());
		node.setNodeTypeId(NodeTypeUtils.TESTSUITE);
		node.setStatus(1);
		nodesHierarchyMng.save(node);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "操作成功", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));

	}

	/**
	 * 修改/删除测试集
	 */
	@RequestMapping(value = "/testsuite/edit_testsuite.htm")
	public String edit_testsuite(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		NodesHierarchy node = nodesHierarchyMng.getById(testManageCmd.getTestSuiteId());
		model.addAttribute("testManageCmd", testManageCmd);
		model.addAttribute("node", node);
		return "testmanage/testsuite/edit_testsuite";
	}

	/**
	 * 修改/删除测试集process
	 */
	@RequestMapping(value = "/testsuite/update_testsuite.htm")
	public void update_testsuite(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession, NodesHierarchy node) {
		NodesHierarchy entity = nodesHierarchyMng.getById(testManageCmd.getTestSuiteId());
		entity.setName(node.getName());
		entity.setStatus(node.getStatus());
		entity.setNodeOrder(node.getNodeOrder());
		nodesHierarchyMng.update(entity);
		nodesHierarchyMng.recursive4InvalidTc(entity);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "操作成功!", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	/**
	 * 复制用例集
	 */
	@RequestMapping(value = "/testsuite/copy_testsuite.htm")
	public String copy_testsuite(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		NodesHierarchy node = nodesHierarchyMng.getById(testManageCmd.getTestSuiteId());
		model.addAttribute("testManageCmd", testManageCmd);
		model.addAttribute("node", node);
		return "testmanage/testsuite/copy_testsuite";
	}

	/**
	 * 复制用例集操作
	 */
	@RequestMapping(value = "/testsuite/copying_testsuite.htm")
	public void copying_testsuite(Long testSuiteIdOld, Long testSuiteId, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession httpSession) {

		nodesHierarchyMng.save4CopyNodesHierarchy(testSuiteIdOld, testSuiteId);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "操作成功!", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
