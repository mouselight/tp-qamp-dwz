package com.bill99.testmp.testmanage.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.rmca.common.util.MyBeanUtils;
import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;

@Controller
public class TestQueryController {
	@Autowired
	private KeywordsMng keywordsMng;
	@Autowired
	private TestCasesMng testCasesMng;

	@RequestMapping(value = "/testcase_manage.htm")
	public String testcase_manage(HttpServletRequest request, HttpServletResponse response, ModelMap model, Page page, Integer pageNum, Integer numPerPage,
			TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {

		//默认设置--状态
		if (testCasesQueryDto.getStatusCos() == null || testCasesQueryDto.getStatusCos().length == 0) {
			testCasesQueryDto.setStatusCos(new Long[] { 1L });
		}

		if (StringUtils.hasLength(testCasesQueryDto.getStatusCosStr())) {
			testCasesQueryDto.setStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getStatusCosStr()));
		}

		//		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		//		page.setPageSize(numPerPage == null ? 20 : numPerPage);
		//		page.setTargetPage(pageNum == null ? 1 : pageNum);

		//		List<TestCases> testCasesList = null;
		//		if (MyBeanUtils.checkBeanEmpty(testCasesQueryDto)) {
		//			testCasesList = new ArrayList<TestCases>();
		//		} else {
		//			TestProjects currentTestProject = user.getCurrentTestProject();
		//			if (currentTestProject != null) {
		//				testCasesQueryDto.setTestProjectId(currentTestProject.getId());
		//			}
		//			testCasesList = testCasesMng.queryTestcasesList(testCasesQueryDto, page);
		//		}

		//		model.addAttribute("testCasesList", testCasesList);
		//		model.addAttribute("page", page);

		initKeywordsList(model, httpSession);
		model.addAttribute("queryCmd", testCasesQueryDto);

		return "framwork/menu/testcase_manage";
	}

	@RequestMapping(value = "/testquery/query_testcase.htm")
	public String query_testcase(HttpServletRequest request, HttpServletResponse response, ModelMap model, Page page, Integer pageNum, Integer numPerPage,
			TestCasesQueryDto testCasesQueryDto, HttpSession httpSession) {

		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		initKeywordsList(model, httpSession);
		page.setPageSize(numPerPage == null ? 20 : numPerPage);
		page.setTargetPage(pageNum == null ? 1 : pageNum);

		List<TestCases> testCasesList = null;
		if (MyBeanUtils.checkBeanEmpty(testCasesQueryDto)) {
			testCasesList = new ArrayList<TestCases>();
		} else {
			TestProjects currentTestProject = user.getCurrentTestProject();
			if (currentTestProject != null) {
				if (StringUtils.hasLength(testCasesQueryDto.getKeywordsCosStr())) {
					testCasesQueryDto.setKeywordsCos(StringUtil.string2LongArray(testCasesQueryDto.getKeywordsCosStr()));
				}
				if (StringUtils.hasLength(testCasesQueryDto.getStatusCosStr())) {
					testCasesQueryDto.setStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getStatusCosStr()));
				}
				if (StringUtils.hasLength(testCasesQueryDto.getImportanceCosStr())) {
					testCasesQueryDto.setImportanceCos(StringUtil.string2LongArray(testCasesQueryDto.getImportanceCosStr()));
				}
				if (StringUtils.hasLength(testCasesQueryDto.getExecutionTypeCosStr())) {
					testCasesQueryDto.setExecutionTypeCos(StringUtil.string2LongArray(testCasesQueryDto.getExecutionTypeCosStr()));
				}
				if (StringUtils.hasLength(testCasesQueryDto.getTaStatusCosStr())) {
					testCasesQueryDto.setTaStatusCos(StringUtil.string2LongArray(testCasesQueryDto.getTaStatusCosStr()));
				}
				if (StringUtils.hasLength(testCasesQueryDto.getTestCaseSn())) {
					String testCaseStr = testCasesQueryDto.getTestCaseSn().replaceAll("[^\\d\\-\\.]*", "");
					if (StringUtils.hasLength(testCaseStr)) {
						Long testCaseId = Long.valueOf(testCaseStr);
						testCasesQueryDto.setTestCaseId(testCaseId);///
					}

				}
				testCasesQueryDto.setTestProjectId(currentTestProject.getId());
			}

			if (StringUtils.hasLength(testCasesQueryDto.getTestCaseSn())) {//sn转换为ID

			}

			testCasesList = testCasesMng.queryTestcasesList(testCasesQueryDto, page);
		}

		model.addAttribute("testCasesList", testCasesList);
		model.addAttribute("page", page);
		model.addAttribute("queryCmd", testCasesQueryDto);
		return "testmanage/testquery/index_query";
	}

	private void initKeywordsList(ModelMap model, HttpSession httpSession) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
		}
		model.addAttribute("keywordsList", keywordsList);
	}

	public static void main(String[] args) {
		String s = "CBS_FO_339522";
		s = s.replaceAll("[^\\d\\-\\.]*", "");
		System.out.println(s);
	}
}
