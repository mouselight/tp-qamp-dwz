package com.bill99.testmp.testmanage.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.orm.entity.Keywords;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.KeywordsMng;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TcStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.web.controller.command.TestCasesCmd;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

@Controller
public class TestCaseController {
	@Autowired
	private TcStepsMng tcStepsMng;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TestCasesMng testCasesMng;
	@Autowired
	private KeywordsMng keywordsMng;
	@Autowired
	private TestAutomationMng testAutomationMng;

	//编辑测试用例
	@RequestMapping(value = "/testcase/edit_testcase.htm")
	public String edit(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		editProcess(request, response, model, testManageCmd, httpSession);
		return "testmanage/testcase/edit_testcase";
	}

	public void editProcess(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		TestCases testCases = testCasesMng.getById(testManageCmd.getTestCaseId());
		NodesHierarchy node = nodesHierarchyMng.getById(testManageCmd.getTestCaseId());
		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
			model.addAttribute("keywordsList", keywordsList);
			//			List<NodesHierarchy> nodesTree = testManageService.getNodesTree(currentTestProject.getId());
			//			for (NodesHierarchy nodesHierarchy : nodesTree) {
			//				System.out.println(nodesHierarchy.getOrderMark() + "_" + nodesHierarchy.getName());
			//			}
		}
		Long maxStepNum = tcStepsMng.getMaxStepNum(testManageCmd.getTestCaseId());
		if (maxStepNum == null) {
			testManageCmd.setMaxStepNum(1);
		} else {
			testManageCmd.setMaxStepNum(maxStepNum.intValue() + 1);
		}

		testManageCmd.setTestSuiteId(node.getParentId());//设置测试集合ID
		testCases.setTestSuiteName(nodesHierarchyMng.getById(node.getParentId()).getName());

		model.addAttribute("taMethods", testAutomationMng.queryTaMethodByTcId(testCases.getTestCaseId()));
		model.addAttribute("testCases", testCases);
		model.addAttribute("node", node);
		model.addAttribute("testManageCmd", testManageCmd);
	}

	//更新测试用例
	@RequestMapping(value = "/testcase/update_testcase.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, TestCasesCmd testCases, TestManageCmd testManageCmd) {

		NodesHierarchy nodesHierarchy = nodesHierarchyMng.getById(testManageCmd.getTestCaseId());
		nodesHierarchy.setName(testManageCmd.getTestCaseName());
		nodesHierarchy.setStatus(testCases.getStatus());
		if (testManageCmd.getTestSuiteId() != null) {
			nodesHierarchy.setParentId(testManageCmd.getTestSuiteId());
		}

		nodesHierarchyMng.update(nodesHierarchy);

		TestCases entity = testCasesMng.getById(testManageCmd.getTestCaseId());
		entity.setTestCaseName(testCases.getTestCaseName());
		entity.setSummary(testCases.getSummary());
		entity.setPreconditions(testCases.getPreconditions());
		entity.setExecutionType(testCases.getExecutionType());
		entity.setImportance(testCases.getImportance());
		entity.setStatus(testCases.getStatus());
		entity.setTaStatus(testCases.getTaStatus());
		entity.setUpdateDate(DateUtil.getTimeNow());
		entity.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());

		Long[] keywordIds = testManageCmd.getKeywordIds();
		if (keywordIds != null && keywordIds.length > 0) {
			Set<Keywords> keywordsSet = new HashSet<Keywords>();
			for (Long keywordId : keywordIds) {
				keywordsSet.add(keywordsMng.getById(keywordId));
			}
			entity.setKeywordsSet(keywordsSet);
		} else {
			entity.setKeywordsSet(null);
		}

		testCasesMng.update(entity);
		tcStepsMng.batch4ReBuildTcSteps(entity.getTestCaseId(), initReSortList(testManageCmd.getTcStepsItems()));

		//		testCasesMng.update(entity);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "ref_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	private List<TcSteps> initReSortList(List<Serializable> icStepsItems) {
		List<TcSteps> reSortList = new ArrayList<TcSteps>();
		int i = 1;
		for (Serializable tcSteps : icStepsItems) {
			TcSteps tcStep = (TcSteps) tcSteps;
			if (StringUtils.hasLength(tcStep.getActions())) {//步骤预期结果不为空时保存
				tcStep.setStepNumber(i);
				reSortList.add(tcStep);
				i++;
			}
		}
		return reSortList;
	}

	private void initKeywordsList(ModelMap model, HttpSession httpSession) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);
		TestProjects currentTestProject = user.getCurrentTestProject();
		List<Keywords> keywordsList = null;
		if (currentTestProject != null) {
			keywordsList = keywordsMng.getByTestProjectId(currentTestProject.getId());
		}
		model.addAttribute("keywordsList", keywordsList);
	}

	//添加测试用例
	@RequestMapping(value = "/testcase/add_testcase.htm")
	public String add_testcase(HttpServletRequest request, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		initKeywordsList(model, httpSession);
		model.addAttribute("testManageCmd", testManageCmd);
		return "testmanage/testcase/add_testcase";
	}

	//保存测试用例
	@RequestMapping(value = "/testcase/save_testcase.htm")
	public void save_testcase(HttpServletRequest request, HttpServletResponse response, TestCases testCases, TestManageCmd testManageCmd, HttpSession httpSession) {

		testCases.setCreateUser(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());

		Long[] keywordIds = testManageCmd.getKeywordIds();
		if (keywordIds != null && keywordIds.length > 0) {
			Set<Keywords> keywordsSet = new HashSet<Keywords>();
			for (Long keywordId : keywordIds) {
				keywordsSet.add(keywordsMng.getById(keywordId));
			}
			testCases.setKeywordsSet(keywordsSet);
		} else {
			testCases.setKeywordsSet(null);
		}

		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		testCases.setTestprojectId(currentTestProject.getId());

		NodesHierarchy node = new NodesHierarchy();
		node.setStatus(testCases.getStatus());
		node.setName(testCases.getTestCaseName());
		node.setParentId(testManageCmd.getTestSuiteId());
		node.setNodeTypeId(NodeTypeUtils.TESTCASE);

		node = nodesHierarchyMng.save(node);
		testCases.setTestCaseId(node.getId());

		testCases = testCasesMng.save(testCases);
		tcStepsMng.batch4ReBuildTcSteps(testCases.getTestCaseId(), initReSortList(testManageCmd.getTcStepsItems()));

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, null, null, "forward", "testcase/edit_testcase.htm?testCaseId=" + testCases.getTestCaseId());
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//复制测试用例
	@RequestMapping(value = "/testcase/copy_testcase.htm")
	public String copy_testcase(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, HttpSession httpSession) {
		editProcess(request, response, model, testManageCmd, httpSession);
		return "testmanage/testcase/copy_testcase";
	}

	//删除测试用例
	@RequestMapping(value = "/testcase/delete_testcase.htm")
	public void delete_testcase(HttpServletRequest request, HttpServletResponse response, TestManageCmd testManageCmd, HttpSession httpSession) {
		testCasesMng.delete(testManageCmd.getTestCaseId());
		nodesHierarchyMng.delete(testManageCmd.getTestCaseId());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", null, null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
