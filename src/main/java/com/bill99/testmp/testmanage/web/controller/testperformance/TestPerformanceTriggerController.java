package com.bill99.testmp.testmanage.web.controller.testperformance;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.testmp.ta.mdp.api.common.TaTaskRequestBo;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.dto.TpTaskRequestBo;
import com.bill99.testmp.testmanage.common.dto.TpTaskResponesBo;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.domain.TpTaskService;
import com.bill99.testmp.testmanage.orm.entity.TcExeLogDetail;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;
import com.bill99.testmp.testmanage.orm.manager.TcExeLogDetailMng;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.orm.manager.TestTaTaskLogMng;
import com.bill99.testmp.testmanage.web.controller.command.TestPlanAssoCaseCmd;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestPerformanceTriggerController {

	@Autowired
	private TestAutomationMng testAutomationMng;
	@Autowired
	private TestTaTaskLogMng testTaTaskLogMng;
	@Autowired
	private TcFilterService tcFilterService;
	@Autowired
	private TestReportService testReportService;
	@Autowired
	private TestCasesMng testCasesMng;
	@Autowired
	private TpTaskService tpTaskService;
	@Autowired
	private TcExeLogDetailMng tcExeLogDetailMng;

	/**
	 * jenkins JobName#默认并发数#默认并发时间15分钟#自定义版本号#输入测试类型：压力测试  基准测试  负载测试  稳定性测试#输入测试接口名称#输入测试描述#输入备注
	 * @param request
	 * @param response
	 * @param cmd
	 * @return
	 */
	@RequestMapping(value = "/testperformance/trigger_tptask.htm")
	public void trigger_tptask(HttpServletResponse response, HttpSession httpSession, ModelMap model, TestPlanAssoCaseCmd cmd, HttpSession session) {
		TaTaskRequestBo taTaskRequestBo = new TaTaskRequestBo();
		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		AjaxDoneUtil ajaxDoneUtil = null;
		BeanUtils.copyProperties(cmd, taTaskRequestBo);

		User user = RiaFrameworkUtils.getUseFromSession(session);

		Map<Integer, List<Long>> nodeMapPerformance = new HashMap<Integer, List<Long>>();
		Map<Integer, List<Long>> nodeMapManual = new HashMap<Integer, List<Long>>();
		//树- 性能
		if (!StringUtil.isNull(cmd.getParentIdsPerformance())) {
			TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
			testCaseDto.setExecutionTypeCos(new Long[] { 3L });
			testCaseDto.setTestProjectId(currentTestProject.getId());
			nodeMapPerformance = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsPerformance()), testCaseDto);
		}
		//子节点
		List<Long> tcIds = new ArrayList<Long>();
		if (null != nodeMapPerformance) {
			tcIds = StringUtil.appendList(nodeMapPerformance.get(TreeUtils.CHILD_NODE), nodeMapManual.get(TreeUtils.CHILD_NODE));
		}

		List<Long> tcs = StringUtil.strToLongList(cmd.getTestCaseIds());
		if (tcs != null && tcs.size() > 0) {
			tcIds.addAll(tcs);
		}

		taTaskRequestBo.setTestPlanId(cmd.getPlanId());
		taTaskRequestBo.setGroupId(cmd.getUnitType());

		if (currentTestProject != null) {
			taTaskRequestBo.setTestProjectId(currentTestProject.getId());
		}

		TestTaTaskLog testTaTaskLog = new TestTaTaskLog();

		TestReport testReport = testReportService.getCaseStatis(cmd.getPlanStepId(), null);

		BeanUtils.copyProperties(taTaskRequestBo, testTaTaskLog);
		testTaTaskLog.setCreateUser(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
		testTaTaskLog.setPlanStepId(cmd.getPlanStepId());
		testTaTaskLog.setAllCnt(testReport.getAllCnt());
		testTaTaskLog.setUpdateDate(new Date());
		testTaTaskLogMng.save(testTaTaskLog);

		testAutomationMng.insertTestTaTaskTcs4Batch(testTaTaskLog.getIdTestTaTask(), taTaskRequestBo.getTcIds());

		taTaskRequestBo.setIdTestTaTask(testTaTaskLog.getIdTestTaTask());
		if (tcs != null) {
			TcExeLogDetail tcExeLogDetail;
			for (Long tcId : tcs) {
				TestCases testCases = testCasesMng.getById(tcId);
				if (testCases != null && testCases.getExecutionType() == 3) {
					JSONObject jsonObject = JSONObject.fromObject(testCases.getPreconditions().toString().trim());
					TpTaskRequestBo tpTaskRequestBo = (TpTaskRequestBo) JSONObject.toBean(jsonObject, TpTaskRequestBo.class);

					TpTaskResponesBo tpTaskResponesBo = tpTaskService.triggerTpTask(tpTaskRequestBo);
					tcExeLogDetail = new TcExeLogDetail();
					tcExeLogDetail.setTestPlanId(cmd.getPlanId());
					tcExeLogDetail.setPlanStepId(cmd.getPlanStepId());
					tcExeLogDetail.setTestProjectId(currentTestProject.getId());
					tcExeLogDetail.setTestCaseName(testCases.getTestCaseName());

					tcExeLogDetailMng.saveExeLogDetail(tcExeLogDetail);
				}
			}
		}
		ajaxDoneUtil = new AjaxDoneUtil("200", testTaTaskLog.getMemo(), "list_plan_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
