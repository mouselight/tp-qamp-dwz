package com.bill99.testmp.testmanage.web.controller.tree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.testmp.testmanage.common.utils.NodeTypeUtils;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@Controller
public class OptionTreeController {

	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@RequestMapping(value = "/option_tree/get_subtree.htm")
	public void get_subtree(HttpServletResponse response, HttpServletRequest request, Long id, HttpSession httpSession) throws Exception {

		if (id == null) {
			id = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject().getId();
		}
		List<NodesHierarchy> nodes = nodesHierarchyMng.getChildSuite(id);
		String json = createRespJSON(nodes);
		//		json = "	{\"221\":\"Baby Safety\",\"226\":\"Baby Toys\",\"236\":\"Baby Transport\",\"240\":\"Diapering\",\"246\":\"Nursing & Feeding\"}";
		ResponseUtils.renderJson(response, json);
		return;
	}

	private String createRespJSON(List<NodesHierarchy> nodes) {
		Map<String, String> jsonMap = new HashMap<String, String>();

		if (nodes != null) {
			for (NodesHierarchy node : nodes) {
				jsonMap.put(node.getId().toString(), node.getName() + getTcQuantity(node));
			}
		}
		return JSONObject.fromObject(jsonMap).toString();
	}

	private String getTcQuantity(NodesHierarchy node) {
		String tcQuantity = "";
		if (node.getNodeTypeId() != null && node.getNodeTypeId().longValue() != NodeTypeUtils.TESTCASE) {
			//			tcQuantity = "(" + calcTcQuantity(node.getId()) + ")"; //实时递归,测试时注释,有性能消耗
			//			tcQuantity = "(TC数)";
			tcQuantity = TreeUtils.TC_QUANTITY_MAP.get(node.getId().toString());
			if (!StringUtils.hasLength(tcQuantity)) {
				tcQuantity = "(0)";
			}
		}
		return tcQuantity;
	}
}
