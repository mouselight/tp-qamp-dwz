package com.bill99.testmp.testmanage.web.controller.command;

public class TestCasesCmd {

	private String testCaseName;
	private String summary;//摘要
	private String preconditions;//前置条件
	private Integer executionType;//测试方式
	private Integer importance;//用例等级
	private Integer status;//状态
	private Integer taStatus;//自动化

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPreconditions() {
		return preconditions;
	}

	public void setPreconditions(String preconditions) {
		this.preconditions = preconditions;
	}

	public Integer getExecutionType() {
		return executionType;
	}

	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	public Integer getImportance() {
		return importance;
	}

	public void setImportance(Integer importance) {
		this.importance = importance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTaStatus() {
		return taStatus;
	}

	public void setTaStatus(Integer taStatus) {
		this.taStatus = taStatus;
	}

}
