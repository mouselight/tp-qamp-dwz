package com.bill99.testmp.testmanage.web.controller.testautomation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.testmp.ta.mdp.api.common.TaTaskRequestBo;
import com.bill99.testmp.ta.mdp.api.common.TaTaskResponesBo;
import com.bill99.testmp.ta.mdp.api.common.UnitUtil;
import com.bill99.testmp.ta.mdp.api.service.TaTaskService;
import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TcFilterService;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.entity.TestReport;
import com.bill99.testmp.testmanage.orm.entity.TestTaTaskLog;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.orm.manager.TestTaTaskLogMng;
import com.bill99.testmp.testmanage.web.controller.command.TestPlanAssoCaseCmd;
import com.jeecms.common.util.ResponseUtils;

@Controller
public class TestAutomationTriggerController {

	@Autowired
	private TestAutomationMng testAutomationMng;
	@Autowired
	private TaTaskService taTaskService;
	@Autowired
	private TestTaTaskLogMng testTaTaskLogMng;
	@Autowired
	private TcFilterService tcFilterService;
	@Autowired
	private TestReportService testReportService;

	@RequestMapping(value = "/testautomation/trigger_tatask.htm")
	public void triggerTaTask(HttpServletResponse response, HttpSession httpSession, TestPlanAssoCaseCmd cmd) {
		TaTaskRequestBo taTaskRequestBo = new TaTaskRequestBo();
		TestProjects currentTestProject = RiaFrameworkUtils.getUseFromSession(httpSession).getCurrentTestProject();
		AjaxDoneUtil ajaxDoneUtil = null;
		BeanUtils.copyProperties(cmd, taTaskRequestBo);

		if (cmd.getIsTaFail() == null || !cmd.getIsTaFail()) {
			Map<Integer, List<Long>> nodeMapAuto = new HashMap<Integer, List<Long>>();
			Map<Integer, List<Long>> nodeMapManual = new HashMap<Integer, List<Long>>();
			//树-自动
			if (!StringUtil.isNull(cmd.getParentIdsAuto())) {
				TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
				testCaseDto.setExecutionTypeCos(new Long[] { 2L });
				testCaseDto.setTestProjectId(currentTestProject.getId());
				nodeMapAuto = tcFilterService.getFilterChildIds(StringUtil.strToListObj(cmd.getParentIdsAuto()), testCaseDto);
			}
			//子节点
			List<Long> tcIds = new ArrayList<Long>();
			if (null != nodeMapAuto) {
				tcIds = StringUtil.appendList(nodeMapAuto.get(TreeUtils.CHILD_NODE), nodeMapManual.get(TreeUtils.CHILD_NODE));
			}
			List<Long> tcs = StringUtil.strToLongList(cmd.getTestCaseIds());
			if (tcs != null && tcs.size() > 0) {
				tcIds.addAll(tcs);
			}

			if (tcIds != null && tcIds.size() > 0) {
				List<Long> testPlanTcs = testAutomationMng.getTestPlanTcs(cmd.getPlanStepId());
				if (testPlanTcs != null) {
					tcIds.retainAll(testPlanTcs);
				}
				Set<Long> tcIdsSet = new HashSet<Long>(tcIds);
				if (StringUtils.hasLength(cmd.getTestCaseIds())) {
					Set<Long> tcIds4zSet = new HashSet<Long>(StringUtil.strToLongList(cmd.getTestCaseIds()));
					tcIdsSet.addAll(tcIds4zSet);
				}
				taTaskRequestBo.setTcIds(tcIdsSet.toArray(new Long[] {}));
			}
		} else {
			taTaskRequestBo.setTcIds(testAutomationMng.getStepTaFailTcs(cmd.getPlanStepId()).toArray(new Long[] {}));
			taTaskRequestBo.setTaTaskId("TA_" + new Date().getTime());
		}

		if (cmd.getStepType() != null && cmd.getStepType().intValue() == 6) {
			taTaskRequestBo.setStageId(UnitUtil.STAGEID_STAGE2);
		}
		if (cmd.getStepType() != null && cmd.getStepType().intValue() == 10) {
			taTaskRequestBo.setStageId(UnitUtil.STAGEID_STAGE3);
		}
		taTaskRequestBo.setTestPlanId(cmd.getPlanId());
		taTaskRequestBo.setGroupId(cmd.getUnitType());

		if (currentTestProject != null) {
			taTaskRequestBo.setTestProjectId(currentTestProject.getId());
		}

		TestTaTaskLog testTaTaskLog = new TestTaTaskLog();

		TestReport testReport = testReportService.getCaseStatis(cmd.getPlanStepId(), null);

		BeanUtils.copyProperties(taTaskRequestBo, testTaTaskLog);
		testTaTaskLog.setCreateUser(RiaFrameworkUtils.getUseFromSession(httpSession).getUsername());
		testTaTaskLog.setPlanStepId(cmd.getPlanStepId());
		testTaTaskLog.setAllCnt(testReport.getAllCnt());
		testTaTaskLog.setUpdateDate(new Date());
		testTaTaskLogMng.save(testTaTaskLog);

		testAutomationMng.insertTestTaTaskTcs4Batch(testTaTaskLog.getIdTestTaTask(), taTaskRequestBo.getTcIds());

		taTaskRequestBo.setTcIds(null);

		taTaskRequestBo.setIdTestTaTask(testTaTaskLog.getIdTestTaTask());

		TaTaskResponesBo taTaskResponesBo = taTaskService.triggerTaTask(taTaskRequestBo);
		testTaTaskLog.setState(taTaskResponesBo.getResult());
		testTaTaskLog.setMemo(taTaskResponesBo.getMemo());
		testTaTaskLogMng.update(testTaTaskLog);

		ajaxDoneUtil = new AjaxDoneUtil("200", testTaTaskLog.getMemo(), "list_testcase", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
