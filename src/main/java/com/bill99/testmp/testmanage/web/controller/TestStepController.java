package com.bill99.testmp.testmanage.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.testmp.testmanage.domain.TestManageService;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TcStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;
import com.bill99.testmp.testmanage.web.controller.command.TestManageCmd;

@Controller
public class TestStepController {

	@Autowired
	private TcStepsMng tcStepsMng;
	@Autowired
	private TestManageService testManageService;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private TestCasesMng testCasesMng;

	@RequestMapping(value = "/teststep/update_teststepnum.htm")
	public void update_index(HttpServletRequest request, HttpServletResponse response, ModelMap model, Long tcstepId, Integer tcstepNum, Long testCaseId) {
		TcSteps tcSteps = tcStepsMng.getById(tcstepId);
		tcSteps.setStepNumber(tcstepNum);
		tcStepsMng.update(tcSteps);

		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "ref_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//删除步骤
	@RequestMapping(value = "/teststep/del_teststep.htm")
	public void del_teststep(HttpServletRequest request, HttpServletResponse response, ModelMap model, Long tcstepId, Long testCaseId) {
		TestCases testCase = testCasesMng.getById(testCaseId);
		List<TcSteps> tcStepsList = testCase.getTcStepsList();
		if (tcStepsList != null && tcStepsList.size() > 0) {
			int i = 1;
			for (TcSteps tcSteps : tcStepsList) {
				if (tcSteps.getId().longValue() != tcstepId.longValue()) {
					tcSteps.setStepNumber(i);
					i++;
					tcStepsMng.update(tcSteps);
				} else {
					tcStepsMng.delete(tcSteps);
				}

			}

		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "ref_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/teststep/add_teststep.htm")
	public String add(HttpServletRequest request, ModelMap model, TestManageCmd testManageCmd) {
		Long maxStepNum = tcStepsMng.getMaxStepNum(testManageCmd.getTestCaseId());
		if (maxStepNum == null) {
			testManageCmd.setMaxStepNum(1);
		} else {
			testManageCmd.setMaxStepNum(maxStepNum.intValue() + 1);
		}
		model.addAttribute("testManageCmd", testManageCmd);
		return "testmanage/teststep/add_teststep";
	}

	@RequestMapping(value = "/teststep/save_teststep.htm")
	public void save(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, TcSteps tcSteps) {
		tcSteps.setActive(1);
		tcSteps.setExecutionType(1);
		tcStepsMng.save(tcSteps);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "ref_tc", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/teststep/edit_teststep.htm")
	public String edit(HttpServletRequest request, ModelMap model, TestManageCmd testManageCmd) {
		model.addAttribute("testManageCmd", testManageCmd);
		model.addAttribute("tcSteps", tcStepsMng.getById(testManageCmd.getTcStepsId()));
		return "testmanage/teststep/edit_teststep";
	}

	@RequestMapping(value = "/teststep/update_teststep.htm")
	public void update(HttpServletRequest request, HttpServletResponse response, ModelMap model, TestManageCmd testManageCmd, TcSteps tcSteps) {
		TcSteps entity = tcStepsMng.getById(testManageCmd.getTcStepsId());
		entity.setActions(tcSteps.getActions());
		entity.setExpectedResults(tcSteps.getExpectedResults());
		entity.setStepNumber(tcSteps.getStepNumber());
		tcStepsMng.update(entity);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", null, "ref_tc", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
}
