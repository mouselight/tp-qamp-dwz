package com.bill99.testmp.testmanage.web.controller.testautomation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.helper.HttpFixtureHelper;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.orm.manager.TestAutomationMng;
import com.bill99.testmp.testmanage.web.controller.command.TestPlanAssoCaseCmd;

@Controller
public class TestTaTaskController {
	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());
	@Autowired
	private TestAutomationMng testAutomationMng;

	@RequestMapping(value = "/testautomation/show_console.htm")
	public String show_console(String jobName, String buildNum, ModelMap model) {
		StringBuilder url = new StringBuilder("http://192.168.15.192:8081/jenkins/job/");
		url.append(jobName);
		url.append("/");
		url.append(buildNum);
		url.append("/consoleText");
		HttpFixtureHelper requery = new HttpFixtureHelper();
		requery.setUrl(url.toString());
		requery.Get();
		model.addAttribute("consoleText", requery.getResponseBody());
		model.addAttribute("consoleTexts", requery.getResponseBody().split("[\r|\n]"));
		return "testmanage/testplan/tatask/show_console";
	}

	@RequestMapping(value = "/testautomation/list_tatask_log_tc.htm")
	public String list_tatask_log_tc(Integer type, HttpServletResponse response, HttpSession httpSession, TestPlanAssoCaseCmd cmd, ModelMap model) {

		Map<String, List<TaResultDto>> taResultMap = new HashMap<String, List<TaResultDto>>();
		Map<String, List<TaTaskMethodResultDto>> taTaskMethodResultMap = new HashMap<String, List<TaTaskMethodResultDto>>();

		List<TaTaskMethodResultDto> taTaskMethodResultDtos = testAutomationMng.getTaTaskMethodResult(cmd.getIdTestTaTask(), cmd.getPlanStepId(), type);
		List<TaTaskMethodResultDto> taTaskMethodResultDtos1 = testAutomationMng.getTaTaskMethodResult(cmd.getIdTestTaTask(), cmd.getPlanStepId(), 0);
		List<TaTaskMethodResultDto> taTaskMethodResultDtos2 = testAutomationMng.getTaTaskMethodResult(cmd.getIdTestTaTask(), cmd.getPlanStepId(), 1);

		double errorCount = taTaskMethodResultDtos1.size();
		double SuccessCount = taTaskMethodResultDtos2.size();
		double sumCount = errorCount + SuccessCount;
		double successsCale = (SuccessCount / sumCount) * 100;
		DecimalFormat df = new DecimalFormat("#.00");

		if (taTaskMethodResultDtos != null) {
			List<TaResultDto> taResults = null;
			List<TaResultDto> taResultTmps = null;
			List<TaTaskMethodResultDto> taTaskMethodResults = null;
			for (TaTaskMethodResultDto taTaskMethodResultDto : taTaskMethodResultDtos) {

				String key = taTaskMethodResultDto.getClassName() + taTaskMethodResultDto.getMethodName();
				if (taResultMap.get(key) != null) {
					taResults = taResultMap.get(key);
				} else {
					taResults = new ArrayList<TaResultDto>();
					taResultTmps = testAutomationMng.getTaResult(taTaskMethodResultDto.getIdTaTask(), taTaskMethodResultDto.getIdSvnMethod());
					if (taResultTmps != null) {
						taResults.addAll(taResultTmps);
					}
				}
				taResultMap.put(key, taResults);

				//大集合
				if (taTaskMethodResultMap.get(key) != null) {
					taTaskMethodResults = taTaskMethodResultMap.get(key);
				} else {
					taTaskMethodResults = new ArrayList<TaTaskMethodResultDto>();
				}
				taTaskMethodResults.add(taTaskMethodResultDto);
				taTaskMethodResultMap.put(key, taTaskMethodResults);
			}
			model.addAttribute("taResultMap", taResultMap);
			model.addAttribute("taTaskMethodResultMap", taTaskMethodResultMap);
			model.addAttribute("errorCount", errorCount);
			model.addAttribute("SuccessCount", SuccessCount);
			model.addAttribute("sumCount", sumCount);
			model.addAttribute("successsCale", df.format(successsCale));

		}

		model.addAttribute("testTaTaskLogs", testAutomationMng.getTaTaskLogById(cmd.getIdTestTaTask()));
		//		model.addAttribute("testTaTaskLog", testTaTaskLogMng.get(cmd.getIdTestTaTask()));
		//		model.addAttribute("taTaskTcs", testCasesMng.getTaTaskTcs(cmd.getTaTaskId()));
		model.addAttribute("cmd", cmd);

		return "testmanage/testplan/tatask/list_tatask_log_tc";
	}
}
