package com.bill99.testmp.testmanage.web.controller.testsys;

public class TestSysCmd {

	private Boolean notifyIssueState;
	private String notifyIssueRecvaddress;
	private String notifyIssueCcaddress;

	private Boolean notifyBugState;
	private String notifyBugRecvaddress;
	private String notifyBugCcaddress;

	private Boolean notifyScheduleState;
	private String notifyScheduleRecvaddress;
	private String notifyScheduleCcaddress;

	private Boolean notifyTaState;
	private String notifyTaRecvaddress;
	private String notifyTaCcaddress;

	private String notifyIssueRecvaddress4Issue;
	private String notifyBugRecvaddress4Issue;
	private String notifyScheduleRecvaddress4Issue;
	private String notifyTaRecvaddress4Issue;

	public Boolean getNotifyIssueState() {
		return notifyIssueState;
	}

	public void setNotifyIssueState(Boolean notifyIssueState) {
		this.notifyIssueState = notifyIssueState;
	}

	public String getNotifyIssueRecvaddress() {
		return notifyIssueRecvaddress;
	}

	public void setNotifyIssueRecvaddress(String notifyIssueRecvaddress) {
		this.notifyIssueRecvaddress = notifyIssueRecvaddress;
	}

	public String getNotifyIssueCcaddress() {
		return notifyIssueCcaddress;
	}

	public void setNotifyIssueCcaddress(String notifyIssueCcaddress) {
		this.notifyIssueCcaddress = notifyIssueCcaddress;
	}

	public Boolean getNotifyBugState() {
		return notifyBugState;
	}

	public void setNotifyBugState(Boolean notifyBugState) {
		this.notifyBugState = notifyBugState;
	}

	public String getNotifyBugRecvaddress() {
		return notifyBugRecvaddress;
	}

	public void setNotifyBugRecvaddress(String notifyBugRecvaddress) {
		this.notifyBugRecvaddress = notifyBugRecvaddress;
	}

	public String getNotifyBugCcaddress() {
		return notifyBugCcaddress;
	}

	public void setNotifyBugCcaddress(String notifyBugCcaddress) {
		this.notifyBugCcaddress = notifyBugCcaddress;
	}

	public Boolean getNotifyScheduleState() {
		return notifyScheduleState;
	}

	public void setNotifyScheduleState(Boolean notifyScheduleState) {
		this.notifyScheduleState = notifyScheduleState;
	}

	public String getNotifyScheduleRecvaddress() {
		return notifyScheduleRecvaddress;
	}

	public void setNotifyScheduleRecvaddress(String notifyScheduleRecvaddress) {
		this.notifyScheduleRecvaddress = notifyScheduleRecvaddress;
	}

	public String getNotifyScheduleCcaddress() {
		return notifyScheduleCcaddress;
	}

	public void setNotifyScheduleCcaddress(String notifyScheduleCcaddress) {
		this.notifyScheduleCcaddress = notifyScheduleCcaddress;
	}

	public String getNotifyIssueRecvaddress4Issue() {
		return notifyIssueRecvaddress4Issue;
	}

	public void setNotifyIssueRecvaddress4Issue(String notifyIssueRecvaddress4Issue) {
		this.notifyIssueRecvaddress4Issue = notifyIssueRecvaddress4Issue;
	}

	public String getNotifyBugRecvaddress4Issue() {
		return notifyBugRecvaddress4Issue;
	}

	public void setNotifyBugRecvaddress4Issue(String notifyBugRecvaddress4Issue) {
		this.notifyBugRecvaddress4Issue = notifyBugRecvaddress4Issue;
	}

	public String getNotifyScheduleRecvaddress4Issue() {
		return notifyScheduleRecvaddress4Issue;
	}

	public void setNotifyScheduleRecvaddress4Issue(String notifyScheduleRecvaddress4Issue) {
		this.notifyScheduleRecvaddress4Issue = notifyScheduleRecvaddress4Issue;
	}

	public Boolean getNotifyTaState() {
		return notifyTaState;
	}

	public void setNotifyTaState(Boolean notifyTaState) {
		this.notifyTaState = notifyTaState;
	}

	public String getNotifyTaRecvaddress() {
		return notifyTaRecvaddress;
	}

	public void setNotifyTaRecvaddress(String notifyTaRecvaddress) {
		this.notifyTaRecvaddress = notifyTaRecvaddress;
	}

	public String getNotifyTaCcaddress() {
		return notifyTaCcaddress;
	}

	public void setNotifyTaCcaddress(String notifyTaCcaddress) {
		this.notifyTaCcaddress = notifyTaCcaddress;
	}

	public String getNotifyTaRecvaddress4Issue() {
		return notifyTaRecvaddress4Issue;
	}

	public void setNotifyTaRecvaddress4Issue(String notifyTaRecvaddress4Issue) {
		this.notifyTaRecvaddress4Issue = notifyTaRecvaddress4Issue;
	}

}
