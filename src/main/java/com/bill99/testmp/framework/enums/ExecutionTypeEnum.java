package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.Map;

/**
 * 测试方式枚举类
 * 
 * @author leo.feng
 */
public enum ExecutionTypeEnum {
	STATUS1(1), STATUS2(2), STATUS3(3);

	private Integer value;

	ExecutionTypeEnum(Integer status) {
		this.value = status;
	}

	public Integer getValue() {
		return value;
	}

	public final static Map<ExecutionTypeEnum, String> statusMap;

	static {
		statusMap = new EnumMap<ExecutionTypeEnum, String>(ExecutionTypeEnum.class);
		statusMap.put(ExecutionTypeEnum.STATUS1, "人工");
		statusMap.put(ExecutionTypeEnum.STATUS2, "自动");
		statusMap.put(ExecutionTypeEnum.STATUS3, "性能");
	}

	/**
	 * 返回AuditStatusMap对应的描述.
	 */
	public static String getAuditStatusStrValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return ExecutionTypeEnum.statusMap.get(ExecutionTypeEnum.getAuditStatusKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static ExecutionTypeEnum getAuditStatusKey(Integer value) {
		ExecutionTypeEnum tmpKey = null;
		for (ExecutionTypeEnum tmpEnum : ExecutionTypeEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static Integer getAuditKeyByValue(String value) {
		Integer status = ExecutionTypeEnum.STATUS1.value;
		for (ExecutionTypeEnum tmpEnum : ExecutionTypeEnum.values()) {
			if (ExecutionTypeEnum.statusMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
