package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.Map;

/**
 * 用例状态枚举类
 * 
 * @author leo.feng
 */
public enum StatusEnum {
	STATUS0(0), STATUS1(1);

	private Integer value;

	StatusEnum(Integer status) {
		this.value = status;
	}

	public Integer getValue() {
		return value;
	}

	public final static Map<StatusEnum, String> statusMap;

	static {
		statusMap = new EnumMap<StatusEnum, String>(StatusEnum.class);
		statusMap.put(StatusEnum.STATUS0, "无效");
		statusMap.put(StatusEnum.STATUS1, "有效");
	}

	/**
	 * 返回AuditStatusMap对应的描述.
	 */
	public static String getAuditStatusStrValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return StatusEnum.statusMap.get(StatusEnum.getAuditStatusKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static StatusEnum getAuditStatusKey(Integer value) {
		StatusEnum tmpKey = null;
		for (StatusEnum tmpEnum : StatusEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static Integer getAuditKeyByValue(String value) {
		Integer status = StatusEnum.STATUS1.value;
		for (StatusEnum tmpEnum : StatusEnum.values()) {
			if (StatusEnum.statusMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
