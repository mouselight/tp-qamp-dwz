package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum TestPlanStepEnum {

	TEST_PLANN_1(1), TEST_PLANN_2(2), TEST_PLANN_3(3), TEST_PLANN_4(4), TEST_PLANN_5(5), TEST_PLANN_6(6), TEST_PLANN_7(7), TEST_PLANN_8(8), TEST_PLANN_9(9),TEST_PLANN_10(10);

	private Integer value;

	TestPlanStepEnum(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public static final Map<TestPlanStepEnum, String> testPlanStepMap;

	static {
		testPlanStepMap = new EnumMap<TestPlanStepEnum, String>(TestPlanStepEnum.class);
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_1, "测试设计");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_2, "设计评审");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_3, "用例编写");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_4, "用例评审");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_5, "用例执行（开发环境）");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_6, "用例执行（集成环境）");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_7, "测试报告");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_8, "用例执行（生产环境）");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_9, "性能测试");
		testPlanStepMap.put(TestPlanStepEnum.TEST_PLANN_10, "用例执行（验收环境）");
		
	}

	/**
	 * 返回Map
	 * @return
	 */
	public static Map<Integer, String> getTestPlanStepMap() {
		Map testPlanStepMap_ = new HashMap();
		Iterator keyIts = TestPlanStepEnum.testPlanStepMap.keySet().iterator();

		while (keyIts.hasNext()) {
			Integer key = ((TestPlanStepEnum) keyIts.next()).getValue();
			testPlanStepMap_.put(key, getTestPlanStepValue(key));
		}
		return testPlanStepMap_;
	}

	/**
	 * 返回testPlanStepMap对应的描述.
	 */
	public static String getTestPlanStepValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return TestPlanStepEnum.testPlanStepMap.get(TestPlanStepEnum.getTestPlanStepKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static TestPlanStepEnum getTestPlanStepKey(Integer value) {
		TestPlanStepEnum tmpKey = null;
		for (TestPlanStepEnum tmpEnum : TestPlanStepEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 */
	public static Integer getStepKeyByValue(String value) {
		Integer status = null;
		for (TestPlanStepEnum tmpEnum : TestPlanStepEnum.values()) {
			if (TestPlanStepEnum.testPlanStepMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
