package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.Map;

/**
 * 用例等级
 * 
 * @author leo.feng
 */
public enum ImportanceEnum {
	STATUS1(1), STATUS2(2), STATUS3(3);

	private Integer value;

	ImportanceEnum(Integer status) {
		this.value = status;
	}

	public Integer getValue() {
		return value;
	}

	public final static Map<ImportanceEnum, String> statusMap;

	static {
		statusMap = new EnumMap<ImportanceEnum, String>(ImportanceEnum.class);
		statusMap.put(ImportanceEnum.STATUS1, "一般");
		statusMap.put(ImportanceEnum.STATUS2, "重要");
		statusMap.put(ImportanceEnum.STATUS3, "核心");
	}

	/**
	 * 返回AuditStatusMap对应的描述.
	 */
	public static String getAuditStatusStrValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return ImportanceEnum.statusMap.get(ImportanceEnum.getAuditStatusKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static ImportanceEnum getAuditStatusKey(Integer value) {
		ImportanceEnum tmpKey = null;
		for (ImportanceEnum tmpEnum : ImportanceEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 */
	public static Integer getAuditKeyByValue(String value) {
		Integer status = ImportanceEnum.STATUS1.value;
		for (ImportanceEnum tmpEnum : ImportanceEnum.values()) {
			if (ImportanceEnum.statusMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
