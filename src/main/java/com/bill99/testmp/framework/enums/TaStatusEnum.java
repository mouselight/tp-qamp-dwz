package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.Map;

/**
 * 自动化覆盖状态枚举类
 * 
 * @author leo.feng
 */
public enum TaStatusEnum {
	STATUS0(0), STATUS1(1);

	private Integer value;

	TaStatusEnum(Integer status) {
		this.value = status;
	}

	public Integer getValue() {
		return value;
	}

	public final static Map<TaStatusEnum, String> statusMap;

	static {
		statusMap = new EnumMap<TaStatusEnum, String>(TaStatusEnum.class);
		statusMap.put(TaStatusEnum.STATUS0, "未完成");
		statusMap.put(TaStatusEnum.STATUS1, "完成");
	}

	/**
	 * 返回AuditStatusMap对应的描述.
	 */
	public static String getAuditStatusStrValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return TaStatusEnum.statusMap.get(TaStatusEnum.getAuditStatusKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static TaStatusEnum getAuditStatusKey(Integer value) {
		TaStatusEnum tmpKey = null;
		for (TaStatusEnum tmpEnum : TaStatusEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static Integer getAuditKeyByValue(String value) {
		Integer status = TaStatusEnum.STATUS0.value;
		for (TaStatusEnum tmpEnum : TaStatusEnum.values()) {
			if (TaStatusEnum.statusMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
