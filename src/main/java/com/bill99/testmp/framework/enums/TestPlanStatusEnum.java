package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum TestPlanStatusEnum {

	STATUS_0(0), STATUS_1(1), STATUS_2(2), STATUS_3(3);

	private Integer value;

	TestPlanStatusEnum(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static Map<TestPlanStatusEnum, String> testPlanStatusMap;

	static {
		testPlanStatusMap = new EnumMap<TestPlanStatusEnum, String>(TestPlanStatusEnum.class);
		testPlanStatusMap.put(TestPlanStatusEnum.STATUS_0, "未开始");
		testPlanStatusMap.put(TestPlanStatusEnum.STATUS_1, "进行中");
		testPlanStatusMap.put(TestPlanStatusEnum.STATUS_2, "完成");
		testPlanStatusMap.put(TestPlanStatusEnum.STATUS_3, "停止");
	}

	public static Map<String, String> getTestPlanMap() {
		Map<String, String> testPlanMap = new HashMap<String, String>();
		for (Iterator<TestPlanStatusEnum> it = TestPlanStatusEnum.testPlanStatusMap.keySet().iterator(); it.hasNext();) {
			TestPlanStatusEnum nextEnum = it.next();
			testPlanMap.put(nextEnum.getValue().toString(), getTestPlanStatusValue(nextEnum.getValue()));
		}
		return testPlanMap;
	}

	public static String getTestPlanStatusValue(Integer key) {
		TestPlanStatusEnum tmpEnum = getEnumKey(key);
		return TestPlanStatusEnum.testPlanStatusMap.get(tmpEnum);
	}

	public static TestPlanStatusEnum getEnumKey(Integer key) {
		TestPlanStatusEnum tmpEnum = TestPlanStatusEnum.STATUS_0;
		for (Iterator<TestPlanStatusEnum> it = TestPlanStatusEnum.testPlanStatusMap.keySet().iterator(); it.hasNext();) {
			TestPlanStatusEnum nextEnum = it.next();
			if (nextEnum.getValue().compareTo(key) == 0) {
				tmpEnum = nextEnum;
			}
		}
		return tmpEnum;
	}
}
