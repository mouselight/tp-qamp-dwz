package com.bill99.testmp.framework.enums;

import java.util.EnumMap;
import java.util.Map;

public enum PlanStateEnum {

	STATUS_0(0), STATUS_1(1), STATUS_2(2), STATUS_3(3), STATUS_99(99);

	private Integer value;

	PlanStateEnum(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static Map<PlanStateEnum, String> planStateEnumMap;

	static {
		planStateEnumMap = new EnumMap<PlanStateEnum, String>(PlanStateEnum.class);
		planStateEnumMap.put(PlanStateEnum.STATUS_0, "未开始");
		planStateEnumMap.put(PlanStateEnum.STATUS_1, "进行中");
		planStateEnumMap.put(PlanStateEnum.STATUS_2, "完成");
		planStateEnumMap.put(PlanStateEnum.STATUS_3, "停止");
		planStateEnumMap.put(PlanStateEnum.STATUS_99, "无效");
	}

	/**
	 * 返回AuditStatusMap对应的描述.
	 */
	public static String getPlanStateStrValue(final Integer value) {
		if (value == null) {
			return "";
		}
		return PlanStateEnum.planStateEnumMap.get(PlanStateEnum.getPlanStateKey(value));
	}

	/**
	 * 跟据value返回枚举对应的key
	 */
	public static PlanStateEnum getPlanStateKey(Integer value) {
		PlanStateEnum tmpKey = null;
		for (PlanStateEnum tmpEnum : PlanStateEnum.values()) {
			if (tmpEnum.value.intValue() == value.intValue()) {
				tmpKey = tmpEnum;
				break;
			}
		}
		return tmpKey;
	}

	/**
	 * 跟据value返回key
	 * 
	 * @param args
	 */
	public static Integer getPlanStateKeyByValue(String value) {
		Integer status = PlanStateEnum.STATUS_0.value;
		for (PlanStateEnum tmpEnum : PlanStateEnum.values()) {
			if (PlanStateEnum.planStateEnumMap.get(tmpEnum).equals(value)) {
				status = tmpEnum.value;
				break;
			}
		}
		return status;
	}
}
