package com.bill99.testmp.framework.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.entity.Module;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

@Controller
public class FrameController {

	@Autowired
	private ModuleMng moduleMng;
	@Autowired
	private TestProjectsMng testProjectsMng;

	// 首页
	@RequestMapping(value = "/index.htm")
	public String index(HttpServletRequest request, ModelMap model, HttpSession httpSession) {
		model.addAttribute("bannerList", moduleMng.getBannerModules(RiaFrameworkUtils.getUserId(request)));
		initTestProject(httpSession);
		return "framwork/index";
	}

	// 导航
	@RequestMapping("/navigate.htm")
	public String navigate(Long idModule, HttpServletRequest request, ModelMap model) {
		Map<String, Long> userModelsMap = new HashMap<String, Long>();
		for (Module module : moduleMng.getModulesByUser(RiaFrameworkUtils.getUserId(request))) {
			userModelsMap.put(module.getIdModule().toString(), module.getIdModule());
		}
		model.addAttribute("userModelsMap", userModelsMap);

		// 所有存在menu类型子节点的Model
		Map<String, Long> menuModelsMap = new HashMap<String, Long>();
		for (Module module : moduleMng.getVldModules()) {
			if (hasMenuChilds(module)) {
				menuModelsMap.put(module.getIdModule().toString(), module.getIdModule());
			}
		}
		model.addAttribute("module", moduleMng.getModule(idModule));
		model.addAttribute("menuModelsMap", menuModelsMap);
		model.addAttribute("navigateList", moduleMng.getChild(idModule));
		return "framwork/menu/navigate";
	}

	// 当前节点是否包含menu类型的子节点
	private boolean hasMenuChilds(Module module) {
		boolean flag = false;
		if (module != null && module.getChild() != null) {
			for (Module item : module.getChild()) {
				if (item == null) {
					continue;
				}
				if (item.getIsMenu()) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	@RequestMapping(value = "/testProjectChange.htm")
	public String change(HttpServletRequest request, HttpServletResponse response, ModelMap model, Long testProjectId, HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("user");
		user.setCurrentTestProject(testProjectsMng.getById(testProjectId));
		httpSession.setAttribute("user", user);
		model.addAttribute("bannerList", moduleMng.getBannerModules(RiaFrameworkUtils.getUserId(request)));
		return "framwork/index";
	}

	private void initTestProject(HttpSession httpSession) {
		User user = RiaFrameworkUtils.getUseFromSession(httpSession);

		if (user.getCurrentTestProject() == null && user.getTestprojects() != null && user.getTestprojects().size() > 0) {
			for (TestProjects testProjects : user.getTestprojects()) {
				user.setCurrentTestProject(testProjectsMng.getById(testProjects.getId()));
				continue;
			}
		}
		httpSession.setAttribute("user", user);
	}
}
