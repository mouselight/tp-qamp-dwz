package com.bill99.testmp.framework.web.controller.module;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.constants.OperateConstants;
import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.Module;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.riaframework.orm.manager.ModuleResourceMng;
import com.bill99.rmca.common.util.Bill99Logger;

@Controller
public class ModuleController {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private ModuleMng moduleMng;
	@Autowired
	private ModuleResourceMng moduleResourceMng;

	// root列表
	@RequestMapping("/framwork/module/query_module.htm")
	public String query_module(ModelMap model) {
		model.addAttribute("rootList", moduleMng.getRoots());
		model.addAttribute("cmd", new ModuleCmd());
		return "framwork/module/list_module";
	}

	// child列表
	@RequestMapping("/framwork/module/list_module_child.htm")
	public String list_module_child(ModuleCmd moduleCmd, ModelMap model) {
		model.addAttribute("rootList", new ArrayList<Module>(moduleMng.getModule(moduleCmd.getParentId()).getChild()));
		model.addAttribute("cmd", moduleCmd);
		return "framwork/module/list_module_root";
	}

	// 添加
	@RequestMapping("/framwork/module/add_module.htm")
	public String add_module(ModelMap model) {
		model.addAttribute("rootList", moduleMng.getRoots()); // 菜单树
		return "framwork/module/add_module";
	}

	// 保存
	@RequestMapping("/framwork/module/save_module.htm")
	public void save_module(Module module, ModuleCmd moduleCmd, HttpServletResponse response, HttpServletRequest request) {
		module.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
		if (moduleCmd.getIdModuleParent() != null) {
			module.setParentId(moduleCmd.getIdModuleParent());
		}
		moduleMng.save(module);
		logger.info("save Module id=" + module.getIdModule());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "添加成功", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	// 编辑
	@RequestMapping("/framwork/module/edit_module.htm")
	public String edit_module(ModuleCmd moduleCmd, ModelMap model) {
		model.addAttribute("rootList", moduleMng.getRoots()); // 菜单树
		model.addAttribute("cmd", moduleCmd);
		model.addAttribute("itemModule", moduleMng.getModule(moduleCmd.getIdModule()));
		return "framwork/module/edit_module";
	}

	private final String[] moduleExp = { "idModule", "createDate", "createUser", "moduleResources"};

	// 更新
	@RequestMapping("/framwork/module/update_module.htm")
	public void update_module(Module module, ModuleCmd moduleCmd, HttpServletResponse response, HttpServletRequest request) {
		Module moduleBean = moduleMng.getModule(module.getIdModule());
		module.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
		module.setUpdateDate(DateUtil.getTimeNow());
		BeanUtils.copyProperties(module, moduleBean, moduleExp);
		if (moduleCmd.getIdModuleParent() != null) {
			moduleBean.setParentId(moduleCmd.getIdModuleParent());
			// moduleBean.setParent(moduleMng.getModule(moduleCmd.getIdModuleParent()));
		}
		// else {
		// moduleBean.setParent(new Module());
		// }
		moduleMng.update(moduleBean);
		logger.info("update Module id=" + module.getIdModule());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", null, null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	// 关联资源分页查询
	@RequestMapping("/framwork/module/query_asso_module_resource.htm")
	public String query_asso_module_resource(ModuleCmd moduleCmd, ModelMap model) {
		
		ModuleResourceQueryDto moduleResourceQueryDto = new ModuleResourceQueryDto();
		BeanUtils.copyProperties(moduleCmd, moduleResourceQueryDto);
		
		//model.addAttribute("rootList", moduleMng.getRoots()); // 菜单树
		model.addAttribute("cmd", moduleCmd);
		if (moduleCmd.getIdModule() != null) {
			model.addAttribute("itemModule", moduleMng.getModule(moduleCmd.getIdModule()));
		} else {
			model.addAttribute("itemModule", new Module());
		}
		model.addAttribute("pagination", moduleResourceMng.findUnboundModuleResources(moduleResourceQueryDto, moduleCmd.getPageNum(), moduleCmd.getNumPerPage()));
		return "framwork/module/list_asso_module_resource";
	}

	// 保存关联资源
	@RequestMapping("/framwork/module/save_asso_module_resource.htm")
	public void save_asso_module_resource(ModuleCmd moduleCmd, HttpServletResponse response) {
		Module moduleBean = moduleMng.getModule(moduleCmd.getIdModule());
		
		Set<ModuleResource> moduleResources = moduleBean.getModuleResources();
		if(moduleResources == null){
			moduleResources = new HashSet<ModuleResource>();
		}
		if (StringUtils.hasLength(moduleCmd.getIdModuleResources())) {
			for (Long idModuleResource : StringUtil.string2LongArray(moduleCmd.getIdModuleResources())) {
				moduleResources.add(moduleResourceMng.getModuleResource(idModuleResource));
			}
		}
		moduleBean.setModuleResources(moduleResources);
		moduleMng.update(moduleBean);
		logger.info("save_asso_module_resource Module id=" + moduleBean.getIdModule());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_SUCCESS, OperateConstants.MSG_OPT_SUCCESS, 
				"relation_module_resource", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}
	
	//移除关联资源
	@RequestMapping("/framwork/module/remove_asso_module_resource.htm")
	public void remove_asso_module_resource(ModuleCmd moduleCmd, HttpServletResponse response) {
		Module moduleBean = moduleMng.getModule(moduleCmd.getIdModule());
		Set<ModuleResource> moduleResources = moduleBean.getModuleResources();
		if(moduleResources!=null && moduleResources.size() >0){
			if (moduleCmd.getIdModuleResource() != null) {
				for (ModuleResource obj : moduleResources) {
					if(obj==null){
						continue;
					}
					if(moduleCmd.getIdModuleResource().longValue() == obj.getIdModuleResource().longValue()){
						moduleResources.remove(obj);
						break;
					}
				}
			}
		}
		moduleBean.setModuleResources(moduleResources);
		moduleMng.update(moduleBean);
		logger.info("remove_asso_module_resource Module id=" + moduleBean.getIdModule());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_SUCCESS, OperateConstants.MSG_OPT_SUCCESS, 
				"relation_module_resource", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
