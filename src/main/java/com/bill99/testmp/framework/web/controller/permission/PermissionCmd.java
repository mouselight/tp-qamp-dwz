/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
*/

package com.bill99.testmp.framework.web.controller.permission;

import com.bill99.riaframework.common.command.PageCmd;

/**
 * @author leofung
 *
 */
public class PermissionCmd extends PageCmd {

	private static final long serialVersionUID = -191363031856464083L;

	private Long idModule;

	private Long idModuleResource;
	private String name;
	private String uri;
	private String moduleType;
	private Integer optType;
	private Boolean status;
	private String idModuleResources;
	private String idModules;

	private Long idRole;

	private Boolean isSuper;
	private Boolean isWarning;

	private Long parentId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public Integer getOptType() {
		return optType;
	}

	public void setOptType(Integer optType) {
		this.optType = optType;
	}

	public Long getIdModuleResource() {
		return idModuleResource;
	}

	public void setIdModuleResource(Long idModuleResource) {
		this.idModuleResource = idModuleResource;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	public String getIdModuleResources() {
		return idModuleResources;
	}

	public void setIdModuleResources(String idModuleResources) {
		this.idModuleResources = idModuleResources;
	}

	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public String getIdModules() {
		return idModules;
	}

	public void setIdModules(String idModules) {
		this.idModules = idModules;
	}

	public Boolean getIsSuper() {
		return isSuper;
	}

	public void setIsSuper(Boolean isSuper) {
		this.isSuper = isSuper;
	}

	public Boolean getIsWarning() {
		return isWarning;
	}

	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

}
