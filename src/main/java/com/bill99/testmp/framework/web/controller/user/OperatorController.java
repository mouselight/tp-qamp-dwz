package com.bill99.testmp.framework.web.controller.user;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.constants.OperateConstants;
import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.common.dto.UserExtDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.RequestUtils;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.domain.permission.WebErrors;
import com.bill99.riaframework.domain.security.encoder.PwdEncoder;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.entity.UserExt;
import com.bill99.riaframework.orm.manager.RoleService;
import com.bill99.riaframework.orm.manager.UserService;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;
import com.jeecms.common.page.Pagination;

@Controller
public class OperatorController {

	private Bill99Logger logger = Bill99Logger.getLogger(OperatorController.class);

	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;
	@Autowired
	private PwdEncoder pwdEncoder;
	@Autowired
	private TestProjectsMng testProjectsMng;

	// ///////////////////////////////////////////////////////操作员管理///////////////////////////////////////////////
	@RequestMapping("/framwork/operator/list_operator.htm")
	public String list(ModelMap model, UserCmd userCmd) {
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userCmd, userDto);

		Pagination pagination = userService.getPage(userDto, userCmd.getPageNum(), userCmd.getNumPerPage());
		model.addAttribute("pagination", pagination);
		model.addAttribute("cmd", userCmd);
		return "framwork/operator/list_operator";
	}

	@RequestMapping("/framwork/operator/add_operator.htm")
	public String add(ModelMap model, HttpServletRequest request, UserCmd cond) {
		// Map<String, String> roleMap = roleService.getRoleMap();
		// Init NameMap
		model.addAttribute("cond", cond);
		// model.addAttribute("roleMap", roleMap);
		model.addAttribute("validRoleList", roleService.getValidRole());
		model.addAttribute("validProjectList", testProjectsMng.findValidList());

		model.addAttribute("checkedRoleMap", new HashMap<String, Long>());
		model.addAttribute("user", RiaFrameworkUtils.getUser(request));
		return "framwork/operator/add_operator";
	}

	@RequestMapping("/framwork/operator/edit_operator.htm")
	public String edit(UserCmd cond, HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateEdit(cond.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		User user = userService.findById(cond.getId());

		// Init mapName
		model.addAttribute("user", user);
		// model.addAttribute("roleMap", roleService.getRoleMap());
		model.addAttribute("validRoleList", roleService.getValidRole());
		model.addAttribute("validProjectList", testProjectsMng.findValidList());
		model.addAttribute("checkedRoleMap", roleService.getCheckedRole(cond.getId()));
		// model.addAttribute("roleIds", user.getRoleIds());
		return "framwork/operator/edit_operator";
	}

	/* 内页操作 */
	@RequestMapping("/framwork/operator/save_operator.htm")
	public void save(HttpServletResponse response, User bean, UserExt ext, Long[] roleIds, Long[] projectIds, HttpServletRequest request, ModelMap model) {
		bean.setPassword(pwdEncoder.encodePassword(bean.getPassword()));
		bean.setRegisterIp(RequestUtils.getIpAddr(request));
		ext.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
		try {
			if (StringUtils.hasLength(bean.getUsername()) && !userService.usernameNotExist(bean.getUsername())) {
				AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("300", "用户名已存在", null, null, null, null);
				ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
				return;
			}

			bean = userService.registerUser(bean, ext, roleIds, projectIds);
			logger.info("save User id={}" + bean.getId());
			model.addAttribute("message", "global.success.save");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("OperatorUserAct.save Occur Exception", e);
			model.addAttribute("message", "global.failed.save");
		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "添加用户成功", "list_operator", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/framwork/operator/update_operator.htm")
	public void update(HttpServletResponse response, UserCmd cond, HttpServletRequest request, ModelMap model) {

		String userName = RiaFrameworkUtils.getUser(request).getUsername();
		// Prepare Cond
		UserDto userDto = new UserDto();
		userDto.setId(cond.getId());// PK
		if (StringUtils.hasLength(cond.getPassword())) {
			userDto.setPassword(pwdEncoder.encodePassword(cond.getPassword()));
		}
		userDto.setEmail(cond.getEmail());
		userDto.setIsDisabled(cond.getIsDisabled());
		userDto.setRoleIds(cond.getRoleIds());
		userDto.setEntryMode(cond.getEntryMode());

		// Ext
		UserExtDto userExtDto = new UserExtDto();
		userExtDto.setBirthday(DateUtil.getDateFromStrings(cond.getBirthday()));
		userExtDto.setGender(cond.getGender());
		userExtDto.setMoble(cond.getMoble());
		userExtDto.setMsn(cond.getMsn());
		userExtDto.setPhone(cond.getPhone());
		userExtDto.setQq(cond.getQq());
		userExtDto.setRealname(cond.getRealname());
		userExtDto.setWeddingday(DateUtil.getDateFromStrings(cond.getWeddingday()));
		userExtDto.setComefrom(cond.getComefrom());
		userExtDto.setIntro(cond.getIntro());
		userExtDto.setUpdateUser(userName);
		userExtDto.setUpdateDate(new Date());
		userDto.setUserExtDto(userExtDto);
		userDto.setProjectIds(cond.getProjectIds());

		try {
			userService.update(userDto);
			logger.info("<-- Update UserInfo Success & optUser = " + userName + "-->");
			model.addAttribute("message", "global.success.update");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("OperatorUserAct.update Occur Exception", e);
			model.addAttribute("message", "global.failed.update");
		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改用户成功", "list_operator", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/framwork/operator/delete_operator.htm")
	public void delete(HttpServletResponse response, UserCmd cond, HttpServletRequest request) {
		User user = userService.findById(cond.getId());
		AjaxDoneUtil ajaxDoneUtil = null;

		if (RiaFrameworkUtils.getUser(request).getId().longValue() == cond.getId().longValue()) {
			ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_FAILED, "不能删除当前登录用户", "list_operator", null, null, null);
		} else if ("admin".equals(user.getUsername())) {
			ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_FAILED, "不能删除admin用户", "list_operator", null, null, null);
		} else { // if (RiaFrameworkUtils.getUser(request).getId() != cond.getId() && !"admin".equals(user.getUsername()))
			User userObj = userService.deleteById(cond.getId());
			if (userObj != null) {
				logger.info("delete--" + userObj.getUsername() + " success!! &currentUser--" + RiaFrameworkUtils.getUser(request).getUsername());
				ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_SUCCESS, OperateConstants.MSG_DEL_SUCCESS, "list_operator", null, null, null);
			} else {
				ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_FAILED, "用户不存在或已被删除!", "list_operator", null, null, null);
			}
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/framwork/operator/check_operatorname.htm")
	public void checkUsername(String username, HttpServletResponse response) {
		String pass;
		if (!StringUtils.hasLength(username)) {
			pass = "false";
		} else {
			pass = userService.usernameNotExist(username) ? "true" : "false";
		}
		ResponseUtils.renderJson(response, pass);
	}

	private WebErrors validateSave(User bean, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		return errors;
	}

	private WebErrors validateEdit(Long id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Long id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		// 验证是否为管理员，管理员不允许修改。
		return errors;
	}

	private WebErrors validateDelete(Long[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Long id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Long id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		User entity = userService.findById(id);
		if (errors.ifNotExist(entity, User.class, id)) {
			return true;
		}
		return false;
	}

}
