/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-8-27		    leofung	   		leofung
 */

package com.bill99.testmp.framework.web.controller.module;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.dto.ModuleResourceQueryDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.Module;
import com.bill99.riaframework.orm.entity.ModuleResource;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.riaframework.orm.manager.ModuleResourceMng;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.rmca.common.util.DateUtil;

/**
 * @author leofung
 * 
 */
@Controller
public class ModuleResourceController {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());
	@Autowired
	private ModuleMng moduleMng;
	@Autowired
	private ModuleResourceMng moduleResourceMng;

	// 资源分页查询
	@RequestMapping("/framwork/module/query_module_resource.htm")
	public String query_module_resource(ModuleCmd moduleCmd, ModelMap model) {
		ModuleResourceQueryDto moduleResourceQueryDto = new ModuleResourceQueryDto();
		BeanUtils.copyProperties(moduleCmd, moduleResourceQueryDto);

		model.addAttribute("rootList", moduleMng.getRoots()); // 菜单树
		model.addAttribute("cmd", moduleCmd);
		model.addAttribute("pagination", moduleResourceMng.findAllModuleResources(moduleResourceQueryDto, moduleCmd.getPageNum(), moduleCmd.getNumPerPage()));
		return "framwork/module/list_module_resource";
	}

	// 添加资源
	@RequestMapping("/framwork/module/add_module_resource.htm")
	public String add_module_resource(ModelMap model) {
		model.addAttribute("vldRootList", moduleMng.getVldRoots());
		return "framwork/module/add_module_resource";
	}

	// 保存资源
	@RequestMapping("/framwork/module/save_module_resource.htm")
	public void save_module_resource(ModuleCmd moduleCmd, ModuleResource moduleResource, HttpServletResponse response, HttpServletRequest request) {
		Set<Module> modules = new HashSet<Module>();
		if (StringUtils.hasLength(moduleCmd.getIdModules())) {
			for (Long idModule : StringUtil.string2LongArray(moduleCmd.getIdModules())) {
				modules.add(moduleMng.getModule(idModule));
			}
		}
		moduleResource.setModules(modules);
		moduleResource.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
		moduleResourceMng.save(moduleResource);
		logger.info("save ModuleResource id=" + moduleResource.getIdModuleResource());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "添加成功", "list_module_resource", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	// 编辑资源
	@RequestMapping("/framwork/module/edit_module_resource.htm")
	public String edit_module_resource(ModuleCmd moduleCmd, ModelMap model) {
		model.addAttribute("item", moduleResourceMng.getModuleResource(moduleCmd.getIdModuleResource()));
		model.addAttribute("vldRootList", moduleMng.getVldRoots());
		return "framwork/module/edit_module_resource";
	}

	private final String[] moduleResourceExp = { "idModuleResource", "createDate", "createUser", "modules" };

	// 更新资源
	@RequestMapping("/framwork/module/update_module_resource.htm")
	public void update_module_resource(ModuleCmd moduleCmd, ModuleResource moduleResource, HttpServletResponse response, HttpServletRequest request) {
		Set<Module> modules = new HashSet<Module>();
		if (StringUtils.hasLength(moduleCmd.getIdModules())) {
			for (Long idModule : StringUtil.string2LongArray(moduleCmd.getIdModules())) {
				modules.add(moduleMng.getModule(idModule));
			}
		}
		moduleResource.setModules(modules);
		moduleResource.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
		moduleResource.setUpdateDate(DateUtil.getTimeNow());
		ModuleResource moduleResourceBean = moduleResourceMng.getModuleResource(moduleResource.getIdModuleResource());
		BeanUtils.copyProperties(moduleResource, moduleResourceBean, moduleResourceExp);
		moduleResourceMng.update(moduleResourceBean);
		logger.info("update ModuleResource id=" + moduleResource.getIdModuleResource());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_module_resource", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
