package com.bill99.testmp.framework.web.controller.permission;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.constants.OperateConstants;
import com.bill99.riaframework.common.dto.RoleQueryDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.common.utils.StringUtil;
import com.bill99.riaframework.orm.entity.Role;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.riaframework.orm.manager.RoleService;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.rmca.common.util.DateUtil;
import com.bill99.testmp.framework.domain.CallbackBuildService;
import com.jeecms.common.page.Pagination;

@Controller
public class RoleController {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private RoleService roleService;
	@Autowired
	private ModuleMng moduleMng;
	@Autowired
	private CallbackBuildService callbackBuildService;

	// ///////////////////////////////////////////////////////角色管理///////////////////////////////////////////////
	@RequestMapping("/framwork/permission/list_role.htm")
	public String listRole(PermissionCmd permissionCmd, ModelMap model) {
		RoleQueryDto roleQueryDto = new RoleQueryDto();
		BeanUtils.copyProperties(permissionCmd, roleQueryDto);
		Pagination pagination = roleService.getRoleList(roleQueryDto, permissionCmd.getPageNum(), permissionCmd.getNumPerPage());
		model.addAttribute("cmd", permissionCmd);
		model.addAttribute("pagination", pagination);
		return "framwork/permission/list_role";
	}

	@RequestMapping("/framwork/permission/add_role.htm")
	public String addRole(ModelMap model) {
		model.addAttribute("vldRootList", moduleMng.getVldRoots());
		return "framwork/permission/add_role";
	}

	@RequestMapping("/framwork/permission/edit_role.htm")
	public String editRole(PermissionCmd permissionCmd, ModelMap model) {
		model.addAttribute("vldRootList", moduleMng.getVldRoots());
		model.addAttribute("item", roleService.findById(permissionCmd.getIdRole()));
		return "framwork/permission/edit_role";
	}

	/* 内页操作 */
	@RequestMapping("/framwork/permission/save_role.htm")
	public void saveRole(HttpServletRequest request, PermissionCmd permissionCmd, HttpServletResponse response, Role bean) {
		if (StringUtils.hasLength(permissionCmd.getIdModules()) && (permissionCmd.getIsSuper() == null || !permissionCmd.getIsSuper())) {
			Long[] idModules = StringUtil.string2LongArray(permissionCmd.getIdModules());
			bean.setModules(callbackBuildService.getRoleModuleCallback(idModules));
		}
		bean.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
		roleService.save(bean);
		logger.info("batch_touch_module_ Module id=" + permissionCmd.getIdModule());
		logger.info("save Role id={}" + bean.getId());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "添加成功", "list_role", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/framwork/permission/update_role.htm")
	public void updateRole(PermissionCmd permissionCmd, HttpServletResponse response, HttpServletRequest request) {
		Role role = roleService.findById(permissionCmd.getIdRole());
		if (StringUtils.hasLength(permissionCmd.getIdModules()) && (permissionCmd.getIsSuper() == null || !permissionCmd.getIsSuper())) {
			Long[] idModules = StringUtil.string2LongArray(permissionCmd.getIdModules());
			role.setModules(callbackBuildService.getRoleModuleCallback(idModules));
		} else {
			//修改时没有勾选任何权限则清空
			role.setModules(null);
		}
		role.setName(permissionCmd.getName());
		role.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
		role.setUpdateDate(DateUtil.getTimeNow());
		role.setIsSuper(permissionCmd.getIsSuper());
		role.setIsWarning(permissionCmd.getIsWarning());
		role.setStatus(permissionCmd.getStatus());
		roleService.update(role);
		logger.info("update Role id={}." + permissionCmd.getIdRole());
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "list_role", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/framwork/permission/delete_role.htm")
	public void deleteRole(HttpServletResponse response, Long id) {
		AjaxDoneUtil ajaxDoneUtil = null;
		Role bean = roleService.deleteById(id);
		if (bean != null) {
			logger.info("delete Role id={}" + bean.getId());
			ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_SUCCESS, OperateConstants.MSG_DEL_SUCCESS, "list_role", null, null, null);
		} else {
			ajaxDoneUtil = new AjaxDoneUtil(OperateConstants.STATUS_CODE_FAILED, "角色不存在或已被删除!", "list_role", null, null, null);
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
