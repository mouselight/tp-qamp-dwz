package com.bill99.testmp.framework.web.controller;

import java.math.BigDecimal;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bill99.testmp.framework.vo.Application;
import com.bill99.testmp.framework.vo.Apply;
import com.bill99.testmp.framework.vo.Category;
import com.bill99.testmp.framework.vo.Chart;
import com.bill99.testmp.framework.vo.Color;
import com.bill99.testmp.framework.vo.ColorRange;
import com.bill99.testmp.framework.vo.Dataset;
import com.bill99.testmp.framework.vo.Definition;
import com.bill99.testmp.framework.vo.Dial;
import com.bill99.testmp.framework.vo.Dials;
import com.bill99.testmp.framework.vo.Set;
import com.bill99.testmp.framework.vo.Style;
import com.bill99.testmp.framework.vo.Styles;
import com.thoughtworks.xstream.XStream;

public class MonitorParent {

	// protected RmService rmService = ServiceLocatorFactory.getRmService();

	protected String convertXML(String src) {
		// replace all the double quotation to the single quotation
		// otherwise fusioncharts cannot display it correctly
		Pattern p = Pattern.compile("\"");
		Matcher m = p.matcher(src);
		String tmp = m.replaceAll("'");
		// remove all the whitespaces between '>' and '<'
		// otherwise fusioncharts cannot display it correctly
		p = Pattern.compile(">\\s+<");
		m = p.matcher(tmp);
		return m.replaceAll("><");
	}

	protected String chart2XML(Chart chart) {
		XStream xstream = new XStream();
		// Chart
		xstream.alias("chart", Chart.class);
		xstream.addImplicitCollection(Chart.class, "dataset");
		xstream.addImplicitCollection(Chart.class, "set");
		xstream.useAttributeFor(Chart.class, "colorRange");
		xstream.useAttributeFor(Chart.class, "dials");
		xstream.useAttributeFor(Chart.class, "styles");

		xstream.useAttributeFor(Chart.class, "subcaption");
		xstream.useAttributeFor(Chart.class, "font");
		xstream.useAttributeFor(Chart.class, "palette");
		xstream.useAttributeFor(Chart.class, "labelDisplay");
		xstream.useAttributeFor(Chart.class, "slantLabels");
		xstream.useAttributeFor(Chart.class, "areaOverColumns");
		xstream.useAttributeFor(Chart.class, "useRoundEdges");
		xstream.useAttributeFor(Chart.class, "shownames");
		xstream.useAttributeFor(Chart.class, "decimals");
		xstream.useAttributeFor(Chart.class, "legendBorderAlpha");
		xstream.useAttributeFor(Chart.class, "divLineIsDashed");
		xstream.useAttributeFor(Chart.class, "labelStep");
		xstream.useAttributeFor(Chart.class, "numvdivlines");
		xstream.useAttributeFor(Chart.class, "chartRightMargin");
		xstream.useAttributeFor(Chart.class, "bgAngle");
		xstream.useAttributeFor(Chart.class, "bgAlpha");
		xstream.useAttributeFor(Chart.class, "lineColor");
		// 仪表
		xstream.useAttributeFor(Chart.class, "fillAngle");
		xstream.useAttributeFor(Chart.class, "upperLimit");
		xstream.useAttributeFor(Chart.class, "lowerLimit");
		xstream.useAttributeFor(Chart.class, "majorTMNumber");
		xstream.useAttributeFor(Chart.class, "majorTMHeight");
		xstream.useAttributeFor(Chart.class, "showGaugeBorder");
		xstream.useAttributeFor(Chart.class, "gaugeOuterRadius");
		xstream.useAttributeFor(Chart.class, "gaugeOriginX");
		xstream.useAttributeFor(Chart.class, "gaugeOriginY");
		xstream.useAttributeFor(Chart.class, "gaugeInnerRadius");
		xstream.useAttributeFor(Chart.class, "displayValueDistance");
		xstream.useAttributeFor(Chart.class, "tickMarkDecimalPrecision");
		xstream.useAttributeFor(Chart.class, "pivotBorderThickness");
		xstream.useAttributeFor(Chart.class, "pivotFillMix");
		xstream.useAttributeFor(Chart.class, "pivotRadius");
		xstream.useAttributeFor(Chart.class, "showPivotBorder");
		xstream.useAttributeFor(Chart.class, "pivotBorderColor");
		xstream.useAttributeFor(Chart.class, "numVisiblePlot");
		xstream.useAttributeFor(Chart.class, "drawAnchors");
		xstream.useAttributeFor(Chart.class, "anchorSides");
		xstream.useAttributeFor(Chart.class, "showAnchors");
		xstream.useAttributeFor(Chart.class, "bgSWF");
		xstream.useAttributeFor(Chart.class, "rotateYAxisName");
		xstream.useAttributeFor(Chart.class, "showDivLineSecondaryValue");
		xstream.useAttributeFor(Chart.class, "yAxisValuesStep");
		xstream.useAttributeFor(Chart.class, "showYAxisValues");
		xstream.useAttributeFor(Chart.class, "showSYAxisValues");
		xstream.useAttributeFor(Chart.class, "PYAxisMaxValue");
		 
		

		// 功能特性
		xstream.useAttributeFor(Chart.class, "animation");// 是否动画显示数据，默认为1(True)
		xstream.useAttributeFor(Chart.class, "showNames");// 是否显示横向坐标轴(x轴)标签名称
		xstream.useAttributeFor(Chart.class, "rotateNames");// 是否旋转显示标签，默认为0(False):横向显示
		xstream.useAttributeFor(Chart.class, "showValues");// 是否在图表显示对应的数据值，默认为1(True)
		xstream.useAttributeFor(Chart.class, "yAxisMinValue");// 指定纵轴(y轴)最小值，数字
		xstream.useAttributeFor(Chart.class, "yAxisMaxValue");// 指定纵轴(y轴)最小值，数字
		xstream.useAttributeFor(Chart.class, "showLimits");// 是否显示图表限值(y轴最大、最小值)，默认为1(True)

		xstream.useAttributeFor(Chart.class, "xAxisMinValue");// 指定纵轴(X轴)最小值，数字
		xstream.useAttributeFor(Chart.class, "xAxisMaxValue");// 指定纵轴(X轴)最小值，数字

		// 图表标题和轴名称
		xstream.useAttributeFor(Chart.class, "caption");// 图表主标题
		xstream.useAttributeFor(Chart.class, "subCaption");// 图表副标题
		xstream.useAttributeFor(Chart.class, "xAxisName");// 横向坐标轴(x轴)名称
		xstream.useAttributeFor(Chart.class, "yAxisName");// 纵向坐标轴(y轴)名称

		// 图表和画布的样式
		xstream.useAttributeFor(Chart.class, "bgColor");// 图表背景色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "canvasBgColor");// 画布背景色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "canvasBgAlpha");// 画布透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "canvasBorderColor");// 画布边框颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "canvasBorderThickness");// 画布边框厚度，[0-100]
		xstream.useAttributeFor(Chart.class, "shadowAlpha");// 投影透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "showLegend");// 是否显示系列名，默认为1(True)

		// 字体属性
		xstream.useAttributeFor(Chart.class, "baseFont");// 图表字体样式
		xstream.useAttributeFor(Chart.class, "baseFontSize");// 图表字体大小
		xstream.useAttributeFor(Chart.class, "baseFontColor");// 图表字体颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "outCnvBaseFont");// 图表画布以外的字体样式
		xstream.useAttributeFor(Chart.class, "outCnvBaseFontSize");// 图表画布以外的字体大小
		xstream.useAttributeFor(Chart.class, "outCnvBaseFontColor");// 图表画布以外的字体颜色，6位16进制颜色值

		// 分区线和网格
		xstream.useAttributeFor(Chart.class, "numDivLines");// 画布内部水平分区线条数，数字
		xstream.useAttributeFor(Chart.class, "divLineColor");// 水平分区线颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "divLineThickness");// 水平分区线厚度，[1-5]
		xstream.useAttributeFor(Chart.class, "divLineAlpha");// 水平分区线透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "showAlternateHGridColor");// 是否在横向网格带交替的颜色，默认为0(False)
		xstream.useAttributeFor(Chart.class, "alternateHGridColor");// 横向网格带交替的颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "alternateHGridAlpha");// 横向网格带的透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "showDivLinues");// 是否显示Div行的值，默认？？
		xstream.useAttributeFor(Chart.class, "numVDivLines");// 画布内部垂直分区线条数，数字
		xstream.useAttributeFor(Chart.class, "vDivLineColor");// 垂直分区线颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "vDivLineThickness");// 垂直分区线厚度，[1-5]
		xstream.useAttributeFor(Chart.class, "vDivLineAlpha");// 垂直分区线透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "showAlternateVGridColor");// 是否在纵向网格带交替的颜色，默认为0(False)
		xstream.useAttributeFor(Chart.class, "alternateVGridColor");// 纵向网格带交替的颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "alternateVGridAlpha");// 纵向网格带的透明度，[0-100]

		// 数字格式
		xstream.useAttributeFor(Chart.class, "numberPrefix");// 增加数字前缀
		xstream.useAttributeFor(Chart.class, "numberSuffix");// 增加数字后缀 % 为 '%25'
		xstream.useAttributeFor(Chart.class, "formatNumberScale");// 是否格式化数字,默认为1(True),自动的给你的数字加上K（千）或M（百万）；若取0,则不加K或M
		xstream.useAttributeFor(Chart.class, "decimalPrecision");// 指定小数位的位数，[0-10]
																	// 例如：='0'
																	// 取整
		xstream.useAttributeFor(Chart.class, "divLineDecimalPrecision");// 指定水平分区线的值小数位的位数，[0-10]
		xstream.useAttributeFor(Chart.class, "limitsDecimalPrecision");// 指定y轴最大、最小值的小数位的位数，[0-10]
		xstream.useAttributeFor(Chart.class, "formatNumber");// 逗号来分隔数字(千位，百万位),默认为1(True)；若取0,则不加分隔符
		xstream.useAttributeFor(Chart.class, "decimalSeparator");// 指定小数分隔符,默认为'.'
		xstream.useAttributeFor(Chart.class, "thousandSeparator");// 指定千分位分隔符,默认为','

		// Tool-tip/Hover标题
		xstream.useAttributeFor(Chart.class, "showhovercap");// 是否显示悬停说明框，默认为1(True)
		xstream.useAttributeFor(Chart.class, "hoverCapBgColor");// 悬停说明框背景色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "hoverCapBorderColor");// 悬停说明框边框颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "hoverCapSepChar");// 指定悬停说明框内值与值之间分隔符,默认为','

		// 折线图的参数
		xstream.useAttributeFor(Chart.class, "lineThickness");// 折线的厚度
		xstream.useAttributeFor(Chart.class, "anchorRadius");// 折线节点半径，数字
		xstream.useAttributeFor(Chart.class, "anchorBgAlpha");// 折线节点透明度，[0-100]
		xstream.useAttributeFor(Chart.class, "anchorBgColor");// 折线节点填充颜色，6位16进制颜色值
		xstream.useAttributeFor(Chart.class, "anchorBorderColor");// 折线节点边框颜色，6位16进制颜色值

		// Set标签使用的参数
		xstream.useAttributeFor(Chart.class, "value");// 数据值
		xstream.useAttributeFor(Chart.class, "color");// 颜色
		xstream.useAttributeFor(Chart.class, "link");// 链接（本窗口打开[Url]，新窗口打开[n-Url]，调用JS函数[JavaScript:函数]）
		xstream.useAttributeFor(Chart.class, "name");// 横向坐标轴标签名称

		xstream.useAttributeFor(Chart.class, "canvasBorderAlpha");
		xstream.useAttributeFor(Chart.class, "background");

		xstream.useAttributeFor(Chart.class, "showBorder");
		xstream.useAttributeFor(Chart.class, "borderColor");
		xstream.useAttributeFor(Chart.class, "borderThickness");
		xstream.useAttributeFor(Chart.class, "borderAlpha");
		xstream.useAttributeFor(Chart.class, "backgroundImage");
		xstream.useAttributeFor(Chart.class, "showShadow");
		xstream.useAttributeFor(Chart.class, "numberScaleUnit");
		xstream.useAttributeFor(Chart.class, "numberScaleValue");
		xstream.useAttributeFor(Chart.class, "showLabels");
		

		// Category
		xstream.alias("category", Category.class);
		xstream.useAttributeFor(Category.class, "label");
		// Dataset
		xstream.alias("dataset", Dataset.class);
		xstream.addImplicitCollection(Dataset.class, "set");
		xstream.useAttributeFor(Dataset.class, "seriesName");
		xstream.useAttributeFor(Dataset.class, "color");
		xstream.useAttributeFor(Dataset.class, "showValue");
		xstream.useAttributeFor(Dataset.class, "showValues");
		xstream.useAttributeFor(Dataset.class, "lineThickness");
		xstream.useAttributeFor(Dataset.class, "yaxismaxvalue");
		xstream.useAttributeFor(Dataset.class, "anchorSides");
		xstream.useAttributeFor(Dataset.class, "anchorRadius");
		xstream.useAttributeFor(Dataset.class, "anchorAlpha");
		xstream.useAttributeFor(Dataset.class, "parentYAxis");
		xstream.useAttributeFor(Dataset.class, "renderAs");
		xstream.useAttributeFor(Dataset.class, "showYAxisValues");
		

		// Dataset-new
		xstream.useAttributeFor(Dataset.class, "anchorBorderColor");
		xstream.useAttributeFor(Dataset.class, "anchorBgColor");
		// set
		xstream.alias("set", Set.class);
		xstream.useAttributeFor(Set.class, "label");
		xstream.useAttributeFor(Set.class, "value");
		xstream.useAttributeFor(Set.class, "color");
		xstream.useAttributeFor(Set.class, "alpha");
		// ColorRange
		xstream.alias("colorRange", ColorRange.class);
		xstream.addImplicitCollection(ColorRange.class, "color");
		// Dials
		xstream.alias("dials", Dials.class);
		xstream.addImplicitCollection(Dials.class, "dial");
		// Dial
		xstream.alias("dial", Dial.class);
		xstream.useAttributeFor(Dial.class, "value");
		xstream.useAttributeFor(Dial.class, "borderAlpha");
		xstream.useAttributeFor(Dial.class, "bgColor");
		xstream.useAttributeFor(Dial.class, "baseWidth");
		xstream.useAttributeFor(Dial.class, "topWidth");
		xstream.useAttributeFor(Dial.class, "radius");
		// Color
		xstream.alias("color", Color.class);
		xstream.useAttributeFor(Color.class, "minValue");
		xstream.useAttributeFor(Color.class, "maxValue");
		xstream.useAttributeFor(Color.class, "code");
		// styles
		xstream.alias("styles", Styles.class);
		xstream.useAttributeFor(Styles.class, "definition");
		xstream.useAttributeFor(Styles.class, "application");
		// Definition
		xstream.alias("definition", Definition.class);
		xstream.addImplicitCollection(Definition.class, "style");

		// Style
		xstream.alias("style", Style.class);
		xstream.useAttributeFor(Style.class, "name");
		xstream.useAttributeFor(Style.class, "type");
		xstream.useAttributeFor(Style.class, "param");
		xstream.useAttributeFor(Style.class, "start");
		xstream.useAttributeFor(Style.class, "duration");
		xstream.useAttributeFor(Style.class, "alpha");
		xstream.useAttributeFor(Style.class, "bgColor");
		xstream.useAttributeFor(Style.class, "face");
		xstream.useAttributeFor(Style.class, "size");
		xstream.useAttributeFor(Style.class, "color");
		xstream.useAttributeFor(Style.class, "bold");
		xstream.useAttributeFor(Style.class, "lineThickness");
		xstream.useAttributeFor(Style.class, "anchorRadius");
		xstream.useAttributeFor(Style.class, "anchorBgAlpha");
		xstream.useAttributeFor(Style.class, "anchorBgColor");
		xstream.useAttributeFor(Style.class, "anchorBorderColor");

		// Application
		xstream.alias("application", Application.class);
		xstream.addImplicitCollection(Application.class, "apply");
		// Apply
		xstream.alias("apply", Apply.class);
		xstream.useAttributeFor(Apply.class, "toObject");
		xstream.useAttributeFor(Apply.class, "styles");

		return xstream.toXML(chart);
	}

	public String intDay2Str(int day) {
		switch (day) {
		case 1:
			return "周日";
		case 2:
			return "周一";
		case 3:
			return "周二";
		case 4:
			return "周三";
		case 5:
			return "周四";
		case 6:
			return "周五";
		case 7:
			return "周六";
		}
		return String.valueOf(day);
	}

	public String shortIntAdd0(int shortInt) {
		if (shortInt < 10) {

			switch (shortInt) {
			case 0:
				return "00:00";
			case 1:
				return "01:00";
			case 2:
				return "02:00";
			case 3:
				return "03:00";
			case 4:
				return "04:00";
			case 5:
				return "05:00";
			case 6:
				return "06:00";
			case 7:
				return "07:00";
			case 8:
				return "08:00";
			case 9:
				return "09:00";
			}
		}
		return String.valueOf(shortInt) + ":00";
	}

	public BigDecimal randomMN(BigDecimal m, BigDecimal n, int scale) {
		return m.add(n.subtract(m).multiply(new BigDecimal(new Random().nextDouble()))).setScale(scale,
				java.math.BigDecimal.ROUND_HALF_UP);
	}

	public static void main(String[] args) {
		MonitorParent m = new MonitorParent();
		System.out.println(m.randomMN(new BigDecimal(0.6), new BigDecimal(0.7), 5));
	}
}
