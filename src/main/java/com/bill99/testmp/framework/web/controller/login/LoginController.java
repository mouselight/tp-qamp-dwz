package com.bill99.testmp.framework.web.controller.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.RequestUtils;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.domain.security.BadCredentialsException;
import com.bill99.riaframework.domain.security.DisabledException;
import com.bill99.riaframework.domain.security.UserNoPermsException;
import com.bill99.riaframework.domain.security.UsernameNotFoundException;
import com.bill99.riaframework.domain.session.SessionProvider;
import com.bill99.riaframework.orm.entity.Authentication;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.AuthenticationService;
import com.bill99.riaframework.orm.manager.UserService;

@Controller
public class LoginController {

	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public String input(HttpServletRequest request, ModelMap model) {
		String authId = (String) sessionProvider.getAttribute(request, RiaFrameworkUtils.AUTH_KEY);
		if (authId != null) {
			// 存在认证ID
			Authentication auth = authenticationService.retrieve(authId);
			// 存在认证信息，且未过期
			if (auth != null) {
				model.addAttribute("auth", auth);
			}
		}
		return "framwork/login";
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public void login(String username, String password, String message, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
		AjaxDoneUtil ajaxDoneUtil = null;
		String eMessage = null;
		try {
			// 验证用户名密码、保存登录信息、更新session信息
			String ip = RequestUtils.getIpAddr(request);
			Authentication auth = authenticationService.porocessLogin(username.trim(), password, ip, request, response, sessionProvider);
			User user = userService.findById(auth.getIdUser());
			// 用户是否禁用
			if (user.getIsDisabled()) {
				// 如果禁用则退出登录
				authenticationService.deleteById(auth.getId());
				sessionProvider.logout(request, response);
				throw new DisabledException("用户已禁用");
			}
			// 用户是否有首页访问权限
			if ((user.getIsAdmin() != null && !user.getIsAdmin()) && !user.getPerms().contains("/index.htm")) {
				throw new UserNoPermsException("用户无首页访问权限");
			}
			RiaFrameworkUtils.setUseToSession(httpSession, user);// user加入至Session
			ajaxDoneUtil = new AjaxDoneUtil("200", null, null, null, null, "index.htm");
			ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			return;
		} catch (UsernameNotFoundException e) {
			eMessage = e.getMessage();
		} catch (BadCredentialsException e) {
			eMessage = e.getMessage();
		} catch (DisabledException e) {
			eMessage = e.getMessage();
		} catch (UserNoPermsException e) {
			eMessage = e.getMessage();
		}
		if (!StringUtils.hasLength(message)) {
			message = eMessage;
		}
		ajaxDoneUtil = new AjaxDoneUtil("300", message, null, null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping(value = "/login_dialog.htm")
	public String login_dialog() {
		return "framwork/login_dialog";
	}

	@RequestMapping(value = "/logout.htm")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		String authId = (String) sessionProvider.getAttribute(request, RiaFrameworkUtils.AUTH_KEY);
		if (authId != null) {
			authenticationService.deleteById(authId);
			sessionProvider.logout(request, response);
		}
		return "redirect:index.htm";
	}

	@Autowired
	private UserService userService;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private SessionProvider sessionProvider;
}
