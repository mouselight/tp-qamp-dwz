package com.bill99.testmp.framework.web.controller.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.dto.UserDto;
import com.bill99.riaframework.common.dto.UserExtDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.DateUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.domain.security.encoder.PwdEncoder;
import com.bill99.riaframework.orm.entity.User;
import com.bill99.riaframework.orm.manager.UserService;

@Controller
public class PersonalController {

	private static Logger logger = Logger.getLogger(PersonalController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private PwdEncoder pwdEncoder;

	//编辑用户信息 - 页面
	@RequestMapping("/framwork/personal/edit_info.htm")
	public String edit_info(HttpServletRequest request, Model model) {
		User user = userService.findById(RiaFrameworkUtils.getUserId(request));
		model.addAttribute("user", user);
		return "framwork/personal/edit_info";
	}

	//更新用户信息
	@RequestMapping("/framwork/personal/update_info.htm")
	public void update_info(HttpServletResponse response, HttpServletRequest request, Model model, UserCmd cond) {
		User user = RiaFrameworkUtils.getUser(request);
		try {
			// Prepare Cond
			UserDto userDto = new UserDto();
			userDto.setId(cond.getId());// PK
			userDto.setEmail(cond.getEmail());
			userDto.setPassword(cond.getPassword());

			// Ext
			UserExtDto userExtDto = new UserExtDto();
			userExtDto.setBirthday(DateUtil.parseDate(cond.getBirthday()));
			userExtDto.setGender(cond.getGender());
			userExtDto.setMoble(cond.getMoble());
			userExtDto.setMsn(cond.getMsn());
			userExtDto.setPhone(cond.getPhone());
			userExtDto.setQq(cond.getQq());
			userExtDto.setRealname(cond.getRealname());
			userExtDto.setWeddingday(DateUtil.parseDate(cond.getWeddingday()));
			userExtDto.setComefrom(cond.getComefrom());
			userExtDto.setIntro(cond.getIntro());
			userExtDto.setUpdateUser(user.getUsername());
			userExtDto.setUpdateDate(new Date());

			userDto.setUserExtDto(userExtDto);

			// Update User
			userService.update4Person(userDto);

			// log
			logger.info("<-- User " + RiaFrameworkUtils.getUser(request).getUsername() + " Update User Success & id = " + cond.getId() + "-->");

			model.addAttribute("message", "global.success.update");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PersonalAct.updateUserInfo Occur Exception", e);
			model.addAttribute("message", "global.failed.update");
		}
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改个人信息成功", "edit_info", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	//修改密码 - 页面
	@RequestMapping("/framwork/personal/change_pwd.htm")
	public String change_pwd(HttpServletRequest request, Model model) {
		User user = RiaFrameworkUtils.getUser(request);
		model.addAttribute("user", user);
		return "framwork/personal/edit_password";
	}

	//修改密码
	@RequestMapping("/framwork/personal/update_pwd.htm")
	public void update_pwd(HttpServletResponse response, HttpServletRequest request, Model model, UserCmd cond) {
		try {
			User user = RiaFrameworkUtils.getUser(request);

			if (pwdEncoder.encodePassword(cond.getPassword()).equals(user.getPassword())) {
				userService.update(user.getId(), cond.getNewPwd(), null);
				AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改密码成功", null, null, null, null);
				ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			} else {
				AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("300", "原密码不正确", null, null, null, null);
				ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
			}

			model.addAttribute("message", "global.success.edit");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("PersonalAct.profileUpdate Occur Exception", e);
			model.addAttribute("message", "global.failed.edit");
		}
	}

}
