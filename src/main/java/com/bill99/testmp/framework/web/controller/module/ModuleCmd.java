/*
   File: 
   Description:
   Copyright 2004-2012 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-9-25		    leofung	   		leofung
 */

package com.bill99.testmp.framework.web.controller.module;

import com.bill99.riaframework.common.command.PageCmd;

/**
 * @author leofung
 * 
 */
public class ModuleCmd extends PageCmd {

	private static final long serialVersionUID = 8093079453915544331L;

	private Long idModule;
	private Long idModuleResource;
	private Long parentId;
	private String idModuleResources;
	private String idModules;
	private Long idModuleParent;

	private String name;
	private String uri;
	private String moduleType;
	private Integer optType;
	private Boolean status;
	private Integer type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public Integer getOptType() {
		return optType;
	}

	public void setOptType(Integer optType) {
		this.optType = optType;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the idModule
	 */
	public Long getIdModule() {
		return idModule;
	}

	/**
	 * @param idModule
	 *            the idModule to set
	 */
	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	/**
	 * @return the idModuleResource
	 */
	public Long getIdModuleResource() {
		return idModuleResource;
	}

	/**
	 * @param idModuleResource
	 *            the idModuleResource to set
	 */
	public void setIdModuleResource(Long idModuleResource) {
		this.idModuleResource = idModuleResource;
	}

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @param idModuleResources
	 *            the idModuleResources to set
	 */
	public void setIdModuleResources(String idModuleResources) {
		this.idModuleResources = idModuleResources;
	}

	/**
	 * @return the idModuleResources
	 */
	public String getIdModuleResources() {
		return idModuleResources;
	}

	/**
	 * @param idModules
	 *            the idModules to set
	 */
	public void setIdModules(String idModules) {
		this.idModules = idModules;
	}

	/**
	 * @return the idModules
	 */
	public String getIdModules() {
		return idModules;
	}

	/**
	 * @param idModuleParent the idModuleParent to set
	 */
	public void setIdModuleParent(Long idModuleParent) {
		this.idModuleParent = idModuleParent;
	}

	/**
	 * @return the idModuleParent
	 */
	public Long getIdModuleParent() {
		return idModuleParent;
	}

}
