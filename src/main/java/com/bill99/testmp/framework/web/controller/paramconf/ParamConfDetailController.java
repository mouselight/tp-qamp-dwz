/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.testmp.framework.web.controller.paramconf;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.command.PageCmd;
import com.bill99.riaframework.common.dto.ParamConfDetailDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.common.utils.RiaFrameworkUtils;
import com.bill99.riaframework.orm.manager.ParamConfDetailService;
import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.rmca.common.util.DateUtil;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 * 
 */
@Controller
public class ParamConfDetailController {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	@Autowired
	private ParamConfDetailService paramConfDetailService;

	@RequestMapping("/paramconf/paramconfdetail_list.htm")
	public String list(String classCode, PageCmd pageCmd, ModelMap model) {
		Pagination pagination = paramConfDetailService.getConfDetailList(classCode, pageCmd.getPageNum(), pageCmd.getNumPerPage());
		model.addAttribute("pagination", pagination);
		model.addAttribute("classCode", classCode);
		return "paramconf/list_paramconfdetail";
	}

	@RequestMapping("/paramconf/paramconfdetail_add.htm")
	public String add(String classCode, ModelMap model) {
		model.addAttribute("classCode", classCode);
		return "paramconf/add_paramconfdetail";
	}

	@RequestMapping("/paramconf/paramconfdetail_save.htm")
	public void save(ParamConfDetailDto paramConfDetailDto, HttpServletResponse response, HttpServletRequest request) {
		AjaxDoneUtil ajaxDoneUtil = null;
		paramConfDetailDto.setCreateDate(DateUtil.getTimeNow());
		paramConfDetailDto.setCreateUser(RiaFrameworkUtils.getUser(request).getUsername());
		paramConfDetailDto.setStatus(Boolean.TRUE);
		try {
			paramConfDetailService.save(paramConfDetailDto);
			ajaxDoneUtil = new AjaxDoneUtil("200", "添加成功", "paramconfdetail_list", null, "closeCurrent", null);
		} catch (Exception e) {
			if (e instanceof DataIntegrityViolationException) {
				ajaxDoneUtil = new AjaxDoneUtil("300", "记录已存在,不能重复添加", "paramconfdetail_list", null, "closeCurrent", null);
			} else {
				ajaxDoneUtil = new AjaxDoneUtil("300", "操作失败", "paramconfdetail_list", null, "closeCurrent", null);
				logger.error("ParamConfDetail-save" + e);
			}
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/paramconf/paramconfdetail_edit.htm")
	public String edit(Long idConfDetail, ModelMap model) {
		ParamConfDetailDto paramConfDetailDto = paramConfDetailService.getConfDetailById(idConfDetail);
		model.addAttribute("item", paramConfDetailDto);
		return "paramconf/edit_paramconfdetail";
	}

	@RequestMapping("/paramconf/paramconfdetail_update.htm")
	public void update(ParamConfDetailDto paramConfDetailDto, HttpServletResponse response, HttpServletRequest request) {
		AjaxDoneUtil ajaxDoneUtil = null;
		paramConfDetailDto.setUpdateDate(DateUtil.getTimeNow());
		paramConfDetailDto.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
		try {
			paramConfDetailService.update(paramConfDetailDto);
			ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "paramconfdetail_list", null, "closeCurrent", null);
		} catch (Exception e) {
			if (e instanceof DataIntegrityViolationException) {
				ajaxDoneUtil = new AjaxDoneUtil("300", "记录已存在,不能重复添加", "paramconfdetail_list", null, "closeCurrent", null);
			} else {
				ajaxDoneUtil = new AjaxDoneUtil("300", "操作失败", "paramconfdetail_list", null, "closeCurrent", null);
				logger.error("ParamConfDetail-update" + e);
			}
		}
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/paramconf/paramconfdetail_delete.htm")
	public void delete(ParamConfDetailDto paramConfDetailDto, HttpServletResponse response, HttpServletRequest request) {
		paramConfDetailDto.setUpdateDate(DateUtil.getTimeNow());
		paramConfDetailDto.setUpdateUser(RiaFrameworkUtils.getUser(request).getUsername());
		paramConfDetailService.deleteLogic(paramConfDetailDto);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "删除成功", "paramconfdetail_list", null, null, null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

	@RequestMapping("/paramconf/vld_paramconfdetail_name.htm")
	public void vldName(HttpServletResponse response, String classCode, String name) {
		try {
			ResponseUtils.renderText(response, paramConfDetailService.vldCode(2, classCode, new String(name.getBytes("ISO-8859-1"), "UTF-8")) == true ? "true" : "false");
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
	}

	@RequestMapping("/paramconf/vld_paramconfdetail_code.htm")
	public void vldCode(HttpServletResponse response, String classCode, String code) {
		try {
			ResponseUtils.renderText(response, paramConfDetailService.vldCode(1, classCode, new String(code.getBytes("ISO-8859-1"), "UTF-8")) == true ? "true" : "false");
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
	}

}
