/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-5-8		    leo	   		leo
 */

package com.bill99.testmp.framework.web.controller.paramconf;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill99.riaframework.common.command.PageCmd;
import com.bill99.riaframework.common.dto.ParamConfDto;
import com.bill99.riaframework.common.utils.AjaxDoneUtil;
import com.bill99.riaframework.common.utils.ResponseUtils;
import com.bill99.riaframework.orm.manager.ParamConfService;
import com.jeecms.common.page.Pagination;

/**
 * @author leo
 * 
 */
@Controller
public class ParamConfController {

	@Autowired
	private ParamConfService paramConfService;

	@RequestMapping("/paramconf/paramconf_list.htm")
	public String list(PageCmd pageCmd, ModelMap model) {
		Pagination pagination = paramConfService.getConfList(pageCmd.getPageNum(), pageCmd.getNumPerPage());
		model.addAttribute("pagination", pagination);
		return "paramconf/list_paramconf";
	}

	@RequestMapping("/paramconf/paramconf_edit.htm")
	public String edit(String classCode, ModelMap model) {
		ParamConfDto paramConfDto = paramConfService.getConfById(classCode);
		model.addAttribute("item", paramConfDto);
		return "paramconf/edit_paramconf";
	}

	@RequestMapping("/paramconf/paramconf_update.htm")
	public void update(ParamConfDto paramConfDto, HttpServletResponse response) {
		paramConfService.update(paramConfDto);
		AjaxDoneUtil ajaxDoneUtil = new AjaxDoneUtil("200", "修改成功", "add_teststep", null, "closeCurrent", null);
		ResponseUtils.renderJson(response, ajaxDoneUtil.getJsonAjaxDone(ajaxDoneUtil));
	}

}
