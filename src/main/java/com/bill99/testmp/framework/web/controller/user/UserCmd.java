package com.bill99.testmp.framework.web.controller.user;

import java.util.Map;

import com.bill99.riaframework.common.command.PageCmd;
import com.bill99.testmp.framework.utils.PrimaryDataFactory;

public class UserCmd extends PageCmd {

	private static final long serialVersionUID = -2697902341436544031L;

	private Long id;
	private Long[] ids;// 批量删除时使用
	private Long roleId;
	private Long[] roleIds;// 页面多选获取的角色id
	private Long idRegion;
	private Long idStore;
	private Long idCity;
	private String username;
	private String password;
	private String email;
	private Boolean isDisabled;
	private String entryMode;

	// Ext
	private String realname;
	private Boolean gender;
	private String birthday;
	private String weddingday;
	private String intro;
	private String comefrom;
	private String qq;
	private String msn;
	private String phone;
	private String moble;

	private String origPwd;
	private String newPwd;

	private Long[] projectIds;
	private Integer num;
	private Map<String, String> teamMap;
	private Long teamId;
	private String userIds;
	private String teamIds;

	public String getTeamIds() {
		return teamIds;
	}

	public void setTeamIds(String teamIds) {
		this.teamIds = teamIds;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Long idRegion) {
		this.idRegion = idRegion;
	}

	public Long getIdStore() {
		return idStore;
	}

	public void setIdStore(Long idStore) {
		this.idStore = idStore;
	}

	public Long getIdCity() {
		return idCity;
	}

	public void setIdCity(Long idCity) {
		this.idCity = idCity;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getWeddingday() {
		return weddingday;
	}

	public void setWeddingday(String weddingday) {
		this.weddingday = weddingday;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getComefrom() {
		return comefrom;
	}

	public void setComefrom(String comefrom) {
		this.comefrom = comefrom;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMoble() {
		return moble;
	}

	public void setMoble(String moble) {
		this.moble = moble;
	}

	public void setOrigPwd(String origPwd) {
		this.origPwd = origPwd;
	}

	public String getOrigPwd() {
		return origPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Boolean getIsDisabled() {
		return isDisabled;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public Long[] getIds() {
		return ids;
	}

	/**
	 * @param entryMode
	 *            the entryMode to set
	 */
	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}

	/**
	 * @return the entryMode
	 */
	public String getEntryMode() {
		return entryMode;
	}

	public Long[] getProjectIds() {
		return projectIds;
	}

	public void setProjectIds(Long[] projectIds) {
		this.projectIds = projectIds;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getNum() {
		return num;
	}

	public void setTeamMap(Map<String, String> teamMap) {
		this.teamMap = teamMap;
	}

	public Map<String, String> getTeamMap() {
		teamMap = PrimaryDataFactory.teamMap;
		return teamMap;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getUserIds() {
		return userIds;
	}

}
