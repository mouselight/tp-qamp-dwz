/*
   File: 
   Description:
   Copyright 2004-2012 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-10-10		    leofung	   		leofung
 */

package com.bill99.testmp.framework.domain;

import java.util.Set;

import com.bill99.riaframework.orm.entity.Module;

/**
 * @author leofung
 * 
 */
public interface CallbackBuildService {

	public Set<Module> getRoleModuleCallback(Long[] idModules);
}
