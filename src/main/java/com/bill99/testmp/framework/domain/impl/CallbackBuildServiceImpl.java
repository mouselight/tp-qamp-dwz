/*
   File: 
   Description:
   Copyright 2004-2012 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2012-10-10		    leofung	   		leofung
 */

package com.bill99.testmp.framework.domain.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.bill99.riaframework.orm.entity.Module;
import com.bill99.riaframework.orm.manager.ModuleMng;
import com.bill99.testmp.framework.domain.CallbackBuildService;

/**
 * @author leofung
 * 
 */
public class CallbackBuildServiceImpl implements CallbackBuildService {

	private ModuleMng moduleMng;

	public void setModuleMng(ModuleMng moduleMng) {
		this.moduleMng = moduleMng;
	}

	public Set<Module> getRoleModuleCallback(Long[] idModules) {
		Module module = null;
		Set<Module> modules = new HashSet<Module>();
		Set<Module> parents = null;
		Set<Module> childs = null;
		Map<String, Module> childsMap = null;
		for (Long idModule : idModules) {
			module = moduleMng.getModule(idModule);
			//所有父节点
			parents = this.getAllParents(module);
			if (parents.size() > 0) {
				modules.addAll(parents);
			}
			//用于判断当前节点是否展开。n=0时没有展开; n>=1时展开并存在选中子节点
			int n = 0;
			for (Module item : module.getChild()) {
				if (Arrays.asList(idModules).contains(item.getIdModule())) {
					n++;
				}
			}
			if (n == 0) {
				//没展开时加载所有子节点
				childs = this.loadAllChilds(module);
			} else {
				//展开时加载选中的子节点
				childsMap = this.loadSelectedChilds(module.getChild(), idModules);
			}

			if (childs != null && childs.size() > 0) {
				modules.addAll(childs);
			}
			if (childsMap != null && childsMap.size() > 0) {
				for (Map.Entry<String, Module> entry : childsMap.entrySet()) {
					modules.add(entry.getValue());
				}
			}
			modules.add(module);
		}
		return modules;
	}

	//获得当前节点选中的孩子节点
	private Map<String, Module> loadSelectedChilds(Set<Module> childs, Long[] idModules) {
		Map<String, Module> effectiveChilds = new HashMap<String, Module>();
		if (childs != null && childs.size() > 0) {
			Set<Module> temp = null;
			for (Module item : childs) {
				if (item == null) {
					continue;
				}
				if (Arrays.asList(idModules).contains(item.getIdModule())) {
					effectiveChilds.put(item.getIdModule().toString(), item);
					temp = this.loadAllChilds(item);
					if (temp.size() > 0) {
						for (Module itemTemp : temp) {
							if (itemTemp == null) {
								continue;
							}
							effectiveChilds.put(itemTemp.getIdModule().toString(), itemTemp);
						}
					}
				}
			}
		}
		return effectiveChilds;
	}

	/**
	 * 得到当前节点的所有父节点
	 * @param module 当前节点
	 * @return Set<Module> 父节点集合
	 */
	private Set<Module> getAllParents(Module module) {
		Set<Module> parents = new HashSet<Module>();
		getModuleParent(parents, module);
		return parents;
	}

	/**
	 * 递归得到当前节点所有父节点
	 * @param sets 父节点集合
	 * @param module 当前节点
	 */
	private void getModuleParent(Set<Module> sets, Module module) {
		if (module != null && module.getParent() != null) {
			sets.add(module.getParent());
			getModuleParent(sets, module.getParent());
		}
	}

	/**
	 * 得到当前节点的所有孩子节点
	 * @param module 当前节点
	 * @return Set<Module> 孩子节点集合
	 */
	private Set<Module> loadAllChilds(Module module) {
		Set<Module> childs = new HashSet<Module>();
		loadModuleChilds(childs, module);
		return childs;
	}

	/**
	 * 递归得到当前节点的所有孩子节点
	 * @param sets 孩子节点集合
	 * @param module 当前节点
	 */
	private void loadModuleChilds(Set<Module> sets, Module module) {
		if (module != null && module.getChild() != null) {
			sets.addAll(module.getChild());
			for (Module obj : module.getChild()) {
				if (obj == null) {
					continue;
				}
				loadModuleChilds(sets, obj);
			}
		}
	}
}
