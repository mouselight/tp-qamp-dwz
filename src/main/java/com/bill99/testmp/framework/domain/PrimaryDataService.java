package com.bill99.testmp.framework.domain;

import java.util.Map;

public interface PrimaryDataService {

	public Map<String, String> initTeamMap();

	public Map<String, String> initIssueStatus();

	public Map<String, String> initIssueType();

	public Map<String, String> initPriority();
	
	public Map<Integer, String> initAssoTcStatus();

}
