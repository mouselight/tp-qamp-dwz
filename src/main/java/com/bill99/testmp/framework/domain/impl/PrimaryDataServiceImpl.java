package com.bill99.testmp.framework.domain.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.rmca.common.util.Bill99Logger;
import com.bill99.testmp.framework.domain.PrimaryDataService;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

public class PrimaryDataServiceImpl implements PrimaryDataService {

	private final Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TestProjectsMng testProjectsMng;

	public void setTestProjectsMng(TestProjectsMng testProjectsMng) {
		this.testProjectsMng = testProjectsMng;
	}

	//Team(rm、cps、inf...)
	public Map<String, String> initTeamMap() {
		Map<String, String> teamMap = new HashMap<String, String>();
		try {
			List<TestProjects> projectList = testProjectsMng.findALl();
			if (null != projectList && projectList.size() > 0) {
				for (TestProjects project : projectList) {
					if (null != project.getJiraTeemId()) {
						teamMap.put(project.getJiraTeemId().toString(), project.getPrefix());
					}
				}
			}
			logger.info("-- data init succ --");
		} catch (Exception e) {
			logger.error("-- data init error --", e);
		}
		return teamMap;
	}

	//Issue-status
	public Map<String, String> initIssueStatus() {
		Map<String, String> issueStatusMap = new HashMap<String, String>();
		//issue workflow status
		issueStatusMap.put("1", "Open");
		issueStatusMap.put("10022", "Accepted");
		issueStatusMap.put("10023", "Analyzed");
		issueStatusMap.put("10024", "Assigned");
		issueStatusMap.put("10025", "Coded");
		issueStatusMap.put("10026", "Scheduled");
		issueStatusMap.put("10027", "Packaged");
		issueStatusMap.put("10028", "Deployed");
		issueStatusMap.put("10029", "Tested");

		//bug  workfolw status
		issueStatusMap.put("3", "In Progress");
		issueStatusMap.put("4", "Reopen");
		issueStatusMap.put("5", "Resolved");
		issueStatusMap.put("6", "Closed");
		issueStatusMap.put("10004", "集成测试");
		issueStatusMap.put("10005", "上线完成");
		issueStatusMap.put("10011", "功能测试");
		issueStatusMap.put("10012", "上线准备");
		issueStatusMap.put("10013", "人员确定");
		issueStatusMap.put("10015", "later");
		issueStatusMap.put("10016", "rejected");
		issueStatusMap.put("10017", "fixed");
		issueStatusMap.put("10019", "出包部署");
		issueStatusMap.put("10020", "pending");
		issueStatusMap.put("10021", "排期");
		return issueStatusMap;
	}

	public Map<String, String> initIssueType() {
		Map<String, String> issueTypeMap = new HashMap<String, String>();
		issueTypeMap.put("1", "Bug");
		issueTypeMap.put("2", "需求");
		issueTypeMap.put("3", "Task");
		issueTypeMap.put("4", "Improvement");
		issueTypeMap.put("6", "Epic");
		issueTypeMap.put("7", "Story");
		issueTypeMap.put("18", "SandBox2");
		return issueTypeMap;
	}

	public Map<String, String> initPriority() {
		Map<String, String> priorityMap = new HashMap<String, String>();
		priorityMap.put("1", "Blocker");
		priorityMap.put("2", "Critical");
		priorityMap.put("3", "Major");
		priorityMap.put("4", "Minor");
		priorityMap.put("5", "Trivial");
		priorityMap.put("6", "高");
		priorityMap.put("7", "中");
		priorityMap.put("8", "低");
		return priorityMap;
	}

	public Map<Integer, String> initAssoTcStatus() {
		Map<Integer, String> assoTcStatusMap = new HashMap<Integer, String>();
		assoTcStatusMap.put(1, "未执行");
		assoTcStatusMap.put(2, "成功");
		assoTcStatusMap.put(3, "失败");
		return assoTcStatusMap;
	}
}
