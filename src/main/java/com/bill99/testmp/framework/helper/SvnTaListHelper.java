package com.bill99.testmp.framework.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.bill99.riaframework.common.helper.Bill99Logger;

public class SvnTaListHelper {
	private static Bill99Logger logger = Bill99Logger.getLogger(SvnTaListHelper.class);

	public static void main(String[] args) {
		Map<String, List<String>> taSvnPathMap = parseSvn("http://svn.99bill.net/opt/99billdoc/PMD/DOC/QA/TestAutomation/project/trunk/99bill_QA_TA_DDP", "leo.feng@99bill.com",
				"99bill");
		for (Entry<String, List<String>> entry : taSvnPathMap.entrySet()) {
			System.out.println(entry.getKey());
			for (String string : entry.getValue()) {
				System.out.println(string);
			}
			System.out.println("-------------------------------------------");
		}
	}

	public static Map<String, List<String>> parseSvn(String url, String name, String password) {

		Map<String, List<String>> taSvnPathMap = new HashMap<String, List<String>>();
		SVNRepository repository = setupLibrary(url, name, password);
		if (repository == null) {
			return taSvnPathMap;
		}
		try {
			//			// 打印版本库的根
			//			System.out.println("Repository Root: " + repository.getRepositoryRoot(true));
			//			// 打印出版本库的UUID
			//			System.out.println("Repository UUID: " + repository.getRepositoryUUID(true));
			//			System.out.println("");
			// 打印版本库的目录树结构
			List<Map> itemList = new ArrayList<Map>();
			listEntries(repository, "", itemList);
			String fileName = null;
			String filePath = null;
			for (Map map : itemList) {
				fileName = (String) map.get("fileName");
				filePath = (String) map.get("filePath");
				taSvnPathMap.put(fileName, SvnTaFileHelper.parseSvnTaMethod(repository, fileName));
			}
		} catch (SVNException svne) {
			logger.error("打印版本树时发生错误: " + svne);
		}
		/*
		 * 获得版本库的最新版本树
		 */
		long latestRevision = -1;
		try {
			latestRevision = repository.getLatestRevision();
		} catch (SVNException svne) {
			logger.error("获取最新版本号时出错: " + svne);
		}
		//		System.out.println("版本库的最新版本是: " + latestRevision);

		return taSvnPathMap;

	}

	/*
	 * 此函数递归的获取版本库中某一目录下的所有条目。
	 */
	public static void listEntries(SVNRepository repository, String path, List<Map> itemList) throws SVNException {
		// 获取版本库的path目录下的所有条目。参数－1表示是最新版本。
		Collection entries = repository.getDir(path, -1, null, (Collection) null);
		Iterator iterator = entries.iterator();
		while (iterator.hasNext()) {
			SVNDirEntry entry = (SVNDirEntry) iterator.next();

			//			System.out.println("/" + (path.equals("") ? "" : path + "/") + entry.getName() + " (author: '" + entry.getAuthor() + "'; revision: " + entry.getRevision() + "; date: "
			//					+ entry.getDate() + ")");
			//			if (entry.getName().endsWith(".java")) {
			//				System.out.println("/" + (path.equals("") ? "" : path + "/") + entry.getName());
			//			}
			String fileName = (path.equals("") ? "" : path + "/") + entry.getName();

			// System.out.println(fileName);
			if (entry.getKind() == SVNNodeKind.FILE && entry.getName().endsWith(".java")) {
				Map m = new HashMap();
				m.put("fileName", fileName);
				m.put("reversion", new Long(entry.getRevision()));
				m.put("submitTime", entry.getDate());
				m.put("user", entry.getAuthor());
				m.put("filePath", "/" + (path.equals("") ? "" : path + "/"));
				itemList.add(m);
			}

			/*
			 * 检查此条目是否为目录，如果为目录递归执行
			 */
			if (entry.getKind() == SVNNodeKind.DIR) {
				listEntries(repository, (path.equals("")) ? entry.getName() : path + "/" + entry.getName(), itemList);
			}
		}
	}

	/*
	 * 初始化库
	 */
	private static SVNRepository setupLibrary(String url, String name, String password) {
		/*
		 * For using over http:// and https://
		 */
		DAVRepositoryFactory.setup();
		/*
		 * For using over svn:// and svn+xxx://
		 */
		SVNRepositoryFactoryImpl.setup();

		/*
		 * For using over file:///
		 */
		FSRepositoryFactory.setup();
		/*
		 * 相关变量赋值
		 */
		// 定义svn版本库的URL。
		SVNURL repositoryURL = null;
		// 定义版本库。
		SVNRepository repository = null;
		/*
		 * 实例化版本库类
		 */
		try {
			// 获取SVN的URL。
			repositoryURL = SVNURL.parseURIEncoded(url);
			// 根据URL实例化SVN版本库。
			repository = SVNRepositoryFactory.create(repositoryURL);
		} catch (SVNException svne) {
			/*
			 * 打印版本库实例创建失败的异常。
			 */
			logger.error("创建版本库实例时失败，版本库的URL是 '" + url + "': " + svne);
			return repository;
		}

		/*
		 * 对版本库设置认证信息。
		 */
		ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(name, password);
		repository.setAuthenticationManager(authManager);

		/*
		 * 上面的代码基本上是固定的操作。 下面的部门根据任务不同，执行不同的操作。
		 */
		return repository;
	}
}
