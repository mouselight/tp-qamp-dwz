package com.bill99.testmp.framework.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestMethodRegexHelper {
	//(^\\($\\))
	private final static String regexStart = "@Test(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)";
	private final static String regexEnd = "@Test(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)(\\S+)\\(";
	private final static Pattern patternStart = Pattern.compile(regexStart);
	private final static Pattern patternEnd = Pattern.compile(regexEnd);

	public static List<String> getTestMethod(String classContenx) {
		List<String> tempList = new ArrayList<String>();

		List<String> strListStart = Arrays.asList(patternStart.split(classContenx));
		//		List<String> strListEnd = Arrays.asList(patternEnd.split(classContenx));

		if (strListStart != null && strListStart.size() > 0) {
			Matcher mStart = patternStart.matcher(classContenx);
			Matcher mEnd = patternEnd.matcher(classContenx);
			int i = 1;
			while (mStart.find() && mEnd.find()) {
				String start = classContenx.substring(mStart.start(), mStart.end());
				String end = classContenx.substring(mEnd.start(), mEnd.end() - 1);
				//				tempList.add(end.replaceAll(start, ""));
				tempList.add(end.substring(start.length(), end.length()));
				i++;
			}
		}
		return tempList;
	}

	/*	public static void main(String[] args) {
			String s = "@Test public  void       set_access_limit(hashPara={}){}  @Test public  void       set_access_limit(hashPara={}) ";
			//		String regex = "@Test\\spublic\\s+[\\S]+\\s+(.*?)[(].*";
			//		String methodName = s.replaceAll(regex, "$1");
			//		System.out.println(methodName);
			String regexStart = "@Test(\\s+)public(\\s+)(\\S+)(\\s+)";
			String regexEnd = "@Test(\\s+)public(\\s+)(\\S+)(\\s+)(\\S+)\\(";

			Pattern patternStart = Pattern.compile(regexStart);
			Pattern patternEnd = Pattern.compile(regexEnd);

			List<String> strListStart = Arrays.asList(patternStart.split(s));
			List<String> strListEnd = Arrays.asList(patternEnd.split(s));

			//		for (int i = 0; i < strListStart.size(); i++) {
			//			System.out.println(strListStart.get(i));
			//			System.out.println(strListEnd.get(i));
			//			System.out.println("=======================");
			//		}

			//		for (String string : strListStart) {
			//			System.out.println(string);
			//		}

			if (strListStart != null && strListStart.size() > 0) {
				List<String> tempList = new ArrayList<String>();
				Matcher mStart = patternStart.matcher(s);
				Matcher mEnd = patternEnd.matcher(s);
				int i = 1;
				while (mStart.find() && mEnd.find()) {
					String start = s.substring(mStart.start(), mStart.end());
					String end = s.substring(mEnd.start(), mEnd.end() - 1);
					tempList.add(end.replaceAll(start, ""));
					i++;
				}
				for (String string : tempList) {
					System.out.println(string);
				}
			}
		}*/

}
