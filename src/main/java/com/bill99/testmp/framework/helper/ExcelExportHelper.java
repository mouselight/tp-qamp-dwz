package com.bill99.testmp.framework.helper;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.bill99.testmp.framework.enums.ExecutionTypeEnum;
import com.bill99.testmp.framework.enums.ImportanceEnum;
import com.bill99.testmp.framework.enums.StatusEnum;
import com.bill99.testmp.framework.enums.TaStatusEnum;
import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;

public class ExcelExportHelper {

	private static Log logger = LogFactory.getLog(ExcelExportHelper.class);

	public static void testSuiteExport4Excel(HttpServletResponse response, List<TestCases> testCasesList, String testProjectName, String excelName) throws Exception {
		WritableWorkbook workbook = null;
		ServletOutputStream os = null;
		try {
			String fileName = testProjectName + "_" + excelName;
			fileName = new String(fileName.getBytes(), "iso-8859-1");
			response.setCharacterEncoding("gb2312");
			response.reset();
			response.setHeader("pragma", "no-cache");
			//设置下载格式
			response.setHeader("Content-Type", "application/vnd.ms-excel.numberformat:@;charset=gb2312");
			response.addHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".xls\"");// 点击导出excle按钮时候页面显示的默认名称
			response.setHeader("Connection", "close");
			//获取输出流
			os = response.getOutputStream();
			workbook = Workbook.createWorkbook(os);
			//设置单元格样式
			WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK));//字体,大小,加粗,斜体,下划线,颜色
			titleFormat.setBackground(Colour.BLUE_GREY);//背景色
			//设置分隔符样式
			WritableCellFormat splitFormat = new WritableCellFormat();
			splitFormat.setAlignment(Alignment.CENTRE);//水平居中
			//设置字段类型
			WritableCellFormat defaultFormat = new WritableCellFormat(NumberFormats.TEXT);

			WritableCellFormat wrappedText = new WritableCellFormat(WritableWorkbook.ARIAL_10_PT);
			wrappedText.setWrap(true);//可换行的label样式

			//创建Sheet
			WritableSheet sheet = workbook.createSheet("Sheet_TestCases", 0);
			//			设置Head
			sheet.addCell(new Label(0, 0, "用例标识", titleFormat));
			sheet.addCell(new Label(1, 0, "用例名称", titleFormat));
			sheet.addCell(new Label(2, 0, "摘要", titleFormat));
			sheet.addCell(new Label(3, 0, "前置条件", titleFormat));
			sheet.addCell(new Label(4, 0, "测试步骤", titleFormat));
			sheet.addCell(new Label(5, 0, "预期结果", titleFormat));
			sheet.addCell(new Label(6, 0, "用例等级", titleFormat));
			sheet.addCell(new Label(7, 0, "测试方式", titleFormat));
			sheet.addCell(new Label(8, 0, "自动化覆盖", titleFormat));
			sheet.addCell(new Label(9, 0, "状态", titleFormat));
			sheet.setColumnView(0, 10);//设置列宽
			sheet.setColumnView(1, 60);//设置列宽
			sheet.setColumnView(2, 20);//设置列宽
			sheet.setColumnView(3, 25);//设置列宽
			sheet.setColumnView(4, 80);//设置列宽
			sheet.setColumnView(5, 80);//设置列宽
			sheet.setColumnView(6, 10);//设置列宽
			sheet.setColumnView(7, 10);//设置列宽
			sheet.setColumnView(8, 10);//设置列宽
			sheet.setColumnView(9, 10);//设置列宽

			TestCases testCase = null;
			StringBuilder actionsBuilder = null;
			StringBuilder expectedResultsBuilder = null;
			for (int j = 0; j < testCasesList.size(); j++) {
				testCase = testCasesList.get(j);
				sheet.addCell(new Label(0, j + 1, testProjectName + "-" + testCase.getTestCaseId(), defaultFormat));//用例编号
				sheet.addCell(new Label(1, j + 1, testCase.getTestCaseName(), defaultFormat));//用例名称
				sheet.addCell(new Label(2, j + 1, testCase.getSummary(), defaultFormat));//摘要
				sheet.addCell(new Label(3, j + 1, testCase.getPreconditions(), defaultFormat));// 前置条件
				List<TcSteps> tcStepsList = testCase.getTcStepsList();
				if (tcStepsList != null && tcStepsList.size() > 0) {
					actionsBuilder = new StringBuilder();
					expectedResultsBuilder = new StringBuilder();
					for (TcSteps tcSteps : tcStepsList) {
						if (StringUtils.hasLength(tcSteps.getActions())) {
							actionsBuilder.append(tcSteps.getStepNumber());
							actionsBuilder.append(".");
							actionsBuilder.append(tcSteps.getActions());
							actionsBuilder.append("\r\n");
						}
						if (StringUtils.hasLength(tcSteps.getExpectedResults())) {
							expectedResultsBuilder.append(tcSteps.getStepNumber());
							expectedResultsBuilder.append(".");
							expectedResultsBuilder.append(tcSteps.getExpectedResults());
							expectedResultsBuilder.append("\r\n");
						}
					}
					sheet.addCell(new Label(4, j + 1, actionsBuilder.toString(), wrappedText));// 测试步骤
					sheet.addCell(new Label(5, j + 1, expectedResultsBuilder.toString(), wrappedText));// 预期结果
				}

				if (testCase.getImportance() != null) {
					sheet.addCell(new Label(6, j + 1, ImportanceEnum.getAuditStatusStrValue(testCase.getImportance()), defaultFormat));// 用例等级
				}
				if (testCase.getExecutionType() != null) {
					sheet.addCell(new Label(7, j + 1, ExecutionTypeEnum.getAuditStatusStrValue(testCase.getExecutionType()), defaultFormat));// 测试方式
				}
				if (testCase.getTaStatus() != null) {
					sheet.addCell(new Label(8, j + 1, TaStatusEnum.getAuditStatusStrValue(testCase.getTaStatus()), defaultFormat));// 自动化覆盖
				}
				if (testCase.getStatus() != null) {
					sheet.addCell(new Label(9, j + 1, StatusEnum.getAuditStatusStrValue(testCase.getStatus()), defaultFormat));// 状态
				}

			}
			workbook.write();
		} catch (IOException e) {
			logger.error("-- ConfirmReport , Workbook.Write Error --", e);
			throw new Exception(e.getMessage());
		} catch (WriteException e) {
			logger.error("-- ConfirmReport , Create Writable Error --", e);
			throw new Exception(e.getMessage());
		} finally {
			try {
				if (null != workbook) {
					workbook.close();
				}
				if (null != os) {
					os.close();
				}
			} catch (WriteException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			} catch (IOException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			}

		}
	}
}
