package com.bill99.testmp.framework.helper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;
import com.bill99.testmp.testmanage.orm.entity.Testplans;

public class ExcelExportPlanHelper {

	private static Log logger = LogFactory.getLog(ExcelExportHelper.class);

	public static void planExport4Excel(HttpServletResponse response, TestIssue testIssue, Testplans testplan, String testProjectName,
			String excelName) throws Exception {
		WritableWorkbook workbook = null;
		ServletOutputStream os = null;
		try {
			String fileName = testProjectName+"_"+testIssue.getSummary();
			fileName = new String(fileName.getBytes(), "iso-8859-1");
			response.setCharacterEncoding("gb2312");
			response.reset();
			response.setHeader("pragma", "no-cache");
			// 设置下载格式
			response.setHeader("Content-Type", "application/vnd.ms-excel.numberformat:@;charset=gb2312");
			response.addHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".xls\"");// 点击导出excle按钮时候页面显示的默认名称
			response.setHeader("Connection", "close");
			// 获取输出流
			os = response.getOutputStream();
			workbook = Workbook.createWorkbook(os);
			// 设置单元格样式
			WritableCellFormat titleFormat = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.BLACK));// 字体,大小,加粗,斜体,下划线,颜色
			titleFormat.setBackground(Colour.BLUE_GREY);// 背景色
		
			// 设置分隔符样式
			WritableCellFormat splitFormat = new WritableCellFormat();
			splitFormat.setAlignment(Alignment.CENTRE);// 水平居中
			// 设置字段类型
			WritableCellFormat defaultFormat = new WritableCellFormat(NumberFormats.TEXT);

			WritableCellFormat wrappedText = new WritableCellFormat(WritableWorkbook.ARIAL_10_PT);
			wrappedText.setWrap(true);// 可换行的label样式

			// 创建Sheet
			WritableSheet sheet = workbook.createSheet(testIssue.getSummary(), 0);
			// 设置Head
			sheet.addCell(new Label(0, 0, "关键字", titleFormat));
			sheet.addCell(new Label(2, 0, "测试时间", titleFormat));
			sheet.addCell(new Label(4, 0, "上线时间", titleFormat));
			sheet.addCell(new Label(6, 0, "更新时间", titleFormat));
			sheet.addCell(new Label(0, 1, "项目名称", titleFormat));
			sheet.addCell(new Label(0, 2, "项目描述", titleFormat));
			sheet.addCell(new Label(0, 3, "需求地址", titleFormat));
			sheet.addCell(new Label(0, 4, "计划状态", titleFormat));
			sheet.addCell(new Label(0, 5, "计划描述", titleFormat));
			sheet.addCell(new Label(0, 6, "子任务", titleFormat));
			sheet.addCell(new Label(1, 6, "计划时间", titleFormat));
			sheet.addCell(new Label(2, 6, "资源", titleFormat));
			sheet.addCell(new Label(3, 6, "产品文档SVN地址", titleFormat));
			sheet.addCell(new Label(4, 6, "进度", titleFormat));
			sheet.addCell(new Label(5, 6, "TC进度/总数", titleFormat));
			sheet.addCell(new Label(6, 6, "备注", titleFormat));

			sheet.setColumnView(0, 20);// 设置列宽
			sheet.setColumnView(1, 40);// 设置列宽
			sheet.setColumnView(2, 20);// 设置列宽
			sheet.setColumnView(3, 25);// 设置列宽
			sheet.setColumnView(4, 15);// 设置列宽
			sheet.setColumnView(5, 20);// 设置列宽
			sheet.setColumnView(6, 20);// 设置列宽
			sheet.setColumnView(7, 20);// 设置列宽

			// 关键字

			if (testplan.getState().intValue() == 0) {
				sheet.addCell(new Label(1, 0, testIssue.getPkey() + "(未开始)", defaultFormat));
				sheet.addCell(new Label(1, 4, "(未开始)", defaultFormat));// 计划状态
			} else if (testplan.getState().intValue() == 1) {
				sheet.addCell(new Label(1, 0, testIssue.getPkey() + "(进行中)", defaultFormat));
				sheet.addCell(new Label(1, 4, "(进行中)", defaultFormat));// 计划状态
			} else if (testplan.getState().intValue() == 2) {
				sheet.addCell(new Label(1, 0, testIssue.getPkey() + "(完成)", defaultFormat));
				sheet.addCell(new Label(1, 4, "(完成)", defaultFormat));// 计划状态
			} else if (testplan.getState().intValue() == 3) {
				sheet.addCell(new Label(1, 0, testIssue.getPkey() + "(停止)", defaultFormat));
				sheet.addCell(new Label(1, 4, "(停止)", defaultFormat));// 计划状态
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sheet.addCell(new Label(3, 0,
					sdf.format(testplan.getPlanBeginDate()).toString() + "至" + sdf.format(testplan.getPlanEndDate()).toString(), defaultFormat));// 测试时间
            if(testIssue.getReleaseDate()!=null){
            	sheet.addCell(new Label(5, 0, sdf.format(testIssue.getReleaseDate()).toString(), defaultFormat));// 上线时间
            }
			if(testplan.getUpdateTime()!=null){
				sheet.addCell(new Label(7, 0, sdf1.format(testplan.getUpdateTime()).toString(), defaultFormat));// 更新时间
			}
			
			sheet.addCell(new Label(1, 1, testIssue.getSummary(), defaultFormat));// 项目名称
			sheet.addCell(new Label(1, 2, testIssue.getDescription(), defaultFormat));// 项目描述
			sheet.addCell(new Label(1, 3, testIssue.getSrsSvnPath(), defaultFormat));// 需求地址
			sheet.addCell(new Label(1, 5, testplan.getMemo(), defaultFormat));// 计划描述
			int j = 6;
			Set<TestplanSteps> testplanSteps = testplan.getTestplanSteps();
			for (TestplanSteps testplanStep : testplanSteps) {
				// 计划步骤
				if (testplanStep.getType() == 1) {
					j++;
					sheet.addCell(new Label(0, j, "测试设计", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					sheet.addCell(new Label(5, j, "--", defaultFormat));// Tc进度总数
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 2) {
					j++;
					sheet.addCell(new Label(0, j, "设计评审", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址

					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					sheet.addCell(new Label(5, j, "--", defaultFormat));// Tc进度总数
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 3) {
					j++;
					sheet.addCell(new Label(0, j, "用例编写", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					sheet.addCell(new Label(5, j, "--", defaultFormat));// Tc进度总数
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 4) {
					j++;
					sheet.addCell(new Label(0, j, "用例评审", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					sheet.addCell(new Label(5, j, "--", defaultFormat));// Tc进度总数
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 5) {
					j++;
					sheet.addCell(new Label(0, j, "用例执行-开发环境", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					if (testplanStep.getAssoTcCount() != null && testplanStep.getAssoTcCount().toString().length() > 0
							&& testplanStep.getAssoTcCount().intValue() != 0) {

						sheet.addCell(new Label(5, j, ((testplanStep.getAssoFinishTcCount() / testplanStep.getAssoTcCount()) * 100) + "%" + "/" + "("
								+ testplanStep.getAssoTcCount().toString() + ")", defaultFormat));// Tc进度总数
					} else {
						sheet.addCell(new Label(5, j, "关联TC", defaultFormat));// Tc进度总数
					}
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 6) {
					j++;
					sheet.addCell(new Label(0, j, "用例执行-集成环境", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					if (testplanStep.getAssoTcCount() != null && testplanStep.getAssoTcCount().toString().length() > 0
							&& testplanStep.getAssoTcCount().intValue() != 0) {

						sheet.addCell(new Label(5, j, ((testplanStep.getAssoFinishTcCount() / testplanStep.getAssoTcCount()) * 100) + "%" + "/" + "("
								+ testplanStep.getAssoTcCount().toString() + ")", defaultFormat));// Tc进度总数
					} else {
						sheet.addCell(new Label(5, j, "关联TC", defaultFormat));// Tc进度总数
					}
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				} else if (testplanStep.getType() == 7) {
					j++;
					sheet.addCell(new Label(0, j, "测试报告", defaultFormat));
					// 步骤计划时间
					sheet.addCell(new Label(1, j, sdf.format(testplanStep.getBeginTime()).toString() + "至"
							+ sdf.format(testplanStep.getEndTime()).toString(), defaultFormat));
					sheet.addCell(new Label(2, j, testplanStep.getRealnames(), defaultFormat));// 资源
					sheet.addCell(new Label(3, j, testplanStep.getOutput(), defaultFormat));// 产出文档SVN地址
					if (testplanStep.getState().intValue() == 0) {
						sheet.addCell(new Label(4, j, "未开始", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 1) {
						sheet.addCell(new Label(4, j, "进行中", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 2) {
						sheet.addCell(new Label(4, j, "完成", defaultFormat));// 进度
					} else if (testplanStep.getState().intValue() == 3) {
						sheet.addCell(new Label(4, j, "停止", defaultFormat));// 进度
					}
					sheet.addCell(new Label(5, j, "--", defaultFormat));// Tc进度总数
					sheet.addCell(new Label(6, j, testplanStep.getMemo(), defaultFormat));// 备注
				}
			}

			workbook.write();
		} catch (IOException e) {
			logger.error("-- ConfirmReport , Workbook.Write Error --", e);
			throw new Exception(e.getMessage());
		} catch (WriteException e) {
			logger.error("-- ConfirmReport , Create Writable Error --", e);
			throw new Exception(e.getMessage());
		} finally {
			try {
				if (null != workbook) {
					workbook.close();
				}
				if (null != os) {
					os.close();
				}
			} catch (WriteException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			} catch (IOException e) {
				logger.error("-- ConfirmReport , Workbook.Close Error --", e);
			}

		}
	}
}
