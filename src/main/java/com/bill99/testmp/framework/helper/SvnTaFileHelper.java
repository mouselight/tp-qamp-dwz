package com.bill99.testmp.framework.helper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.io.SVNRepository;

import com.bill99.riaframework.common.helper.Bill99Logger;

public class SvnTaFileHelper {
	private static Bill99Logger logger = Bill99Logger.getLogger(SvnTaFileHelper.class);

	public static List<String> parseSvnTaMethod(SVNRepository repository, String filePath) {
		List<String> taMethods = new ArrayList<String>();

		// 此变量用来存放要查看的文件的属性名/属性值列表。
		SVNProperties fileProperties = new SVNProperties();
		// 此输出流用来存放要查看的文件的内容。
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			// 获得版本库中文件的类型状态（是否存在、是目录还是文件），参数-1表示是版本库中的最新版本。
			SVNNodeKind nodeKind = repository.checkPath(filePath, -1);

			if (nodeKind == SVNNodeKind.NONE) {
				logger.error("要查看的条目 '" + filePath + "'不存在.");
			} else if (nodeKind == SVNNodeKind.DIR) {
				logger.error("要查看的条目 '" + filePath + "'是一个目录.");
			}
			// 获取要查看文件的内容和属性，结果保存在baos和fileProperties变量中。
			repository.getFile(filePath, -1, fileProperties, baos);

			if (baos != null && baos.size() > 0) {
				taMethods = RegexTaHelper.getTestMethod(baos.toString());
			}

		} catch (SVNException svne) {
			logger.error("在获取文件内容和属性时发生错误: " + svne);
		}

		// 获取文件的mime-type
		//		String mimeType = fileProperties.getStringValue(SVNProperty.MIME_TYPE);
		// 判断此文件是否是文本文件
		//		boolean isTextType = SVNProperty.isTextMimeType(mimeType);
		/*
		 * 显示文件的所有属性
		 */
		//		Iterator iterator = fileProperties.nameSet().iterator();
		//		while (iterator.hasNext()) {
		//			String propertyName = (String) iterator.next();
		//			String propertyValue = fileProperties.getStringValue(propertyName);
		//			System.out.println("文件的属性: " + propertyName + "=" + propertyValue);
		//		}
		/*
		 * 获得版本库的最新版本号。
		 */
		//		long latestRevision = -1;
		//		try {
		//			latestRevision = repository.getLatestRevision();
		//		} catch (SVNException svne) {
		//			System.err.println("获取最新版本号时出错: " + svne.getMessage());
		//			System.exit(1);
		//		}
		//		System.out.println("版本库的最新版本号: " + latestRevision);
		return taMethods;
	}
}
