package com.bill99.testmp.framework.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class RegexTaHelper {

	private final static String regexStart = "@Test(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)";
	private final static String regexEnd = "@Test(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)(\\S+)\\(";
	private final static Pattern patternStart = Pattern.compile(regexStart);
	private final static Pattern patternEnd = Pattern.compile(regexEnd);

	public static List<String> getTestMethod(String classContenx) {
		List<String> tempList = new ArrayList<String>();

		List<String> strListStart = Arrays.asList(patternStart.split(classContenx));
		//		List<String> strListEnd = Arrays.asList(patternEnd.split(classContenx));

		if (strListStart != null && strListStart.size() > 0) {
			Matcher mStart = patternStart.matcher(classContenx);
			Matcher mEnd = patternEnd.matcher(classContenx);
			int i = 1;
			while (mStart.find() && mEnd.find()) {
				String start = classContenx.substring(mStart.start(), mStart.end());
				String end = classContenx.substring(mEnd.start(), mEnd.end() - 1);
				//				tempList.add(end.replaceAll(start, ""));
				tempList.add(end.substring(start.length(), end.length()));
				i++;
			}
		}
		return tempList;
	}

	private final static String getTestCaseIdRegex = "(.*)?_";
	private final static Pattern getTestCaseIdPattern = Pattern.compile(getTestCaseIdRegex);

	public static Long getTestCaseId(String testCaseSn) {
		Long testCaseId = null;
		List<String> strListStart = Arrays.asList(getTestCaseIdPattern.split(testCaseSn));
		for (String string : strListStart) {
			if (StringUtils.hasLength(string)) {
				testCaseId = Long.valueOf(string.trim());
			}
		}
		return testCaseId;
	}

}
