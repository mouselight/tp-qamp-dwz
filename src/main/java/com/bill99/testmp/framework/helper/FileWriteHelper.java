package com.bill99.testmp.framework.helper;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jeecms.utils.StringUtil;

public class FileWriteHelper {
	private static Log logger = LogFactory.getLog(FileWriteHelper.class);

	/**
	 * write
	 * @param response
	 * @param bankConfirmUpload
	 */
	public static void write(HttpServletResponse response, InputStream inputStream, String fileName) {
		InputStream is = inputStream;
		ServletOutputStream writer = null;
		try {
			//准备输出流
			response.setContentType("text/plain;charset=UTF-8");
			response.setHeader("Connection", "close");
			response.setHeader("Content-Type", "application/octet-stream");
			writer = response.getOutputStream();
			//文件名
			if (StringUtil.isNull(fileName)) {
				fileName = "fileName";
			}
			byte[] b = fileName.getBytes("GBK");
			fileName = new String(b, "ISO-8859-1");
			response.setHeader("Content-disposition", "attachment; filename=" + fileName);
			//循环外写流
			byte[] bt = new byte[1024];
			int len;
			while ((len = is.read(bt)) != -1) {
				writer.write(bt, 0, len);
				writer.flush();
			}
			is.close();
			writer.close();
		} catch (Exception e) {
			logger.error("<-- write file stream error  -->", e);
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error("<-- close outputstream error -->", e);
				}
			}
			if (null != is) {
				try {
					is.close();
				} catch (IOException e) {
					logger.error("<-- close inputstream error -->", e);
				}
			}
		}
	}

}
