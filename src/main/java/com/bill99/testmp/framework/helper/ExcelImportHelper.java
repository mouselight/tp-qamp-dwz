package com.bill99.testmp.framework.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.bill99.testmp.testmanage.orm.entity.TcSteps;

public class ExcelImportHelper {
	private final static Log logger = LogFactory.getLog(ExcelExportHelper.class);
	private final static Pattern pattern = Pattern.compile("(\\d+[.、])");
	private static final int MAX_LENGTH = 500;

	public static List<TcSteps> getTcStepsList(String actions, String expectedResults) {
		List<TcSteps> tcStepsList = new ArrayList<TcSteps>();
		if (StringUtils.hasLength(actions) || StringUtils.hasLength(expectedResults)) {

			Map<String, String> actionsMap = subContexts(actions);
			Map<String, String> expectedResultsMap = subContexts(expectedResults);

			Set<String> keys = new HashSet<String>();
			keys.addAll(actionsMap.keySet());//
			keys.addAll(expectedResultsMap.keySet());//

			int i = 0;
			TcSteps tcSteps = null;
			for (String key : keys) {
				tcSteps = new TcSteps();
				if (StringUtils.hasLength(actionsMap.get(key)) && actionsMap.get(key).length() > MAX_LENGTH) {
					tcSteps.setActions(actionsMap.get(key).substring(0, MAX_LENGTH));
				} else {
					tcSteps.setActions(actionsMap.get(key));
				}
				if (StringUtils.hasLength(expectedResultsMap.get(key)) && expectedResultsMap.get(key).length() > MAX_LENGTH) {
					tcSteps.setExpectedResults(expectedResultsMap.get(key).substring(0, MAX_LENGTH));
				} else {
					tcSteps.setExpectedResults(expectedResultsMap.get(key));
				}
				tcSteps.setStepNumber(Integer.valueOf(key));

				if (!StringUtils.hasLength(tcSteps.getActions()) && i == 0) {
					tcSteps.setActions(actions);
				}
				if (!StringUtils.hasLength(tcSteps.getExpectedResults()) && i == 0) {
					tcSteps.setExpectedResults(expectedResults);
				}
				i++;
				tcStepsList.add(tcSteps);
			}

			if (keys.size() == 0) {
				tcSteps = new TcSteps();
				tcSteps.setActions(actions);
				tcSteps.setExpectedResults(expectedResults);
				tcSteps.setStepNumber(1);
				tcStepsList.add(tcSteps);
			}
		}
		return tcStepsList;
	}

	private static Map<String, String> subContexts(String contexts) {
		Map<String, String> contextsMap = new HashMap<String, String>();
		if (!StringUtils.hasLength(contexts)) {
			return contextsMap;
		}
		List<String> strList = Arrays.asList(pattern.split(contexts));
		Matcher m = pattern.matcher(contexts);
		int i = 1;
		String str = null;
		while (m.find()) {
			str = strList.get(i);
			contextsMap.put(contexts.substring(m.start(), m.end() - 1), str.trim());
			i++;
		}
		return contextsMap;
	}

	private final static Pattern getTcCaseIdPattern = Pattern.compile("(-\\d)");

	public static Long getTcCaseId(String contexts) {
		Long testCaseId = null;
		List<String> strList = Arrays.asList(getTcCaseIdPattern.split(contexts));

		List<String> tempList = new ArrayList<String>();
		Matcher m = getTcCaseIdPattern.matcher(contexts);
		int i = 1;
		while (m.find()) {
			tempList.add(contexts.substring(m.start() + 1, m.end()) + strList.get(i));
			i++;
		}
		if (tempList.size() > 0) {
			testCaseId = Long.valueOf(tempList.get(tempList.size() - 1));
		}
		return testCaseId;
	}

}
