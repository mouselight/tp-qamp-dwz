package com.bill99.testmp.framework.hession;

public interface QaInterface {

	/**
	 * 保存QA测试结果
	 * 
	 * @param releaseId
	 *            上线项目ID
	 * @param qaFileSvnUrl
	 *            QA测试报告文件的svn url
	 * @param qaTestResult
	 *            QA测试项目结果：0--待确认，1--测试通过，2测试不通过
	 * @param qaMemo
	 *            QA备注
	 * @param submitUserId       
	 *            提交者id，可为空 必须是域用户帐号
	 * @return 成功更新返回true，否则返回false
	 */

	public boolean saveQaTestResult(String releaseId, String qaFileSvnUrl, int qaTestResult, String qaMemo, String submitUserId);

	public  boolean saveQaPrdTestResult(String releaseId, String qaFileSvnUrl, int qaTestResult, String qaMemo, String submitUserId);
}
