package com.bill99.testmp.framework.utils;

import java.util.Map;

import com.bill99.testmp.framework.domain.PrimaryDataService;

public class PrimaryDataFactory {

	public static Map<String, String> teamMap;//业务组
	public static Map<String, String> issueStatusMap;//需求状态
	public static Map<String, String> issueTypeMap;//需求类型
	public static Map<String, String> priorityMap;//优先级
	public static Map<Integer, String> assoTcStatusMap;//TC状态

	private PrimaryDataService primaryDataService;

	public void dataInit() {
		if (null == teamMap || teamMap.keySet().size() == 0) {
			teamMap = primaryDataService.initTeamMap();
		}
		if (null == issueStatusMap || issueStatusMap.keySet().size() == 0) {
			issueStatusMap = primaryDataService.initIssueStatus();
		}
		if (null == issueTypeMap || issueTypeMap.keySet().size() == 0) {
			issueTypeMap = primaryDataService.initIssueType();
		}
		if (null == assoTcStatusMap || assoTcStatusMap.keySet().size() == 0) {
			assoTcStatusMap = primaryDataService.initAssoTcStatus();
		}
		if (null == priorityMap || priorityMap.keySet().size() == 0) {
			priorityMap = primaryDataService.initPriority();
		}
	}

	public void setPrimaryDataService(PrimaryDataService primaryDataService) {
		this.primaryDataService = primaryDataService;
	}
}
