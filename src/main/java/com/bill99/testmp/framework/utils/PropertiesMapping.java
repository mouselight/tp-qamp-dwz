package com.bill99.testmp.framework.utils;

public class PropertiesMapping {

	public String svnReportUrl;
	public String svnReportPath;
	public String hessianUrl;
	public String svnUser;
	public String svnPwd;

	public String reportTempDir;
	public String reportTempJnaDir;
	public String comBill99;
	public String svnCommitDescription;
	public String snvProjectChangeList;

	public String getSvnReportUrl() {
		return svnReportUrl;
	}

	public void setSvnReportUrl(String svnReportUrl) {
		this.svnReportUrl = svnReportUrl;
	}

	public String getSvnReportPath() {
		return svnReportPath;
	}

	public void setSvnReportPath(String svnReportPath) {
		this.svnReportPath = svnReportPath;
	}

	public String getHessianUrl() {
		return hessianUrl;
	}

	public void setHessianUrl(String hessianUrl) {
		this.hessianUrl = hessianUrl;
	}

	public String getSvnUser() {
		return svnUser;
	}

	public void setSvnUser(String svnUser) {
		this.svnUser = svnUser;
	}

	public String getSvnPwd() {
		return svnPwd;
	}

	public void setSvnPwd(String svnPwd) {
		this.svnPwd = svnPwd;
	}

	public String getReportTempDir() {
		return reportTempDir;
	}

	public void setReportTempDir(String reportTempDir) {
		this.reportTempDir = reportTempDir;
	}

	public String getReportTempJnaDir() {
		return reportTempJnaDir;
	}

	public void setReportTempJnaDir(String reportTempJnaDir) {
		this.reportTempJnaDir = reportTempJnaDir;
	}

	public String getComBill99() {
		return comBill99;
	}

	public void setComBill99(String comBill99) {
		this.comBill99 = comBill99;
	}

	public String getSvnCommitDescription() {
		return svnCommitDescription;
	}

	public void setSvnCommitDescription(String svnCommitDescription) {
		this.svnCommitDescription = svnCommitDescription;
	}

	public String getSnvProjectChangeList() {
		return snvProjectChangeList;
	}

	public void setSnvProjectChangeList(String snvProjectChangeList) {
		this.snvProjectChangeList = snvProjectChangeList;
	}
}
