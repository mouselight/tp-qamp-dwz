package com.bill99.testmp.framework.vo;

import java.util.List;

public class Definition {
	private List<Style> style;

	public List<Style> getStyle() {
		return style;
	}

	public void setStyle(List<Style> style) {
		this.style = style;
	}

}
