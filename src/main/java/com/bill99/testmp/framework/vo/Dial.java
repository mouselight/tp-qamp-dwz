package com.bill99.testmp.framework.vo;

public class Dial {

	private String value;
	private String borderAlpha;
	private String bgColor;
	private String baseWidth;
	private String topWidth;
	private String radius;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getBorderAlpha() {
		return borderAlpha;
	}

	public void setBorderAlpha(String borderAlpha) {
		this.borderAlpha = borderAlpha;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getBaseWidth() {
		return baseWidth;
	}

	public void setBaseWidth(String baseWidth) {
		this.baseWidth = baseWidth;
	}

	public String getTopWidth() {
		return topWidth;
	}

	public void setTopWidth(String topWidth) {
		this.topWidth = topWidth;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

}
