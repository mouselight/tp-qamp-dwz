package com.bill99.testmp.framework.vo;

public class Apply {
	private String toObject;
	private String styles;

	public String getToObject() {
		return toObject;
	}

	public void setToObject(String toObject) {
		this.toObject = toObject;
	}

	public String getStyles() {
		return styles;
	}

	public void setStyles(String styles) {
		this.styles = styles;
	}

}
