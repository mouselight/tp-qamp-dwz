package com.bill99.testmp.framework.vo;

import java.util.List;

public class Dataset {
	/**
	 * <dataset seriesName='Offline Marketing' color='1D8BD1'
	 * anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>
	 */
	private String seriesName;
	private String color;
	private String showValue;
	private String lineThickness;
	private String yaxismaxvalue;
	private String anchorSides;
	private String anchorRadius;
	private String anchorAlpha;
	private String parentYAxis;
	private String renderAs;
	private String showYAxisValues;
	private List<Set> set;

	private String anchorBorderColor;
	private String anchorBgColor;

	private String showValues;

	
	
	public String getShowYAxisValues() {
		return showYAxisValues;
	}

	public void setShowYAxisValues(String showYAxisValues) {
		this.showYAxisValues = showYAxisValues;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getShowValue() {
		return showValue;
	}

	public void setShowValue(String showValue) {
		this.showValue = showValue;
	}

	public String getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(String lineThickness) {
		this.lineThickness = lineThickness;
	}

	public String getYaxismaxvalue() {
		return yaxismaxvalue;
	}

	public void setYaxismaxvalue(String yaxismaxvalue) {
		this.yaxismaxvalue = yaxismaxvalue;
	}

	public String getAnchorSides() {
		return anchorSides;
	}

	public void setAnchorSides(String anchorSides) {
		this.anchorSides = anchorSides;
	}

	public String getAnchorRadius() {
		return anchorRadius;
	}

	public void setAnchorRadius(String anchorRadius) {
		this.anchorRadius = anchorRadius;
	}

	public String getAnchorAlpha() {
		return anchorAlpha;
	}

	public void setAnchorAlpha(String anchorAlpha) {
		this.anchorAlpha = anchorAlpha;
	}

	public String getParentYAxis() {
		return parentYAxis;
	}

	public void setParentYAxis(String parentYAxis) {
		this.parentYAxis = parentYAxis;
	}

	public List<Set> getSet() {
		return set;
	}

	public void setSet(List<Set> set) {
		this.set = set;
	}

	public String getAnchorBorderColor() {
		return anchorBorderColor;
	}

	public void setAnchorBorderColor(String anchorBorderColor) {
		this.anchorBorderColor = anchorBorderColor;
	}

	public String getAnchorBgColor() {
		return anchorBgColor;
	}

	public void setAnchorBgColor(String anchorBgColor) {
		this.anchorBgColor = anchorBgColor;
	}

	public String getShowValues() {
		return showValues;
	}

	public void setShowValues(String showValues) {
		this.showValues = showValues;
	}

	public String getRenderAs() {
		return renderAs;
	}

	public void setRenderAs(String renderAs) {
		this.renderAs = renderAs;
	}

}
