package com.bill99.testmp.framework.vo;

import java.util.List;

public class ColorRange {

	List<Color> color;

	public List<Color> getColor() {
		return color;
	}

	public void setColor(List<Color> color) {
		this.color = color;
	}

}
