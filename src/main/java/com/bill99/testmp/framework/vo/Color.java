package com.bill99.testmp.framework.vo;

public class Color {
	private String minValue;
	private String maxValue;
	private String code;

	public Color(String minValue, String maxValue, String code) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.code = code;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
