package com.bill99.testmp.framework.vo;

public class Style {
	private String name;
	private String type;
	private String param;
	private String start;
	private String duration;
	private String alpha;
	private String bgColor;
	private String face;
	private String size;
	private String color;
	private String bold;

	private String lineThickness;// 折线的厚度
	private String anchorRadius;// 折线节点半径，数字
	private String anchorBgAlpha;// 折线节点透明度，[0-100]
	private String anchorBgColor;// 折线节点填充颜色，6位16进制颜色值
	private String anchorBorderColor;// 折线节点边框颜色，6位16进制颜色值

	public String getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(String lineThickness) {
		this.lineThickness = lineThickness;
	}

	public String getAnchorRadius() {
		return anchorRadius;
	}

	public void setAnchorRadius(String anchorRadius) {
		this.anchorRadius = anchorRadius;
	}

	public String getAnchorBgAlpha() {
		return anchorBgAlpha;
	}

	public void setAnchorBgAlpha(String anchorBgAlpha) {
		this.anchorBgAlpha = anchorBgAlpha;
	}

	public String getAnchorBgColor() {
		return anchorBgColor;
	}

	public void setAnchorBgColor(String anchorBgColor) {
		this.anchorBgColor = anchorBgColor;
	}

	public String getAnchorBorderColor() {
		return anchorBorderColor;
	}

	public void setAnchorBorderColor(String anchorBorderColor) {
		this.anchorBorderColor = anchorBorderColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getAlpha() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBold() {
		return bold;
	}

	public void setBold(String bold) {
		this.bold = bold;
	}

}
