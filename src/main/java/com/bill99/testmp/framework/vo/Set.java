package com.bill99.testmp.framework.vo;

public class Set {

	private String label;

	private String value;

	private String color;

	private String alpha;

	public Set() {
	}

	public Set(String value) {
		this.value = value;
	}

	public Set(String label, String value) {
		this.label = label;
		this.value = value;
	}

	public Set(String label, String value, String color) {
		this.label = label;
		this.value = value;
		this.color = color;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getAlpha() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

}
