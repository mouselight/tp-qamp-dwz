package com.bill99.testmp.framework.vo;

public class Category {

	private String label;

	public Category(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
