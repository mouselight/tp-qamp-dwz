package com.bill99.testmp.framework.vo;

import java.util.List;

public class Chart {
	private List<Category> categories;
	private List<Dataset> dataset;
	private List<Set> set;
	private Dials dials;
	private ColorRange colorRange;
	private Styles styles;

	private String subcaption;
	private String font;
	private String palette;// number(1-5) 使用默认的调色板
	private String labelDisplay;// WRAP,STAGGER,ROTATE or NONE )
								// 标签的呈现方式（超长屏蔽、折行、倾斜还是不显示）
	private String slantLabels;// x轴说明文字是否倾斜（1/0）
	private String areaOverColumns;
	private String useRoundEdges;// 平面化效果
	private String shownames;
	private String decimals;
	private String legendBorderAlpha;
	private String divLineIsDashed;
	private String labelStep;
	private String numvdivlines;
	private String chartRightMargin;
	private String bgAngle;
	private String bgAlpha;
	private String lineColor;

	private String showBorder;
	private String borderColor;
	private String borderThickness;
	private String borderAlpha;
	private String showShadow;
	private String drawAnchors;
	private String anchorSides;
	private String showAnchors;
	private String bgSWF;
	private String rotateYAxisName;
	// <chart numberScaleValue='60,60,24,7' numberScaleUnit='min,hr,day,wk' >
	private String numberScaleUnit;// 'K,M,B'
	private String numberScaleValue;// numberScaleValue='1000,1000,1000'

	private String showDivLineSecondaryValue;
	private String yAxisValuesStep;
	private String showYAxisValues;
	private String showSYAxisValues;
	private String PYAxisMaxValue;
	private String showLabels;
	

	public String getShowLabels() {
		return showLabels;
	}

	public void setShowLabels(String showLabels) {
		this.showLabels = showLabels;
	}

	public String getPYAxisMaxValue() {
		return PYAxisMaxValue;
	}

	public void setPYAxisMaxValue(String pYAxisMaxValue) {
		PYAxisMaxValue = pYAxisMaxValue;
	}

	public String getShowSYAxisValues() {
		return showSYAxisValues;
	}

	public void setShowSYAxisValues(String showSYAxisValues) {
		this.showSYAxisValues = showSYAxisValues;
	}

	public String getShowYAxisValues() {
		return showYAxisValues;
	}

	public void setShowYAxisValues(String showYAxisValues) {
		this.showYAxisValues = showYAxisValues;
	}

	public String getShowDivLineSecondaryValue() {
		return showDivLineSecondaryValue;
	}

	public void setShowDivLineSecondaryValue(String showDivLineSecondaryValue) {
		this.showDivLineSecondaryValue = showDivLineSecondaryValue;
	}

	public String getyAxisValuesStep() {
		return yAxisValuesStep;
	}

	public void setyAxisValuesStep(String yAxisValuesStep) {
		this.yAxisValuesStep = yAxisValuesStep;
	}

	public String getNumberScaleValue() {
		return numberScaleValue;
	}

	public void setNumberScaleValue(String numberScaleValue) {
		this.numberScaleValue = numberScaleValue;
	}

	public String getNumberScaleUnit() {
		return numberScaleUnit;
	}

	public void setNumberScaleUnit(String numberScaleUnit) {
		this.numberScaleUnit = numberScaleUnit;
	}

	public String getRotateYAxisName() {
		return rotateYAxisName;
	}

	public void setRotateYAxisName(String rotateYAxisName) {
		this.rotateYAxisName = rotateYAxisName;
	}

	public String getBgSWF() {
		return bgSWF;
	}

	public void setBgSWF(String bgSWF) {
		this.bgSWF = bgSWF;
	}

	public String getShowAnchors() {
		return showAnchors;
	}

	public void setShowAnchors(String showAnchors) {
		this.showAnchors = showAnchors;
	}

	public String getAnchorSides() {
		return anchorSides;
	}

	public void setAnchorSides(String anchorSides) {
		this.anchorSides = anchorSides;
	}

	public String getDrawAnchors() {
		return drawAnchors;
	}

	public void setDrawAnchors(String drawAnchors) {
		this.drawAnchors = drawAnchors;
	}

	public String getShowShadow() {
		return showShadow;
	}

	public void setShowShadow(String showShadow) {
		this.showShadow = showShadow;
	}

	public String getShowBorder() {
		return showBorder;
	}

	public void setShowBorder(String showBorder) {
		this.showBorder = showBorder;
	}

	public String getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}

	public String getBorderThickness() {
		return borderThickness;
	}

	public void setBorderThickness(String borderThickness) {
		this.borderThickness = borderThickness;
	}

	public String getBorderAlpha() {
		return borderAlpha;
	}

	public void setBorderAlpha(String borderAlpha) {
		this.borderAlpha = borderAlpha;
	}

	private String background;

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	// 仪表
	private String fillAngle;
	private String upperLimit;
	private String lowerLimit;
	private String majorTMNumber;
	private String majorTMHeight;
	private String showGaugeBorder;
	private String gaugeOuterRadius;
	private String gaugeOriginX;
	private String gaugeOriginY;
	private String gaugeInnerRadius;
	private String displayValueDistance;
	private String tickMarkDecimalPrecision;
	private String pivotBorderThickness;
	private String pivotFillMix;
	private String pivotRadius;
	private String showPivotBorder;
	private String pivotBorderColor;
	private String numVisiblePlot;

	private String canvasBorderAlpha;

	private String backgroundImage;

	// 功能特性
	private String animation;// 是否动画显示数据，默认为1(True)
	private String showNames;// 是否显示横向坐标轴(x轴)标签名称
	private String rotateNames;// 是否旋转显示标签，默认为0(False):横向显示
	private String showValues;// 是否在图表显示对应的数据值，默认为1(True)
	private String yAxisMinValue;// 指定纵轴(y轴)最小值，数字
	private String yAxisMaxValue;// 指定纵轴(y轴)最大值，数字
	private String showLimits;// 是否显示图表限值(y轴最大、最小值)，默认为1(True)

	private String xAxisMinValue;
	private String xAxisMaxValue;

	// 图表标题和轴名称
	private String caption;// 图表主标题
	private String subCaption;// 图表副标题
	private String xAxisName;// 横向坐标轴(x轴)名称
	private String yAxisName;// 纵向坐标轴(y轴)名称

	// 图表和画布的样式
	private String bgColor;// 图表背景色，6位16进制颜色值
	private String canvasBgColor;// 画布背景色，6位16进制颜色值
	private String canvasBgAlpha;// 画布透明度，[0-100]
	private String canvasBorderColor;// 画布边框颜色，6位16进制颜色值
	private String canvasBorderThickness;// 画布边框厚度，[0-100]
	private String shadowAlpha;// 投影透明度，[0-100]
	private String showLegend;// 是否显示系列名，默认为1(True)

	// 字体属性
	private String baseFont;// 图表字体样式
	private String baseFontSize;// 图表字体大小
	private String baseFontColor;// 图表字体颜色，6位16进制颜色值
	private String outCnvBaseFont;// 图表画布以外的字体样式
	private String outCnvBaseFontSize;// 图表画布以外的字体大小
	private String outCnvBaseFontColor;// 图表画布以外的字体颜色，6位16进制颜色值

	// 分区线和网格
	private String numDivLines;// 画布内部水平分区线条数，数字
	private String divLineColor;// 水平分区线颜色，6位16进制颜色值
	private String divLineThickness;// 水平分区线厚度，[1-5]
	private String divLineAlpha;// 水平分区线透明度，[0-100]
	private String showAlternateHGridColor;// 是否在横向网格带交替的颜色，默认为0(False)
	private String alternateHGridColor;// 横向网格带交替的颜色，6位16进制颜色值
	private String alternateHGridAlpha;// 横向网格带的透明度，[0-100]
	private String showDivLinues;// 是否显示Div行的值，默认？？
	private String numVDivLines;// 画布内部垂直分区线条数，数字
	private String vDivLineColor;// 垂直分区线颜色，6位16进制颜色值
	private String vDivLineThickness;// 垂直分区线厚度，[1-5]
	private String vDivLineAlpha;// 垂直分区线透明度，[0-100]
	private String showAlternateVGridColor;// 是否在纵向网格带交替的颜色，默认为0(False)
	private String alternateVGridColor;// 纵向网格带交替的颜色，6位16进制颜色值
	private String alternateVGridAlpha;// 纵向网格带的透明度，[0-100]

	// 数字格式
	private String numberPrefix;// 增加数字前缀
	private String numberSuffix;// 增加数字后缀 % 为 '%25'
	private String formatNumberScale;// 是否格式化数字,默认为1(True),自动的给你的数字加上K（千）或M（百万）；若取0,则不加K或M
	private String decimalPrecision;// 指定小数位的位数，[0-10] 例如：='0' 取整
	private String divLineDecimalPrecision;// 指定水平分区线的值小数位的位数，[0-10]
	private String limitsDecimalPrecision;// 指定y轴最大、最小值的小数位的位数，[0-10]
	private String formatNumber;// 逗号来分隔数字(千位，百万位),默认为1(True)；若取0,则不加分隔符
	private String decimalSeparator;// 指定小数分隔符,默认为'.'
	private String thousandSeparator;// 指定千分位分隔符,默认为','

	// Tool-tip/Hover标题
	private String showhovercap;// 是否显示悬停说明框，默认为1(True)
	private String hoverCapBgColor;// 悬停说明框背景色，6位16进制颜色值
	private String hoverCapBorderColor;// 悬停说明框边框颜色，6位16进制颜色值
	private String hoverCapSepChar;// 指定悬停说明框内值与值之间分隔符,默认为','

	// 折线图的参数
	private String lineThickness;// 折线的厚度
	private String anchorRadius;// 折线节点半径，数字
	private String anchorBgAlpha;// 折线节点透明度，[0-100]
	private String anchorBgColor;// 折线节点填充颜色，6位16进制颜色值
	private String anchorBorderColor;// 折线节点边框颜色，6位16进制颜色值

	// Set标签使用的参数
	private String value;// 数据值
	private String color;// 颜色
	private String link;// 链接（本窗口打开[Url]，新窗口打开[n-Url]，调用JS函数[JavaScript:函数]）
	private String name;// 横向坐标轴标签名称

	public String getxAxisMinValue() {
		return xAxisMinValue;
	}

	public void setxAxisMinValue(String xAxisMinValue) {
		this.xAxisMinValue = xAxisMinValue;
	}

	public String getxAxisMaxValue() {
		return xAxisMaxValue;
	}

	public void setxAxisMaxValue(String xAxisMaxValue) {
		this.xAxisMaxValue = xAxisMaxValue;
	}

	public String getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public String getCanvasBorderAlpha() {
		return canvasBorderAlpha;
	}

	public void setCanvasBorderAlpha(String canvasBorderAlpha) {
		this.canvasBorderAlpha = canvasBorderAlpha;
	}

	public String getShowLegend() {
		return showLegend;
	}

	public void setShowLegend(String showLegend) {
		this.showLegend = showLegend;
	}

	public String getyAxisMinValue() {
		return yAxisMinValue;
	}

	public void setyAxisMinValue(String yAxisMinValue) {
		this.yAxisMinValue = yAxisMinValue;
	}

	public String getCanvasBorderColor() {
		return canvasBorderColor;
	}

	public void setCanvasBorderColor(String canvasBorderColor) {
		this.canvasBorderColor = canvasBorderColor;
	}

	public String getBaseFontColor() {
		return baseFontColor;
	}

	public void setBaseFontColor(String baseFontColor) {
		this.baseFontColor = baseFontColor;
	}

	public String getLineColor() {
		return lineColor;
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public String getxAxisName() {
		return xAxisName;
	}

	public void setxAxisName(String xAxisName) {
		this.xAxisName = xAxisName;
	}

	public String getyAxisName() {
		return yAxisName;
	}

	public void setyAxisName(String yAxisName) {
		this.yAxisName = yAxisName;
	}

	public String getAnchorBgColor() {
		return anchorBgColor;
	}

	public void setAnchorBgColor(String anchorBgColor) {
		this.anchorBgColor = anchorBgColor;
	}

	public String getAnchorBgAlpha() {
		return anchorBgAlpha;
	}

	public void setAnchorBgAlpha(String anchorBgAlpha) {
		this.anchorBgAlpha = anchorBgAlpha;
	}

	public String getShowAlternateVGridColor() {
		return showAlternateVGridColor;
	}

	public void setShowAlternateVGridColor(String showAlternateVGridColor) {
		this.showAlternateVGridColor = showAlternateVGridColor;
	}

	public String getNumVisiblePlot() {
		return numVisiblePlot;
	}

	public void setNumVisiblePlot(String numVisiblePlot) {
		this.numVisiblePlot = numVisiblePlot;
	}

	public ColorRange getColorRange() {
		return colorRange;
	}

	public void setColorRange(ColorRange colorRange) {
		this.colorRange = colorRange;
	}

	public Dials getDials() {
		return dials;
	}

	public void setDials(Dials dials) {
		this.dials = dials;
	}

	public String getPivotRadius() {
		return pivotRadius;
	}

	public void setPivotRadius(String pivotRadius) {
		this.pivotRadius = pivotRadius;
	}

	public String getShowPivotBorder() {
		return showPivotBorder;
	}

	public void setShowPivotBorder(String showPivotBorder) {
		this.showPivotBorder = showPivotBorder;
	}

	public String getPivotBorderColor() {
		return pivotBorderColor;
	}

	public void setPivotBorderColor(String pivotBorderColor) {
		this.pivotBorderColor = pivotBorderColor;
	}

	public String getLimitsDecimalPrecision() {
		return limitsDecimalPrecision;
	}

	public void setLimitsDecimalPrecision(String limitsDecimalPrecision) {
		this.limitsDecimalPrecision = limitsDecimalPrecision;
	}

	public String getNumberSuffix() {
		return numberSuffix;
	}

	public void setNumberSuffix(String numberSuffix) {
		this.numberSuffix = numberSuffix;
	}

	public String getPalette() {
		return palette;
	}

	public void setPalette(String palette) {
		this.palette = palette;
	}

	public String getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(String lineThickness) {
		this.lineThickness = lineThickness;
	}

	public String getShowValues() {
		return showValues;
	}

	public void setShowValues(String showValues) {
		this.showValues = showValues;
	}

	public String getLabelDisplay() {
		return labelDisplay;
	}

	public void setLabelDisplay(String labelDisplay) {
		this.labelDisplay = labelDisplay;
	}

	public String getSlantLabels() {
		return slantLabels;
	}

	public void setSlantLabels(String slantLabels) {
		this.slantLabels = slantLabels;
	}

	public String getAreaOverColumns() {
		return areaOverColumns;
	}

	public void setAreaOverColumns(String areaOverColumns) {
		this.areaOverColumns = areaOverColumns;
	}

	public String getUseRoundEdges() {
		return useRoundEdges;
	}

	public void setUseRoundEdges(String useRoundEdges) {
		this.useRoundEdges = useRoundEdges;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Dataset> getDataset() {
		return dataset;
	}

	public void setDataset(List<Dataset> dataset) {
		this.dataset = dataset;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getSubcaption() {
		return subcaption;
	}

	public void setSubcaption(String subcaption) {
		this.subcaption = subcaption;
	}

	public String getFormatNumberScale() {
		return formatNumberScale;
	}

	public void setFormatNumberScale(String formatNumberScale) {
		this.formatNumberScale = formatNumberScale;
	}

	public String getAnchorRadius() {
		return anchorRadius;
	}

	public void setAnchorRadius(String anchorRadius) {
		this.anchorRadius = anchorRadius;
	}

	public String getDivLineAlpha() {
		return divLineAlpha;
	}

	public void setDivLineAlpha(String divLineAlpha) {
		this.divLineAlpha = divLineAlpha;
	}

	public String getDivLineColor() {
		return divLineColor;
	}

	public void setDivLineColor(String divLineColor) {
		this.divLineColor = divLineColor;
	}

	public String getDivLineIsDashed() {
		return divLineIsDashed;
	}

	public void setDivLineIsDashed(String divLineIsDashed) {
		this.divLineIsDashed = divLineIsDashed;
	}

	public String getShowAlternateHGridColor() {
		return showAlternateHGridColor;
	}

	public void setShowAlternateHGridColor(String showAlternateHGridColor) {
		this.showAlternateHGridColor = showAlternateHGridColor;
	}

	public String getAlternateHGridAlpha() {
		return alternateHGridAlpha;
	}

	public void setAlternateHGridAlpha(String alternateHGridAlpha) {
		this.alternateHGridAlpha = alternateHGridAlpha;
	}

	public String getAlternateHGridColor() {
		return alternateHGridColor;
	}

	public void setAlternateHGridColor(String alternateHGridColor) {
		this.alternateHGridColor = alternateHGridColor;
	}

	public String getShadowAlpha() {
		return shadowAlpha;
	}

	public void setShadowAlpha(String shadowAlpha) {
		this.shadowAlpha = shadowAlpha;
	}

	public String getLabelStep() {
		return labelStep;
	}

	public void setLabelStep(String labelStep) {
		this.labelStep = labelStep;
	}

	public String getNumvdivlines() {
		return numvdivlines;
	}

	public void setNumvdivlines(String numvdivlines) {
		this.numvdivlines = numvdivlines;
	}

	public String getChartRightMargin() {
		return chartRightMargin;
	}

	public void setChartRightMargin(String chartRightMargin) {
		this.chartRightMargin = chartRightMargin;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getBgAngle() {
		return bgAngle;
	}

	public void setBgAngle(String bgAngle) {
		this.bgAngle = bgAngle;
	}

	public String getBgAlpha() {
		return bgAlpha;
	}

	public void setBgAlpha(String bgAlpha) {
		this.bgAlpha = bgAlpha;
	}

	public String getShownames() {
		return shownames;
	}

	public void setShownames(String shownames) {
		this.shownames = shownames;
	}

	public String getDecimals() {
		return decimals;
	}

	public void setDecimals(String decimals) {
		this.decimals = decimals;
	}

	public String getNumberPrefix() {
		return numberPrefix;
	}

	public void setNumberPrefix(String numberPrefix) {
		this.numberPrefix = numberPrefix;
	}

	public String getLegendBorderAlpha() {
		return legendBorderAlpha;
	}

	public void setLegendBorderAlpha(String legendBorderAlpha) {
		this.legendBorderAlpha = legendBorderAlpha;
	}

	public String getAnimation() {
		return animation;
	}

	public void setAnimation(String animation) {
		this.animation = animation;
	}

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}

	public List<Set> getSet() {
		return set;
	}

	public void setSet(List<Set> set) {
		this.set = set;
	}

	public String getBaseFontSize() {
		return baseFontSize;
	}

	public void setBaseFontSize(String baseFontSize) {
		this.baseFontSize = baseFontSize;
	}

	public String getyAxisMaxValue() {
		return yAxisMaxValue;
	}

	public void setyAxisMaxValue(String yAxisMaxValue) {
		this.yAxisMaxValue = yAxisMaxValue;
	}

	public String getFillAngle() {
		return fillAngle;
	}

	public void setFillAngle(String fillAngle) {
		this.fillAngle = fillAngle;
	}

	public String getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(String upperLimit) {
		this.upperLimit = upperLimit;
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getMajorTMNumber() {
		return majorTMNumber;
	}

	public void setMajorTMNumber(String majorTMNumber) {
		this.majorTMNumber = majorTMNumber;
	}

	public String getMajorTMHeight() {
		return majorTMHeight;
	}

	public void setMajorTMHeight(String majorTMHeight) {
		this.majorTMHeight = majorTMHeight;
	}

	public String getShowGaugeBorder() {
		return showGaugeBorder;
	}

	public void setShowGaugeBorder(String showGaugeBorder) {
		this.showGaugeBorder = showGaugeBorder;
	}

	public String getGaugeOuterRadius() {
		return gaugeOuterRadius;
	}

	public void setGaugeOuterRadius(String gaugeOuterRadius) {
		this.gaugeOuterRadius = gaugeOuterRadius;
	}

	public String getGaugeOriginX() {
		return gaugeOriginX;
	}

	public void setGaugeOriginX(String gaugeOriginX) {
		this.gaugeOriginX = gaugeOriginX;
	}

	public String getGaugeOriginY() {
		return gaugeOriginY;
	}

	public void setGaugeOriginY(String gaugeOriginY) {
		this.gaugeOriginY = gaugeOriginY;
	}

	public String getGaugeInnerRadius() {
		return gaugeInnerRadius;
	}

	public void setGaugeInnerRadius(String gaugeInnerRadius) {
		this.gaugeInnerRadius = gaugeInnerRadius;
	}

	public String getDisplayValueDistance() {
		return displayValueDistance;
	}

	public void setDisplayValueDistance(String displayValueDistance) {
		this.displayValueDistance = displayValueDistance;
	}

	public String getDecimalPrecision() {
		return decimalPrecision;
	}

	public void setDecimalPrecision(String decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}

	public String getTickMarkDecimalPrecision() {
		return tickMarkDecimalPrecision;
	}

	public void setTickMarkDecimalPrecision(String tickMarkDecimalPrecision) {
		this.tickMarkDecimalPrecision = tickMarkDecimalPrecision;
	}

	public String getPivotBorderThickness() {
		return pivotBorderThickness;
	}

	public void setPivotBorderThickness(String pivotBorderThickness) {
		this.pivotBorderThickness = pivotBorderThickness;
	}

	public String getPivotFillMix() {
		return pivotFillMix;
	}

	public void setPivotFillMix(String pivotFillMix) {
		this.pivotFillMix = pivotFillMix;
	}

	public String getShowNames() {
		return showNames;
	}

	public void setShowNames(String showNames) {
		this.showNames = showNames;
	}

	public String getRotateNames() {
		return rotateNames;
	}

	public void setRotateNames(String rotateNames) {
		this.rotateNames = rotateNames;
	}

	public String getShowLimits() {
		return showLimits;
	}

	public void setShowLimits(String showLimits) {
		this.showLimits = showLimits;
	}

	public String getSubCaption() {
		return subCaption;
	}

	public void setSubCaption(String subCaption) {
		this.subCaption = subCaption;
	}

	public String getCanvasBgColor() {
		return canvasBgColor;
	}

	public void setCanvasBgColor(String canvasBgColor) {
		this.canvasBgColor = canvasBgColor;
	}

	public String getCanvasBgAlpha() {
		return canvasBgAlpha;
	}

	public void setCanvasBgAlpha(String canvasBgAlpha) {
		this.canvasBgAlpha = canvasBgAlpha;
	}

	public String getCanvasBorderThickness() {
		return canvasBorderThickness;
	}

	public void setCanvasBorderThickness(String canvasBorderThickness) {
		this.canvasBorderThickness = canvasBorderThickness;
	}

	public String getBaseFont() {
		return baseFont;
	}

	public void setBaseFont(String baseFont) {
		this.baseFont = baseFont;
	}

	public String getOutCnvBaseFont() {
		return outCnvBaseFont;
	}

	public void setOutCnvBaseFont(String outCnvBaseFont) {
		this.outCnvBaseFont = outCnvBaseFont;
	}

	public String getOutCnvBaseFontSize() {
		return outCnvBaseFontSize;
	}

	public void setOutCnvBaseFontSize(String outCnvBaseFontSize) {
		this.outCnvBaseFontSize = outCnvBaseFontSize;
	}

	public String getOutCnvBaseFontColor() {
		return outCnvBaseFontColor;
	}

	public void setOutCnvBaseFontColor(String outCnvBaseFontColor) {
		this.outCnvBaseFontColor = outCnvBaseFontColor;
	}

	public String getNumDivLines() {
		return numDivLines;
	}

	public void setNumDivLines(String numDivLines) {
		this.numDivLines = numDivLines;
	}

	public String getDivLineThickness() {
		return divLineThickness;
	}

	public void setDivLineThickness(String divLineThickness) {
		this.divLineThickness = divLineThickness;
	}

	public String getShowDivLinues() {
		return showDivLinues;
	}

	public void setShowDivLinues(String showDivLinues) {
		this.showDivLinues = showDivLinues;
	}

	public String getNumVDivLines() {
		return numVDivLines;
	}

	public void setNumVDivLines(String numVDivLines) {
		this.numVDivLines = numVDivLines;
	}

	public String getvDivLineColor() {
		return vDivLineColor;
	}

	public void setvDivLineColor(String vDivLineColor) {
		this.vDivLineColor = vDivLineColor;
	}

	public String getvDivLineThickness() {
		return vDivLineThickness;
	}

	public void setvDivLineThickness(String vDivLineThickness) {
		this.vDivLineThickness = vDivLineThickness;
	}

	public String getvDivLineAlpha() {
		return vDivLineAlpha;
	}

	public void setvDivLineAlpha(String vDivLineAlpha) {
		this.vDivLineAlpha = vDivLineAlpha;
	}

	public String getAlternateVGridColor() {
		return alternateVGridColor;
	}

	public void setAlternateVGridColor(String alternateVGridColor) {
		this.alternateVGridColor = alternateVGridColor;
	}

	public String getAlternateVGridAlpha() {
		return alternateVGridAlpha;
	}

	public void setAlternateVGridAlpha(String alternateVGridAlpha) {
		this.alternateVGridAlpha = alternateVGridAlpha;
	}

	public String getDivLineDecimalPrecision() {
		return divLineDecimalPrecision;
	}

	public void setDivLineDecimalPrecision(String divLineDecimalPrecision) {
		this.divLineDecimalPrecision = divLineDecimalPrecision;
	}

	public String getFormatNumber() {
		return formatNumber;
	}

	public void setFormatNumber(String formatNumber) {
		this.formatNumber = formatNumber;
	}

	public String getDecimalSeparator() {
		return decimalSeparator;
	}

	public void setDecimalSeparator(String decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	public String getThousandSeparator() {
		return thousandSeparator;
	}

	public void setThousandSeparator(String thousandSeparator) {
		this.thousandSeparator = thousandSeparator;
	}

	public String getShowhovercap() {
		return showhovercap;
	}

	public void setShowhovercap(String showhovercap) {
		this.showhovercap = showhovercap;
	}

	public String getHoverCapBgColor() {
		return hoverCapBgColor;
	}

	public void setHoverCapBgColor(String hoverCapBgColor) {
		this.hoverCapBgColor = hoverCapBgColor;
	}

	public String getHoverCapBorderColor() {
		return hoverCapBorderColor;
	}

	public void setHoverCapBorderColor(String hoverCapBorderColor) {
		this.hoverCapBorderColor = hoverCapBorderColor;
	}

	public String getHoverCapSepChar() {
		return hoverCapSepChar;
	}

	public void setHoverCapSepChar(String hoverCapSepChar) {
		this.hoverCapSepChar = hoverCapSepChar;
	}

	public String getAnchorBorderColor() {
		return anchorBorderColor;
	}

	public void setAnchorBorderColor(String anchorBorderColor) {
		this.anchorBorderColor = anchorBorderColor;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Styles getStyles() {
		return styles;
	}

	public void setStyles(Styles styles) {
		this.styles = styles;
	}

}

// FusionCharts 的 XML标签属性有一下四种数据类型
// * Boolean - 布尔类型，只能为1或者0。例如：<graph showNames=’1′ >
// * Number - 数字类型，只能为数字。例如：<graph yAxisMaxValue=’200′ >
// * String - 字符串类型，只能为字符串。例如： <graph caption=’My Chart’ >
// * Hex Color Code - 十六进制颜色代码，前边没有’#’.例如： <graph bgColor=’FFFFDD’ >
//
// XML中的标签和属性有：
// <graph> 所具有的属性
// flash背景参数：
// * bgColor=”HexColorCode” : 设置flash的背景颜色
// * bgAlpha=”NumericalValue(0-100)” : 设置背景的透明度
// * bgSWF=”Path of SWF File” : 设置一个外部的Flash 为flash的背景
// 图表背景参数：
// * canvasBgColor=”HexColorCode” : 设置图表背景的颜色
// * canvasBaseColor=”HexColorCode” : 设置图表基部的颜色
// * canvasBaseDepth=”Numerical Value” : 设置图表基部的高度
// * canvasBgDepth=”Numerical Value” : 设置图表背景的深度
// * showCanvasBg=”1/0″ : 设置是否显示图表背景
// * showCanvasBase=”1/0″ : 设置是否显示图表基部
// 图表和轴的标题
// * caption=”String” : 图表上方的标题
// * subCaption=”String” : 图表上方的副标题
// * xAxisName= “String” : X轴的名字
// * yAxisName= “String” : y轴的名字
// 图表数量值的限制
// * yAxisMinValue=”value”: y轴最小值
// * yAxisMaxValue=”value”: y舟最大值
// 通用参数
// * shownames=”1/0″ : 设置是否在x轴下显示<set>里指定的name
// * showValues=”1/0″ : 设置是否在柱型图或饼型图上显示数据的值
// * showLimits=”1/0″ : 设置是否在图表的y轴坐标上显示最大最小的数据值
// * rotateNames=”1/0″ : 设置x轴下的name 是水平显示还是垂直显示
// * animation=”1/0″ : 设置柱型图的显示是否是动画显示
// 字体属性
// * baseFont=”FontName” : 设置字体样式
// * baseFontSize=”FontSize” : 设置字体大小
// * baseFontColor=”HexColorCode” : 设置字体颜色
// * outCnvBaseFont = “FontName” : 设置图表外侧的字体样式
// * outCnvBaseFontSze=”FontSize” : 设置图表外侧的字体大小
// * outCnvBaseFontColor=”HexColorCode”: 设置图表外侧的字体颜色
// 数字格式选项
// * numberPrefix=”$” : 设置数据值的前缀
// * numberSuffix=”p.a” : 设置数据值的后缀（如果是特殊字符，需要使用URL Encode重编码）
// * formatNumber=”1/0″ : 设置是否格式化数据
// * formatNumberScale=”1/0″ : 设置是否用“K”来代表千，“M”来代表百万
// * decimalSeparator=”.” : 用指定的字符来代替小数点
// * thousandSeparator=”,” : 用指定的字符来代替千位分隔符
// * decimalPrecision=”2″ : 设置十进制的精度
// * divLineDecimalPrecision=”2″: 设置y轴数值的小数位数
// * limitsDecimalPrecision=”2″ : 设置y轴的最大最小值的小数位数
// 水平分隔线
// * numdivlines=”NumericalValue” : 设置水平分隔线的数量
// * divlinecolor=”HexColorCode” : 设置水平分隔线的颜色
// * divLineThickness=”NumericalValue” : 设置水平分隔线的宽度
// * divLineAlpha=”NumericalValue0-100″ : 设置水平分隔线的透明度
// * showDivLineValue=”1/0″ : 设置是否显示水平分隔线的数值
// 鼠标旋停参数
// * showhovercap=”1/0″ : 显示是否激活鼠标旋停效果
// * hoverCapBgColor=”HexColorCode” : 设置鼠标旋停效果的背景颜色
// * hoverCapBorderColor=”HexColorCode” : 设置鼠标旋停效果的边框颜色
// * hoverCapSepChar=”Char” : 设置鼠标旋停后显示的文本中的分隔符号
// 图表边距的设置
// * chartLeftMargin=”Numerical Value (in pixels)” : 设置图表左边距
// * chartRightMargin=”Numerical Value (in pixels)” : 设置图表右边距
// * chartTopMargin=”Numerical Value (in pixels)” : 设置图表上边距
// * chartBottomMargin=”Numerical Value (in pixels)” : 设置图表下边距
// Zero Plane
// The zero plane is a 3D plane that signifies the 0 position on the chart. If
// there are no negative numbers on the chart, you won’t see a visible zero
// plane.
// * zeroPlaneShowBorder=”1/0″ : Whether the border of a 3D zero plane would be
// plotted or not.
// * zeroPlaneBorderColor=”Hex Code” : If the border is to be plotted, this
// attribute sets the border color for the plane.
// * zeroPlaneColor=”Hex Code” : The intended color for the zero plane.
// * zeroPlaneAlpha=”Numerical Value 0-100″ : The intended transparency for the
// zero plane.
//
// <set> 所具有的属性
// * name=”string” : 设置在图表中体现出来的名字
// Example: <set name=’Jan’ …>
// * value=”NumericalValue” : 设置在图表中各个名字想对应的值
// Example: <set name=’Jan’ value=’12345′ …>
// * color=”HexCode” : 设置在图表中相对应的柱行图的颜色
// Example: <set name=’Jan’ value=’12345′ color=’636363′ …>
// * hoverText=”String value” : 设置鼠标旋停在相对应的柱行图 上出现的文本内容
// Example: <set name=’Jan’ value=’12345′ color=’636363′ hoverText=’January’…>
// * link=”URL” : 设置该柱行图的链接地址（需要URL Encode重编码）
// Example: <set … link=’ShowDetails.asp%3FMonth=Jan’ …>
// * alpha=”Numerical Value 0-100″ : 设置在图表中相对应的柱行图的透明度
// Example: <set … alpha=’100′ …>
// * showName=”1″ : 设置在是否显示图表中相对应的柱行图的name
// Example : <set … showName=”1″ …>

// <chart caption='各地市对比图' xAxisName='' yAxisName=''
// outCnvBaseFontColor='#000000' showValues='0' decimals='3'
// formatNumberScale='0' baseFontSize='13' bgAlpha='40' bgColor='#F3EED1'
// legendPadding='0' slantLabels='1' showLabels='1' chartLeftMargin='1'
// chartRightMargin='1' chartTopMargin='1' chartBottomMargin='1'
// canvasBgColor='#F9F9F9' canvasBaseColor='#F9F9F9' canvasBaseDepth='3'
// showCanvasBg='1' showCanvasBase='1'><set lable='...' value='...'/><set
// lable='...' value='...'/><set lable='...' value='...'/><set lable='...'
// value='...'/> </chart>
//
// FusionCharts 的 XML标签属性有一下四种数据类型
// * Boolean - 布尔类型，只能为1或者0。例如：<graph showNames=’1′ >
// * Number - 数字类型，只能为数字。例如：<graph yAxisMaxValue=’200′ >
// * String - 字符串类型，只能为字符串。例如： <graph caption=’My Chart’ >
// * Hex Color Code - 十六进制颜色代码，前边没有’#’.例如： <graph bgColor=’FFFFDD’ >
//
// XML中的标签和属性有：
// <graph> 所具有的属性
// flash背景参数：
// * bgColor=”HexColorCode” : 设置flash的背景颜色
// * bgAlpha=”NumericalValue(0-100)” : 设置背景的透明度
// * bgSWF=”Path of SWF File” : 设置一个外部的Flash 为flash的背景
// 图表背景参数：
// * canvasBgColor=”HexColorCode” : 设置图表背景的颜色
// * canvasBaseColor=”HexColorCode” : 设置图表基部的颜色
// * canvasBaseDepth=”Numerical Value” : 设置图表基部的高度
// * canvasBgDepth=”Numerical Value” : 设置图表背景的深度
// * showCanvasBg=”1/0″ : 设置是否显示图表背景
// * showCanvasBase=”1/0″ : 设置是否显示图表基部
// 图表和轴的标题
// * caption=”String” : 图表上方的标题
// * subCaption=”String” : 图表上方的副标题
// * xAxisName= “String” : X轴的名字
// * yAxisName= “String” : y轴的名字
// 图表数量值的限制
// * yAxisMinValue=”value”: y轴最小值
// * yAxisMaxValue=”value”: y舟最大值
// 通用参数
// * shownames=”1/0″ : 设置是否在x轴下显示<set>里指定的name
// * showValues=”1/0″ : 设置是否在柱型图或饼型图上显示数据的值
// * showLimits=”1/0″ : 设置是否在图表的y轴坐标上显示最大最小的数据值
// * rotateNames=”1/0″ : 设置x轴下的name 是水平显示还是垂直显示
// * animation=”1/0″ : 设置柱型图的显示是否是动画显示
// 字体属性
// * baseFont=”FontName” : 设置字体样式
// * baseFontSize=”FontSize” : 设置字体大小
// * baseFontColor=”HexColorCode” : 设置字体颜色
// * outCnvBaseFont = “FontName” : 设置图表外侧的字体样式
// * outCnvBaseFontSze=”FontSize” : 设置图表外侧的字体大小
// * outCnvBaseFontColor=”HexColorCode”: 设置图表外侧的字体颜色
// 数字格式选项
// * numberPrefix=”$” : 设置数据值的前缀
// * numberSuffix=”p.a” : 设置数据值的后缀（如果是特殊字符，需要使用URL Encode重编码）
// * formatNumber=”1/0″ : 设置是否格式化数据
// * formatNumberScale=”1/0″ : 设置是否用“K”来代表千，“M”来代表百万
// * decimalSeparator=”.” : 用指定的字符来代替小数点
// * thousandSeparator=”,” : 用指定的字符来代替千位分隔符
// * decimalPrecision=”2″ : 设置十进制的精度
// * divLineDecimalPrecision=”2″: 设置y轴数值的小数位数
// * limitsDecimalPrecision=”2″ : 设置y轴的最大最小值的小数位数
// 水平分隔线
// * numdivlines=”NumericalValue” : 设置水平分隔线的数量
// * divlinecolor=”HexColorCode” : 设置水平分隔线的颜色
// * divLineThickness=”NumericalValue” : 设置水平分隔线的宽度
// * divLineAlpha=”NumericalValue0-100″ : 设置水平分隔线的透明度
// * showDivLineValue=”1/0″ : 设置是否显示水平分隔线的数值
// 鼠标旋停参数
// * showhovercap=”1/0″ : 显示是否激活鼠标旋停效果
// * hoverCapBgColor=”HexColorCode” : 设置鼠标旋停效果的背景颜色
// * hoverCapBorderColor=”HexColorCode” : 设置鼠标旋停效果的边框颜色
// * hoverCapSepChar=”Char” : 设置鼠标旋停后显示的文本中的分隔符号
// 图表边距的设置
// * chartLeftMargin=”Numerical Value (in pixels)” : 设置图表左边距
// * chartRightMargin=”Numerical Value (in pixels)” : 设置图表右边距
// * chartTopMargin=”Numerical Value (in pixels)” : 设置图表上边距
// * chartBottomMargin=”Numerical Value (in pixels)” : 设置图表下边距
// Zero Plane
// The zero plane is a 3D plane that signifies the 0 position on the chart. If
// there are no negative numbers on the chart, you won’t see a visible zero
// plane.
// * zeroPlaneShowBorder=”1/0″ : Whether the border of a 3D zero plane would be
// plotted or not.
// * zeroPlaneBorderColor=”Hex Code” : If the border is to be plotted, this
// attribute sets the border color for the plane.
// * zeroPlaneColor=”Hex Code” : The intended color for the zero plane.
// * zeroPlaneAlpha=”Numerical Value 0-100″ : The intended transparency for the
// zero plane.
//
// <set> 所具有的属性
// * name=”string” : 设置在图表中体现出来的名字
// Example: <set name=’Jan’ …>
// * value=”NumericalValue” : 设置在图表中各个名字想对应的值
// Example: <set name=’Jan’ value=’12345′ …>
// * color=”HexCode” : 设置在图表中相对应的柱行图的颜色
// Example: <set name=’Jan’ value=’12345′ color=’636363′ …>
// * hoverText=”String value” : 设置鼠标旋停在相对应的柱行图 上出现的文本内容
// Example: <set name=’Jan’ value=’12345′ color=’636363′ hoverText=’January’…>
// * link=”URL” : 设置该柱行图的链接地址（需要URL Encode重编码）
// Example: <set … link=’ShowDetails.asp%3FMonth=Jan’ …>
// * alpha=”Numerical Value 0-100″ : 设置在图表中相对应的柱行图的透明度
// Example: <set … alpha=’100′ …>
// * showName=”1″ : 设置在是否显示图表中相对应的柱行图的name
// Example : <set … showName=”1″ …>
//
// ////////////////////////////////////////////////////////////////////////////////////////////
//
// ANCHORS 锚点 用于标识line或area的数值点
// 支持效果 Animation 动画、Shadow 阴影、Glow 发光、Bevel 倾斜、Blur 模糊
// 动画属性 _alpha、_x、_y、_xScale、_yScale
// BACKGROUND 整个图表的背景
// 支持属性 Animation、Shadow、Glow、Bevel、Blur
// 动画属性 _alpha、_x、_y、_xScale、_yScale
// CANVAS 区域图中的区域
// 支持属性 Animation、Shadow、Glow、Bevel、Blur
// 动画属性 _alpha、_x、_y、_xScale、_yScale
// CAPTION 图表标题
// SUBCAPTION 图表子标题
// 支持属性 Animation、Shadow、Glow、Bevel、Blur、Font 字体
// 动画属性 _alpha、_x、_y
// DATALABELS 数据的x轴标签列表
// 支持属性 Animation、Shadow、Glow、Bevel、Blur、Font 字体
// 动画属性 _alpha、_x、_y
// DATAPLOT 数据细节（如：2D图表中的列）
// 支持属性 Animation、Shadow、Glow、Bevel、Blur
// 动画属性 _alpha、_x、_y、_xScale、_yScale
// DATAVALUES 图表数据
// 支持属性 Animation、Shadow、Glow、Bevel、Blur、Font 字体
// 动画属性 _alpha、_x、_y
// DIVLINES 水平的列表区域(由div组成的线)
// 支持属性 Animation、Shadow、Glow、Bevel、Blur
// 动画属性 _alpha、_x、_y、_xScale
// HGRID 水平的两个列表区域中交替的颜色
// 支持属性 Animation、Shadow、Glow、Bevel、Blur
// 动画属性 _alpha、_x、_y、_xScale、_yScale
// VDIVLINES 垂直的列表区域
// VGRID 垂直的两个列表区域中交替的颜色
// VLINES 垂直分割线
// XAXISNAME x轴名称
// YAXISNAME y轴名称
// YAXISVALUES y轴的值列表
//
// TOOLTIP 在鼠标移动到数据点上的时候的提示
// 支持属性 Font
// TRENDLINES 趋势线
// TRENDVALUES
// chart
// Functional Attributes
// animation bool 是否使用动画
// palette number(1-5) 使用默认的调色板
// connectNullData bool 是否呈现空值（？猜测）
// showLabels bool 是否显示标签
// labelDisplay string (WRAP,STAGGER,ROTATE or NONE ) 标签的呈现方式（超长屏蔽、折行、倾斜还是不显示）
// rotateLabels bool
// slantLabels bool x轴说明文字是否倾斜（1/0）
// labelStep number (1 or above)
// staggerLines number (2 or above) 多少个字符后折行（labelDisplay=stagger）
// showValues bool 是否一直显示数据值在数据点上
// rotateValues bool 是否滚动显示值 （showValues=1）
// showYAxisValues bool 是否显示y轴数据
// showLimits bool
// showDivLineValues bool
// yAxisValuesStep number (1 or above) y轴标记的显示间隔
// adjustDiv bool 自动调整divlines
// rotateYAxisName bool
// yAxisNameWidth number (In Pixels)
// clickURL String 图表的焦点链接地址
// defaultAnimation bool 是否开启默认动画
// yAxisMinValue number Y轴中最小值
// yAxisMaxValue number Y轴中最大值
// setAdaptiveYMin
// Chart Titles and Axis Names
// caption
// subCaption
// xAxisName
// yAxisName
// Chart Cosmetics
// bgColor color 可以使用逗号分割多个颜色值 FF5904,FFFFFF
// bgAlpha number (0-100) 透明度
// bgRatio number (0-100) 多个颜色值所占的比率
// bgAngle number (0-360) 角度
// bgSWF string 背景flash，但必须在同一个域下
// bgSWFAlpha number (0-100)
// canvasBgColor Color 区域背景颜色
// canvasBgAlpha
// canvasBgRatio
// canvasBgAngle
// canvasBorderColor
// canvasBorderThickness number (0-5) 边框厚度
// canvasBorderAlpha
// showBorder bool
// borderColor
// borderThickness number In Pixels
// borderAlpha
// Data Plot Cosmetics
// showPlotBorder bool
// plotBorderColor
// plotBorderThickness (0-5)pixels
// plotBorderAlpha
// plotBorderDashed bool 是否使用虚线
// plotBorderDashLen number in pixels
// plotBorderDashGap number in pixels
// plotFillAngle number 0-360
// plotFillRatio number 0-100
// plotFillAlpha
// plotGradientColor color 渐变颜色
// showShadow bool 是否显示阴影
// plotFillColor
// Anchors
// drawAnchors bool
// anchorSides Number 3-20 边数
// anchorRadius number in pixels 半径
// anchorBorderColor color hex code
// anchorBorderThickness number in pixels
// anchorBgColor
// anchorAlpha
// anchorBgAlpha
// Divisional Lines & Grids
// numDivLines number >0 水平区域线数量
// divLineColor
// divLineThickness number 1-5
// divLineAlpha
// divLineIsDashed bool 虚线
// divLineDashLen
// divLineDashGap
// zeroPlaneColor
// zeroPlaneThickness
// zeroPlaneAlpha
// showAlternateHGridColor
// alternateHGridColor
// alternateHGridAlpha
// numVDivLines
// vDivLineColor
// vDivLineThickness
// vDivLineAlpha
// vDivLineIsDashed
// vDivLineDashLen
// vDivLineDashGap
// showAlternateVGridColor
// alternateVGridColor
// alternateVGridAlpha
// Number Formatting
// formatNumber bool
// formatNumberScale bool
// defaultNumberScale string
// numberScaleUnit string
// numberScaleValue string
// numberPrefix string
// numberSuffix string
// decimalSeparator string
// thousandSeparator string
// inDecimalSeparator string
// inThousandSeparator string
// decimals number 0-10
// forceDecimals bool
// yAxisValueDecimals number 0-10
// Font Properties
// baseFont
// baseFontSize number 0-72
// baseFontColor
// outCnvBaseFont cnv canvas
// outCnvBaseFontSize
// outCnvBaseFontColor
// Tool-tip
// showToolTip bool
// toolTipBgColor
// toolTipBorderColor
// toolTipSepChar
// Chart Padding & Margins
// captionPadding
// xAxisNamePadding
// yAxisNamePadding
// yAxisValuesPadding
// labelPadding
// valuePadding
// chartLeftMargin
// chartRightMargin
// chartTopMargin
// chartBottomMargin
// canvasPadding
// set element
// label string
// value number
// color color hex code
// link string
// toolText string
// showLavel
// showValue
// dashed
// alpha
// anchorSides
// anchorRadius
// anchorBorderColor
// anchorBorderThickness
// anchorBgColor
// anchorAlpha
// anchorBgAlpha
// Vertical data separator lines
// <set label=’Dec 2005′ value=’36556′ />
// <vLine color=’FF5904′ thickness=’2′ />
// <set label=’Jan 2006′ value=’45456′ />
// color
// thickness
// alpha
// dashed
// dashLen
// dashGap
// Trend-lines
// <trendLines>
// <line startValue=’895′ color=’FF0000′ displayvalue=’Average’ />
// </trendLines>
// startValue
// endValue
// displayValue
// color
// isTrendZone
// showOnTop
// thickness
// alpha
// dashed
// dashLen
// FusionCharts 的XML标签属性