package com.bill99.testmp.framework.process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.bill99.testmp.framework.vo.Application;
import com.bill99.testmp.framework.vo.Apply;
import com.bill99.testmp.framework.vo.Category;
import com.bill99.testmp.framework.vo.Chart;
import com.bill99.testmp.framework.vo.Dataset;
import com.bill99.testmp.framework.vo.Definition;
import com.bill99.testmp.framework.vo.Set;
import com.bill99.testmp.framework.vo.Style;
import com.bill99.testmp.framework.vo.Styles;
import com.bill99.testmp.framework.web.controller.MonitorParent;
import com.bill99.testmp.testmanage.common.dto.TaReportChartDto;

public class BPProcess extends MonitorParent {
	
	private static String[] ColorList ={"1D8BD1","F1683C","2AD62A","d80400","DBDC25","72952f","843584","0fc0c0","FCC916","96c40c","FF6302","FEBD47","C5824D","FF032F","AA0201","5BA8D6","2F50C4","1D8BD1","DBDC25","000033","330066" ,"330000", "33CCCC", "FFFFCC"};
		
	public String createBPByTypeId(List<TaReportChartDto> taChartDto) throws Exception {
		// TODO Auto-generated method stub
		return convertXML(chart2XML(createChart(taChartDto)));
	}

	private Chart createChart(List<TaReportChartDto> taChartDto) throws Exception {
		Chart chart = new Chart();
		
		chart.setCaption("自动化成功比例趋势图");
		
		chart.setyAxisName("自动化执行成功比例");
		chart.setxAxisName("上线时间");
		chart.setFormatNumberScale("0");
		chart.setyAxisMaxValue("100");
		chart.setyAxisMinValue("0");
		chart.setyAxisValuesStep("1");
		chart.setNumberSuffix("%");
		chart.setLineThickness("1");
		chart.setShowValues("1");
		chart.setAnchorRadius("2");
		chart.setDivLineAlpha("20");
		chart.setDivLineColor("CC3300");
		chart.setDivLineIsDashed("1");
		chart.setShowAlternateHGridColor("1");
		chart.setAlternateHGridColor("CC3300");
		chart.setShadowAlpha("40");
		chart.setLabelStep("1");
		chart.setChartRightMargin("35");
		chart.setBgColor("FFFFFF,CC3300");
		chart.setBgAngle("270");
		chart.setBgAlpha("10,10");
		chart.setAlternateHGridAlpha("5");
	   
	
	  
		//chart.setBaseFont("微软雅黑");
		List<Category> categories = new ArrayList<Category>();
		List<String> listCycle = new ArrayList<String>();
		java.util.Set<String> categorieSets = new java.util.HashSet<String>();
		for (TaReportChartDto chartDto : taChartDto) {
			for(TaReportChartDto chartDtonew :chartDto.getChartDtos()){
		         String cycleString = chartDtonew.getCycle();
			 if (!categorieSets.contains(cycleString)) {
				 listCycle.add(cycleString);
				
			}
			categorieSets.add(cycleString);
			
			}	
		}
		Collections.sort(listCycle);
		if(listCycle != null && listCycle.size()>0){
		chart.setSubcaption("(Cycle From"+ listCycle.get(0)+"To"+listCycle.get(listCycle.size()-1)+":总计:"+listCycle.size()+"个cycle)");
		}
		for(String cycString:listCycle){
			categories.add(new Category(cycString));
		}
		
		List<Dataset> dataset = new ArrayList<Dataset>();
		    int i = 0;
			for(TaReportChartDto chartDto : taChartDto){
				  
				   List<Set> set = new ArrayList<Set>();
					Dataset ds = new Dataset();
			for(Category category:categories){
				boolean flag = false;
				Set setTmp2 = new Set(); 
				for(TaReportChartDto chartDtonew :chartDto.getChartDtos()){
					if(category.getLabel().equals(chartDtonew.getCycle())){
							 setTmp2.setValue(String.valueOf(chartDtonew.getRate().multiply(new BigDecimal(100)).setScale(0)));
							 set.add(setTmp2);	
							 flag = true;
							 break;
				  }
				}
				if(!flag){
					 setTmp2.setValue("0");
					 set.add(setTmp2);
				}
				
				}
			ds.setSet(set);
			ds.setSeriesName(chartDto.getPrefix());
			ds.setColor(ColorList[i]);
			ds.setAnchorBorderColor(ColorList[i]);
			ds.setAnchorBgColor(ColorList[i]);
			dataset.add(ds);
			i ++;
			}
			
			
			/*  System.out.println("categories.size()=" + categories.size());
		       System.out.println("dataset.size()=" + dataset.size());*/
				chart.setCategories(categories);
				chart.setNumvdivlines(String.valueOf(categories.size()));
				chart.setDataset(dataset);
				
				Styles styles = new Styles();
				// Definition
				Definition definition = new Definition();
				List<Style> styleTmp = new ArrayList<Style>();
				Style style = new Style();
				style.setName("CaptionFont");
				style.setType("font");
				style.setSize("12");
				styleTmp.add(style);
				definition.setStyle(styleTmp);
				
				Application application = new Application();
				List<Apply> applyTmp = new ArrayList<Apply>();
				Apply apply = new Apply();
				apply.setToObject("CAPTION");
				apply.setStyles("CaptionFont");
				Apply apply2 = new Apply();
				apply2.setToObject("SUBCAPTION");
				apply2.setStyles("CaptionFont");
				applyTmp.add(apply);
				applyTmp.add(apply2);
				application.setApply(applyTmp);
				styles.setDefinition(definition);
				styles.setApplication(application);
				chart.setStyles(styles);
				return chart;
		
	}
}


