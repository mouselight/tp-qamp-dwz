package com.bill99.sysoperation;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class RecoverTestCase extends AbstractJUnit4SpringContextTests {

	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@Test
	public void recoverTestCase() {

		Long tcSuiteId = 1602231L;
		NodesHierarchy nodesHierarchy = nodesHierarchyMng.getById(tcSuiteId);
		nodesHierarchy.setStatus(1);
		nodesHierarchyMng.update(nodesHierarchy);

		//实时递归
		nodesHierarchyMng.recursive4InvalidTc(nodesHierarchy);
	}
}
