/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2011-9-6		    Leo Feng	   		Leo Feng
*/

package com.bill99.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Leo Feng
 *
 */
public class TestSubString4Excel {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String actions = "1.点击“30交易审核”" + "	2.点击“选择审核”" + "	3、.点击“审核”操作" + "	4.点击“可申诉”按钮" + "	5.点击：‘是’按钮" + "	6.输入审核处置说明文本框" + "	7.点击“确认可申诉”按钮	";
		String expectedResults = "			4.弹出提示“确定可申诉吗？”" + "			7.系统提示：“操作完成”" + "（但是处理交易除了该单被后台系统处理以外）	";

		Pattern pattern = Pattern.compile("(\\d[.|、])");
		List<String> strList = Arrays.asList(pattern.split(actions));

		//		for (String string : strList) {
		//			System.out.println(string);
		//		}

		List<String> tempList = new ArrayList<String>();
		Map<String, String> actionsMap = new HashMap<String, String>();
		Matcher m = pattern.matcher(actions);
		int i = 1;
		String str = null;
		while (m.find()) {
			str = strList.get(i);
			tempList.add(actions.substring(m.start(), m.end()) + strList.get(i));
			actionsMap.put(actions.substring(m.start(), m.end() - 1), str);
			i++;
		}
		for (String string : tempList) {
			System.out.println(string);
		}
		System.out.println("------------------");

		for (Entry<String, String> entry : actionsMap.entrySet()) {
			System.out.println(entry.getKey() + entry.getValue());
		}

		//		for (int i = 1; i < strList.size(); i++) {
		//			tempList.add("G" + i + ":" + strList.get(i));
		//		}
		//		if(m.find()){
		//		System.out.println(m.group());
		//		}

		//		for (int i = 1; i < strs.length; i++) {
		//			System.out.println(strs[i]);
		//		}

		//		String input = "This!!unusual use!!of exclamation!!points";
		//		System.out.println(Arrays.asList(Pattern.compile("!!").split(input)));
		//		// Only do the first three:
		//		System.out.println(Arrays.asList(Pattern.compile("!!").split(input, 3)));
		//		System.out.println(Arrays.asList("Aha! String has a split() built in!".split(" ")));

		//		String input = "G1:0瓦;精度0.01至0.03;G2:-3;G3:析功能;检测对象 混合气体;型号GPR-1200;无试纸试剂打印机;";
		//		System.out.println(Arrays.asList(Pattern.compile("(\\G+\\d\\:)").split(input)));
	}
}
