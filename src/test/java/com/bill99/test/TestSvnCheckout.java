package com.bill99.test;

import java.io.File;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class TestSvnCheckout {
	//声明SVN客户端管理类
	private static SVNClientManager ourClientManager;

	public static void main(String... args) throws Exception {
		//初始化支持svn://协议的库。 必须先执行此操作。   
		SVNRepositoryFactoryImpl.setup();
		//相关变量赋值
		SVNURL repositoryURL = null;
		try {
			//			repositoryURL = SVNURL.parseURIEncoded("http://svn.99bill.net/opt/99billsrc/PMD/SRC/EAP/BTA/Applications/cps-tpm/billtrunk/src/main/java/cps/tpm/facade/");
			repositoryURL = SVNURL
					.parseURIEncoded("http://svn.99bill.net/opt/99billsrc/PMD/SRC/EAP/BTA/Applications/cps-tpm/billtrunk/src/main/java/cps/tpm/facade/CardFacadeService.java");
		} catch (SVNException e) {
			//
			System.out.println("无法连接");
		}
		String name = "leo.feng@99bill.com";
		String password = "99bill";
		String workPath = "C:/Users/leofung/Desktop/sonar/ant/src/" + "cps/tpm/facade";
		ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
		//实例化客户端管理类
		ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, name, password);
		//要把版本库的内容check out到的目录
		//FIle wcDir = new File("d:/test")
		File wcDir = new File(workPath);
		wcDir.mkdirs();
		//通过客户端管理类获得updateClient类的实例。
		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
		//sets externals not to be ignored during the checkout
		updateClient.setIgnoreExternals(false);
		//执行check out 操作，返回工作副本的版本号。
		long workingVersion = updateClient.doExport(repositoryURL, wcDir, SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.UNKNOWN);
		System.out.println("把版本：" + workingVersion + " check out 到目录：" + wcDir + "中。");
	}
}