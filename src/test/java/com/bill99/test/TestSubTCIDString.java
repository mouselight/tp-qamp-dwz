/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2011-9-6		    Leo Feng	   		Leo Feng
*/

package com.bill99.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Leo Feng
 *
 */
public class TestSubTCIDString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Pattern pattern = Pattern.compile("(-\\d)");
		String str = "RM-211308";
		List<String> strList = Arrays.asList(pattern.split(str));
		List<String> tempList = new ArrayList<String>();
		Matcher m = pattern.matcher(str);
		int i = 1;
		while (m.find()) {
			tempList.add(str.substring(m.start() + 1, m.end()) + strList.get(i));
			i++;
		}
		System.out.println(tempList.get(tempList.size() - 1));
	}
}
