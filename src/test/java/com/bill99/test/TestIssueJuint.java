package com.bill99.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.bill99.testmp.testmanage.orm.entity.TestIssue;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;
import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
import com.jeecms.common.page.Pagination;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
public class TestIssueJuint extends AbstractJUnit4SpringContextTests {

	@Autowired
	private JiraMng jiraMng;
	@Autowired
	private TestIssueMng testIssueMng;

	@Test
	public void initIssueData() {
		jiraMng.initJiraData();
	}

	@Test
	public void queryCycle() {
		//		System.out.println(testIssueMng.queryCycle(10214L));

		for (Entry<String, String> iterable_element : testIssueMng.queryCycle(10214L).entrySet()) {

			System.out.println(iterable_element.getValue());

		}
	}

	@Test
	public void queryIssueTest() {
		TestIssue testIssue = new TestIssue();
		//		testIssue.setCreatedStart(DateUtil.parseDate(DateUtil.getDate(-90)));
		//		testIssue.setCreatedEnd(DateUtil.parseDate(DateUtil.getDate()));
		//		testIssue.setReleaseDateStart(DateUtil.parseDate("2012-07-23"));
		//		testIssue.setReleaseDateEnd(DateUtil.parseDate("2012-07-26"));
		//		testIssue.setSummary("RIFLE");
		//		testIssue.setIssueStatusCos(new Long[]{10029L});
		//		testIssue.setPlanStatusCos(new Long[]{1L});
		//		testIssue.setPlanStatusCos(new Long[]{1L});
		//		testIssue.setReleaseId("CRL-12Q2-MAM053003");
		testIssue.setTeamId(10216l);
		testIssue.setCycleId(11401l);
		Pagination pagination = testIssueMng.query(testIssue, 1, 20);

		//		System.out.println("==============="+pagination.getTotalCount());
	}

	@Test
	@Ignore
	public void tesGetIssueDetail() {
		TestIssue issue = testIssueMng.getIssueDetail(15438L);
		System.out.println(issue);
	}

	@Test
	public void testQueryUser() {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		System.out.println(testIssueMng.queryUser("冯小磊", ids));
	}

	@Test
	public void queryCycleByIssue() {
		System.out.println(jiraMng.queryCycleByIssue(11274L));
	}

}
