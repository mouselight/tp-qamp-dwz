package com.bill99.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testtools.domain.AcceptabilityCNP;
import com.bill99.testmp.testtools.domain.impl.AcceptabilityCNPImpl;
import com.bill99.testmp.testtools.domain.impl.AcceptabilityDDPImpl;
import com.bill99.testmp.testtools.orm.dao.CheckedItemInfoDao;
import com.bill99.testmp.testtools.orm.dao.impl.CheckedItemInfoDaoImpl;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;
import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.entity.CheckedItemInfo;
import com.bill99.testmp.testtools.orm.entity.CnpRequstMsg;
import com.bill99.testmp.testtools.orm.manager.AcceptAbilityTaskMng;
import com.bill99.testmp.testtools.orm.manager.CheckedItemInfoMng;


@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestAcceptability extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AcceptAbilityTaskMng acceptAbilityTaskMng;
	@Autowired
	@Qualifier("acceptabilityDDP")
	private AcceptabilityCNP acceptabilityDDP ;
//	@Autowired
//	@Qualifier("acceptabilityCNP")
//	private AcceptabilityCNP acceptabilityCNP ;
		
//	@Test
//	public void test1() {
//		try {
//			List <Long> pans =  new ArrayList<Long> (); 
////			pans.add(4L);
//			pans.add(16L);
////			pans.add(6L);
//			acceptabilityCNP.exploring(acceptAbilityTaskMng.queryById(2282L), pans);
////			AcceptAbilityTask acceptAbilityTask = new AcceptAbilityTask();
////			acceptAbilityTask.setIdTask(2218L);
////			acceptAbilityTask.setAmt(BigDecimal.valueOf(121.00));
////			acceptAbilityTask.setTerminaId("99999901");
////			acceptAbilityTask.setMerchantId("999999999000001");
////			acceptAbilityTask.setTxnInterface("PUR");
////			
////			acceptabilityCNPImpl.exploring(acceptAbilityTask, pans);
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		
//	}
	
	@Test
	public void test2() {
		try {
			List <Long> pans =  new ArrayList<Long> (); 
			pans.add(24L);
			//acceptabilityDDP.exploring(acceptAbilityTaskMng.queryById(2314L), pans);
			acceptabilityDDP.exploring(acceptAbilityTaskMng.queryById(2308L), pans);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		
	}

}
