package com.bill99.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.util.StringUtils;

import com.bill99.testmp.testmanage.orm.entity.TcSteps;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.manager.TcStepsMng;
import com.bill99.testmp.testmanage.orm.manager.TestCasesMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestTcTextFormat extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestCasesMng testCasesMng;

	@Autowired
	private TcStepsMng tcStepsMng;

	@Test
	public void testTcTextFormat() {

		//testCases
		List<TestCases> testCases = testCasesMng.queryAll();
		List<TestCases> testCasesuUdate = new ArrayList<TestCases>();
		for (TestCases testCase : testCases) {
			if (StringUtils.hasLength(testCase.getSummary())) {
				testCase.setSummary(testCase.getSummary().replaceAll("<.+?>", ""));
			}
			if (StringUtils.hasLength(testCase.getPreconditions())) {
				testCase.setPreconditions(testCase.getPreconditions().replaceAll("<.+?>", ""));
			}
			testCasesuUdate.add(testCase);
		}
		testCasesMng.bath4Update(testCasesuUdate);

		//tcSteps
		List<TcSteps> tcSteps = tcStepsMng.queryAll();
		List<TcSteps> tcStepsUpdate = new ArrayList<TcSteps>();
		for (TcSteps tcStep : tcSteps) {
			if (StringUtils.hasLength(tcStep.getActions())) {
				tcStep.setActions(tcStep.getActions().replaceAll("<.+?>", ""));
			}
			if (StringUtils.hasLength(tcStep.getExpectedResults())) {
				tcStep.setExpectedResults(tcStep.getExpectedResults().replaceAll("<.+?>", ""));
			}
			tcStepsUpdate.add(tcStep);
		}
		tcStepsMng.batch4Update(tcStepsUpdate);
	}

}
