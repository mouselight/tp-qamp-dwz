package com.bill99.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testtools.domain.AcceptabilityExtension;


@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestAcceptabilityExtension extends AbstractJUnit4SpringContextTests {
	@Autowired
	private AcceptabilityExtension acceptabilityExtension;
		
	@Test
	public void test() throws Exception {
		acceptabilityExtension.getExtFromDB(2428L);
	}
}
