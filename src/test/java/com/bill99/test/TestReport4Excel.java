package com.bill99.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.domain.TestReportService;
import com.bill99.testmp.testmanage.orm.entity.TestCaseTree;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TestReportIbatisDao;

@ContextConfiguration(locations = { "classpath:context/applicationContext.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
public class TestReport4Excel extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestReportService testReportService;
	@Autowired
	private TestReportIbatisDao testReportIbatisDao;

	@Test
	public void getAssoTcTreeReport() {
		//Tc列表 Content
		Map<String, String> tcNameMap = new HashMap<String, String>();
		Map<String, Map<String, List<TestCaseTree>>> resultReport = new TreeMap<String, Map<String, List<TestCaseTree>>>();
		Map<String, Map<String, List<TestCaseTree>>> result = new TreeMap<String, Map<String, List<TestCaseTree>>>();
		List<TestCaseTree> caseList = testReportIbatisDao.getCaseList(1360138627044L,null);
		if (null != caseList && caseList.size() > 0) {
			for (TestCaseTree tc : caseList) {
				Map<String, String> map = testReportService.getAllParentTc(tc.getTcId());

				String allParentId = "";
				for (Iterator it = map.keySet().iterator(); it.hasNext();) {
					String key = (String) it.next();
					if (allParentId == "") {
						allParentId = String.valueOf(key);
					} else {
						allParentId = allParentId + TreeUtils.NODE_NAME_SPILT + String.valueOf(key);
					}
				}

				String[] ids = allParentId.split(TreeUtils.NODE_NAME_SPILT);

				String parentId = "";
				String parentName = "";
				for (int i = 1; i < ids.length; i++) {
					if (parentId == "") {
						parentId = ids[i];
						parentName = map.get(parentId);
					} else {
						parentId = parentId + TreeUtils.NODE_NAME_SPILT + ids[i];
						parentName = parentName + TreeUtils.NODE_NAME_SPILT + map.get(ids[i]);
					}
				}

				String topParent = ids[0];

				tcNameMap.put(topParent, map.get(topParent));
				tcNameMap.put(parentId, parentName);

				Map<String, List<TestCaseTree>> map_ = result.get(topParent);

				if (null == map_) {
					List<TestCaseTree> list = new ArrayList<TestCaseTree>();
					list.add(tc);
					map_ = new TreeMap<String, List<TestCaseTree>>();
					map_.put(parentId, list);
					result.put(topParent, map_);
				} else {
					List<TestCaseTree> list = map_.get(parentId);
					if (null == list || list.size() <= 0) {
						list = new ArrayList<TestCaseTree>();
					}
					list.add(tc);
					map_.put(parentId, list);
					result.put(topParent, map_);
				}
			}
		}

		String[] report_key = result.keySet().toArray(new String[] {});

		int first_begin_row = 5;//下一个功能集行
		int second_begin_row = 5;//下一个功能集行
		int end_row_tmp = 5;

		List<TestCaseTree> tcList = new ArrayList<TestCaseTree>();
		for (int i = 0; i < report_key.length; i++) {
			//模块
			//			System.out.println(first_begin_row + " | " + 0 + " | " + tcNameMap.get(report_key[i]));

			//功能集
			Map<String, List<TestCaseTree>> map_tmp = result.get(report_key[i]);

			String[] node_key = map_tmp.keySet().toArray(new String[] {});
			for (int j = 0; j < node_key.length; j++) {
				//				System.out.println(second_begin_row + " | " + 1 + " | " + tcNameMap.get(node_key[j]));

				tcList = map_tmp.get(node_key[j]);
				for (int k = 0; k < tcList.size(); k++) {
					//测试用例
					//					System.out.println((second_begin_row + k) + " | " + 2 + " | " + tcList.get(k).getName());
					//					System.out.println((second_begin_row + k) + " | " + 3 + " | " + tcList.get(k).getUpdateTimeStr());
					//					System.out.println((second_begin_row + k) + " | " + 4 + " | " + tcList.get(k).getStatusName());
					//					System.out.println((second_begin_row + k) + " | " + 5 + " | " + tcList.get(k).getUpdateUser());

				}
				//合并列-功能集
				System.out.println("功能集：" + 1 + " | " + second_begin_row + " | " + 1 + " | "
						+ (second_begin_row + tcList.size() - 1));

				//行号
				second_begin_row = second_begin_row + tcList.size();
				end_row_tmp = end_row_tmp + tcList.size();
			}

			//合并列-模块
			System.out.println("模块：" + 1 + " | " + first_begin_row + " | " + 1 + " | " + (end_row_tmp - 1));

			//下一行
			first_begin_row = end_row_tmp;
		}
	}
}
