package com.bill99.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.riaframework.common.helper.Bill99Logger;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestThread extends AbstractJUnit4SpringContextTests {
	private static Bill99Logger logger = Bill99Logger.getLogger(TestThread.class);

	@Autowired
	private TaskExecutor taskExecutor;

	@Test
	public void taskTest() {

		for (int i = 0; i < 8; i++) {
			processByTread(i);
		}

		try {
			Thread.sleep(10000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processByTread(final int i) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				try {
					Thread.sleep(1000L);
					logger.info("this is " + i);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}

}
