//package com.bill99.test;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//
//import com.bill99.riaframework.orm.manager.RoleService;
//import com.bill99.rmca.common.util.DateUtil;
//import com.bill99.testmp.framework.enums.StatusEnum;
//import com.bill99.testmp.framework.enums.TestPlanStatusEnum;
//import com.bill99.testmp.testmanage.common.dto.TestPlansDto;
//import com.bill99.testmp.testmanage.domain.TestPlanService;
//import com.bill99.testmp.testmanage.orm.entity.TestIssue;
//import com.bill99.testmp.testmanage.orm.entity.TestReportLog;
//import com.bill99.testmp.testmanage.orm.entity.Testplans;
//import com.bill99.testmp.testmanage.orm.manager.TestIssueMng;
//import com.bill99.testmp.testmanage.orm.manager.TestReportLogMng;
//import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
//import com.bill99.testmp.testmanage.orm.manager.TestplansMng;
//
//@ContextConfiguration(locations = { "classpath:context/applicationContext.xml" })
//@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
//public class TestIssuePlan extends AbstractJUnit4SpringContextTests {
//
//	@Autowired
//	private TestPlanService testPlanService;
//	@Autowired
//	private TestplansMng testplansMng;
//	@Autowired
//	private TestplanStepsMng testplanStepsMng;
//	@Autowired
//	private RoleService roleService;
//
//	@Test
//	public void mergeTestPlans() {
//		TestPlansDto plansDto = new TestPlansDto();
//		plansDto.setIssueId(100L);
//
//		plansDto.setPlanBeginDate(DateUtil.parseDate(DateUtil.getDate(-90)));
//		plansDto.setPlanEndDate(DateUtil.parseDate(DateUtil.getDate()));
//		plansDto.setState(StatusEnum.STATUS1.getValue());
//		plansDto.setCreateUser("admin");
//		plansDto.setMemo("plan_memo");
//
//		List<Integer> types = new ArrayList<Integer>();
//		List<Date> planBeginDates = new ArrayList<Date>();
//		List<Date> planEndDates = new ArrayList<Date>();
//		List<String> memos = new ArrayList<String>();
//		List<String> resources = new ArrayList<String>();
//
//		types.add(1);
//		types.add(2);
//		types.add(3);
//
//		planBeginDates.add(DateUtil.parseDate(DateUtil.getDate(-1)));
//		planBeginDates.add(DateUtil.parseDate(DateUtil.getDate(-2)));
//		planBeginDates.add(DateUtil.parseDate(DateUtil.getDate(-3)));
//
//		planEndDates.add(DateUtil.parseDate(DateUtil.getDate(-4)));
//		planEndDates.add(DateUtil.parseDate(DateUtil.getDate(-5)));
//		planEndDates.add(DateUtil.parseDate(DateUtil.getDate(-6)));
//
//		resources.add("11");
//		resources.add("12");
//		resources.add("13");
//
//		memos.add("memo_4");
//		memos.add("memo_5");
//		memos.add("memo_6");
//
//		plansDto.setTypes(types);
//		plansDto.setPlanBeginDates(planBeginDates);
//		plansDto.setPlanEndDates(planEndDates);
//		plansDto.setResources(resources);
//		plansDto.setMemos(memos);
//
//		testPlanService.mergeTestPlans(plansDto, null);
//	}
//
//	@Test
//	public void queryTest() {
//		Testplans testPlan = new Testplans();
//		testPlan.setReleaseId("RM011403");
//		testPlan.setTeamId(10214L);
//		testPlan.setSummary("RIFLE");
//		testPlan.setReleaseDateStart(DateUtil.parseDate(DateUtil.getDate(-4)));
//		testPlan.setReleaseDateEnd(DateUtil.parseDate(DateUtil.getDate(-1)));
//		testplansMng.query(testPlan, 1, 20);
//	}
//
//	@Test
//	public void saveStepTcAsso() {
//		List<Long> list = new ArrayList<Long>();
//
//		for (int i = 0; i <= 1000; i++) {
//			list.add(Long.valueOf(new Integer(i).toString()));
//		}
//
//		List<Long> parentIds = new ArrayList<Long>();
//
//		testplanStepsMng.saveAssoTcIds(1L, list, "leo");
//	}
//
//	@Test
//	public void getStepAllChilds() {
//		List<Long> parentIds = new ArrayList<Long>();
//		parentIds.add(87838L);
//		System.out.println(testPlanService.getStepAllChilds(parentIds, 1361438223003L));
//	}
//
//	@Test
//	public void getAllChildIds() {
//		List<Long> list = new ArrayList<Long>();
//		list.add(199693L);
//		list.add(72L);
//		System.out.println(testPlanService.getAllChildIds(list));
//	}
//
//	@Test
//	public void getPlans() {
//		System.out.println(testPlanService.getParentIdsExecution(1361341364885L, 1, 2));
//	}
//
//	@Autowired
//	private TestIssueMng testIssueMng;
//
//	@Test
//	public void test() {
//		List<TestIssue> testIssueList = new ArrayList<TestIssue>();
//		List<Testplans> testplansList = new ArrayList<Testplans>();
//		try {
//			//待排期的需求
//			TestIssue queryModel = new TestIssue();
//			queryModel.setTeamId(10214L);
//			queryModel.setIssueStatusCos(new Long[] { 1L, 10022L, 10023L, 10024L, 10025L, 10026L, 10027L, 10028L });
//			testIssueList = testIssueMng.query(queryModel);
//
//			System.out.println("testIssueList:" + testIssueList.size());
//
//			//待跟踪的计划
//			Testplans testPlan = new Testplans();
//			testPlan.setTeamId(10214l);
//			testPlan.setStates(new Integer[] { TestPlanStatusEnum.STATUS_0.getValue(), TestPlanStatusEnum.STATUS_1.getValue() });
//			testplansList = testplansMng.query(testPlan);
//
//			System.out.println("testplansList:" + testplansList.size());
//		} catch (Exception e) {
//			logger.error("-- query tl_index_plan error --", e);
//		}
//	}
//
//	@Autowired
//	private TestReportLogMng testReportLogMng;
//
//	@Test
//	public void testReportSave() {
//		TestReportLog testReportLog = new TestReportLog();
//		testReportLog.setCtmpSyncResult(true);
//		testReportLog.setMemo("memo");
//		testReportLog.setPlanId(111L);
//		testReportLog.setQaResult(1);
//		testReportLog.setReleasId("ReleaseId");
//		testReportLog.setStepId(222L);
//		testReportLog.setSvnPath("svnPath");
//		testReportLog.setCreateUser("createUser");
//		testReportLog.setSvnCommitResult(true);
//		testReportLog.setCreateTime(new Date());
//		testReportLogMng.save(testReportLog);
//	}
//
//}
