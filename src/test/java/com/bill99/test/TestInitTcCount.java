package com.bill99.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.dao.NodesHierarchyDao;
import com.bill99.testmp.testmanage.orm.dao.TestCasesDao;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestCases;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
import com.bill99.testmp.testmanage.orm.manager.TestProjectsMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestInitTcCount extends AbstractJUnit4SpringContextTests {

	@Autowired
	private NodesHierarchyDao nodesHierarchyDao;
	@Autowired
	private TestCasesDao testCasesDao;
	@Autowired
	private TestProjectsMng testProjectsMng;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@Test
	public void main() {
		//		Long[] ids = new Long[] { 23489L, 23490L, 23491L, 61L, 65L, 66L, 67L, 68L, 69L, 70L, 72L, 73L, 74L, };
		//		Long[] ids = new Long[] { 1L, 12L, 227L, 23487L, 23488L, 64L, 63L, 62L, };
		//		Long[] ids = new Long[] { 62L };
		Long[] ids = new Long[] { 62L, 23491L, 1L };

		for (Long id : ids) {
			updateTcCount(id);
		}
	}

	public void updateTcCount(Long id) {

		List<NodesHierarchy> nh4del = new ArrayList<NodesHierarchy>();
		List<TestCases> tc4del = new ArrayList<TestCases>();

		List<TestCases> testCases = testCasesDao.queryByProjectId(id);
		NodesHierarchy nh = null;
		int i = 0;
		for (TestCases testCase : testCases) {
			nh = nodesHierarchyMng.getById(testCase.getTestCaseId());
			if (nh == null || checkDelTc(nh.getParentId())) {
				i++;
				nh4del.add(nh);
				tc4del.add(testCase);
				//				nodesHierarchyMng.delete(nh.getId());
				//				testCasesDao.delete(testCase.getTestCaseId());
			}
		}
		if (nh4del.size() > 0) {
			nodesHierarchyDao.batch4rvCaseAndSuite(nh4del);
		}
		if (tc4del.size() > 0) {
			testCasesDao.bath4Del(tc4del);
		}
		System.out.println(i);

	}

	private boolean checkDelTc(Long pid) {
		NodesHierarchy nh = getParentNh(pid);
		if (nh == null || nh.getParentId() != null || nh.getNodeTypeId() != 1) {
			return true;
		}
		return false;
	}

	private NodesHierarchy getParentNh(Long pid) {
		NodesHierarchy nh = nodesHierarchyMng.getById(pid);
		if (nh != null && nh.getParentId() != null && nh.getNodeTypeId() != 1) {
			nh = getParentNh(nh.getParentId());
		}
		return nh;
	}

	//	@Test
	public void initTcCount() {

		List<TestCases> testCases4del = new ArrayList<TestCases>();
		List<TestCases> testCases4de2 = new ArrayList<TestCases>();

		List<TestCases> testCases = testCasesDao.queryAll();
		for (TestCases testCase : testCases) {
			boolean flag = false;
			List<Long> childIds = new ArrayList<Long>();
			childIds.add(testCase.getTestCaseId());
			List<Long> parentIds = nodesHierarchyMng.getAllParentIds(childIds);

			if (parentIds == null || parentIds.size() == 0) {
				testCases4del.add(testCase);
			}
			List<NodesHierarchy> nfs = new ArrayList<NodesHierarchy>();
			for (Long parentId : parentIds) {
				NodesHierarchy nodesHierarchy = nodesHierarchyMng.getById(parentId);
				if (nodesHierarchy == null) {
					nodesHierarchyDao.batch4rvCaseAndSuite(nodesHierarchyMng.getChild(parentId));
				} else {
					nfs.add(nodesHierarchy);
					if (nodesHierarchy.getNodeTypeId() == 1) {
						flag = true;
					}
				}
			}
			if (!flag) {
				testCases4de2.add(testCase);
				if (nfs.size() > 0) {
					nodesHierarchyDao.batch4rvCaseAndSuite(nfs);
				}
			}

		}
		if (testCases4del.size() > 0) {
			testCasesDao.bath4Del(testCases4del);
		}
		if (testCases4de2.size() > 0) {
			testCasesDao.bath4Del(testCases4de2);
		}

		//		List<TestProjects> testProjects = testProjectsMng.findALl();
		//		for (TestProjects testProject : testProjects) {
		//
		//			testProject.getId();
		//
		//		}
	}
}
