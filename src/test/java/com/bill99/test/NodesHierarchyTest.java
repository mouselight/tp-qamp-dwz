//package com.bill99.test;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//
//import com.bill99.testmp.testmanage.common.dto.TestCasesQueryDto;
//import com.bill99.testmp.testmanage.domain.TcFilterService;
//import com.bill99.testmp.testmanage.domain.TestPlanService;
//import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
//import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;
//import com.bill99.testmp.testmanage.orm.manager.TestplanStepsMng;
//
//@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
//@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
//public class NodesHierarchyTest extends AbstractJUnit4SpringContextTests {
//
//	@Autowired
//	private NodesHierarchyMng nodesHierarchyMng;
//	@Autowired
//	private TestplanStepsMng testplanStepsMng;
//	@Autowired
//	private TestPlanService testPlanService;
//	@Autowired
//	private TcFilterService tcFilterService;
//
//	@Test
//	public void test() {
//		List<Long> allTcParentIds = testPlanService.getAllParentIds(11L);
//		List<NodesHierarchy> list = nodesHierarchyMng.getAssoChild(198534L, allTcParentIds, 11L, 1, null);
//		System.out.println(list.size());
//	}
//
//	@Test
//	public void updateAssoStepTc() {
//		List<Long> tcIds = new ArrayList<Long>();
//		tcIds.add(199899L);
//		tcIds.add(199921L);
//		tcIds.add(199954L);
//
//		List<Long> parentIds = new ArrayList<Long>();
//		testplanStepsMng.finishAssoTc(11L, tcIds, parentIds, 2, "admin");
//	}
//
//	@Test
//	public void getAllChildIds() {
//		List<Long> parentIds = new ArrayList<Long>();
//		parentIds.add(46887L);
//		//		System.out.println(nodesHierarchyMng.getAllChildIds(parentIds));
//
//		List<Long> tcIds = new ArrayList<Long>();
//		List<Long> partIds = new ArrayList<Long>();
//
//		System.out.println(nodesHierarchyMng.getNoAssoChild(46887L, 1359689274516L, tcIds, partIds));
//	}
//
//	@Test
//	public void getFilterChildIds() {
//		Map<Integer, List<Long>> nodeMapAuto = new HashMap<Integer, List<Long>>();
//		TestCasesQueryDto testCaseDto = new TestCasesQueryDto();
//		testCaseDto.setExecutionTypeCos(new Long[] { 2L });
//		testCaseDto.setTestProjectId(72L);
//		List<Long> list = new ArrayList<Long>();
//		list.add(1586357L);
//		nodeMapAuto = tcFilterService.getFilterChildIds(list, testCaseDto);
//
//		System.out.println(nodeMapAuto);
//	}
//
//	@Test
//	public void testRvCaseAndSuite4Sync() {//qamp(原平台数据)删除需要同步的组
//
//		Long[] ids = new Long[] {
//
//				//		23487L, //INO
//				//		227L, //MA-DDP
//				//
//				//		12L,//B2B-CFOTOOLS
//				//		61L, //B2B-EBPP
//				//
//				//		66L, //B2B-FO
//				//		23491L, //INFS-BGW
//				//		69L, //INFS-CAP
//				//		67L, //INFS-FSC
//				//		68L, //INFS-INF
//				//		1L, //INFS-MAM
//				//		72L, //INFS-RM
//				63L, 74L
//
//		};
//
//		for (Long id : ids) {
//			nodesHierarchyMng.rvCaseAndSuite(id);
//		}
//
//	}
//
//	@Test
//	public void testRvCaseAndSuite4Nync() {//qamp_init(原testlink数据)删除不需要同步的组
//
//		Long[] ids = new Long[] {
//
//		//		62L, //MA-CPS
//		//		65L, //MA-FI
//		//		70L, //OB
//		//		23488L,//EAP-ATF
//		//		23490L,//EAP-BTA
//		//		73L,//EAP-HOTEL
//		//		23489L, //EAP-SVC
//		//		64L,//EFS
//		//				63L, 74L
//
//		};
//
//		for (Long id : ids) {
//			nodesHierarchyMng.rvCaseAndSuite(id);
//		}
//
//	}
//}
