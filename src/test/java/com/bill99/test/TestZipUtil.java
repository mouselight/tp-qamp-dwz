//package com.bill99.test;
//
///**
//* 压缩解压缩工具类(解决了中文文件名问题)
//* 依赖包：ant1.6.jar支持
//* @author ***
//* @create 2009-9-10
//*/
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Enumeration;
//
//import org.apache.tools.zip.ZipEntry;
//import org.apache.tools.zip.ZipFile;
//import org.apache.tools.zip.ZipOutputStream;
//
///**
//* 压缩/解压缩工具类（解决中文文件名问题）
//* @author jibaogang
//* @create 2009-9-10
//*/
//
//public class TestZipUtil {
//	public TestZipUtil() {
//	}
//
//	/**
//	* 压缩文件实现1
//	* @param inputFileName  需要被压缩的源文件/夹
//	* @param zipName  打包后文件名字
//	* @throws Exception
//	*/
//	public void zipFile(String inputFileName, String zipFileName) throws Exception {
//		zip(zipFileName, new File(inputFileName));
//	}
//
//	/**
//	* 实现1
//	* @param zipFileName
//	* @param inputFile
//	* @throws Exception
//	*/
//	private void zip(String zipFileName, File inputFile) throws Exception {
//		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
//		zip(out, inputFile, "");
//		out.close();
//	}
//
//	/**
//	* 实现1
//	* @param out
//	* @param f
//	* @param base
//	* @throws Exception
//	*/
//	private void zip(ZipOutputStream out, File f, String base) throws Exception {
//		if (f.isDirectory()) {
//			File[] fl = f.listFiles();
//			out.putNextEntry(new org.apache.tools.zip.ZipEntry(base + File.separator));
//			base = base.length() == 0 ? "" : base + File.separator;
//			for (int i = 0; i < fl.length; i++) {
//				zip(out, fl[i], base + fl[i].getName());
//			}
//		} else {
//			out.putNextEntry(new org.apache.tools.zip.ZipEntry(base));
//			FileInputStream in = new FileInputStream(f);
//			int b;
//			System.out.println(base);
//			while ((b = in.read()) != -1) {
//				out.write(b);
//			}
//			in.close();
//		}
//	}
//
//	/**
//	* 压缩文件实现2
//	* @param sourceDir 需要压缩文件目录
//	* @param zipFile    压缩后zip文件名
//	*/
//	public static void zip(String sourceDir, String zipFile) {
//		OutputStream os;
//		try {
//			os = new FileOutputStream(zipFile);
//			BufferedOutputStream bos = new BufferedOutputStream(os);
//			ZipOutputStream zos = new ZipOutputStream(bos);
//			File file = new File(sourceDir);
//			String basePath = null;
//			if (file.isDirectory()) {
//				basePath = file.getPath();
//			} else {
//				basePath = file.getParent();
//			}
//			zipFile(file, basePath, zos);
//			zos.closeEntry();
//			zos.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	* 提供给实现2使用
//	* @param source
//	* @param basePath
//	* @param zos
//	*/
//	private static void zipFile(File source, String basePath, ZipOutputStream zos) {
//		File[] files = new File[0];
//		if (source.isDirectory()) {
//			files = source.listFiles();
//		} else {
//			files = new File[1];
//			files[0] = source;
//		}
//		String pathName;
//		byte[] buf = new byte[1024];
//		int length = 0;
//		try {
//			for (File file : files) {
//				if (file.isDirectory()) {
//					pathName = file.getPath().substring(basePath.length() + 1) + File.separator;
//					zos.putNextEntry(new ZipEntry(pathName));
//					zipFile(file, basePath, zos);
//				} else {
//					pathName = file.getPath().substring(basePath.length() + 1);
//					InputStream is = new FileInputStream(file);
//					BufferedInputStream bis = new BufferedInputStream(is);
//					zos.putNextEntry(new ZipEntry(pathName));
//					while ((length = bis.read(buf)) > 0) {
//						zos.write(buf, 0, length);
//					}
//					is.close();
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	* 解压 zip 文件，注意不能解压 rar 文件哦，只能解压 zip 文件 解压 rar 文件 会出现 java.io.IOException: Negative
//	* seek offset 异常 create date:2009- 6- 9 author:Administrator
//	*
//	* @param zipfile
//	*             zip 文件，注意要是正宗的 zip 文件哦，不能是把 rar 的直接改为 zip 这样会出现 java.io.IOException:
//	*             Negative seek offset 异常
//	* @param destDir
//	* @throws IOException
//	*/
//
//	public static void unZip(String zipfile, String destDir) {
//		destDir = destDir.endsWith(File.separator) ? destDir : destDir + File.separator;
//		byte b[] = new byte[1024];
//		int length;
//		ZipFile zipFile;
//		try {
//			zipFile = new ZipFile(new File(zipfile));
//			Enumeration enumeration = zipFile.getEntries();
//			ZipEntry zipEntry = null;
//			while (enumeration.hasMoreElements()) {
//				zipEntry = (ZipEntry) enumeration.nextElement();
//				File loadFile = new File(destDir + zipEntry.getName());
//				if (zipEntry.isDirectory()) {
//					// 这段都可以不要，因为每次都貌似从最底层开始遍历的
//					loadFile.mkdirs();
//				} else {
//					if (!loadFile.getParentFile().exists())
//						loadFile.getParentFile().mkdirs();
//					OutputStream outputStream = new FileOutputStream(loadFile);
//					InputStream inputStream = zipFile.getInputStream(zipEntry);
//					while ((length = inputStream.read(b)) > 0)
//						outputStream.write(b, 0, length);
//				}
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static void main(String[] args) {
//		unZip("E:\\++tmp+\\Txn-20130925141038.zip_1052511.zip", "E:\\++tmp+\\");
//	}
//}