package com.bill99.test;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TestLDAPService {
	private static Log log = LogFactory.getLog(TestLDAPService.class);
	private String ldapUrl = "LDAP://shpdc.99bill.com:389/DC=99bill,DC=com";

	/**
	 * 验证域用户
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public boolean validateLdap(String userName, String password) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		InitialContext iCnt = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", this.ldapUrl);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userName);
			envi.put("java.naming.security.credentials", password);
			iCnt = new InitialContext(envi);
			return iCnt == null ? false : true;
		} catch (Exception e) {
			//e.printStackTrace();
			log.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if (iCnt != null) {
					iCnt.close();
				}
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
	}

	/**
	 * 获取域用户名称
	 * 
	 * @param userCode
	 * @param password
	 * @return userName
	 */
	@SuppressWarnings("unchecked")
	public String getDomainUserName(String userCode, String password) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		LdapContext ctx = null;
		String userName = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", this.ldapUrl);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userCode);
			envi.put("java.naming.security.credentials", password);

			ctx = new InitialLdapContext(envi, null);
			if (ctx == null) {
				return userName;
			}
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String searchFilter = "mail=" + userCode;
			String returnedAtts[] = { "" }; // 定制返回属性   
			searchCtls.setReturningAttributes(returnedAtts); // 设置返回属性集
			NamingEnumeration answer = ctx.search("", searchFilter, searchCtls);
			if (answer == null) {
				return userName;
			}
			if (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String[] srNameInfo = sr.getName().split(",")[0].split("\\|")[0].split("=");
				userName = srNameInfo[srNameInfo.length - 1];
				log.info("login userName:" + userName);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if (ctx != null) {
					ctx.close();
				}
			} catch (NamingException e) {
				log.error(e.getMessage(), e);
			}
		}
		return userName;
	}

	/**
	 * 验证域用户是否存在,目前得不到用户密码不起作用
	 * 需要一个已有的帐号和密码来验证
	 * @param userName
	 * @return
	 */
	public boolean validateLdap(String userName) {
		Hashtable<String, String> envi = new Hashtable<String, String>();
		InitialContext iCnt = null;
		try {
			envi.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			envi.put("java.naming.provider.url", this.ldapUrl);
			envi.put(Context.SECURITY_AUTHENTICATION, "simple");
			envi.put("java.naming.security.principal", userName);
			iCnt = new InitialContext(envi);
			return iCnt == null ? false : true;
		} catch (Exception e) {
			//e.printStackTrace();
			log.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if (iCnt != null) {
					iCnt.close();
				}
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		TestLDAPService t = new TestLDAPService();
		String userName = "leo.feng@99bill.com";
		String password = "ioryouran9++";
		System.out.println(t.getDomainUserName(userName, password));
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}

}
