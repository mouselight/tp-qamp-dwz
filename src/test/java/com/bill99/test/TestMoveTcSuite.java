package com.bill99.test;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestMoveTcSuite extends AbstractJUnit4SpringContextTests {
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@Test
	public void moveTcSuiteTest() {
		System.out.println(calcTcQuantity(68L, 378538L));//378538L

	}

	//实时递归
	private long calcTcQuantity(Long testprojectId, Long pid) {
		long tcQuantity = 0;
		nodesHierarchyMng.initTestCase(testprojectId, pid);//初始化teemID数据用
		//		nodesHierarchyMng.recursive4InvalidTc(nodesHierarchyMng.getById(pid));//递归无效TC
		tcQuantity += nodesHierarchyMng.getTcQuantity(pid);
		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				tcQuantity += calcTcQuantity(testprojectId, suiteId);
			}
		}
		return tcQuantity;
	}
}
