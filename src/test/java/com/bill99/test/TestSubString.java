/*
   File: 
   Description:
   Copyright 2004-2010 99Bill Corporation. All rights reserved.
    Date            Author          Changes
   2011-9-6		    Leo Feng	   		Leo Feng
*/

package com.bill99.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Leo Feng
 *
 */
public class TestSubString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Pattern pattern = Pattern.compile("(G+\\d:)");
		String str = "G1:0瓦;精度0.01至0.03;G2:-3;G4:析功能;检测对象 混合气体;型号GPR-1200;无试纸试剂打印机;";
		List<String> strList = Arrays.asList(pattern.split(str));

		List<String> tempList = new ArrayList<String>();
		Matcher m = pattern.matcher(str);
		int i = 1;
		while (m.find()) {
			tempList.add(str.substring(m.start(), m.end()) + strList.get(i));
			i++;
		}
		for (String string : tempList) {
			System.out.println(string);
		}

		//		for (int i = 1; i < strList.size(); i++) {
		//			tempList.add("G" + i + ":" + strList.get(i));
		//		}
		//		if(m.find()){
		//		System.out.println(m.group());
		//		}

		//		for (int i = 1; i < strs.length; i++) {
		//			System.out.println(strs[i]);
		//		}

		//		String input = "This!!unusual use!!of exclamation!!points";
		//		System.out.println(Arrays.asList(Pattern.compile("!!").split(input)));
		//		// Only do the first three:
		//		System.out.println(Arrays.asList(Pattern.compile("!!").split(input, 3)));
		//		System.out.println(Arrays.asList("Aha! String has a split() built in!".split(" ")));

		//		String input = "G1:0瓦;精度0.01至0.03;G2:-3;G3:析功能;检测对象 混合气体;型号GPR-1200;无试纸试剂打印机;";
		//		System.out.println(Arrays.asList(Pattern.compile("(\\G+\\d\\:)").split(input)));
	}
}
