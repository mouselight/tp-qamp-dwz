package com.bill99.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.bill99.testmp.testmanage.orm.manager.TestCaseCloudMng;
import com.bill99.testmp.testmanage.web.controller.command.TestCaseCloudCommand;

@ContextConfiguration(locations = { "classpath:context/applicationContext.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
public class TestCaseCloud extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestCaseCloudMng testCaseCloudMng;

	@Test
	public void testQuery() {
		TestCaseCloudCommand cmd = new TestCaseCloudCommand();
		cmd.setTeamId(72L);
		System.out.println(testCaseCloudMng.query(cmd).getTotalCount());
	}

	@Test
	public void getCloudMap() {
		System.out.println(testCaseCloudMng.getCloudNameMap(10214L));
	}
	
	@Test
	public void update() {
	}
}
