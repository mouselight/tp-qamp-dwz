package com.bill99.testmp.framework.hession;

import org.junit.Test;

import com.caucho.hessian.client.HessianProxyFactory;

public class QaInterfaceTest {

	@Test
	public void testSaveQaTestResult() {

		boolean result = false;
		String releaseId = "CRL-12Q4-INF102399";
		String qaFileSvnUrl = "http://svn.99bill.net/opt/99billdoc/PUBLIC/POA/QA/Qamp-Report/20130228-INFS_BGW-测试报告.xls";

		int qaTestResult = 2;
		String qaMemo = "测试测试测试leo222";
		String submitUserId = "lory.shi@99bill.com";
		//		String submitUserId = "leo.feng@99bill.com";
		//		submitUserId = null;
		try {
			HessianProxyFactory proxy = new HessianProxyFactory();
			QaInterface qaInterface = (QaInterface) proxy.create(QaInterface.class, "http://kd.99bill.net/ctmp/qaInterface.ws");
			result = qaInterface.saveQaTestResult(releaseId, qaFileSvnUrl, qaTestResult, qaMemo, submitUserId);
			System.out.println("result: " + result);
		} catch (Exception e) {
			System.err.println("-- send ctmp error ,releaseId = " + releaseId + "--" + e);
		}

	}

	@Test
	public void testSaveQaPrdTestResult() {
		boolean result = false;
		String releaseId = "CRL-12Q4-INF102399";
		String qaFileSvnUrl = "http://svn.99bill.net/opt/99billdoc/PUBLIC/POA/QA/Qamp-Report/20130228-INFS_BGW-测试报告.xls";

		Integer qaTestResult = 2;
		//		qaTestResult = null;
		String qaMemo = "测试测试测试leo2";
		String submitUserId = "leo.feng@99bill.com";
		try {
			HessianProxyFactory proxy = new HessianProxyFactory();
			QaInterface qaInterface = (QaInterface) proxy.create(QaInterface.class, "http://kd.99bill.net/ctmp/qaInterface.ws");
			result = qaInterface.saveQaPrdTestResult(releaseId, qaFileSvnUrl, qaTestResult, qaMemo, submitUserId);
			System.out.println("result: " + result);
		} catch (Exception e) {
			System.err.println("-- send ctmp error ,releaseId = " + releaseId + "--" + e);
		}

	}

}
