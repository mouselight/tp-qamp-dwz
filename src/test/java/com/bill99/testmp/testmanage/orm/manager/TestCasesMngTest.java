package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.entity.TestCases;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestCasesMngTest extends AbstractJUnit4SpringContextTests {
	@Autowired
	private TestCasesMng testCaseMng;

	@Test
	public void testGetPlanTrackCases() {
		Long planTrackId = 1367055705415L;
		List<TestCases> testCaseList = testCaseMng.getPlanTrackCases(planTrackId);
		for (TestCases testCases : testCaseList) {
			System.out.println(testCases.getTestCaseName());
		}
	}

}
