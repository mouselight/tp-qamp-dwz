package com.bill99.testmp.testmanage.orm.manager;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class JiraMngTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private JiraMng jiraMng;

	@Test
	public void testInitJiraData() {
		jiraMng.initJiraData();
	}

}
