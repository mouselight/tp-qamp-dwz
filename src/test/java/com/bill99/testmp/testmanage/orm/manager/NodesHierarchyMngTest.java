package com.bill99.testmp.testmanage.orm.manager;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class NodesHierarchyMngTest extends AbstractJUnit4SpringContextTests {
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@Test
	public void testGetGroup() {

		Set<Long> pids = new HashSet();
		pids.add(62L);
		nodesHierarchyMng.getGroup(pids);
	}
}
