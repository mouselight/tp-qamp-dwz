package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.domain.TestManageService;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestManageServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestManageService testManageService;

	@Test
	public void testInitJiraData() {
		testManageService.getProjectGroup(1373866128293L);

	}

}
