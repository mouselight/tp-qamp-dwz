package com.bill99.testmp.testmanage.orm.manager;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.entity.TestplanSteps;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestplanStepsMngTest extends AbstractJUnit4SpringContextTests {
	@Autowired
	private TestplanStepsMng testplanStepsMng;

	@Test
	public void testGetUnitStepsByStepId() {
		List<TestplanSteps> unitSteps = testplanStepsMng.getUnitStepsByStepId(1365661994080L);

		if (unitSteps != null) {
			for (TestplanSteps testplanSteps : unitSteps) {
				System.out.println(testplanSteps.getUnitType());
			}
		}
	}

	@Test
	public void testGet() {
		TestplanSteps testplanSteps = testplanStepsMng.get(1365661994080L);
		System.out.println(testplanSteps.getParent());
	}

	@Test
	public void testEnvStep() {
		Map<Long,String> testEnvSteps=testplanStepsMng.getEnvStep(1365661994080L);
		
		for (Map.Entry entry : testEnvSteps.entrySet()) {
		    String key = entry.getKey().toString();
		    String value = entry.getValue().toString();
		    System.out.println("key=" + key + " value=" + value);
		   }
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAssoTcIds() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveFullAssoParentIds() {
		fail("Not yet implemented");
	}

	@Test
	public void testFinishAssoTc() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveAssoTc() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTcIdLong() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTcIdLongInteger() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAssoTcCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAssoCloud() {
		fail("Not yet implemented");
	}

}
