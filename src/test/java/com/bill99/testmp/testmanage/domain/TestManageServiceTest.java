package com.bill99.testmp.testmanage.domain;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.common.utils.TreeUtils;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.entity.TestProjects;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestManageServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestManageService testManageService;
	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;

	@Test
	public void testInitTreeTcQuantity() {

		testManageService.initTreeTcQuantity();
		List<NodesHierarchy> roots = nodesHierarchyMng.getRoots();
		for (NodesHierarchy nodesHierarchy : roots) {

			System.out.println(TreeUtils.TC_QUANTITY_MAP.get(nodesHierarchy.getId().toString()));

			//			if (TreeUtils.TC_QUANTITY_MAP.get(nodesHierarchy.getId().toString()).equals("(0)")) {
			//				System.out.println(nodesHierarchy.getName());
			//			}
		}

	}

	/*
	 * 删除空用例集
	 */
	@Test
	public void testCalcTcQuantity() {
		long a = calcTcQuantity(1589091L);
		System.out.println(a);
	}

	//实时递归
	private long calcTcQuantity(Long pid) {
		long tcQuantity = 0;
		tcQuantity += nodesHierarchyMng.getTcQuantity(pid);
		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				tcQuantity += calcTcQuantity(suiteId);
			}
		}
		return tcQuantity;
	}

	@Test
	public void testGetNodesTree() {

	}

	@Test
	public void testGetProjectGroup() {
		List<TestProjects> testProjects = testManageService.getProjectGroup(1365661994080L);
		for (TestProjects testProjects2 : testProjects) {
			System.out.println(testProjects2.getPrefix());
		}
	}

}
