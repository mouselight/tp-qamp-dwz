package com.bill99.testmp.testmanage.domain;

import net.sf.json.JSONObject;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.common.dto.TpTaskRequestBo;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TpTaskServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TpTaskService tpTaskService;

	@Test
	public void testTriggerTpTask() {

		TpTaskRequestBo tpTaskRequestBo = new TpTaskRequestBo();
		tpTaskRequestBo.setJobName("99billTest");
		tpTaskRequestBo.setTpRuntime("1");
		tpTaskRequestBo.setPublishVersion("1.2.0");
		tpTaskRequestBo.setTpTestinterface("null");
		tpTaskRequestBo.setTpUsers("1");

		JSONObject json = JSONObject.fromObject(tpTaskRequestBo);
		System.out.println(json.toString());

		JSONObject jsonObject = JSONObject.fromObject(json.toString());

		TpTaskRequestBo tpTaskRequestBo2 = (TpTaskRequestBo) JSONObject.toBean(jsonObject, TpTaskRequestBo.class);

		System.out.println(tpTaskRequestBo2.getJobName());

		//		TpTaskResponesBo tpTaskResponesBo = tpTaskService.triggerTpTask(tpTaskRequestBo);

		//		System.out.println(tpTaskResponesBo.getMemo());
	}
}
