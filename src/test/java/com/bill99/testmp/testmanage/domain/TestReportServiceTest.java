package com.bill99.testmp.testmanage.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.entity.TestReport;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestReportServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TestReportService testReportService;

	
	//必测回归集报告导出
	@Test
	public void testTestReport() {
		TestReport testReport = new TestReport();

		testReport.setIssueId(11382L);
		testReport.setStepId(1387187275665l);
		testReport.setStepType(6);

		try {
			testReportService.testReport(null, testReport);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
   //项目测试报告导出
	@Test
	public void tcTestEnviroSheet() {
		TestReport testReport = new TestReport();
//		testReport.setStepId(1365661994080L);
//		testReport.setIssueId(999999L);
//		testReport.setStepType(6);
//		
		testReport.setStepId(1376384393939L);
		testReport.setIssueId(19485L);
		testReport.setStepType(6);
		
		
		
		
		try {
			testReportService.testReport(null, testReport);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//
	//	@Test
	//	public void testGetAllParentTc() {
	//		fail("Not yet implemented");
	//	}
	//
	//	@Test
	//	public void testGetCaseStatis() {
	//		fail("Not yet implemented");
	//	}
	//
	//	@Test
	//	public void testGetNodesId() {
	//		fail("Not yet implemented");
	//	}

}
