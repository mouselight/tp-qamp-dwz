package com.bill99.testmp.testmanage.domain;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.riaframework.common.dto.CaseStateDto;
import com.bill99.testmp.testmanage.orm.entity.NodesHierarchy;
import com.bill99.testmp.testmanage.orm.ibatis.dao.NodesHierarchyIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.NodesHierarchyMng;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestMaReporter extends AbstractJUnit4SpringContextTests {

	@Autowired
	private NodesHierarchyMng nodesHierarchyMng;
	@Autowired
	private NodesHierarchyIbatisDao nodesHierarchyIbatisDao;

	@Test
	public void test() {
		maReporter(1377519542793L);//步骤ID
	}

	private void maReporter(Long stepId) {

		//		Long[] appIds = new Long[] { 885324L, };
		int[] appIds = new int[] {

		523327, //POSP
				816198,//POSP机业务
				885324,//IDS
				885325,//MRS
				885326,//BGW
				1600137,//VPOS
				1600138,//POSP
				1600139,//MGW
				1600140,//internal
				1600141,//tais
				1600142,//MNP
				1600143,//OQS
				1600146,//PE
				1600147,//CCS
				1600148,//COD
				1600151,//反接POSP
				1600152,//MAS
				1600408,//快捷支付
				3799822,//CPS定时任务
				3800250,//MCS
				1610954,//MAS
				2218257,//亚马逊
				2349020,//平安积分支付
				23487,//INO

		};//应用ID
		for (int appIdInt : appIds) {
			Long appId = Long.valueOf(String.valueOf(appIdInt));
			NodesHierarchy nf = nodesHierarchyMng.getById(appId);
			CaseStateDto caseStateDto = getTcExecute4Recursive(appId, stepId);
			System.out.print(nf.getName() + "  			  ");
			System.out.print(caseStateDto.getAllCnt() + "  			");
			System.out.println(caseStateDto.getDoneCnt());
		}

	}

	private CaseStateDto getTcExecute4Recursive(Long pid, Long stepId) {
		int allCnt = 0;
		int doneCnt = 0;
		CaseStateDto caseStateDto = nodesHierarchyIbatisDao.getTcStateByStep(pid, stepId);
		allCnt += caseStateDto.getAllCnt();
		doneCnt += caseStateDto.getDoneCnt();

		List<Long> suiteIds = nodesHierarchyMng.getChildHql4Count(pid);
		if (suiteIds != null) {
			for (Long suiteId : suiteIds) {
				caseStateDto = getTcExecute4Recursive(suiteId, stepId);
				allCnt += caseStateDto.getAllCnt();
				doneCnt += caseStateDto.getDoneCnt();
			}
		}
		caseStateDto.setAllCnt(allCnt);
		caseStateDto.setDoneCnt(doneCnt);
		return caseStateDto;

	}
}
