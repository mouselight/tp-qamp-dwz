package com.bill99.testmp.testmanage.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class TestNotifierTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	@Qualifier("bugsNotifier")
	private TestNotifier bugsNotifier;

	@Autowired
	@Qualifier("testSchedulesNotifier")
	private TestNotifier testSchedulesNotifier;

	@Autowired
	@Qualifier("testIssueNotifier")
	private TestNotifier testIssueNotifier;

	@Test
	public void testReportorProcess4bugsNotifier() {
		bugsNotifier.reportorProcess();
	}

	@Test
	public void testReportorProcess4testSchedulesNotifier() {
		testSchedulesNotifier.reportorProcess();
	}

	@Test
	public void testReportorProcess4testIssueNotifier() {
		testIssueNotifier.reportorProcess();
	}

}
