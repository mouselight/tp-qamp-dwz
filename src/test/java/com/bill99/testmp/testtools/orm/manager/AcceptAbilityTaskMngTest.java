package com.bill99.testmp.testtools.orm.manager;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.seashell.orm.pagination.Page;
import com.bill99.testmp.testtools.orm.entity.AcceptAbilityTask;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class AcceptAbilityTaskMngTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AcceptAbilityTaskMng acceptAbilityTaskMng;

	@Test
	public void testSave() {
		for (int i = 0; i < 50; i++) {
			AcceptAbilityTask acceptAbilityTask = new AcceptAbilityTask();
			acceptAbilityTask.setpKey(i + "");
			acceptAbilityTask.setProductName("test" + i);
			acceptAbilityTask.setSubtaskStatus("00");
			acceptAbilityTaskMng.save(acceptAbilityTask);
		}

	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testQueryForPage() {

		AcceptAbilityTask acceptAbilityTask = new AcceptAbilityTask();
		acceptAbilityTask.setProductName("j");
		Page page = new Page();

		page.setPageSize(2);
		page.setTargetPage(1);

		List<AcceptAbilityTask> acceptAbilityTasks = acceptAbilityTaskMng.queryForPage(acceptAbilityTask, page);
		for (AcceptAbilityTask acceptAbilityTask2 : acceptAbilityTasks) {
			System.out.println(acceptAbilityTask2.getIdTask());
		}
	}

}
