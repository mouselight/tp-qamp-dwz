package com.bill99.testmp.testtools.orm.manager;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testtools.orm.entity.BankPanInfo;
import com.bill99.testmp.testtools.orm.entity.TaskLogInfo;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class BankPanInfoMngTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private BankPanInfoMng bankPanInfoMng;

	@Test
	public void getByPan() {
		BankPanInfo  bankPanInfo = bankPanInfoMng.queryById(4L);
		System.out.println(bankPanInfo);
	}

}
