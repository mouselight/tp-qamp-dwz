package com.bill99.testmp.testtools.orm.manager;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testtools.orm.entity.AcceptAbilitySubTask;

@ContextConfiguration(locations = { "classpath:test/context/applicationContext.xml" })
public class AcceptAbilitySubTaskMngTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AcceptAbilitySubTaskMng acceptAbilitySubTaskMng;

	@Test
	public void testQueryForList() {
		List<AcceptAbilitySubTask> acceptAbilitySubTasks = acceptAbilitySubTaskMng.queryForList(2218L);
		for (AcceptAbilitySubTask acceptAbilitySubTask : acceptAbilitySubTasks) {
			if (acceptAbilitySubTask.getBankPanInfo() != null) {
				System.out.println(acceptAbilitySubTask.getBankPanInfo().getCardholder());
			}
		}
	}
}
