/**
 * @author ZhangHuihua@msn.com
 */
(function($){
	$.fn.extend({
		
		checkboxCtrl: function(parent){
			return this.each(function(){
				var $trigger = $(this);
				$trigger.click(function(){
					var group = $trigger.attr("group");
					if ($trigger.is(":checkbox")) {
						var type = $trigger.is(":checked") ? "all" : "none";
						if (group) $.checkbox.select(group, type, parent);
					} else {
						if (group) $.checkbox.select(group, $trigger.attr("selectType") || "all", parent);
					}
					
				});
			});
		}
	});
	
	$.checkbox = {
		selectAll: function(_name, _parent){
			this.select(_name, "all", _parent);
		},
		unSelectAll: function(_name, _parent){
			this.select(_name, "none", _parent);
		},
		selectInvert: function(_name, _parent){
			this.select(_name, "invert", _parent);
		},
		select: function(_name, _type, _parent){
			$parent = $(_parent || document);
			$checkboxLi = $parent.find(":checkbox[name='"+_name+"']");
			switch(_type){
				case "invert":
					$checkboxLi.each(function(){
						$checkbox = $(this);
						//$checkbox.attr('checked', !$checkbox.is(":checked")); --old
						// behin modify by William 20120830 增加点击时对disabled的checkBox排除 添加条件判断
						if(!$checkbox.attr('disabled')){
							$checkbox.attr('checked', !$checkbox.is(":checked"));
						}
						// end
					});
					break;
				case "none":
					$checkboxLi.attr('checked', false);
					break;
				default:
					// $checkboxLi.attr('checked', true); --old
					// begin modify by William 20120830 增加全选时对disabled的checkBox排除 
					$checkboxLi.each(function(){
						$checkbox = $(this); 
						//alert($checkbox.attr('disabled'));
						if(!$checkbox.attr('disabled')){
							$checkbox.attr('checked', true);
						}
					});
					// end
					break;
			}
		}
	};
})(jQuery);
