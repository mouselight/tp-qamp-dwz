<#--
表格标签：用于显示列表数据。
	value：列表数据，可以是Pagination也可以是List。
	class：table的class样式。默认"pn-ltable"。
	sytle：table的style样式。默认""。
	width：表格的宽度。默认100%。
-->
<#macro table value listAction="v_list.htm" class="table" style="" theadClass="" tbodyClass="" width="100%">
<table class="${class}" style="${style}" width="${width}" cellspacing="1" cellpadding="0" border="0">
<#if !value?exists || (!value?is_sequence && !value.list?exists) >
	<div class="pn-lnoresult">没有相关数据！</div>
<#else>
<#if value?is_sequence><#local pageList=value/><#else><#local pageList=value.list/></#if>
<#if !pageList?exists || pageList?size <= 0>
	<div class="pn-lnoresult">没有相关数据！</div>
<#else>
<#list pageList as row>
<#if row_index==0>
<#assign i=-1/>
<thead class="${theadClass}"><tr target="slt_uid" rel="${row.id}"><#nested row,i,true/></tr></thead>
</#if>
<#assign i=row_index has_next=row_has_next/>
<#if row_index==0><tbody  class="${tbodyClass}"><tr target="slt_uid" rel="${row.id}"><#else><tr target="slt_uid" rel="${row.id}"></#if><#nested row,row_index,row_has_next/>
<#if !row_has_next>
</tr></tbody>
<#else>
</tr>
</#if>
</#list>
</#if></#if>
</table>

<#if pageList?exists && pageList?size!=0 >
<script type="text/javascript">
$(function() {
	$('#pageSize').change(function() {_gotoPage(1)});
	$('#pageSize').val(${value.pageSize!});
	})
function _gotoPage(pageNo) {
	try{
		var tableForm = getTableForm();
		$("input[name=pageNo]").val(pageNo);
		tableForm.action="${listAction}";
		tableForm.onsubmit="pageForm required-validate";
		tableForm.onsubmit="return validateCallback(tableForm);
		tableForm.submit();
	} catch(e) {
		alert('_gotoPage(pageNo)方法出错');
	}
}
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="center" class="pn-sp">
	共 ${value.totalCount} 条&nbsp;
	每页
	<select name="pageSize" id="pageSize">
		 <option value="10">10</option>
		 <option value="20">20</option>
		 <option value="40">40</option>
		 <option value="100">100</option>
 	</select>条&nbsp;
	<input type="button" value="首 页" onclick="_gotoPage('1');"<#if value.firstPage> disabled="disabled"</#if>/>
	<input type="button" value="上一页" onclick="_gotoPage('${value.prePage}');"<#if value.firstPage> disabled="disabled"</#if>/>
	<input type="button" value="下一页" onclick="_gotoPage('${value.nextPage}');"<#if value.lastPage> disabled="disabled"</#if>/>
	<input type="button" value="尾 页" onclick="_gotoPage('${value.totalPage}');"<#if value.lastPage> disabled="disabled"</#if>/>&nbsp;
	当前 ${value.pageNo}/${value.totalPage} 页 &nbsp;转到第<input type="text" id="_goPs" style="width:50px" onfocus="this.select();" onkeypress="if(event.keyCode==13){$('#_goPage').click();return false;}"/>页
	<input id="_goPage" type="button" value="转" onclick="_gotoPage($('#_goPs').val());"<#if value.totalPage==1> disabled="disabled"</#if>/>
	<input type="hidden" name="pageNo" value="${value.pageNo}"/>
</td></tr></table>
</#if>
</#macro>